{
 "cells": [
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import mmf_setup;mmf_setup.nbinit()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Resampling"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Here we consider an alternate approach of resampling. Suppose we have a periodic representation of $\\psi(x)$ in a box of length $L$ *and* know that to high accuracy, it vanishes analytically at the boundaries.  Then we can use the periodic representation to sample it on a larger lattice, explicitly setting the function to zero outside.  Here we demonstrate with a gaussian.\n",
    "\n",
    "$$\n",
    "  \\psi(x) \\propto e^{-x^2/a^2/2}, \\qquad\n",
    "  \\tilde{\\psi}(k) \\propto e^{-a^2k^2/2}\n",
    "$$"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%pylab inline --no-import-all\n",
    "import numpy as np\n",
    "from numpy.fft import fftshift as fs_\n",
    "\n",
    "eps = np.finfo(float).eps\n",
    "L = np.sqrt(-8.0*np.log(eps))\n",
    "# k_max = N*np.pi/L\n",
    "N = int(np.ceil(L * np.sqrt(-2.0*np.log(eps))/np.pi))\n",
    "dx = L/N\n",
    "print(N)\n",
    "x = np.arange(N)*dx - L/2\n",
    "k = 2*np.pi * np.fft.fftfreq(N, dx)\n",
    "\n",
    "def f(x):\n",
    "    \"\"\"Gaussian function.\"\"\"\n",
    "    return np.exp(-x**2/2.0)\n",
    "def ft(k):\n",
    "    \"\"\"Analytic Fourier transform.\"\"\"\n",
    "    return np.sqrt(2*np.pi)*np.exp(-0.5j*k*L)*np.exp(-k**2/2.0)/dx\n",
    "psi = f(x)\n",
    "psit = np.fft.fft(psi)\n",
    "assert np.allclose(psit, ft(k))\n",
    "plt.semilogy(x, abs(psi)/abs(psi).max())\n",
    "plt.semilogy(fs_(k), fs_(abs(psit)/abs(psit).max()), '+')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Note that we have a good approximation of the density, but that we have lost accuracy (as expected) in Fourier space."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "To interpolate to a larger box, we can use the explicit form of the Fourier basis:\n",
    "\n",
    "$$\n",
    "  \\psi(x) = \\frac{1}{N}\\sum_{k} e^{\\I k x}\\tilde{\\psi}_k.\n",
    "$$\n",
    "\n",
    "This, however, is valid only if the abscissa $x$ start from $0$.  In our case, we have shifted by $L/2$ to place the origin in the middle of the interval, so we have:\n",
    "\n",
    "$$\n",
    "  \\psi(x) = \\frac{1}{N}\\sum_{k} e^{\\I k (x + L/2)}\\tilde{\\psi}_k.\n",
    "$$\n",
    "\n",
    "To interpolate to a large box with coordinates $x_2 = L_2 x/L$ we simply apply this at the new abscissa, and then set the values to zero outside the original box:\n",
    "\n",
    "$$\n",
    "  \\psi(x_2) = \\begin{cases}\n",
    "    \\frac{1}{N}\\sum_{k} e^{\\I k (x_2 + L/2)}\\tilde{\\psi}_k & \\abs{x_2} < \\frac{L}{2}\\\\\n",
    "    0 & \\text{otherwise}\n",
    "    \\end{cases}\n",
    "$$"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from scipy.interpolate import InterpolatedUnivariateSpline\n",
    "L2 = 2*L\n",
    "N2 = N+32\n",
    "dx2 = L2/N2\n",
    "x2 = np.arange(N2)*dx2 - L2/2\n",
    "k2 = 2*np.pi * np.fft.fftfreq(N2, dx2)\n",
    "psi2_ = np.exp(1j*(x2[:, None] + L/2)*k[None,:]).dot(np.fft.fft(psi))/N\n",
    "psi2 = np.where(abs(x2) < L/2, psi2_, 0.0)\n",
    "psi2s = InterpolatedUnivariateSpline(x, psi, k=5, ext=1)(x2)\n",
    "\n",
    "def f2t(k):\n",
    "    \"\"\"Analytic Fourier transform.\"\"\"\n",
    "    return np.sqrt(2*np.pi)*np.exp(-0.5j*k*L2)*np.exp(-k**2/2.0)/dx2\n",
    "\n",
    "assert np.allclose(psi2, f(x2))\n",
    "assert np.allclose(psi2s, f(x2), rtol=1e-4, atol=1e-7)\n",
    "psi2t = np.fft.fft(psi2)\n",
    "psi2st = np.fft.fft(psi2s)\n",
    "plt.semilogy(x2, abs(psi2)/abs(psi2).max())\n",
    "plt.semilogy(fs_(k2), fs_(abs(psi2t)/abs(psi2t).max()), '+')\n",
    "plt.semilogy(x2, abs(psi2_)/abs(psi2).max(), ':')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "plt.plot(x2, psi2)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Here are the two wavefunctions demonstrating the cutoff at the boundary.  Since this is small enough (the original box was large enough), the sharp features at the edges do not significantly impact the Fourier transform.  Here we compare the errors between the Fourier method and the Spline method."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "plt.subplot(211)\n",
    "plt.plot(x2, abs(psi2 - f(x2))/abs(psi2).max(), 'b', label='Fourier')\n",
    "plt.semilogy(x2, abs(psi2s - f(x2))/abs(psi2).max(), 'g', label='Spline')\n",
    "plt.legend(loc='best')\n",
    "plt.subplot(212)\n",
    "plt.plot(fs_(k2), fs_(abs(psi2t - f2t(k2)))/abs(psi2t).max(), 'b')\n",
    "plt.semilogy(fs_(k2), fs_(abs(psi2st - f2t(k2)))/abs(psi2t).max(), 'g')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In the physical regions, the Fourier method achieves machine precision as expected.  For larger momenta, we see the effects of the truncation.  The only way I know of avoiding this is to make sure that the physical constraints are satisfied whenever the box is increased."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Resampling in 3D"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Here we resample a wavefunction in 3D and address questions of performance.  There is no obvious way of implementing this procedure with the FFT, however, since the grid is rectilinear, we can apply each interpolation independently which should work fairly quickly.  Here we use `np.einsum` which is not the fastest, but is convenient.  Things could probably be sped up using BLAS which can make use of multiple cores etc."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%pylab inline --no-import-all\n",
    "eps = np.finfo(float).eps\n",
    "L = np.sqrt(-8.0*np.log(eps))\n",
    "Nxyz = (2**7, 2**7, 2**6)\n",
    "Lxyz = np.array((L,)*len(Nxyz))\n",
    "xyz = np.meshgrid(\n",
    "    *[np.arange(_N)*_L/_N - _L/2.0 for (_N, _L) in zip(Nxyz, Lxyz)],\n",
    "    indexing='ij', sparse=True)\n",
    "kxyz = np.meshgrid(\n",
    "    *[2*np.pi * np.fft.fftfreq(_N, _L/_N) for (_N, _L) in zip(Nxyz, Lxyz)],\n",
    "    indexing='ij', sparse=True)\n",
    "\n",
    "Nxyz2 = np.array(Nxyz) + (16, 16, 16)\n",
    "Lxyz2 = 2.0*Lxyz\n",
    "\n",
    "xyz2 = np.meshgrid(\n",
    "    *[np.arange(_N)*_L/_N - _L/2.0 for (_N, _L) in zip(Nxyz2, Lxyz2)],\n",
    "    indexing='ij', sparse=True)\n",
    "kxyz2 = np.meshgrid(\n",
    "    *[2*np.pi * np.fft.fftfreq(_N, _L/_N) for (_N, _L) in zip(Nxyz2, Lxyz2)],\n",
    "    indexing='ij', sparse=True)\n",
    "\n",
    "def f(r):\n",
    "    \"\"\"Gaussian function.\"\"\"\n",
    "    return np.exp(-r**2/2.0)\n",
    "\n",
    "r = np.sqrt(sum(_x**2 for _x in xyz))\n",
    "r2 = np.sqrt(sum(_x**2 for _x in xyz2))\n",
    "\n",
    "psi = f(r=r)\n",
    "psi2 = f(r=r2)\n",
    "%time psit = np.fft.fftn(psi)\n",
    "\n",
    "\n",
    "x, y, z = xyz\n",
    "kx, ky, kz = kxyz\n",
    "x2, y2, z2 = xyz2\n",
    "kx2, ky2, kz2 = kxyz2\n",
    "\n",
    "Nx, Ny, Nz = Nxyz\n",
    "Nx2, Ny2, Nz2 = Nxyz2\n",
    "Lx, Ly, Lz = Lxyz\n",
    "Lx2, Ly2, Lz2 = Lxyz2\n",
    "\n",
    "\n",
    "# Get the interpolation matrices\n",
    "def Q(x2, kx, Lx):\n",
    "    res = np.exp(1j*(x2.ravel()[:, None] + Lx/2.0)*kx.ravel()[None, :])\n",
    "    res[np.where(abs(x2.ravel()) >= Lx/2.0)[0], ...] = 0.0\n",
    "    return res\n",
    "\n",
    "Qxyz = [Q(_x2, _k, _L)/_N for (_x2, _k, _L, _N) in zip(xyz2, kxyz, Lxyz, Nxyz)]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%%time\n",
    "tmp = psit\n",
    "\n",
    "tmp = np.einsum('xyz,ax->ayz', tmp, Qxyz[0])\n",
    "tmp = np.einsum('xyz,ay->xaz', tmp, Qxyz[1])\n",
    "tmp = np.einsum('xyz,az->xya', tmp, Qxyz[2])\n",
    "\n",
    "psi2_ = tmp\n",
    "\n",
    "assert np.allclose(psi2_, psi2)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "On my computer, this is about 8 times slower than computing a single FFT, so if we do this at most every 8 steps or so, it should not slow down the performance more than by a factor of 2.  Here we play with a few other methods... `np.tensordot` seems to be the fastest."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "res = np.einsum('xyz,az->xya', psit, Qxyz[2])\n",
    "assert np.allclose(res, np.dot(psit, Qxyz[2].T))\n",
    "assert np.allclose(res, np.tensordot(psit, Qxyz[2].T, axes=1))\n",
    "assert np.allclose(res, np.tensordot(psit, Qxyz[2], axes=[[2], [1]]))\n",
    "\n",
    "%timeit np.einsum('xyz,az->xya', psit, Qxyz[2])\n",
    "%timeit np.tensordot(psit, Qxyz[2].T, axes=1)\n",
    "%timeit np.tensordot(psit, Qxyz[2], axes=[[2], [1]])\n",
    "%timeit np.dot(psit, Qxyz[2].T)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "res1 = np.einsum('xyz,ax->ayz', psit, Qxyz[0])\n",
    "res2 = np.einsum('xyz,ay->xaz', psit, Qxyz[1])\n",
    "res3 = np.einsum('xyz,az->xya', psit, Qxyz[2])\n",
    "assert np.allclose(res1, np.tensordot(Qxyz[0], psit, axes=1))\n",
    "assert np.allclose(res2, np.rollaxis(np.tensordot(psit, Qxyz[1], axes=[[1], [1]]), 1, 3))\n",
    "assert np.allclose(res3, np.tensordot(psit, Qxyz[2], axes=[[2], [1]]))\n",
    "\n",
    "%timeit np.einsum('xyz,az->xya', psit, Qxyz[2])\n",
    "%timeit np.tensordot(Qxyz[0], psit, axes=1)\n",
    "%timeit np.rollaxis(np.tensordot(psit, Qxyz[1], axes=[[1], [1]]), 1, 3)\n",
    "%timeit np.tensordot(psit, Qxyz[2], axes=[[2], [1]])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Conclusions"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Resampling in Fourier space can work well.  One simply evaluates the wavefunction using the Fourier basis at the new abscissa, then sets the value of the function to zero outside the initial interval.  There are some conditions however:\n",
    "\n",
    "1. The initial wavefunction *and its derivatives* must be zero (i.e. at machine precision) at the boundary, otherwise, high-momentum components will be introduced by the truncation proceedure.\n",
    "2. The new box must contain enough points to properly cover the support of the wavefunction in momentum space.\n",
    "\n",
    "One con of the method is that it can be slow since we cannot use the FFT and must use matrix multiplication, however, we can do this in each dimensions independently and using `np.tensordot` seems pretty quick."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Tests (incomplete)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In this notebook we will explore how to treat an expanding BEC cloud.  We shall construct the potential so that the time-dependent solution is\n",
    "\n",
    "$$\n",
    "  \\psi(x, t) = e^{\\I\\phi(x,t)} e^{-x^2/2a(t)^2}\n",
    "$$\n",
    "\n",
    "where $a(t)$ is a function of time.  The GPE has the form:\n",
    "\n",
    "$$\n",
    "  \\I\\hbar \\dot{\\psi} = - \\frac{\\hbar^2}{2m}\\psi'' + gn(x,t)\\psi + V(x,t)\\psi\\\\\n",
    "$$\n",
    "\n",
    "Let $y = x/a(t)$ be the invariant coordinate.  This is an exact solution iff:\n",
    "\n",
    "$$\n",
    "- \\frac{x^2}{a^4} + (\\phi')^2 + \\frac{1}{a^2} + \\frac{2m}{\\hbar}\\left(\\dot{\\phi} + \\frac{V+gn}{\\hbar}\\right) = 0 \\\\\n",
    " \\phi'' - \\frac{2}{a^2}x\\phi' + \\frac{2m \\dot{a}}{\\hbar a^3}x^2 = 0.\n",
    "$$\n",
    "\n",
    "The latter equation has the general solution in terms of $y = x/a$:\n",
    "\n",
    "$$\n",
    "  \\DeclareMathOperator{\\erfi}{erfi}\n",
    "  \\phi'(x) = C_1e^{y^2} - \\frac{m\\dot{a}}{2\\hbar}\\left(e^{y^2}\\sqrt{\\pi}\\erf y - 2 y\\right)\\\\\n",
    "  \\phi(x) = C_0 + C_1a\\frac{\\sqrt{\\pi}}{2}\\erfi{y} - \\frac{m a\\dot{a}}{2\\hbar}y^2\\left({}_2F_2(1,1;\\tfrac{3}{2}, 2;y^2) - 1\\right)\\\\\n",
    "$$\n",
    "\n",
    "Keeping the phase $\\phi(0) = 0$ in the center and flow $\\phi'(0) = 0$ (maintained by parity symmetry if we do not start with an initial flow), this has the specific solution:\n",
    "\n",
    "$$\n",
    "  \\phi'(x) = \\frac{m\\dot{a}}{2\\hbar}\\left(2 y - e^{y^2}\\sqrt{\\pi}\\erf y\\right)\\\\\n",
    "  \\phi(x) = \\frac{ma\\dot{a}}{2\\hbar}y^2\\left(1 - {}_2F_2(1,1;\\tfrac{3}{2}, 2;y^2)\\right)\\\\\n",
    "  \\dot{\\phi}(x) = \\left(\\frac{\\dot{a}}{a} + \\frac{\\ddot{a}}{\\dot{a}}\\right)\\phi(x)\n",
    "  - \\frac{\\dot{a}}{a}x\\phi'(x)\n",
    "  = \\frac{m}{2\\hbar}\\left((\\dot{a}^2 + a\\ddot{a})y^2\\left(1 - {}_2F_2(1,1;\\tfrac{3}{2}, 2;y^2)\\right)\n",
    "  - \\dot{a}^2y\\left(2 y - e^{y^2}\\sqrt{\\pi}\\erf y\\right)\\right)\n",
    "$$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "These can be directly evaluated using `mpmath` (which is slow)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import sympy\n",
    "sympy.init_session()\n",
    "var('a, m, h')\n",
    "phi = var('phi', function=True)\n",
    "psi = exp(I*phi(x, t)-x**2/2/a(t)**2)\n",
    "expr = ((-h**2*diff(psi, x, x)/2/m - I*h*diff(psi, t))/psi).simplify()\n",
    "expr.collect(I)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%pylab inline --no-import-all\n",
    "import mpmath\n",
    "mpmath.hyp2f2(1,1,3/2,2,0), mpmath.erfi(0), mpmath.erfi(0)\n",
    "y = np.linspace(-0.1,0.1,100)\n",
    "def phi(y):\n",
    "    return y**2*(1-mpmath.hyp2f2(1,1,3/2,2,y**2))\n",
    "plt.plot(y, [float(phi(_y)) for _y in y])"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import bec;reload(bec)\n",
    "import mpmath\n",
    "\n",
    "class State(bec.State):\n",
    "    def a(self, d=0):\n",
    "        t = self.t\n",
    "        if d == 0:\n",
    "            return t**2/2.0\n",
    "        elif d == 1:\n",
    "            return t\n",
    "        elif d == 2:\n",
    "            return 1\n",
    "        else:\n",
    "            return 0.0\n",
    "        \n",
    "    def init(self):\n",
    "        y = self.x / self.a()\n",
    "        phi = y**2*(1-mpmath.hyp2f2(1, 1, 3./2,2, y**2))\n",
    "        return bec.State.init(self)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "2**12, 3**8, 5**6"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "good_ns = set()\n",
    "for m1 in range(13):\n",
    "    for m2 in range(9):\n",
    "        for m3 in range(7):\n",
    "            n = 2**m1*3**m2*5**m3\n",
    "            if n < 2**12+1:\n",
    "                good_ns.add(n)\n",
    "print(sorted(good_ns))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import sympy\n",
    "sympy.isprime(34)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%%time\n",
    "import numpy as np\n",
    "import time\n",
    "from numpy.fft import fft, ifft\n",
    "ts = []\n",
    "ns = np.arange(2, 2**12+1)\n",
    "ts0 = []\n",
    "ns0 = []\n",
    "ts1 = []\n",
    "ns1 = []\n",
    "ts_good = []\n",
    "ns_good = []\n",
    "for N in ns:\n",
    "    X = np.ones(N)*1j\n",
    "    t = np.inf\n",
    "    for m in range(3):\n",
    "        tic = time.time()\n",
    "        for n in range(10):\n",
    "            res = ifft(fft(X))\n",
    "        toc = time.time() - tic\n",
    "        t = min(t, toc)\n",
    "    if N in set(2**np.arange(1,13)):\n",
    "        ts0.append(t)\n",
    "        ns0.append(N)\n",
    "    if sympy.isprime(N):\n",
    "        ts1.append(t)\n",
    "        ns1.append(N)\n",
    "    if N in good_ns:\n",
    "        ts_good.append(t)\n",
    "        ns_good.append(N)\n",
    "    ts.append(t)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%pylab inline\n",
    "plt.plot(ns, ts/ns/np.log(ns), '+y')\n",
    "plt.plot(ns0, np.array(ts0)/ns0/np.log(ns0), '-g')\n",
    "plt.plot(ns_good, np.array(ts_good)/ns_good/np.log(ns_good), '-b')\n",
    "plt.plot(ns1, np.array(ts1)/ns1/np.log(ns1), '-r')\n",
    "\n",
    "plt.ylim(0,0.000001)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "plt.plot(ns, ts/ns/np.log(ns))\n",
    "plt.ylim(0,0.0000001)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "anaconda-cloud": {},
  "kernelspec": {
   "display_name": "Python 2 (Ubuntu Linux)",
   "language": "python",
   "name": "python2-ubuntu"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 2
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython2",
   "version": "2.7.12"
  },
  "toc": {
   "colors": {
    "hover_highlight": "#DAA520",
    "running_highlight": "#FF0000",
    "selected_highlight": "#FFD700"
   },
   "moveMenuLeft": true,
   "nav_menu": {
    "height": "66px",
    "width": "252px"
   },
   "navigate_menu": true,
   "number_sections": true,
   "sideBar": true,
   "threshold": 4,
   "toc_cell": false,
   "toc_section_display": "block",
   "toc_window_display": true
  }
 },
 "nbformat": 4,
 "nbformat_minor": 0
}
