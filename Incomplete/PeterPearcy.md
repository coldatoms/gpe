---
jupytext:
  formats: md:myst,ipynb
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.16.2
kernelspec:
  display_name: Python 3 (ipykernel)
  language: python
  name: python3
---

```{code-cell} ipython3
import numpy as np, matplotlib.pyplot as plt
import mmf_setup;mmf_setup.nbinit()
import logging;logging.getLogger('matplotlib').setLevel(logging.CRITICAL)
```

```{code-cell} ipython3
%matplotlib inline
import numpy as np, matplotlib.pyplot as plt
from importlib import reload
from gpe import utils, bec, tube, minimize
from pytimeode.evolvers import EvolverABM
reload(bec)
reload(tube)

plt.rcParams['figure.figsize'] = (4, 3)

u = bec.u


class Mixin:
    def __init__(self, experiment, **kw):
        self.experiment = experiment
        super().__init__(**kw)

    def get_ws(self, t=None):
        if t is None:
            t = self.t
        
        if self.initializing or t <= 0:
            return self.experiment.w0s
        else:
            return self.experiment.w1s
    
    def _get_Vext_(self):
        Vext = self.experiment.get_Vext(state=self, gpu=True)
        if (self.initializing or self.t < 0) and getattr(self, "mu", None):
            Vext -= self.mu
        return Vext

    
class State(Mixin, tube.StateGPEdrZ):
    """dr-GPE in y and z, but full x dynamics."""
    pass
        

class Experiment(utils.ExperimentBase):
    trapping_frequencies0_Hz = (3.66, 39.7, 30.1)
    trapping_frequencies1_Hz = (4.5, 4.5, 0.0)
    #trapping_frequencies1_Hz = (3.66, 39.7, 30.1)

    x_TF_micron = 144.0
    Rx_TF = 1.5  # Box is this much larger than x_TF
    species = (2, 0)

    dx_healing_length = 0.5
    
    m = u.m_Rb87
    hbar = u.hbar
    
    State = State
    
    def init(self):
        self.w0s = 2*np.pi * np.array(self.trapping_frequencies0_Hz) * u.Hz
        self.w1s = 2*np.pi * np.array(self.trapping_frequencies1_Hz) * u.Hz
        a = u.scattering_lengths[(self.species, self.species)]
        self.g = 4 * np.pi * self.hbar**2 * a / self.m
        self.x_TF = self.x_TF_micron * u.micron
        Lx = 2 * self.Rx_TF * self.x_TF
        V_TF = self.m * (self.w0s[0]*self.x_TF)**2 / 2

        mu = V_TF # Should correct if we have tight radial confinement
        self.healing_length = self.hbar / np.sqrt(2 * self.m * mu)
        dx = self.dx_healing_length * self.healing_length
        Nx = utils.get_good_N(Lx / dx)
        self.state_args = dict(experiment=self, Lxyz=(Lx,), 
                               mu=None, Nxyz=(Nx,), m=self.m, g=self.g, hbar=self.hbar)
        s = self.get_state()
        mu = s.get_mu_from_V_TF(V_TF=V_TF)
        self.state_args.update(mu=mu)
        super().init()
        
    def get_Vext(self, state, gpu=False):
        xyz = state._xyz_ if gpu else state.xyz
        np = state.xp
        ws = state.get_ws()
        Vext = self.m * sum((x*w)**2 for x, w in zip(xyz, ws)) / 2
        return Vext
        
    def get_state(self):
        return self.State(**self.state_args)
    
    def get_initialized_state(self, use_scipy=True):    
        s = minimize.MinimizeState(self.get_state(), fix_N=False).minimize(use_scipy=use_scipy)
        state = self.get_state()
        state.set_psi(s.get_psi())
        return state

e = Experiment()
s = e.get_state()
s.plot()
s = e.get_initialized_state()
s.plot()
s.x_TF
```

```{code-cell} ipython3
from IPython.display import clear_output, display
from mmfutils.contexts import FPS

dT = 1*u.ms
dt = 0.2*s.t_scale
steps = max(int(np.ceil(dT/dt)), 2)
print(steps)
dt = dT / steps
ev = EvolverABM(s, dt=dt)
for frame in FPS(1000, timeout=60*10):
    ev.evolve(steps)
    plt.clf()
    ev.y.plot(log=True)
    clear_output(wait=True)
    display(plt.gcf())
```

```{code-cell} ipython3
s = e.get_state()
ts = np.linspace(0, 120*u.ms)
s.initializing = False
lams = np.array([s.get_lambdas()[0] for s.t in ts])
plt.plot(ts / u.ms, lams)
```
