{
 "cells": [
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import mmf_setup;mmf_setup.nbinit()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Introduction"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In this notebook we demonstrate the exact soliton solutions from [Liu:2009].  These are a series of moving dark/bright solitons where the nature of the solution DD, DB, BD, or BB depends on the value of the coupling constants.  BB solitons are fairly straightforward to implement, but anything with a dark component needs some careful attention to matching the boundary conditions, so in general a twist is required.  If the solution has a phase $\\exp(\\I k x)$, then we must include a twist $\\theta$ such that $kL + \\theta = 2\\pi n$.\n",
    "\n",
    "[Liu:2009]: http://dx.doi.org/10.1103/PhysRevA.79.013423 (Xunxu Liu, Han Pu, Bo Xiong, W. M. Liu, and Jiangbin Gong, \"Formation and transformation of vector solitons in two-species Bose-Einstein condensates with a tunable interaction\", Phys. Rev. A 79, 013423 (2009) )"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Units"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We express everything in terms of the following coupled GPE equations:\n",
    "\n",
    "\\begin{align}\n",
    "  \\I \\hbar \\dot{\\psi}_1 &= \\left(\n",
    "    \\frac{-\\hbar^2\\nabla^2}{2m_1} + g_{11}n_1 + g_{12}n_2\\right)\\psi_1\\\\\n",
    "  \\I \\hbar \\dot{\\psi}_2 &= \\left(\n",
    "    \\frac{-\\hbar^2\\nabla^2}{2m_2} + g_{12}n_1 + g_{22}n_2\\right)\\psi_2.\n",
    "\\end{align}\n",
    "\n",
    "The solitons are expressed in terms of these physical parameters, and the parameters $\\eta = 1/\\sigma$ describing the width of the solitons, and the velocity $v$ of the soliton."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In the paper, they scale things as follows:\n",
    "\n",
    "\\begin{align}\n",
    "  \\I \\dot{\\psi}_1 &= \\left(\n",
    "    \\frac{-\\nabla^2}{2} + b_{11}n_1 + b_{12}n_2\\right)\\psi_1\\\\\n",
    "  \\I \\dot{\\psi}_2 &= \\left(\n",
    "    \\frac{-\\kappa\\nabla^2}{2} + b_{12}n_1 + b_{22}n_2\\right)\\psi_2.\n",
    "\\end{align}\n",
    "\n",
    "In other words, they choose units where $\\hbar = m_1 = 1$, $\\kappa = m_1/m_2$, and $g_{ab} = b_{ab}$.  The classification of the solutions is give in terms of:\n",
    "\n",
    "$$\n",
    "  C_1 = \\frac{b_{22} - \\kappa b_{12}}{b_{12}^2 - b_{11}b_{22}}, \\qquad\n",
    "  C_2 = \\frac{b_{12} - \\kappa b_{11}}{b_{12}^2 - b_{11}b_{22}}.\n",
    "$$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## BB"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "No issue of a twist here since the density must vanish at the boundaries:\n",
    "\n",
    "$$\n",
    "  \\psi_{1B} = \\eta\\sqrt{C_1}\\sech[\\eta(x-vt)]e^{\\I[vx + (\\eta^2-v^2)t/2]}, \\qquad\n",
    "  \\psi_{2B} = \\eta\\sqrt{-C_2}\\sech[\\eta(x-vt)]e^{\\I[vx/\\kappa + (\\kappa\\eta^2-v^2/\\kappa)t/2]}  \n",
    "$$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Note that in the limit $C_2=0$ this gives the solution for a single component GPE:\n",
    "\n",
    "$$\n",
    "  \\psi_B = \\eta\\sqrt{C_1}\\sech[\\eta(x-vt)]e^{\\I[vx + (\\eta^2-v^2)t/2]}\n",
    "$$"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%pylab inline --no-import-all\n",
    "import exact_solutions;reload(exact_solutions)\n",
    "s = exact_solutions.BrightSoliton(Nx=2**4*3**3, Lx=58.0, v=0.0)\n",
    "s.plot(log=True)\n",
    "plt.twiny()\n",
    "psi_k = abs(np.fft.fft(s[...]))\n",
    "plt.plot(np.fft.fftshift(s.kxyz[0]),\n",
    "         np.fft.fftshift(np.log10(psi_k/psi_k.max())), ':')\n",
    "\n",
    "#assert np.allclose(0, s.get_Hy(subtract_mu=True)[...])\n",
    "\n",
    "s.get_Hy(subtract_mu=True).plot(log=True)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%pylab inline --no-import-all\n",
    "from mmfutils.contexts import NoInterrupt\n",
    "from pytimeode.evolvers import EvolverSplit, EvolverABM\n",
    "from IPython.display import display, clear_output\n",
    "import exact_solutions;reload(exact_solutions)\n",
    "s = exact_solutions.BrightSoliton(v=5.0, cooling_phase=1.0)\n",
    "\n",
    "E_max = np.abs(s.kxyz).max()**2/2.0/s.m\n",
    "\n",
    "#e = EvolverSplit(s, dt=0.01*u.hbar/E_max, normalize=True)\n",
    "e = EvolverABM(s, dt=0.1/E_max, normalize=True)\n",
    "\n",
    "with NoInterrupt(ignore=True) as interrupted:\n",
    "    while e.y.t < 4 and not interrupted:\n",
    "        e.evolve(100)\n",
    "        plt.clf()\n",
    "        e.y.plot()\n",
    "        plt.twinx()\n",
    "        plt.semilogy(e.y.x, abs(abs(e.y[...]) - abs(e.y.psi0)), ':y')\n",
    "        display(plt.gcf())\n",
    "        clear_output(wait=True)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%pylab inline --no-import-all\n",
    "import numpy as np\n",
    "from IPython.display import display, clear_output\n",
    "import bec2; reload(bec2)\n",
    "from bec2 import u\n",
    "\n",
    "class State(bec2.State):\n",
    "    def __init__(self, kappa=1.0, bs=[-1.0, -2.0, -3.0], lambdas=(0.0, 0.0), \n",
    "                 **kw):\n",
    "        self.kappa = kappa\n",
    "        self.bs = bs\n",
    "        self.lambdas = lambdas\n",
    "\n",
    "        b11, b22, b12 = bs\n",
    "        self.Cs = np.divide((b22 - kappa*b12,b12 - kappa*b11),\n",
    "                            b12**2 - b12*b22)\n",
    "        bec2.State.__init__(self, ms=(1.0, 1./self.kappa), \n",
    "                            ws=self.lambdas, gs=bs, **kw)\n",
    "\n",
    "\n",
    "    def get_initial_state(self, x0=0.0, eta=1.0, v=1.0, t=0.0, \n",
    "                          mu=1.0):        \n",
    "        x = self.xyz[0]\n",
    "        L = self.Lxyz[0]\n",
    "        kappa = self.kappa\n",
    "        arg = eta*(x-v*t)\n",
    "        b11, b22, b12 = self.bs\n",
    "        C1, C2 = self.Cs        \n",
    "        C = np.sqrt(abs(self.Cs))[self.bcast]\n",
    "        psi_B = eta/np.cosh(arg)\n",
    "        f1 = -eta**2-v**2*(-b11*C1 + b12*C2/mu**2) + 0*x\n",
    "        f2 = -mu*eta**2-v**2*(b12*C1+b22*C2/mu**2) + 0*x\n",
    "        twist = [0, 0]\n",
    "        if C1 < 0:\n",
    "            if C2 < 0:\n",
    "                print(\"DB\")\n",
    "                phi = np.array([f2*t + 0*x, \n",
    "                                v*x + f1*t])\n",
    "                psi = C*[1j*v       + eta*np.tanh(arg), \n",
    "                         psi_B]\n",
    "                twist = [np.pi, v*L - np.pi]\n",
    "            else:\n",
    "                print(\"DD\")\n",
    "                phi = np.array([f1*t, f2*t])\n",
    "                psi = C*[1j*v       + eta*np.tanh(arg),\n",
    "                         1j*v/kappa + eta*np.tanh(arg)]\n",
    "        else:\n",
    "            if C2 < 0:\n",
    "                print(\"BB\")\n",
    "                phi = np.array(\n",
    "                    [v*x       + (      eta**2-v**2      )*t/2.0,\n",
    "                     v*x/kappa + (kappa*eta**2-v**2/kappa)*t/2.0])\n",
    "                psi = C*[psi_B,\n",
    "                         psi_B]\n",
    "            else:\n",
    "                print(\"BD\")\n",
    "                phi = np.array([v*x + f1*t, \n",
    "                                f2*t])\n",
    "                psi = C*[psi_B, \n",
    "                         1j*v/kappa + eta*np.tanh(arg)]\n",
    "                theta = np.pi\n",
    "                twist = [0, theta]\n",
    "\n",
    "        self.twist = np.asarray(twist)\n",
    "        return psi*np.exp(1j*phi)\n",
    "        \n",
    "s = State(Nxyz=(128,), Lxyz=(40*u.micron,), N=1e5)\n",
    "s[...] = s.get_initial_state()\n",
    "s.plot()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from mmfutils.contexts import NoInterrupt\n",
    "from pytimeode.evolvers import EvolverSplit, EvolverABM\n",
    "from IPython.display import display, clear_output\n",
    "\n",
    "s = State(Nxyz=(128*2,), Lxyz=(2*80*u.micron,), \n",
    "          #bs=[-1.0, -2.0, -3.0], # BB\n",
    "          #bs=[1.0, 2.0, 3.0], # DD\n",
    "          bs=[-1.0, 2.0, 1.0], # DB\n",
    "          #bs=[-1.0, 2.0, -1.0], # BD\n",
    "          N=1e5)\n",
    "s[...] = s.get_initial_state(v=0, eta=0.2)\n",
    "s.init()\n",
    "s.cooling_phase = 1.0\n",
    "E_max = s.E_max\n",
    "s.plot()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "x = s.xyz[0]\n",
    "dy = s.empty()\n",
    "dy = s.compute_dy_dt(dy)\n",
    "plt.plot(x, abs(dy[0]))\n",
    "plt.plot(x, abs(dy[1]))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "x = s.xyz[0]\n",
    "k = s.kxyz[0][0]\n",
    "y = s[...]\n",
    "yt = np.array([_y/_tp for _tp, _y in zip(s.twist_phase, y)])\n",
    "plt.plot(x, s.ifft(s.K*s.fft(s[...]))[1])\n",
    "#plt.plot(s.kxyz[0][0], abs(np.fft.fft(yt[0])), '+')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#e = EvolverSplit(s, dt=0.01*u.hbar/E_max, normalize=True)\n",
    "e = EvolverABM(s, dt=0.1*u.hbar/E_max, normalize=True)\n",
    "\n",
    "with NoInterrupt(ignore=True) as interrupted:\n",
    "    while e.y.t < 4*u.ms and not interrupted:\n",
    "        e.evolve(2)\n",
    "        plt.clf()\n",
    "        e.y.plot()\n",
    "        display(plt.gcf())\n",
    "        clear_output(wait=True)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Testing"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "To test the code, we will set $g=0$ and use the exact solution for the Harmonic Oscillator:\n",
    "\n",
    "$$\n",
    "  \\psi(x) \\propto e^{-(x/a)^2/2}, \\qquad\n",
    "  a^2 = \\frac{\\hbar}{m\\omega}\n",
    "$$"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def get_err(N=128, L=24*u.micron):\n",
    "    s = State(Nxyz=(N,), Lxyz=(L,), N=1e5)\n",
    "    s.gs = 0, 0, 0\n",
    "    a = np.sqrt(u.hbar/u.m/s.ws[0])\n",
    "    x = s.xyz[0]\n",
    "    psi_0 = np.exp(-(x/a)**2/2.0)\n",
    "    s[...] = psi_0[None, :]\n",
    "    s.normalize()\n",
    "    dy = s.empty()\n",
    "    s.compute_dy_dt(dy=dy, subtract_mu=True)\n",
    "    return abs(dy[...]).max()\n",
    "\n",
    "Ns = 2**np.arange(2,8)\n",
    "errs = map(get_err, Ns)\n",
    "plt.semilogy(Ns, errs, '-+')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Why are $L=23$microns and $N=2^6$ optimal?"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "s = State(Nxyz=(46,), Lxyz=(23*u.micron,))\n",
    "a = np.sqrt(u.hbar/u.m/s.ws[0])\n",
    "L, N = s.Lxyz[0], s.Nxyz[0]\n",
    "k_max = np.pi*(N-2)/L   # For Khalid...\n",
    "print k_max, s.kxyz[0].max()\n",
    "print np.exp(-(L/2/a)**2/2)   # Wavefunction drops by factor of macheps"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "psi_0 = s.xyz[0]*np.exp(-(s.xyz[0]/a)**2/2)\n",
    "plt.semilogy(np.fft.fftshift(s.kxyz[0]), \n",
    "         np.fft.fftshift(abs(np.fft.fft(psi_0))), '-+')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "So we see that for the ground state $k$ needs to go up to $6$."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Exact Solution with Interactions"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Consider now a state with $\\psi_a = e^{-(x/a)^2/2}$ and $\\psi_b = e^{-(x/b)^2/2}$.\n",
    "\n",
    "$$\n",
    "  \\psi_a''(x) \n",
    "$$"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%pylab inline --no-import-all\n",
    "from IPython.display import display, clear_output\n",
    "import bec2; reload(bec2)\n",
    "from bec2 import State, u\n",
    "\n",
    "s = State(Nxyz=(64,), Lxyz=(23*u.micron,), N=1e5)\n",
    "a = np.sqrt(u.hbar/u.m/s.ws[0])\n",
    "x = s.xyz[0]\n",
    "psi_0 = np.exp(-(x/a)**2/2)\n",
    "\n",
    "class State1(State):\n",
    "    def __init__(self, *v, **kw):\n",
    "        State.__init__(self, *v, **kw)\n",
    "        a = np.sqrt(u.hbar/u.m/self.ws[0])\n",
    "        x = self.xyz[0]\n",
    "        k = 1./2./a**2\n",
    "        psi_0 = 4.0*np.exp(-(x/a)**2/2)\n",
    "        n_0 = abs(psi_0)**2\n",
    "        g_aa, g_bb, g_ab = self.gs\n",
    "        self._V_ext = (u.hbar**2/2.0/u.m*(4*(k*x)**2 - 2*k) - g_aa*n_0,\n",
    "                       u.hbar**2/2.0/u.m*(4*(k*x)**2 - 2*k) - g_bb*n_0,\n",
    "                       0*n_0\n",
    "                      )\n",
    "        self.data[...] = psi_0\n",
    "        self.get_Vext = lambda: self._V_ext\n",
    "        self.pre_evolve_hook()\n",
    "        \n",
    "s = State1(Nxyz=(64,), Lxyz=(23*u.micron,))\n",
    "s.plot()\n",
    "plt.plot(x, s.get_Vext()[0])\n",
    "dy = s.empty()\n",
    "s.compute_dy_dt(dy=dy, subtract_mu=False)\n",
    "abs(dy[...]).max()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from mmfutils.contexts import NoInterrupt\n",
    "from pytimeode.evolvers import EvolverSplit, EvolverABM\n",
    "from IPython.display import display, clear_output\n",
    "\n",
    "s = State1(Nxyz=(64*4,), Lxyz=(23*u.micron,))\n",
    "assert np.allclose(s._N, s.get_N())\n",
    "\n",
    "s[...] = 1.0\n",
    "s.normalize()\n",
    "s.cooling_phase = 1j\n",
    "\n",
    "E_max = u.hbar**2*np.abs(s.kxyz).max()**2/2.0/u.m\n",
    "\n",
    "#e = EvolverSplit(s, dt=0.01*u.hbar/E_max, normalize=True)\n",
    "e = EvolverABM(s, dt=0.1*u.hbar/E_max, normalize=True)\n",
    "\n",
    "with NoInterrupt(ignore=True) as interrupted:\n",
    "    while e.y.t < 4*u.ms and not interrupted:\n",
    "        e.evolve(100)\n",
    "        plt.clf()\n",
    "        e.y.plot()\n",
    "        display(plt.gcf())\n",
    "        clear_output(wait=True)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from mmfutils.contexts import NoInterrupt\n",
    "from pytimeode.evolvers import EvolverSplit, EvolverABM\n",
    "from IPython.display import display, clear_output\n",
    "\n",
    "s = State1(Nxyz=(64*4,), Lxyz=(23*u.micron,))\n",
    "s *= np.sign(s.xyz[0] - 0.5)\n",
    "s.cooling_phase = 1 + 0.01j\n",
    "\n",
    "E_max = u.hbar**2*np.abs(s.kxyz).max()**2/2.0/u.m\n",
    "#e = EvolverSplit(s, dt=0.01*u.hbar/E_max, normalize=True)\n",
    "e = EvolverABM(s, dt=0.5*u.hbar/E_max, normalize=True)\n",
    "\n",
    "with NoInterrupt(ignore=True) as interrupted:\n",
    "    while e.y.t < 40*u.ms and not interrupted:\n",
    "        e.evolve(100)\n",
    "        plt.clf()\n",
    "        e.y.plot()\n",
    "        display(plt.gcf())\n",
    "        clear_output(wait=True)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# 2D Scissor Modes (incomplete)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We base the physical parameters on a system like [Marago:2001] so we can explore damping of the normal modes of the system.  They have trapping frequencies of $\\omega_y=\\omega_z = 128$Hz and $\\omega_x = \\sqrt{8}\\omega_y$ and $N = 2\\times 10^4$ particles.  In the TF approximation at $T=0$ this corresponds to $\\mu = $\n",
    "\n",
    "[Marago:2001]: http://dx.doi.org/10.1103/PhysRevLett.86.3938 (Onofrio Marag\\`o, Gerald Hechenblaikner, Eleanor Hodby, and Christopher Foot, \"Temperature Dependence of Damping and Frequency Shifts of the Scissors Mode of a Trapped Bose-Einstein Condensate\", Phys. Rev. Lett. 86, 3938--3941 (2001) )"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%pylab inline --no-import-all\n",
    "from IPython.display import display, clear_output"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import bec;reload(bec)\n",
    "from pytimeode.evolvers import EvolverABM\n",
    "from mmfutils.contexts import NoInterrupt\n",
    "from bec import State, u\n",
    "\n",
    "s = State()\n",
    "s.cooling_phase = 1j\n",
    "s.t = -100*u.ms\n",
    "e = EvolverABM(s, dt=0.001)\n",
    "with NoInterrupt(ignore=True) as interrupted:\n",
    "    while not interrupted:\n",
    "        e.evolve(500)\n",
    "        plt.clf()\n",
    "        e.y.plot()\n",
    "        display(plt.gcf())\n",
    "        clear_output(wait=True)\n",
    "        "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "s = e.get_y()\n",
    "n = s.get_density()\n",
    "x, y, z = s.xyz\n",
    "plt.plot(x.ravel(), n.sum(axis=-1).sum(axis=-1))\n",
    "s.mu, s._mu"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Bright Solitons"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "There is nice integrable 1D model that is useful for testing code in the case of attractive interactions (the so-called focusing NLSEQ):\n",
    "\n",
    "$$\n",
    "  \\I\\hbar \\partial_t \\Psi = - \\frac{\\hbar^2\\nabla^2}{2m}\\Psi \n",
    "  + \\begin{pmatrix}\n",
    "    -gn_{+} & \\Omega(t)\\\\\n",
    "    \\Omega(t) & -gn_{+}\n",
    "  \\end{pmatrix}\\cdot\\Psi\n",
    "$$\n",
    "\n",
    "where $n_{+} = n_a + n_b$.  This corresponds to a coupled set of GPEs with equal attractive interactions $g_{aa} = g_{bb} = g_{ab} = -g$.\n",
    "\n",
    "\n",
    "$$\n",
    "  \\DeclareMathOperator{\\sech}{sech}\n",
    "  \\Psi(x, t) = \\frac{1}{2}\\begin{pmatrix}\n",
    "      \\cos\\theta e^{-\\I\\Gamma(t)} + \\sin\\theta e^{\\I\\Gamma(t)}\\\\\n",
    "      \\cos\\theta e^{-\\I\\Gamma(t)} - \\sin\\theta e^{\\I\\Gamma(t)}      \n",
    "  \\end{pmatrix}e^{\\I g b^2 t/4} b\\sech\\frac{b \\sqrt{gm} x}{\\hbar}, \\qquad\n",
    "  -\\frac{\\hbar^2 \\psi''(x)}{2m} - g\\abs{\\psi}^2\\psi = -\\frac{b^2 g}{2}\\psi, \\\\\n",
    "  \\Gamma(t) = \\int_{0}^{t}\\Omega(t')\\d{t'}\n",
    "$$\n",
    "\n",
    "$$\n",
    "  \\Psi(x, t) = \\left(\\frac{\\cos\\theta e^{-\\I\\Gamma(t)}}{2}\n",
    "  \\begin{pmatrix}1\\\\1\\end{pmatrix}\n",
    "  +\n",
    "  \\frac{\\sin\\theta e^{\\I\\Gamma(t)}}{2}\n",
    "  \\begin{pmatrix}1\\\\ -1\\end{pmatrix} \\right)\n",
    "  e^{\\I g b^2 t/4} b\\sech(b x\\sqrt{gm/\\hbar^2}), \\qquad\n",
    "  -\\frac{\\hbar^2 \\psi''(x)}{2m} - g\\abs{\\psi}^2\\psi = -\\frac{b^2 g}{2}\\psi, \\\\\n",
    "  \\Gamma(t) = \\int_{0}^{t}\\Omega(t')\\d{t'}\n",
    "$$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We have the following dimensions:\n",
    "\n",
    "$$\n",
    "  [\\hbar] = \\frac{M D^2}{T}, \\qquad\n",
    "  [2m] = M, \\qquad\n",
    "  [gn] = [E] = \\frac{M D^2}{T^2}, \\qquad\n",
    "  [g] = [VE] = \\frac{M D^5}{T^2}, \\qquad\n",
    "  [\\Omega] = [E] = \\frac{M D^2}{T^2}\\\\\n",
    "  \\left[\\frac{2mg}{\\hbar^2}\\right] = D, \\qquad\n",
    "  \\left[\\frac{2m\\Omega}{\\hbar^2}\\right] = \\frac{1}{D^2}, \\qquad\n",
    " \\left[\\frac{t\\hbar}{2m}\\right] = D^2.\n",
    "$$\n",
    "\n",
    "In the paper, units are scaled so that $2m = \\hbar = 1$.  This means that $g$ provides the length scale for the problem."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from numpy import cos, sin, exp\n",
    "theta, Gamma = np.random.random(2)\n",
    "(cos(theta)*exp(-1j*Gamma) + sin(theta)*np.exp(1j*Gamma))/2, (cos(theta+Gamma) - 1j*sin(theta-Gamma))/4"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%pylab inline --no-import-all\n",
    "from IPython.display import display, clear_output\n",
    "from pytimeode.evolvers import EvolverABM\n",
    "from mmfutils.contexts import NoInterrupt\n",
    "import bec2; reload(bec2)\n",
    "from bec2 import State2, u\n",
    "\n",
    "class StateBS(State2):\n",
    "    t0 = u.micron**2*2*u.m/u.hbar\n",
    "    def get_Vext(self):\n",
    "        Va = Vb = 0*self.xyz[0]\n",
    "        return (Va, Vb, self.Omega)\n",
    "\n",
    "# Paper dimensions\n",
    "g_ = 2.0\n",
    "Omega_ = 2.0\n",
    "theta_ = np.pi/4.0\n",
    "b_ = 2.0\n",
    "s = StateBS(Nxyz=(128,), Lxyz=(4*u.micron,), N=1e5)\n",
    "s = StateBS(Nxyz=(64*4,), Lxyz=(16*u.micron,), N=1e5, \n",
    "            gs=(-g_*u.hbar**2/2.0/u.m,)*3, \n",
    "            Omega=Omega_*u.hbar**2/2.0/u.m, \n",
    "           cooling_phase=1.0j)\n",
    "\n",
    "# Fig 9\n",
    "g = -g_*u.hbar**2/2.0/u.m\n",
    "s = StateBS(Nxyz=(64,), Lxyz=(16*u.micron,), N=1e5, \n",
    "            gs=(g*1.01,g*1.01, g*0.99), \n",
    "            Omega=Omega_*u.hbar**2/2.0/u.m, \n",
    "            cooling_phase=1.0)\n",
    "x = s.xyz[0]\n",
    "s[0, ...] = (np.cos(theta_) + np.sin(theta_))/2.0*b_/np.cosh(b_*np.sqrt(g_)*x/2.0)\n",
    "s[1, ...] = (np.cos(theta_) - np.sin(theta_))/2.0*b_/np.cosh(b_*np.sqrt(g_)*x/2.0)\n",
    "#s[...] += np.random.random(s.shape)*0.1\n",
    "s.plot()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "e = EvolverABM(s, dt=0.5/s.E_max/u.hbar)\n",
    "with NoInterrupt(ignore=True) as interrupted:\n",
    "    while not interrupted:\n",
    "        e.evolve(10)\n",
    "        plt.clf()\n",
    "        e.y.plot()\n",
    "        display(plt.gcf())\n",
    "        clear_output(wait=True)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import sympy\n",
    "sympy.init_session()\n",
    "var('a, theta, b, g, x')\n",
    "psi = a/cosh(x*b*sqrt(g)/2)\n",
    "n = psi**2\n",
    "r1, r2 = (-psi.diff(x,x)/2).trigsimp().simplify(), (g*n*psi).simplify()\n",
    "r1, r2"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "sympy.expand_trig"
   ]
  }
 ],
 "metadata": {
  "anaconda-cloud": {},
  "kernelspec": {
   "display_name": "Python 2 (Ubuntu, plain)",
   "language": "python",
   "name": "python2-ubuntu"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 2
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython2",
   "version": "2.7.13"
  },
  "nav_menu": {},
  "toc": {
   "colors": {
    "hover_highlight": "#DAA520",
    "navigate_num": "#000000",
    "navigate_text": "#333333",
    "running_highlight": "#FF0000",
    "selected_highlight": "#FFD700",
    "sidebar_border": "#EEEEEE",
    "wrapper_background": "#FFFFFF"
   },
   "moveMenuLeft": true,
   "nav_menu": {
    "height": "12px",
    "width": "252px"
   },
   "navigate_menu": true,
   "number_sections": true,
   "sideBar": true,
   "threshold": 4,
   "toc_cell": false,
   "toc_section_display": "block",
   "toc_window_display": true,
   "widenNotebook": false
  }
 },
 "nbformat": 4,
 "nbformat_minor": 1
}
