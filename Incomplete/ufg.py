"""Code for simulating a single component BEC.

This is the simplest code in the project, implementing all of the basic
features without excess complications.  It is somewhat excessively commented to
provide a pedagogical introduction for users unfamiliar with the techniques and
using python for scientific compuing.
"""

# The start of every python files should contain a multi-line string like that
# above which describes the purpose of the file.  This is called a docstring.
# The first line is the summary, followed by more details.  You can see this
# documentation from the IPython prompt by adding a question mark after the
# module: `bec?` for example

from __future__ import absolute_import, division, print_function, unicode_literals

# This should be the first code line in the module.  The __future__ module
# provides some features that will be a standard part of python 3, to ease
# adapting our code later on.  Importing division is important as it prevents
# one from mistakes where `1/2 = 0` because the old default was integer
# division.

# Now should follow all the imports.  Use the following order:
# 1. Standard libraries (sorted)
# 2. Other libraries like numpy, scipy, etc.
# 3. Our libraries
# 4. Local packages for this project.
import math

import numpy as np

# This is my custom utility library.  Here we use wrappers to the
# high-performance FFTW libraries.
from mmfutils.performance.fft import fftn, ifftn

# The basis for time evolution in our systems is my pytimeode package.  This
# provides a core interface which we must code to in order to use the
# functionality of evolution code.
from pytimeode import interfaces, mixins

import bec

# The special variable __all__ is used in python to list which functions,
# classes, etc. should be used by users.  It is not strictly needed, but tells
# people what portions of the module they should look at
__all__ = ["Units", "u", "State"]


class Units:
    """Units and physical constants.

    This class is simply a container for physical constants and units.  We have
    chosen values here that are relevant for 87Rb which is studied at WSU in
    Peter Engels' lab.
    """

    hbar = 1.0
    micron = 1.0
    mm = 1e3 * micron
    cm = 1e4 * micron
    nm = 1e-3 * micron
    meter = 1e3 * mm

    u = 1.0  # AMU
    kg = u / 1.660539040e-27
    G = 1.0  # Gauss

    m = 86.909187 * u
    a_B = 5.2917721054980885238e-5 * micron  # Bohr radius
    a = 100.40 * a_B  # |F=1, m=-1>

    # hbar/micron^2/u = 63507.799258914903398 Hz
    Hz = hbar / micron**2 / u / 63507.799258914903398
    kHz = 1e3 * Hz
    s = 1.0 / Hz
    ms = 1e-3 * s
    xi = 0.37  # Bertsch Parameter


# Here is a convenience instance for use in the code.
u = units = Units()


@interfaces.implementer(
    [
        interfaces.IStateForABMEvolvers,
        interfaces.IStateForSplitEvolvers,
        interfaces.IStateWithNormalize,
    ]
)
class State(bec.State):
    """State class implementing the GPE for a symmetric unitary Fermi gas."""

    def __init__(
        self,
        Nxyz=(2**5, 2**5, 2**5),
        Lxyz=(30 * u.micron, 50 * u.micron, 50 * u.micron),
        ws=np.array([np.sqrt(8) * 126.0, 126.0, 126.0]) * u.Hz,
        g=4 * np.pi * u.hbar**2 * u.a / u.m,
        m=u.m,
        N=2e4,
        cooling_phase=1.0,
        symmetric_grid=False,
    ):
        self.m = m
        self.Nxyz = np.asarray(Nxyz)
        self.Lxyz = np.asarray(Lxyz)
        self.ws = ws
        self.N = N
        self.g = g
        self.cooling_phase = cooling_phase
        self.symmetric_grid = symmetric_grid

        # Here is the data for the state.  The ArrayStateMixin class uses this
        # to provide most of the required functionality required by the IState
        # interface.  Use np.zeros here so that we don't get overflow errors.
        # (Using np.empty is slightly faster, but the call to get_N() may then
        # raise an overflow error.)
        self.data = np.zeros(self.Nxyz, dtype=complex)

        # We defer all other calculations to the init() method so that they
        # will use the current values of the various parameters.  This allows
        # the user to change the values of the parameters and they will take
        # effect the next time init() is called.
        self.init()

        # Once the state is initialized, we can set the initial state.
        self.set_initial_state()

    def init(self):
        """Initialize the state.

        This method defines the basis positions, momenta, etc. for use later
        on.  We define these here rather than in the constructor `__init__()`
        so that the user can change them later and the reinitialize the state.
        We also call this function from the `pre_evolve_hook()` so that it is
        called before any evolution takes place.  For this reason, we should
        not modify the state here.
        """
        # Check that the user did not change the shape of the data, otherwise
        # the state will be invalid.  As this is a potential user error, we
        # raise an exception here with a helpful message
        if not np.allclose(self.Nxyz, self.data.shape):
            raise ValueError(
                "The dimension of the state changed from Nxyz={} to {}.".format(
                    self.data.shape, self.Nxyz
                )
            )

        # The metric is used wen computing integrals etc.
        self.metric = np.prod(np.divide(self.Lxyz, self.Nxyz))

        # Define the center of grid to respect the symmetric_grid flag.
        xyz0 = self.Lxyz / 2.0
        if self.symmetric_grid:
            dxyz = self.Lxyz / self.Nxyz
            xyz0 -= dxyz / 2.0

        # Here we use the meshgrid function to construct the lattice points.
        # This is a little complicated, but allows the code to work in D
        # dimensions.  The indexing and sparse options differ from the MATLAB
        # inspired defaults which focus on plotting.  Here is the equivalent
        # explicit 2D code for comparison:
        #
        #     Nx, Ny = self.Nxyz
        #     Lx, Ly = self.Lxyz
        #     x0, y0 = xyz0
        #     x = np.arange(Nx)*Lx/Nx - x0
        #     y = np.arange(Ny)*Ly/Ny - y0
        #     self.xyz = [x[:, None], y[None, :]]
        self.xyz = np.meshgrid(
            *[
                (np.arange(_N)) * _L / _N - _x0
                for _N, _L, _x0 in zip(self.Nxyz, self.Lxyz, xyz0)
            ],
            sparse=True,
            indexing="ij",
        )

        if self.symmetric_grid:
            for _x in self.xyz:
                # It is good practice to put assert statements in your code to
                # make sure that everything is working as expected.  If they
                # become a performance issue, you can disable them after you
                # test your code by running python in -O mode.
                assert np.allclose(_x.ravel(), -_x.ravel()[::-1])

        # Here we generate the momenta.  The equivalent explicit 2D code is:
        #
        #     kx = 2*np.pi * np.fft.ffrfreq(Nx, Lx/Nx)
        #     ky = 2*np.pi * np.fft.ffrfreq(Ny, Ly/Ny)
        #     self.kxyz = [kx[:, None], ky[None, :]]
        self.kxyz = np.meshgrid(
            *[
                2 * np.pi * np.fft.fftfreq(_N, _L / _N)
                for _N, _L in zip(self.Nxyz, self.Lxyz)
            ],
            sparse=True,
            indexing="ij",
        )

        # Here we compute the kinetic energy matrix for use with the FFT.
        self.K = sum((u.hbar * _k) ** 2 / 2.0 / self.m for _k in self.kxyz)

        # Here we precompute the "phase" factor appearing in the GPE relating
        # H(psi) with dpsi/dt.  We include the value of hbar here and the
        # cooling_phase.  A potential optimization here is to allow the state
        # to be real if the cooling phase is purely imaginary
        self._phase = 1.0 / 1j / u.hbar / self.cooling_phase

        # We also record the current particle number so that normalize() can
        # restore it if requested during evolution.
        self._N = self.get_N()

    def pre_evolve_hook(self):
        """This method is called by the evolvers at the start of evolution to
        ensure that the state is properly initialized.
        """
        self.init()  # We just defer everything to init()

    def set_initial_state(self):
        """Set the state using the Thomas Fermi (TF) approximation."""
        # This value of mu is valid in the TF approximation for a harmonic
        # trap.  It allows us to convert the total particle number N specified
        # in the 3D experiment into a reasonable initial state.
        if self.g != 0:
            w3 = (np.prod(self.ws) ** (1.0 / len(self.ws))) ** 3
            self.mu = (
                (15.0 * self.g * self.N * w3 / (16 * np.pi)) ** 2 * self.m**3 / 2
            ) ** (1.0 / 5.0)

            V_ext = self.get_Vext()
            n = np.maximum(0, (self.mu - V_ext)) / self.g
        else:
            n = self.N / np.prod(self.Lxyz)

        self.data[...] = np.sqrt(n)
        self._N = self.get_N()

    # The use of the property decorator here makes `dim` an attribute rather
    # than a method.  This means you treat it as if it were a variable
    # `self.dim` instead of as a function `self.dim()` but it computes the
    # result so it is always up to date.
    @property
    def dim(self):
        """Dimension of the state."""
        return len(self.Nxyz)

    @property
    def shape(self):
        """Shape of the state Nxyz."""
        return tuple(self.Nxyz)

    def get_Vext(self):
        """Return the external potential.

        Overload this method if you want to change the external potential.  If
        the potential should be time dependent, use `self.t` which will be
        updated by the evolvers.
        """
        return 0.5 * self.m * sum((_w * _x) ** 2 for _w, _x in zip(self.ws, self.xyz))

    def get_density(self):
        """Return the density of the state."""
        return abs(self[...]) ** 2

    def get_N(self):
        """Return the total D-dimensional particle number in the state."""
        n = self.get_density()
        return self.integrate(n)

    def integrate(self, a):
        """Return the integral of `a` over the box."""
        return self.metric * np.sum(a)

    def braket(self, a, b):
        """Return the inner product of the functions `a` and `b`.

        Note: `a` is conjugated and the metric is applied.
        """
        return self.metric * a[...].ravel().conj().dot(b[...].ravel())

    def get_V(self):
        """Return the complete potential `V` - internal and external.

        This function defines the non-linear interaction of the equations.  For
        the GPE, the energy-density has $gn^2/2$, so we have the derivative
        `gn` here.
        """
        n = self.get_density()
        V_ext = self.get_Vext()
        V_int = self.g * n
        return V_ext + V_int

    ######################################################################
    # Required by interface IStateForABMEvolvers
    def compute_dy_dt(self, dy, subtract_mu=True):
        """Return `dy_dt` storing the results in `dy`.

        Arguments
        ---------
        subtract_mu : bool
           If `True`, then subtract the chemical potential such that `dy_dt` is
           orthogonal to the original state `y`.  This will minimize the
           evolution of the overall phase during real-time evolution (which can
           reduce numerical errors) and will ensure that evolution under
           imaginary or complex time will preserve particle number.

           This should not be set if computing physical energy of the state,
           however, which is why it is a parameter.
        """
        y = self[...]
        Ky = ifftn(self.K * fftn(y))
        Vy = self.get_V() * y
        Hy = Ky + Vy
        if subtract_mu:
            mu = self.braket(y, Hy) / self.braket(y, y)
            assert np.allclose(0, mu.imag)
            Hy[...] -= mu * y
            self._mu = mu

        dy[...] = Hy * self._phase
        return dy

    ######################################################################
    # Required by interface IStateForSplitEvolvers
    linear = False

    def apply_exp_K(self, dt):
        r"""Apply $e^{-i K dt}$ in place."""
        y = self[...]
        self[...] = ifftn(np.exp(self.K * self._phase * dt) * fftn(y))

    def apply_exp_V(self, dt, state):
        r"""Apply $e^{-i V dt}$ in place using `state` for any
        nonlinear dependence in V. (Linear problems should ignore
        `state`.)"""
        self *= np.exp(self.get_V() * self._phase * dt)

    ######################################################################
    # Required by interface IStateWithNormalize
    def normalize(self, s=None):
        """Normalize the state and return the scale factors `s`."""
        if s is None:
            s = np.sqrt(self._N / self.get_N())
        self *= s
        assert np.allclose(self._N, self.get_N())
        return s

    # End of interface definitions
    ######################################################################

    ######################################################################
    # The equations of motion follow from minimizing the energy of the state.
    # Here we compute the energy so that we can test the equations of motion
    # and use a minimizer to find the ground state.  These methods are not
    # needed for evolution.
    def get_energy(self):
        """Return the energy of the state.  Useful for minimization."""
        E = self.integrate(self.get_energy_density())
        assert np.allclose(0, E.imag)
        return E.real

    def get_energy_density(self):
        """Return the energy density."""
        y = self[...]
        n = self.get_density()
        K = y.conj() * ifftn(self.K * fftn(y))
        Vint = self.g * n**2 / 2.0
        Vext = self.get_Vext() * n
        return K + Vint + Vext

    ######################################################################
    # The remaining methods are not needed for evolution or ground state
    # preparation, but may be helpful for analysis.
    def get_H(self, subtract_mu=False):
        """Return the single-particle Hamiltonian for convenience only.

        .. note:: May be huge!

           This is a square matrix whose sides are of dimension `prod(Nxyz)`
           which can be huge.  It is not needed for general computations, but
           might be useful for analysis such as exploring the BdG in 1D
           systems.
        """
        N = np.prod(self.Nxyz)
        Q = fftn(np.eye(N).reshape(self.shape * 2), axes=range(self.dim)).reshape((N, N))
        Qinv = Q.T.conj() / N
        mu = 0 if not subtract_mu else self.get_mu()
        K = Qinv.dot(self.K.ravel()[:, None] * Q)
        V = np.diag(self.get_V().ravel() - mu)
        H = K + V
        return H

    def get_mu(self):
        """Compute the chemical potential for convenience only."""
        y = self[...]
        dy = self.empty()
        self.compute_dy_dt(dy, subtract_mu=False)
        Hy = dy[...] / self._phase
        mu = self.braket(y, Hy) / self.braket(y, y)
        assert np.allclose(0, mu.imag)
        return mu

    def get_Hy(self, subtract_mu=False):
        """Return `H(y)` for convenience only."""
        dy = self.empty()
        self.compute_dy_dt(dy=dy, subtract_mu=subtract_mu)
        Hy = dy / self._phase
        return Hy

    def plot(self, log=False):  # pragma: nocover
        from matplotlib import pyplot as plt
        from matplotlib.gridspec import GridSpec
        from mmfutils.plot import imcontourf

        n = self.get_density()
        if log:
            n = np.log10(n)

        if self.dim == 1:
            x = self.xyz[0] / u.micron
            plt.plot(x, n)
        elif self.dim == 2:
            from mmfutils import plot as mmfplt

            x, y = [_x / u.micron for _x in self.xyz[:2]]
            psi = self[...]
            n = self.get_density()
            ax = plt.subplot(121)
            imcontourf(x, y, n)
            ax.set_aspect(1)
            plt.colorbar()
            ax = plt.subplot(122)
            mmfplt.imcontourf(x, y, np.angle(psi), cmap="huslp", aspect=1)
            plt.colorbar()
            mmfplt.phase_contour(x, y, psi, aspect=1, linewidths=0.5)
            ax.set_aspect(1)

        elif self.dim == 3:
            x, y, z = [_x / u.micron for _x in self.xyz]
            nxy = n.sum(axis=2)
            nxz = n.sum(axis=1)
            nyz = n.sum(axis=0)

            gs = GridSpec(1, 3)
            ax = plt.subplot(gs[0])
            imcontourf(x, y, nxy)
            ax.set_aspect(1)
            ax = plt.subplot(gs[1])
            imcontourf(x, z, nxz)
            ax.set_aspect(1)
            ax = plt.subplot(gs[2])
            imcontourf(y, z, nyz)
            ax.set_aspect(1)

        E = self.get_energy()
        N = self.get_N()
        plt.suptitle("N={:.4f}, E={:.4f}".format(N, E))
