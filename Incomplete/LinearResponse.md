---
jupytext:
  formats: md:myst,ipynb
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.16.2
kernelspec:
  display_name: Python 3 (ipykernel)
  language: python
  name: python3
---

```{code-cell} ipython3
%matplotlib inline
import numpy as np, matplotlib.pyplot as plt
from IPython.display import display, clear_output
import mmf_setup;mmf_setup.nbinit()
```

```{code-cell} ipython3
from pytimeode.evolvers import EvolverABM, EvolverSplit
from mmfutils.contexts import FPS
```

```{code-cell} ipython3
%load_ext autoreload
%autoreload 2
from gpe.bec import StateBase, StateHOMixin, u

class State(StateHOMixin, StateBase):
    pass

s = State(mu=1.0, #Ntot=1e1, 
          Nxyz=(2**7,), Lxyz=(20*u.micron,))
s.pre_evolve_hook()
s._N = 1e1
s.normalize()
s.plot()
s.cooling_phase = 1j

e = EvolverSplit(s, dt=1.0, normalize=True)

E_tol = 1e-8
E1 = e.y.get_energy()
E0 = E1 + 2*E_tol

log = True

for frame in FPS(timeout=60):
    if abs(E1 - E0) < E_tol:
        break
    e.evolve(100)
    E0, E1 = E1, e.y.get_energy()
    plt.clf()
    e.y.plot(log=log)
    display(plt.gcf())
    clear_output(wait=True)
    
e.dt = 0.01
E0 = E1 + 2*E_tol        
for frame in FPS(timeout=60):
    if abs(E1 - E0) < E_tol/100:
        break
    print(abs(E1 - E0))
    e.evolve(100)
    E0, E1 = E1, e.y.get_energy()
    plt.clf()
    e.y.plot(log=log)
    clear_output(wait=True)
    display(plt.gcf())

fact = 100.0
e = EvolverABM(e.get_y(), dt=1.0/fact)
E0 = E1 + 2*E_tol        
for frame in FPS(timeout=60):
    if abs(E1 - E0) < E_tol/fact/10:
        break
    e.evolve(100)
    E0, E1 = E1, e.y.get_energy()
    plt.clf()
    e.y.plot(log=log)
    clear_output(wait=True) 
    display(plt.gcf())

plt.close('all')

s = e.get_y()
```

# Normal Modes

+++

Here we consider the fluctuations about a stationary state $\psi_0$ of the GPE:

$$
  \psi = \psi_0 + u(x)e^{\I\omega t} + v^*(x) e^{-\I\omega t}.
$$

This gives the following generalized eigenvalue problem for the modes:

$$
  \begin{pmatrix}
    \op{H} + g n_0 & g\psi_0^2\\
    g \bar{\psi}_0^2 & \bar{\op{H}} + g n_0
  \end{pmatrix}\cdot
  \begin{pmatrix}
    u(x)\\
    v(x)
  \end{pmatrix}
  =
  \omega
  \begin{pmatrix}
    \mat{1} \\
     & -\mat{1}
  \end{pmatrix}\cdot
  \begin{pmatrix}
    u(x)\\
    v(x)
  \end{pmatrix},
$$

where $n_0 = \abs{\psi_0}^2$ and $\op{H} = -\hbar^2\nabla^2/2m + gn_0 + \op{V}_{\text{ext}}$ is the single-particle Hamiltonian for the ground state. To solve this numerically, we write this as $\mat{A}\cdot\vect{q} = \omega \mat{B}\cdot\vect{q}$.

These matrices have the following properties: $\mat{A} = \mat{A}^\dagger$ and $\mat{B} = \mat{B}^\dagger$ are Hermitian, and the matrix $\mat{C} = \mat{C}^{-1} =  \bigl(\begin{smallmatrix}&\mat{1}\\\mat{1}\end{smallmatrix}\bigr)$ conjugates $\mat{A}$:

$$
  \mat{C}\cdot\mat{A}\cdot\mat{C} = \bar{\mat{A}}\\
  \mat{C}\cdot\mat{B}\cdot\mat{C} = -\bar{\mat{B}}.
$$.  Thus, if we have one eigenvalue $\omega_{+}$ and eigenvector $\vect{q}_{+}$, then, we must have another pair $\omega_{-} = \bar{\omega}_{+}$ and $\vect{q}_{-} = \mat{C}\cdot\bar{\vect{q}}_{+}$:

$$
  \mat{A}\vect{q}_{+} = \omega_+ \mat{B}\vect{q}_{+}\\
  \bar{\mat{A}}\mat{C}\vect{q}_{+} = \omega_+ \bar{\mat{B}}\mat{C}\vect{q}_{+}.
$$

Furthermore, if $\psi_0$ is a stationary state, then $\op{H}\psi_0 = \mu\psi_0$.



One has two choices: solve the non-symmetric eigenvalue problem $(\mat{B}^{-1}\cdot\mat{A})\cdot\vect{q} = \omega\vect{q}$, or try to massage this into a form where $\mat{B}$ is positive definite.

+++

$$
  (\mat{A} + 2\mat{1})\cdot\vect{q} = (\omega\mat{B} + 2\mat{I})\cdot\vect{q}
$$

+++

## Fixed Particle Number

+++

Let $\psi = \psi_0 + \d{\psi}$.  The change in density is:

$$
  \dot{n} = \dot{\psi}^\dagger \psi_0 + \psi_0^\dagger \dot{\psi} 
          + \dot{\psi}^\dagger \dot{\psi}
$$
$$
  n - n_0 = e^{-\I\omega t}(u \psi_0^* + v\psi_0)
  + e^{\I\omega t}(u^* \psi_0 + v^*\psi_0^*)
  + e^{-2\I\omega t} u^*v^*
  + e^{2\I\omega t} uv
  + (u^*u + v^*v)
$$

particle number is:

$$
  \d{N} = \int\left(
    \d{\psi}^\dagger \psi_0 + \psi_0^\dagger \d{\psi} + \d{\psi}^\dagger \d{\psi}
  \right)\d{x}\\
  = e^{-\I\omega t}\int(u \psi_0^* + v\psi_0)\d{x}
  + e^{\I\omega t}\int(u^* \psi_0 + v^*\psi_0^*)\d{x}
  + e^{-2\I\omega t} \int u^*v^*\d{x}
  + e^{2\I\omega t}\int uv\d{x}
  + \int(u^*u + v^*v)\d{x}
$$

To linear order, conservation of particle number thus implies that

$$
  \int(u \psi_0^\dagger + v\psi_0)\d{x} = 0.
$$

```{code-cell} ipython3
import scipy.linalg
sp = scipy
H = s.get_H()


n_0 = s.get_density().ravel()
psi_0 = 1*s[...].ravel()
A = np.bmat(
    [[H + np.diag(s.g*n_0), np.diag(s.g*psi_0**2)],
     [np.diag(s.g*psi_0.conj()**2), H.conj() + np.diag(s.g*n_0)]])
A = np.asarray(A)
i = np.eye(len(H))
z = np.zeros_like(H)
B = np.asarray(np.bmat([[i, z], [z, -i]]))
C = np.asarray(np.bmat([[z, i], [i, z]]))
I = np.eye(2*len(H))

assert np.allclose(C.dot(C), I)
assert np.allclose(C.dot(A).dot(C), A.conj())
ws, uvs = sp.linalg.eig(np.linalg.inv(B).dot(A))
inds = np.argsort(abs(ws.real))
ws = ws[inds]
uvs = uvs[:, inds]
us, vs = uvs.reshape((2, len(H), 2*len(H)))
```

```{code-cell} ipython3
ws[:10], s.ws[0]
```

```{code-cell} ipython3
s.cooling_phase = 1.0
s.pre_evolve_hook()
s.cooling_phase = 1.0
s.t = 0.0

mode = 4
d = 0.001
u_, v_ = us[:, mode], vs[:, mode]
s[...] = psi_0 + d*(u_ + v_.conj())
w = ws[mode].real
T = 2*np.pi / w 
dt = T/100/100
e = EvolverSplit(s, dt=dt, normalize=False)
log = False
x = s.xyz[0]
for frame in FPS(timeout=60):
    e.evolve(100)
    plt.clf()
    #e.y.plot(log=log)
    t = e.y.t
    psi = psi_0 + d*(u_*np.exp(1j*w*t) + v_.conj()*np.exp(-1j*w*t))
    plt.plot(x, abs(e.y[...])**2 - abs(psi_0)**2)
    plt.plot(x, abs(psi)**2 - abs(psi_0)**2)

    #plt.ylim(0, 2)
    plt.xlim(-6, 6)
    plt.title("N = {}".format(e.y.get_N()))
    display(plt.gcf())
    clear_output(wait=True)
```

```{code-cell} ipython3
np.allclose(np.linalg.inv(B).dot(A).dot(uvs), ws[None,:]*uvs)
```

# Response in a Periodic Box

+++

Here we consider the response of a BEC in a box.  We simply state the analytical result, then check this numerically.

$$
  \psi(x, t) = 
$$

To demonstrate, we perturb the uniform state with
\begin{gather*}
  V(x, t) = \exp\left(-\frac{x^2}{2\sigma_x^2} - \frac{(t-t_0)^2}{2\sigma_t^2}\right),\qquad
  \tilde{V}(k, \omega) = 2\pi\sigma_x\sigma_t\exp\left(
    -\frac{k^2\sigma_x^2}{2} - \frac{\omega^2\sigma_t^2}{2} + \I\omega t_0
  \right).
\end{gather*}

```{code-cell} ipython3
import sympy
t, w, t_0, s = sympy.var('t,w,t_0,s', positive=True)
sympy.integrate(sympy.exp(sympy.I*w*t-(t-t_0)**2/2/s**2), (t, -sympy.oo, sympy.oo)).simplify()
```

```{code-cell} ipython3
%matplotlib inline
import numpy as np, matplotlib.pyplot as plt
from IPython.display import display, clear_output
import mmf_setup;mmf_setup.nbinit()

%load_ext autoreload
%autoreload 2
from gpe.bec import StateBase, u
from gpe.minimize import MinimizeState
from pytimeode.evolvers import EvolverABM, EvolverSplit
from mmfutils.contexts import FPS

class StateKick(StateBase):
    """
    Arguments
    ---------
    sigma_t, sigma_x : float
        Width of kick in time and space.
    kick_delay : float
        Calculate t_0 so that `V(0)/V(t_0) = kick_delay`.
    """
    Vkick_p = 10
    sigma_t = 1.0
    sigma_x = 1.0
    kick_delay = 1e-12
    kick_V_mu = 0.1
    
    def init(self):
        super().init()
        self.kick_V = self.kick_V_mu * self.mu
        self.t0 = self.sigma_t * np.sqrt(-2 * np.log(self.kick_delay))
        self._Vkick = self.get_Vkick()
        
    def get_Vwk(self, w, k):
        """Return the Fourier transform of the kick."""
        return 2*np.pi * self.sigma_t * self.sigma_x * np.exp(
            -(k*self.sigma_x)**2/2 - (w*self.sigma_t)**2/2 + 1j*self.t0*w
        )
    
    def get_Vkick(self):
        x = self.xyz[0]
        return np.exp(-(x/self.sigma_x)**2/2)

    def get_V0_kick(self, t):
        return self.kick_V * np.exp(-((t-self.t0)/self.sigma_t)**2/2)
    
    def _get_Vext_(self):
        Vext = self._Vkick * self.get_V0_kick(t=self.t)
        if self.initializing or self.t < 0:
            Vext -= self.mu
        return Vext

s = StateKick(mu=1.0, Nxyz=(2**8,), Lxyz=(10*u.micron,))
s = MinimizeState(s).minimize()
s.plot()
```

```{code-cell} ipython3
fact = 4.0
s = StateKick(mu=1.0, Nxyz=(2**8,), Lxyz=(40*u.micron,))
s = MinimizeState(s).minimize()
s.cooling_phase = 1+0.1j
ev = EvolverABM(s, dt=1.0/fact*s.t_scale)
states = [ev.get_y()]
T = 200.0
steps = 100
skip = 10
n_tol = 1e-12
for frame in FPS(frames=2**16, timeout=60*60):
    ev.evolve(steps)
    #if ev.y.t > 2*s.kick_T0 + T:
    #    break
    n = ev.y.get_density() 
    if (ev.y.t > 2*s.t0 + T) and (n.std() / n.mean() < n_tol):
        break
    states.append(ev.get_y())
    if frame % skip == 0:
        plt.clf()
        ev.y.plot(log=False)
        clear_output(wait=True) 
        display(plt.gcf())

plt.close('all')

s = ev.get_y()
ev.y.t
```

```{code-cell} ipython3
ts = np.array([s.t for s in states])
xs = states[-1].x
psis = np.array([s.get_psi() for s in states])
ns = abs(psis)**2
plt.pcolormesh(ts, xs, ns.T)
```

```{code-cell} ipython3
s = states[-1]
Nt = len(ts)
dt = np.diff(ts).mean()
T = dt*Nt
ns = abs(psis)**2
nst = np.fft.fftshift(np.fft.fftn(ns-ns.mean(axis=1)[:, None]))
ws = np.fft.fftshift(2*np.pi * np.fft.fftfreq(Nt, dt))
ks = np.fft.fftshift(states[-1].basis.kx)

#Vk = np.fft.fftshift(s._Vkick_k)
#Vw = np.fft.fftshift(np.fft.fft(s.get_V0_kick(ts)))
#Vkw = Vk[None, :]*Vw[:, None]
Vkw = s.get_Vwk(w=ws[:, None], k=ks[None, :])
#plt.pcolormesh(ks, ws, np.log(abs(Vkw)**2), shading='auto')
iw = np.where(abs(ws) < 3)[0][0]
ik = np.where(abs(ks) < 3)[0][0]
plt.pcolormesh(ks[ik:-ik], ws[iw:-iw], abs(nst/Vkw)[iw:-iw,ik:-ik], shading='auto')
ax = plt.gca()
ax.set(xlabel="$k$", ylabel="$\omega$")
```

The idea here is that, evolving for one period of the drive, we should have
\begin{gather*}
  \ket{\psi_1} = e^{\I\phi}\ket{\psi_0}.
\end{gather*}
We can consider minimizing the difference:
\begin{gather*}
  \braket{\psi_1 - e^{-\I\phi}\psi_0|\psi_1 - e^{-\I\phi}\psi_0} = 
  \braket{\psi_1|\psi_1} + \braket{\psi_0|\psi_0}
  - \Re\Bigl(e^{-\I\phi}\braket{\psi_0|\psi_1}\Bigr).
\end{gather*}
If our states are normalized, this is equivalent to minimizing the last term, which happens if we choose
\begin{gather*}
  \braket{\psi_0|\psi_1} = \abs{\braket{\psi_0|\psi_1}}e^{\I\phi}.
\end{gather*}
The objective is thus to maximize the overlap:
\begin{gather*}
  \max \abs{\braket{\psi_1|\psi_0}}.
\end{gather*}

```{code-cell} ipython3
from gpe.utils import get_good_N

class StateDrive(StateBase):
    """Driven Oscillator
    
    This class provides methods for periodically driving a system at
    a given wavelength.  We simulate a single cell and look for
    fixedpoint after a period.
    
    Arguments
    ---------
    w : float
        Drive frequency.
    Lx : float
        Wavelength of mode: `k = 2*np.pi*nk/Lx`
    """
    hbar = 1.0
    m = 1.0
    w = None
    Lx = 1.0
    V0_mu = 0.1
    mu = 1.0
    dx_healing_length = 0.3
    mixing = 0.5
    
    def __init__(self, **kw):
        for key in list(kw):
            if hasattr(self, key):
                setattr(self, key, kw.pop(key))
        
        self.healing_length = self.hbar / np.sqrt(2 * self.m * self.mu)
        dx = self.dx_healing_length * self.healing_length 
        Nx = get_good_N(np.ceil(self.Lx/dx))
        kw.update(hbar=self.hbar, m=self.m, mu=self.mu, Lxyz=(self.Lx,), Nxyz=(Nx,),
                  constraint="N")
        super().__init__(**kw)
        
    def init(self):
        super().init()
        self.V0 = self.V0_mu * self.mu
        self.k = 2*np.pi / self.Lx
        if self.w is None:
            n0 = self.mu
            H0 = (self.hbar*self.k)**2/2/self.m
            self.w = np.sqrt(H0*(H0 + 2*self.mu)) / self.hbar
        self.T = 2*np.pi / self.w
        
    def _get_Vext_(self):
        Vext = self.V0 * np.sin(self.w*self.t)*np.cos(self.k*self.xyz[0])
        if self.initializing or self.t < 0:
            Vext -= self.mu
        return Vext

    def evolve_T(self, fraction=1.0, dt_t_scale=0.4):
        """Evolver for fraction of a single period."""
        dt = dt_t_scale*self.t_scale
        T = fraction * self.T
        steps = max(int(np.ceil(T / dt)), 2)
        dt = T / steps
        ev = EvolverABM(self, dt=dt)
        ev.evolve(steps)
        self.set_psi(ev.y.get_psi())
        self.t = ev.y.t
    
    def step(self, dt_t_scale=0.4):
        """Take a step using Anderson mixing."""
        psi0 = self.get_psi().copy()
        self.evolve_T(dt_t_scale=dt_t_scale)
        psi1 = self.get_psi()
        
        # Remove phase
        phi = np.angle((psi0.conj()*psi1).sum())
        psi1 /= np.exp(1j*phi)
        m = self.mixing
        f = self._f(psi0=psi0, psi1=psi1)
        self.set_psi((1-m)*psi0 + m*psi1)
        self.normalize()
        return f
    
    def g_broyden(self, psi0):
        """Broyden iteration."""
        psi0 = np.array(psi0) # Make a copy
        self.set_psi(psi0)
        self.step()
        psi1 = self.get_psi()
        return psi1 - psi0
        
    def _f(self, psi0, psi1):
        return (abs(psi0)**2 + abs(psi1)**2).sum() - 2*abs((psi1.conj() * psi0).sum())
    
    def f(self, x):
        """Objective function"""
        psi0 = x
        self.set_psi(psi0)
        self.step()
        psi1 = self.get_psi()
        return self._f(psi0=psi0, psi1=psi1)
```

```{code-cell} ipython3
s = StateDrive(V0_mu=0.2, mixing=1, dx_healing_length=0.1, cooling_phase=1+0.1j)
for frame in FPS(30):
    n0 = s.get_density()
    f = s.step()
    if frame % 10 == 0:
        plt.clf()
    n1 = s.get_density()
    plt.plot(s.xyz[0], n1/n0 - 1)
    plt.plot(s.xyz[0], n1, '--')
    plt.title(f"{f}, {frame}, {s.get_N()}")
    clear_output(wait=True)
    display(plt.gcf())
s0 = s.copy()
#z = s.gets.f_broyden
```

```{code-cell} ipython3
s0 = s
```

```{code-cell} ipython3
s = s0.copy()
```

```{code-cell} ipython3
psi0 = s.get_psi()
psi1 = s.g_broyden(psi0)
psi1 + psi0
```

```{code-cell} ipython3
self = s
psi0 = self.get_psi().copy()
dt = 0.4*self.t_scale
steps = max(int(np.ceil(self.T / dt)), 2)
dt = self.T / steps
ev = EvolverABM(self, dt=dt)
ev.evolve(steps)
psi1 = ev.y.get_psi().copy()
phi = np.angle((psi0.conj() * psi1).sum())
psi1 *= np.exp(1j*phi)
```

```{code-cell} ipython3
psi1 - psi0
```

```{code-cell} ipython3
psi0 = s.get_psi().copy()
s.evolve_T()
psi1 = s.get_psi().copy()
(psi1/psi1.mean())/(psi0/psi0.mean()) - 1
```

```{code-cell} ipython3
ev = EvolverABM(s, dt=0.1*s.t_scale)
steps = 10
for frame in FPS(1000):
    ev.evolve(steps)
    plt.clf()
    ev.y.plot()
    clear_output(wait=True)
    display(plt.gcf())
```

```{code-cell} ipython3
s = StateDrive(mu=1.0, nk=20, V0_mu=0.1, Nxyz=(2**8,), Lxyz=(40*u.micron,), mixing=0.5)
for frame in FPS(1000):
    n0 = s.get_density()
    f = s.step()
    if frame % 10 == 0:
        plt.clf()
    n1 = s.get_density()
    plt.plot(s.xyz[0], n1/n0 - 1)
    plt.plot(s.xyz[0], n1, '--')
    plt.title(f"{f}, {frame}")
    clear_output(wait=True)
    display(plt.gcf())
```

```{code-cell} ipython3
s = StateDrive(mu=1.0, Nxyz=(2**8,), Lxyz=(40*u.micron,))
s.plot()
#for frame in FPS(timeout=60):
#    s.step()
#    plt.clf()
#    s.plot()
#    clear_output(wait=True)
#    display(plt.gcf())

s.cooling_phase = 1 + 0.01j
for frame in FPS(100):
    s.step()
    clear_output(wait=True)
    print(s.f(abs(s.get_psi())))
    s.plot()
    display(plt.gcf())
```

```{code-cell} ipython3
s = StateDrive(mu=1.0, nk=20, V0_mu=0.1, Nxyz=(2**8,), Lxyz=(40*u.micron,), mixing=1)
x0 = np.ones(s.shape)
x1 = f(f(x0))
plt.plot(x1/x0-1)
```

```{code-cell} ipython3
s = StateDrive(mu=1.0, nk=10, V0_mu=0.1, Nxyz=(2**8,), Lxyz=(40*u.micron,), mixing=1.0)
Ntot = s.get_N()
def f(x, Ntot=Ntot):
    psi0 = x
    s.set_psi(psi0)
    s.scale(np.sqrt(Ntot / s.get_N()))
    s.step()
    psi1 = s.get_psi()
    #assert np.allclose(s.get_N(), Ntot)
    phase = np.exp(1j*np.angle((psi1.conj() * psi0).sum()))
    psi1 *= phase
    return psi1.real

def g(x):
    return f(x) - x

x0 = np.ones(s.shape)
for frame in FPS(1000):
    x1 = f(x0)
    
    if frame % 10 == 0:
        plt.clf()
    plt.plot(s.xyz[0], x1/x0 - 1)
    plt.plot(s.xyz[0], x1, '--')
    #plt.title(f"{f}")
    display(plt.gcf())
    clear_output(wait=True)
    x0 = x1
```

```{code-cell} ipython3
s.set_psi(s.get_psi()/(s.get_psi().mean()))
s.normalize()
s.plot()
```

```{code-cell} ipython3
s.plot()
s.step()
s.plot()
s.evolve_T()
s.plot()
s.g_broyden(s.get_psi())
```

```{code-cell} ipython3
s1 = s.copy()
psi0 = s.get_psi()
s1.set_psi(psi0)
s1.normalize()
psi1 = s1.evolve_T()
s1.plot()
s.plot()
```

```{code-cell} ipython3
self = s
self.set_psi(psi0)
self.step()
psi1 = self.get_psi()
dpsi = psi1 - psi0
dpsi
```

```{code-cell} ipython3
s = s0.copy()
```

```{code-cell} ipython3
psi0 = s.get_psi().copy()
s.evolve_T()
psi1 = s.get_psi().copy()
phi = np.angle((psi0.conj()*psi1).sum())
psi1 /= np.exp(1j*phi)
psi1/psi0
```

```{code-cell} ipython3
psi0 = s.get_psi().copy()
s.set_psi(psi0)
print(s.step())
psi1 = s.get_psi()
psi1/psi0
psi0.mean(), psi1.mean()
```

```{code-cell} ipython3
psi0 = self.get_psi().copy()
        self.evolve_T(dt_t_scale=dt_t_scale)
        psi1 = self.get_psi()
        
        # Remove phase
        phi = np.angle((psi1.conj()*psi0).sum())
        psi1 /= np.exp(1j*phi)
        m = self.mixing
        f = self._f(psi0=psi0, psi1=psi1)
        self.set_psi((1-m)*psi0 + m*psi1)
        self.normalize()
        return f
```

```{code-cell} ipython3
psi0 = s.get_psi().copy()
s.g_broyden(psi0)
s.set_psi(s.get_psi() + psi0)
s.plot()
```

## Broyden

```{code-cell} ipython3
import scipy as sp
s = StateDrive(V0_mu=0.1, mixing=0.5, dx_healing_length=0.1, cooling_phase=1+0.001j)
s = StateDrive(V0_mu=0.2, mixing=1.0, dx_healing_length=0.1, cooling_phase=1+0.01j)
x0 = np.ones(s.shape) * abs(s.get_psi()).mean() + 0j

def g(x):
    f = s.g_broyden(x)
    callback(x, f)
    return f

def callback(x, f, n=[0], skip=1):
    if n[0] % skip == 0:
        plt.clf()
    n[0] += 1
    plt.title(f"{n[0]}")
    plt.plot(s.xyz[0], f)
    plt.plot(s.xyz[0], x, '--')
    clear_output(wait=True)
    display(plt.gcf())

s.t = 0
opts = dict(jac_options=dict(alpha=1, reduction_method='svd'))
#opts = {}
x0 = np.sqrt(s.get_density().mean()) + 0.5*np.cos(s.k*s.xyz[0]) + 0j
res = sp.optimize.root(g, x0=x0, method='broyden2', options=opts, callback=callback)
s.set_psi(res.x)
plt.close('all')
print(res)
```

```{code-cell} ipython3
s.plot()
s0.plot()
```

```{code-cell} ipython3
np.asarray([1,2], dtype=complex)
```

```{code-cell} ipython3
from mmfutils.solve.broyden import Jacobian
s = StateDrive(V0_mu=0.1, mixing=0.5, dx_healing_length=0.1, cooling_phase=1+0.001j)

def pack(x):
    return x.view(dtype=complex)

def unpack(psi):
    return np.asarray(psi, dtype=complex).view(float)

def g(x):
    f = unpack(s.g_broyden(pack(x)))
    callback(f, x)
    return f

def callback(f, x, n=[0], skip=1):
    if n[0] % skip == 0:
        plt.clf()
    n[0] += 1
    plt.title(f"{n[0]}")
    plt.plot(s.xyz[0], abs(pack(f)))
    plt.plot(s.xyz[0], abs(pack(x)), '--')
    clear_output(wait=True)
    display(plt.gcf())

s.t = 0
x0 = unpack(np.sqrt(s.get_density().mean()) + 0*np.cos(s.k*s.xyz[0]))
J = Jacobian(alpha=1)
x = x0
for frame in FPS(1000):
    f = g(x)
    J.update(x, f)
    x -= 0.5*J.solve(f)
```

```{code-cell} ipython3
s.t = s.T/4
plt.plot(s.get_Vext())
```

```{code-cell} ipython3
from scipy.optimize import minimize
s = StateDrive(mu=1.0, nk=20, V0_mu=0.1, Nxyz=(2**8,), Lxyz=(40*u.micron,))
x0 = np.ones(s.shape) * abs(s.get_psi()).mean()
res0 = s.f(x0)
def f(x, n=[0], skip=10):
    res = s.f(x)
    if n[0] % skip == 0:
        plt.clf()
        s.plot()
        plt.title(f"{res}")
        clear_output(wait=True)
        display(plt.gcf())
    n[0] += 1
    return res
minimize(f, x0=x0, method='BFGS', options=dict(eps=0.1))
```

##
