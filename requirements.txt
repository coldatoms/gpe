# This is an implicit value, here for clarity
#--index-url https://pypi.python.org/simple/
--find-links https://github.com/forbes-group/MyPI

pytimeode==0.10.0.dev0
mmfutils>0.5.4
pytest>=6.2.3
pytest-cov>=2.11.1
pytest-flake8>=1.0.7
boltons>=20.2.1
wrapt>=1.12.1
numexpr
persist
mmf_setup
xarray
