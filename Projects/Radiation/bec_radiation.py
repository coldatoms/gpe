from __future__ import division, with_statement, print_function

from argparse import Namespace
from collections import namedtuple
import inspect
import logging
import time

import numpy as np
import scipy.interpolate
from scipy.stats.mstats import gmean

sp = scipy

import attr

import mmfutils
from mmfutils.contexts import coroutine
from mmfutils.math.bases import CylindricalBasis

from pytimeode.evolvers import EvolverABM

from gpe import utils
from gpe import bec, bec2, axial
from gpe import tube, tube2
from gpe.soc import Experiment, u

from gpe.minimize import MinimizeState

from SOC import soc_catch_and_release
from SOC.soc_catch_and_release import ExperimentCatchAndRelease

__all__ = ["ExperimentGPE", "State", "StateTube", "StateAxial"]


class ExperimentGPE(utils.ExperimentBase):
    """Represents a set of experimental parameters.

    1. Responsible for translating these into a proper State object.  This
       generally amounts to converting experimental parameters into values
       useful to the state class.  These are stored in self.state_args for use
       in `self.get_state` and `self.get_initial_state`.
    2. Provides get_Vext(state) with a (possibly time dependent) external
       potential.
    3. Also provides a mechanism for recording time-dependent parameters.
       These should be provided through a set of methods names `*_t_()` which
       take the normalized time `t_` as an argument and return the
       time-dependent value of this parameter.  For plotting purposes an
       accompanying method `*_info()` should be defined which returns the
       corresponding unit value and a label.
    4. Internal methods should use the `get(param, t_)` method which will
       delegate to the appropriate time-dependent function if it exists (or
       fall back to the basic parameter access).
    5. Implements an experimental protocol.  To do this, subclass and define
       the run_experiment method.  This can be used to get the initial state,
       then to manipulate it.  (Not implemented yet.)

    Note: these experiments are designed for SOC along the x-axis only.  The
    following basis options are supported:

    '1D' : Periodic basis along the x-axis.

       `tube==False` : Assume a constant density along y and z.  This
          should be thought of as modeling a 3D gas with translation symmetry
          along the transverse directions.  (The coupling constants are not
          modified for modeling 1D systems.)  The density here will be the
          central density.

       `tube==True` : Use the the tube code to model the transverse trap.  The
          density here will be the line density obtained by integrating over
          the transverse directions. This will effectively modify the coupling
          constants.
    'axial' : Periodic basis along the x-axis with radial excitations assuming
       axial symmetry.  The transverse trapping frequency will be the average
       of the y and z frequencies.  The radial extent and lattice spacing will
       be inferred from `Nx`, `dx` and `Lx` using the ratio of the trapping
       frequencies at time `t_=0`.
    '2D', '3D' : Full 2D or 3D simulation assuming no symmetry. The transverse
       extents and lattice spacing should be provided in `Nxyz` and `Lxyz`,
       otherwise it will be inferred from `Nx`, `dx` and `Lx` using the ratio
       of the trapping frequencies at time `t_=0`.

    Note: All times in the Experiment classes are specified in units of
    `t_unit`.  In arguments this is `t_ = t/t_unit` and we use `t_` as a kwarg
    to ensure that this is properly used.

    Parameters
    ----------
    dx : None, (float,)
       If specified (not `None`), then `Nx` will be computed such that his
       minimum lattice spacing is realized, guaranteeing a certain UV cutoff.
    f_osc : float
       Frequency of oscillations
    R_osc : float
       Amplitude of oscillations
    r_osc : float
       Width of Gaussian potential.
    V_osc : float
       Depth of Gaussian potential.
    """

    t_unit = u.ms  # All times specified in this unit

    t__final = np.inf  # Time when potentials are turned off for expansion.
    t__image = 0  # Expansion time for imaging

    State = None

    # Default values.  These are not expected to change, but could be changed
    # by passing kwargs to the base class __init__ method.
    species = (1, -1)  # Spin states of the species
    g = None  # Coupling constants: determined from states
    trapping_frequencies_Hz = (3.07, 278.08, 278.04)
    x_TF = 234.6 * u.micron  # Thomas Fermi "radius" (where V(x_TF) = mu)
    mu = None  # Alternative: specify mu
    Nx = None  # Default lattice and box size
    Lx_expt = 1000 * u.micron  # Lx used for experiment.
    Lx = None  # Actual problem Lx.  Might be a central portion
    dx = 0.061 * u.micron  # Can be used instead to fix Nx
    Nr = None  # Size of axial basis (computed if not provided)
    R = None  # Radius of axial basis (computed if not provided)

    f_osc = 1 * u.kHz  # Oscillator frequency
    R_osc = 10 * u.micron  # Oscillator amplitude
    r_osc = 4 * u.micron  # Oscillator size (radius)
    V_osc = 10 * u.nK  # Oscillator depth
    phase_osc = 0  # phase=0 is a sine, phase = pi/2 is a cosine

    cooling_phase = 1.0
    tube = False
    basis_type = "1D"  # '1D', '2D', '3D', `axial`

    @property
    def dim(self):
        if self.basis_type == "1D":
            return 1
        elif self.basis_type in set(["2D", "axial"]):
            return 2
        elif self.basis_type == "3D":
            return 3
        else:
            raise ValueError(
                "Unknown basis_type={} (use one of '1D', '2D', '3D', or 'axial')".format(
                    self.basis_type
                )
            )

    def init(self):
        msgs = []

        scattering_length = u.scattering_lengths[(self.species, self.species)]
        self.m = u.masses[self.species]

        if getattr(self, "trapping_frequencies_Hz", None) is not None:
            if not hasattr(self, "ws"):
                # Allow subclasses to change this
                self.ws = 2 * np.pi * np.array(self.trapping_frequencies_Hz) * u.Hz
            self.ws = np.asarray(self.ws)
            if not hasattr(self, "ws_expt"):
                # These may be set externally, but if not, then define them
                # here.  They are used in get_fiducial_V_TF().
                self.ws_expt = self.ws

        if getattr(self, "Lx_expt", None) is None:
            self.Lx_expt = self.Lx
        if getattr(self, "Lx", None) is None:
            self.Lx = self.Lx_expt

        if self.dx is not None:
            # Special case to calculate Nxyz in terms of a lattice spacing
            self.Nx = utils.get_good_N(self.Lx / self.dx)
        else:
            self.dx = self.Lx / self.Nx

        msgs.append("Using 3D coupling constants")
        g = 4 * np.pi * u.hbar**2 / self.m * scattering_length

        if self.g is None:
            self.g = g
        else:
            mesg.append("Scattering length self.g defined manually...")

        # Collect all time-dependent methods.
        # Note: getmembers() accesses fiducial_V_TF which used to trigger an
        # unwanted call to the potentially expensive get_fiducial_V_TF().  We
        # preempt this here by setting it to None, then deleting it later.
        # This also resets it which is consistent with init()'s semantics of
        # resetting the experiment (parameters may have changed).
        self._fiducial_V_TF = None

        self.time_dependent_parameters = {
            _name[:-3]: _meth
            for (_name, _meth) in inspect.getmembers(self, predicate=callable)
            if _name.endswith("_t_")
        }

        del self._fiducial_V_TF

        for name in self.time_dependent_parameters:
            # method_name = name + "_t_"
            info_name = name + "_info"
            if not hasattr(self, info_name):
                setattr(self, info_name, (1.0, name))

        self.msgs = msgs

    ######################################################################
    # Parameter access
    def get(self, name, t_):
        """Return the value of the parameter `name` at time `t_`.

        If the method `name_t_()` exists, it is called, otherwise the attribute
        `name` is used.
        """
        _meth = self.time_dependent_parameters.get(name)
        if _meth is None:
            return getattr(self, name)
        else:
            return _meth(t_)

    ######################################################################
    # External potential.
    #
    # Here are some methods to help with defining the external potential.
    def _get_Lx(self, state):
        """Return the actual length of the box represented by the state.

        This is needed because self.Lx might not be the same as self.Lx_expt and
        the latter is sometimes needed for fiducial states.
        """
        x = state.xyz[0]
        dx = np.diff(x.ravel())
        assert np.allclose(0, np.diff(dx))
        dx = dx[0]
        Lx = x.max() - x.min() + dx
        return Lx

    def get_VHO(self, state, expt=False):
        """Harmonic oscillator trapping potential."""
        # Makes an assumption about the form of xyz and ws
        # xyz = [x] or [x, r], or [x, y, z]
        if expt:
            ws = self.ws_expt
        else:
            t_ = state.t / self.t_unit
            ws = self.get("ws", t_=t_)

        V_HO = 0.5 * state.m * sum((_w * _x) ** 2 for _w, _x in zip(ws, state.xyz))
        return V_HO

    def get_Vcos(self, state, order=6):
        """Periodic approximation for HO Potential.

        V_0(1-cos(k*x)) ~ V_0k^2*x^2/2 ~ mw^2x^2/2
        k = 2\pi/L
        V_0 = m(w/k)^2
        """
        t_ = state.t / self.t_unit

        xyz = state.xyz
        ws = self.get("ws", t_=t_)
        Lx = self.Lx
        kx = 2 * np.pi / Lx
        V_m = (ws[0] / kx) ** 2 * utils.x2_2(kx * xyz[0])
        for _x, _w in zip(xyz[1:], ws[1:]):
            V_m = V_m + (_w * _x) ** 2 / 2.0

        return state.m * V_m

    def get_Vt(self, state):
        """Optional time-dependent trapping potentials.

        These potentials are ignored when determining the initial particle
        number for When determining the initial stat
        """
        t_ = state.t / self.t_unit
        r_osc = self.get("r_osc", t_=t_)
        V_osc = self.get("V_osc", t_=t_)

        x0 = self.get("x_osc", t_=t_)
        t_ = state.t / self.t_unit
        x = state.xyz[0]
        Lx = self._get_Lx(state)
        x = (x - x0 - x.min()) % Lx + x.min()
        Vt = V_osc * np.exp(-((x / r_osc) ** 2) / 2)
        return Vt

    def x_osc_t_(self, t_):
        R_osc = self.get("R_osc", t_=t_)
        f_osc = self.get("f_osc", t_=t_)
        phase_osc = self.get("phase_osc", t_=t_)
        t = t_ * self.t_unit
        return R_osc * np.sin(2 * np.pi * f_osc * t + phase)

    def get_Vext(self, state, fiducial=False, expt=False):
        """Return the external potential.

        For `t_ > self.experiment.t__final`, all the potentials are set to
        zero.
        """
        t_ = state.t / self.t_unit
        zero = np.zeros(state.shape)

        if expt:
            V = self.get_VHO(state=state, expt=expt)
            if not fiducial:
                V += self.get_Vt(state=state)
        elif t_ > self.t__final:
            V = zero
        else:
            if self.Lx_expt <= self.Lx:
                V = self.get_VHO(state=state)
            else:
                V = self.get_Vcos(state=state)

            V += self.get_Vt(state=state)

        return V

    @property
    def fiducial_V_TF(self):
        """This may be slow to calculate, so we defer calculation until we
        really need it."""
        if not hasattr(self, "_fiducial_V_TF"):
            self._fiducial_V_TF = self.get_fiducial_V_TF()
        return self._fiducial_V_TF

    def get_fiducial_V_TF(self, t_=0.0, Nx=2**12, Lx_factor=1.1):
        """Return the V_TF required to initialize the state.

        If V_TF is None or not defined, then compute the V_TF that defines the
        state in terms of the Thomas-Fermi radius x_TF along the x axis using a
        Harmonic trapping potential with frequencies `ws_expt` as follows:

        1. Construct an axially symmetric state using a CylindricalBasis with
           enough points to model the harmonically trapped cloud in the TF
           approximation.  The assumption is made here that the extremities of
           the cloud are harmonic.  Inside, the potential need not be
           harmonic.  This construction uses `self.ws_expt`.
        2. Compute `V_TF` from `self.x_TF` and `self.ws_expt` assuming harmonic
           extremities.
        3. Get the external potential using this state by calling
           `get_Vext(expt_state, fiducial=True, expt=True)`.  This allows the
           function `get_Vext()` to customize what is meant by the fiducial
           state (i.e. including or excluding parts of the time-varying
           potential.)
        4. Set the initial state and compute the total particle number in the
           TF approximation using:

           `expt_state.set_initial_state(V_TF=V_TF, fiducial=True, expt=True)`

        5. Adjust V_TF to reproduce the total particle number in the TF
           approximation using:

           `expt_state.set_initial_state(V_TF=V_TF, fiducial=False, expt=True)`

           This allows one to simulate a long time cooling into the ground
           with a different potential, while fixing `x_TF` without that
           potential.
        """
        V_TF = getattr(self, "V_TF", None)
        x_TF = getattr(self, "x_TF", None)

        if V_TF is not None:
            if x_TF is not None:
                raise ValueError(
                    "Both V_TF={} and x_TF={} set. Set one to None".format(V_TF, x_TF)
                )
            return V_TF

        self._computing_fiducial_V_TF = True
        # Construct fiducial axial state to get proper chemical potential.
        ws = self.ws_expt
        wx, wr = ws[0], gmean(ws[1:])
        Lx = 2 * Lx_factor * x_TF
        Nr = int(np.ceil(Nx * wx / wr))
        R = Lx / 2.0 * wx / wr
        Nxr = [Nx, Nr]
        Lxr = [Lx, R]

        # E_R = self.get('E_R', t_=t_)
        V_TF = self.m * wx**2 * x_TF**2 / 2.0

        return V_TF
        # E = self.get_dispersion()
        # E0 = E(E.get_k0())[0]*4*E_R
        # mu0 = mu0 - E0

        expt_basis = CylindricalBasis(Nxr=Nxr, Lxr=Lxr, symmetric_x=False, boost_px=0.0)
        expt_state = State(
            basis=expt_basis,
            experiment=self,
            g=self.g,
            m=self.m,
            x_TF=None,
            cooling_phase=self.cooling_phase,
            constraint="N",
            t=0.0,
        )

        # Sanity check that V_TF is correctly defined
        V = self.get_Vext(state=expt_state, fiducial=True, expt=True)
        assert np.all(np.where(expt_state.xyz[0] >= x_TF, V >= V_TF, True))

        expt_state.set_initial_state(V_TF=V_TF, fiducial=True, expt=True)
        self._N_expt = N = expt_state.get_N()

        def f(V_TF):
            expt_state.set_initial_state(V_TF=V_TF, fiducial=False, expt=True)
            res = expt_state.get_N() - N
            return res

        V0, V1 = mmfutils.optimize.bracket_monotonic(f, x0=V_TF)
        # V_TF0 = V_TF
        V_TF = brentq(f, V0, V1)
        self._expt_state = expt_state
        del self._computing_fiducial_V_TF

        return V_TF

    def get_state(self, expt=False):
        """Quickly return an appropriate initial state."""
        state_args = dict(
            experiment=self,
            x_TF=None,
            cooling_phase=self.cooling_phase,
            t=0.0,
            g=self.g,
            m=self.m,
            constraint="N",
        )

        if expt:
            Lx = self.Lx_expt
            Nx = utils.get_good_N(Lx / self.dx)
        else:
            Lx = self.Lx
            Nx = self.Nx

        if self.basis_type == "1D" and self.tube:
            state = StateTube(Nxyz=[Nx], Lxyz=[Lx], **state_args)
        elif self.basis_type == "axial":
            from mmfutils.math.bases import CylindricalBasis

            if self.Nr is None or self.R is None:
                ws = self.ws_expt
                wx, wr = ws[0], gmean(ws[1:])
                a_perp = u.hbar / wr / self.m

                # Maximum of radial width or 4*a_perp where a Gaussian would
                # fall off by a factor of 1e-7.
                R = max(4 * a_perp, self.Lx_expt / 2.0 * wx / wr)
                Nr = int(np.ceil(R / self.dx))
            else:
                Nr, R = self.Nr, self.R
            Nxr = [Nx, Nr]
            Lxr = [Lx, R]
            basis = CylindricalBasis(Nxr=Nxr, Lxr=Lxr, symmetric_x=False, boost_px=0.0)
            state = StateAxial(basis=basis, **state_args)
        else:
            Nxyz = getattr(self, "Nxyz", None)
            Lxyz = getattr(self, "Lxyz", None)
            if Nxyz is None or Lxyz is None:
                ws = self.ws_expt[: self.dim]
                wx, ws = ws[0], ws[1:]
                if self.basis_type == "2D":
                    ws = [gmean(ws)]
                aspect_ratios = np.divide(wx, ws)

                if Lxyz is None:
                    Lxyz = [Lx] + [self.Lx_expt * _aspect for _aspect in aspect_ratios]
                else:
                    Lxyz = [Lx] + list(Lxyz[1:])
                if Nxyz is None:
                    Nxyz = [Nx] + [utils.get_good_N(_L / self.dx) for _L in Lxyz[1:]]
            state = State(Nxyz=Nxyz, Lxyz=Lxyz, **state_args)

        V_TF = self.fiducial_V_TF

        state.set_initial_state(V_TF=V_TF)
        if np.allclose(state[...], 0):
            state[...] = 1.0

        return state

    def get_initial_state(
        self,
        E_tol=1e-12,
        psi_tol=1e-12,
        disp=1,
        tries=20,
        cool_steps=100,
        cool_dt_t_scale=0.1,
        minimize=True,
        **kw,
    ):
        """Return an initial state with the specified population fractions.

        This initial state is prepared in state[0] with the potentials as
        they are at time `t=0`, then the `initial_imbalance` is transferred as
        specified simulating an RF pulse by simply the appropriate fraction in each
        state.  Phases are kept the same as in the state[0].
        """
        state = self.get_state()
        state.cooling_phase = 1.0

        # Cool to minimum with fixed chemical potential
        state.mu = state.get_mu_from_V_TF(V_TF=self.get_fiducial_V_TF())
        state.constraint = None
        fix_N = False

        state.init()

        if minimize:
            m = MinimizeState(state, fix_N=fix_N)
            self._debug_state = m  # Store in case minimize fails
            if "use_scipy" not in kw:
                kw["tries"] = tries
            state = m.minimize(E_tol=E_tol, psi_tol=psi_tol, disp=disp, **kw)

        self._debug_state = state  # Store in case evolve fails

        if cool_steps > 1:
            # Cool a bit to remove any fluctuations.
            state.cooling_phase = 1j
            dt = cool_dt_t_scale * state.t_scale
            state.t = -dt * cool_steps
            evolver = EvolverABM(state, dt=dt)
            evolver.evolve(cool_steps)
            state = evolver.get_y()

        del self._debug_state

        psi0 = state[...]
        # Rely on get_state for all other parameters like t, cooling_phase etc.
        self._state = state = self.get_state()
        state[...] = psi0
        return state

    @coroutine
    def plotter(self, fig=None, plot=True):
        from matplotlib import pyplot as plt

        if fig is None:
            fig = plt.figure(figsize=(20, 5))
        plt.clf()
        lines = [
            plt.plot([], [], "k-")[0],
            plt.plot([], [], "b:")[0],
            plt.plot([], [], "g:")[0],
        ]
        title = plt.title("")
        x = None
        plt.xlabel("x [micron]")
        plt.ylabel("n")
        plt.xlim(-300, 300, auto=None)
        plt.ylim(0, 300, auto=None)

        while True:
            state = yield lines
            na, nb = state.get_density_x()
            E = state.get_energy()
            N = state.get_N()
            t = state.t
            title_text = "t={:.4f}ms, N={:.4f}, E={:.4f}".format(t / u.ms, N, E)
            if x is None:
                x = state.xyz[0] / u.micron
                # lines.extend(plt.plot(x, na, 'b'))
                # lines.extend(plt.plot(x, nb, 'g'))
            lines[0].set_data(x, na + nb)
            lines[1].set_data(x, na)
            lines[2].set_data(x, nb)
            title.set_text(title_text)

            if plot:
                import IPython.display

                IPython.display.display(fig)
                IPython.display.clear_output(wait=True)
            else:
                print("{} < 1000ms".format(t / u.ms))

    def run(
        self,
        get_data,
        fig=None,
        figsize=(20, 5),
        plot=True,
        factor=8.0,
        filename="generated_plots.mp4",
    ):
        if plot:
            import IPython.display
            from matplotlib import pyplot as plt
            from mmfutils.plot.animation import MyFuncAnimation

            if fig is None:
                fig = plt.figure(figsize=figsize)

            with self.plotter(fig=fig, plot=plot) as plot_data:
                anim = MyFuncAnimation(
                    fig,
                    plot_data,
                    frames=get_data(factor=factor, steps=1000),
                    save_count=None,
                )

                anim.save(
                    filename=filename,
                    fps=20,
                    extra_args=["-vcodec", "libx264", "-pix_fmt", "yuv420p"],
                )
                plt.close("all")
                IPython.display.clear_output()
        self.video = IPython.display.HTML(
            '<video src="{0}" width="100%", type="video/mp4" controls/>'.format(filename)
        )


class StateMixin:
    """Mixing to help define States.

    This class depends on an Experiment() instance to define the time-dependent
    properties.

    To do this, the experiment can define the following methods, which will be
    used if they exist.  Note: for performance reasons, these should return
    `NotImplemented` if they are not used.

    `experiment.V_osc_t_(t_)`: `experiment.V_osc`

    In addition, the following attributes are expected in experiments:

    `experiment.t_unit`:
       All experiment times are specified in this unit `t_ = t/t_unit`.
    `experiment.t__final`:
       For `t_ > self.experiment.t__final`, all the potentials are set to
       zero.
    `experiment.t__image`:
       If imaging is performed (see `image_ts_` in the Simulation class), then
       expansion occurs for this length of time (previously this was
       `t__expand`).
    """

    experiment = None

    def get_mu_from_V_TF(self, V_TF):
        """Return the corrected chemical potential from V_TF.

        Arguments
        ---------
        V_TF : float
           External potential at the Thomas Fermi "radius".  (The external
           potential is evaluated at this position and this is used to get
           `mu`.)
        """
        mu = V_TF
        if self.experiment.basis_type == "1D" and not self.experiment.tube:
            # 1D bases represent homogeneous transverse dimensions so have no
            # hbar*omega corrections.
            pass
        else:
            w_perp = gmean(self.get_ws_perp(t=0))
            mu += self.hbar * w_perp

        return mu

    def get_V_TF_from_mu(self, mu):
        """Return V_TF from the chemical potential mu.

        Arguments
        ---------
        mu : float
           Physical chemical potential (i.e. what you would pass to the
           minimizer).
        """
        V_TF = mu
        return V_TF

    ######################################################################
    # Required by base State classes.
    def get_Vext(self, mu=None, fiducial=False, expt=False):
        """Return the external potential `V_ext`.

        This method just delegates to the experiment, and provides some simple
        memoization for performance.

        Arguments
        ---------
        mu : float, None
           If None, then subtract `self.mu`.  This will minimize the phase
           rotations during time evolution, but is undesirable when computing
           initial states.  In the latter case, set `mu=0`.
        """
        if fiducial or expt:
            return self.experiment.get_Vext(state=self, fiducial=fiducial, expt=expt)
        if mu is None and hasattr(self, "mu"):
            mu = self.mu
        if (
            self._Vext
            and self._Vext[1] is not None
            and (self.t == self._Vext[0] or self._time_independent_Vext)
        ):
            Vext = self._Vext[1]
        else:
            Vext = self.experiment.get_Vext(state=self)
            self._Vext = [self.t, Vext]

        if mu is not None:
            Vext = Vext - mu

        return Vext

    ######################################################################
    # Initial State
    #
    # These functions differ from the underlying bec2 versions implementing the
    # protocol we discuss in the introduction.
    def get_ns_TF(self, V_TF, fiducial=False, expt=False):
        """Return the TF densities."""
        g = self.get("g")
        V = self.get_Vext(fiducial=fiducial, expt=expt)
        n = self.get_n_TF(V_TF=V_TF, V_ext=V, g=g)

        # Here we add a full array of zeros to make sure that V_ext is full
        # size.  (Sometimes this might try to save memory by returning an
        # object like 0.0 that does not have full shape, but here we are
        # initializing the state, so we should make sure it gets expended.
        zero = np.zeros(self.shape)

        return zero + n

    def set_initial_state(self, V_TF=None, fiducial=False, expt=False):
        """Set the state using the Thomas Fermi (TF) approximation.

        This will overload the base class versions to pass the `fiducial` and
        `expt` arguments to the experiments.  See `get_fiducial_V_TF()` for
        details about the meaning of these parameters.  This also sets more
        appropriate phases for the state.

        Arguments
        ---------
        V_TF : float
           Value of the external potential at the Thomas-Fermi "radius".  Note:
           this is the chemical potential without any corrections from the tube
           or SOC and is applied directly to the external potential to get the
           densities.  These are then transformed into components using the
           spin-quasimomentum map.
        fiducial : bool
           If True, then use the fiducial initial state - i.e. do not
           include the barrier potential.
        expt : bool
           If True, then initialize the state using experimental
           parameters.  This is needed, for example, if the desired
           state is homogeneous (`wx=0`) but we want to initialize the
           state with the experimental `x_TF` parameter.  the
           `expt=True` flag should be used when initializing a
           (typically) larger state with `Lx=Lx_expt` and frequencies
           `wx=wx_expt` to compute the `fiducial_V_TF` in the presence
           of a barrier potential.
        """
        if V_TF is None:
            if hasattr(self.experiment, "_computing_fiducial_V_TF"):
                # This happens from within get_fiducial_V_TF() so we cannot
                # call that here.  Hence, we just return zero.
                self.data[...] = 0
                self._N = 0
                return
            V_TF = self.experiment.fiducial_V_TF

        ns = self.get_ns_TF(V_TF=V_TF, fiducial=fiducial, expt=expt)

        self.data[...] = np.sqrt(ns)
        self._N = self.get_N()

    ######################################################################
    # Required for using tube codes.
    def get_ws_perp(self, t=None):
        """Return the frequencies at time `t`.  Required by tube2 classes."""
        if t is None:
            t = self.t
        ws_perp = self.experiment.get("ws", t_=t / self.experiment.t_unit)[1:]
        if t <= self.t_final:
            return ws_perp
        else:
            return np.zeros_like(ws_perp)

    ######################################################################
    # The following are for user convenience only.  They should not be used
    # internally.
    @property
    def t_unit(self):
        """Get the time unit from the experiment."""
        # Convenience method
        return self.experiment.t_unit

    @property
    def t_final(self):
        # Convenience access to expansion time.
        return self.experiment.t__final * self.experiment.t_unit

    @property
    def g(self):
        """Get gs from experiment so it can manipulate them."""
        return self.get("g")

    @g.setter
    def g(self, g):
        assert np.allclose(self.g, self.experiment.g)

    @property
    def ws(self):
        """Get ws from experiment so it can manipulate them."""
        return self.get("ws")

    @ws.setter
    def ws(self, ws):
        assert np.allclose(self.ws, self.experiment.ws)

    def get(self, name, t_=None):
        """Return the value of the parameter `name` at time `self.t`.

        This just delegates to `self.experiment.get()`.
        """
        if t_ is None:
            t_ = self.t / self.t_unit
        return self.experiment.get(name, t_=t_)

    def get_density_x(self):
        """Return the density along x.

        For periodic boxes, this is the average density in the transverse plane
        (which is the central density if the transverse plane is
        translationally invariant) and the integrated 1D line density for tube
        and axial codes.
        """
        if self.experiment.basis_type == "axial":
            n = axial.StateAxial.get_density_x(self)
        elif self.experiment.basis_type in set(["2D", "3D"]):
            n = self.get_density()
            while len(n.shape) > 1:
                n = n.mean(axis=-1)
        else:
            n = self.get_density()

    ######################################################################
    # Plotting
    #
    # Here we provide various stack-able plotting components.  Each plot
    # function takes three arguments:
    #
    # data : PlotData
    #    Result of get_plot_data() with the data to be plotted.
    # grid : MPLGrid
    #    If the plot is being generated from scratch, then this will be
    #    provided, and should be used to get the axis for plotting.  New axes
    #    can be generated with `grid.next(size)` where size represents the
    #    relative size of the axis (default is 1).  If more control is needed,
    #    one can generate a subgrid with `grid.grid(size)`.
    #
    #    If `grid` is not provided, then it is expected that the plot function
    #    saved the axes in `plot_elements` during a previous call and those
    #    elements should be used.  If `plot_elements.ax` is set, then the
    #    decorators will automatically make this active with
    #    `plt.sca(plot_elements.ax)`.  If possible, previous elements should be
    #    updated rather than redrawn for performance, but if needed `ax.cla()`
    #    can be used.  (Do not use `plt.clf()` as there may be other plot
    #    elements.)
    # plot_elements : Namespace
    #    Each plot function should store their plot elements in this
    #    namespace.  If the namespace passed in contains previously stored
    #    elements, then these should be updated instead if possible to speed
    #    the drawing of the plot.  The latter feature is used when making
    #    movies and animations.  In general, when updating, the ranges should
    #    *not* be adjusted so that movies and animations are smooth.
    #
    #    If the plot is being generated as a subplot, then the plot_elements
    #    object will contain the following which should be used:
    #
    #    fig : Figure
    #       Matplotlib figure instance.

    @attr.s
    class PlotElements:
        """Collection of elements for redrawing a plot."""

        fig = attr.ib(None)
        densities = attr.ib(attr.Factory(Namespace))
        history = attr.ib(attr.Factory(Namespace))
        master_grid = attr.ib(None)

        def _repr_html_(self):
            """Provide display capability in IPython."""
            from IPython.display import display

            return display(self.fig)

    def plot(
        self,
        plot_elements=None,
        fig=None,
        data=None,
        grid=None,
        show_n=True,
        show_na=True,
        show_nb=True,
        show_log_n=True,
        show_momenta=False,
        show_mixtures=False,
        show_V=False,
        show_history_ab=True,
        show_history_a=False,
        show_history_b=False,
        show_log_history=False,
        combine_momenta_and_history=False,
        dynamic_range=100,
        log_dynamic_range=5,
        parity=False,
        # kwargs for get_momenta_data
        clip_momenta_modes=1,
        k_range_k_r=(-2.5, 2.5),
        nk_amplitude_factor=0.5,
        mu_background=None,
        symmetric=True,
        fig_width=15,
        pane_height=2,
        space=0.1,
        history=None,
        split=False,
        rescale=False,
        plot_dim=None,
        subplot_spec=None,
        # These are full-panel components.
    ):  # pragma: nocover
        """Plot the state.

        Parameters
        ----------
        data : PlotData
           Instance returned by self.get_plot_data() of the data to be plotted.
        plot_elements : PlotElements
           If defined, then update the plot here (faster, but will not rescale).
        history : [State]
           List of states defining the frames.

        parity : bool
           If `True`, then plot abs(x) on the abscissa of the density plots to
           show parity violations.

        clip_momenta_modes : int
           Clip this many momenta modes (peaks).  If most of the cloud occupies
           a single background momentum state, then there will be a large peak
           that obscures features.  This many peaks will be clipped.

        plot_dim : None, int
           If provided, then try to reduce plots to this dimension.  If None,
           then use self.dim.  This is particularly useful for plot_dim = 1
           which will plot the line density along the x axis, averaging or
           integrating over the other dimensions.

        combine_momenta_and_history : bool
           If True, then combine the momenta and history plots in a single pane.

        The following "boolean" flags can actually be numbers which will
        specify the relative height of the corresponding pane in the plot:

        show_n, show_mixtures, show_momenta, show_history_ab, show_history_a,
        show_history_b

        """
        from matplotlib import pyplot as plt

        if isinstance(fig, self.PlotElements):
            # Backwards compatibility
            fig, plot_elements = None, plot_elements

        if plot_elements is None:
            pe_ = self.PlotElements(fig=fig)
        elif isinstance(plot_elements, self.PlotElements):
            pe_ = plot_elements
        elif fig is None:
            # Assume a fig was passed instead (old behaviour)
            pe_ = self.PlotElements(fig=plot_elements)
            plot_elements = None

        fig = pe_.fig

        if data is None:
            get_momenta_kw = dict(
                k_range_k_r=k_range_k_r,
                clip_momenta_modes=clip_momenta_modes,
                log_dynamic_range=log_dynamic_range,
                mu_background=mu_background,
            )
            data = self.get_plot_data(
                states=history,
                dynamic_range=dynamic_range,
                get_momenta_data=show_momenta,
                **get_momenta_kw,
            )

        # from mmfutils.plot import imcontourf

        show_histories = (
            show_history_ab + show_history_a + show_history_b + show_log_history
        )

        if plot_dim is None:
            if show_histories:
                dim = 1
            else:
                dim = self.dim
        else:
            dim = plot_dim

        if dim == 1:
            if not history:
                show_histories = show_log_history = False
                show_history_ab = show_history_a = show_history_b = False

            if show_histories and show_momenta and combine_momenta_and_history:
                show_momenta_history = max(show_momenta, show_histories)
            else:
                show_momenta_history = show_momenta + show_histories

            if fig is None:
                panes = sum(
                    [
                        not split and (show_n or show_na or show_nb),
                        split and show_n,
                        split and show_na,
                        split and show_nb,
                        show_mixtures,
                        show_momenta_history,
                    ]
                )

                fig = pe_.fig = plt.figure(figsize=(fig_width, pane_height * panes))

            if split:
                show_densities = show_n + show_na + show_nb + show_mixtures
            else:
                show_densities = max(show_n, show_na, show_nb) + show_mixtures

            density_grid = None
            momenta_grid = None
            history_grid = None
            if not plot_elements:
                # Allocate all grids:
                master_grid = MPLGrid(direction="down", share=False, fig=fig, space=space)
                density_grid = master_grid.grid(show_densities)
                if combine_momenta_and_history:
                    momenta_history_grid = master_grid.grid(
                        show_momenta_history, direction="right"
                    )
                    momenta_grid = momenta_history_grid.grid()
                    history_grid = momenta_history_grid.grid()
                else:
                    momenta_grid = master_grid.grid(show_momenta)
                    history_grid = master_grid.grid(show_histories)
                pe_.master_grid = master_grid

            with NoInterrupt(ignore=False):
                if show_densities:
                    self.plot_densities(
                        data=data,
                        plot_elements=pe_.densities,
                        grid=density_grid,
                        show_n=show_n,
                        show_na=show_na,
                        show_nb=show_nb,
                        show_mixtures=show_mixtures,
                        show_V=show_V,
                        show_log_n=show_log_n,
                        split=split,
                        parity=parity,
                        symmetric=symmetric,
                        dynamic_range=dynamic_range,
                        log_dynamic_range=log_dynamic_range,
                    )
                if show_momenta:
                    self.plot_momenta(
                        data=data,
                        plot_elements=pe_.momenta,
                        grid=momenta_grid,
                        amplitude_factor=nk_amplitude_factor,
                    )
                if show_histories:
                    self.plot_history(
                        data=data,
                        plot_elements=pe_.history,
                        grid=history_grid,
                        show_history_ab=show_history_ab,
                        show_history_a=show_history_a,
                        show_history_b=show_history_b,
                        show_log_history=show_log_history,
                    )

                E = self.get_energy()
                N = self.get_N()
                if self.single_band:
                    msg = "t={:.4g}t0, N={:.4g}, E={:.4g}".format(self.t / self.t0, N, E)
                else:
                    Na, Nb = self.get_Ns()
                    msg = "t={:.4g}t0, Ns={:.4g}+{:.4g}={:.4g}, E={:.4g}".format(
                        self.t / self.t0, Na, Nb, N, E
                    )

                if not plot_elements:
                    pe_.title = plt.suptitle(msg)

                    # Add space for title:
                    # https://stackoverflow.com/a/45161551
                    plt.tight_layout(rect=[0, 0.03, 1, 0.95])
                else:
                    pe_.title.set_text(msg)

            if rescale:
                pe_.master_grid.rescale(tight=None, scalex=True, scaley=True)
            return pe_
        elif dim == 2:
            from mmfutils import plot as mmfplt

            if fig is None:
                fig = plt.figure(figsize=(10, 5))

            x, y = self.xyz[:2]

            plt.subplot(121)
            z = self[0]
            vmax = 1.1 * abs(self[...]).max()
            mmfplt.imcontourf(x, y, mmfplt.colors.color_complex(z, vmax=vmax), aspect=1)
            mmfplt.phase_contour(x, y, z, aspect=1, linewidths=0.5)

            plt.subplot(122)
            z = self[1]
            mmfplt.imcontourf(x, y, mmfplt.colors.color_complex(z, vmax=vmax), aspect=1)
            mmfplt.phase_contour(x, y, z, aspect=1, linewidths=0.5)
        else:
            raise NotImplementedError("Only 1D and 2D plotting supported")

        E = self.get_energy()
        N = self.get_N()
        if self.single_band:
            plt.suptitle("t={}t0, N={:.4f}, E={:.4f}".format(self.t / self.t0, N, E))
        else:
            Na, Nb = self.get_Ns()
            plt.suptitle(
                "t={}t0, Ns={:.4f}+{:.4f}={:.4f}, E={:.4f}".format(
                    self.t / self.t0, Na, Nb, N, E
                )
            )
        pe_.fig = fig
        return pe_

    def plot_densities(
        self,
        data,
        plot_elements,
        grid=None,
        split=False,
        show_n=True,
        show_na=True,
        show_nb=True,
        show_mixtures=False,
        show_log_n=False,
        show_V=False,
        parity=False,
        symmetric=False,
        dynamic_range=100,
        log_alpha=0.3,
        log_dynamic_range=5,
    ):
        # Determine plot limits.
        xs = np.ma.MaskedArray(data.xs, mask=data.mask)
        log_xs = np.ma.MaskedArray(data.xs, mask=data.log_mask)

        if grid:
            # Drawing for the first time.

            # Record these to ensure that updates make sense
            plot_elements.show_n = show_n
            plot_elements.show_na = show_na
            plot_elements.show_nb = show_nb
            plot_elements.show_log_n = show_log_n
            plot_elements.show_V = show_V
            plot_elements.show_mixtures = show_mixtures

            x_min = xs.min()
            x_max = xs.max()

            x_log_min = log_xs.min()
            x_log_max = log_xs.max()

            if symmetric:
                x_min = min(x_min, -x_max)
                x_max = max(x_max, -x_min)
                x_log_min = min(x_log_min, -x_log_max)
                x_log_max = max(x_log_max, -x_log_min)

            if split:
                size = show_n + show_na + show_nb + show_mixtures
                grid = grid.grid(size, direction="down", share=True)
                ax_n = grid.next(show_n)
                ax_na = grid.next(show_na)
                ax_nb = grid.next(show_nb)
            else:
                n_size = max(show_n, show_na, show_nb)
                size = n_size + show_mixtures
                grid = grid.grid(size, direction="down", share=True)
                ax = grid.next(n_size)
                ax_n = show_n and ax
                ax_na = show_na and ax
                ax_nb = show_nb and ax

            l = plot_elements.lines = []
            cs = plot_elements.colors = []
            for _ax, _n, _label, _ls in [
                (ax_n, data.n, "a+b", ("-", "--")),
                (ax_na, data.na, "a", ("--", "-.")),
                (ax_nb, data.nb, "b", ("--", "-.")),
            ]:
                if _ax:
                    ax = _ax
                    if parity:
                        left, right = np.where(xs <= 0), np.where(xs >= 0)
                        l.extend(
                            _ax.plot(
                                -xs[left], _n[left], _ls[0], label=_label + " (left)"
                            )
                        )
                        cs.append(l[-1].get_c())
                        l.extend(
                            _ax.plot(
                                xs[right],
                                _n[right],
                                _ls[1],
                                label=_label + " (right)",
                                c=cs[-1],
                            )
                        )
                        _ax.set_xlim(0, x_max, auto=None)
                    else:
                        l.extend(_ax.plot(xs, _n, _ls[0], label=_label))
                        cs.append(l[-1].get_c())
                        _ax.set_xlim(x_min, x_max, auto=None)
                    if _label == "a+b":
                        _ax.set_ylim(*data.n_lim, auto=None)
                    _ax.set_ylabel("n")

            if not split:
                ax.legend()

            if show_log_n:
                l = plot_elements.lines_log = []
                ax_log = None
                cs = list(plot_elements.colors)
                for _ax, ys in [
                    (ax_n, data.log_n),
                    (ax_na, data.log_na),
                    (ax_nb, data.log_nb),
                ]:
                    if _ax:
                        c = cs.pop(0)
                        if not ax_log or split:
                            ax_log = _ax.twinx()
                            ax_log.set_ylim([-log_dynamic_range, 1], auto=None)
                            plt.ylabel("log10(n/n_max)")

                        args = dict(c=c, alpha=log_alpha)
                        if parity:
                            l.extend(ax_log.plot(-xs[left], ys[left], "-", **args))
                            l.extend(ax_log.plot(xs[right], ys[right], "-.", **args))
                        else:
                            l.extend(ax_log.plot(xs, ys, "-", **args))

            if show_mixtures:
                grid.size = show_mixtures
                plot_elements.mixtures = Namespace()
                self.plot_mixtures(
                    data=data,
                    plot_elements=plot_elements.mixtures,
                    grid=grid,
                    show_V=show_V,
                )
                ax = plot_elements.mixtures.ax

            ax.set_xlabel("x [micron]")

        else:
            # Just update lines
            xs = data.xs
            ls = list(plot_elements.lines)
            if parity:
                left, right = np.where(xs <= 0), np.where(xs >= 0)
                if plot_elements.show_n:
                    ls.pop(0).set_data(-xs[left], data.n[left])
                    ls.pop(0).set_data(xs[right], data.n[right])
                if plot_elements.show_na:
                    ls.pop(0).set_data(-xs[left], data.na[left])
                    ls.pop(0).set_data(xs[right], data.na[right])
                if plot_elements.show_nb:
                    ls.pop(0).set_data(-xs[left], data.nb[left])
                    ls.pop(0).set_data(xs[right], data.nb[right])
                if plot_elements.show_log_n:
                    ls = list(plot_elements.lines_log)
                    if plot_elements.show_n:
                        ys = data.log_n
                        ls.pop(0).set_data(-xs[left], ys[left])
                        ls.pop(0).set_data(xs[right], ys[right])
                    if plot_elements.show_na:
                        ys = data.log_na
                        ls.pop(0).set_data(-xs[left], ys[left])
                        ls.pop(0).set_data(xs[right], ys[right])
                    if plot_elements.show_nb:
                        ys = data.log_nb
                        ls.pop(0).set_data(-xs[left], ys[left])
                        ls.pop(0).set_data(xs[right], ys[right])
            else:
                left, right = np.where(xs <= 0), np.where(xs >= 0)
                if plot_elements.show_n:
                    ls.pop(0).set_data(xs, data.n)
                if plot_elements.show_na:
                    ls.pop(0).set_data(xs, data.na)
                if plot_elements.show_nb:
                    ls.pop(0).set_data(xs, data.nb)
                if plot_elements.show_log_n:
                    ls = list(plot_elements.lines_log)
                    if plot_elements.show_n:
                        ls.pop(0).set_data(xs, data.log_n)
                    if plot_elements.show_na:
                        ls.pop(0).set_data(xs, data.log_na)
                    if plot_elements.show_nb:
                        ls.pop(0).set_data(xs, data.log_nb)

            if plot_elements.show_mixtures:
                self.plot_mixtures(data=data, plot_elements=plot_elements.mixtures)
        return plot_elements

    @property
    def time_dependent_quantities(self):
        time_dependent_quantities = []
        for name in self.experiment.time_dependent_parameters:
            method = self.experiment.time_dependent_parameters[name]
            info_name = name + "_info"
            unit, label = getattr(self.experiment, info_name)
            time_dependent_quantities.append((method, unit, label))
        return time_dependent_quantities

    def plot_time_dependent_parameters(
        self, fig=None, subplot_spec=None, share=False
    ):  # pragma: nocover
        """Plot all the time-dependent parameters stacked on top of each
        other.  Used as part of other plotting routines.
        """
        from gpe.plot_utils import MPLGrid

        ts_ = np.linspace(0, self.experiment.t__final, 200)
        t_ = self.t / self.t_unit

        grid = MPLGrid(fig=fig, subplot_spec=subplot_spec, right=True, hspace=0.0)

        for _n, (_f_t_, _unit, _label) in enumerate(self.time_dependent_quantities):
            ax = grid.next(share=share)
            share = True
            ys = np.array(map(_f_t_, ts_)) / _unit
            ls = ax.plot(ts_, ys)
            ys = np.ravel(_f_t_(t_)) / _unit  # Ravel to make scalars a 1D array
            for _y, _l in zip(ys, ls):
                ax.plot(t_, _y, "o", c=_l.get_c())
            ax.set_ylabel(_label)
        ax.set_xlabel("t [ms]")
        return ax

    def plot_mixtures(
        self, data, plot_elements, grid=None, dynamic_range=100, show_V=False
    ):
        """Plot the occupation of the two bands."""
        x = data.xs

        n_p, n_m = self.get_branch_mixture()
        y_m = np.ma.masked_array(n_m / (n_p + n_m), mask=data.mask)
        y_p = np.ma.masked_array(n_p / (n_p + n_m), mask=data.mask)

        if getattr(plot_elements, "show_V", show_V):
            Va, Vb, Vab = [
                np.ma.masked_array(_V, data.mask)
                for _V in self.get_Vext(single_band=False)
            ]
        if grid:
            # Drawing for the first time.
            ax = plot_elements.ax = grid.next()
            plot_elements.lines = ax.plot(x, y_m, "b-", label="Lower branch") + ax.plot(
                x, y_p, "r:", label="Upper branch"
            )
            ax.legend()

            plot_elements.show_V = show_V
            if show_V:
                ax_V = ax.twinx()
                zero = np.zeros_like(x)
                plot_elements.lines_V = ax_V.plot(
                    x, Va + zero, "b--", label="Va"
                ) + ax_V.plot(x, Vb + zero, "g--", label="Vb")
                ax_V.legend()

        else:
            plot_elements.lines[0].set_data(x, y_m)
            plot_elements.lines[1].set_data(x, y_p)
            if plot_elements.show_V:
                plot_elements.lines_V[0].set_data(x, Va)
                plot_elements.lines_V[1].set_data(x, Vb)

        return plot_elements

    def plot_history(
        self,
        data,
        plot_elements,
        grid=None,
        rescale=False,
        show_history_ab=True,
        show_history_a=False,
        show_history_b=False,
        show_log_history=False,
        dynamic_range=100,
        log_dynamic_range=5,
        **kw,
    ):
        """Show an imcountorf plot of of the states.

        Parameters
        ----------
        states : list
           List of states to show.
        rescale : bool
           If True, then scale each time-slice so the maximum is 1.
        """
        from matplotlib import pyplot as plt
        from mmfutils.plot import imcontourf

        ts = data.ts
        xs = data.xs
        t = data.t

        if not grid:
            # Just update the line positions
            for line in plot_elements.lines:
                line.set_data([[t] * 2, [xs.min(), xs.max()]])
            return plot_elements

        # Drawing for the first time.
        grid.share = True

        if rescale:
            ns = data.ns / data.ns.max(axis=1)[:, None]
            nas = data.nas / data.nas.max(axis=1)[:, None]
            nbs = data.nbs / data.nbs.max(axis=1)[:, None]
        else:
            ns, nas, nbs = data.ns, data.nas, data.nbs

        n_max = ns.max()

        x_log_min = np.ma.masked_array(xs, data.log_mask).min()
        x_log_max = np.ma.masked_array(xs, data.log_mask).max()
        x_min = np.ma.masked_array(xs, data.mask).min()
        x_max = np.ma.masked_array(xs, data.mask).max()

        histories = []
        if show_history_ab:
            histories.append((show_history_ab, ns, "$n$"))
        if show_history_a:
            histories.append((show_history_a, nas, "$n_a$"))
        if show_history_b:
            histories.append((show_history_b, nbs, "$n_b$"))

        plot_elements.lines = []
        plot_elements.imgs = []
        for _size, _ns, _label in histories:
            _grid = grid.grid(size=_size, direction="right", space=0)
            ax = _grid.next()
            plot_elements.imgs.append(imcontourf(ts, xs, _ns))
            ax.set_xlim([ts.min(), ts.max()], auto=None)
            ax.set_ylim([x_min, x_max], auto=None)
            plot_elements.lines.append(plt.axvline([data.t], c="r", ls=":"))
            plt.ylabel("x [micron]")
            ax.xaxis.set_visible(False)

            cax_hist = _grid.next(0.01)
            plt.colorbar(cax=cax_hist, label=_label)

        if show_log_history:
            _grid = grid.grid(size=show_log_history, direction="right", space=0)
            ax = _grid.next()
            plot_elements.imgs.append(
                imcontourf(ts, xs, np.log10(ns / n_max), vmin=-log_dynamic_range)
            )
            ax.set_xlim([ts.min(), ts.max()], auto=None)
            ax.set_ylim([x_log_min, x_log_max], auto=None)
            plot_elements.lines.append(plt.axvline([data.t], c="r", ls=":"))
            plt.ylabel("x [micron]")

            cax_log_hist = _grid.next(0.01)
            plt.colorbar(cax=cax_log_hist, label="log10(n/n_max)")

            ax.xaxis.set_visible(False)

        ax.xaxis.set_visible(True)
        ax.set_xlabel("t [ms]")

        plot_elements.ax = ax

        return plot_elements

    def get_momenta_data(
        self,
        clip_momenta_modes=1,
        log_dynamic_range=5,
        mu_background=None,
        k_range_k_r=(-2.5, 2.5),
    ):
        """Return (ks, Em, Ep, E_ph, nk, log_nk).

        Arguments
        ---------
        clip_momenta_modes : int
           Clip this many momenta modes (peaks).  If most of the cloud occupies
           a single background momentum state, then there will be a large peak
           that obscures features.  This many peaks will be clipped.
        log_dynamic_range : int
           Return the log of the density scaled so that 0 corresponds to a
           10**log_dynamic_range suppression.
        k_range_k_r : (float, float)
           Range of momenta (in units of k_r).
        mu_background: float, None
           Background chemical potential used for computing the phonon
           dispersion.  If this is None, the the maximum density will be used.

        Returns
        -------
        ks : array
           Momenta abscissa.
        Em, Ep : array
           Dispersion bands.
        Ph_m, Ph_p : array
           Phonon dispersions
        nk : array
           Momentum distribution ranging from 0 to the `Em.max()-Em.min()`.
           This is intended to be added to Em for plotting.
        log_nk : array
           Log of the momentum distribution ranging from 0 to the
           `Em.max()-Em.min()`.  This is intended to be added to Em for
           plotting.
        """
        Omega = self.get("Omega")
        detuning = self.get("detuning")
        k_r = self.get("k_r")
        E_R = self.get("E_R")

        d = detuning / 4 / E_R
        w = Omega / 4 / E_R

        if self.single_band:
            # Single-band models are already in the rotating phase basis.
            nk = np.fft.fftshift(abs(np.fft.fft(self[...], axis=0) ** 2))
        else:
            # Go to rotating phase basis
            ytilde = self[...] * np.exp(
                -1j * k_r * self.xyz[0] * np.array([1, -1])[self.bcast]
            )
            na_k, nb_k = np.fft.fftshift(abs(np.fft.fft(ytilde, axis=1) ** 2))
            nk = na_k + nb_k

        ka = np.fft.fftshift(self.basis.kx.ravel())

        def get_E(q):
            D = np.sqrt((q - d) ** 2 + w**2)
            Ep = (q**2 + 1) / 2.0 + D
            Em = (q**2 + 1) / 2.0 - D
            return Em, Ep

        km, kp = np.asarray(k_range_k_r) * k_r
        _ks = np.linspace(km, kp, 100)
        Em, Ep = get_E(_ks / k_r)
        Em_range = Em.max() - Em.min()
        inds = np.where(np.logical_and(km <= ka, ka <= kp))[0]
        ks = ka[inds]
        Em, Ep = get_E(ks / k_r)

        _inds = np.argsort(nk)
        k0 = ka[_inds[-1]]
        _inds = _inds[-clip_momenta_modes - 1 :]
        nk[_inds] = nk[_inds].min()

        log_nk = (
            np.maximum(np.log10(nk / nk.max()), -log_dynamic_range) + log_dynamic_range
        )

        nk *= Em_range / nk.max()
        log_nk *= Em_range / log_nk.max()
        nk = nk[inds]
        log_nk = log_nk[inds]

        if mu_background is None:
            # Guess background chemical potential
            mu_background = np.mean(self.gs) * self.get_density().max()

        # Compute phonon dispersion relationships
        def get_E_ph(k, k0, mu_=mu_background / 2 / E_R):
            q = k - k0
            Em_0, Ep_0 = get_E(k0)
            Em_p, Ep_p = get_E(k0 + q)
            Em_m, Ep_m = get_E(k0 - q)

            Em = (Em_p - Em_m) / 2.0
            Ep = (Em_p + Em_m - 2 * Em_0) / 2.0
            E_ph = Em + np.sqrt(Ep * (Ep + 2 * mu_))
            # E_ph_m = Em - np.sqrt(Ep*(Ep + 2*mu_))
            return E_ph + Em_0

        E_ph = get_E_ph(ks / k_r, k0=k0 / k_r)
        return self.MomentaData(ks, Em, Ep, E_ph, nk, log_nk, d, w)

    MomentaData = namedtuple(
        "MomentaData", ("ks", "Em", "Ep", "E_ph", "nk", "log_nk", "d", "w")
    )

    PlotData = namedtuple(
        "PlotData",
        (
            "frame",
            "frames",
            "xs",
            "ts",
            "t",
            "na",
            "nb",
            "n",
            "nas",
            "nbs",
            "ns",
            "log_na",
            "log_nb",
            "log_n",
            "mask",
            "log_mask",
            "n_lim",
            "state",
        )
        + MomentaData._fields,
    )

    def get_plot_data(
        self,
        states=None,
        frame=None,
        t_unit=u.ms,
        x_unit=u.micron,
        n_unit=1.0 / u.micron**3,
        dynamic_range=100,
        log_dynamic_range=5,
        get_momenta_data=False,
        **get_momenta_kw,
    ):
        """Return a PlotData namedtuple with the data needed for plotting.

        Arguments
        ---------
        states : None, [State]
           If a list of states is provided, then this is used to generate data
           for a history vs time contour plot.
        frame : int
           Frame when making movies.
        **get_momenta_kw :
           Additional keywords are passed to get_momenta_data.
        """
        nas = []
        nbs = []
        ns = []
        ts = []
        frames = None
        if states:
            frames = len(states)
            for state in states:
                na, nb = state.get_density_x()
                nas.append(na)
                nbs.append(nb)
                ts.append(state.t)
            nas, nbs, ts = map(np.array, [nas, nbs, ts])
            ts /= t_unit
            nas /= n_unit
            nbs /= n_unit
            ns = nas + nbs
            _n = ns.max(axis=0)
            n_min = ns.min()

            if frame is None:
                frame = states.index(self) if self in states else 0
            if states[frame] is self:
                na, nb = nas[frame], nbs[frame]
            else:
                na, nb = self.get_density_x()
                warn("Inconsistent frame={} found: self != states[frame]".format(frame))

        else:
            na, nb = self.get_density_x() / n_unit
            _n = na + nb
            n_min = _n.min()

        n_max = _n.max()
        mask = _n < n_max / dynamic_range
        log_mask = _n < n_max / 10**log_dynamic_range

        na = np.ma.MaskedArray(na, mask=mask)
        nb = np.ma.MaskedArray(nb, mask=mask)
        n = np.ma.MaskedArray(na + nb, mask=mask)
        log_na = np.ma.MaskedArray(np.log10(na / n_max), mask=log_mask)
        log_nb = np.ma.MaskedArray(np.log10(nb / n_max), mask=log_mask)
        log_n = np.ma.MaskedArray(np.log10(n / n_max), mask=log_mask)
        xs = self.xyz[0].ravel() / x_unit

        if get_momenta_data:
            momenta_data = self.get_momenta_data(**get_momenta_kw)
        else:
            momenta_data = self.MomentaData(*(None,) * len(self.MomentaData._fields))

        return self.PlotData(
            frame=frame,
            frames=frames,
            xs=xs,
            ts=ts,
            t=self.t / t_unit,
            na=na,
            nb=nb,
            n=n,
            nas=nas,
            nbs=nbs,
            ns=ns,
            log_na=log_na,
            log_nb=log_nb,
            log_n=log_n,
            mask=mask,
            log_mask=log_mask,
            n_lim=(n_min, n_max),
            state=self,
            **momenta_data._asdict(),
        )

    def plot_momenta(self, data, plot_elements, grid=None, amplitude_factor=0.5, ax=None):
        """Plot the dispersion and momentum distribution.  Return plot_elements.

        Arguments
        ---------
        amplitude_factor : float
           The occupation is shown added on top of the dispersion such that the
           maximum occupation is this fraction of the dispersion range.
           I.e. If amplitude_factor=0.5, then the maximum momentum occupation
           will be half of the dispersion range.
        """
        Em, Ep, E_ph = data.Em, data.Ep, data.E_ph
        ks_kr = data.ks / self.k_r
        _i = np.where(np.logical_and(ks_kr < 1.5, -2.5 < ks_kr))
        ylim = (min(Em.min(), E_ph[_i].min()), max(Em.max(), E_ph[_i].max()))

        if grid:
            # Drawing for the first time.
            ax = plot_elements.ax = grid.next()
            plot_elements.lines = (
                ax.plot(ks_kr, Em, "b-")
                + ax.plot(ks_kr, E_ph, "b:")
                + ax.plot(ks_kr, Ep, "r-")
                + ax.plot(ks_kr, Em + amplitude_factor * data.nk, "--")
                + ax.plot(ks_kr, Em + amplitude_factor * data.log_nk, ":y")
            )
            ax.set_ylim(ylim, auto=None)
            ax.set_xlabel("$k$ [$k_R$]")
        else:
            # Update existing plot
            lines = plot_elements.lines
            lines[0].set_data(ks_kr, Em)
            lines[1].set_data(ks_kr, E_ph)
            lines[2].set_data(ks_kr, Ep)
            lines[3].set_data(ks_kr, Em + amplitude_factor * data.nk)
            lines[4].set_data(ks_kr, Em + amplitude_factor * data.log_nk)

        return plot_elements

    def make_movie(
        self,
        filename,
        states,
        skip=1,
        fps=20,
        skip_frames=10,
        show_frames=2,
        dpi=None,
        display=True,
        extra_args=("-vcodec", "libx264", "-pix_fmt", "yuv420p"),
        **kw,
    ):
        """Make a movie of the states.

        Arguments
        ---------
        filename : str
           Name of movie file such as "movie.mp4".
        states : [State]
           List of states defining the frames.
        skip : int
           If this is >1, then skip this many states when making the movie.
           This allows one to have more states for smooth history plots, but
           speeds making the movie.
        fps : int
           Frames-per-second for output movie
        show_frames : int
           Display this many initial frames to aid debugging.
           Does not affect the generated movie.
        skip_frames : int
           After the initial frames, draw only these frames.  (Drawing frames
           greatly slows down the making of the movie so we limit the display).
           Does not affect the generated movie.
        display : bool
           If `False`, then do not display anything. (Use for offline generation
           of movies for example.)
        """
        if display:
            from IPython.display import display, clear_output

        from matplotlib import pyplot as plt
        from mmfutils.plot import animation

        movie_states = states[::skip]

        def get_data(states):
            if movie_states[-1] is not states[-1]:
                movie_states.append(states[-1])

            for frame, state in enumerate(movie_states):
                yield frame, state

        fig_ = [None]

        @coroutine
        def get_plot_data(fig_=fig_):
            tic = time.time()
            tocs = []

            plot_elements = states[0].plot(history=movie_states, **kw)
            fig_[0] = plot_elements.fig

            with NoInterrupt(ignore=True) as interrupted:
                while not interrupted:
                    frame, state = yield plot_elements.fig
                    frames = len(movie_states)
                    plot_elements = state.plot(
                        plot_elements=plot_elements, history=movie_states, **kw
                    )

                    tocs.append(time.time() - tic)
                    avg_time_for_frame = np.mean(np.diff(tocs))
                    time_to_completion = (frames - frame) * avg_time_for_frame
                    msg = "{} of {}: {:1g}s remaining ({}ms per frame)".format(
                        frame, frames, time_to_completion, avg_time_for_frame
                    )
                    if display:
                        if frame < show_frames or frame % skip_frames == 0:
                            clear_output(wait=True)
                            display(plot_elements.fig)
                        display(msg)
                    else:
                        if frame < show_frames or frame % skip_frames == 0:
                            logging.info(msg)

        self._l = locals()
        with get_plot_data(fig_) as plot_data:
            anim = animation.MyFuncAnimation(
                fig_[0], plot_data, get_data(states), save_count=len(states)
            )
            anim.save(filename, fps=fps, dpi=dpi, extra_args=extra_args)
        plt.close("all")


class State(StateMixin, bec.StateTwist_x):
    def __init__(self, experiment, **kw):
        self.experiment = experiment
        bec.StateTwist_x.__init__(self, **kw)

        self._time_independent_Vext = False
        self._Vext = [None, None]  # Cache for performance
        self._twist_phase_x = None


class StateTube(StateMixin, tube.StateGPEdrZ):
    def __init__(self, experiment, **kw):
        self.experiment = experiment
        tube.StateGPEdrZ.__init__(self, **kw)

        self._time_independent_Vext = False
        self._Vext = [None, None]  # Cache for performance


"""
class StateAxial(StateMixin, axial.StateAxial):    
    def __init__(self, experiment, **kw):
        self.experiment = experiment
        axial.StateAxial.__init__(self, **kw)

        self._time_independent_Vext = False
        self._Vext = [None, None]  # Cache for performance
"""


class ExperimentSOC(ExperimentCatchAndRelease):
    """Radiation Experiment.

    Motivated by experiment:
    https://arxiv.org/pdf/1805.08618.pdf


    Attributes
    ----------
    t__wait : float
       If this is `inf`, then we start in the ground state including the
       attractive central beam, otherwise, we start in the ground state without
       the central beam, and then wait for this time while the central beam is
       on before turning it off.

    """

    states = ((1, -1), (1, 0))
    x_TF = 10 * u.micron
    t_unit = u.ms

    B_gradient_mG_cm = 0  # Magnetic field gradients are on the order of 5-10 mG/cm
    recoil_frequency_Hz = 1843.0
    rabi_frequency_E_R = 0.0
    detuning_kHz = 0.0
    trapping_frequencies_Hz = (3.07, 278, 278)

    initial_imbalance = 0.0  # Nothing RF transfered, all in state[...][0]

    bucket = True
    # gaussian = True               #Guassian vs Harmonic
    omega_bucket_ms = 1.0  # Bucket Oscillation period
    number_of_oscillations = 0.5  # How many time the bucket sweeps back and forth
    barrier_width_micron = 2.4  # Bucket width
    barrier_depth_nK = -200  # Bucket depth

    # t__barrier = 0
    t__step = 0.1  # Time to turn on barrier
    t__wait = 1.0  # Time until expansion
    t__image = 10.1

    def bucket_position_t_(self, t_=0):
        """Constant velocity from t_=0 to t_=t__bucket."""
        # t_ = state.t / self.t_unit
        x0 = self.x_TF
        omega = self.omega_bucket
        if t_ <= 0:
            return x0
        elif t_ > 0:
            return B * np.cos(t_ * 2.0 * np.pi / omega)

    @staticmethod
    def plot1(
        state,
        fig=None,
        subplot_spec=None,
        parity=False,
        a=False,
        b=False,
        show_mixtures=False,
        show_momenta=False,
        history=None,
    ):  # pragma: nocover
        from matplotlib import pyplot as plt
        from gpe.plot_utils import MPLGrid

        if fig is None:
            fig = plt.figure(figsize=(15, 5))

        fig.clf()

        grid = MPLGrid(fig=fig, subplot_spec=subplot_spec, right=False)

        state.plot_densities(
            grid=grid, split=False, show_V=False, parity=parity, a=a, b=b
        )
        Va, Vb = state.experiment.get_Vt(state=state)
        plt.legend()
        x = state.xyz[0]
        if parity:
            x = abs(x)

        plt.twinx()
        plt.plot(x, Va, ":y")
        plt.plot(x, Vb, "--y")

        if show_mixtures:
            state.plot_mixtures(ax=grid.next())

        if show_momenta:
            state.plot_momenta(ax=grid.next())

        if history is not None:
            state.plot_history(ax=grid.next(), states=history)

        plt.tight_layout()

        E = state.get_energy()
        Na, Nb = state.get_Ns()
        N = Na + Nb
        plt.suptitle(
            "t={}t0, Ns={:.4f}+{:.4f}={:.4f}, E={:.4f}".format(
                state.t / state.t0, Na, Nb, N, E
            )
        )
        return fig, grid

    def init(self):
        self.barrier_depth = self.barrier_depth_nK * u.nK
        self.barrier_width = self.barrier_width_micron * u.micron

        self.t__bucket_on = self.omega_bucket_ms * u.ms * self.number_of_oscillations
        self.t__final = self.t__bucket_on + self.t__wait

        self.barrier_depth_t_ = utils.get_smooth_transition(
            [0, self.barrier_depth, 0.0],
            durations=[0, self.t__bucket_on - 2 * self.t__step],
            transitions=[self.t__step, self.t__step],
        )
        Experiment.init(self)
