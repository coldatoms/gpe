import numpy as np
from gpe import bec, utils, minimize

from pytimeode import evolvers


u = bec.u


class State(bec.StateWithExperimentMixin, bec.StateTwist_x):
    def _set_initial_state(self, mu=None, x_TF=None):
        super().set_initial_state(mu=mu, x_TF=x_TF)
        self.set_psi(self.twist_phase_x * self.get_psi())

    ######################################################################
    # Helper functions
    def get_current(self):
        n = self.get_density()
        data = self.get_data()
        j = (data.conj()*self.basis.get_gradient(data)[0]).imag
        return self.hbar * (j + n*self.k_B)

    
@utils._GPU.add_non_GPU_methods
class Experiment(utils.ExperimentBase):
    hbar = m = 1.0
    healing_length_micron = 1.0
    dx_healing_length = 0.5
    Lx_micron = 10.0
    n0 = 1.0
    lattice_cells = 1
    lattice_V_mu = 0.1

    twist = 0.0
    v_x_c = 0.0   # Flow speed in units of c - moving lattice
    fix_N = True
    
    
    State = State
    
    def init(self):
        healing_length = self.healing_length_micron * u.micron
        dx = self.dx_healing_length * healing_length
        Lx = self.Lx = self.Lx_micron * u.micron
        Nx = utils.get_good_N(Lx/dx)
        self.mu = (self.hbar/healing_length)**2/2/self.m
        g = self.mu / self.n0
        c = np.sqrt(self.mu / self.m)  # Speed of sound
        self.N = self.n0 * Lx
        self.state_args = dict(Lxyz=(Lx,), Nxyz=(Nx,), g=g, hbar=self.hbar, m=self.m,
                              twist=self.twist, v_x=c * self.v_x_c)
        
        self.kL = 2*np.pi / self.lattice_cells / Lx
        self.VL = self.lattice_V_mu * self.mu
        
    def get_Vext_GPU(self, state):
        """Return the external potential."""
        x = state.get_xyz_GPU()[0]
        V_ext = self.VL * (1-state.xp.cos(self.kL * x))/2

        if (state.initializing or state.t < 0) and getattr(self, "mu", None):
            V_ext -= self.mu

        return V_ext

    def get_Vint_GPU(self, state):
        """Return the "internal" mean-field potential.

        This version implements the standard GPE where the
        energy-density has $gn^2/2$, so we have the derivative `gn` here.
        """
        n = state.get_density_GPU()
        if state.PGPE and state.basis.smoothing_cutoff != 0.5:
            n = state.basis.smooth(n)
        V_int = state.g * n
        return V_int

    def get_Eint(self, state):
        """Return the "internal" mean-field energy-density.

        This version implements the standard GPE where the
        energy-density has $gn^2/2$.  The method get_Vint() should
        return the appropriate derivative of this.
        """
        # TODO Make this work with PGPE.
        n = state.get_density()
        if state.PGPE and state.basis.smoothing_cutoff != 0.5:
            n = self.basis.smooth(n)
        return state.g * n**2 / 2.0
    
    def get_state(self, initialize=True):
        """Quickly return a valid `State` object."""
        state = self.State(experiment=self, **self.state_args)
        if self.fix_N:
            state *= np.sqrt(self.N/state.get_N())
        state.pre_evolve_hook()
        return state

    def _callback(self, state):
        state.initializing = False
        self._Es.append(state.get_energy())
        self._s = state
        
    def get_initial_state(self, _E_tol=np.nan, _psi_tol=1e-6, debug=False, **kw):
        """Return the valid `t=0` state to initialize the simulations."""
        state = self.get_state()
        state.initializing = True
        if debug:
            state *= np.exp(1j*2*np.pi * np.random.random(state.shape))
        minimizer = minimize.MinimizeState(state, fix_N=True)
        if debug:
            minimizer.check()
        self._Es = []
        state0 = minimizer.minimize(E_tol=_E_tol, psi_tol=_psi_tol, 
                                    use_scipy=True, 
                                    callback=self._callback, **kw)
        state = self.get_state()
        state.set_psi(state0.get_psi())
        self._minimize_results = minimizer.minimize_results
        if not self._minimize_results.success:
            print(self._minimize_results.message)
        return state

    
def angle(psi):
    """Smooth version of np.angle."""
    angle = [np.angle(psi[0])]
    for n in range(1, len(psi)):
        angle.append(angle[-1] + np.angle(psi[n]/psi[n-1]))
    return np.asarray(angle)
