"""Three component spinor BECs.

Simplifications:

* g_aa = g_bb = g_ab = g
* m_a = m_b = m
* Assume that only total particle number is conserved.
"""

import warnings

import numpy as np

from mmfutils.math.bases import PeriodicBasis
from mmfutils.contexts import NoInterrupt

from pytimeode import interfaces, mixins, evolvers
from zope.interface import implementer

from gpe import bec
from gpe.utils import AsNumpyMixin, _GPU
from gpe.interfaces import IStateMinimizeGPE

u = bec.u
u.m_Li7 = 7.01600343 * u.u

__all__ = ["State", "u"]

_TINY = np.finfo(float).tiny


@implementer(
    interfaces.IStateForABMEvolvers,
    interfaces.IStateWithNormalize,
    IStateMinimizeGPE,
)
@_GPU.add_non_GPU_methods
class State(bec.StateGPEBase):
    """Three component state.

    Arguments
    ---------
    basis : IBasis, None
       If provided, then this is used to specify the basis.  If not, then a
       periodic basis is constructed from `Nxyz` and `Lxyz`.
    constraint : 'N' | 'Nabc'
       If 'N', then constrain the total density allowing particles to convert
       from one species to the other.  If 'Nabc' Then independently constrain
       `Na`, `Nb`, and `Nb`.
    twist : (float, float)
       Twisted boundary conditions for species a and b.  This is the twist
       angle.
    x_TF : float
       Thomas Fermi "radius" for setting the initial state.  The initial state
       will be set so that the density will fall to zero at this point
       `x=x_TF` (with y and z in the middle of the trap).  If `None`, then we
       default to 80% of the length.
    """

    t0 = u.ms  # Base unit for times.

    def __init__(
        self,
        #
        # Specify either basis or the following
        Nxyz=(640, 640),
        Lxyz=(350 * u.micron, 350 * u.micron),
        symmetric_grid=False,
        #
        # Basis independent parameters
        t=0,
        hbar=u.hbar,
        #
        # Physical Parameters from paper
        n0=1.0,  # Density... not give, but sets units.
        # c_nn_h=150.0 * u.Hz,  # density-dependent interaction: Not specified!
        c_sn_h=-150.0 * u.Hz,  # spin-dependent interaction.
        p_h=0.0 * u.Hz,  # linear Zeeman coeffient: Not specified!
        q_h=-20.0 * u.Hz,  # quadratic Zeeman coeffient
        mu_h=310 * u.Hz,
        B_grad=9.0 * u.mG / u.cm,
        m=u.m_Li7,
        ws=2 * np.pi * np.array([7, 8, 680]) * u.Hz,
        cooling_phase=1.0,
        # In principle, different types of constraints might
        # be applied - such as fixed momentum, fixed angular
        # momentum etc.  Here we currently only support
        # constraint='N' or 'Nabc'
        constraint="N",
        x_TF=None,
        mu_Bs=(0 * u.mu_B, 0 * u.mu_B),  # Magnetic moments
        basis_args={},
    ):
        args = dict(Nxyz=Nxyz, Lxyz=Lxyz, symmetric_lattice=symmetric_grid)
        args.update(basis_args)
        self.basis = PeriodicBasis(**args)

        # Here is the data for the state.  The ArrayStateMixin class uses this
        # to provide most of the required functionality required by the IState
        # interface.  Use np.zeros here so that we don't get overflow errors.
        # (Using np.empty is slightly faster, but the call to get_N() may then
        # raise an overflow error.)
        self.data = self.xp.zeros((3,) + self.shape, dtype=complex)

        # We defer all other calculations to the init() method so that they
        # will use the current values of the various parameters.  This allows
        # the user to change the values of the parameters and they will take
        # effect the next time init() is called.
        self.t = t
        self.hbar = hbar
        self.m = m
        h = self.hbar * 2 * np.pi
        self.mu = mu_h * h
        self.p = p_h * h
        self.q = q_h * h
        self.c_s = c_sn_h * h * n0
        c_nn_h = self.mu / h
        self.c_n = c_nn_h * h * n0

        self.sigmas = np.zeros((3, 3, 3), dtype=complex)
        self.sigmas[0, ...] = np.array([[0, 1, 0], [1, 0, 1], [0, 1, 0]])
        self.sigmas[1, ...] = np.array([[0, -1j, 0], [1j, 0, -1j], [0, 1j, 0]])
        self.sigmas /= np.sqrt(2)
        self.sigmas[2, ...] = np.array([[1, 0, 0], [0, 0, 0], [0, 0, -1]])
        self.sigma2 = np.einsum("iab,icd", self.sigmas, self.sigmas)
        self.sigma2 += np.einsum("abcd->cbad", self.sigma2)
        self.healing_length_micron = 4.7  # Not given, but this is the wall thickness
        #

        x, y = self.get_xyz_GPU()
        wx, wy = ws[:2]
        self.U = self.m * ((wx * x) ** 2 + (wy * y) ** 2) / 2
        self.cooling_phase = cooling_phase

        # In principle, different types of constraints might be applied - such
        # as fixed momentum, fixed angular momentum etc.  Here we currently
        # only support constraint in ('N', 'Nabc').
        self.constraint = constraint
        self.x_TF = x_TF
        self.init()

        # Once the state is initialized, we can set the initial state.
        self.set_initial_state()

    def braket_GPU(self, y):
        """Return the dot product of `self.conj()` with `y`.

        Arguments
        ---------
        y : State
           The states for which the braket will be computed.
        """
        return self.xp.asarray(
            [
                (y1_ * self.metric).ravel().conj().dot(y2_.ravel())
                for y1_, y2_ in zip(self, y)
            ]
        )

    def init(self):
        """Initialize the state.

        This method defines the basis positions, momenta, etc. for use later
        on.  We define these here rather than in the constructor `__init__()`
        so that the user can change them later and the reinitialize the state.
        We also call this function from the `pre_evolve_hook()` so that it is
        called before any evolution takes place.  For this reason, we should
        not modify the state here.
        """
        self.mus = np.ones(3) * self.mu
        self.K_factor = -((self.hbar) ** 2) / 2.0 / self.m

        # Here we precompute the "phase" factor appearing in the GPE relating
        # H(psi) with dpsi/dt.  We include the value of hbar here and the
        # cooling_phase.  A potential optimization here is to allow the state
        # to be real if the cooling phase is purely imaginary
        cooling_phase = self.cooling_phase / abs(self.cooling_phase)
        self._phase = 1.0 / 1j / self.hbar / cooling_phase

        # We also record the current particle number so that normalize() can
        # restore it if requested during evolution.
        self._N = self.get_Ns_GPU()

        # Always call inherited init methods.
        super().init()

    @property
    def x(self):
        """Flat x abscissa as a numpy array."""
        return self.get_xyz()[0].ravel()

    def set_initial_state(self, mus=None, x_TF=None):
        """Set the state using the Thomas Fermi (TF) approximation from either
        `mus` or `x_TF` (pick only one or the other).

        Arguments
        ---------
        mus : (float, float, float)
           Fixed chemical potentials.
        x_TF : float
           Position defining the Thomas Fermi "radius".  (The external
           potential is evaluated at this position and this is used to
           set `mu`.)
        """
        if mus is not None and x_TF is not None:
            raise ValueError(f"Got both {mus=} and {x_TF=} (specify only one)")

        if mus is None and x_TF is None:
            mus = self.mus
            x_TF = self.x_TF

        if mus is None:
            if x_TF is None:
                x = self.xyz[0].ravel()
                x_TF = 0.2 * x[0] + 0.8 * x[-1]  # Choose point 80% along
            mus = self.get_mus_from_Vs_TF(Vs_TF=self.get_Vs_TF(x_TF=x_TF))

        Vs_TF = self.get_Vs_TF_from_mus(mus)
        ns = self.get_ns_TF(Vs_TF=Vs_TF)

        # Here we add a full array of zeros to make sure that V_ext is full
        # size.  (Sometimes this might try to save memory by returning an
        # object like 0.0 that does not have full shape, but here we are
        # initializing the state, so we should make sure it gets expended.
        ns = ns + np.zeros(self.shape)
        psi = np.sqrt(ns)
        Ly = self.basis.Lxyz[1] * 0.6
        x, y = self.xyz
        y = y.ravel()
        psi[1:, :, y > Ly / 6] *= 0
        psi[:2, :, y < -Ly / 6] *= 0
        psi[0, :, abs(y) < Ly / 6] *= 0
        psi[2, :, abs(y) < Ly / 6] *= 0
        self.set_psi(psi)
        self._N = self.get_Ns_GPU()

    def get_mus_from_Vs_TF(self, Vs_TF):
        """Return the corrected chemical potential from Vs_TF.

        In some cases, the chemical potential may differ from the value of the
        external potentials at V(x_TF) due to kinetic energy shifts (in the SOC
        case for example) or due to the energy of radial excitations (see the
        tube codes).  This function adds the appropriate correction.

        Arguments
        ---------
        Vs_TF : (float, float, float)
           External potentials at the Thomas Fermi "radius".  (The external
           potential is evaluated at this position and this is used to get
           `mus`.)
        """
        # Here we assume that self.mus is subtracted in get_Vext().
        mus = np.asarray(Vs_TF)
        if self.mus is not None:
            mus = mus + self.mus

        if not self.initializing:
            warnings.warn(
                f"In get_mus_from_Vs_TF() while not initializing: `{mus=}` might be incorrect."
            )
        return mus

    def get_Vs_TF_from_mus(self, mus):
        """Return Vs_TF from the chemical potentials mus.

        Arguments
        ---------
        mus : (float, float)
           Physical chemical potentials (i.e. what you would pass to the minimizer).
        """
        if not self.initializing:
            warnings.warn(
                f"In get_Vs_TF_from_mus() while not initializing: `{mus=}` might be incorrect."
            )
        Vs_TF = np.asarray(mus)
        # Here we assume that self.mu is subtracted in get_V().
        if self.mus is not None:
            Vs_TF = Vs_TF - self.mus
        return Vs_TF

    def get_n_TF(self, V_TF, V_ext=None, g=None):
        """Return the total TF density for a single component.

        Arguments
        ---------
        V_TF : float
           Value of V(x_TF) where the density should vanish in the TF limit.
        """
        if g is None:
            g = self.g
        if V_ext is None:
            V_ext = self.get_Vext()

        # In some applications, the external potential may be complex, so we
        # consider only the real part here
        V_ext = V_ext.real + np.zeros(self.shape)
        return np.maximum(0, V_TF - V_ext) / g

    def get_ns_TF(self, Vs_TF, V_ext=None):
        """Return the TF densities.  (Ignores `gab`.)"""
        if not self.initializing:
            warnings.warn(
                f"In get_ns_TF() while not initializing: `{Vs_TF=}` might be incorrect."
            )

        if V_ext is None:
            V_ext = self.get_Vext()
        g = self.c_n

        # Here we add a full array of zeros to make sure that V_ext is full
        # size.  (Sometimes this might try to save memory by returning an
        # object like 0.0 that does not have full shape, but here we are
        # initializing the state, so we should make sure it gets expended.
        zero = np.zeros(self.shape)

        try:
            V_TF_a, V_TF_b, V_TF_c = Vs_TF
        except (TypeError, ValueError):
            V_TF_a = V_TF_b = V_TF_c = Vs_TF

        ns = zero + [
            self.get_n_TF(V_TF=V_TF_a, V_ext=V_ext, g=g),
            self.get_n_TF(V_TF=V_TF_b, V_ext=V_ext, g=g),
            self.get_n_TF(V_TF=V_TF_c, V_ext=V_ext, g=g),
        ]

        return ns

    def get_Vs_TF(self, x_TF, V_ext=None):
        """Return the Thomas Fermi chemical potential at x_TF.

        Arguments
        ---------
        x_TF : float
           Position defining the Thomas Fermi "radius".  (The external
           potential is evaluated at this position and this is used to get `mu`.)
        """
        zero = np.zeros(self.shape)
        if V_ext is None:
            V_ext = self.get_Vext()
        Va, Vb, Vab = V_ext
        Va = Va + zero
        Vb = Vb + zero
        while 1 < len(Va.shape):
            Va = Va.min(axis=-1)
            Vb = Vb.min(axis=-1)
        x = self.xyz[0].ravel()
        i = np.argmin(abs(x - x_TF))
        inds = slice(i - 1, i + 2)
        order = min(len(x[inds]) - 1, 2)
        Vs_TF = [
            np.polyval(np.polyfit(x[inds], _V[inds], order), x_TF) for _V in [Va, Vb]
        ]
        return Vs_TF

    ######################################################################
    # User functions.
    # These can be overloaded by the user to implement different
    # potentials, functionals, etc.

    def get_Eint(self):
        """Return the "internal" mean-field energy-density."""
        na, nb = self.get_density()
        gaa, gbb, gab = self.gs
        Eint = gaa * na**2 / 2.0 + gbb * nb**2 / 2.0 + gab * na * nb
        return Eint

    # End user functions.
    ######################################################################

    ######################################################################
    # State functions and attributes
    #
    # These functions likely need to be modified by more complicated
    # state implementations.  See StateTwist_x below for an example.
    # In most cases, if these are properly defined, then the other
    # functions below will work properly.
    def get_xyz_GPU(self):
        return tuple(self.basis.xyz)

    @property
    def xyz(self):
        return self.get_xyz()

    def get_metric_GPU(self):
        return self.basis.metric

    def get_metric_GPU(self):
        return self.basis.metric

    @property
    def metric(self):
        return self.get_metric()

    def get_psi_GPU(self):
        """Return the physical wavefunction (applying any twist)."""
        return self.data

    def set_psi(self, psi):
        """Set the state from a physical wavefunction (removing any twist)."""
        self.set_data(psi)

    def get_density_GPU(self):
        psi = self.get_psi_GPU()
        return (psi.conj() * psi).real

    # End state functions
    ######################################################################

    @property
    def dim(self):
        """Dimension of the state."""
        return len(self.shape)

    @property
    def shape(self):
        """Shape of the state Nxyz (not including components)."""
        return tuple(self.basis.shape)

    @property
    def bcast(self):
        """Return a set of indices suitable for broadcasting masses etc."""
        return (slice(None),) + (None,) * self.dim

    @property
    def E_max(self):
        """Return the maximum kinetic energy in the basis.

        This is useful when using evolvers as convergence should be obtained
        when the time-step is roughly::

            dt = 0.1 * state.hbar / state.E_max

        See `t_scale`.
        """
        # The use of np.asarray().max() allows for the possibility that
        # self.basis.k_max is a float or an array (in 2D or 3D).  This
        # should be np.asarray, not self.xp.asarray since it needs to
        # support objects.
        return self.asnumpy(np.asarray(abs(self.K_factor) * self.basis.k_max**2).max())

    @property
    def t_scale(self):
        """Return the smallest time-scale for the problem.

        Evolvers - especially the ABM evolvers - should use a `dt=0.1*t_scale` or
        so.  If much smaller `dt` values are required for convergence, then it
        usually indicates that your lattice spacing is too larger.  Likewise,
        if you can get away with much larger `dt` values, then your lattice
        spacing might be unnecessarily small.

        Required by `Simulation`.
        """
        return self.hbar / self.E_max

    def get_Ns_GPU(self):
        return self.integrate_GPU(self.get_density_GPU())

    def get_N_GPU(self):
        return sum(self.get_Ns_GPU())

    def integrate_GPU(self, a):
        """Integrate over both components, but do not sum."""
        res = [(self.metric * self.xp.asarray(_a)).sum() for _a in a]
        return self.xp.asarray(res, dtype=res[0].dtype)

    def get_Vext_GPU(self):
        return self.U - self.mu

    ######################################################################
    # Required by interface IStateForABMEvolvers
    def compute_dy_dt(self, dy, subtract_mu=True):
        """Return `dy_dt` storing the results in `dy`.

        Arguments
        ---------
        subtract_mu : bool
           If `True`, then subtract the chemical potential such that `dy_dt` is
           orthogonal to the original state `y`.  This will minimize the
           evolution of the overall phase during real-time evolution (which can
           reduce numerical errors) and will ensure that evolution under
           imaginary or complex time will preserve particle number.

           This should not be set if computing physical energy of the state,
           however, which is why it is a parameter.
        """
        y = self
        Ky = y.copy()
        Ky.apply_laplacian(factor=self.K_factor)

        psi = y.get_psi_GPU()
        psic = psi.conj()
        n = np.einsum("a...,a...->...", psic, psi)
        Vpsi = (self.c_s / 2) * self.xp.einsum(
            "abcd,c...,d...,b...->a...", self.sigma2, psic, psi, psi
        )
        Vy = y.copy()
        Vy.set_data(Vpsi)

        Vy += (self.c_n * n[None, ...]) * psi
        sigma_z = self.sigmas[2]
        A = self.q * (sigma_z @ sigma_z) + self.p * sigma_z
        Vy += np.einsum("ab,b...->a...", A, psi)
        U = self.get_Vext_GPU()
        Vy += U[None, :] * psi
        Hy = Ky + Vy
        if subtract_mu:
            if self.constraint == "N":
                mu = y.braket_GPU(Hy).sum() / y.braket_GPU(y).sum()
            elif self.constraint == "Nabc":
                # We use _TINY to deal with special cases where
                # one component may be zero (which would lead to mu =
                # nan).
                mu = (y.braket_GPU(Hy) / (y.braket_GPU(y) + _TINY))[self.bcast]
            elif not self.constraint:
                mu = 0
            else:
                raise ValueError("constraint={} not recognized".format(self.constraint))
            # Check this - does not work if Vab != 0
            mu = mu.real
            Hy.axpy(y, -mu)
            self._mu = mu

        dy.copy_from(Hy)
        dy.scale(self._phase)
        return dy

    ######################################################################
    # Required by interface IStateWithNormalize
    #
    def normalize(self, s=None):
        """Normalize the state, return the scale factors and number `(s, N)`."""
        if self.constraint == "N":
            N = sum(self._N)
            if s is None:
                s = self.xp.sqrt(N / sum(self.get_Ns_GPU()))
        elif self.constraint == "Nabc":
            N = self._N
            if s is None:
                s = self.xp.sqrt((N + _TINY) / (self.get_Ns_GPU() + _TINY))[self.bcast]
            N = N[self.bcast]
        else:
            raise ValueError("constraint={} not recognized".format(self.constraint))
        self *= s
        if self.constraint == "N":
            assert self.xp.allclose(N, self.get_N_GPU())
        elif self.constraint == "Nabc":
            assert self.xp.allclose(N.ravel(), self.get_Ns_GPU().ravel())
        return s, N

    ######################################################################
    # Required by interface IStateGPE
    #
    def apply_laplacian(self, factor, exp=False, **_kw):
        """Apply the laplacian multiplied by `factor` to the state.

        Arguments
        ---------
        factor : array-like
           The result will be multiplied by this factor.
        exp : bool
           If `True` then `exp(factor*laplacian)(y)` will be computed instead.
        """
        # Note: twist_phase_x need not be applied since we store the
        # periodic function.
        extras = ["kx2", "kwz2"]
        for _k in extras:
            _v = getattr(self, _k, None)
            if _v is not None:
                _kw[_k] = _v
        self.data[...] = self.basis.laplacian(self.data, factor=factor, exp=exp, **_kw)

    ######################################################################
    # Required by IStateMinimizeGPE
    #
    def get_energy(self):
        E = sum(self.integrate([self.get_energy_density()]))
        assert np.allclose(0, E.imag / (np.abs(E) + _TINY))
        return E.real

    def get_Hy(self, subtract_mu=False):
        dy = self.empty()
        self.compute_dy_dt(dy=dy, subtract_mu=subtract_mu)
        Hy = dy / self._phase
        return Hy

    # End of interface definitions
    ######################################################################

    def get_energy_density(self):
        # Warning: this is not correct.  It may not be real until summed.  The
        # correct energy density requires abs(grad psi)^2
        y = self
        psi_a, psi_b, psi_c = psi = self.get_psi()
        psic = psi.conj()
        na, nb, nc = self.get_density()
        n = na + nb + nc
        Ky = y.copy()
        Ky.apply_laplacian(factor=self.K_factor)
        K = (psi.conj() * Ky.get_psi()).sum(axis=0)
        # gaa, gbb, gab = self.gs

        sx, sy, sz = self.xp.einsum("zab,a...,b...->z...", self.sigmas, psic, psi)
        s2 = sx**2 + sy**2 + sz**2
        Eint = (self.c_n * n**2 + self.c_s * s2) / 2 - self.p * sz + self.q * (na + nc)
        U = self.get_Vext_GPU()
        Vext = U * n
        return K + Eint + Vext

    ######################################################################
    # The remaining methods are not needed for evolution or ground state
    # preparation, but may be helpful for analysis.
    def get_mu(self):
        """Compute the chemical potential for convenience only."""
        y = self[...]
        dy = self.empty()
        self.compute_dy_dt(dy, subtract_mu=False)
        Hy = dy[...] / self._phase
        mu = sum(y.braket(Hy)) / sum(y.braket(y))
        assert np.allclose(0, mu.imag)
        return mu

    def plot(self, log=False):  # pragma: nocover
        from matplotlib import pyplot as plt
        from matplotlib.gridspec import GridSpec
        from mmfutils.plot import imcontourf

        na, nb, nc = self.get_density()
        if log:
            na, nb, nc = np.log10(na), np.log10(nb), np.log10(nc)

        if self.dim == 1:
            x = self.x / u.micron
            plt.plot(x, na, "b:")
            plt.plot(x, nb, "g:")
            plt.plot(x, na + nb, "k-")
        elif self.dim == 2:
            from mmfutils import plot as mmfplt

            x, y = [self.asnumpy(_x) / u.micron for _x in self.xyz[:2]]
            n = self.get_density()
            Ns = self.get_Ns()
            E = self.get_energy()
            vmin = 0
            vmax = n.max()
            for i, n in enumerate((na, nb, nc)):
                ax = plt.subplot(131 + i)
                mmfplt.imcontourf(x, y, n, vmin=vmin, vmax=vmax)
                ax.set(aspect=1, title=f"N={Ns[i]:.4f}")
            plt.suptitle(f"t={self.t/self.t0:.4f}ms, N={sum(Ns):.4f}, E={E:.4f}")
            # plt.colorbar()

        elif self.dim == 3:
            x, y, z = [self.asnumpy(_x) / u.micron for _x in self.xyz]
            nxy = n.sum(axis=2)
            nxz = n.sum(axis=1)
            nyz = n.sum(axis=0)

            gs = GridSpec(1, 3)
            ax = plt.subplot(gs[0])
            imcontourf(x, y, nxy)
            ax.set_aspect(1)
            ax = plt.subplot(gs[1])
            imcontourf(x, z, nxz)
            ax.set_aspect(1)
            ax = plt.subplot(gs[2])
            imcontourf(y, z, nyz)
            ax.set_aspect(1)

        # E = self.get_energy()
        # Na, Nb, Nc = self.get_Ns()
        # N = Na + Nb + Nc
        # E = 0
        # plt.title(
        #    f"t={self.t/self.t0:.4f}t0, Ns={Na:.4f}+{Nb:.4f}+{Nc:.4f}={N:.4f}, E={E:.4f}"
        # )

    def plot_k(self):  # pragma: nocover
        """Plot the momentum distribution."""
        from matplotlib import pyplot as plt

        # from matplotlib.gridspec import GridSpec
        # from mmfutils.plot import imcontourf
        na_k, nb_k = np.fft.fftshift(abs(np.fft.fft(self[...]) ** 2))
        if self.dim == 1:
            ka = np.fft.fftshift(self.asnumpy(self.kxyz[0][0]).ravel())
            kb = np.fft.fftshift(self.asnumpy(self.kxyz[1][0]).ravel())
            k_r = self.k_r
            plt.semilogy(ka / k_r, na_k, "b:")
            plt.semilogy(kb / k_r, nb_k, "g:")
        else:
            raise NotImplementedError

        E = self.get_energy()
        Na, Nb = self.get_Ns()
        N = Na + Nb
        plt.title(
            "t={}t0, Ns={:.4f}+{:.4f}={:.4f}, E={:.4f}".format(
                self.t / self.t0, Na, Nb, N, E
            )
        )

    def evolve_to(
        self,
        t_end,
        dt_t_scale=0.2,
        Evolver=evolvers.EvolverABM,
        evolve_steps=200,
        callback=None,
    ):
        """Evolve state to `self.t = t_end`.

        Arguments
        ---------
        t_end : float
           Evolves the state for `t_end` time units. Remember to
           convert times by `self.t_unit`.
        dt_t_scale : float
            Scales dt.
        Evolver : IEvolver
            Pick the relevent evolver, either `evolvers.EvolverABM` or
            `evolvers.EvolverSplit`.
        evolve_steps : float
            Number of evolution steps between callbacks if defined.
        callback : function
            Any function that takes the state as an argument, like
            plotting or visualization.
        """

        dt = dt_t_scale * self.t_scale
        T = t_end - self.t
        Nt = int(np.ceil(T / dt))
        dt = T / Nt
        evolver = Evolver(self, dt=dt)

        with NoInterrupt() as interrupted:
            while not interrupted and Nt > 1:
                _steps = min(evolve_steps, Nt)
                evolver.evolve(_steps)
                Nt -= _steps
                if callback:
                    callback(evolver.y)

        self[...] = evolver.y[...]
        self.t = evolver.y.t

    def arrested_newton_flow(
        self,
        dt=0.1,  # ??? Not specified in the paper?
        Etol=1e-6,
        display=False,
    ):
        """Find the initial state using arrested newton flow algorithm."""
        self._Es = []
        psi = self.get_psi_GPU()
        dpsi = 0 * psi

        s = self.copy()
        s.set_psi(psi)
        s.cooling_phase = 1j
        s.minimizing = True
        s.t = 0
        s.init()

        N0 = s.get_N()
        E0 = s.get_energy()
        E1 = np.inf

        while abs(E1 / E0 - 1) > Etol:
            self._Es.append(E0)
            psi = s.get_psi()
            if E1 > E0:
                ddpsi = s.compute_dy_dt(s.copy()).get_psi()
                dpsi = ddpsi * dt
            else:
                s.set_psi(psi)
                ddpsi = s.compute_dy_dt(s.copy()).get_psi()
                dpsi += ddpsi * dt

            psi += dpsi * dt
            s.set_psi(psi)
            s *= np.sqrt(N0 / s.get_N())
            E0, E1 = E1, s.get_energy()
            if display:
                import IPython.display
                from matplotlib import pyplot as plt

                plt.clf()
                IPython.display.clear_output(wait=True)
                s.plot()
                IPython.display.display(plt.gcf())

        self.set_psi(s.get_psi())

    def gradient_descent(
        self,
        dt=0.1,  # ??? Not specified in the paper?
        dt_step_up=1.01,
        dt_step_down=0.5,
        Etol=1e-6,
        display=False,
        max_iter=300,
    ):
        """Find the initial state using arrested newton flow algorithm."""
        self._Es = []
        psi = self.get_psi_GPU()

        dt_max = dt

        s = self.copy()
        s.set_psi(psi)
        s.cooling_phase = 1j
        s.minimizing = True
        s.t = 0
        s.init()

        N0 = s.get_N()
        E0 = s.get_energy()
        E1 = np.inf
        step = 0
        while step < max_iter:
            self._Es.append(E0)
            psi = s.get_psi().copy()
            dpsi = s.compute_dy_dt(s.copy()).get_psi()
            s.set_psi(psi + dt * dpsi)
            s *= np.sqrt(N0 / s.get_N())
            E1 = s.get_energy()
            if E1 < E0:
                if abs(E1 / E0 - 1) < Etol:
                    break
                dt = min(dt_step_up * dt, dt_max)
                E0 = E1
            else:
                # Bad Step
                s.set_psi(psi)
                dt *= dt_step_down
            step += 1
            if display:
                import IPython.display
                from matplotlib import pyplot as plt

                plt.clf()
                IPython.display.clear_output(wait=True)
                s.plot()
                IPython.display.display(plt.gcf())

        self.set_psi(s.get_psi())
