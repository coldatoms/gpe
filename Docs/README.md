---
jupytext:
  formats: md:myst,ipynb
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.16.4
kernelspec:
  display_name: Python 3 (ipykernel)
  language: python
  name: python3
---

# Contents

## Programming Interface

* [Examples.md](Examples.md): Start here.  This provides some simple examples, showing
  you how to use the programming interface to solve physics problems.

## Basic Demonstrations

* [BEC.md](BEC.md): Demonstration of the single-component BEC code.
* [BEC2.md](BEC2.md): Demonstration of the two-component BEC code.

* [FFT.md](FFT.md): Demonstration of the properties of the Fast Fourier Transform (FFT)
  which underlies our methods. 
* [Minimization.md](Minimization.md): Description of the constrained minimization
  techniques used to find initial stationary and ground states. 

* [LinearResponse.md](LinearResponse.md): Demonstration of collective excitations from
  linear response theory. 

* Demo.md


## Spin-Orbit Coupled BECs
* [SOC/NegativeMass](../SOC/NegativeMass.md): Basic spin-orbit coupled dynamics.
  Includes some movies used in talks.
* [SOC/SOC Solitons](../SOC/SOC%20Solitons.md):

## Entrainment (Andreev-Bashkin effect)
* [Entrainment.md](Entrainment.md)

## Misc
* Asymmetric Instability.md
* Aurel's Idea.md
* Expansion.md
* Solitons2.md
* Wall.md
