---
jupytext:
  formats: md:myst,ipynb
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.16.4
kernelspec:
  display_name: Python 3 (ipykernel)
  language: python
  name: python3
---

(sec:Optimization)=
Optimization
============

```{code-cell}
%pylab inline --no-import-all
from gpe import bec
from gpe.bec import u
from pytimeode.evolvers import EvolverABM

s = bec.State(
    Nxyz=(2**9, 2**7, 2**7), Lxyz=(80 * u.micron, 45 * u.micron, 45 * u.micron)
)
# state = bec.State(Nxyz=(2**8, 2**5, 2**5), Lxyz=(80*u.micron, 30*u.micron, 30*u.micron))
e = EvolverABM(s, dt=0.1 * s.t_scale)
%time e.evolve(5)
```

```{code-cell}
%pylab inline --no-import-all
from gpe import bec

reload(bec)
from gpe.bec import u
from pytimeode.evolvers import EvolverABM

s = bec.State(
    Nxyz=(2**9, 2**7, 2**7), Lxyz=(80 * u.micron, 45 * u.micron, 45 * u.micron)
)
# state = bec.State(Nxyz=(2**8, 2**5, 2**5), Lxyz=(80*u.micron, 30*u.micron, 30*u.micron))
e = EvolverABM(s, dt=0.1 * s.t_scale)
%time e.evolve(5)
```
