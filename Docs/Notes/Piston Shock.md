---
jupytext:
  formats: md:myst,ipynb
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.16.4
kernelspec:
  display_name: Python 3 (ipykernel)
  language: python
  name: python3
---

(sec:PistonShock)=
Piston Shock
============

```{code-cell}
import mmf_setup
```

```{code-cell}
mmf_setup.nbinit()
from gpe.imports import *
from importlib import reload
from gpe.Examples import piston
from gpe import minimize
```

```{code-cell}
plt.rcParams["figure.figsize"] = (10, 5)
```

```{code-cell}
reload(piston)
e1 = piston.Experiment(tube=True)
```

```{code-cell}
# e2 = piston.Experiment(basis_type='axial')
# s = e.get_initial_state()
def callback(state, _n=[0]):
    if _n[0] % 10 == 0:
        plt.clf()
        state.plot()
        display(plt.gcf())
        clear_output(wait=True)
    _n[0] += 1
```

```{code-cell}
# s = minimize.MinimizeState(e1.get_state()).minimize(f_tol=0.1, callback=callback)
# s0 = e0.get_initial_state()
# s0.plot()
s1 = e1.get_initial_state()
# s2 = e2.get_state()
s1.plot()
```

```{code-cell}
s1.m
```

```{code-cell}
from mmfutils.plot import imcontourf
```

```{code-cell}
imcontourf(s2.xyz[0], s2.xyz[1], s2.get_density())
plt.xlim(-60, 250)
plt.ylim(-2, 2)
```

```{code-cell}
s.t = 0 * piston.u.ms
plt.plot(s.basis.xyz[0], s.experiment.get_Vt(s))
```

```{code-cell}
from mmfutils.plot import imcontourf
from gpe.plot_utils import MPLGrid
```

```{code-cell}
e = EvolverSplit(s1, dt=1.0 * s1.t_scale)
history = [s1.copy()]
```

```{code-cell}
e = EvolverSplit(history[-1], dt=0.5 * s1.t_scale)
with NoInterrupt() as interrupted:
    while not interrupted and e.t < 120 * piston.u.ms:
        e.evolve(1000)
        history.append(e.get_y())
        plt.clf()
        # grid = MPLGrid()
        s = e.y
        # grid.grid()
        s.plot()
        # ax = grid.next()
        # imcontourf(s.xyz[0], s.xyz[1], s.get_density())
        # ax.set_xlim(0, 150)
        # ax.set_aspect(1)
        display(plt.gcf())
        clear_output(wait=True)
```

```{code-cell}
data = np.asarray([_s[...] for _s in history])
```

```{code-cell}
np.save("piston.npy", data)
```

```{code-cell}
grid = MPLGrid()
s1.plot??
# (grid.grid())
```
