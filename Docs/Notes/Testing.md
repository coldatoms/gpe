---
jupytext:
  formats: md:myst,ipynb
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.16.4
kernelspec:
  display_name: Python 3 (ipykernel)
  language: python
  name: python3
---

(sec:Testing)=
Testing
=======

```{code-cell}
:init_cell: true

import mmf_setup;mmf_setup.nbinit()
```

```{code-cell}
%pylab inline --no-import-all
from gpe.tests.test_minimize import state
from gpe import exact_solutions
from gpe.minimize import MinimizeState
s = exact_solutions.HarmonicOscillator()
np.random.seed(11)
s[...] = np.random.random(s.shape) + np.random.random(s.shape)*1j - 0.5 - 0.5j
s.normalize()
m = MinimizeState(s)
s.get_energy()
s.set_data(s.psi_exact)
s.get_energy()
abs(s.get_Hy(subtract_mu=True).ravel()).max()
```

```{code-cell}
import gpe.axial

import gpe.utils
class Experiment(gpe.utils.ExperimentBase):
    axial = True
    exact_state = exact_solutions.HarmonicOscillator())
    def get_state(self):
        if self.axial:
            s = self.exact_state
            state = gpe.axial.StateAxial(
                Nxr=(Nx, Nx//2),
                Lxr=(s.Lx, s.Lx//2),
            )
            
            return state
```

```{code-cell}
if True:
    s = exact_solutions.HarmonicOscillator(g=1.0)
    ds = s.empty()
    s.compute_dy_dt(ds, subtract_mu=True)
    print(abs(ds.ravel()).max())
    assert np.allclose(ds.ravel(), 0)
```

$$
  \psi(x) = e^{-x^2/2}, \qquad
  N = \sqrt{\pi}
$$

```{code-cell}
s = exact_solutions.HarmonicOscillator(g=1.0)
assert np.allclose(s.ravel(), s.psi_exact)
ds = s.empty()
pdb.run('s.compute_dy_dt(ds, subtract_mu=True)')
x = s.x
plt.plot(x, abs(ds.ravel()))
```

```{code-cell}
V = s.get_Vext()
Ks = s.copy()
Ks.apply_laplacian(1.0)
```

```{code-cell}
pdb.run('s.get_Vext()')
```

```{code-cell}
#plt.plot(x, (-Ks/2 - s*V).ravel().real)
#plt.plot(x, s.g*s.psi_exact**2*s.psi_exact, ':')
#plt.plot(x, Ks.ravel().real)
#plt.plot(x, (-1+x**2)*s.ravel())
#plt.plot(x, ds.ravel())
```

```{code-cell}

```
