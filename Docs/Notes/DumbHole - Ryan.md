---
jupytext:
  formats: md:myst,ipynb
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.16.4
kernelspec:
  display_name: Python 3 (ipykernel)
  language: python
  name: python3
---

```{code-cell}
import mmf_setup
```

```{code-cell}
mmf_setup.nbinit()
```

```{code-cell}
%pylab inline --no-import-all
from IPython.display import display, clear_output
from mmfutils.contexts import NoInterrupt
from pytimeode.evolvers import EvolverABM, EvolverSplit
from gpe.minimize import MinimizeState

import gpe.soc_soliton

reload(gpe.soc_soliton)
from gpe import utils

from gpe.soc_soliton import u

experiment = gpe.soc_soliton.Experiment()


###################
class State(gpe.soc_soliton.State2tube):
    def __init__(self, experiment=experiment, Nx=4800 / 2, Lx=320 * u.micron):
        self.U0 = 39 * u.nK
        self.Us = -6 * u.nK
        self.mu = 8.0 * u.nK
        self.v_s = 0.21 * u.mm / u.s
        self.x0 = -100 * u.micron
        self.w0 = 5 * u.micron
        self.lam = 812 * u.nm
        gpe.soc_soliton.State2tube.__init__(
            self, experiment=experiment, Nxyz=(Nx,), Lxyz=(Lx,)
        )

    @property
    def x_s(self):
        return self.x0 + self.t * self.v_s

    def get_Vtrap(self):
        x = self.xyz[0]
        x0 = np.pi * self.w0**2 / self.lam
        w = self.w0 * np.sqrt(1 + (x / x0) ** 2)
        d_x = 1 * u.micron
        V_s = self.Us * np.vectorize(utils.step)(self.x_s - x - d_x / 2, d_x)
        V_a = V_b = self.U0 * (1 - (self.w0 / w) ** 2) + V_s - self.mu
        return V_a, V_b


###################

# Plotting
s0 = State()
m = MinimizeState(s0, fix_N=False)
assert m.check()
s = m.minimize()
s.plot()
```

### Dumbhole

+++

From soc_soliton, create a new experiment for dumbhole, with SOC

```{code-cell}
%pylab inline --no-import-all
from IPython.display import display, clear_output
from mmfutils.contexts import NoInterrupt
from pytimeode.evolvers import EvolverABM, EvolverSplit
from gpe.minimize import MinimizeState

import gpe.soc_soliton

reload(gpe.soc_soliton)
from gpe import utils

from gpe.soc_soliton import u

experiment = gpe.soc_soliton.Experiment()
```
