---
jupytext:
  encoding: '# -*- coding: utf-8 -*-'
  formats: md:myst,ipynb
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.16.4
kernelspec:
  display_name: Python 3 (ipykernel)
  language: python
  name: python3
---

# DVR Basis

+++

In this notebook we describe how to use the BEC code to describe axially symmetric systems.  The need for a new basis can be seen by considering that we now need to tabulate points at $(x, r)$ where $r=\sqrt{y^2+z^2}$.  The radial dimension $r$ is not periodic, so the simple Fourier techniques we have been using do not work.

+++

## `mmfutils.math.bases`

+++

To implement more general bases we use the `mmfutils.math.bases` package.  This provides various classes satisfying the following interface:

```{code-cell}
from IPython.display import display
from mmfutils.interface import describe_interface
import mmfutils.math.bases.interface
```

```{code-cell}
display(describe_interface(mmfutils.math.bases.interface.IBasisMinimal))
display(describe_interface(mmfutils.math.bases.interface.IBasis))
```

To use this, we thus redesign our classes to call only these methods.  A good exercise is to take the code in [`bec_basic.py`](../gpe/bec_basic.py) and refactor it to use `mmfutils.math.bases.PeriodicBasis`.  *(This was done with revision [2abbfcb0d613] for the `bec.py` file if you want to see the changes)*

1. Make sure that you have comprehensive testing that works for your file and make sure that all relevant lines of our source code are covered by the tests.  *(See revision [2abbfcb0d613] for passing tests with `bec.py`.)*
2. Start replacing the relevant code with the basis code.  We needed to do the following:
   * Create `self.basis`.
   * Create appropriate delegations for attributes that are now provided by the basis:
     * `Nxyz`→`basis.Nxyz`, `Lxyz`→`basis.Lxyz`, `xyz`→`basis.xyz`, `metric`→`basis.metric`, `symmetric_grid`→`basis.symmetric_lattice`.
   * Remove dependence on `K`.  This was a little more complicated.  We replaced this with `K_factor` which contained the factor $-\hbar^2/2m$ and then use `basis.laplacian`.
   * Removed dependence on `kxyz` and various `fft` stuff which assumes a periodic basis.
   * I also removed `get_H()`.  This could be added later, but will need the basis to provide the matrix representation of the Laplacian.
   * *These changes can be seen in revision [e6d500d279a6].*
3. Change interface so that different bases can be used by allowing the `State()` object to be constructed from a `basis`.
   * This requires a bit more careful thought because some functionality is still assuming that `Nxyz` and `Lxyz` can be used.  These had to be removed since they are not part of the `IBasis` interface.
4. These changes should be complete in revision [0765cab259a6] but testing is needed with the notebooks which might depend on some other features.

[2abbfcb0d613]: https://bitbucket.org/mforbes/gpe/commits/2abbfcb0d613
[e6d500d279a6]: https://bitbucket.org/mforbes/gpe/commits/e6d500d279a6
[0765cab259a6]: https://bitbucket.org/mforbes/gpe/commits/0765cab259a6
