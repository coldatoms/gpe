---
jupytext:
  formats: md:myst,ipynb
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.16.4
kernelspec:
  argv: [/usr/bin/python3, -m, ipykernel, --HistoryManager.enabled=False, --matplotlib=inline,
    -c, '%config InlineBackend.figure_formats = set([''retina''])

      import matplotlib; matplotlib.rcParams[''figure.figsize''] = (12, 7)', -f, '{connection_file}']
  display_name: Python 3 (system-wide)
  env: {}
  language: python
  metadata:
    cocalc:
      description: Python 3 programming language
      priority: 100
      url: https://www.python.org/
  name: python3
  resource_dir: /ext/jupyter/kernels/python3
---

```{code-cell} ipython3
:tags: [hide-cell]

import mmf_setup; mmf_setup.nbinit()
import os
from pathlib import Path
FIG_DIR = Path(mmf_setup.ROOT) / '../Docs/_build/figures/'
os.makedirs(FIG_DIR, exist_ok=True)
import logging; logging.getLogger("matplotlib").setLevel(logging.CRITICAL)
%matplotlib inline
import numpy as np, matplotlib.pyplot as plt
try: from myst_nb import glue
except: glue = None
```

(sec:LinearResponse)=
Linear Response
===============

Here we consider the fluctuations about a stationary state $\psi_0$ of GPE-like
theories.  We assume that the equations of motion (EoM) arise from a principle of stationary
action:
%
\begin{gather*}
  S[\psi] = \int \left(\int \I\hbar \psi^\dagger\dot{\psi} \d{x} - E[\psi]\right)\d{t},\\
  \I\hbar \dot{\psi} = \op{H}[\psi]\psi = \frac{\delta E[\psi]}{\delta \psi^\dagger}.
\end{gather*}
%
For standard GPE-like theories, we have
\begin{gather*}
  E[\psi] = \int \d{x}\; \left(
    \psi^{\dagger}K(\op{p})\psi + \mathcal{E}(n)
   \right), \qquad
   n = \abs{\psi}^2,\\
   \I\hbar \dot{\psi} = \Bigl(K(\op{p}) + \mathcal{E}'(n)\Bigr)\psi,
\end{gather*}
%
where $\mathcal{E}(n)$ is the equation of state and $K(\op{p})$ is the single-particle
dispersion relation (kinetic energy).  More generally, we might have
an external potential, chemical potential, etc.  We roll all of these into
$\mathcal{E}(n)$ for this discussion and keep this general.  Thus, for the standard GPE
we would have
\begin{gather*}
  K_{GPE}(\op{p}) = \frac{\op{p}^2}{2m} = \frac{-\hbar^2\nabla^2}{2m},\\
  \mathcal{E}_{GPE}(n) = g\frac{n^2}{2} + \Bigl(V(x, t) - \mu\Bigr)n,\\
  \I\hbar \dot{\psi} = \Biggl(
    \underbrace{\frac{-\hbar^2\nabla^2}{2m}}_{K(\op{p})}
    + \underbrace{gn + V(x, t) - \mu}_{\mathcal{E}'_{GPE}(n)}
  \Biggr)\psi.
\end{gather*}

**To Do: Put back $\mu$.**

We start with a stationary state $\psi_0(x)$, and expand:
\begin{gather*}
  \op{H}[\psi_0]\psi_0(x) = 0, \qquad
  \psi = \psi_0(1 + \epsilon), \qquad
  \epsilon = u_+ e^{\omega t/\I} + u_- e^{-\omega t/\I}.
\end{gather*}

:::{margin}
A couple of tricks are important.  Note that the density is
\begin{gather*}
  n = n_0\bigl(1 + \epsilon + \epsilon^* + O(\epsilon^2)\bigr).
\end{gather*}
As discussed in the text, the presence of both $\epsilon$ and $\epsilon^*$ lead to a
coupled set of equations with both positive and negative frequency components.  For
example, if we had just considered $\epsilon = ue^{\omega t/\I}$, then this term would
force the inclusion of the term proportional to $e^{-\omega t/\I}$.  Including both and
keep only linear terms, we have a closed system.  If we keep $O(\epsilon^2)$ terms, then
we additional harmonics $e^{2\omega t/\I}$ etc. will appear, requiring a general solution
as provided by, e.g., solving the full GPE.  Finally, although we expect $\omega$ to be
real, we treat it as complex so that we can see how a global cooling phase might enter
the response.
:::
Inserting  this into the EoM and keeping terms of order $\epsilon$, we have
\begin{gather*}
    \I \hbar (\dot{\psi_0}\epsilon + \psi_0\dot{\epsilon}) = 
    K(\op{p})(\psi_0\epsilon) + \mathcal{E}'(n_0)\psi_0\epsilon 
    + \mathcal{E}''(n_0)n_0(\epsilon + \epsilon^*)\psi_0.
\end{gather*}
The last term -- from the expansion of $n$ -- contains both $\epsilon$ and
$\epsilon^$.  This is a consequence of the non-linear nature of the GPE-like equation
and gives rise to a coupled set of equations for the linear response.

To proceed, we want to remove the factor of $\psi_0$.  We first note that we can express
stationary states as
\begin{gather*}
  \psi_0(x, t) = e^{\mu t/\I\hbar}\psi_0(x),
\end{gather*}
and we can absorb the $\mu$ produced by the first term into $\mathcal{E}'$.  If we can
define an operator $\tilde{K}(\op{p})$ such that
\begin{gather*}
  K(\op{p})(\psi_0\epsilon) = \psi_0\tilde{K}(\op{p})\epsilon,
\end{gather*}
then we can express these equations as
\begin{gather*}
  \begin{pmatrix}
    \op{H} + n_0\mathcal{E}'' - \hbar \omega & n_0\mathcal{E}''\\
    n_0\mathcal{E}'' & \op{H}^* + n_0\mathcal{E}'' + \hbar \omega^*
  \end{pmatrix}
  \begin{pmatrix}
    u_+\\
    u_-^*
  \end{pmatrix}
  =
  \vect{0},\\
  \op{H} = \tilde{K}(\op{p}) + \mathcal{E}'(n_0) + V(x, t) - \mu.
\end{gather*}
These are the generalized homogeneous linear response equations.

## Linear Response

We now consider driving the system where $V(x, t) = V_0 \cos(\omega t)\cos(kx)$ is small.  This
adds an inhomogeneous term which we treat as the same order a $u_{\pm}$:
:::{margin}
Recall that the full solution includes both the particular solution plus the general
homogeneous solution.  These additional homogeneous solutions give rise to transient
behavior.  Consider two cases: if there is dissipation, then all of the homogeneous
solutions will decay, leaving only the particular solution.  If there is no dissipation,
then one can add phonons -- at the linear level, these do not interact.
:::
\begin{gather*}
  \begin{pmatrix}
    \op{H} + n_0\mathcal{E}'' - \hbar \omega & n_0\mathcal{E}''\\
    n_0\mathcal{E}'' & \op{H}^* + n_0\mathcal{E}'' + \hbar \omega^*
  \end{pmatrix}
  \begin{pmatrix}
    u_+(x) \\
    u_-^*(x)
  \end{pmatrix}
  =
  -\frac{V_0}{2}\cos(kx)
  \begin{pmatrix}
    \psi_0\\
    \psi_0^*
  \end{pmatrix},
\end{gather*}
the formal solution of which can be found by inverting the matrix.


:::{admonition} Details about $\tilde{K}(\op{p})$.

Consider how this works for various powers of $p$ as a consequence of the [General Leibniz rule][]
\begin{align*}
  \op{p}(\psi_0\epsilon) &=  (\op{p}\psi_0) \epsilon + \psi_0(\op{p}\epsilon)\\ 
  \op{p}^2(\psi_0\epsilon) &= 
  (\op{p}^2\psi_0) \epsilon +  2(\op{p}\psi_0)(\op{p}\epsilon) + \psi_0(\op{p}^2\epsilon),\\
  \op{p}^n(\psi_0\epsilon) &= \sum_{m=0}^{n}\binom{n}{m}(\op{p}^{m}\psi_0)(\op{p}^{n-m} \epsilon).
\end{align*}
For the usual quadratic dispersion $K(\op{p}) = \op{p^2}/2m$ and expanding about a state
$\psi_0$ with momentum $p_0$, we have
\begin{align*}
  \tilde{K}(\op{p}) = \frac{(p_0+\vect{p})^2}{2m}
  = \frac{p_0^2}{2m} + \frac{p_0\op{p}}{m}  + \frac{\op{p}^2}{2m}
  = E_0 + v_0\op{p} + \frac{\op{p}^2}{2m},\\
  \tilde{K}^*(\op{p}) = \frac{(p_0-\vect{p})^2}{2m}
  = \frac{p_0^2}{2m} - \frac{p_0\op{p}}{m}  + \frac{\op{p}^2}{2m}
  = E_0 - v_0\op{p} + \frac{\op{p}^2}{2m}.
\end{align*}
Note that $\op{p}^* = -\op{p}$.  This is exactly the transformation required to restore
Galilean invariance.  I.e., the normal modes about the state $\psi_0$ with momentum
$p_0$ (and velocity $v_0=p_0/m$) should be the same as the normal modes in a co-moving
frame where $\psi_0$ has zero momentum.  More on this below.
:::

:::::{admonition} Alternative Approach
:class: toggle

It might be instructive to consider the same derivation from a slightly different
approach.  Consider instead expanding as follows:
\begin{gather*}
  \op{H}[\psi_0]\psi_0(x) = 0, \qquad
  \psi = \psi_0 + \epsilon, \qquad
  \epsilon = \psi_+ e^{\omega t/\I} + \psi_- e^{-\omega t/\I}.
\end{gather*}
I.e., without including an overall factor of $\psi_0$.  This seems to be easier:
\begin{gather*}
  n = n_0 + \psi_0^* \epsilon + \epsilon^* \psi_0 + O(\epsilon^2),\\
  \I \hbar \dot{\epsilon} = 
    K(\op{p})\epsilon + \mathcal{E}'(n_0)\epsilon 
    + \mathcal{E}''(n_0)(\psi_0^* \epsilon + \epsilon^* \psi_0)\psi_0,\\
  %\hbar \omega \psi_+ = 
  %  \Bigl(K(\op{p}) + \mathcal{E}'\Bigr)\psi_+ 
  %  + \mathcal{E}''(\psi_0^* \psi_+ + \psi_-^* \psi_0)\psi_0,\\
  %-\hbar \omega \psi_- =
  %  \Bigl(K(\op{p}) + \mathcal{E}'\Bigr)\psi_- 
  %  + \mathcal{E}''(\psi_0^* \psi_- + \psi_+^* \psi_0)\psi_0,\\
  \begin{pmatrix}
    K(\op{p}) + \mathcal{E}' + n_0\mathcal{E}'' - \hbar \omega & \psi_0^2\mathcal{E}''\\
    \psi_0^{*2}\mathcal{E}'' & K^*(\op{p}) + \mathcal{E}'{}^* + n_0\mathcal{E}'' + \hbar \omega^*
  \end{pmatrix}
  \begin{pmatrix}
    \psi_+\\
    \psi_-^*
  \end{pmatrix}
  =
  \vect{0}.
\end{gather*}
This seems simpler in that we don't have $\tilde{K}(\op{p})$, but now the off-diagonal
terms in the matrix have a factor $\psi_0^2 = e^{2\I\phi}n_0$ and its conjugate, whereas
the diagonal terms have a factor of $n_0$ without the phase.

In most cases, we can take the stationary state as having no phase, but if it does
(i.e. twisted boundary conditions, finite momentum, etc.), we must redefine: 
\begin{gather*}
  \psi = e^{\I\phi}(\sqrt{n_0} + \epsilon).
\end{gather*}
Thus, below we simply replace $\psi_0^2 = n_0$.  We also keep $\omega^*$ so that we can
see how a cooling phase might enter.  This leads again to terms like
$\tilde{K}(\op{p})$.
:::::

## Homogeneous Matter

:::{margin}
Note how the wave-vectors appear here.  Both $u_\pm$ have phase velocity in the same
direction $v = \omega/k$, but since $u_-$ has negative frequency, this adds in the
opposite direction to the background wave $k_0$.  This is the origin of the sign in
$\tilde{K}(\op{p})$.
:::
To see how this works, consider homogeneous matter where $\psi_0$ has momentum $p_0$ and
we can use plane-waves $u_\pm \propto e^{\pm \I k x}$:
\begin{gather*}
  \psi = \sqrt{n_0}e^{\I k_0 x}\Bigl(
    1 + u_+ e^{\I(kx - \omega t)} + u_- e^{-\I(kx - \omega t)}
  \Bigr),\\
  = \sqrt{n_0}\Bigl(
    e^{\I k_0 x} 
    + u_+ e^{\I\bigl((k+k_0)x - \omega t\bigr)} 
    + u_- e^{-\I\bigl(k-k_0)x - \omega t\bigr)}
  \Bigr).
\end{gather*}
In this case $\op{H}$ and $\op{H}^*$ can be treated as a numbers 
\begin{gather*}
  H = \frac{(p_0 + \hbar k)^2}{2m} + \mathcal{E}'(n_0) - \mu, \qquad
  H^* = \frac{(p_0 - \hbar k)^2}{2m} + \mathcal{E}'(n_0) - \mu.
\end{gather*}
We start by expanding about the ground state with $k_0=0$ so that $H = H^*$. Taking the
determinant, we have
\begin{gather*}
  H^2 + 2Hn_0\mathcal{E}''(n_0) - H \hbar (\omega - \omega^*) - \hbar^2\abs{\omega}^2 = 0.
\end{gather*}
If $\omega = \omega^*$, then we find the usual phonon dispersion relation:
\begin{gather*}
  \hbar\omega(k) = \pm\sqrt{H(H + 2n_0\mathcal{E}'')}
                 = \pm\sqrt{H(H + 2mc^2)},\qquad
  c = \sqrt{\frac{n_0\mathcal{E}''(n_0)}{m}},
\end{gather*}
where (as we shall see shortly) $c$ is the long wavelength **speed of sound**, expressed in
terms of the compressibility $\mathcal{E}''(n_0)$. 

The chemical potential $\mu$ cancels the constant pieces so that $\op{H}\psi_0 = 0$ and:
\begin{gather*}
  H = \frac{\hbar^2 k^2}{2m}.
\end{gather*}

(sec:PhononDispersion)=
### Phonon Dispersion
This gives the familiar dispersion of phonons about the ground state $p_0 = 0$:
:::{margin}
We note that, in the proper GPE with $\mathcal{E}(n_0) = gn_0^2/2$, we can related this
to the **healing length** $h = \hbar/\sqrt{2m\mathcal{E}'}$ since $\mathcal{E}''(n_0)
= \mathcal{E}'(n_0)/n_0$:
\begin{gather*}
  c = \frac{\hbar}{\sqrt{2}m h}.
\end{gather*}
This is not a general result though -- the compressibility has the correct physics.
:::
\begin{align*}
  \omega(k) &= \pm ck 
  \sqrt{1 + \frac{\hbar^2k^2}{4m^2c^2}}, & 
  c &= \sqrt{\frac{n_0\mathcal{E}''(n_0)}{m}},\\
  E(p) = \hbar\omega &= \pm cp 
  \sqrt{1 + \frac{p^2}{2m} \frac{1}{2mc^2}}.
\end{align*}
This is linear in the long wavelength limit $k \rightarrow 0$ with the advertised speed
of sound $c$.  Note that when expressed in terms of the energy and momentum, this is a
classical result without any explicit factors of $\hbar$.

More generally, expanding about state $\psi_0$ with momentum $p_0$,
\begin{gather*}
  H = \overbrace{\frac{\hbar^2k^2}{2m}}^{H_0} + v_0k = \frac{\hbar^2(k+k_0)^2}{2m} - E_0,\\
  H^* = \frac{\hbar^2k^2}{2m} - v_0k = \frac{\hbar^2(k-k_0)^2}{2m} - E_0,\\
  \begin{vmatrix} 
    H_0 - \hbar(\omega - v_0k) + n_0\mathcal{E}''(n_0) & n_0\mathcal{E}''(n_0)\\
    n_0\mathcal{E}''(n_0) & H_0 + \hbar(\omega - v_0k)^* + n_0\mathcal{E}''(n_0)
  \end{vmatrix} = 0
\end{gather*}
where $v_0 = p_0/m$ and $E_0 = mv_0^2/2$.  Taking the determinant, we have the slightly
more cumbersome
\begin{gather*}
  H_0^2 + 2H_0n_0\mathcal{E}''(n_0) - H_0 \hbar (\omega - \omega^*) 
  - \hbar^2\abs{(\omega-v_0k)}^2 = 0.
\end{gather*}
And if $\omega = \omega^*$, we find the boosted phonon dispersion:
\begin{gather*}
  \hbar\omega(k) - v_0k = \pm\sqrt{H_0(H_0 + 2n_0\mathcal{E}'')}.
\end{gather*}

```{code-cell} ipython3
hbar = m = 1
c = 1
k = np.linspace(-2, 2, 101)
k0 = 0.5

def get_w(k,  k0=k0, c=c):
    v0 = hbar*k0/m
    p = hbar*k
    H0 = p**2/2/m
    w0 = np.sqrt(H0*(H0 + 2*m*c**2))/hbar
    return w0 + v0*k, -w0 + v0*k
    
fig, ax = plt.subplots()
for w in get_w(k):
    ax.plot(k, w)
```

The full linear response follows from inverting the matrix, but we need to be a bit
careful.  We start by setting $k_0 = 0$ so that parity is conserved.  Then we expand:
\begin{gather*}
  \psi = \sqrt{n_0}\Bigl(
    1 + (u_{+}e^{-\I\omega t} + u_{-}e^{\I\omega t})\cos(kx)\Bigr),\\
  n = n_0 + \sqrt{n_0}\cos(kx)2\Re\Bigl(
    u_{+}e^{-\I\omega t} + u_{-}e^{\I\omega t}\Bigr)
\end{gather*}
Collecting all terms, we have two independent sets of equations:
\begin{gather*}
  \begin{pmatrix} 
    A - \hbar\omega e^{\I\eta} & B\\
    B & A + \hbar\omega e^{-\I\eta}
  \end{pmatrix}
  \begin{pmatrix}
    u_{+}\\
    u_{-}^*
  \end{pmatrix}
  =
  -\frac{V_0\sqrt{n_0}}{4}
  \begin{pmatrix}
    1\\
    1
  \end{pmatrix},\\
  A = H_0 + B, \qquad 
  B = n_0\mathcal{E}''(n_0).
\end{gather*}
\begin{gather*}
  \begin{pmatrix}
    u_{+}\\
    u_{-}^*
  \end{pmatrix}
  =
  -\frac{V_0\sqrt{n_0}}{4\Bigl(
    (A - \hbar\omega e^{\I\eta})(A + \hbar\omega e^{-\I\eta}) - B^2
  \Bigr)}
  \begin{pmatrix} 
    A + \hbar\omega e^{-\I\eta} & -B\\
    -B & A - \hbar\omega e^{\I\eta}
  \end{pmatrix}
  \begin{pmatrix}
    1\\
    1
  \end{pmatrix},\\
  =
  -\frac{V_0\sqrt{n_0}}{4\Bigl(
    A^2 - \hbar^2\omega^2 - B^2 - 2\I A\hbar\omega \sin(\eta)
  \Bigr)}
  \begin{pmatrix} 
    A + \hbar\omega e^{-\I\eta} -B\\
    A - \hbar\omega e^{\I\eta} - B
  \end{pmatrix},\\
  u_{+} = 
  -\frac{V_0\sqrt{n_0}(A + \hbar\omega e^{-\I\eta} -B)}{4\Bigl(
    A^2 - \hbar^2\omega^2 - B^2 - 2\I A\hbar\omega \sin(\eta)\Bigr)}\\
  u_{-} = 
  -\frac{V_0\sqrt{n_0}(A - \hbar\omega e^{\I\eta} - B)}{4\Bigl(
    A^2 - \hbar^2\omega^2 - B^2 - 2\I A\hbar\omega \sin(\eta)\Bigr)},\\
  u_{+} = 
  -\frac{V_0\sqrt{n_0}(A + \hbar\omega e^{-\I\eta} -B)}{4\Bigl(
    A^2 - \hbar^2\omega^2 - B^2 - 2\I A\hbar\omega \sin(\eta)\Bigr)}\\
  u_{-} = 
  -\frac{V_0\sqrt{n_0}(A - \hbar\omega e^{\I\eta} - B)}{4\Bigl(
    A^2 - \hbar^2\omega^2 - B^2 - 2\I A\hbar\omega \sin(\eta)\Bigr)}
\end{gather*}













$$
  \begin{pmatrix}
    \op{H} + g n_0 & g\psi_0^2\\
    g \bar{\psi}_0^2 & \bar{\op{H}} + g n_0
  \end{pmatrix}\cdot
  \begin{pmatrix}
    u(x)\\
    v(x)
  \end{pmatrix}
  =
  \omega
  \begin{pmatrix}
    \mat{1} \\
     & -\mat{1}
  \end{pmatrix}\cdot
  \begin{pmatrix}
    u(x)\\
    v(x)
  \end{pmatrix},
$$

where $n_0 = \abs{\psi_0}^2$ and $\op{H} = -\hbar^2\nabla^2/2m + gn_0 + \op{V}_{\text{ext}}$ is the single-particle Hamiltonian for the ground state. To solve this numerically, we write this as $\mat{A}\cdot\vect{q} = \omega \mat{B}\cdot\vect{q}$.

These matrices have the following properties: $\mat{A} = \mat{A}^\dagger$ and $\mat{B} = \mat{B}^\dagger$ are Hermitian, and the matrix $\mat{C} = \mat{C}^{-1} =  \bigl(\begin{smallmatrix}&\mat{1}\\\mat{1}\end{smallmatrix}\bigr)$ conjugates $\mat{A}$:

$$
  \mat{C}\cdot\mat{A}\cdot\mat{C} = \bar{\mat{A}}\\
  \mat{C}\cdot\mat{B}\cdot\mat{C} = -\bar{\mat{B}}.
$$.  Thus, if we have one eigenvalue $\omega_{+}$ and eigenvector $\vect{q}_{+}$, then, we must have another pair $\omega_{-} = \bar{\omega}_{+}$ and $\vect{q}_{-} = \mat{C}\cdot\bar{\vect{q}}_{+}$:

$$
  \mat{A}\vect{q}_{+} = \omega_+ \mat{B}\vect{q}_{+}\\
  \bar{\mat{A}}\mat{C}\vect{q}_{+} = \omega_+ \bar{\mat{B}}\mat{C}\vect{q}_{+}.
$$

Furthermore, if $\psi_0$ is a stationary state, then $\op{H}\psi_0 = \mu\psi_0$.



One has two choices: solve the non-symmetric eigenvalue problem $(\mat{B}^{-1}\cdot\mat{A})\cdot\vect{q} = \omega\vect{q}$, or try to massage this into a form where $\mat{B}$ is positive definite.

+++

$$
  (\mat{A} + 2\mat{1})\cdot\vect{q} = (\omega\mat{B} + 2\mat{I})\cdot\vect{q}
$$

+++

## Fixed Particle Number

+++

Let $\psi = \psi_0 + \d{\psi}$.  The change in density is:

$$
  \dot{n} = \dot{\psi}^\dagger \psi_0 + \psi_0^\dagger \dot{\psi} 
          + \dot{\psi}^\dagger \dot{\psi}
$$
$$
  n - n_0 = e^{-\I\omega t}(u \psi_0^* + v\psi_0)
  + e^{\I\omega t}(u^* \psi_0 + v^*\psi_0^*)
  + e^{-2\I\omega t} u^*v^*
  + e^{2\I\omega t} uv
  + (u^*u + v^*v)
$$

particle number is:

$$
  \d{N} = \int\left(
    \d{\psi}^\dagger \psi_0 + \psi_0^\dagger \d{\psi} + \d{\psi}^\dagger \d{\psi}
  \right)\d{x}\\
  = e^{-\I\omega t}\int(u \psi_0^* + v\psi_0)\d{x}
  + e^{\I\omega t}\int(u^* \psi_0 + v^*\psi_0^*)\d{x}
  + e^{-2\I\omega t} \int u^*v^*\d{x}
  + e^{2\I\omega t}\int uv\d{x}
  + \int(u^*u + v^*v)\d{x}
$$

```{code-cell} ipython3
from pytimeode.evolvers import EvolverABM, EvolverSplit
from mmfutils.contexts import NoInterrupt
```

```{code-cell} ipython3
import bec;reload(bec)
from bec import State, u
s = State(N=1e1, Nxyz=(2**7,), Lxyz=(20*u.micron,))
s.pre_evolve_hook()
s._N = 1e1
s.normalize()
s.plot()
s.cooling_phase = 1j

e = EvolverSplit(s, dt=1.0, normalize=True)

E_tol = 1e-8
E1 = e.y.get_energy()
E0 = E1 + 2*E_tol

log = True

with NoInterrupt(ignore=True) as interrupted:
    while not interrupted and abs(E1 - E0) > E_tol:
        e.evolve(100)
        E0, E1 = E1, e.y.get_energy()
        plt.clf()
        e.y.plot(log=log)
        display(plt.gcf())
        clear_output(wait=True)

e.dt = 0.01
E0 = E1 + 2*E_tol        
with NoInterrupt(ignore=True) as interrupted:
    while not interrupted and abs(E1 - E0) > E_tol/100:
        print abs(E1 - E0)
        e.evolve(100)
        E0, E1 = E1, e.y.get_energy()
        plt.clf()
        e.y.plot(log=log)
        display(plt.gcf())
        clear_output(wait=True)

fact = 100.0
e = EvolverABM(e.get_y(), dt=1.0/fact)
E0 = E1 + 2*E_tol        
with NoInterrupt(ignore=True) as interrupted:
    while not interrupted and abs(E1 - E0) > E_tol/fact/10:
        print abs(E1 - E0)
        e.evolve(100)
        E0, E1 = E1, e.y.get_energy()
        plt.clf()
        e.y.plot(log=log)
        display(plt.gcf())
        clear_output(wait=True) 
s = e.get_y()
```

# Normal Modes

+++

Here we consider the fluctuations about a stationary state $\psi_0$ of the GPE:

$$
  \psi = \psi_0 + u(x)e^{\I\omega t} + v^*(x) e^{-\I\omega t}.
$$

This gives the following generalized eigenvalue problem for the modes:

$$
  \begin{pmatrix}
    \op{H} + g n_0 & g\psi_0^2\\
    g \bar{\psi}_0^2 & \bar{\op{H}} + g n_0
  \end{pmatrix}\cdot
  \begin{pmatrix}
    u(x)\\
    v(x)
  \end{pmatrix}
  =
  \omega
  \begin{pmatrix}
    \mat{1} \\
     & -\mat{1}
  \end{pmatrix}\cdot
  \begin{pmatrix}
    u(x)\\
    v(x)
  \end{pmatrix},
$$

where $n_0 = \abs{\psi_0}^2$ and $\op{H} = -\hbar^2\nabla^2/2m + gn_0 + \op{V}_{\text{ext}}$ is the single-particle Hamiltonian for the ground state. To solve this numerically, we write this as $\mat{A}\cdot\vect{q} = \omega \mat{B}\cdot\vect{q}$.

These matrices have the following properties: $\mat{A} = \mat{A}^\dagger$ and $\mat{B} = \mat{B}^\dagger$ are Hermitian, and the matrix $\mat{C} = \mat{C}^{-1} =  \bigl(\begin{smallmatrix}&\mat{1}\\\mat{1}\end{smallmatrix}\bigr)$ conjugates $\mat{A}$:

$$
  \mat{C}\cdot\mat{A}\cdot\mat{C} = \bar{\mat{A}}\\
  \mat{C}\cdot\mat{B}\cdot\mat{C} = -\bar{\mat{B}}.
$$.  Thus, if we have one eigenvalue $\omega_{+}$ and eigenvector $\vect{q}_{+}$, then, we must have another pair $\omega_{-} = \bar{\omega}_{+}$ and $\vect{q}_{-} = \mat{C}\cdot\bar{\vect{q}}_{+}$:

$$
  \mat{A}\vect{q}_{+} = \omega_+ \mat{B}\vect{q}_{+}\\
  \bar{\mat{A}}\mat{C}\vect{q}_{+} = \omega_+ \bar{\mat{B}}\mat{C}\vect{q}_{+}.
$$

Furthermore, if $\psi_0$ is a stationary state, then $\op{H}\psi_0 = \mu\psi_0$.



One has two choices: solve the non-symmetric eigenvalue problem $(\mat{B}^{-1}\cdot\mat{A})\cdot\vect{q} = \omega\vect{q}$, or try to massage this into a form where $\mat{B}$ is positive definite.

+++

$$
  (\mat{A} + 2\mat{1})\cdot\vect{q} = (\omega\mat{B} + 2\mat{I})\cdot\vect{q}
$$

+++

## Fixed Particle Number

+++

Let $\psi = \psi_0 + \d{\psi}$.  The change in density is:

$$
  \dot{n} = \dot{\psi}^\dagger \psi_0 + \psi_0^\dagger \dot{\psi} 
          + \dot{\psi}^\dagger \dot{\psi}
$$
$$
  n - n_0 = e^{-\I\omega t}(u \psi_0^* + v\psi_0)
  + e^{\I\omega t}(u^* \psi_0 + v^*\psi_0^*)
  + e^{-2\I\omega t} u^*v^*
  + e^{2\I\omega t} uv
  + (u^*u + v^*v)
$$

particle number is:

$$
  \d{N} = \int\left(
    \d{\psi}^\dagger \psi_0 + \psi_0^\dagger \d{\psi} + \d{\psi}^\dagger \d{\psi}
  \right)\d{x}\\
  = e^{-\I\omega t}\int(u \psi_0^* + v\psi_0)\d{x}
  + e^{\I\omega t}\int(u^* \psi_0 + v^*\psi_0^*)\d{x}
  + e^{-2\I\omega t} \int u^*v^*\d{x}
  + e^{2\I\omega t}\int uv\d{x}
  + \int(u^*u + v^*v)\d{x}
$$

To linear order, conservation of particle number thus implies that

$$
  \int(u \psi_0^\dagger + v\psi_0)\d{x} = 0.
$$

```{code-cell} ipython3
import scipy.linalg
sp = scipy
H = s.get_H()
n_0 = s.get_density().ravel()
psi_0 = 1*s[...].ravel()
A = np.bmat(
    [[H + np.diag(s.g*n_0), np.diag(s.g*psi_0**2)],
     [np.diag(s.g*psi_0.conj()**2), H.conj() + np.diag(s.g*n_0)]])
A = np.asarray(A)
i = np.eye(len(H))
z = np.zeros_like(H)
B = np.asarray(np.bmat([[i, z], [z, -i]]))
C = np.asarray(np.bmat([[z, i], [i, z]]))
I = np.eye(2*len(H))

assert np.allclose(C.dot(C), I)
assert np.allclose(C.dot(A).dot(C), A.conj())
ws, uvs = sp.linalg.eig(np.linalg.inv(B).dot(A))
inds = np.argsort(abs(ws.real))
ws = ws[inds]
uvs = uvs[:, inds]
us, vs = uvs.reshape((2, len(H), 2*len(H)))
```

```{code-cell} ipython3
ws[:10], s.ws[0]
```

```{code-cell} ipython3
s.cooling_phase = 1.0
s.pre_evolve_hook()
s.cooling_phase = 1.0
s.t = 0.0

mode = 4
d = 0.001
u_, v_ = us[:, mode], vs[:, mode]
s[...] = psi_0 + d*(u_ + v_.conj())
w = ws[mode].real
T = 2*np.pi / w 
dt = T/100/100
e = EvolverSplit(s, dt=dt, normalize=False)
log = False
x = s.xyz[0]
with NoInterrupt(ignore=True) as interrupted:
    while not interrupted:
        e.evolve(100)
        plt.clf()
        #e.y.plot(log=log)
        t = e.y.t
        psi = psi_0 + d*(u_*np.exp(1j*w*t) + v_.conj()*np.exp(-1j*w*t))
        plt.plot(x, abs(e.y[...])**2 - abs(psi_0)**2)
        plt.plot(x, abs(psi)**2 - abs(psi_0)**2)

        #plt.ylim(0, 2)
        plt.xlim(-6, 6)
        plt.title("N = {}".format(e.y.get_N()))
        display(plt.gcf())
        clear_output(wait=True)
```

```{code-cell} ipython3
np.allclose(np.linalg.inv(B).dot(A).dot(uvs), ws[None,:]*uvs)
```

## Hydrodynamics

Consider the following GPE-like theory
\begin{gather*}
  \I\hbar e^{\I\eta}\dot{\psi} = \left(
    \frac{-\hbar^2\nabla^2}{2m} + \mathcal{E}'(n)
    + \beta \dot{n}
  \right)\psi.
\end{gather*}
Now apply the Madelung transform $\psi = \sqrt{n}e^{\I\phi}$.  We have the following
derivatives (similarly with spatial derivatives):

:::{margin}
The following identity is useful, with $a=-1/2$ in our case.
\begin{gather*}
  2\frac{(n^an')'}{n^{a-1}} = 2a(n')^2 + 2nn''
\end{gather*}
:::
\begin{gather*}
  \dot{\psi} = \left(\frac{\dot{n}}{2n} + \I\dot{\phi}\right)\psi, \\
  \psi'' = \left(
    \frac{n''}{2n} - \frac{(n')^2}{2n^2} + \I\phi''
  \right)\psi 
  + \left(\frac{n'}{2n} + \I\phi'\right)^2\psi
  = \Biggl(
    \overbrace{\frac{\sqrt{n}''}{\sqrt{n}}}
    ^{\frac{2nn'' - (n')^2}{4n^2}}
    - (\phi')^2
    + \I\frac{(n\phi')'}{n} 
  \Biggr)\psi,\\
  \I\hbar e^{\I\eta}\left(\frac{\dot{n}}{2n} + \I\dot{\phi}\right) = 
  \frac{-\hbar^2}{2m}
  \left(
    \frac{\sqrt{n}''}{\sqrt{n}}
    - (\phi')^2
    + \I\frac{(n\phi')'}{n} 
  \right)
  + \mathcal{E}'(n) + \beta\dot{n}.
\end{gather*}
Consider first $\eta=0$.  Collecting the imaginary and real parts, we have
\begin{gather*}
  \dot{n} = -\vect{\nabla}\cdot\underbrace{
    \Bigl(n\overbrace{\frac{\hbar \vect{\nabla}\phi}{m}}^{\vect{v}}\Bigr)}_{\vect{j}},\\
  - \hbar\dot{\phi} = 
  \frac{-\hbar^2}{2m}
  \left(
    \frac{\nabla^2\sqrt{n}}{\sqrt{n}}
    - (\vect{\nabla}\phi)^2
  \right)
  + \mathcal{E}'(n) + \beta\dot{n}.
\end{gather*}
Introducing the velocity $\vect{v} = \hbar \vect{\nabla}\phi / m$ and current $\vect{j}
= n\vect{v}$, we have the traditional conservation of particle number and Euler equations:
\begin{gather*}
  \dot{n} + \vect{\nabla}\cdot\vect{j} = 0, \qquad
  \hbar\dot{\phi} + \frac{1}{2} m v^2 = 
  \frac{\hbar^2}{2m}
  \frac{\nabla^2\sqrt{n}}{\sqrt{n}}
  - \mathcal{E}'(n) - \beta\dot{n}.
\end{gather*}
:::{margin}
Is this a problem?   The left-hand-side does not look like a convective derivative.  In
index notation:
\begin{gather*}
  \vect{\nabla}\frac{v^2}{2} \neq (\vect{v}\cdot\vect{\nabla}) \vect{v}\\
  \left(\frac{v^2}{2}\right)_{,i} = v_{j}v_{j,i} \neq v_{j}v_{i,j}
\end{gather*}


:::
One typically takes the gradient of the last equation (divided by $m$) to obtain:
\begin{gather*}
  \frac{\hbar}{m}\vect{\nabla}\dot{\phi} 
  + \vect{\nabla}\frac{v^2}{2}
  = 
  \dot{\vect{v}}
  + \vect{\nabla}\frac{v^2}{2}
  =
  \underbrace{
    \frac{-\vect{\nabla}}{m}
    \overbrace{
      \left(
        \frac{-\hbar^2}{2m}
        \frac{\nabla^2\sqrt{n}}{\sqrt{n}}
        + \mathcal{E}'(n) + \beta\dot{n}
      \right)
    }^{V}
  }_{\vect{F}/m = -\vect{\nabla}V/m}
\end{gather*}

:::{admonition} Convective Derivative
:class: dropdown

We have one complication here -- the typical convective derivative is:
\begin{gather*}
  D_t\vect{A} = \dot{\vect{A}} + \vect{v}\cdot\vect{\nabla}\vect{A},
\end{gather*}
but the second term is not generally equivalent to $\vect{\nabla}v^2/2$.  Specifically,
in index form, we have
\begin{gather*}
  \dot{v}_i + \nabla_{i}\frac{v^2}{2} 
  = \dot{v}_i + v_j\nabla_{i}v_j
  = D_t v_i + v_j\nabla_{i}v_j - v_j\nabla_{j}v_i
  = D_t v_i + \underbrace{v_j(\nabla_{i}v_j - \nabla_{j}v_i)}_{\propto \vect{v}\cdot(\vect{\nabla}\times\vect{v})}.
\end{gather*}
This correction, however, vanishes for potential flow $v \propto \vect{\nabla}\phi$
since the divergence of a curl vanishes.  With indices, this is obvious:
\begin{gather*}
  \nabla_{i}v_j - \nabla_{j}v_i \propto
  \nabla_{i}\nabla_{j}\phi - \nabla_{j}\nabla_{i}\phi = 0.
\end{gather*}
:::

Summarizing, we have
\begin{gather*}
  \dot{n} + \vect{\nabla}\cdot\vect{j} = 0,\qquad
  mD_t\vect{v} = -\vect{\nabla}V,\\
\end{gather*}
where
\begin{gather*}
  D_t A = \dot{A} + (\vect{v}\cdot\vect{\nabla})A, \qquad
  V = \frac{-\hbar^2}{2m}\frac{\nabla^2\sqrt{n}}{\sqrt{n}}
      + \mathcal{E}'(n) + \beta\dot{n}.
\end{gather*}

## Navier Stokes Equations
The [Navier-Stokes equations][] are usually expressed in terms of the [Cauchy stress
tensor][] $\mat{\sigma}$ 
\begin{gather*}
  \dot{n} + \vect{\nabla}\cdot\vect{j} = 0,\qquad
  \underbrace{mn}_{\rho}D_t\vect{v}
  = \underbrace{\vect{\nabla}\cdot\mat{\sigma}}
              _{-n\vect{\nabla}V}.
\end{gather*}
:::{margin}
The [dynamic viscosity][] is also called the shear viscosity, but can impact non-shear
flow, so this term is not recommended.
*(I had this, but think it is wrong: "The viscosity $\lambda = \zeta -
\tfrac{2}{3}\mu$ is called the [second viscosity][volume viscosity]")*.
:::
This can be expressed in terms of the pressure $p$ and the [deviatoric stress][]
$\mat{\tau}$, the latter of which can be expressed in terms of the [dynamic viscosity][]
$\mu$ and the [bulk viscosity][] $\zeta = \lambda + \tfrac{2}{3}\mu$: 
\begin{gather*}
  \mat{\sigma} = -p\mat{1} + \mat{\tau},\\
  \mat{\tau} = \lambda (\vect{\nabla}\cdot\vect{v})\mat{1} 
  + \mu\bigl(\vect{\nabla}\mat{v} + (\vect{\nabla}\mat{v})^T\bigr)
  =
  \zeta (\vect{\nabla}\cdot\vect{v})\mat{1} + \mu\left[
    \vect{\nabla}\mat{v} 
    + (\vect{\nabla}\mat{v})^T - \tfrac{2}{3}(\vect{\nabla}\cdot\vect{v})\mat{1}
  \right].
\end{gather*}
:::::{admonition} Viscosity Coefficients
There are various different forms and terms for the viscosity coefficients.  In addition
to those listed here, we have
* **First viscosity** or **[dynamic viscosity][].** This we have above $\mu$ but is also referred to as the
  **standard viscosity** and **scale viscosity**, such as in the [Wikipedia article on
  viscosity](https://en.wikipedia.org/wiki/Viscosity#General_definition).  That same
  article uses the symbols $\alpha\equiv \lambda$ (with $\kappa \equiv \zeta$), and
  $\beta \equiv \gamma \equiv \mu$.  This can also be normalized by the mass density, in
  what is called the **kinematic viscosity:**
  \begin{gather*}
    \nu = \frac{\mu}{\rho} = \frac{\mu}{mn}.
  \end{gather*}
* **Second viscosity** or **bulk viscosity** is sometimes called **[volume
  viscosity][]**, or **dilatatic viscosity** with common symbols
  $\zeta \equiv \mu' \equiv \mu_{b} \equiv \kappa \equiv \xi$. 
:::::
:::::{admonition} 1D Flow
If we restrict our attention to 1D compressible flow with viscosity, we have
\begin{gather*}
  \dot{n} + (nv)' = 0,\qquad
  mn(\dot{v} + vv') = \Bigl(-p + (\lambda + 2\mu) v'\Bigr)'
\end{gather*}
For comparison, consider Eq. (9.38) of {cite}`Nazarenko:` which has the net viscosity
coefficient $(\xi + 4\mu/3)u_{,xx}$ (although with a typo in the derivative) where $\mu$
and $\xi$ are the **first** and **second** viscosity coefficients.

\begin{gather*}
  \mat{\tau} = 
  \lambda(v_{x,x} + v_{y,y} + v_{z,z})\mat{1}
  +
  \mu
  \begin{pmatrix}
    2v_{x,x} & v_{x,y} + v_{y,x} & v_{x,z} + v_{z,x}\\
    v_{x,y} + v_{y,x} & 2v_{y,y} & v_{y,z} + v_{z,y}\\
    v_{x,z} + v_{z,x} & v_{y,z} + v_{z,y} & 2v_{z,z}
  \end{pmatrix}
\end{gather*}

:::::


:::{margin}
I am not sure if this can be done with the quantum pressure.  I.e., what $p$ satisfies
\begin{gather*}
  \psi^2(\psi_{,jj}/\psi)_{,i} = p_{,i}\\
  \psi\psi_{,jji} - \psi_{,jj}\psi_{,i} = p_{,i},
\end{gather*}
where $\psi = \sqrt{n}$?
:::
In terms of our equations, this requires expressing our potential in terms of the
pressure:
\begin{gather*}
  n\vect{\nabla}V = \vect{\nabla}p.
\end{gather*}
:::{margin}
Here $\mu$ is the chemical potential, not a viscosity.
:::
For example, using the equation of state:
\begin{gather*}
  p(n) = \underbrace{n\mathcal{E}'(n)}_{\mu n} - \mathcal{E}(n).
\end{gather*}
:::{admonition} Do it!  Check that $\vect{\nabla}p = n\vect{\nabla}\mathcal{E}'(n)$.
:class: dropdown
\begin{gather*}
  \vect{\nabla}p = (\vect{\nabla}n)\mathcal{E}'(n) + n\vect{\nabla}\mathcal{E}'(n) 
  - \vect{\nabla}\mathcal{E}(n)\\
  =(\vect{\nabla}n)\mathcal{E}'(n) + n\vect{\nabla}\mathcal{E}'(n) 
  - \mathcal{E}'(n)\vect{\nabla}n\\
  = n\vect{\nabla}\mathcal{E}'(n) = n\mathcal{E}''(n)\vect{\nabla}n.
\end{gather*}
:::

\begin{gather*}
  n((nv_{j})_{,j})_{,i} = \tau_{ji,j} =
  n(n_{,j}v_{j} + n v_{j,j})_{,i} = 
  n(n_{,ij}v_{j} + n_{,j}v_{j,i} + n_{,i} v_{j,j} + n v_{j,ij})
\end{gather*}





In particular, we have
\begin{gather*}
  \vect{\nabla}\cdot\mat{\tau} = -\beta n\vect{\nabla}\dot{n}
  = \beta n\vect{\nabla}\bigl(\vect{\nabla}\cdot (n\vect{v})\bigr)
\end{gather*}

\begin{gather*}
  \nabla_{i}\tau_{ij} = 
  \lambda \nabla_{j}\nabla_{i}v_i
  + \mu(\nabla^2 v_{j} + \nabla_{j}\nabla_{i}v_{i})
  =
  \zeta\nabla_{j}\nabla_{i}v_i
  + \mu(\nabla^2 v_{j} + \tfrac{1}{3}\nabla_{j}\nabla_{i}v_{i})
  \\
  = \beta n\nabla_{j}\bigl(\nabla_{i}(nv_i)\bigr)
  = \beta n\nabla_{j}\bigl(v_i\nabla_{i}n + n\nabla_{i}v_i)\bigr)\\
  = \beta \bigl(
    n(\nabla_{j}v_i)\nabla_{i}n + nv_i\nabla_{i}\nabla_{j}n 
    + n(\nabla_{j}n)\nabla_{i}v_i + 
    n^2\nabla_{j}\nabla_{i}v_i\bigr)
\end{gather*}

Let's try in 1D
\begin{gather*}
  \tau = (\lambda + 2\mu) v' = (\zeta - 4/3\mu) v',\\
  \tau' = (\lambda + 2\mu) v'' =
  \beta (nn''v + 2nn'v' + n^2v'')
\end{gather*}

Thus, the quantum friction plays some role similar to viscosity, but there are
additional terms.  Note that these vanish if the density is constant.







[Navier-Stokes equations]: <https://en.wikipedia.org/wiki/Navier%E2%80%93Stokes_equations>
[Cauchy stress tensor]: <https://en.wikipedia.org/wiki/Cauchy_stress_tensor>
[deviatoric stress]: <https://en.wikipedia.org/wiki/Cauchy_stress_tensor#Stress_deviator_tensor>
[bulk viscosity]: <https://en.wikipedia.org/wiki/Volume_viscosity>
[dynamic viscosity]: <https://en.wikipedia.org/wiki/Viscosity#Dynamic_viscosity>
[volume viscosity]: <https://en.wikipedia.org/wiki/Volume_viscosity>

## Hi





[General Leibniz rule]: <https://en.wikipedia.org/wiki/General_Leibniz_rule>
