"""Common imports to simplify notebooks.

Usage::

    from gpe.imports import *
"""

try:
    from importlib import reload
except ImportError:
    pass

import IPython
from IPython.display import display, clear_output
import numpy as np

# Fix annoying warnings about fonts on OS X
# https://stackoverflow.com/a/58393562/1088938
import logging

logging.getLogger("matplotlib.font_manager").disabled = True

from matplotlib import pyplot as plt  # noqa: E402

from mmfutils.contexts import NoInterrupt  # noqa: E402
from mmfutils.plot import imcontourf  # noqa: E402
from gpe.minimize import MinimizeState  # noqa: E402
from gpe.utils import evolve_to, evolve, evolves  # noqa: E402
from gpe.plot_utils import MPLGrid  # noqa: E402
from pytimeode.evolvers import EvolverSplit, EvolverABM  # noqa: E402

ip = IPython.get_ipython()
try:
    ip.run_line_magic("matplotlib", "inline")
except Exception:
    print("Could not run %matplotlib inline.  Continuing...")
del ip

__all__ = [
    "np",
    "plt",
    "display",
    "clear_output",
    "NoInterrupt",
    "EvolverSplit",
    "EvolverABM",
    "evolve_to",
    "evolve",
    "evolves",
    "MinimizeState",
    "reload",
    "imcontourf",
    "MPLGrid",
]
