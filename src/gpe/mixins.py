"""Mixins to help implement required interfaces from minimal implementations."""

__all__ = ["StateMixin"]


class StateMixin:
    experiment = None
    t__final = 0.0
