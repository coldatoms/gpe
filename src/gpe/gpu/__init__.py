import os

try:
    if "PYTEST_CURRENT_TEST" in os.environ:
        import pytest

        cupy = pytest.importorskip("cupy")
        cupyx = pytest.importorskip("cupyx")
    import cupy, cupyx
except ImportError:
    cupy = None
    cupyx = None
