"""Keep this file in the top level directory - it allows pytest to see the
project even if it is not installed.

https://stackoverflow.com/a/20972950
"""

try:
    import cupy
except ImportError:
    import pytest

    def pytest_collection_modifyitems(config, items):
        skip_gpu = pytest.mark.skip(reason="Could not import cupy.")
        for item in items:
            if ".gpu" in item.name:
                item.add_marker(skip_gpu)
