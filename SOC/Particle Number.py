# ---
# jupyter:
#   jupytext:
#     formats: ipynb,py
#     text_representation:
#       extension: .py
#       format_name: light
#       format_version: '1.5'
#       jupytext_version: 1.4.0
#   kernelspec:
#     display_name: Python [conda env:_gpe3]
#     language: python
#     name: conda-env-_gpe3-py
# ---


# + init_cell=true
import mmf_setup

mmf_setup.nbinit()
import sys, os
import SOC

_SOC_PATH = os.path.dirname(SOC.__file__)
if _SOC_PATH not in sys.path:
    sys.path.append(_SOC_PATH)
from gpe.imports import *
from mmfutils.plot import imcontourf

# -

# # Start
#

# Our simulations are designed to match a specific experiment, however, approximations and error that separate the two. Its possible to have simulations and experiments with the same particle number but different densities.
#
# This not book is designed to compare some of these inconsistencies.
#

#

# # Tube v Axial

# +
# %pylab inline --no-import-all
from gpe.soc import ExperimentBarrier, u
import SOC.soc_car_fast  # ;reload(SOC.soc_car_fast)
from SOC.soc_car_fast import ExperimentCARFast, ExperimentCARFastSmall


from gpe.plot_utils import MPLGrid

plt.figure(figsize=(15, 4))
grid = MPLGrid(direction="down", space=0.0)


# Sim data
import runs_car_fast_final

r0 = runs_car_fast_final.Run_IPG_xTF150()
r1 = runs_car_fast_final.Run_IPG_tube()

barrier_depth_nK = -30

sim0 = [
    _sims
    for _sims in r0.simulations
    if _sims.experiment.barrier_depth_nK == barrier_depth_nK
][0]
sim1 = [
    _sims
    for _sims in r1.simulations
    if _sims.experiment.barrier_depth_nK == barrier_depth_nK
][0]


t_ = 0
s0 = sim0.get_state(t_=t_, image=False)
s1 = sim1.get_state(t_=t_, image=False)
plt.plot(s0.xyz[0], s0.get_density_x().sum(axis=0), label=r"Axial")
plt.plot(s0.xyz[0], s0.get_density_x()[0], label=r"Axial a")
plt.plot(s0.xyz[0], s0.get_density_x()[1], label=r"Axial b")
plt.plot(s1.xyz[0], s1.get_density().sum(axis=0), label=r"Tube")
plt.plot(s1.xyz[0], s1.get_density()[0], label=r"Tube a")
plt.plot(s1.xyz[0], s1.get_density()[1], label=r"Tube b")
title = r"t_wait$={:.1f}$ms, $\qquad $ n_axial={:.0f}, $\qquad $n_tube={:.0f}, $\qquad $Barrier Depth$={}$nK ".format(
    t_ - 10, s0.get_N(), s1.get_N(), barrier_depth_nK
)
plt.title(title)
# plt.xlim(-160,160)
plt.xlabel(r"$x$ [micron]", size="xx-large")
plt.legend()
display()
clear_output(wait=True)

print(s0.get_N(), s1.get_N(), s0.get_Ns(), s1.get_Ns())
assert np.allclose(s0.get_N(), s1.get_N())
assert np.allclose(s0.get_Ns(), s1.get_Ns())
# -


262.64674


print(s.mus)


# +
# ws = np.arange(262.6467,272.6468,.00001)
# ws = np.arange(-10,0,0.1)
# for _ws in ws:

args = dict(
    barrier_depth_nK=barrier_depth_nK,
    cooling_phase=1.0,
    trapping_frequencies_Hz=(3.49, 278, 278),
    # trapping_frequencies_Hz=(3.49, _ws, _ws),
    x_TF=150.0 * u.micron,
    # Lx = 480*u.micron
    # Nx = 2**13
    # detuning_kHz=0,
    # rabi_frequency_E_R=0,
    # mu=_ws,
)

eeT = ExperimentCARFast(**args, tube=True, basis_type="1D")
eeA = ExperimentCARFast(**args, basis_type="axial")

sT = eeT.get_state()
sA = eeA.get_state()

# ss = ee.get_initial_state()
print(sT.get_N(), sA.get_N(), sT.get_N() - sA.get_N())
print(eeT.fiducial_V_TF, eeA.fiducial_V_TF)

plt.plot(sA.xyz[0], sA.get_density_x().sum(axis=0), label=r"Axial")
plt.plot(sT.xyz[0], sT.get_density_x().sum(axis=0), label=r"Tube")

# -

sA.basis.Nxr

sT.get_mu_from_V_TF(eeT.fiducial_V_TF), sA.get_mu_from_V_TF(eeA.fiducial_V_TF)

eeT.fiducial_V_TF

s.get_N(), ss.get_N()

sT.get_ws_perp(), sT.w0_perp

sA.ws

sA.basis


# +
# %pylab inline --no-import-all
from gpe.bec import State, u
from gpe.tube import StateGPEdrZ


args = dict(
    cooling_phase=1.0,
    ws=2 * np.pi * np.asarray([3.49, 278, 278]) * u.Hz,
    x_TF=150.0 * u.micron,
    Lxyz=(480 * u.micron, 6 * u.micron, 6 * u.micron),
)


class StateTube(StateGPEdrZ):
    def get_ws_perp(self, t):
        return self.ws[1:]


s3 = StateTube(Nxyz=(8000,), **args)
s4 = State(Nxyz=(8000, 64, 64), **args)

print(s3.get_N(), s4.get_N(), s3.get_N() - s4.get_N())
# print( eeT.fiducial_V_TF, eeA.fiducial_V_TF )

plt.plot(s4.xyz[0].ravel(), s4.get_density_x(), label=r"3D")
plt.plot(s3.xyz[0].ravel(), s3.get_density_x(), label=r"Tube")

ee = State()
# -

s3.g, sA.gs

plt.plot(s4.xyz[0].ravel(), s4.get_density()[:, 32, 32])
plt.plot(s3.xyz[0].ravel(), s3.get_central_density(TF=True))


s3.get_central_density().shape, s3.get_density().shape

s3.shape, s4.shape

s3.data.shape


self = sA
n = self.get_density()
# n.shape, self.xyz[0].shape
for _x in reversed(self.xyz[1:]):
    n = np.trapz(n, _x, axis=-1)

# +
n = self.get_density()

x, y, z = self.xyz
n = np.trapz(n, z, axis=-1)
n = np.trapz(n, y.ravel(), axis=-1)
n.shape
# -

y


sA.xyz[0].shape, sA.get_density_x().shape

sA.basis.Nxyz
