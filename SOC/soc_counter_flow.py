import numpy as np
import scipy.interpolate
from scipy.stats.mstats import gmean

sp = scipy

from gpe import utils, bec2, tube2
from gpe.minimize import MinimizeState
from gpe.soc import ExperimentBarrier, u
from gpe.utils import _GPU
from soc_catch_and_release import ExperimentCatchAndRelease

from pytimeode.evolvers import EvolverABM

__all__ = ["ExperimentCounterFlow"]


class ExperimentCounterFlow(ExperimentCatchAndRelease):
    """
    Experimental Parameters from paper
    https://journals.aps.org/prl/abstract/10.1103/PhysRevLett.111.264101
        alt: https://journals.aps.org/pra/abstract/10.1103/PhysRevA.84.041605
             https://journals.aps.org/prl/abstract/10.1103/PhysRevLett.106.065302

    In this paper
    The y direction is vertical
    The z direction is axial

    N = 450000 Rb87
    up = |1,-1>
    down = |2,0>

    wx,wy,wz = 2*np.pi*np.array([178,145,1.5]) Hz
        thus
        wx,wy,wz = 2*np.pi*np.array([1.5, 178, 145]) Hz


    rabi_requency_0 = 7.4kHz

    Magnetic feild:
        By = 1G
        Bz = 0.017 G/cm * (z-z0)


    detuning(x) = m*x + 1.3
    detuning(x=0) = 1.3kHz
    detuning(x=-outside_the_cloud) = 0
    m = 1.3/x_TF

    In 87Rb, the linear Zeeman effect leads to a shift of approximately 700 kHz/G



    Rabi_freq(x) = np.sqrt(rabi_requency_0**2 + \Delta**2)







    Attributes
    ----------
    t__wait : float
       If this is `inf`, then we start in the ground state including the
       attractive central beam, otherwise, we start in the ground state without
       the central beam, and then wait for this time while the central beam is
       on before turning it off.

    """

    t_unit = u.ms
    x_TF = 400 * u.micron  # Thomas Fermi "radius" (where V(x_TF) = mu)
    B_gradient_mG_cm = 0.017 * 1000  # Linear gradient

    detuning_kHz = 1.3
    detuning_E_R = None
    rabi_frequency_kHz = 7.4
    rabi_frequency_E_R = None  # Rabi frequency
    recoil_frequency_Hz = 1e-32

    initial_imbalance = 0.0  # Start in true ground state.

    species = ((1, -1), (2, 0))

    trapping_frequencies_Hz = 2.0 * np.pi * np.array([1.5, 178, 145])
    trapping_wavelength_nm = 1064

    harmonic_trap = True  # If False, use the real trap: see get_Vtrap()

    # barrier_width_micron = 0    # Width of central Gaussian beam
    barrier_k_k_r = 0.0  # Barrier lattice momentum
    barrier_depth_nK = 0  # Depth of barrier (25uW)  (100uW=-210nK)
    barrier_x = 0.0  # Position of barrier
    t__barrier = 0  # Wait time after turning on the central beam

    # gaussian = False

    t__wait = 1800  # Time to wait before imaging
    t__step = 0.1  # Time to turn on barrier
    t__image = 10.1  # Time for expansion


@_GPU.add_non_GPU_methods
class StateMixin:
    def __init__(self, experiment, **kw):
        self.experiment = experiment

        self._time_independent_Vext = False
        self._Vext = [None, None]  # Cache for performance
        super().__init__(**kw)

    ######################################################################
    # Required by base State classes.
    def get_Vext_GPU(self):
        if (
            self._Vext
            and self._Vext[1] is not None
            and (self.t == self._Vext[0] or self._time_independent_Vext)
        ):
            Va, Vb, Vab = self._Vext[1]
        else:
            _Vext = self.experiment.get_Vext(state=self)
            self._Vext = [self.t, _Vext]
            Va, Vb, Vab = _Vext

        return (Va, Vb, Vab)

    ######################################################################
    # Required for using tube codes.
    def get_ws_perp(self, t=None):
        """Return the frequencies at time `t`.  Required by tube2 classes."""
        if t is None:
            t = self.t
        ws_perp = self.experiment.get("ws", t_=t / self.experiment.t_unit)[1:]
        if t <= self.t_final:
            return ws_perp
        else:
            return np.zeros_like(ws_perp)

    @property
    def t_final(self):
        # Convenience access to expansion time.
        return self.experiment.t__final * self.experiment.t_unit


class State2(StateMixin, bec2.StateBase):
    pass


class State2Tube(StateMixin, tube2.StateGPEdrZ):
    pass


class ExperimentCF(utils.ExperimentBase):
    t_unit = u.ms
    t_name = "ms"
    t__final = 1800  # Time to wait before imaging
    t__image = 10.1  # Time for expansion

    dx = 0.1 * u.micron
    x_TF = 400 * u.micron  # Thomas Fermi "radius" (where V(x_TF) = mu)
    Lx = 1000 * u.micron
    B_gradient_mG_cm = 0.017 * 1000  # Linear gradient

    detuning_kHz = 1.3

    species = ((1, -1), (2, 0))

    trapping_frequencies_Hz = np.array([1.5, 145, 178])
    rabi_frequency_kHz = 7.4

    initial_imbalance = 0.0  # Fractional populations in initial state.
    cooling_phase = 1.0

    basis_type = "1D"
    tube = False

    @property
    def dim(self):
        if self.basis_type == "1D":
            return 1
        elif self.basis_type in set(["2D", "axial"]):
            return 2
        elif self.basis_type == "3D":
            return 3
        else:
            raise ValueError(
                "Unknown basis_type={} (use one of '1D', '2D', '3D', or 'axial')".format(
                    self.basis_type
                )
            )

    ######################################################################
    # Methods required by IExperiment
    def init(self):
        self.ws = 2 * np.pi * self.trapping_frequencies_Hz * u.Hz
        self.Omega0 = 2 * np.pi * u.hbar * self.rabi_frequency_kHz * u.kHz
        self.Delta0 = 2 * np.pi * u.hbar * self.detuning_kHz * u.kHz

        if self.dx is not None:
            # Special case to calculate Nxyz in terms of a lattice spacing
            self.Nx = utils.get_good_N(self.Lx / self.dx)
        else:
            self.dx = self.Lx / self.Nx

        _a, _b = self.species
        scattering_lengths = np.array(
            [u.scattering_lengths[_k] for _k in [(_a, _a), (_b, _b), (_a, _b)]]
        )
        self.mu_Bs = (u.magnetic_moments[_a], u.magnetic_moments[_b])
        self.ms = (u.masses[_a], u.masses[_b])
        m = np.mean(self.ms)

        assert np.allclose(self.ms, m)
        self.gs = 4 * np.pi * u.hbar**2 / m * scattering_lengths
        self.B_gradient = self.B_gradient_mG_cm * u.mG / u.cm

        super().init()

    def get_state(self, initialize=False):
        """Quickly return an appropriate initial state."""
        state_args = dict(
            experiment=self,
            x_TF=None,
            cooling_phase=self.cooling_phase,
            t=0.0,
            gs=self.gs,
            ms=self.ms,
            constraint="N",
        )

        Lx = self.Lx
        Nx = self.Nx

        if self.basis_type == "1D" and self.tube:
            state = State2Tube(Nxyz=[Nx], Lxyz=[Lx], **state_args)
        elif self.basis_type == "axial":
            from mmfutils.math.bases import CylindricalBasis

            if self.R is None:
                ws = self.ws_expt
                wx, wr = ws[0], gmean(ws[1:])
                a_perp = u.hbar / wr / min(self.ms)

                # Maximum of radial width or 4*a_perp where a Gaussian would
                # fall off by a factor of 1e-7.
                R = max(4 * a_perp, self.Lx_expt / 2.0 * wx / wr)
            else:
                R = self.R

            if self.Nr is None:
                Nr = int(np.ceil(R / self.dx))
            else:
                Nr = self.Nr

            Nxr = [Nx, Nr]
            Lxr = [Lx, R]
            basis = CylindricalBasis(Nxr=Nxr, Lxr=Lxr, symmetric_x=False, boost_px=0.0)
            state = State2Axial(basis=basis, **state_args)
        else:
            Nxyz = getattr(self, "Nxyz", None)
            Lxyz = getattr(self, "Lxyz", None)
            if Nxyz is None or Lxyz is None:
                ws = self.ws[: self.dim]
                wx, ws = ws[0], ws[1:]
                if self.basis_type == "2D":
                    ws = [gmean(ws)]
                aspect_ratios = np.divide(wx, ws)

                if Lxyz is None:
                    Lxyz = [Lx] + [Lx * _aspect for _aspect in aspect_ratios]
                else:
                    Lxyz = [Lx] + list(Lxyz[1:])
                if Nxyz is None:
                    Nxyz = [Nx] + [utils.get_good_N(_L / self.dx) for _L in Lxyz[1:]]
            state = State2(Nxyz=Nxyz, Lxyz=Lxyz, **state_args)

        return state

    def get_initial_state(
        self,
        E_tol=1e-12,
        psi_tol=1e-12,
        disp=1,
        tries=20,
        cool_steps=100,
        cool_dt_t_scale=0.1,
        t_=0,
        minimize=True,
        **kw
    ):
        """Return an initial state with the specified population fractions.

        This initial state is prepared in state[0] with the potentials
        as they are at time `t=0`, then the `initial_imbalance` is
        transferred as specified simulating an RF pulse by simply the
        appropriate fraction in each state.  Phases are kept the same
        as in the state[0].
        """
        state = self.get_state()
        state.cooling_phase = 1.0

        # Cool to minimum with only component 0 then RF transfered
        state.constraint = "N"
        state[0, ...] = np.sqrt((abs(state[...]) ** 2).sum(axis=0))
        state[1, ...] = np.sqrt(self.initial_imbalance) * state[0, ...]
        state[0, ...] *= np.sqrt(1 - self.initial_imbalance)
        fix_N = True

        state.init()
        t = t_ * self.t_unit
        state.t = t

        if minimize:
            m = MinimizeState(state, fix_N=fix_N)
            print("Minimize check:")
            m.check()
            self._debug_state = m  # Store in case minimize fails
            if "use_scipy" not in kw:
                kw["tries"] = tries
            state = m.minimize(E_tol=E_tol, psi_tol=psi_tol, disp=disp, **kw)

        self._debug_state = state  # Store in case evolve fails

        if cool_steps > 1:
            # Cool a bit to remove any fluctuations.
            state.cooling_phase = 1j
            dt = cool_dt_t_scale * state.t_scale
            state.t = -dt * cool_steps
            evolver = EvolverABM(state, dt=dt)
            evolver.evolve(cool_steps)
            state = evolver.get_y()

        del self._debug_state

        psi0 = state[...]
        # Rely on get_state for all other parameters like t, cooling_phase etc.
        self._state = state = self.get_state()
        state[...] = psi0
        state.t = t
        return state

    # End of methods required by IExperiment
    ######################################################################

    def get_Vext(self, state, fiducial=False, expt=False):
        """Return (V_a, V_b, V_ab), the external potentials.

        For `t_ > self.t__final`, all the potentials are set to zero.

        Arguments
        ---------
        state : IState
           Current state.  Use this to get the time `state.t` and
           abscissa `state.basis.xyz`.
        fiducial : bool
           If `True`, then return the potential that should be used to
           define the initial state in terms of the Thomas Fermi
           radius of the cloud `x_TF`.
        expt : bool
           If `True`, then return the proper experimental potential
           rather than the potential used in the simulation.

        See Also
        --------
        * interface.IExperiment.get_Vext

        """
        zero = np.zeros(state.shape)
        # Convert times into experiment units
        t_ = state.t / self.t_unit

        Omega0 = self.get("Omega0", t_=t_)
        Delta0 = self.get("Delta0", t_=t_)

        if t_ > self.t__final:
            _Vext = (zero,) * 3
            return _Vext

        xyz = state._xyz_

        Va, Vb = self.get_Vtrap(state=state, xyz=xyz)
        x = xyz[0]

        B_gradient = self.get("B_gradient", t_=t_)

        V_Bs = (B_gradient * np.asarray(self.mu_Bs)[state.bcast]) * (x[None, ...])
        V_ab = Omega0 + zero

        _Vext = (Va + Delta0 + V_Bs[0], Vb + V_Bs[1], V_ab)

        return _Vext

    def B_gradient_t_(self, t_):
        if t_ <= 0:
            return 0
        else:
            return self.B_gradient

    def Omega0_t_(self, t_):
        if t_ <= 0:
            return 0
        else:
            return self.Omega0

    def get_Vtrap(self, state, xyz):
        t_ = state.t / self.t_unit
        ws = self.get("ws", t_=t_)
        V_m = 0.5 * sum((_w * _x) ** 2 for _w, _x in zip(ws, xyz))
        return state.ms[state.bcast] * V_m


class Homogeneous:
    """Simple solver for Homogeneous matter."""

    hbar = 1.0
    Omega = 1.0
    delta = 1.0
    k_R = 1.0
    gs = (1.0, 1.0, 1.0)
    ms = (2.0, 2.0)

    def __init__(self, **kw):
        for k in kw:
            assert hasattr(self, k)
            setattr(self, k, kw[k])

    def get_homogeneous_state(self, p, n, band=0, tol=1e-12):
        """Return (mu, (n_a, n_b)) for the homogeneous state."""
        Omega = self.Omega
        delta = self.delta
        gaa, gbb, gab = self.gs
        k_R = self.k_R
        m_a, m_b = self.ms
        Ka = (p + self.hbar * k_R) ** 2 / 2 / m_a - delta / 2
        Kb = (p - self.hbar * k_R) ** 2 / 2 / m_b + delta / 2

        def iterate(na, nb):
            n = na + nb
            H = np.array(
                [
                    [Ka + gaa * na + gab * nb, Omega / 2],
                    [Omega / 2, Kb + gab * na + gbb * nb],
                ]
            )
            mus, psis = np.linalg.eigh(H)
            ns = n * abs(psis) ** 2
            return mus[band], ns[:, band]

        ns = np.array([n / 2, n / 2])
        ns0 = 0
        while max(abs(ns - ns0) / n) > tol:
            ns0 = ns
            mu, ns = iterate(*ns)
        return mu, ns

    def get_phonon_dispersion(self, p, n, band=0, tol=1e-12):
        Omega = self.Omega
        delta = self.delta
        gaa, gbb, gab = self.gs
        mu, ns = self.get_homogeneous_state(p=p, n=n, band=band, tol=tol)
        na, nb = ns
        gnab = gab * np.sqrt(na * nb)
        gnaa = gaa * na
        gnbb = gbb * nb
        hbar = self.hbar
        k_R = self.k_R
        m_a, m_b = self.ms

        def Eph(q):
            Ka_p = (
                (p + q + hbar * k_R) ** 2 / 2 / m_a
                - delta / 2
                - mu
                + gaa * na
                + gab * nb
            )
            Ka_m = (
                (p - q + hbar * k_R) ** 2 / 2 / m_a
                - delta / 2
                - mu
                + gaa * na
                + gab * nb
            )
            Kb_p = (
                (p + q - hbar * k_R) ** 2 / 2 / m_b
                + delta / 2
                - mu
                + gab * na
                + gbb * nb
            )
            Kb_m = (
                (p - q - hbar * k_R) ** 2 / 2 / m_b
                + delta / 2
                - mu
                + gab * na
                + gbb * nb
            )

            A = np.array(
                [
                    [Ka_p + gnaa, gnaa, Omega / 2 + gnab, gnab],
                    [gnaa, Ka_m + gnaa, gnab, Omega / 2 + gnab],
                    [Omega / 2 + gnab, gnab, Kb_p + gnbb, gnbb],
                    [gnab, Omega / 2 + gnab, gnbb, Kb_m + gnbb],
                ]
            )
            B = np.diag([1, -1, 1, -1])
            ws = sp.linalg.eigvals(a=A, b=B)
            return ws

        return Eph
