# ---
# jupyter:
#   jupytext:
#     formats: ipynb,py
#     text_representation:
#       extension: .py
#       format_name: light
#       format_version: '1.5'
#       jupytext_version: 1.4.0
#   kernelspec:
#     display_name: Python [conda env:_gpe]
#     language: python
#     name: conda-env-_gpe-py
# ---

# + init_cell=true
import mmf_setup

mmf_setup.nbinit()
from gpe.imports import *

# -

# Here we attempt to create variants of a dumb hole by using a spatially dependent detuning.
#
# The code is in [`soc_dumb_hole.py`](soc_dumb_hole.py) which sets up a 1D experiment (by 1D we actually mean 3D but homogeneous in $y$ and $z$ so the only variations are along $x$.)
#
# We provide two interfaces - the default sets parameters like the mass `m` and initial density `n0`.  Currently units are defined by:
#
# * `hbar`: Planck's constant.
# * `m`: The mass.
# * `k_r`: The recoil wave-vector $k_R$.  Sets the length scale $1/k_R$ and energy scale $E_R = \hbar^2k_R^2/2m$.
# * `n_0`: The background density $n_0$ used to set the initial state.
#
# The simulation is described by
#
# * `dx`: The lattice spacing (should be less than the healing length, etc.)
# * `Lx`: The box size.
#
# These are used internally.  However, one can instead specify more physically interesting parameters include the following which will adjust these as follows:
#
# * `d`: Used to set the background detuning $\delta = d 4E_R$.
# * `w`: Used to set the background coupling $\Omega = w 4E_R$.  Together, $d$ and $w$ are the most natural parameters for describing the single-particle dispersion:
#   $$
#     \frac{E_\pm(\hbar k k_r)}{E_R} = \frac{k^2 + 1}{2} \pm \sqrt{(k-d)^2 + w^2}.
#   $$
#
# * `healing_length`: $\xi_h = \sqrt{\hbar^2/2m\mu_0}$.
# * `c_s`: Speed of sound $c_s = \sqrt{\mu_0/m}$.
#
# If these are defined (not `None`), then we use them in the following relations depending on which quantities we are given.
#
# \begin{align}
#     (c_s)&:& \mu_0 &= mc_s^2, & g &= \mu_0/n_0,\\
#     (\xi_h)&:& \mu_0 &= \hbar^2/2m\xi_h^2, & g &= \mu_0/n_0,\\
#     (c_s, \xi_h)&:& \mu_0 &= \hbar^2/2m\xi_h^2, & m &= \hbar/\sqrt{2}\xi_h c_s, &g &= \mu_0/n_0.
# \end{align}

import soc_dumb_hole

soc_dumb_hole.ExperimentDumbHole

import soc_dumb_hole

reload(soc_dumb_hole)
e = soc_dumb_hole.ExperimentDumbHole(w=1.0)
E = e.get_dispersion()
k_kRs = np.linspace(-3, 3, 100)
plt.figure(figsize=(10, 3))
plt.subplot(121)
plt.plot(k_kRs, E(k_kRs)[0])
plt.xlabel("k/k_R")
plt.ylabel("E/E_R")
plt.subplot(122)
plt.plot(s.xyz[0], e.get_delta_t_(1.0, s))
plt.xlabel("x")
plt.ylabel("$\delta(x)$")

e = soc_dumb_hole.ExperimentDumbHole(ddelta_k_r=-2.0, dx=0.3, Lx=100.0)
s = e.get_initial_state()

s.experiment.ddelta_k_r = -1.3
s.cooling_phase = 1
evolver = EvolverABM(s, dt=0.5 * s.t_scale)
with NoInterrupt() as interrupted:
    while not interrupted:
        evolver.evolve(100)
        plt.clf()
        evolver.y.plot()
        plt.twinx()
        plt.plot(evolver.y.xyz[0], evolver.y.experiment._d_x, ":y")
        display(plt.gcf())
        clear_output(wait=True)
