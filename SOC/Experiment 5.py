# ---
# jupyter:
#   jupytext:
#     formats: ipynb,py
#     text_representation:
#       extension: .py
#       format_name: light
#       format_version: '1.5'
#       jupytext_version: 1.4.0
#   kernelspec:
#     display_name: Python 2 (Ubuntu, plain)
#     language: python
#     name: python2-ubuntu
# ---

import mmf_setup

mmf_setup.nbinit()

# +
# %pylab inline --no-import-all
from gpe import utils, bec2, tube2, minimize
from soc_experiments import u

import soc_experiments

experiment = soc_experiments.Experiment5(
    State=soc_experiments.State2Tube, Lx=440 * u.micron, Nx=2**11
)

s0 = experiment.get_state()

s0.plot()
# plt.xlim(-40, 40)


m = minimize.MinimizeState(s0, fix_N=False)
s = m.minimize()
s.plot()
# plt.xlim(-40, 40)

# +
from IPython.display import clear_output, display
from pytimeode.evolvers import EvolverABM
from mmfutils.contexts import NoInterrupt

s1 = s.copy()
s1.cooling_phase = 1 + 0.000001j
s1.ws[0] = 0.0
# s1[...][1] = 0.

e = EvolverABM(s1, dt=0.05 * s.t_scale)
fig = None
with NoInterrupt() as interrupted:
    while not interrupted:
        e.evolve(200)
        plt.clf()
        fig = e.y.plot(fig)
        display(fig)
        clear_output(wait=True)
# -
