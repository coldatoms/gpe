"""This module defines experiments reproducing our original negative-mass
hydrodynamics self-trapping results."""
from __future__ import division, absolute_import, with_statement, print_function

import numpy as np
import scipy.interpolate

sp = scipy

from gpe import utils
from gpe.soc import u
import gpe.soc

__all__ = ["ExperimentNegativeMass"]


class ExperimentNegativeMass(gpe.soc.Experiment):
    """Self-trapping "experiment".

    Attributes
    ----------

    """

    t_unit = u.ms
    B_gradient_mG_cm = 0  # No counterflow

    recoil_frequency_Hz = 1843
    rabi_frequency_E_R = 2.5
    detuning_E_R = 0.54  # Values in fig 2: [2.71, 1.36, 0.54]
    detuning_kHz = None

    Lx = 276 * u.micron
    Nx = 4096

    initial_imbalance = None  # Start in true ground state.

    x_TF = 24.0 * u.micron  # Thomas Fermi "radius" (where V(x_TF) = mu)

    trapping_frequencies_Hz = (26, 170, 154)

    t__wait = 20.0  # Time to wait before expansion
    t__image = 7.0  # Time for expansion
    t__jump = 0.1  # Time over which we "jump" on or off things.
    gaussian = False

    barrier_depth_mu = 0.0  # No barrier here.

    tube = True

    def init(self):
        gpe.soc.Experiment.init(self)
        self.wx_t_ = utils.get_smooth_transition(
            [self.ws[0], 0.0], durations=[0], transitions=[self.t__jump]
        )

    def ws_t_(self, t_):
        """Time-dependent trapping potential."""
        ws = np.asarray(self.ws).tolist()
        ws[0] = self.wx_t_(t_)
        return np.asarray(ws)
