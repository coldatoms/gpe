import numpy as np
import scipy.interpolate
from scipy.stats.mstats import gmean

sp = scipy

from gpe import utils
from gpe.soc import ExperimentBarrier, u

__all__ = ["ExperimentCatchAndRelease"]


class ExperimentCatchAndRelease(ExperimentBarrier):
    """Catch and release experiment.

    Corresponding experimental data:
    * SOC Bucket:Detuning/data_180129

    Here we start with an elongated cloud in the ground state of a harmonic
    trap with some value of the SOC parameters.  Instead of letting this cloud
    expand, we turn on an attractive Gaussian beam in the center to create a
    small bump which will then expand.  If the parameters are properly tuned,
    this bump will also hit the negative mass regime, but now in the background
    of the larger cloud.

    The experimental parameters are:

    * SOC parameters
    * Large trapping frequencies
    * Gaussian width and energy for attractive center

    Attributes
    ----------
    t__wait : float
       If this is `inf`, then we start in the ground state including the
       attractive central beam, otherwise, we start in the ground state without
       the central beam, and then wait for this time while the central beam is
       on before turning it off.

    """

    t_unit = u.ms
    B_gradient_mG_cm = 0  # No counterflow

    detuning_kHz = 2.0
    detuning_E_R = None
    rabi_frequency_E_R = 1.5  # Rabi frequency

    initial_imbalance = None  # Start in true ground state.

    trapping_frequencies_Hz = (3.07, 278, 278)

    harmonic_trap = True  # If False, use the real trap: see get_Vtrap()
    trapping_wavelength_nm = 1064

    x_TF = 232.5 * u.micron  # Thomas Fermi "radius" (where V(x_TF) = mu)
    barrier_width_micron = 4.8  # Width of central Gaussian beam
    barrier_k_k_r = 0.0  # Barrier lattice momentum
    barrier_depth_nK = -52.5  # Depth of barrier (25uW)  (100uW=-210nK)
    barrier_x = 0.0  # Position of barrier
    t__wait = 40  # Time to wait before imaging
    t__step = 0.1  # Time to turn on barrier
    t__image = 10.1  # Time for expansion

    # If t__barrier == 0, then start with the barrier on and cool into this
    # state.  This corresponds with the SLOW experiments that Maren ran.  If
    # t__barrier is finite, then the barrier starts off to get the ground
    # state, then is jumped on over time t__step, and left on for this length
    # of time.  Note: if you want to start with a barrier in the ground state
    # and then move the barrier, you will need to redefine the barrier_depth_t_
    # function.
    t__barrier = 0  # Wait time after turning on the central beam

    gaussian = False

    def simulate_image(
        self,
        state,
        pixel_size=1.116 * u.micron,
        sigma_2D=110.0 / 9.0 / (1.116 * u.micron) ** 2,
        Ny=100,
    ):
        """Return the density with simulated noise and pixel binning for
        comparison with the experiment.

        Arguments
        ---------
        pixel_size : float
           Linear size of pixels.
        sigma_2D : float
           Standard deviation for 2D noise.
        Ny : int, None
           Number of pixels averaged over to get 1D signal.  If `None`, this is
           estimated from the expanded cloud size.
        """
        if Ny is None:
            # Use tube2 code to estimate width of expanded cloud.
            Ly = 2 * (state.get_sigma2s().max(axis=-1) * state.get_lambdas()).max()
            Ny = Ly / pixel_size

        sigma_1D = pixel_size * np.sqrt(Ny) * sigma_2D
        ns = state.get_density_x()

        x = state.xyz[0].ravel()
        Lx = state.experiment.Lx

        Npixels = int(np.ceil(Lx / pixel_size))
        pixel_boundaries = np.arange(Npixels + 1) * pixel_size + x[0]
        ns_function = sp.interpolate.interp1d(
            x, ns, axis=-1, kind="linear", bounds_error=False, fill_value=0
        )
        ns_ = []
        self._l = locals()
        for _n in range(Npixels):
            x0, x1 = pixel_boundaries[_n : _n + 2]
            inds = np.where(np.logical_and(x0 <= x, x <= x1))[0]
            x_ = np.concatenate([[x0], x[inds], [x1]])
            ns_.append(np.trapz(ns_function(x_), x_) / pixel_size)

        ns_ = np.array(ns_).T
        ns_ += np.random.normal(loc=0.0, scale=sigma_1D, size=ns_.shape)
        xs_ = (pixel_boundaries[1:] + pixel_boundaries[:-1]) / 2.0
        return xs_, ns_

    def init(self):
        ExperimentBarrier.init(self)

        if not hasattr(self, "barrier_depth"):
            self.barrier_depth = self.barrier_depth_nK * u.nK
        if not hasattr(self, "barrier_width"):
            self.barrier_width = self.barrier_width_micron * u.micron

        self.barrier_k = self.barrier_k_k_r * self.k_r

        if not hasattr(self, "barrier_depth_t_"):
            if self.t__barrier == 0:
                self.barrier_depth_t_ = utils.get_smooth_transition(
                    [self.barrier_depth, 0],
                    durations=[self.t__barrier],
                    transitions=[min(0.1, self.t__barrier)],
                )
            else:
                self.barrier_depth_t_ = utils.get_smooth_transition(
                    [0, self.barrier_depth, 0.0],
                    durations=[0, self.t__barrier - 2 * self.t__step],
                    transitions=[self.t__step, self.t__step],
                )
        if not hasattr(self, "t__final") or np.isinf(self.t__final):
            # Only set if it has not already been set!
            self.t__final = self.t__barrier + self.t__wait
        ExperimentBarrier.init(self)

    def get_Vtrap_expt(self, state, xyz, ws=None):
        """Return the external trapping potential used in the experiment.

        This version uses the usual harmonic trap if
        `self.harmonic_trap` but allows for a more physical trapping
        potential otherwise,
        """
        if self.harmonic_trap:
            return super().get_Vtrap_expt(state=state, xyz=xyz, ws=ws)

        # Physical trapping potential
        if xyz is None:
            xyz = self.xyz
        if ws is None:
            t_ = state.t / self.t_unit
            ws = self.get("ws", t_=t_)

        x = xyz[0]
        r2 = sum(_x**2 for _x in xyz[1:])

        wx = ws[0]
        w_perp = gmean(ws[1:])
        trapping_wavelength = self.trapping_wavelength_nm * u.nm
        w0 = w_perp / wx * trapping_wavelength / np.sqrt(2) / np.pi
        x_R = np.pi * w0**2 / trapping_wavelength
        w2 = w0**2 * (1 + (x / x_R) ** 2)
        V_m = w0**4 * w_perp**2 / 4 * (1.0 / w0**2 - np.exp(-2 * r2 / w2) / w2)
        return state.ms[state.bcast] * V_m


class ExperimentCatchAndReleaseSmall(ExperimentCatchAndRelease):
    """Small versions of the Adiabticity experiment allowing control of the
    dimensionless parameters.  All parameters are either dimensionless, or
    specified in micron or ms.
    """

    t_unit = u.ms

    cells_x = 100
    dx = 0.1 * u.micron
    T__x = None

    tube = False

    def init(self):
        if self.T__x is not None:
            self.ws_expt = 2 * np.pi * np.asarray(self.trapping_frequencies_Hz) * u.Hz
            wx = 2 * np.pi / (self.T__x * self.t_unit)
            self.ws = (wx,) + tuple(self.ws_expt[1:])

        ExperimentCatchAndRelease.init(self)

    @staticmethod
    def plot1(
        state,
        fig=None,
        subplot_spec=None,
        parity=False,
        a=False,
        b=False,
        show_mixtures=False,
        show_momenta=False,
        history=None,
    ):  # pragma: nocover
        from matplotlib import pyplot as plt
        from gpe.plot_utils import MPLGrid

        if fig is None:
            fig = plt.figure(figsize=(15, 5))

        fig.clf()

        grid = MPLGrid(fig=fig, subplot_spec=subplot_spec, right=False)

        state.plot_densities(
            grid=grid, split=False, show_V=False, parity=parity, a=a, b=b
        )
        Va, Vb = state.experiment.get_Vt(state=state)
        plt.legend()
        x = state.xyz[0]
        if parity:
            x = abs(x)

        plt.twinx()
        plt.plot(x, Va, ":y")
        plt.plot(x, Vb, "--y")

        if show_mixtures:
            state.plot_mixtures(ax=grid.next())

        if show_momenta:
            state.plot_momenta(state.get_momenta_data(), ax=grid.next())

        if history is not None:
            data = state.get_plot_data(states=history)
            state.plot_history(data=data, ax=grid.next())

        plt.tight_layout()

        E = state.get_energy()
        Na, Nb = state.get_Ns()
        N = Na + Nb
        plt.suptitle(
            "t={}t0, Ns={:.4f}+{:.4f}={:.4f}, E={:.4f}".format(
                state.t / state.t0, Na, Nb, N, E
            )
        )
        return fig, grid


class ExperimentCatchAndRelease1a(ExperimentCatchAndRelease):
    """Old class with incorrect detuning.  We have data for this."""

    detuning_kHz = 20.0
    rabi_frequency_E_R = 1.9


class ExperimentCatchAndRelease1b(ExperimentCatchAndRelease):
    """Old class with incorrect trapping frequency.  We have data for this."""

    trapping_frequencies_Hz = (2.07, 278, 278)
