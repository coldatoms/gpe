# ---
# jupyter:
#   jupytext:
#     formats: ipynb,py
#     text_representation:
#       extension: .py
#       format_name: light
#       format_version: '1.5'
#       jupytext_version: 1.4.0
#   kernelspec:
#     display_name: Python [conda env:_soc3]
#     language: python
#     name: conda-env-_soc3-py
# ---

# + init_cell=true
import mmf_setup

mmf_setup.nbinit()
# -

# # Spin-Orbit Coupled BECs

# Here we describe simulations that model Peter Engels's experiments.  They study a tightly confined two-component BEC with the $\ket{1,-1}$ and $\ket{1,0}$ states of $^{87}$Rb in the presence of counter-flow and spin-orbit coupling (SOC).  The Hamiltonian for the system is
#
# The Hamiltonian is:
#
# $$
#   \I\hbar\partial_t
#   \begin{pmatrix}
#     \psi_a\\
#     \psi_b
#   \end{pmatrix}
#   =
#   \begin{pmatrix}
#     \frac{-\hbar^2 \nabla^2}{2m} + V_a - \mu - \frac{\delta}{2} + g_{aa}n_a + g_{ab}n_b & \frac{\Omega}{2}e^{2\I k_r x} \\
#     \frac{\Omega}{2}e^{-2\I k_r x} & \frac{-\hbar^2 \nabla^2}{2m} + V_b - \mu + \frac{\delta}{2} + g_{ab}n_a + g_{bb}n_b
#   \end{pmatrix}
#   \begin{pmatrix}
#     \psi_a\\
#     \psi_b
#   \end{pmatrix}.
# $$

# ## Rotating Phases

# We start by performing a phase rotation:
#
# $$
#   \begin{pmatrix}
#     \tilde{\psi}_a\\
#     \tilde{\psi}_b
#   \end{pmatrix}
#   =
#   e^{-\I\vect{k}_r x \mat{\sigma}_z}
#   \begin{pmatrix}
#    \psi_a\\
#    \psi_b
#   \end{pmatrix}
#   =
#   \begin{pmatrix}
#     e^{-\I \vect{k}_r x}\psi_a\\
#     e^{\I \vect{k}_r x}\psi_b
#   \end{pmatrix}
# $$
#
# so that
#
# $$
#   \I\hbar\partial_t
#   \begin{pmatrix}
#     \tilde{\psi}_a\\
#     \tilde{\psi}_b
#   \end{pmatrix}
#   =
#   \begin{pmatrix}
#     \frac{\hbar^2 (-\I\vect{\nabla} + \vect{k}_r)^2}{2m} + V_a - \mu - \frac{\delta}{2} + g_{aa}n_a + g_{ab}n_b & \frac{\Omega}{2} \\
#     \frac{\Omega}{2} & \frac{\hbar^2 (-\I\vect{\nabla} - \vect{k}_r)^2}{2m} + V_b - \mu + \frac{\delta}{2} + g_{ab}n_a + g_{bb}n_b
#   \end{pmatrix}
#   \begin{pmatrix}
#     \tilde{\psi}_a\\
#     \tilde{\psi}_b
#   \end{pmatrix}.
# $$
#
# so that

# $$
#   \I\hbar\partial_t
#   \tilde{\Psi}
#   =
#   \left[
#     \frac{\hbar^2\left(
#     -\I\nabla \mat{1} + \vect{k}_r\mat{\sigma}_z
#   \right)^2}{2m}
#   -
#   \mu\mat{1}
#   -
#   \delta\frac{\mat{\sigma}_z}{2}
#   +
#   \Omega\frac{\mat{\sigma}_x}{2}
#   \right]\tilde{\Psi}
#   =
#   \left[
#     \left(
#       \frac{-\hbar^2\nabla^2 + k_r^2}{2m}
#       -
#       \mu
#     \right)\mat{1}
#     -
#     \left(
#       \delta
#       -
#       \frac{2\hbar^2(-\I\vect{\nabla})\cdot\mat{k}_r}{m}
#     \right)
#     \frac{\mat{\sigma}_z}{2}
#     +
#     \Omega\frac{\mat{\sigma}_x}{2}
#   \right]\tilde{\Psi}\\
#   \mu \equiv \mu - \frac{V_a + V_b + (g_{aa} + g_{ab})n_a + (g_{ab} + g_{bb})n_b}{2}, \qquad
#   \frac{\delta}{2} \equiv \frac{\delta}{2} - \frac{V_a - V_b + (g_{aa} - g_{ab})n_a + (g_{ab} - g_{bb})n_b}{2}.
# $$
#
# In case of  of equal couplings, the latter expression is just the detuning.
#

# Note now that without loss of generality, for homogeneous states we can take
#
# $$
#   \tilde{\Psi} = e^{\I k x}
#   \begin{pmatrix}
#     \sqrt{n_a}\\
#     \sqrt{n_b}
#   \end{pmatrix}
# $$
#
# since any relative velocity will result in a term proportional to the identity $\mat{1}$.  We can then neglect the evolution of this overall phase by subtracting an appropriate constant, obtaining the following rotation on the Bloch sphere:
#
# $$
#   \tilde{\Psi} = \exp\left(-\I t \vec{\omega}\cdot \frac{\vec{\mat{\sigma}}}{2}\right)\tilde{\Psi}, \qquad
#   \vec{\omega} = \begin{pmatrix}
#     \frac{\Omega}{\hbar}\\
#     0\\
#     \frac{2\hbar k k_r}{m} - \frac{\delta}{\hbar}
#   \end{pmatrix}.
# $$

# To avoid any complications (these fictitious phases must be carefully applied to initial states and currents for example) we do not use this transform in the actual code, however, it is very useful for analysis.  In particular, for homogeneous states, we can diagonalize this to obtain two branches with dispersions:
#
# $$
#   \epsilon_{+}(\vect{k}) = \frac{\hbar^2(k^2 + k_r^2)}{2m} - \mu_+, \qquad
#   \epsilon_{-}(\vect{k}) = \frac{\hbar^2\vect{k}\cdot\vect{k}_r}{m} - \mu_{-}, \qquad
#   D(\vect{k}) = \sqrt{\epsilon_{-}^2(\vect{k}) +\frac{\Omega^2}{4}}, \qquad
#   \mu_{\pm} = \frac{\mu_a \pm \mu_b}{2},
#   \\
#   \mu_{+} = \mu - \frac{V_a + V_b + g_{aa}n_a + g_{bb}n_b + g_{ab}(n_a+n_b)}{2}, \qquad
#   \mu_{-} = \frac{\delta}{2} - \frac{V_a - V_b + g_{aa}n_a - g_{bb}n_b + g_{ab}(n_a-n_b)}{2},
#   \\
#   E_{\pm}(\vect{k}) = \epsilon_{+}(\vect{k}) \pm D(\vect{k}), \qquad
#   \frac{u_\pm}{v_{\pm}} = \frac{-C}{\epsilon_{-}(\vect{k}) \mp D(\vect{k})}
#   = \frac{\epsilon_{-}(\vect{k}) \pm D(\vect{k})}{C}, \qquad
#   \frac{\abs{u_\pm}}{\abs{v_{\pm}}} = \sqrt{\frac{1\pm K}{1\mp K}}, \qquad
#   K = \frac{\epsilon_{-}(\vect{k})}{D(\vect{k})}
#   = \pm\frac{\frac{\abs{u_\pm}^2}{\abs{v_{\pm}}^2}-1}{\frac{\abs{u_\pm}^2}{\abs{v_{\pm}}^2}+1}
# $$

# $$
#   u_{\pm} = \frac{-1}
#   {\sqrt{2}\sqrt{1 + \frac{\epsilon_{-}(\vect{x})}{\Omega/2}
#               \frac{\epsilon_{-}(\vect{x}) \mp D(\vect{x})}{\Omega/2}}},\qquad
#   v_{\pm} = \frac{\frac{\epsilon_{-}(\vect{x}) \mp D(\vect{x})}{\Omega/2}}
#   {\sqrt{2}\sqrt{1 + \frac{\epsilon_{-}(\vect{x})}{\Omega/2}
#                      \frac{\epsilon_{-}(\vect{x}) \mp D(\vect{x})}{\Omega/2}}},
# $$
# $$
#   u_{\pm} = \frac{-C}
#   {\sqrt{2C^2 + 2\epsilon_{-}(\vect{x})[\epsilon_{-}(\vect{x}) \mp D(\vect{x})]}},\qquad
#   v = \frac{\epsilon_{-}(\vect{x}) \mp D(\vect{x})}
#   {\sqrt{2C^2 + 2\epsilon_{-}(\vect{x})[\epsilon_{-}(\vect{x}) \mp D(\vect{x})]}},
# $$
# $$
#   u_{\pm} = \frac{-\Omega/2}
#   {\sqrt{2D(\vect{k})}\sqrt{D(\vect{k}) \mp \epsilon_{-}(\vect{k})}},\qquad
#   v_{\pm} = \mp\frac{\sqrt{D(\vect{k})\mp\epsilon_{-}(\vect{x})}}
#   {\sqrt{2D(\vect{k})}}.
# $$


# The population of the upper/lower branches can be computed as:
#
# $$
#   n_{\pm} = \frac{1\pm K}{2}n_a + \frac{1\mp K}{2}n_b.
# $$

# +
# Quick check of formula:
np.random.seed(3)
A, B, C = np.random.random(3)
H = np.array([[A, C], [C, B]])
ABs = [(A + B) / 2, (A - B) / 2]
D = np.sqrt(ABs[1] ** 2 + C**2)
Es = [ABs[0] - D, ABs[0] + D]
K = ABs[1] / D
E, V = np.linalg.eigh(H)
assert np.allclose(Es, E)
um, vm = V[:, 0]
up, vp = V[:, 1]
assert np.allclose([-C / (ABs[1] - D) * vp, -C / (ABs[1] + D) * vm], [up, um])
assert np.allclose(
    np.abs([up / vp, um / vm]), np.sqrt([(1 + K) / (1 - K), (1 - K) / (1 + K)])
)

assert np.allclose(
    [
        (abs(up / vp) ** 2 - 1) / (1 + abs(up / vp) ** 2),
        -(abs(um / vm) ** 2 - 1) / (1 + abs(um / vm) ** 2),
    ],
    K,
)
psi_pm = np.random.random(2)
n_p, n_m = abs(psi_pm) ** 2
psi_ab = psi_pm[0] * np.array([up, vp]) + psi_pm[1] * np.array([um, vm])
n_a, n_b = abs(psi_ab) ** 2

assert np.allclose(n_a + n_b, n_m + n_p)
(
    ((1 + K) * n_a + (1 - K) * n_b - 2 * np.sqrt((1 - K**2) * n_a * n_b)) / 2,
    n_p,
    ((1 - K) * n_a + (1 + K) * n_b + 2 * np.sqrt((1 - K**2) * n_a * n_b)) / 2,
    n_m,
)
# -

# Note that although this phase rotation does not affect the densities, it will affect currents, and needs to be carefully applied to, for example, initial states.  In the code, we add a property `rotating_phases` which can be set or unset, applying the appropriate transformation to the underlying state and including or removing the $\vect{k}_r$ terms from the kinetic energy.
