# ---
# jupyter:
#   jupytext:
#     formats: ipynb,py
#     text_representation:
#       extension: .py
#       format_name: light
#       format_version: '1.5'
#       jupytext_version: 1.4.0
#   kernelspec:
#     display_name: Python [conda env:_gpe3]
#     language: python
#     name: conda-env-_gpe3-py
# ---

# + init_cell=true
import mmf_setup;mmf_setup.nbinit()
import sys, os
import SOC
_SOC_PATH = os.path.dirname(SOC.__file__)
if _SOC_PATH not in sys.path:
    sys.path.append(_SOC_PATH)
from gpe.imports import *
from mmfutils.plot import imcontourf
# -

# # Intro 

# # Prep
#
# 3D runs are very memory intensive to setup and run, the minimization process in particular takes days. To get around this we will start from an already minimized 2D-axial state. To adjust for incongiutities we will cool for a litte before starting the experiemnts. 
#





# +
# %pylab inline --no-import-all
from gpe.soc import ExperimentBarrier, u
import SOC.soc_car_fast#;reload(SOC.soc_car_fast)
from SOC.soc_car_fast import ExperimentCARFast, ExperimentCARFast3D, ExperimentCARFast3D_AxialImprint


#e = ExperimentCARFast3D(dx=1)
e = ExperimentCARFast3D_AxialImprint(dx=1)
# -

s = e.get_initial_state()



??e.get_initial_state

# +
# %pylab inline --no-import-all
from gpe.soc import ExperimentBarrier, u
import SOC.soc_car_fast#;reload(SOC.soc_car_fast)
from SOC.soc_car_fast import ExperimentCARFast, ExperimentCARFast3D

#e = ExperimentCARFast3D(Lxyz=(480*u.micron, 7.68*u.micron, 7.68*u.micron),
#                        Nxyz=(8000, 128, 128))

e = ExperimentCARFast3D(dx=1)
s = e.get_state();
# -

s.plot()

# +
# %pylab inline --no-import-all
from gpe.soc import ExperimentBarrier, u
import SOC.soc_car_fast#;reload(SOC.soc_car_fast)
from SOC.soc_car_fast import ExperimentCARFast

#Sim data
import runs_car_fast_final
r1 = runs_car_fast_final.Run_IPG_xTF150_RNR() # Axial data for comparison

barrier_depth_nK = -90 # -30, -60, -90

sim1 = [_sims for _sims in r1.simulations
       if _sims.experiment.barrier_depth_nK == barrier_depth_nK
       ][0]

s1 = sim1.get_state(t_=0, image=False);

# -

x, y, z = s.xyz
r = np.sqrt(y**2 + z**2)
xr = np.asarray([s1.xyz[0], r.ravel()])
s[...] = s1.basis.Psi(s1[...], xr).reshape(s[...].shape)

(2*8000*108**2)*16/1024**3*8

s1[...].shape

cool_steps = 200
s.cooling_phase = 1j
cool_dt_t_scale = 0.1
dt = cool_dt_t_scale * s.t_scale
s.t = -dt * cool_steps
evolver = EvolverABM(s, dt=dt)
evolver.evolve(cool_steps)
s = evolver.get_y()





# ## How Much cooling? 

# +
# %pylab inline --no-import-all
from gpe.soc import ExperimentBarrier, u
import SOC.soc_car_fast#;reload(SOC.soc_car_fast)
from SOC.soc_car_fast import ExperimentCARFastSmall

#e = ExperimentCARFast3D(Lxyz=(480*u.micron, 7.68*u.micron, 7.68*u.micron),
#                        Nxyz=(8000, 128, 128))

e0 = ExperimentCARFastSmall(tube=True, dx=0.06, cooling_phase=1)
e1 = ExperimentCARFastSmall(tube=True, dx=0.06, cooling_phase=1J)

s = [e0.get_state(), e1.get_state()]

# +
t_end = 1*u.ms
def callback():
    
s[0].evolve_to(self, t_end, dt_t_scale=0.2,
               Evolver=evolvers.EvolverABM,  
               evolve_steps=200, callback=None)
# -



s[...].shape, s1[...].shape, s1.basis.Lx/s1.basis.Nx, s.basis.Lx/s.basis.Nx

s1.basis.Lxr[1]/s1.basis.Nxr[1], s.basis.Lxyz[1]/s.basis.Nxyz[1]

s1.basis.Lxr[1]

s.basis.Lxyz, s1.basis.Lxr 

s.basis.Nx



6.02589928/100, 480/8000, 7.68/128



# Sever 3D runs were made to test different things. Check the run file runs_car_fast_final.py for specific parameters.
#
#
#
# Different run functions
# ----------------------
#
# \textbf{Run_IPG_small_3D_big} \\
# dx=[0.1*u.micron], \\
# cells_x=[300], \\ 
# barrier_depth_nK=[-30, -60, -90], \\
# y_tilt=[0, 0.01], \\
# image_ts_=[18] \\
#

# # Snap Shots 

# +
# %pylab inline --no-import-all
from gpe.soc import ExperimentBarrier, u
import SOC.soc_car_fast#;reload(SOC.soc_car_fast)
from SOC.soc_car_fast import ExperimentCARFast

#Sim data
import runs_car_fast_final
r0 = runs_car_fast_final.Run_IPG_small_3D_big() # 3D data
r1 = runs_car_fast_final.Run_IPG_xTF150_RNR() # Axial data for comparison


barrier_depth_nK = -90 # -30, -60, -90
y_tilt = 0.0 # 0, 0.01, 0.02, 0.1, 1

sim0 = [_sims for _sims in r0.simulations
       if _sims.experiment.barrier_depth_nK == barrier_depth_nK
       and _sims.experiment.y_tilt == y_tilt
       ][0]

sim1 = [_sims for _sims in r1.simulations
       if _sims.experiment.barrier_depth_nK == barrier_depth_nK
       ][0]

params0 = sim0.experiment.items()
params1 = sim1.experiment.items()
print(params0, params1)

s = sim0.get_state(t_=0, image=False);

# -

s0 = sim0.get_state(t_=20, image=False)
s1 = sim1.get_state(t_=20, image=False)

s0.basis.Lxyz/s0.basis.Nxyz



# +
from scipy.stats.mstats import gmean
_E = s1.get_dispersion()
k0 = _E.get_k0()

m = s1.ms[0]/_E(k=k0, d=2)[0]

#g = s1.gs[0]
g = gmean(s1.gs)
ix, ir = s1.basis.Nxr//2
#print(ix, iy, iz)
n3 = s1.get_density().sum(axis=0)[ix, 0]

cs = np.sqrt(g*n3/2/m)
print(r'c_s = {:.4f} mm/s'.format(cs*u.s/u.mm))

# +
from scipy.stats.mstats import gmean
_E = s1.get_dispersion()
k0 = _E.get_k0()

m = s1.ms[0]/_E(k=k0, d=2)[0]

#g = s1.gs[0]
g = gmean(s1.gs)

a = gmean(s1.gs/4/np.pi/u.hbar**2*u.m)
w = s1.ws[-1]
ix, ir = s1.basis.Nxr//2
n3 = s1.get_density().sum(axis=0)[ix, 0]

n1 = np.pi*g/s1.ms[0]/w**2*n3**2
n1 = s1.get_density_x().sum(axis=0)[ix]

gn = np.asarray([
    u.hbar*w*np.sqrt(a*n1),
    2*u.hbar*w*a*n1/np.sqrt(1+4*a*n1),
    u.hbar*w*a*n1*(3*a*n1+2)*(1+2*a*n1)**(-3/2)
])


cs = np.sqrt(gn/m)
print(r'c_s = {:.4f} mm/s'.format(cs[0]*u.s/u.mm))
print(r'c_s = {:.4f} mm/s'.format(cs[1]*u.s/u.mm))
print(r'c_s = {:.4f} mm/s'.format(cs[2]*u.s/u.mm))
# -

u.m, m, s1.ms

s1.ws

u.a_B, u.mm



s0.gs, s0.gs/4/np.pi/u.hbar**2*u.m, u.scattering_lengths







[_x.shape for _x in s0.xyz],[_x.shape for _x in s1.xyz]



# +
from mmfutils.plot import imcontourf
from gpe.plot_utils import MPLGrid
from ipywidgets import interact, interact_manual


@interact_manual(time=(0,50,0.1),dx=(0,140,20))
def plot_func(time=0, dx=0): 
    image=False 
    fig=None
    if fig is None:
        fig = plt.figure(figsize=(12,2.5))
    fig.clear()
    
    grid = MPLGrid(fig=fig, 
               direction='right',
               space=0., 
               share=True)

    sub_grid = grid.grid(direction='down',
                         space=0.,
                         share=True)

    ##########################
    ax = sub_grid.next()
    s = sim1.get_state(t_=time, image=image);
    x, r = [_x.ravel() for _x in s.xyz]
    r = np.concatenate((-r[::-1],r))
    #nx = s.get_density_x().sum(axis=0)/nx_max1
    ns = na, nb = s.get_density()/s.get_density().sum(axis=0).max()
    ns = na, nb = np.concatenate((ns[:,:,::-1], ns), axis=2)

    imcontourf(x, r, 
               ns.sum(axis=0), 
               aspect=1)
    ax.set_ylabel(r'3D')#, size='xx-large')
    ax.set_xlim(-20-dx, 20+dx)
    ax.set_ylim(-3.2,3.2)
    ax.set_xlabel(r'$x$ [micron]', size='xx-large')
    plt.tight_layout()


    
# -





plot_func(time=4, image=False, fig=None)





# # In Situ Movie 

# +
# %pylab inline --no-import-all
from gpe.soc import ExperimentBarrier, u
import SOC.soc_car_fast#;reload(SOC.soc_car_fast)
from SOC.soc_car_fast import ExperimentCARFast

#Sim data
import runs_car_fast_final
r0 = runs_car_fast_final.Run_IPG_small_3D_big() # 3D data
r1 = runs_car_fast_final.Run_IPG_xTF150_RNR() # Axial data for comparison


barrier_depth_nK = -90 # -30, -60, -90
y_tilt = 0.0 # 0, 0.01, 0.02, 0.1, 1

sim0 = [_sims for _sims in r0.simulations
       if _sims.experiment.barrier_depth_nK == barrier_depth_nK
       and _sims.experiment.y_tilt == y_tilt
       ][0]

sim1 = [_sims for _sims in r1.simulations
       if _sims.experiment.barrier_depth_nK == barrier_depth_nK
       ][0]

params0 = sim0.experiment.items()
params1 = sim1.experiment.items()
print(params0, params1)

s = sim0.get_state(t_=0, image=False);

# -



# + init_cell=true
nx_max0 = sim0.get_state(t_=10, image=False).get_density_x().max()
nx_max1 = sim1.get_state(t_=10, image=False).get_density_x().max()

# +
# Load previous data
filename = '_temp_data/Nmax_CAR_FAST_3DvAxial_{:.0f}nK_{:.0f}tilt.npz'.format(
    abs(barrier_depth_nK),
    y_tilt*100)

try:
    t = np.load(filename)['t']
    ts = np.linspace(t[-1]+0.1, 20, np.ceil((20-t[-1]-0.1)*10)+1)    
    Nmax = np.load(filename)['Nmax'].tolist()
    
except:
    ts = np.linspace(0,20,201)    
    Nmax = []
    
    
# Save new data
for _t in ts:
    time = np.floor(_t*100)/100
    s0 = sim0.get_state(t_=time, image=False);
    s1 = sim1.get_state(t_=time, image=False);
    Nmax.append((s0.get_density().sum(axis=0).max(),
                 s1.get_density().sum(axis=0).max()))
    
    np.savez(filename, 
             t=np.asarray(np.linspace(0, time, time*10+1)),
             Nmax=np.asarray(Nmax))
    del(s0)
    del(s1)
    print(time)

Nmax = np.asarray(Nmax)
N_max = [Nmax[0].max(), Nmax[1].max()]

# -





# ## Main plot function
#
# The function will save the data to be loaded later.
# Plots the 1D and 2D profiles of the 3d and axial data
#
# the 3d is slice through z=z/2

# ## single shot plot func
#
# I used this to make some alternative figures for a grant, not used to make the movies

# +
import matplotlib.image as mpimg
from mmfutils.plot import imcontourf
from gpe.plot_utils import MPLGrid

def plot_func(time=0, image=False, fig=None):
    if fig is None:
        fig = plt.figure(figsize=(12,2.5))
    fig.clear()
    
    filename = 'Frame_CAR_FAST_3DvAxial_{:.0f}nK_{:.0f}tilt_{:.0f}ms_alt.png'.format(
        abs(barrier_depth_nK),
        y_tilt*100,
        time*10)
       
    try:
        img = mpimg.imread(filename)
        plt.imshow(img)
        plt.tick_params(
            axis='both',       # changes apply  both axis
            which='both',      # both major and minor ticks are affected
            bottom=False,      # ticks along the bottom edge are off
            top=False,         # ticks along the top edge are off
            labelbottom=False) # labels along the bottom edge are off
        plt.axis('off');
        del(img)
        
    except:
        grid = MPLGrid(fig=fig, 
               direction='right',
               space=0., 
               share=True)

        sub_grid = grid.grid(direction='down',
                             space=0.,
                             share=True)
    
        ##########################
        ax = sub_grid.next()

        s = sim0.get_state(t_=time, image=image);
        x_3d, y_3d, z_3d = [_x.ravel() for _x in s.xyz]
        nx = s.get_density_x().sum(axis=0)/nx_max0
        n_3d = na_3d, nb_3d = s.get_density()/N_max[0]

        plt.plot(x_3d, nx, label='3D')
        ax.set_xlim(-40, 40)
        ##########################
        s = sim1.get_state(t_=time, image=image);
        x_r, r = [_x.ravel() for _x in s.xyz]
        r = np.concatenate((-r[::-1],r))
        nx = s.get_density_x().sum(axis=0)/nx_max1
        ns = na, nb = s.get_density()/N_max[1]
        ns_r = na_r, nb_r = np.concatenate((ns[:,:,::-1], ns), axis=2)

        plt.plot(x_r, nx, alpha=0.8, label='axial')
        ax.legend(loc='upper right', prop={'size': 20})
        ax.set_xlim(-40, 40)
        #plt.ylim(0,1.2)

        ##########################
        ax = sub_grid.next()
        imcontourf(x_3d, y_3d, 
                   n_3d.sum(axis=0)[:,:,n_3d.shape[-1]//2], 
                   aspect=1)#, **args)
        ax.set_ylabel(r'3D', size='xx-large')#, size='xx-large')
        ax.set_xlim(-40, 40)
        ax = sub_grid.next()
        imcontourf(x_r, r, ns_r.sum(axis=0), aspect=1)#, **args)
        ax.set_ylabel(r'Axial', size='xx-large')#, size='xx-large')
        ax.set_xlim(-40, 40)
        ax.set_ylim(-2.5,2.5)
        ax.set_xlabel(r'$x$ [micron]', size='xx-large')
        ax.tick_params(axis="x", labelsize=20)
        #plt.tight_layout()

        plt.savefig(filename)
        del(s)
    


# -

plot_func(time=19, fig=plt.figure(figsize=(20,4.6)))







# +
import matplotlib.image as mpimg
from mmfutils.plot import imcontourf
from gpe.plot_utils import MPLGrid

def plot_func(time=0, image=False, fig=None):
    if fig is None:
        fig = plt.figure(figsize=(12,2.5))
    fig.clear()
    
    filename = 'Frame_CAR_FAST_3DvAxial_{:.0f}nK_{:.0f}tilt_{:.0f}ms.png'.format(
        abs(barrier_depth_nK),
        y_tilt*100,
        time*10)
       
    try:
        img = mpimg.imread(filename)
        plt.imshow(img)
        plt.tick_params(
            axis='both',       # changes apply  both axis
            which='both',      # both major and minor ticks are affected
            bottom=False,      # ticks along the bottom edge are off
            top=False,         # ticks along the top edge are off
            labelbottom=False) # labels along the bottom edge are off
        plt.axis('off');
        del(img)
        
    except:
        grid = MPLGrid(fig=fig, 
               direction='right',
               space=0., 
               share=True)

        sub_grid = grid.grid(direction='down',
                             space=0.,
                             share=True)
    
        ##########################
        ax = sub_grid.next()

        title = r't_wait$={:.1f}$ms, $\qquad barrier={:.0f}$nK, $\qquad   tilt={:.2f}$'.format(
            time-10, barrier_depth_nK, y_tilt)
        ax.set_title(title)
        s = sim0.get_state(t_=time, image=image);
        x_3d, y_3d, z_3d = [_x.ravel() for _x in s.xyz]
        nx = s.get_density_x().sum(axis=0)/nx_max0
        n_3d = na_3d, nb_3d = s.get_density()/N_max[0]

        plt.plot(x_3d, nx, label='3D')
        _dx = 5
        ax.set_xlim(x_3d[0] - _dx, x_3d[-1] + _dx)
        ##########################
        s = sim1.get_state(t_=time, image=image);
        x_r, r = [_x.ravel() for _x in s.xyz]
        r = np.concatenate((-r[::-1],r))
        nx = s.get_density_x().sum(axis=0)/nx_max1
        ns = na, nb = s.get_density()/N_max[1]
        ns_r = na_r, nb_r = np.concatenate((ns[:,:,::-1], ns), axis=2)

        plt.plot(x_r, nx, alpha=0.8, label='axial')
        ax.legend(loc='upper right')
        ax.set_xlim(x_3d[0] - _dx, x_3d[-1] + _dx)
        #plt.ylim(0,1.2)

        ##########################
        ax = sub_grid.next()
        imcontourf(x_3d, y_3d, 
                   n_3d.sum(axis=0)[:,:,n_3d.shape[-1]//2], 
                   aspect=1)#, **args)
        ax.set_ylabel(r'3D')#, size='xx-large')
        ax.set_xlim(x_3d[0] - _dx, x_3d[-1] + _dx)
        ax = sub_grid.next()
        imcontourf(x_r, r, ns_r.sum(axis=0), aspect=1)#, **args)
        ax.set_ylabel(r'Axial')#, size='xx-large')
        ax.set_xlim(x_3d[0] - _dx, x_3d[-1] + _dx)
        ax.set_ylim(-2.5,2.5)
        ax.set_xlabel(r'$x$ [micron]', size='xx-large')
        plt.tight_layout()

        plt.savefig(filename)
        del(s)
    


# -

plot_func(time=10, fig=plt.figure(figsize=(30,10)))











ts = np.linspace(0,20,201)    
for _t in ts:
    time = np.floor(10*_t)/10
    print(time)
    plot_func(time=time, fig=plt.figure(figsize=(12,2.5)))





history = np.floor(10*np.linspace(0,20,200))/10
history

# +
from gpe import plot_for_khalid

fig = plt.figure(figsize=(30,10))
filename = 'CAR_FAST_3DvAxial_{:.0f}nK_{:.0f}tilt.mp4'.format(
        abs(barrier_depth_nK),
        y_tilt*100)
    
plot_for_khalid.make_movie(filename,
                           history.tolist(), 
                           lambda ts:plot_func(time=ts, image=False, fig=fig), fps=12, fig=fig)

# -





# ## Alt plot function
# Just 1D profiles for 3D and axial

# +

def plot_func(time=0, image=False, fig=None):
    if fig is None:
        fig = plt.figure(figsize=(8,4))
    fig.clear()
    
    ##########################
    title = r't_wait$={:.1f}$ms, $\qquad barrier={:.0f}$nK, $\qquad   tilt={:.2f}$'.format(
        time-10, barrier_depth_nK, y_tilt)
    plt.title(title)
    s = sim0.get_state(t_=time, image=image);
    x,y,z = s.xyz
    nx = s.get_density_x().sum(axis=0)
    plt.plot(x.ravel(), nx/nx_max0, label='3D')
    _dx = 5
    plt.xlim(x[0] - _dx, x[-1] + _dx)
    ##########################
    s = sim1.get_state(t_=time, image=image);
    x,r = s.xyz
    nx = s.get_density_x().sum(axis=0)
    plt.plot(x.ravel(), nx/nx_max1, alpha=0.8, label='axial')
    plt.xlabel(r'$x$ [micron]', size='xx-large')
    plt.legend(loc='upper right')
    plt.ylim(0,1.2)



# + [markdown] slideshow={"slide_type": "slide"}
# # Imaging 
# -

# ## Axial expansion
#

# +
# %pylab inline --no-import-all
from gpe.soc import ExperimentBarrier, u
import SOC.soc_car_fast#;reload(SOC.soc_car_fast)
from SOC.soc_car_fast import ExperimentCARFast

#Sim data
import runs_car_fast_final
#r0 = runs_car_fast_final.Run_IPG_small_3D_big() # 3D data
r1 = runs_car_fast_final.Run_IPG_xTF150_RNR() # Axial data for comparison


barrier_depth_nK = -90 # -30, -60, -90
y_tilt = 0.00 # 0, 0.01, 0.02, 0.1, 1

"""
sim0 = [_sims for _sims in r0.simulations
       if _sims.experiment.barrier_depth_nK == barrier_depth_nK
       and _sims.experiment.y_tilt == y_tilt
       ][0]
"""

sim1 = [_sims for _sims in r1.simulations
       if _sims.experiment.barrier_depth_nK == barrier_depth_nK
       ][0]

#params0 = sim0.experiment.items()
params1 = sim1.experiment.items()
#print(params0, params1)

#s = sim0.get_state(t_=0, image=False);

# -



# +
# Load previous data
filename = 'Nmax_CAR_FAST_Axial_Expansions_{:.0f}nK.npz'.format(
    abs(barrier_depth_nK))


t_waits = np.asarray([2,6,8,10,14,20,26])
    
try:
    t = np.load(filename)['t']
    _i = np.where(t==t_waits)[0][0]
    t_waits = t_waits[_i+1:]
    Nmax = np.load(filename)['Nmax'].tolist()

except:
    t_waits = [2,6,8,10,14,20,26]
    Nmax = []


t_exps = np.linspace(0, 20.1, 202)
for _t_wait in t_waits:
    _tw = np.floor(_t_wait*10)/10
    _Nmax = []
    for _t_exp in t_exps[:102]:
        _te = np.floor(_t_exp*10)/10
        #print('t_wait, t_exp = {}, {} ms'.format(_tw,_te))
        s0 = sim1.get_state(t_=_tw+10, image=_te);
        _Nmax.append(s0.get_density_x().sum(axis=0).max())
    _Nmax = np.asarray(_Nmax)
    Nmax.append(_Nmax.max())
    np.savez(filename,
             t=_t_wait,
             Nmax=np.asarray(Nmax))
    del(_Nmax)

Nmax = np.asarray(Nmax)
print(Nmax)
# -





# +
import matplotlib.image as mpimg

from gpe.plot_utils import MPLGrid

def plot_func(t_exp=0, fig=None):
    if fig is None:
        fig = plt.figure(figsize=(12,8))
    fig.clear()
    
    filename = 'Frame_CAR_FAST_Axial_Expansions_{:.0f}nK_{:.0f}ms.png'.format(
        abs(barrier_depth_nK), t_exp*10)

    try:
        img = mpimg.imread(filename)
        plt.imshow(img)
        plt.tick_params(
            axis='both',       # changes apply  both axis
            which='both',      # both major and minor ticks are affected
            bottom=False,      # ticks along the bottom edge are off
            top=False,         # ticks along the top edge are off
            labelbottom=False) # labels along the bottom edge are off
        plt.axis('off');
        del(img)
        
    except:
        grid = MPLGrid(fig=fig, 
               direction='right',
               space=0., 
               share=True)

        sub_grid = grid.grid(direction='down',
                             space=0.,
                             share=True)

        title = r't_exp$={:.1f}$ms, $\qquad $barrier={:.0f}$nK$'.format(
                t_exp, barrier_depth_nK)

        t_wait = [2,6,8,10,14,20,26]
        # Pasted from above for memory reasons
        N_max = [5909.45127529, 4490.17609724, 4125.04157581, 
                 3637.56434931, 3501.40199946, 3859.23241679,
                 3143.39494824]
        for _t_wait, _N_max in zip(t_wait, N_max):
            #_tw = np.floor(_t_wait*10)/10
            ax = sub_grid.next()
            if _t_wait==t_wait[0]:
                ax.set_title(title)
            s = sim1.get_state(t_=10+_t_wait, image=t_exp);
            x, r = [_x.ravel() for _x in s.xyz]
            nx = s.get_density_x()/_N_max
            ax.plot(x, nx.sum(axis=0))
            dx=70
            ax.set_xlim(-100-dx,100+dx)
            ax.set_ylabel(r't_wait={:.0f}ms'.format(_t_wait))#, size='xx-large')

            del(s, x, r, nx)
            
        ax.set_xlabel(r'$x$ [micron]', size='xx-large')
        plt.tight_layout()
        plt.savefig(filename)


# -

plot_func(t_exp=0, fig=plt.figure(figsize=(22,15)))




history = np.floor(10*np.linspace(0,20,200))/10
history[:102]
for _t in history[:102]:
    print(_t*10)
    plot_func(t_exp=_t, fig=plt.figure(figsize=(12,10)))



history = np.floor(10*np.linspace(0,20,200))/10
history[:102]





# +
from gpe import plot_for_khalid

fig = plt.figure(figsize=(22,15))
filename = 'CAR_FAST_Axial_Expansions_{:.0f}nK.mp4'.format(
        abs(barrier_depth_nK))
    
plot_for_khalid.make_movie(filename,
                           history[:102].tolist(), 
                           lambda ts:plot_func(t_exp=ts, fig=fig), fps=12, fig=fig)

# -



# ### t_wait = 20ms 

# +
# %pylab inline --no-import-all
from gpe.soc import ExperimentBarrier, u
import SOC.soc_car_fast#;reload(SOC.soc_car_fast)
from SOC.soc_car_fast import ExperimentCARFast

#Sim data
import runs_car_fast_final
#r0 = runs_car_fast_final.Run_IPG_small_3D_big() # 3D data
r1 = runs_car_fast_final.Run_IPG_xTF150_RNR() # Axial data for comparison


barrier_depth_nK = -90 # -30, -60, -90
y_tilt = 0.00 # 0, 0.01, 0.02, 0.1, 1

"""
sim0 = [_sims for _sims in r0.simulations
       if _sims.experiment.barrier_depth_nK == barrier_depth_nK
       and _sims.experiment.y_tilt == y_tilt
       ][0]
"""

sim1 = [_sims for _sims in r1.simulations
       if _sims.experiment.barrier_depth_nK == barrier_depth_nK
       ][0]

#params0 = sim0.experiment.items()
params1 = sim1.experiment.items()
#print(params0, params1)

#s = sim0.get_state(t_=0, image=False);

# -



# +
t_exps = np.linspace(0, 20.1, 202)

Nmax = []
Nmax_1D = []
for _t_exp in t_exps[:102]:
    _te = np.floor(_t_exp*10)/10
    #print('t_wait, t_exp = {}, {} ms'.format(_tw,_te))
    s0 = sim1.get_state(t_=26+10, image=_te);
    Nmax.append(s0.get_density().sum(axis=0).max())
    Nmax_1D.append(s0.get_density_x().sum(axis=0).max())

Nmax = np.asarray(Nmax)
print(Nmax.max())
# -

Nmax_1D = np.asarray(Nmax_1D)
print(Nmax_1D.max())

# +
import matplotlib.image as mpimg
from gpe.plot_utils import MPLGrid
from matplotlib.cm import viridis


def plot_func(t_exp=0, fig=None):
    if fig is None:
        fig = plt.figure(figsize=(12,8))
    fig.clear()
    
    twait = 26
    filename = 'Frame_CAR_FAST_Axial_Expansions_twait{:.0f}ms_{:.0f}nK_{:.0f}ms.png'.format(
        twait, abs(barrier_depth_nK), t_exp*10)

    """
    try:
        img = mpimg.imread(filename)
        plt.imshow(img)
        plt.tick_params(
            axis='both',       # changes apply  both axis
            which='both',      # both major and minor ticks are affected
            bottom=False,      # ticks along the bottom edge are off
            top=False,         # ticks along the top edge are off
            labelbottom=False) # labels along the bottom edge are off
        plt.axis('off');
        del(img)
        
    except:
    """
    grid = MPLGrid(fig=fig, 
           direction='right',
           space=0., 
           share=True)

    sub_grid = grid.grid(direction='down',
                         space=0.,
                         share=True)

    ax = sub_grid.next()
    title = r't_exp$={:.1f}$ms, $\qquad $t_wait$={:.0f}$ms, $\qquad $barrier={:.0f}$nK$'.format(
            t_exp, twait, barrier_depth_nK)
    ax.set_title(title)

    ax.set_facecolor(viridis(0))
    s = sim1.get_state(t_=10+twait, image=t_exp);
    x, r = [_x.ravel() for _x in s.xyz]
    r = np.concatenate((-r[::-1],r))
    ns = na, nb = s.get_density()/284.3612593493806
    ns = na, nb = np.concatenate((ns[:,:,::-1], ns), axis=2)
    args = dict(vmin=0,vmax=1)
    
    imcontourf(x, r, 
               ns.sum(axis=0),
               aspect=1,
               **args)
    
    axin1 = ax.inset_axes([0., 0.9, 1, 0.1])
    axin1.plot(x, 
               s.get_density_x().sum(axis=0)/3143.3949482419334,
               c='w')
    axin1.set_frame_on(False)
    axin1.set_xticks([])
    axin1.set_yticks([])
    
    dx=10
    axin1.set_xlim(-150-dx, 150+dx)
    ax.set_xlim(-150-dx, 150+dx)
    ax.set_ylim(-100, 100)
    ax.set_ylabel(r'$r$ [micron]', size='xx-large')
    ax.set_xlabel(r'$x$ [micron]', size='xx-large')
    
    
    plt.tight_layout()
    plt.savefig(filename)
    del(s, x, r, ns, na, nb)



# -

plot_func(t_exp=0, fig=plt.figure(figsize=(12,12)))

plot_func(t_exp=10.1, fig=plt.figure(figsize=(12,12)))



history = np.floor(10*np.linspace(0,20,200))/10
history[:102]
for _t in history[:102]:
    print(_t*10)
    plot_func(t_exp=_t, fig=plt.figure(figsize=(12,12)))

# +

history = np.floor(10*np.linspace(0,20,200))/10
history[:102]

# +
from gpe import plot_for_khalid

twait = 26
fig = plt.figure(figsize=(22,15))
filename = 'CAR_FAST_Axial_Expansion_twait{:.0f}_{:.0f}nK.mp4'.format(
        twait, abs(barrier_depth_nK))
    
plot_for_khalid.make_movie(filename,
                           history[:102].tolist(), 
                           lambda ts:plot_func(t_exp=ts, fig=fig), fps=12, fig=fig)

# -







# ## .png 

# +
# %pylab inline --no-import-all
from gpe.soc import ExperimentBarrier, u
import SOC.soc_car_fast#;reload(SOC.soc_car_fast)
from SOC.soc_car_fast import ExperimentCARFast

#Sim data
import runs_car_fast_final
# 3D data
r0 = runs_car_fast_final.Run_IPG_small_3D()
#r0 = runs_car_fast_final.Run_IPG_small_3D_alt()
#r0 = runs_car_fast_final.Run_IPG_small_3D_big()
# Axial data for comparison
r1 = runs_car_fast_final.Run_IPG_xTF150_RNR()



y_tilt = 0 # 0, 0.02, 0.1, 1

sim0 = [_sims for _sims in r0.simulations
       if _sims.experiment.y_tilt == y_tilt]

sim1 = [_sims for _sims in r1.simulations]


# -





# +
time = 18
image = True

fig = plt.figure(figsize=(9,18))
##########################
plt.subplot(311)
title = r't_wait$={:.1f}$ms $\qquad t$_exp$={:.1f}$ms $\qquad barrier$={:.0f}nK,'.format(
    time-10,10.1, 30) 
plt.title(title)

s = sim0[0].get_state(t_=time, image=image);
x,y,z = s.xyz
nx = s.get_density_x().sum(axis=0)
plt.plot(x.ravel(), nx/nx.max(), label='3D')
_dx = 15
plt.xlim(x[0] - _dx, x[-1] + _dx)

s = sim1[0].get_state(t_=time, image=image);
x,r = s.xyz
nx = s.get_density_x().sum(axis=0)
plt.plot(x.ravel(), nx/nx.max(), alpha=0.7, label='axial')
plt.xlabel('x (micron)', size='xx-large')
plt.legend(loc='lower right')

##########################
plt.subplot(312)
title = r't_wait$={:.1f}$ms $\qquad t$_exp$={:.1f}$ms $\qquad barrier$={:.0f}nK,'.format(
    time-10,10.1, 60)
plt.title(title)

s = sim0[1].get_state(t_=time, image=image);
x,y,z = s.xyz
nx = s.get_density_x().sum(axis=0)
plt.plot(x.ravel(), nx/nx.max(), label='3D')
plt.xlim(x[0] - _dx, x[-1] + _dx)

s = sim1[1].get_state(t_=time, image=image);
x,r = s.xyz
nx = s.get_density_x().sum(axis=0)
plt.plot(x.ravel(), nx/nx.max(), alpha=0.7, label='axial')
plt.xlabel('x (micron)', size='xx-large')
plt.legend(loc='lower right')

##########################
plt.subplot(313)
title = r't_wait$={:.1f}$ms $\qquad t$_exp$={:.1f}$ms $\qquad barrier$={:.0f}nK,'.format(
    time-10,10.1, 90)
plt.title(title)

s = sim0[2].get_state(t_=time, image=image);
x,y,z = s.xyz
nx = s.get_density_x().sum(axis=0)
plt.plot(x.ravel(), nx/nx.max(), label='3D')
plt.xlim(x[0] - _dx, x[-1] + _dx)

s = sim1[2].get_state(t_=time, image=image);
x,r = s.xyz
nx = s.get_density_x().sum(axis=0)
plt.plot(x.ravel(), nx/nx.max(), alpha=0.7, label='axial')
plt.xlabel('x (micron)', size='xx-large')
plt.legend(loc='lower right')

name = 'CAR_Fast_3DvAxial_t8ms_TOF.png'
plt.savefig(name)

# -

# ## .mp4 

# +
# %pylab inline --no-import-all
from gpe.soc import ExperimentBarrier, u
import SOC.soc_car_fast#;reload(SOC.soc_car_fast)
from SOC.soc_car_fast import ExperimentCARFast

#Sim data
import runs_car_fast_final
# 3D data
r0 = runs_car_fast_final.Run_IPG_small_3D()
#r0 = runs_car_fast_final.Run_IPG_small_3D_alt()
#r0 = runs_car_fast_final.Run_IPG_small_3D_big()
# Axial data for comparison
r1 = runs_car_fast_final.Run_IPG_xTF150_RNR()



barrier_depth_nK = -90 # -30, -60, -90
y_tilt = 0.02 # 0, 0.02, 0.1, 1

sim0 = [_sims for _sims in r0.simulations
       if _sims.experiment.barrier_depth_nK == barrier_depth_nK
       and _sims.experiment.y_tilt == y_tilt
       ][0]

sim1 = [_sims for _sims in r1.simulations
       if _sims.experiment.barrier_depth_nK == barrier_depth_nK
       ][0]

params0 = sim0.experiment.items()
params1 = sim1.experiment.items()
print(params0, params1)

s = sim0.get_state(t_=0, image=False);

# -

nx_max0 = sim0.get_state(t_=18, image=False).get_density_x().max()
nx_max1 = sim1.get_state(t_=18, image=False).get_density_x().max()


# +

def plot_func(time=0, fig=None):
    if fig is None:
        fig = plt.figure(figsize=(8,4))
    fig.clear()
    
    ##########################
    title = r't_wait$={:.1f}$ms, $\qquad t$_exp$={:.1f}$ms, $\qquad barrier={:.0f}$nK, $\qquad   tilt={:.2f}$'.format(
        8, time, barrier_depth_nK, y_tilt)
    plt.title(title)
    s = sim0.get_state(t_=18, image=time);
    x,y,z = s.xyz
    nx = s.get_density_x().sum(axis=0)
    plt.plot(x.ravel(), nx/nx_max0, label='3D')
    _dx = 7
    plt.xlim(x[0] - _dx, x[-1] + _dx)
    ##########################
    s = sim1.get_state(t_=18, image=time);
    x,r = s.xyz
    nx = s.get_density_x().sum(axis=0)
    plt.plot(x.ravel(), nx/nx_max1, alpha=0.7, label='axial')
    plt.xlabel(r'$x$ [micron]', size='xx-large')
    plt.legend(loc='upper right')
    plt.ylim(0,2.2)



# -

plot_func(time=10.1)

history = np.floor(10*np.linspace(0,10.1,102))/10
history

# +
from gpe import plot_for_khalid


fig = plt.figure(figsize=(8,4))
filename = 'CAR_Fast_3DvAxial_Barrier{}nK_tilt02_TOF.mp4'.format(abs(barrier_depth_nK))

plot_for_khalid.make_movie(filename,
                           history.tolist(), 
                           lambda ts:plot_func(time=ts, fig=fig), fps=12, fig=fig)

# -





# # Contour Plot 

# +
from mmfutils.plot import imcontourf

time = 0
image = False


s = sim0.get_state(t_=time, image=image);
x,y,z = s.xyz
n = s.get_density().sum(axis=0) 

plt.figure(figsize=(5,12))
plt.subplot(311)
imcontourf(x.ravel(), y.ravel(),
           n[:,:,z.ravel().shape[0]//2])
plt.colorbar()

plt.subplot(312)
imcontourf(x.ravel(), z.ravel(),
           n[:,y.ravel().shape[0]//2,:])
plt.colorbar()

plt.subplot(313)
imcontourf(y.ravel(), z.ravel(),
           n[x.ravel().shape[0]//2,:,:])
plt.colorbar()
# -


