"""Defines simulations to run with checkpoint/restart capabilities.

To use this, inherit from ExperimentBase, and pass any important variables to
the `local_dict` argument of the ExperimentBase.__init__() method.  These
variables will be used to generate unique filenames for each run.
"""
import itertools

import numpy as np

import mmf_setup.set_path.hgroot

from mmfutils.contexts import NoInterrupt

import mmfutils.performance.threads
import mmfutils.performance.fft

import sys

from gpe import utils

import soc_counter_flow

u = soc_counter_flow.u

_DATA_DIR = "_data"


def set_threads(numexpr=1, fft=1, *v):
    if v:
        numexpr = fft = v[0]
    mmfutils.performance.threads.set_num_threads(numexpr)
    mmfutils.performance.fft.set_num_threads(fft)


# Needs testing, but performance on swan did not show an appreciable benefit to
# using multiple cores.
set_threads(1)


class RunBase:
    dt_ = 1.0

    experiment_params = dict(
        cooling_phase=[1.0, 1 + 0.005j, 1 + 0.01j],
    )

    simulation_params = {}

    @property
    def experiments(self):
        params = self.experiment_params
        keys = params.keys()
        iterators = [params[_key] for _key in keys]
        return [
            self.Experiment(**dict(zip(keys, values)))
            for values in itertools.product(*iterators)
        ]

    @property
    def simulations(self):
        params = self.simulation_params
        keys = params.keys()
        iterators = [params[_key] for _key in keys]
        return [
            utils.Simulation(
                experiment=expt,
                dt_=self.dt_,
                data_dir=_DATA_DIR,
                **dict(zip(keys, values))
            )
            for expt in self.experiments
            for values in itertools.product(*iterators)
        ]

    def run(self):
        with NoInterrupt(ignore=True) as interrupted:
            for sim in self.simulations:
                if interrupted:
                    break
                sim.run()
                if interrupted:
                    break
                sim.run_images()


class Run0(RunBase):
    experiment_params = dict(
        basis_type=["tube"],
        Lx=[480 * u.micron],
        Nx=[2**13],
        x_TF=[900.0 * u.micron],
        cooling_phase=[1.0],
    )
    simulation_params = dict(
        image_ts_=[
            np.array(
                [0, 10, 50, 100, 150, 200, 250, 300, 350, 400, 450, 500, 550, 600, 1800]
            )
        ]  # This box is too small for 30ms expansion
    )
    simulation_params = dict(image_ts_=[])
    Experiment = soc_counter_flow.ExperimentCounterFlow


class RunMMF(RunBase):
    experiment_params = dict(
        cooling_phase=[1.0, 1.0 + 0.1j],
    )
    simulation_params = dict(
        image_ts_=[
            np.array(
                [0, 10, 50, 100, 150, 200, 250, 300, 350, 400, 450, 500, 550, 600, 1800]
            )
        ]
    )
    Experiment = soc_counter_flow.ExperimentCF


class RunMMFtube(RunBase):
    experiment_params = dict(
        cooling_phase=[1.0, 1.0 + 0.1j],
        tube=[True],
    )
    simulation_params = dict(
        image_ts_=[
            np.array(
                [0, 10, 50, 100, 150, 200, 250, 300, 350, 400, 450, 500, 550, 600, 1800]
            )
        ]
    )
    Experiment = soc_counter_flow.ExperimentCF


if __name__ == "__main__":
    cmd = sys.argv[1]
    if cmd in locals():
        locals()[cmd]().run()
    else:
        raise ValueError("Command {} not found.".format(cmd))
