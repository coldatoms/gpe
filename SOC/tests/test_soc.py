from __future__ import absolute_import, division, print_function, unicode_literals
import itertools
import numpy as np

from zope.interface.verify import verifyObject, DoesNotImplement

from mmfutils.math.differentiate import differentiate

from pytimeode import evolvers, interfaces
from pytimeode.utils.testing import TestState as _TestState

from SOC import soc_catch_and_release
from SOC.soc_catch_and_release import u

import pytest


def test_V_TF():
    """Test that all types of states used the same V_TF."""
    args = dict(
        B_gradient_mG_cm=0, barrier_depth_nK=-400, trapping_frequencies_Hz=(3.0, 200, 200)
    )
    args_s = dict(args, cells_x=100)
    e_axial = soc_catch_and_release.ExperimentCatchAndRelease(basis_type="axial", **args)
    e_tube = soc_catch_and_release.ExperimentCatchAndRelease(
        basis_type="1D", tube=True, **args
    )
    e_1D = soc_catch_and_release.ExperimentCatchAndRelease(
        basis_type="1D", tube=False, **args
    )
    es_axial = soc_catch_and_release.ExperimentCatchAndReleaseSmall(
        basis_type="axial", **args_s
    )
    es_tube = soc_catch_and_release.ExperimentCatchAndReleaseSmall(
        basis_type="1D", tube=True, **args_s
    )
    es_1D = soc_catch_and_release.ExperimentCatchAndReleaseSmall(
        basis_type="1D", tube=False, **args_s
    )
    V_TF = e_axial.get_fiducial_V_TF()
    for e in [e_tube, e_1D, es_axial, es_tube, es_1D]:
        assert np.allclose(V_TF, e.get_fiducial_V_TF())


def test_axial():
    """Check that small axial simulations match experimental parameters with
    full axial simulations."""
    args = dict(
        B_gradient_mG_cm=0, barrier_depth_nK=-400, trapping_frequencies_Hz=(3.0, 200, 200)
    )
    args_s = dict(args, cells_x=100)
    e_axial = soc_catch_and_release.ExperimentCatchAndRelease(basis_type="axial", **args)
    es_axial = soc_catch_and_release.ExperimentCatchAndReleaseSmall(
        basis_type="axial", **args_s
    )
    s = e_axial.get_state()
    ss = es_axial.get_state()
    assert np.allclose(e_axial.Lx_expt, es_axial.Lx_expt)
    assert np.allclose(s.basis.Lxr[1], ss.basis.Lxr[1])
    # assert np.allclose(s.basis.Nxr[1], ss.basis.Nxr[1])


def test_axial_expansion():
    """Check that axial expansion conserves energy and particle number."""
    args = dict(
        B_gradient_mG_cm=0,
        barrier_depth_nK=-400,
        trapping_frequencies_Hz=(3.0, 200, 200),
        t__final=0.0,
        cells_x=20,
    )
    e_axial = soc_catch_and_release.ExperimentCatchAndRelease(basis_type="axial", **args)
    s = e_axial.get_state()
    factor = 10
    e = evolvers.EvolverABM(s, dt=s.t_scale / factor)
    e.evolve(factor)
    N = e.y.get_N()
    E = e.y.get_energy()
    e.evolve(10 * factor)
    errs = [(e.y.get_N() - N) / N, (e.y.get_energy() - E) / E]
    print(errs)

    # assert np.allclose(e_axial.Lx_expt, es_axial.Lx_expt)
    # assert np.allclose(s.basis.Lxr[1], ss.basis.Lxr[1])
    # assert np.allclose(s.basis.Nxr[1], ss.basis.Nxr[1])


def test_branches():
    """Test explicit population of upper and lower SOC branches for
    homogeneous states."""

    e = soc_catch_and_release.ExperimentCatchAndReleaseSmall(
        tube=False,
        T__x=np.inf,
        basis_type="1D",
        species=((1, -1), (1, 0)),
        barrier_depth_nK=0.0,
        B_gradient_mG_cm=0.0,
        detuning_E_R=1.2,
        detuning_kHz=None,
        rabi_frequency_E_R=2.5,
        x_TF=232.5 * u.micron,
        cells_x=2,
        dx=0.05 * u.micron,
    )

    s = e.get_state()
    xs = s.xyz[0]
    np.random.seed(1)
    k0 = s.basis.kx[np.random.randint(len(s.basis.kx))]
    n0 = s.get_density_x().sum(axis=0).mean()
    psi_ab = s.get_branch_psi_ab(k=k0, n0=n0)
    s[0, ...] = psi_ab[0]
    s[1, ...] = psi_ab[1]
    n_upper, n_lower = np.array(s.get_branch_mixture())
    assert np.allclose(0, n_upper)
    assert np.allclose(n0, n_lower)
