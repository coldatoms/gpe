"""Defines simulations to run with checkpoint/restart capabilities.

To use this, inherit from ExperimentBase, and pass any important variables to
the `local_dict` argument of the ExperimentBase.__init__() method.  These
variables will be used to generate unique filenames for each run.
"""

import os.path

THREADS = 1

os.environ["MKL_NUM_THREADS"] = str(THREADS)
os.environ["NUMEXPR_NUM_THREADS"] = str(THREADS)
os.environ["OMP_NUM_THREADS"] = str(THREADS)

import itertools

import numpy as np

import mmf_setup.set_path.hgroot

from mmfutils.contexts import NoInterrupt

import mmfutils.performance.threads
import mmfutils.performance.fft

import sys

from gpe import utils

import soc_catch_and_release

u = soc_catch_and_release.u

_DATA_DIR = "_data"


def set_threads(numexpr=1, fft=1, *v):
    if v:
        numexpr = fft = v[0]
    mmfutils.performance.threads.set_num_threads(numexpr)
    mmfutils.performance.fft.set_num_threads(fft)


# Needs testing, but performance on swan did not show an appreciable benefit to
# using multiple cores.
set_threads(THREADS, THREADS)


class RunBase:
    dt_ = 1.0

    experiment_params = dict(
        cooling_phase=[1.0, 1 + 0.005j, 1 + 0.01j],
    )

    simulation_params = {}

    @property
    def experiments(self):
        params = self.experiment_params
        keys = params.keys()
        iterators = [params[_key] for _key in keys]
        return [
            self.Experiment(**dict(zip(keys, values)))
            for values in itertools.product(*iterators)
        ]

    @property
    def simulations(self):
        params = self.simulation_params
        keys = params.keys()
        iterators = [params[_key] for _key in keys]
        return [
            utils.Simulation(
                experiment=expt,
                dt_=self.dt_,
                data_dir=_DATA_DIR,
                **dict(zip(keys, values))
            )
            for expt in self.experiments
            for values in itertools.product(*iterators)
        ]

    def run(self):
        with NoInterrupt(ignore=True) as interrupted:
            for sim in self.simulations:
                if interrupted:
                    break
                sim.run()
                if interrupted:
                    break
                sim.run_images()


class Run0_axial_full(RunBase):
    """Full Simulation with Axial symmetry and the full experiemtal potential"""

    experiment_params = dict(
        cooling_phase=[1.0, 1 + 0.001j],
        basis_type=["axial"],
        barrier_depth_nK=[-46.0, -60.0],
        x_TF=[200.0 * u.micron, 232.5 * u.micron],
        rabi_frequency_E_R=[1.5],
        t__barrier=[0],
        harmonic_trap=[False],
    )
    simulation_params = dict(
        image_ts_=[
            np.array(
                [
                    0,
                    1,
                    2,
                    3,
                    4,
                    5,
                    6,
                    7,
                    8,
                    9,
                    10,
                    11,
                    12,
                    13,
                    14,
                    15,
                    20,
                    25,
                    30,
                    35,
                    40,
                ]
            )
        ]
    )
    Experiment = soc_catch_and_release.ExperimentCatchAndRelease


class Run1_axial_full(RunBase):
    """Same as Run0_axial_full but iwth small L and N, same dx"""

    experiment_params = dict(
        Lx=[480 * u.micron],
        Nx=[2**13],
        cooling_phase=[1.0, 1 + 0.001j],
        basis_type=["axial"],
        barrier_depth_nK=[-46.0, -60.0],
        x_TF=[200.0 * u.micron, 232.5 * u.micron],
        rabi_frequency_E_R=[1.5],
        t__barrier=[0],
        harmonic_trap=[False],
    )
    simulation_params = dict(
        image_ts_=[
            np.array(
                [
                    0,
                    1,
                    2,
                    3,
                    4,
                    5,
                    6,
                    7,
                    8,
                    9,
                    10,
                    11,
                    12,
                    13,
                    14,
                    15,
                    20,
                    25,
                    30,
                    35,
                    40,
                ]
            )
        ]
    )
    Experiment = soc_catch_and_release.ExperimentCatchAndRelease


class Run0_tube_small(RunBase):
    """Same as Run0_axial_full but iwth small L and N, same dx"""

    experiment_params = dict(
        basis_type=["1D"],
        tube=[True],
        cells_x=[1200],
        cooling_phase=[1.0, 1 + 0.001j],
        barrier_depth_nK=[-46.0 + 4],
        x_TF=[205.0 * u.micron],
        rabi_frequency_E_R=[1.5],
        t__barrier=[0],
        harmonic_trap=[False],
    )
    simulation_params = dict(
        image_ts_=[
            np.array(
                [
                    0,
                    1,
                    2,
                    3,
                    4,
                    5,
                    6,
                    7,
                    8,
                    9,
                    10,
                    11,
                    12,
                    13,
                    14,
                    15,
                    20,
                    25,
                    30,
                    35,
                    40,
                ]
            )
        ]
    )
    Experiment = soc_catch_and_release.ExperimentCatchAndReleaseSmall


class Run1_tube_small(RunBase):
    """Same as Run0_axial_full but iwth small L and N, same dx"""

    experiment_params = dict(
        basis_type=["1D"],
        tube=[True],
        cells_x=[1200],
        cooling_phase=[1.0],
        barrier_depth_nK=[-46.0, -46.0 + 4],
        x_TF=[
            135.0 * u.micron,
            140.0 * u.micron,
            145.0 * u.micron,
            150.0 * u.micron,
            155.0 * u.micron,
            160.0 * u.micron,
            165 * u.micron,
            175.0 * u.micron,
            185.0 * u.micron,
            195.0 * u.micron,
            205.0 * u.micron,
            215.0 * u.micron,
            225.0 * u.micron,
            235.0 * u.micron,
            245.0 * u.micron,
        ],
        rabi_frequency_E_R=[0, 1.5],
        t__barrier=[0],
        harmonic_trap=[False],
    )
    simulation_params = dict(
        image_ts_=[
            np.array(
                [
                    0,
                    1,
                    2,
                    3,
                    4,
                    5,
                    6,
                    7,
                    8,
                    9,
                    10,
                    11,
                    12,
                    13,
                    14,
                    15,
                    20,
                    25,
                    30,
                    35,
                    40,
                ]
            )
        ]
    )
    Experiment = soc_catch_and_release.ExperimentCatchAndReleaseSmall


class Run1_tube_small_temp(RunBase):
    """Same as Run0_axial_full but iwth small L and N, same dx"""

    experiment_params = dict(
        basis_type=["1D"],
        tube=[True],
        cells_x=[1200],
        cooling_phase=[1.0],
        barrier_depth_nK=[-46.0, -46.0 + 4],
        x_TF=[
            195.0 * u.micron,
            205.0 * u.micron,
            215.0 * u.micron,
            225.0 * u.micron,
            235.0 * u.micron,
            245.0 * u.micron,
        ],
        rabi_frequency_E_R=[0, 1.5],
        t__barrier=[0],
        harmonic_trap=[False],
    )
    simulation_params = dict(
        image_ts_=[
            np.array(
                [
                    0,
                    1,
                    2,
                    3,
                    4,
                    5,
                    6,
                    7,
                    8,
                    9,
                    10,
                    11,
                    12,
                    13,
                    14,
                    15,
                    20,
                    25,
                    30,
                    35,
                    40,
                ]
            )
        ]
    )
    Experiment = soc_catch_and_release.ExperimentCatchAndReleaseSmall


class Run_small_tube_scatter(RunBase):
    """Maren did some quick run with only 1ms of expansion with the purpose of nailing down the initial density.
    This is a scattershot attempt to replicate her data.
    These runs where done on 06/11/2018
    """

    experiment_params = dict(
        basis_type=["1D"],
        tube=[True],
        cells_x=[1200],
        cooling_phase=[1.0],
        barrier_depth_nK=[-40.0, -42.0, -44.0, -46.0, -48.0, -50.0, -52.0],
        x_TF=[
            135.0 * u.micron,
            140.0 * u.micron,
            145.0 * u.micron,
            150.0 * u.micron,
            155.0 * u.micron,
            160.0 * u.micron,
            165 * u.micron,
            175.0 * u.micron,
            185.0 * u.micron,
            195.0 * u.micron,
            205.0 * u.micron,
            215.0 * u.micron,
            225.0 * u.micron,
            235.0 * u.micron,
            245.0 * u.micron,
        ],
        rabi_frequency_E_R=[0, 1.5],
        t__barrier=[0],
        t__final=[1],
        t__image=[10.1],
    )
    simulation_params = dict(image_ts_=[np.array([0, 1])])
    Experiment = soc_catch_and_release.ExperimentCatchAndReleaseSmall


class Run_Omega_small_tube(RunBase):
    """omega = 0.75, 2.25
    Intended to compare to Marens data 181106
    """

    experiment_params = dict(
        basis_type=["1D"],
        tube=[True],
        cells_x=[1200],
        cooling_phase=[1.0],
        barrier_depth_nK=[-46.0],
        x_TF=[165.0 * u.micron, 175.0 * u.micron, 235.0 * u.micron],
        rabi_frequency_E_R=[0.75, 2.25],
        t__barrier=[0],
        harmonic_trap=[False],
    )
    simulation_params = dict(image_ts_=[np.array([0, 5, 10, 15, 20, 30])])
    Experiment = soc_catch_and_release.ExperimentCatchAndReleaseSmall


if __name__ == "__main__":
    cmd = sys.argv[1]
    if cmd in locals():
        locals()[cmd]().run()
    else:
        raise ValueError("Command {} not found.".format(cmd))
