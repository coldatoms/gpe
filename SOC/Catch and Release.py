# ---
# jupyter:
#   jupytext:
#     formats: ipynb,py
#     text_representation:
#       extension: .py
#       format_name: light
#       format_version: '1.5'
#       jupytext_version: 1.4.0
#   kernelspec:
#     display_name: Python [conda env:_gpe]
#     language: python
#     name: conda-env-_gpe-py
# ---

# + init_cell=true
import mmf_setup

mmf_setup.nbinit()

# + [markdown] toc=true
# <h1>Table of Contents<span class="tocSkip"></span></h1>
# <div class="toc"><ul class="toc-item"><li><span><a href="#Overview" data-toc-modified-id="Overview-1"><span class="toc-item-num">1&nbsp;&nbsp;</span>Overview</a></span></li><li><span><a href="#Homogeneous-Background" data-toc-modified-id="Homogeneous-Background-2"><span class="toc-item-num">2&nbsp;&nbsp;</span>Homogeneous Background</a></span><ul class="toc-item"><li><span><a href="#Scales" data-toc-modified-id="Scales-2.1"><span class="toc-item-num">2.1&nbsp;&nbsp;</span>Scales</a></span></li></ul></li><li><span><a href="#BdG" data-toc-modified-id="BdG-3"><span class="toc-item-num">3&nbsp;&nbsp;</span>BdG</a></span></li><li><span><a href="#Exploration" data-toc-modified-id="Exploration-4"><span class="toc-item-num">4&nbsp;&nbsp;</span>Exploration</a></span><ul class="toc-item"><li><span><a href="#Expanding-Bubble" data-toc-modified-id="Expanding-Bubble-4.1"><span class="toc-item-num">4.1&nbsp;&nbsp;</span>Expanding Bubble</a></span></li></ul></li><li><span><a href="#Experiments" data-toc-modified-id="Experiments-5"><span class="toc-item-num">5&nbsp;&nbsp;</span>Experiments</a></span><ul class="toc-item"><li><span><a href="#Small-Explorations" data-toc-modified-id="Small-Explorations-5.1"><span class="toc-item-num">5.1&nbsp;&nbsp;</span>Small Explorations</a></span><ul class="toc-item"><li><span><a href="#Expansion-Without-SOC" data-toc-modified-id="Expansion-Without-SOC-5.1.1"><span class="toc-item-num">5.1.1&nbsp;&nbsp;</span>Expansion Without SOC</a></span></li></ul></li><li><span><a href="#Main-Results" data-toc-modified-id="Main-Results-5.2"><span class="toc-item-num">5.2&nbsp;&nbsp;</span>Main Results</a></span><ul class="toc-item"><li><span><a href="#Without-Excitations" data-toc-modified-id="Without-Excitations-5.2.1"><span class="toc-item-num">5.2.1&nbsp;&nbsp;</span>Without Excitations</a></span></li><li><span><a href="#Axial-Simulations" data-toc-modified-id="Axial-Simulations-5.2.2"><span class="toc-item-num">5.2.2&nbsp;&nbsp;</span>Axial Simulations</a></span></li></ul></li><li><span><a href="#Export-Plots" data-toc-modified-id="Export-Plots-5.3"><span class="toc-item-num">5.3&nbsp;&nbsp;</span>Export Plots</a></span><ul class="toc-item"><li><span><a href="#Small" data-toc-modified-id="Small-5.3.1"><span class="toc-item-num">5.3.1&nbsp;&nbsp;</span>Small</a></span></li></ul></li><li><span><a href="#With-Excitations" data-toc-modified-id="With-Excitations-5.4"><span class="toc-item-num">5.4&nbsp;&nbsp;</span>With Excitations</a></span></li><li><span><a href="#Other" data-toc-modified-id="Other-5.5"><span class="toc-item-num">5.5&nbsp;&nbsp;</span>Other</a></span></li></ul></li><li><span><a href="#Imaging" data-toc-modified-id="Imaging-6"><span class="toc-item-num">6&nbsp;&nbsp;</span>Imaging</a></span></li></ul></div>
# -

# # Overview

# The catch and release experiment consists of a large trapped gas in a SOC BEC, which acts as a finite-density background.  On this, various excitations are created, such as a mockup of our original negative-mass experiment, in order to explore how the background affects the behavior of the system.  Relevant questions are:
#
# * What determines the dynamical behavior?  Is it the original negative-mass hydrodynamic picture or is it the density dependent quasi-particle (phonon) spectrum?
# * If it is the latter, can we probe the roton branch, and in particular, can we see negative slope similar to as in the R-roton He experiments?

# # Homogeneous Background

# We start with homogeneous states.

# ## Scales

# We begin with a discussion of the relevant length and time-scales in the problem.  The background for this discussion can be seen in the [Adiabaticity.ipynb](Adiabaticity.ipynb) notebook. (General theory is discussed in the [Homogeneous.ipynb](Homogeneous.ipynb) notebook.)
#
# In the limit $g = g_{aa} = g_{bb} = g_{ab}$, the physics is governed by the following dimensionless parameters.  Everything here is scaled in units of either $2E_R$ (energies),  microns (lengths), or ms (`t_unit` for times):
#
# * Detuning $d=\delta/4E_R$ and SOC strength $w=\Omega/4E_R$.
# * Background density $n_0$ in ($1/\mu$m$)^3$.
# * Recoil length $l_r = 1/k_r$ (in microns).
# * Healing length $\xi = \sqrt{\hbar^2/2m\mu}$ (in $\mu$m).
# * Barrier width $l_B$ (in $\mu$m).
# * Peak density $n_B$ in ($1/\mu$m$)^3$.  (We express this as a fraction of $n_0$).
# * Recoil timescale $T_R = \pi \hbar/2E_R$ (in ms).
#
# Here are the experimental values:
#
# * $d$ starts at $1.36$ and changes to $-0.068$
# * $0.25 < w < 0.73$.
# * $k_r \approx 7.9/\mu$m.
# * $n_0 \approx 160/\mu$m$^3$.
# * $\xi \approx 0.026\mu$m
# * $T_x \approx 326$ms
# * $T_\perp \approx 3.6$ms
# * $T_R = \pi\hbar/2E_R \approx 0.086$ms
# * $T_\omega \approx 0.13$ms

# The evolution of a homogeneous state should be to rotate on the Bloch sphere (see [`Homogeneous.ipynb`](Homogeneous.ipynb):
#
# In the rotating phase basis, we can consider homogeneous states.  (*States with a different relative momentum will develop density oscillations.*)
#
# $$
#   \tilde{\Psi} =
#   \begin{pmatrix}
#     e^{-\I k_r x}\psi_a\\
#     e^{\I k_r x}\psi_b
#   \end{pmatrix}
#   = e^{\I k x}
#   \begin{pmatrix}
#     \sqrt{n_a}\\
#     \sqrt{n_b}
#   \end{pmatrix}, \qquad
#   \I\hbar\partial_t
#   \tilde{\Psi}
#   =
#   \hbar(\omega_0\mat{1} + \vec{\omega}\cdot\vec{\mat{\sigma}})\tilde{\Psi}\\
#   \bar{\mu} \equiv \mu - \frac{(g_{aa} + g_{ab})n_a + (g_{ab} + g_{bb})n_b}{2}, \qquad
#   \frac{\bar{\delta}}{2} \equiv \frac{\delta}{2} - \frac{(g_{aa} - g_{ab})n_a + (g_{ab} - g_{bb})n_b}{2},\\
#   \hbar\omega_0 = \frac{\hbar^2(k^2 + k_r^2)}{2m} - \bar{\mu} , \qquad
#   \hbar\vec{\omega} = \begin{pmatrix}
#     \Omega\\
#     0\\
#     \frac{2\hbar^2\vect{k}\cdot\mat{k}_r}{m} - \bar{\delta}
#   \end{pmatrix}.
# $$
#
# Where the density matrix is:
#
# $$
#   \mat{\rho} = \ket{\tilde{\Psi}}\bra{\tilde{\Psi}}
#   = \frac{1}{2}\left(\mat{1} + \vec{a}\cdot\vec{\mat{\sigma}}\right), \qquad
#   \vec{a}' = e^{t\vec{\omega}\times}\cdot\vec{a}.
# $$
#
# In the case of equal couplings, the latter expression is just the detuning.
#
# $$
#   \bar{\mu} \equiv \mu - gn, \qquad
#   \bar{\delta} \equiv \delta.
# $$
#
# Stationary states of the system thus correspond to $\vec{a} \propto \vec{\omega}$.

# +
from gpe.imports import *
from SOC.soc_catch_and_release import u, ExperimentCatchAndReleaseSmall


class Experiment(ExperimentCatchAndReleaseSmall):
    barrier_depth_nK = 0.0
    detuning_E_R = 4.0
    detuning_kHz = None
    rabi_frequency_E_R = 1.5
    # recoil_frequency_Hz = 1000.0

    def Omega_t_(self, t_):
        """Jump on SOC"""
        if t_ <= 0:
            return 0.0
        else:
            return self.Omega


# This state has 2 points which should be fine for homogeneous states.
e = Experiment(cells_x=1, dx=0.2 * u.micron)
s = e.get_state()
s.shape
# -

# Here we consider an initial state $\Psi = (\sqrt{n_a}, 0)$.  Note that $\tilde{\Psi} = e^{-\I k_r x}\Psi$, hence this state has $k = - k_r$.  Thus, we expect the state to rotate about the Bloch sphere with frequency:
#
# $$
#   \hbar\omega = \sqrt{\Omega^2 + (2\hbar^2k_r^2/m + \bar{\delta})^2}.
# $$

np.sqrt(1.5 ** 2 + (4.0 - 4.0) ** 2)

s[0, ...] = 16.0
s[1, ...] = 0.0
kr = e.k_r
k = 0.0
gaa, gbb, gab = s.gs
m = s.ms[0]
h = s.hbar
na, nb = s.get_density()[:, 0]
delta_bar = e.delta - (gaa - gab) * na + (gab - gbb) * nb
mu_bar = -((gaa + gab) * na + (gab + gbb) * nb) / 2.0
w = np.sqrt(e.Omega ** 2 + (2 * (h * kr) ** 2 / m + delta_bar) ** 2) / h
print(w / (2 * np.pi * u.kHz), w / e.Omega)

# +
from scipy.optimize import leastsq

# s = e.get_initial_state()
ev = EvolverABM(s, dt=0.01 * s.t_scale)
ts = []
Ns = []
with NoInterrupt() as interrupted:
    while not interrupted and ev.t < 0.4 * s.t_unit:
        ev.evolve(4)
        ts.append(ev.t)
        Ns.append(ev.y.get_Ns())

ts, Ns = map(np.asarray, (ts, Ns))
plt.plot(ts / u.ms, Ns)


def f((w, amplitude, offset)):
    return offset + amplitude * np.cos(w * ts) - Ns[:, 0]


ns = Ns[:, 0]
ws = 2 * np.pi * np.fft.fftfreq(len(ts), np.diff(ts).mean())
w0 = abs(ws[np.argmax(abs(np.fft.fft(ns - ns.mean())))])
q, err = leastsq(f, (w0, (ns.max() - ns.min()) / 2, ns.mean()))
plt.plot(ts / u.ms, f(q) + Ns[:, 0], "+")
print(
    "w={:.4f}kHz={:.4f}E_R={:.4f}Omega".format(
        q[0] / (2 * np.pi * u.kHz), q[0] * u.hbar / e.E_R, q[0] * u.hbar / e.Omega
    )
)
# -

# We start with homogeneous states background.  The
# in which we put a perturbation.

# # BdG

# $$
#   \omega = E_- \pm \sqrt{E_+(E_+ + 2gn)}, \qquad
#   E_{-} = \frac{E(k+q) - E(k-q)}{2}, \qquad
#   E_{+} = \frac{E(k-q) + E(k+q) - 2E(k)}{2}.
# $$
#
# In the ground state of the lower band, we have
#
# $$
#   E(k) = E_0 + \frac{(k-k_0)^2}{2m^*} + C (k-k_0)^3 + \order(k-k_0)^4\\
#   \frac{1}{m^*} = E''(k_0), \qquad
#   C = \frac{E'''(k_0)}{3!}.
# $$
#
# The BdG spectrum about $k=k_0$ then simplifies to:
#
# $$
#   \omega = \pm cq\left(1 + \frac{q^2}{2m_*}\frac{1}{4m_*c^2}\right) + C q^3 + \order(q^5), \qquad
#   c = \sqrt{\frac{gn}{m_*}},\\
#   v_p = \frac{\omega}{q} = \pm c\left(1 + \frac{q^2}{2m_*}\frac{1}{4m_*c^2}\right) + C q^2 + \order(q^4)\\
#   v_g = \pdiff{\omega}{q} = \pm c\left(1 + \frac{3q^2}{2m_*}\frac{1}{4m_*c^2}\right)
#    + 3C q^2 + \order(q^4)
# $$
#
# $$
#   E(k) = \frac{k^2+1}{2} - D, \qquad
#   D = \sqrt{(k-d)^2 + w^2}\\
#   E'(k) = k - K(k), \qquad
#   K(k) = D'(k) = \frac{k-d}{D}\\
#   E''(k) = 1 - K'(k) = 1 - \frac{w^2}{D^3}\\
#   E'''(k) = 3\frac{w^2(k-d)}{D^5} = 3\frac{w^2}{D^4}K(k)\\
# $$
#
# In the ground state, $K(k_0) = k_0$ and $D_0 = D(k_0) = (k_0-d)/k_0$, so
#
# $$
#   E''(k_0) = \frac{1}{m_*} = 1 - \frac{w^2}{D_0^3}\\
#   E'''(k_0) = C = 3\frac{w^2k_0}{D_0^4}
#   = \frac{3}{D_0}\left(1 - \frac{1}{m_*}\right)\\
# $$

# +
# %pylab inline
import numpy as np

w = 0.475
d = -0.2
# w=0.
# d=0.

D = lambda k: np.sqrt((k + d) ** 2 + w ** 2)
E = lambda k: (k ** 2 + 1) / 2 - D(k)
K = lambda k: (k + d) / D(k)
dK = lambda k: w ** 2 / D(k) ** 3
dE = lambda k: k - K(k)
ddE = lambda k: 1 - dK(k)


def omega(q, k0, c=1):
    m = 1.0 / ddE(k0)
    gn = c ** 2 * m
    Em = (E(k0 + q) - E(k0 - q)) / 2
    Ep = (E(k0 + q) + E(k0 - q) - 2 * E(k0)) / 2
    w1 = Em + np.sqrt(Ep * (Ep + gn))
    w2 = Em - np.sqrt(Ep * (Ep + gn))
    return w1


k0 = -0.92071623092981747
# k0 = -1
E0 = E(k0)
D0 = D(k0)
print(D0)
C0 = 3 * w ** 2 * k0 / D0 ** 4
print(C0)
m = 1.0 / (1 - w ** 2 / D0 ** 3)
print(m)
ks = np.linspace(-3, 3, 100)
plt.plot(ks, E(ks))
plt.axvline([k0])
w1 = np.vectorize(omega)(ks, k0)
plt.plot(ks + k0, w1 + E0, ":y")
# plt.plot(ks+k0, w2, ':y')
# -

plt.plot(q[1:], np.diff(w1 + E0))


# +
q = np.linspace(-3, 3, 100)
Em = (E(k0 + q) - E(k0 - q)) / 2
dEm = (dE(k0 + q) - dE(k0 - q)) / 2
ddEm = (ddE(k0 + q) - ddE(k0 - q)) / 2

# plt.plot(q,Em)
# plt.plot(q,dEm)
plt.plot(q, ddEm)

# -

plt.plot(q, Em + abs(Ep))


# +
gn = 1.0 / ddE(k0) + 100
q = np.linspace(-3, 3, 100)

Ep = (E(k0 + q) + E(k0 - q) - 2 * E(k0)) / 2
dEp = (dE(k0 + q) + dE(k0 - q)) / 2
ddEp = (ddE(k0 + q) + ddE(k0 - q)) / 2

w1 = np.sqrt(Ep * (Ep + gn))
dw1 = (2 * Ep + gn) * dEp / 2.0 / w1


# plt.plot(q,w1)
# plt.plot(q,dw1)
# plt.axhline(0)

plt.plot(q, Ep)
plt.plot(q, dEp)
print(gn)

# -

from bec2 import u

k0 = 1 + -0.92071623092981747
print(k0)
k0 * expt.k_r * u.hbar / u.m / (u.mm / u.s)


# # Exploration

# %load_ext autoreload
# %autoreload 2
import gpe.soc
from soc_catch_and_release import (
    ExperimentCatchAndRelease,
    ExperimentCatchAndReleaseSmall,
)

expt = ExperimentCatchAndRelease(tube=False, detuning_kHz=1.0, single_band=True)
s = expt.get_state()
s.plot(show_momenta=True)
# expt.detuning/4/expt.E_R

E = s.get_dispersion()
E.get_k0()

# ## Expanding Bubble

# Here we explore an expanding bubble in a superfluid background.  We present a couple of cases here, but a more detailed exploration in:
#
# * [`Details/Expanding Bubbles.ipynb`](Details/Expanding Bubbles.ipynb)

expt = ExperimentCatchAndReleaseSmall(
    tube=False,
    d=0.2,
    barrier_width_micron=1,
    barrier_depth_nK=-200,
    # barrier_width_micron=2, barrier_depth_mu=-20.0
    gaussian=False,
    x_TF=1,
)
s = expt.get_state()
s.plot()

# %%time
s = expt.get_initial_state(cool_steps=200, E_tol=1e-10, psi_tol=1e-6)
s.plot()

from IPython.display import clear_output
from pytimeode.evolvers import EvolverABM
from mmfutils.contexts import NoInterrupt

s.cooling_phase = 1.0
e = EvolverABM(s, dt=0.2 * s.t_scale)
fig = None
with NoInterrupt() as interrupted:
    while not interrupted:
        e.evolve(200)
        fig, grid = e.y.experiment.plot1(e.y, fig=fig, show_momenta=True, parity=True)
        display(fig)
        clear_output(wait=True)

from pytimeode.evolvers import EvolverABM
from mmfutils.contexts import NoInterrupt

s.cooling_phase = 1.0
e = EvolverABM(s, dt=0.4 * s.t_scale)
fig = None
with NoInterrupt() as interrupted:
    while not interrupted:
        e.evolve(200)
        fig = e.y.plot(fig=fig)
        display(fig)
        clear_output(wait=True)

# # Experiments

# The base for the experiments is a large cloud, prepared in the ground state, with a small bump produced with a narrow "bucket" potential.  The bucket potential is then turned off, allowing the bump to expand on top of the background.  This is done both with and without the SOC, and we explore both small and large perturbations.  For small perturbations, we expect that we are exploring the BdG excitations, while larger perturbations explore some of the non-linear features of the system.
#
# There are some subtle effects, and imaging (after expansion for 7ms) seems to be non-trivial.
#
# To proceed, we establish realistic runs matching the experiments, and then corresponding small-scale simulations to quickly explore.  The relevant code is in the following files:
#
# * [`soc_catch_and_release.py`](soc_catch_and_release.py): Defines the experimental parameters etc.
# * [`runs_catch_and_release.py`](runs_catch_and_release.py): Specifies the actual runs for which we record data.

# + init_cell=true
# %pylab inline --no-import-all
from IPython.display import clear_output, display
import holoviews as hv
from IPython.display import clear_output

hv.notebook_extension("bokeh")
# -

# ## Small Explorations

# Notes: 23 January
# - Setting tube=False seems to make a big difference - we do not get the pilup on the right. This might be a bug matching conditions, but may indicate some importance in the trap.
# - Reducing the "barrier" by a factor of 1.5 just starts populating negative mass region.
# - Reducing the "barrier" by a factor of 2 seems to make two nice shockwaves demonstrating the difference between positive and negative mass shockwaves.
# - Reducing by a factor of 8 or more starts getting into phonon regime (but probably depends on width).
#   - Confirmed.  Basically, the force is $F \sim -V_{\text{eff}}'(x) \sim -gn_{b}'(x)$ where $n_b(x) = n(x) - n_0 \sim V_b/g$ is the density contrast.  Thus, $F\sim V_b/l_b$ where $l_b$ is the barrier width and $V_b$ is the barrier height.  Now $F = \hbar\dot{k}$ where $k$ is the quasi-momentum, so the a relevant measure of the non-linearities is if $k$ changes by something comparable to $k_r$ over the time in which the bulge splits $\tau \approx l_b/c_s$ where $c_s = \sqrt{\mu/m}$ is the speed of sound.  Thus, we have the condition for non-linearity
#
#     $$
#       F\tau \gtrsim \hbar k_r, \qquad
#       \frac{V_b}{l_b}\frac{l_b}{c_s} \gtrsim \hbar k_r, \qquad
#       V_b\sqrt{\frac{m}{\mu}} \gtrsim \hbar k_r, \qquad
#     $$
#
#     Using the experimental parameters, this gives an estimate of $V_b \lesssim 200$nK.  This seems to be born out in the experiments, although if you really want no non-linearities you to suppress the RHS by a factor of 2 to 4 or so.

# %load_ext autoreload
# %autoreload 2
import soc_catch_and_release

reload(soc_catch_and_release)
from soc_catch_and_release import u

expt = soc_catch_and_release.ExperimentCatchAndReleaseSmall(
    tube=True,
    cells_x=400,
    # x_TF=10,
    # barrier_depth_nK=-44.5,  # 50mW
    barrier_depth_nK=-200,
    barrier_width_micron=2.4,
)
s = expt.get_initial_state(cool_steps=200, E_tol=1e-5, psi_tol=1e-4)
s.plot()

from pytimeode.evolvers import EvolverABM
from mmfutils.contexts import NoInterrupt

s.cooling_phase = 1.0 + 0.001j
s.experiment.t__final = 10.0
e = EvolverABM(s, dt=0.4 * s.t_scale)
fig = None
with NoInterrupt() as interrupted:
    while not interrupted and e.y.t < 10.0 * u.ms:
        e.evolve(500)
        fig = e.y.plot(fig=fig, show_momenta=True, show_mixtures=True)
        display(fig)
        clear_output(wait=True)

s1 = e.get_y()
x_, n_ = s1.experiment.simulate_image(s1)
plt.plot(x_, n_.sum(axis=0))
plt.plot(s1.xyz[0], -s1.get_density().sum(axis=0))

s1 = e.get_y()
x_, n_ = s1.experiment.simulate_image(s1)
plt.plot(x_, n_.sum(axis=0))
plt.plot(s1.xyz[0], -s1.get_density().sum(axis=0))

s1 = sim0.get_state(t_=2)
clear_output()
s1.plot()
plt.xlim(-20, 20)

# ### Expansion Without SOC

# Here we look at the expansion of a hump without SOC and compare it with regular evolution.

# %load_ext autoreload
# %autoreload 2
import soc_catch_and_release

reload(soc_catch_and_release)
from soc_catch_and_release import u

args = dict(
    rabi_frequency_E_R=0,
    tube=True,
    cells_x=800,
    x_TF=10 * u.micron,
    gaussian=True,
    # barrier_depth_nK=-200.0,
    # barrier_depth_nK=-890*2.5, #250uW
    # barrier_depth_nK=-890*2,  #200uW
    # barrier_depth_nK=-890,    #100uW
    barrier_depth_nK=-445,  # 50uW
    barrier_width_micron=2.4,
)
expt = soc_catch_and_release.ExperimentCatchAndReleaseSmall(**args)
s = expt.get_initial_state(cool_steps=0, E_tol=1e-5, psi_tol=1e-4)
s.plot()

s.plot()

# +
from pytimeode.evolvers import EvolverABM
from mmfutils.contexts import NoInterrupt

s.cooling_phase = 1.0 + 0.000j
s0 = s.copy()
s0.experiment = s.experiment.copy()
s0.experiment.t__final = 0.0

es = [EvolverABM(s0, dt=0.2 * s.t_scale), EvolverABM(s, dt=0.2 * s.t_scale)]

fig = plt.figure(figsize=(10, 5))
with NoInterrupt() as interrupted:
    while not interrupted and es[0].y.t < 7.0 * u.ms:
        [e.evolve(200) for e in es]
        plt.clf()
        x = es[0].y.xyz[0].ravel()
        plt.subplot(211)
        plt.plot(x, es[0].y.get_density().sum(axis=0), label="expanding")
        plt.plot(x, -es[1].y.get_density().sum(axis=0), label="evolving")
        plt.ylabel("n_1D")
        plt.legend()

        plt.subplot(212)
        plt.plot(x, es[0].y.get_central_density(TF=True).sum(axis=0), label="expanding")
        plt.plot(x, es[1].y.get_central_density(TF=True).sum(axis=0), label="evolving")
        plt.ylabel("central density")
        plt.legend()

        plt.suptitle("t={:.2f}ms".format(es[0].y.t / u.ms))
        display(plt.gcf())
        clear_output(wait=True)
# -

# ## Main Results

# Here are the main results for comparison with experiments.  These results are characterized by the code in the file [`runs_catch_and_release.py`](runs_catch_and_release.py), in particular the following:
#
# * `Run0`: Here we start with the ground state with SOC in the presence of the barrier, and watch the evolution with imaging at various times in accordance with the experimental data.
# * `Run10`: Here we start from the ground state with SOC but **without** the barrier, then turn on the barrier for `t__barrier=10`ms.  Imaging times here must be delayed by `t__barrier` compared with the experiment.
# * `Run0_200`, `Run10_200`: Same as above but with a lower `barrier_depth_nK=-200`.

# +
# %pylab inline --no-import-all
from IPython.display import clear_output, display
import holoviews as hv
from IPython.display import clear_output

hv.notebook_extension("bokeh")

from gpe.utils import Simulation2
from soc_catch_and_release import ExperimentCatchAndRelease

args = dict(cooling_phase=1.0, t__barrier=0)
expt0 = ExperimentCatchAndRelease(**args)
expt0_200 = ExperimentCatchAndRelease(barrier_depth_nK=-200.0, **args)

args["t__barrier"] = 10
expt10 = ExperimentCatchAndRelease(**args)
expt10_200 = ExperimentCatchAndRelease(barrier_depth_nK=-200.0, **args)

# +
# %%output size=100
# %%opts QuadMesh [height=300 width=900 colorbar=True tools=['hover'] toolbar='above']
# %%opts Image [height=300 width=900 colorbar=True tools=['hover'] toolbar='above']
# %%opts Curve [height=300 width=900] {+framewise}
# %%opts QuadMesh (cmap='viridis')
# %%opts Image (cmap='viridis')
import logging

sim = Simulation2(expt0_200, logging_level=logging.ERROR)
sim.view_hv(image=False)

# +
# %%output size=100
# %%opts QuadMesh [height=300 width=900 colorbar=True tools=['hover'] toolbar='above']
# %%opts Image [height=300 width=900 colorbar=True tools=['hover'] toolbar='above']
# %%opts Curve [height=300 width=900] {+framewise}
# %%opts QuadMesh (cmap='viridis')
# %%opts Image (cmap='viridis')

sim = Simulation2(expt0_200, logging_level=logging.ERROR)
sim.view_hv(image=True)
# -

t = 10.0
ts = np.arange(1.0, 11.0)
fig = None
for t_ in ts:
    state = sim.get_state(t, image=t_)
    fig = state.plot(fig=fig)
    display(fig)
    clear_output(wait=True)

# ### Without Excitations

# * No SOC: 2.10(5) (right), 1.93(5)(left) mm/s  (slightly larger than c)
# * With SOC: 2.30(4) (right), 1.77(6)(left) mm/s

from gpe.soc import u

expt0.Lxyz[0] / u.micron

# +
from gpe.utils import Simulation2
from soc_catch_and_release import ExperimentCatchAndRelease

expt10 = ExperimentCatchAndRelease(cooling_phase=1.0, t__barrier=10)
expt0 = ExperimentCatchAndRelease(cooling_phase=1.0 + 0.005j, t__barrier=0)

sim0 = Simulation2(expt0)
sim10 = Simulation2(expt10)

# +
# %%output size=100
# %%opts QuadMesh [height=300 width=900 colorbar=True tools=['hover'] toolbar='above']
# %%opts Image [height=300 width=900 colorbar=True tools=['hover'] toolbar='above']
# %%opts Curve [height=300 width=900] {+framewise}
# %%opts QuadMesh (cmap='viridis')
# %%opts Image (cmap='viridis')

# sim10.view(show_momenta=True)
# sim10.experiment.t__final
sim10.view_hv()
# -

# %pylab notebook
s = sim10.get_state(t_=10, image=False)
s.plot()

s = sim10.get_state(t_=10, image=True)
s.plot()

# +
# %%output size=100
# %%opts QuadMesh [height=300 width=900 colorbar=True tools=['hover'] toolbar='above']
# %%opts Image [height=300 width=900 colorbar=True tools=['hover'] toolbar='above']
# %%opts Curve [height=300 width=900] {+framewise}
# %%opts QuadMesh (cmap='viridis')
# %%opts Image (cmap='viridis')

img = sim0.view_hv()
clear_output()
img

# +
# %%output size=100
# %%opts QuadMesh [height=300 width=900 colorbar=True tools=['hover'] toolbar='above']
# %%opts Image [height=300 width=900 colorbar=True tools=['hover'] toolbar='above']
# %%opts Curve [height=300 width=900] {+framewise}
# %%opts QuadMesh (cmap='viridis')
# %%opts Image (cmap='viridis')

img = sim10.view_hv().redim.range(t=(10, 50))
clear_output()
img
# -

# ### Axial Simulations

import runs_catch_and_release

r = runs_catch_and_release.Run0_200_axial()
e = r.experiments[0]
s = e.get_state()
# %timeit s.compute_dy_dt(dy=s.copy())

130 / 0.04 * 10 / 60 / 60 / 24

# ## Export Plots

# Here we are attempting to compare the experimental images with our simulations.  A couple of points:
#
#
# 1. We had to symlink the data-directory here to load the data.
# 2. The data seems to match quite well spatially if the pixel width is 1.6 micron (not 1.116 micron).
# 3. We need to simulate errors with averaging about $N_y=2000$ pixels to get comparable simulation noise.  Perhaps my noise estimates are off.
# 4. Both FAST and SLOW experiments seem to agree quite nicely once these adjustments are made.

# +
from collections import namedtuple
from glob import glob
import gzip
import os.path
import re
import numpy as np

DATA_DIR = "CatchAndRelease_data/"

regexp = re.compile(r"_(\d+)ms")
file_dict = {}
for speed in ["FAST", "SLOW"]:
    for soc in ["SOC", "noSOC"]:
        globname = os.path.join(DATA_DIR, "_".join([speed, soc]), "*")


def load_file(filename, header=2):
    with gzip.GzipFile(filename) as f:
        head = "\n".join([next(f) for x in xrange(header)])
        data = np.loadtxt(f)
    return namedtuple("Data", ("info", "x", "n"))(head, data[:, 0], data[:, 1])


def load_experiment(dir, quiet=True):
    globname = os.path.join(DATA_DIR, dir, "*")
    files = glob(globname)
    t_ = {int(regexp.findall(file)[0]): file for file in files}
    ts = np.asarray(sorted(t_))
    xs = None
    ns = []
    for t in ts:
        data = load_file(t_[t])
        if xs is None:
            xs = data.x
        else:
            # Check that all abscissa are the same
            assert np.allclose(xs, data.x)
        ns.append(data.n)
        if not quiet:
            print(data.info)
    return (ts, np.asarray(xs), np.asarray(ns))


data_dict = {
    key: {t: load_file(file_dict[key][t]) for t in file_dict[key]} for key in file_dict
}

_dict = data_dict

# expt_data = load_experiment('SLOW_SOC')
expt_data = load_experiment("FAST_SOC")
t_, x_, ns_ = expt_data

# +
# %pylab inline
from IPython.display import clear_output
import numpy as np
import scipy as sp
import scipy.signal
from mmfutils.plot import imcontourf
from gpe.utils import Simulation2
from soc_catch_and_release import (
    u,
    ExperimentCatchAndRelease1a,
    ExperimentCatchAndRelease,
)

expt0 = ExperimentCatchAndRelease(cooling_phase=1.0 + 0.001j, t__barrier=0)
sim0 = Simulation2(
    expt0,
    image_ts_=[
        0,
        1,
        2,
        3,
        4,
        5,
        6,
        7,
        8,
        9,
        10,
        11,
        12,
        13,
        14,
        15,
        20,
        25,
        30,
        35,
        40,
    ],
)

expt10 = ExperimentCatchAndRelease(cooling_phase=1.0, t__barrier=10)
sim10 = Simulation2(
    expt10,
    image_ts_=[
        10,
        11,
        12,
        13,
        14,
        15,
        16,
        17,
        18,
        19,
        20,
        21,
        22,
        23,
        24,
        25,
        20,
        30,
        35,
        40,
    ],
)

sim, expt = sim0, expt0

A = 8.85
delta = 1.116 * u.micron

ts = np.array(sim.image_ts_)
xs = expt.get_state().xyz[0]


def image(t_, **kw):
    args = dict(Ny=100, pixel_size=delta)
    args.update(kw)
    state = sim.get_state(t_=t_, image=True)
    xs, ns = sim.experiment.simulate_image(state, **args)
    return xs, ns


Xs = image(10)[0]
ns = np.array([image(_t)[1].sum(axis=0) for _t in ts])
clear_output()
# -

i = 20
# i = 10
# i = 20
# plt.plot(ns[i][55:-55], alpha=0.6, label=str(ts[i]))
plt.plot(ns[i][190:-190], alpha=0.6, label=str(ts[i]))
plt.plot(ns_[i] / (A * delta), label=str(t_[i]))
plt.ylim(0, 14000)
plt.legend()

plt.figure(figsize=(20, 10))
plt.subplot(223)
delta = 1.116 * u.micron
A = 10.0
N = ns.sum(axis=1) * delta
imcontourf(ts, Xs[55:-55], ns[:, 55:-55] / N[:, None] / A)
plt.xlabel("t [ms]")
plt.ylabel("x [micron]")
plt.colorbar()

# +
plt.figure(figsize=(20, 10))

# Pixel size - 1.1166 micron/pixel
window = np.exp(-((xs / 6.116) ** 2) / 2)
ns_ = np.array([sp.signal.convolve(n, window, mode="same") for n in ns])
plt.subplot(223)

imcontourf(ts, xs, ns_ / ns_.max())
plt.xlabel("t [ms]")
plt.ylabel("x [micron]")
plt.colorbar()
# -

imcontourf(d["t"], d["x"], n)

# +
from IPython.display import clear_output

# %pylab inline --no-import-all
from mmfutils.plot import imcontourf
from gpe.utils import Simulation2
from soc_catch_and_release import ExperimentCatchAndRelease1a, ExperimentCatchAndRelease

expt10 = ExperimentCatchAndRelease(cooling_phase=1.0, t__barrier=10)
expt0 = ExperimentCatchAndRelease(cooling_phase=1.0, t__barrier=0)

sim0 = Simulation2(expt0)
sim10 = Simulation2(expt10)
d = sim10.data
clear_output()
plt.figure(figsize=(20, 5))
n = d.sel(species="a") + d.sel(species="b")
n = n / n.max()
imcontourf(d["t"], d["x"], n)
plt.xlim(0, 40)
plt.xlabel("t [ms]")
plt.ylabel("x [micron]")
plt.colorbar()
# -

n / n.max(axis=0)

sim.view(show_momenta=True, show_mixtures=True)
# sim.get_state(t_=0).plot()

# ### Small

# * No SOC: 2.96(54) (right), 1.87(49)(left) mm/s  (slightly larger than c)
# * With SOC: 2.20(9) (right), 2.05(21)(left) mm/s

expt = ExperimentCatchAndRelease(cooling_phase=1.0, t__barrier=10, cells_x=100)
expt.get_state().plot()

# ## With Excitations

import numpy as np
from gpe.soc import u

s = expt.get_state()
s.get_mu() / u.nK, s.ms[0] * (
    2 * np.pi * 3.07 * u.Hz * 232.5 * u.micron
) ** 2 / 2.0 / u.nK

s = sim.get_state(t_=0)
s.m = s.ms[0]
sum(s.get_central_density(TF=True)).max()

# +
# %%output size=100
# %%opts QuadMesh [height=300 width=900 colorbar=True tools=['hover'] toolbar='above']
# %%opts Image [height=300 width=900 colorbar=True tools=['hover'] toolbar='above']
# %%opts Curve [height=300 width=900] {+framewise}
# %%opts QuadMesh (cmap='viridis')
# %%opts Image (cmap='viridis')

from gpe.utils import Simulation2
from soc_catch_and_release import ExperimentCatchAndRelease1a, ExperimentCatchAndRelease

expt1a = ExperimentCatchAndRelease1a(cooling_phase=1.0, t__barrier=2)
expt = ExperimentCatchAndRelease(cooling_phase=1.0, t__barrier=0)
# expt = ExperimentCatchAndRelease(cooling_phase=1.0, t__barrier=10)

sim = Simulation2(expt)
sim.view_hv()
# -

sim.view(show_momenta=True, show_mixtures=True)

# ## Other

from gpe.utils import Simulation2
from soc_catch_and_release import ExperimentCatchAndRelease1

expt = ExperimentCatchAndRelease1()
sim = Simulation2(expt)
sim.view(show_momenta=True)

s = expt.get_state()
s.plot(show_momenta=True)

expt.get_state().plot(show_momenta=True, show_mixtures=True)

expt.Omega / expt.E_R

expt.get_state().plot()

# +
from gpe.utils import Simulation
from soc_catch_and_release import ExperimentCatchAndRelease

# expt = ExperimentCatchAndRelease()
# sim = Simulation(expt)
# sim.run()

expt = ExperimentCatchAndRelease(cooling_phase=1.0, t__barrier=10)
sim2 = Simulation(expt)
sim2.run()

# -

# %%time
s = e.get_initial_state()
s.plot()

e.y.t

from IPython.display import clear_output, display
from pytimeode.evolvers import EvolverABM
from mmfutils.contexts import NoInterrupt

s.cooling_phase = 1.0
e = EvolverABM(s, dt=0.4 * s.t_scale)
fig = None
with NoInterrupt() as interrupted:
    while not interrupted:
        e.evolve(200)
        fig = e.y.plot(fig=fig, show_momenta=True)
        display(fig)
        clear_output(wait=True)

np.zero

import numpy as np

FWHM = np.array((54.37, 9.63)) * 0.587


# $$
#   -r^2/2\sigma^2 = \ln(1/2)*2*
# $$

np.sqrt((FWHM / 2.0) ** 2 / (-2 * np.log(0.5)))

# # Imaging

# Here we demonstrate the effect of imaging:

# +
# %load_ext autoreload
# %autoreload 2
from gpe.utils import Simulation2
from soc_catch_and_release import ExperimentCatchAndRelease1a, ExperimentCatchAndRelease

expt10 = ExperimentCatchAndRelease(cooling_phase=1.0, t__barrier=10)
expt0 = ExperimentCatchAndRelease(cooling_phase=1.0 + 0.005j, t__barrier=0)

sim0 = Simulation2(expt0)
sim10 = Simulation2(expt10)
# -

# %pylab inline
import numpy as np

s = sim10.get_state(t_=10, image=True)
x = s.xyz[0]
ns = s.get_density()
x_, ns_ = s.experiment.simulate_image(s, Ny=5000)
plt.figure(figsize=(20, 5))
plt.subplot(221)
plt.plot(x, ns[0])
plt.subplot(222)
plt.plot(x, ns[1])
plt.subplot(223)
plt.plot(x_, ns_[0])
plt.subplot(224)
plt.plot(x_, ns_[1])
ns.sum(axis=-1) * (x[1] - x[0]), ns_.sum(axis=-1) * (x_[1] - x_[0])
