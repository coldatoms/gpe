"""Defines simulations to run with checkpoint/restart capabilities.

To use this, inherit from ExperimentBase, and pass any important variables to
the `local_dict` argument of the ExperimentBase.__init__() method.  These
variables will be used to generate unique filenames for each run.
"""
import itertools

import numpy as np

import mmf_setup.set_path.hgroot

from mmfutils.contexts import NoInterrupt

import mmfutils.performance.threads
import mmfutils.performance.fft

import sys
import os

from gpe import utils

import soc_catch_and_release

u = soc_catch_and_release.u

import soc_car_fast

_DATA_DIR = "_data"


def set_threads(numexpr=1, fft=1, *v):
    if v:
        numexpr = fft = v[0]
    mmfutils.performance.threads.set_num_threads(numexpr)
    mmfutils.performance.fft.set_num_threads(fft)


# Needs testing, but performance on swan did not show an appreciable benefit to
# using multiple cores.
_NUM_THREADS = int(os.environ.get("OMP_NUM_THREADS", "1"))
set_threads(_NUM_THREADS)


class RunBase:
    dt_ = 0.5

    experiment_params = dict(
        cooling_phase=[1.0, 1 + 0.005j, 1 + 0.01j],
    )

    simulation_params = {}

    @property
    def experiments(self):
        params = self.experiment_params
        keys = params.keys()
        iterators = [params[_key] for _key in keys]
        return [
            self.Experiment(**dict(zip(keys, values)))
            for values in itertools.product(*iterators)
        ]

    @property
    def simulations(self):
        params = self.simulation_params
        keys = params.keys()
        iterators = [params[_key] for _key in keys]
        return [
            utils.Simulation(
                experiment=expt,
                dt_=self.dt_,
                data_dir=_DATA_DIR,
                **dict(zip(keys, values))
            )
            for expt in self.experiments
            for values in itertools.product(*iterators)
        ]

    def run(self, sim_number=None, image_ts_=None):
        if image_ts_ is not None:
            self.simulation_params["image_ts_"] = [image_ts_]

        if sim_number is None:
            sims = self.simulations
        else:
            sims = [self.simulations[sim_number]]

        with NoInterrupt(ignore=True) as interrupted:
            for sim in sims:
                if interrupted:
                    break
                sim.run()
                if interrupted:
                    break
                sim.run_images()


class Run_IPG(RunBase):
    """
    These runs used a different transverse confining laser (IPG)
    This changed the trapping frequency
    """

    experiment_params = dict(
        cooling_phase=[1.0],
        trapping_frequencies_Hz=[(3.49, 278, 278)],
        barrier_depth_nK=[-30, -60, -90],
    )

    simulation_params = dict(
        image_ts_=[
            [
                0,
                10,
                12,
                14,
                16,
                18,
                20,
                22,
                24,
                26,
                28,
                30,
                32,
                34,
                36,
                38,
                40,
                42,
                44,
                46,
                48,
                50,
            ]
        ]
    )
    Experiment = soc_car_fast.ExperimentCARFast


class Run_IPG_small(RunBase):
    """
    These runs used a different transverse confining laser (IPG)
    This changed the trapping frequency
    """

    dt_ = 0.1
    experiment_params = dict(
        cells_x=[600],
        cooling_phase=[1.0],
        trapping_frequencies_Hz=[(3.49, 278, 278)],
        barrier_depth_nK=[-30, -60, -90],
    )

    simulation_params = dict(
        image_ts_=[
            [
                0,
                10,
                12,
                14,
                16,
                18,
                20,
                22,
                24,
                26,
                28,
                30,
                32,
                34,
                36,
                38,
                40,
                42,
                44,
                46,
                48,
                50,
            ]
        ]
    )
    Experiment = soc_car_fast.ExperimentCARFastSmall


class Run_IPG_small_tube(Run_IPG_small):
    """
    These runs used a different transverse confining laser (IPG)
    This changed the trapping frequency
    """

    dt_ = 0.1
    experiment_params = dict(
        Run_IPG_small.experiment_params,
        barrier_depth_nK=[-30],
        x_TF=[150.0 * u.micron],
        tube=[True],
        basis_type=["1D"],
    )

    simulation_params = dict(
        image_ts_=[[10]],
        t__final=[10],
    )


class Run_IPG_xTF150(RunBase):
    """
    These runs used a different transverse confining laser (IPG)
    This changed the trapping frequency
    """

    experiment_params = dict(
        cooling_phase=[1.0],
        trapping_frequencies_Hz=[(3.49, 278, 278)],
        barrier_depth_nK=[-30, -60, -90],
        x_TF=[150.0 * u.micron],
    )

    simulation_params = dict(
        image_ts_=[
            [
                0.0,
                0.5,
                1.0,
                1.5,
                2.0,
                2.5,
                3.0,
                3.5,
                4.0,
                4.5,
                5.0,
                5.5,
                6.0,
                6.5,
                7.0,
                7.5,
                8.0,
                8.5,
                9.0,
                9.5,
                10.0,
                10.5,
                11.0,
                11.5,
                12.0,
                12.5,
                13.0,
                13.5,
                14.0,
                14.5,
                15.0,
                15.5,
                16.0,
                16.5,
                17.0,
                17.5,
                18.0,
                18.5,
                19.0,
                19.5,
                20.0,
                20.5,
                21.0,
                21.5,
                22.0,
                22.5,
                23.0,
                23.5,
                24.0,
                24.5,
                25.0,
                25.5,
                26.0,
                26.5,
                27.0,
                27.5,
                28.0,
                28.5,
                29.0,
                29.5,
                30.0,
                30.5,
                31.0,
                31.5,
                32.0,
                32.5,
                33.0,
                33.5,
                34.0,
                34.5,
                35.0,
                35.5,
                36.0,
                36.5,
                37.0,
                37.5,
                38.0,
                38.5,
                39.0,
                39.5,
                40.0,
                40.5,
                41.0,
                41.5,
                42.0,
                42.5,
                43.0,
                43.5,
                44.0,
                44.5,
                45.0,
                45.5,
                46.0,
                46.5,
                47.0,
                47.5,
                48.0,
                48.5,
                49.0,
                49.5,
                50.0,
            ]
        ]
    )
    Experiment = soc_car_fast.ExperimentCARFast


class Run_IPG_small_xTF150(RunBase):
    """
    These runs used a different transverse confining laser (IPG)
    This changed the trapping frequency
    """

    dt_ = 0.1
    experiment_params = dict(
        cells_x=[600],
        cooling_phase=[1.0],
        trapping_frequencies_Hz=[(3.49, 278, 278)],
        barrier_depth_nK=[-30, -60, -90],
        x_TF=[150.0 * u.micron],
    )

    simulation_params = dict(
        image_ts_=[
            [
                0,
                10,
                12,
                14,
                16,
                18,
                20,
                22,
                24,
                26,
                28,
                30,
                32,
                34,
                36,
                38,
                40,
                42,
                44,
                46,
                48,
                50,
            ]
        ]
    )
    Experiment = soc_car_fast.ExperimentCARFastSmall


class Run_IPG_xTF150_SB(Run_IPG_xTF150):
    """
    These runs used a different transverse confining laser (IPG)
    This changed the trapping frequency
    """

    experiment_params = dict(
        cooling_phase=[1.0],
        trapping_frequencies_Hz=[(3.49, 278, 278)],
        barrier_depth_nK=[-30, -60, -90],
        x_TF=[150.0 * u.micron],
        single_band=[True],
    )


class Run_IPG_small_xTF150_SB(Run_IPG_small_xTF150):
    """
    These runs used a different transverse confining laser (IPG)
    This changed the trapping frequency
    """

    dt_ = 0.1
    experiment_params = dict(
        cells_x=[600],
        cooling_phase=[1.0],
        trapping_frequencies_Hz=[(3.49, 278, 278)],
        barrier_depth_nK=[-30, -60, -90],
        x_TF=[150.0 * u.micron],
        single_band=[True],
    )


class Run_xTF(RunBase):
    """
    These runs used a different transverse confining laser (IPG)
    This changed the trapping frequency
    """

    experiment_params = dict(
        cooling_phase=[1.0],
        trapping_frequencies_Hz=[(3.49, 278, 278)],
        barrier_depth_nK=[-30],
        t__wait=[0],
        x_TF=[
            155.0 * u.micron,
            175.0 * u.micron,
            195.0 * u.micron,
            215.0 * u.micron,
            235.0 * u.micron,
        ],
    )

    simulation_params = dict(image_ts_=[[10]])
    Experiment = soc_car_fast.ExperimentCARFast


class Run_IPG_NoSOC(RunBase):
    """
    These runs used a different transverse confining laser (IPG)
    This changed the trapping frequency
    """

    experiment_params = dict(
        dx=[None],
        cooling_phase=[1.0],
        trapping_frequencies_Hz=[(3.49, 278, 278)],
        barrier_depth_nK=[-30, -60, -90],
        x_TF=[150.0 * u.micron, 160.0 * u.micron, 170.0 * u.micron],
        detuning_kHz=[0],
        rabi_frequency_E_R=[0],
    )

    simulation_params = dict(
        image_ts_=[
            [
                0,
                10,
                12,
                14,
                16,
                18,
                20,
                22,
                24,
                26,
                28,
                30,
                32,
                34,
                36,
                38,
                40,
                42,
                44,
                46,
                48,
                50,
            ]
        ]
    )
    Experiment = soc_car_fast.ExperimentCARFast


class Run_IPG_NoSOC_small(RunBase):
    """
    These runs used a different transverse confining laser (IPG)
    This changed the trapping frequency
    """

    dt_ = 0.1
    experiment_params = dict(
        cells_x=[600],
        cooling_phase=[1.0],
        trapping_frequencies_Hz=[(3.49, 278, 278)],
        barrier_depth_nK=[-30, -60, -90],
        x_TF=[150.0 * u.micron, 160.0 * u.micron, 170.0 * u.micron],
        detuning_kHz=[0],
        rabi_frequency_E_R=[0],
    )

    simulation_params = dict(
        image_ts_=[
            [
                0,
                10,
                12,
                14,
                16,
                18,
                20,
                22,
                24,
                26,
                28,
                30,
                32,
                34,
                36,
                38,
                40,
                42,
                44,
                46,
                48,
                50,
            ]
        ]
    )
    Experiment = soc_car_fast.ExperimentCARFastSmall


class Run_IPG_t_barrier_small(RunBase):
    """
    These runs used a different transverse confining laser (IPG)
    This changed the trapping frequency
    """

    dt_ = 0.1
    experiment_params = dict(
        cells_x=[600],
        cooling_phase=[1.0],
        trapping_frequencies_Hz=[(3.49, 278, 278)],
        barrier_depth_nK=[-60],
        x_TF=[150 * u.micron],
        barrier_width_micron=[2, 8, 15, 30],
        t__barrier=[15, 20, 25, 30],
    )

    Experiment = soc_car_fast.ExperimentCARFastSmall


class Run_IPG_small_3D_big(RunBase):
    """
    Small 3D sims with a tilted barrier
    The alts have smaller dx but less Lx
    """

    dt_ = 0.1
    experiment_params = dict(
        basis_type=["3D"],
        dx=[0.1 * u.micron],
        cells_x=[300],
        cooling_phase=[1.0],
        trapping_frequencies_Hz=[(3.49, 278, 278)],
        barrier_depth_nK=[-30, -60, -90],
        y_tilt=[0, 0.01],
        x_TF=[150.0 * u.micron],
    )
    simulation_params = dict(
        image_ts_=[
            [
                18,
            ]
        ],
        t__final=[20],
    )
    Experiment = soc_car_fast.ExperimentCARFast3DSmall


class Run_IPG_tube(RunBase):
    """
    These runs used a different transverse confining laser (IPG)
    This changed the trapping frequency
    """

    dt_ = 0.1
    experiment_params = dict(
        dx=[None],
        cooling_phase=[1.0],
        trapping_frequencies_Hz=[(3.49, 278, 278)],
        barrier_depth_nK=[-30, -60, -90],
        x_TF=[150.0 * u.micron, 153.23745 * u.micron],
        tube=[True],
        basis_type=["1D"],
    )

    simulation_params = dict(
        image_ts_=[
            [
                0.0,
                0.5,
                1.0,
                1.5,
                2.0,
                2.5,
                3.0,
                3.5,
                4.0,
                4.5,
                5.0,
                5.5,
                6.0,
                6.5,
                7.0,
                7.5,
                8.0,
                8.5,
                9.0,
                9.5,
                10.0,
                10.5,
                11.0,
                11.5,
                12.0,
                12.5,
                13.0,
                13.5,
                14.0,
                14.5,
                15.0,
                15.5,
                16.0,
                16.5,
                17.0,
                17.5,
                18.0,
                18.5,
                19.0,
                19.5,
                20.0,
                20.5,
                21.0,
                21.5,
                22.0,
                22.5,
                23.0,
                23.5,
                24.0,
                24.5,
                25.0,
                25.5,
                26.0,
                26.5,
                27.0,
                27.5,
                28.0,
                28.5,
                29.0,
                29.5,
                30.0,
                30.5,
                31.0,
                31.5,
                32.0,
                32.5,
                33.0,
                33.5,
                34.0,
                34.5,
                35.0,
                35.5,
                36.0,
                36.5,
                37.0,
                37.5,
                38.0,
                38.5,
                39.0,
                39.5,
                40.0,
                40.5,
                41.0,
                41.5,
                42.0,
                42.5,
                43.0,
                43.5,
                44.0,
                44.5,
                45.0,
                45.5,
                46.0,
                46.5,
                47.0,
                47.5,
                48.0,
                48.5,
                49.0,
                49.5,
                50.0,
            ]
        ]
    )

    Experiment = soc_car_fast.ExperimentCARFast


class Run_IPG_xTF150_RNR(RunBase):
    """
    These runs used a different transverse confining laser (IPG)
    This changed the trapping frequency
    """

    dt_ = 0.1
    experiment_params = dict(
        cooling_phase=[1.0],
        R=[7.68 * u.micron],
        Nr=[2**7],
        trapping_frequencies_Hz=[(3.49, 278, 278)],
        barrier_depth_nK=[-30, -60, -90],
        x_TF=[150.0 * u.micron],
    )

    simulation_params = dict(
        image_ts_=[
            [
                0.0,
                0.5,
                1.0,
                1.5,
                2.0,
                2.5,
                3.0,
                3.5,
                4.0,
                4.5,
                5.0,
                5.5,
                6.0,
                6.5,
                7.0,
                7.5,
                8.0,
                8.5,
                9.0,
                9.5,
                10.0,
                10.5,
                11.0,
                11.5,
                12.0,
                12.5,
                13.0,
                13.5,
                14.0,
                14.5,
                15.0,
                15.5,
                16.0,
                16.5,
                17.0,
                17.5,
                18.0,
                18.5,
                19.0,
                19.5,
                20.0,
                20.5,
                21.0,
                21.5,
                22.0,
                22.5,
                23.0,
                23.5,
                24.0,
                24.5,
                25.0,
                25.5,
                26.0,
                26.5,
                27.0,
                27.5,
                28.0,
                28.5,
                29.0,
                29.5,
                30.0,
                30.5,
                31.0,
                31.5,
                32.0,
                32.5,
                33.0,
                33.5,
                34.0,
                34.5,
                35.0,
                35.5,
                36.0,
                36.5,
                37.0,
                37.5,
                38.0,
                38.5,
                39.0,
                39.5,
                40.0,
                40.5,
                41.0,
                41.5,
                42.0,
                42.5,
                43.0,
                43.5,
                44.0,
                44.5,
                45.0,
                45.5,
                46.0,
                46.5,
                47.0,
                47.5,
                48.0,
                48.5,
                49.0,
                49.5,
                50.0,
            ]
        ]
    )
    Experiment = soc_car_fast.ExperimentCARFast


if __name__ == "__main__":
    # runs_car_fast_final Run0
    # runs_car_fast_final Run0 5
    # runs_car_fast_final Run0 5 "10,12"
    cmd = sys.argv[1]
    sim_number = image_ts_ = None
    if len(sys.argv) > 2:
        sim_number = int(sys.argv[2])
    if len(sys.argv) > 3:
        image_ts_ = list(map(float, sys.argv[3].split(",")))
    if cmd in locals():
        locals()[cmd]().run(sim_number=sim_number, image_ts_=image_ts_)
    else:
        raise ValueError("Command {} not found.".format(cmd))
