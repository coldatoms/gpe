---
execution:
  timeout: 30

jupytext:
  formats: md:myst,ipynb
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.16.7
kernelspec:
  display_name: Python 3 (ipykernel)
  language: python
  name: python3
---

```{code-cell} ipython3
:init_cell: true

import numpy as np, matplotlib.pyplot as plt
import mmf_setup

mmf_setup.nbinit()
```

# Solitons in Spin-Orbit Coupled BECs

Here we describe simulations that model Peter Engels's experiments.  They study a
tightly confined two-component BEC with the $\ket{1,-1}$ and $\ket{1,0}$ states of
$^{87}$Rb in the presence of counter-flow and spin-orbit coupling (SOC).  The
Hamiltonian for the system is

The Hamiltonian is:

$$
  \I\hbar\partial_t
  \begin{pmatrix}
    \psi_a\\
    \psi_b
  \end{pmatrix}
  =
  \begin{pmatrix}
    \frac{-\hbar^2 \nabla^2}{2m} + V_a - \mu - \frac{\delta}{2} + g_{aa}n_a + g_{ab}n_b & \frac{\Omega}{2}e^{2\I k_r x} \\
    \frac{\Omega}{2}e^{-2\I k_r x} & \frac{-\hbar^2 \nabla^2}{2m} + V_b - \mu + \frac{\delta}{2} + g_{ab}n_a + g_{bb}n_b
  \end{pmatrix}
  \begin{pmatrix}
    \psi_a\\
    \psi_b
  \end{pmatrix}.
$$

The external potentials $V_{a,b}(\vect{x})$ contain a common harmonic confinement potential plus a counter-flow term induced by a magnetic field with couples differently between the two states:

$$
  V_{ab}(\vect{x}) = \sum_i \frac{m\omega_i^2x_i^2}{2} + B(x)\mu_B^{a,b}
$$

where the magnetic field is roughly linear along the $x$ direction with a magnetic field gradient of $B'(x) = 10$mG/cm and $\mu_B^{a,b}$ are the magnetic moments.

The effect of this linear potential is to shift the harmonic traps for the

$$
  \frac{m\omega^2}{2}x^2 - m\omega^2 x_0 x
  = \frac{m\omega^2}{2}(x-x_0)^2 - \frac{m\omega^2}{2}x_0^2.
$$

+++

## Rotating Phases

+++

One potential simplification for the two-component system, we perform a phase rotation:

$$
  \begin{pmatrix}
    \tilde{\psi}_a\\
    \tilde{\psi}_b
  \end{pmatrix}
  =
  \begin{pmatrix}
    e^{-\I \vect{k}_r x}\psi_a\\
    e^{\I \vect{k}_r x}\psi_b
  \end{pmatrix}
$$

so that

$$
  \I\hbar\partial_t
  \begin{pmatrix}
    \tilde{\psi}_a\\
    \tilde{\psi}_b
  \end{pmatrix}
  =
  \begin{pmatrix}
    \frac{\hbar^2 (-\I\vect{\nabla} + \vect{k}_r)^2}{2m} + V_a - \mu - \frac{\delta}{2} + g_{aa}n_a + g_{ab}n_b & \frac{\Omega}{2} \\
    \frac{\Omega}{2} & \frac{\hbar^2 (-\I\vect{\nabla} - \vect{k}_r)^2}{2m} + V_b - \mu + \frac{\delta}{2} + g_{ab}n_a + g_{bb}n_b
  \end{pmatrix}
  \begin{pmatrix}
    \tilde{\psi}_a\\
    \tilde{\psi}_b
  \end{pmatrix}.
$$

so that

$$
  \I\hbar\partial_t
  \begin{pmatrix}
    \tilde{\psi}_a\\
    \tilde{\psi}_b
  \end{pmatrix}
  =
  \begin{pmatrix}
    \frac{\hbar^2 (-\I\vect{\nabla} + \vect{k}_r)^2}{2m} -\mu_{a} & \frac{\Omega}{2} \\
    \frac{\Omega}{2} & \frac{\hbar^2 (-\I\vect{\nabla} - \vect{k}_r)^2}{2m} - \mu_{b}
  \end{pmatrix}
  \begin{pmatrix}
    \tilde{\psi}_a\\
    \tilde{\psi}_b
  \end{pmatrix}.
$$

+++

To avoid any complications (these fictitious phases must be carefully applied to initial states and currents for example) we do not use this transform in the actual code, however, it is very useful for analysis.  In particular, for homogeneous states, we can diagonalize this to obtain two branches with dispersions:

$$
  \epsilon_{+}(\vect{k}) = \frac{\hbar^2(k^2 + k_r^2)}{2m} - \mu_+, \qquad
  \epsilon_{-}(\vect{k}) = \frac{\hbar^2\vect{k}\cdot\vect{k}_r}{m} - \mu_{-}, \qquad
  D(\vect{k}) = \sqrt{\epsilon_{-}^2(\vect{k}) +\frac{\Omega^2}{4}}, \qquad
  \mu_{\pm} = \frac{\mu_a \pm \mu_b}{2},
  \\
  \mu_{+} = \mu - \frac{V_a + V_b + g_{aa}n_a + g_{bb}n_b + g_{ab}(n_a+n_b)}{2}, \qquad
  \mu_{-} = \frac{\delta}{2} - \frac{V_a - V_b + g_{aa}n_a - g_{bb}n_b + g_{ab}(n_a-n_b)}{2},
  \\
  E_{\pm}(\vect{k}) = \epsilon_{+}(\vect{k}) \pm D(\vect{k}), \qquad
  \frac{u_\pm}{v_{\pm}} = \frac{-C}{\epsilon_{-}(\vect{k}) \mp D(\vect{k})}
  = \frac{\epsilon_{-}(\vect{k}) \pm D(\vect{k})}{C}, \qquad
  \frac{\abs{u_\pm}}{\abs{v_{\pm}}} = \sqrt{\frac{1\pm K}{1\mp K}}, \qquad
  K = \frac{\epsilon_{-}(\vect{k})}{D(\vect{k})}
  = \pm\frac{\frac{\abs{u_\pm}^2}{\abs{v_{\pm}}^2}-1}{\frac{\abs{u_\pm}^2}{\abs{v_{\pm}}^2}+1}
$$

+++

$$
  u_{\pm} = \frac{-1}
  {\sqrt{2}\sqrt{1 + \frac{\epsilon_{-}(\vect{x})}{\Omega/2}
              \frac{\epsilon_{-}(\vect{x}) \mp D(\vect{x})}{\Omega/2}}},\qquad
  v_{\pm} = \frac{\frac{\epsilon_{-}(\vect{x}) \mp D(\vect{x})}{\Omega/2}}
  {\sqrt{2}\sqrt{1 + \frac{\epsilon_{-}(\vect{x})}{\Omega/2}
                     \frac{\epsilon_{-}(\vect{x}) \mp D(\vect{x})}{\Omega/2}}},
$$
$$
  u_{\pm} = \frac{-C}
  {\sqrt{2C^2 + 2\epsilon_{-}(\vect{x})[\epsilon_{-}(\vect{x}) \mp D(\vect{x})]}},\qquad
  v = \frac{\epsilon_{-}(\vect{x}) \mp D(\vect{x})}
  {\sqrt{2C^2 + 2\epsilon_{-}(\vect{x})[\epsilon_{-}(\vect{x}) \mp D(\vect{x})]}},
$$
$$
  u_{\pm} = \frac{-\Omega/2}
  {\sqrt{2D(\vect{k})}\sqrt{D(\vect{k}) \mp \epsilon_{-}(\vect{k})}},\qquad
  v_{\pm} = \mp\frac{\sqrt{D(\vect{k})\mp\epsilon_{-}(\vect{x})}}
  {\sqrt{2D(\vect{k})}}.
$$

+++

The population of the upper/lower branches can be computed as:

$$
  n_{\pm} = \frac{1\pm K}{2}n_a + \frac{1\mp K}{2}n_b.
$$

```{code-cell} ipython3
# Quick check of formula:
np.random.seed(3)
A, B, C = np.random.random(3)
H = np.array([[A, C], [C, B]])
ABs = [(A + B) / 2, (A - B) / 2]
D = np.sqrt(ABs[1] ** 2 + C ** 2)
Es = [ABs[0] - D, ABs[0] + D]
K = ABs[1] / D
E, V = np.linalg.eigh(H)
assert np.allclose(Es, E)
um, vm = V[:, 0]
up, vp = V[:, 1]
assert np.allclose([-C / (ABs[1] - D) * vp, -C / (ABs[1] + D) * vm], [up, um])
assert np.allclose(
    np.abs([up / vp, um / vm]), np.sqrt([(1 + K) / (1 - K), (1 - K) / (1 + K)])
)

assert np.allclose(
    [
        (abs(up / vp) ** 2 - 1) / (1 + abs(up / vp) ** 2),
        -(abs(um / vm) ** 2 - 1) / (1 + abs(um / vm) ** 2),
    ],
    K,
)
psi_pm = np.random.random(2)
n_p, n_m = abs(psi_pm) ** 2
psi_ab = psi_pm[0] * np.array([up, vp]) + psi_pm[1] * np.array([um, vm])
n_a, n_b = abs(psi_ab) ** 2

assert np.allclose(n_a + n_b, n_m + n_p)
(
    ((1 + K) * n_a + (1 - K) * n_b - 2 * np.sqrt((1 - K ** 2) * n_a * n_b)) / 2,
    n_p,
    ((1 - K) * n_a + (1 + K) * n_b + 2 * np.sqrt((1 - K ** 2) * n_a * n_b)) / 2,
    n_m,
)
```

Note that although this phase rotation does not affect the densities, it will affect currents, and needs to be carefully applied to, for example, initial states.  In the code, we add a property `rotating_phases` which can be set or unset, applying the appropriate transformation to the underlying state and including or removing the $\vect{k}_r$ terms from the kinetic energy.

+++

# Scales

+++

Some typical scales for these experiments are as follows:

\begin{align}
  \newcommand\micron{\mu\mathrm{m}}
  R_{TF} &= 230\micron \tag{Cloud radius.}\\
  a_{HO} &= 40\micron = \sqrt{\hbar/m\omega} \tag{Trap length in $x$ direction ($\omega = 2\pi \cdot 3$Hz).}\\
  n_{0}^{-1/3} &= 0.11\micron \tag{Interparticle separation $\mu = m\omega^2R_{TF}^2/2$, $n_0 = \mu/g$.}\\
  \xi_h &= 0.17\micron \tag{$\hbar^2/(2m\xi_h^2) = \mu$.}\\
  k_r^{-1} &= 0.13\micron
\end{align}

+++

To resolve these features, we need a lattice spacing of at least $\d{x} \lesssim 0.1\mu$m, which means that a box of $1000\micron$ will need at least $N=2^{13}$ lattice points for qualitative behaviour.  Simulations suggest that $N=2^{14}$ is enough for reproducble behaviour over time-scales of .

```{code-cell} ipython3
from gpe.soc import u
R = 230 * u.micron
w = 2 * np.pi * 3 * u.Hz
mu = u.m * w ** 2 * R ** 2 / 2.0
n_0 = mu / state.gs[0]
print(1.0 / n_0 ** (1.0 / 3))
xi = np.sqrt(u.hbar ** 2 / 2 / u.m / mu)
xi
1.0 / state.k_r / u.micron
```

Here we start with a low-res simulation of the Rabi flopping in the SOC system.

+++

# Simulations

+++

## Initial State

+++

Most of the experiments start with an initial cloud prepared in the $\ket{1, -1}$ state, optionally with a transfer via RF $\pi$-pulse to a 50/50 mixture of both $\ket{1, -1}$ and $\ket{1, 0}$.  The first initial state should be well approximated by the TF approximation:

$$
  \psi_{a} \approx \sqrt{\mu - V(x)}, \qquad \psi_{b} = 0\\
  \tilde{\psi}_{a} \approx e^{-\I k_r x}\sqrt{\mu - V(x)}.
$$

```{code-cell} ipython3
%matplotlib inline
from importlib import reload
from IPython.display import display, clear_output
import soc_experiments
```

```{code-cell} ipython3
reload(soc_experiments)
from soc_experiments import State2, u, Experiment
```

```{code-cell} ipython3
expt = Experiment(
    Nxyz=(2 ** 12 / 2,),
    B_gradient_mG_cm=10,
    Lxyz=(1000 * u.micron,),
    cooling_phase=1 + 0.000j,
)
s0 = expt.get_state()
x = s0.xyz[0].ravel()
Va, Vb, Vab = s0.get_Vext()
plt.plot(x / u.micron, Va, label=expt.states[0])
plt.plot(x / u.micron, Vb, label=expt.states[1])
plt.xlim(-40, 40)
plt.ylim(-0.02, 0.04)
plt.legend()
```

```{code-cell} ipython3
x = s0.xyz[0]
s0.plot()
plt.twinx()
plt.plot(x, np.angle(s0[0]))
```

```{code-cell} ipython3
state = s0
state.t = -40 * u.ms
s_a = state.copy()
s_b = state.copy()
s_a.rotating_phases = False
s_b.rotating_phases = True
# state.rotating_phase = True
# state.rotating_phases = True

ss = [s_a, s_b]
for _s in ss:
    _s.t = -40 * u.ms
es = [EvolverABM(_s, dt=0.001 * _s.t_scale) for _s in ss]

plt.figure(figsize=(20, 5))
with NoInterrupt(ignore=True) as interrupted:
    while es[0].t < 0 and not interrupted:
        [_e.evolve(2000) for _e in es]
        plt.clf()
        [_e.y.plot() for _e in es]
        plt.twinx()
        e = es[0]
        Va, Vb, Vab = e.y.get_Vext()
        assert np.allclose(Vab, 0)
        plt.plot(e.y.xyz[0], Va)
        plt.plot(e.y.xyz[0], Vb)
        display(plt.gcf())
        clear_output(wait=True)
```

## Rabi Flopping

+++

Here we simulate a with a magnetic field gradient along the trap.  We start with only one species occupied (we do this by setting  a very large detuning $\delta$ and then minimizing), and then evolve with $\delta = 0$ which leads to rapid Rabi flopping from one state to the other forming a series of spin waves.

```{code-cell} ipython3
%pylab inline --no-import-all
from IPython.display import display, clear_output
from soc_experiments import State2, u, Experiment
```

```{code-cell} ipython3
e = Experiment(Nxyz=(2 ** 12 / 8,), Lxyz=(1000 * u.micron,), cooling_phase=1 + 0.000j)
s0 = e.get_state()
# s0[1,...] = 0.0
Va, Vb, Vab = s0.get_Vext()
x = s0.xyz[0]
plt.plot(x / u.micron, Va)
plt.plot(x / u.micron, Vb)
plt.xlim(-50, 50)
plt.ylim(-0.02, 0.03)
```

```{code-cell} ipython3
print ("detuning = {}Hz".format(e.detuning / u.Hz))
print ("Rabi Frequency = {}kHz".format(e.rabi_frequency / u.kHz))
```

Get particle number and initial state.

+++

### Experiment 1cf
* Start with 100% in (1, -1), get ground state, then transfer 50% to (1,0).
* No SOC or Raman couplings.
* Apply gradient (10ms ramp) and hold from 0 to 400ms, then jump off (instantaneous) and wait 200ms.
* Image after expansion for 7ms with everything off.

```{code-cell} ipython3
%pylab inline --no-import-all
import mmf_setup.set_path.hgroot
import soc_experiments
```

```{code-cell} ipython3
reload(soc_experiments)
fig = soc_experiments.Experiment1cf().get_state().plot()
```

### Experiment 2cf
* Start with 100% in (1, -1), get ground state, then transfer 50% to (1,0).
* No SOC or Raman couplings.
* Apply gradient (10ms ramp) and hold for 100ms, then jump off (instantaneous) and wait from 0 to 400ms.
* Image after expansion for 7ms with everything off.

```{code-cell} ipython3
%pylab inline --no-import-all
import mmf_setup.set_path.hgroot
import soc_experiments
```

```{code-cell} ipython3
reload(soc_experiments)
fig = soc_experiments.Experiment1cf().get_state().plot()
```

### Experiment 3cf
* Start with 100% in (1, -1), get ground state, then transfer 50% to (1,0), but in the presence of the gradient.
* Wait for 1s.
* Ramp on Raman coupling (fixed detuning) over 10ms, 100ms, 200ms.  Final amplitude 1kHz to 10.7kHz (initial calculations at 5kHz).
* Watch over 100ms to 200ms.
* Image after expansion for 7ms with everything off.

```{code-cell} ipython3
%pylab inline --no-import-all
import mmf_setup.set_path.hgroot
import soc_experiments
```

```{code-cell} ipython3
reload(soc_experiments)
fig = soc_experiments.Experiment3cf().get_state().plot()
```

```{code-cell} ipython3
soc_experiments.Experiment3cf().Omega_t_(1050.0)
```

### Experiment 1
* No counterflow (gradient)
* Roughly zero detuning.
* Start with 100% in (1, -1), get ground state, then transfer 50% to (1,0).
* Ramp on SOC/Raman couplings over 100ms (10ms should work too).
  * `rabi_frequency_E_R=1.9`
* Wait for up to 200ms
* Image after expansion for 7ms with everything off.

Observations:
  * After ramp there are lots of excitations - are they just spin waves, or are they solitons?
  * Lots of excitations and bunny-ear solitons

```{code-cell} ipython3
%pylab inline --no-import-all
import mmf_setup.set_path.hgroot
import soc_experiments
```

```{code-cell} ipython3
reload(soc_experiments)
fig = soc_experiments.Experiment1().get_state().plot()
```

```{code-cell} ipython3
import runs
```

```{code-cell} ipython3
r = runs.Run1()
r.simulations[0].view()
```

```{code-cell} ipython3
s = sim.get_state(0)
```

```{code-cell} ipython3
from IPython.display import clear_output, display
from pytimeode.evolvers import EvolverABM
from mmfutils.contexts import NoInterrupt
```

```{code-cell} ipython3
s = sim.get_state(0)
s.cooling_phase = 1j
# e = EvolverABM(s, dt=0.1*s.t_scale)
# e.evolve(100)
# s1 = e.get_y()
s1 = sim.get_state(0)
```

```{code-cell} ipython3
# s1.cooling_phase = 1.0
e = EvolverABM(s1, dt=0.5 * s.t_scale)
fig = None
with NoInterrupt(ignore=True) as i:
    while not i:
        e.evolve(100)
        fig = e.y.plot(fig=fig)
        display(fig)
        clear_output(wait=True)
```

```{code-cell} ipython3
%pylab inline --no-import-all
import runs
```

```{code-cell} ipython3
r = runs.Run1()
s = r.simulations[1]
```

```{code-cell} ipython3
s = expt.get_state()
Va, Vb, Vab = s.get_V()
plt.plot(s.get_sigma2s()[0])
# plt.plot(Va)
```

```{code-cell} ipython3
import soc_experiments
```

```{code-cell} ipython3
reload(gpe.tube2)
reload(soc_experiments)
from gpe.minimize import MinimizeState
```

```{code-cell} ipython3
expt = soc_experiments.Experiment1(tube=True)
state = expt.get_state()
state.gs = (2.1, 3.2, 4.3)
MinimizeState(state).check()
s = state.copy()
sh = s.shape
np.random.seed(1)
s[...] = 1000 * (np.random.random(sh) + np.random.random(sh) * 1j - 0.5 - 0.5j)
MinimizeState(s).check()
```

```{code-cell} ipython3
%pylab inline --no-import-all
import mmf_setup.set_path.hgroot
import soc_experiments
```

```{code-cell} ipython3
reload(soc_experiments)
from gpe.minimize import MinimizeState
```

```{code-cell} ipython3
expt = soc_experiments.Experiment1(tube=True)
state = expt.get_state()
m = MinimizeState(state)
print m.check()
state = m.minimize()
```

```{code-cell} ipython3
Na, Nb = state.get_Ns()
state[...] = 1.0
Na_, Nb_ = state.get_Ns()
state[0, ...] *= np.sqrt(Na / Na_)
state[1, ...] = 0
# sh = state[...].shape
# state[...] += np.random.random(sh) + np.random.random(sh)*1j
# state[1, ...] = 0.0
m = MinimizeState(state)
m.check()
m.minimize().plot()
```

### Experiment 2
* No counterflow (gradient)
* Detuning = 1kHz (1, -1) lower or 0.25E_R
* Start with 100% in (1, -1), get ground state, then transfer 50% to (1,0).
* Ramp on SOC/Raman couplings over 100ms (10ms should work too).
  * `rabi_frequency_E_R=1.9`
* Wait for up to 200ms
* Image after expansion for 7ms with everything off.

Observations:
  * Lower well populated on a 100ms timescale.
  * Upper well becomes thermal as particle numbers go do.
  * Little heating of lower state.
  * Asymmetric heating (more heating when (1, -1) is lower, but this probably due to a residual magnetic field curvature.)
  * Still see some rabbit-ear solitons.

```{code-cell} ipython3
%pylab inline --no-import-all
import mmf_setup.set_path.hgroot
import soc_experiments
```

```{code-cell} ipython3
reload(soc_experiments)
fig = soc_experiments.Experiment2().get_state().plot()
```

### Experiment 3
* No counterflow (gradient)
* Detuning = 1kHz (1, -1) lower or 0.25E_R
* Start with 100% in (1, -1), get ground state.  No transfer (stay in (1, -1)).
* Ramp on SOC/Raman couplings over 100ms (10ms should work too).
  * `rabi_frequency_E_R=1.9`
* Ramp detuning from 1kHz to -1kHz in 100ms.
* Wait for up to 500ms
* Image after expansion for 7ms with everything off.

Observations:
  * (1, -1) state remains quite stable, even though we pass through 0 detuning.
  * (1, 0) components are very heated.

```{code-cell} ipython3
%pylab inline --no-import-all
import mmf_setup.set_path.hgroot
import soc_experiments
```

```{code-cell} ipython3
reload(soc_experiments)
fig = soc_experiments.Experiment3().get_state().plot()
```

### Experiment 4
* No counterflow (gradient)
* Detuning = 1kHz (1, -1) lower or 0.25E_R
* Start with 100% in (1, -1), get ground state.  No transfer (stay in (1, -1)).
* Ramp on SOC/Raman couplings over 100ms (10ms should work too).
  * `rabi_frequency_E_R=1.9`
* Ramp detuning from 1kHz to -4kHz in 50ms.
* Wait for up to 500ms
* Image after expansion for 7ms with everything off.

Observations:
  * Now see transition from (1, -1) to (1,0), then back to upper state, then finally lower state.
  * Faster ramp causes some "sloshing".
  * In the final state, they see bunny-ears even though there is no.

```{code-cell} ipython3
%pylab inline --no-import-all
import mmf_setup.set_path.hgroot
import soc_experiments
```

```{code-cell} ipython3
reload(soc_experiments)
fig = soc_experiments.Experiment4().get_state().plot()
```

### Experiment 5

* Detuning = 2kHz (1, -1) lower or 0.5E_R
* Start with 100% in (1, -1), get ground state.  No transfer (stay in (1, -1)).
* Ramp on SOC/Raman couplings over 100ms (10ms should work too).
  * `rabi_frequency_E_R=1.9`
* Jump on counterflow gradient field (1ms) to various levels (see graph)
* Wait for up to 9ms
* Jump off gradient (1ms) (irrelevant?)
* No wait yet...
* Image after expansion for 7ms with everything off.

Observations:
  * Negative gradient pushes left.  See nothing interesting (sloshing).
  * Positive gradient pushes right.
  * Nothing until 0.69V when suddenly there is an instability.
  * After 0.8V everything goes to (1,0)
  * Look at how dispersion changes as you jump on gradient.  0.9V might get close to 4E_R detuning.  (Check again.)

```{code-cell} ipython3
%pylab inline --no-import-all
import mmf_setup.set_path.hgroot
import soc_experiments
```

```{code-cell} ipython3
reload(soc_experiments)
fig = soc_experiments.Experiment5().get_state().plot()
```

```{code-cell} ipython3
import mmfutils.math
```

```{code-cell} ipython3
mmfutils.math
```

Observations:
* with SOC, clouds stay overlapped whereas they separate with counterflow.
* Unclear whether we have bright solitons or dark solitions.

* Look for small state starting at modulational instability.  Expansion should self trap, but too small for modulational instability.  Is there some stable bright soliton or a gap Soliton?  A Gap soliton would require two branches.

* At zero detuning, there are very bright states that are well localized.

* Ramp delta to simulate bucket.

* The fact that most solitons emerge at $\delta=0$ could be an indication of the supersolid phase.  Perhaps something could be said by varying the radial trap frequencies?


* Starting with no SOC and almost zero detuning, ramping on SOC, one finds all particles go into one minimum or the other.  Apparently repulsive interactions tend to cause clumping in momentum space.  Question: can one construct a low-energy state which occupies both minima (striped phase)?

* Bunny ears solitons.

+++

From Peter:

Just a short update:
Maren and I did some experiments where we used RF transfer between $\ket{1,-1}$ and $\ket{1,0}$ to generate a 50/50 mixture, followed by a 1 sec wait time. This is similar to the experiment described in our last email, just that we replaced the Raman lasers by an RF transfer.

After the RF transfer, we also observed the fringes in the BEC. We know where they come from in this case: There is a small spurious magnetic gradient along the long BEC axis that induces counterflow. The counterflow triggers a modulational instability. This is a nice tool to generate dark bright solitons.

Based on this, we think that also with the Raman transfer we had the counterflow induced instability. We figured out how to compensate the gradient and suppress the counterflow if needed, but we think it might be interesting to study counterflow in the presence of SOC, in particular since it automatically should lead to solitons in a spin-orbit couples system. Does that sound interesting?

Maren and I measured the trap frequencies today for the SOC counterflow geometry. We have axially 3 Hz, vertically 273 Hz, horizontally 277 Hz (all times 2 pi if you want omegas).
Note that here the Raman beams are counterpropagating (unlike the other machine where they are intersecting under a 90 deg angle).
Atom numbers are around 0.8 * 10^6 total (both states together) or so, give or take (this statement is not as precise as the trap frequencies).

Small spurious field pulls |1,-1> gives $10$mG/cm gives an offset of about 10micron and a change in detuning.

Check if 10micron shift of equal components gives completely immiscible fluid.

$m_j = 1/2$  700kHZ/G shift in potential energy.

+++

# Experiment 1cf

+++

* Start with 100% in (1, -1), get ground state, then transfer 50% to (1,0).
* No SOC or Raman couplings.
* Apply gradient (10ms ramp) and hold from 0 to 400ms, then jump off (instantaneous) and wait 200ms.
* Image after expansion for 7ms with everything off.

```{code-cell} ipython3
%pylab inline --no-import-all
import mmf_setup.set_path.hgroot
from soc_experiments import Experiment1cf

expt = Experiment1cf()
state = expt.get_state()
fig = state.plot()
```

```{code-cell} ipython3
%pylab inline --no-import-all
from IPython.display import display, clear_output
import bec2

reload(bec2)
from soc_experiments import u, Experiment1cf
import minimize

reload(minimize)

expt = Experiment1cf()
state = expt.get_state(
    Nxyz=(2 ** 12,),
    Lxyz=(1000 * u.micron,),
    delta=0.0,
    cooling_phase=1.0,
    constraint="Nab",
)
state[1, :] = 0
state.init()
state.plot()
m = minimize.MinimizeState(state, fix_N=True)
%time state0 = m.minimize(psi_tol=1e-12, disp=1, tries=10)
state0.plot()
plt.figure()

state[0, ...] = state[1, ...] = state0[0] / np.sqrt(2.0)
state.plot()
```

```{code-cell} ipython3
from IPython.display import clear_output, display
from pytimeode.evolvers import EvolverABM
from mmfutils.contexts import NoInterrupt
```

```{code-cell} ipython3
state.t = -20 * u.ms
e = EvolverABM(state, dt=0.1 * state.t_scale)
plt.figure(figsize=(20, 5))
with NoInterrupt(ignore=True) as interrupted:
    while e.t < expt.t_final and not interrupted:
        e.evolve(2000)
        plt.clf()
        e.y.plot()
        plt.twinx()
        Va, Vb, Vab = e.y.get_Vext()
        assert np.allclose(Vab, 0)
        plt.plot(e.y.xyz[0], Va)
        plt.plot(e.y.xyz[0], Vb)
        display(plt.gcf())
        clear_output(wait=True)
```

```{code-cell} ipython3
plt.figure(figsize=(40, 10))
state1 = e.get_y()
state1.plot()
plt.xlim(-300, 300)
plt.savefig("tst.pdf")
!open tst.pdf
```

# Experiment 1

+++

* No counterflow (gradient)
* Roughly zero detuning.
* Start with 100% in (1, -1), get ground state, then transfer 50% to (1,0).
* Ramp on SOC/Raman couplings over 100ms (10ms should work too).
  * `rabi_frequency_E_R=1.9`
* Wait for up to 200ms
* Image after expansion for 7ms with everything off.

Observations:
  * After ramp there are lots of excitations - are they just spin waves, or are they solitons?
  * Lots of excitations and bunny-ear solitons

```{code-cell} ipython3
from soc_experiments import Experiment1
```

```{code-cell} ipython3
expt = Experiment1(coolong_phase=1.0)
expt
```

```{code-cell} ipython3
from runs import Run1
```

```{code-cell} ipython3
r = Run1()
s = r.simulations[0]
```

```{code-cell} ipython3
%pylab inline --no-import-all
import mmf_setup.set_path.hgroot
from soc_experiments import Experiment1

expt = Experiment1()
state = expt.get_state()
fig = state.plot()
```

## Small

+++

To understand what is happening here, we can do several things.  First we use a small periodic box with a slight spatial modulation.  Second we use a small trap.

```{code-cell} ipython3
1000.0 / 2 ** 14
```

### Box

```{code-cell} ipython3
%pylab inline --no-import-all
import mmf_setup.set_path.hgroot
from IPython.display import display, clear_output
from gpe import bec2

reload(bec2)
from gpe import utils

reload(utils)
import soc_experiments

reload(soc_experiments)
from gpe.minimize import MinimizeState

from soc_experiments import u, State2, Experiment1


class Experiment1small(Experiment1):
    def __init__(self, dxyz=(0.03 * u.micron,), cells_x=20, cooling_phase=0.0):
        Experiment1.__init__(
            self, dxyz=dxyz, cells_x=cells_x, cooling_phase=cooling_phase
        )


expt = Experiment1small(cells_x=100, cooling_phase=0.015)
state = expt.get_initial_state(perturb=0.1)
fig = state.plot()
```

```{code-cell} ipython3
expt = Experiment1small(cells_x=100, cooling_phase=0.015)
state = expt.get_initial_state(perturb=0.1)
state.plot()

from IPython.display import clear_output, display
from pytimeode.evolvers import EvolverABM
from mmfutils.contexts import NoInterrupt

state.t = 0 * u.ms
state.cooling_phase = 1 + 0.02j
e = EvolverABM(state, dt=0.5 * state.t_scale)
plt.figure(figsize=(20, 6))
# import cProfile
# cProfile.run('e.evolve(2000)', filename='prof.dat')

fig = plt.figure(figsize=(10, 5))
with NoInterrupt(ignore=True) as interrupted:
    while e.t < expt.t_final * expt.t_unit and not interrupted:
        e.evolve(2000)
        plt.clf()
        plt.subplot(211)
        fig = e.y.plot(fig=fig)
        plt.twinx()
        Va, Vb, Vab = e.y.get_Vext()
        plt.plot(e.y.xyz[0], Va)
        plt.plot(e.y.xyz[0], Vb)
        # plt.subplot(212)
        # e.y.plot_k()
        # plt.xlim(-5,5)
        display(plt.gcf())
        clear_output(wait=True)
```

```{code-cell} ipython3
from IPython.display import clear_output, display
from pytimeode.evolvers import EvolverABM
from mmfutils.contexts import NoInterrupt
```

```{code-cell} ipython3
state.t = 0 * u.ms
state.cooling_phase = 1 + 0.001j
e = EvolverABM(state, dt=0.5 * state.t_scale)
plt.figure(figsize=(20, 5))
with NoInterrupt(ignore=True) as interrupted:
    while e.t < expt.t_final and not interrupted:
        e.evolve(2000)
        plt.clf()
        e.y.plot()
        plt.twinx()
        Va, Vb, Vab = e.y.get_Vext()
        plt.plot(e.y.xyz[0], Va)
        plt.plot(e.y.xyz[0], Vb)
        display(plt.gcf())
        clear_output(wait=True)
```

```{code-cell} ipython3
def get_box_state(
    s, Experiment=Experiment1, Nxyz=(2 ** 13,), Lxyz=(1000 * u.micron,), **kw
):
    experiment = Experiment(**kw)
    experiment.Rx_TF /= s
    tf = experiment.trapping_frequencies
    experiment.trapping_frequencies = (tf[0] * s,) + tf[1:]
    experiment.init()
    state = experiment.get_initial_state(
        Nxyz=(Nxyz[0] // s,) + Nxyz[1:], Lxyz=(Lxyz[0] / s,) + Lxyz[1:], **kw
    )
    return state


%time state = get_small_state(s=32)
expt = state.experiment
state.plot()
```

### Small Trap

+++

To get an idea for what is happening here, we start with a small simulation.  We keep the central density fixed so that the central healing length remains at the same scale compared with the SOC length $k_r^{-1}$, but we increase the trapping frequency so that the simulation takes place in a small physical space.  We scale the number of lattice points so that we maintain UV convergence with a $\d{x} \sim 0.1\mu$m.  We scale down from the original simulations by a factor of $s$ as follows:

\begin{align}
  \d{x} &\rightarrow \d{x},
  & \mu &\rightarrow \mu,
  & g &\rightarrow g,
  & k_r & \rightarrow k_r,
  & \Omega &\rightarrow \Omega,
  & \delta &\rightarrow \delta,
  \\
  N &\rightarrow s^{-1}N,
  & L &\rightarrow s^{-1}L,
  & R_{TF} &\rightarrow s^{-1}R_{TF},
  & \omega_x &\rightarrow s\omega_x,\\
\end{align}

```{code-cell} ipython3
%pylab inline --no-import-all
from IPython.display import display, clear_output
import bec2

reload(bec2)
import soc_experiments

reload(soc_experiments)
from soc_experiments import u, Experiment1

s = 32


def get_small_state(
    s, Experiment=Experiment1, Nxyz=(2 ** 13,), Lxyz=(1000 * u.micron,), **kw
):
    experiment = Experiment(**kw)
    experiment.Rx_TF /= s
    tf = experiment.trapping_frequencies
    experiment.trapping_frequencies = (tf[0] * s,) + tf[1:]
    experiment.init()
    state = experiment.get_initial_state(
        Nxyz=(Nxyz[0] // s,) + Nxyz[1:], Lxyz=(Lxyz[0] / s,) + Lxyz[1:], **kw
    )
    return state


%time state = get_small_state(s=32)
expt = state.experiment
state.plot()
```

```{code-cell} ipython3
from IPython.display import clear_output, display
from pytimeode.evolvers import EvolverABM
from mmfutils.contexts import NoInterrupt
```

```{code-cell} ipython3
state.t = 0 * u.ms
state.cooling_phase = 1 + 0.01j
e = EvolverABM(state, dt=0.2 * state.t_scale)
plt.figure(figsize=(20, 5))
with NoInterrupt(ignore=True) as interrupted:
    while e.t < expt.t_final and not interrupted:
        e.evolve(2000)
        plt.clf()
        e.y.plot()
        plt.twinx()
        Va, Vb, Vab = e.y.get_Vext()
        plt.plot(e.y.xyz[0], Va)
        plt.plot(e.y.xyz[0], Vb)
        display(plt.gcf())
        clear_output(wait=True)
```

To test convergence, we run two simulations with different $\d{x}$:

```{code-cell} ipython3
def get_small_state(
    s, Experiment=Experiment1, Nxyz=(2 ** 13,), Lxyz=(1000 * u.micron,), **kw
):
    experiment = Experiment(**kw)
    experiment.Rx_TF /= s
    tf = experiment.trapping_frequencies
    experiment.trapping_frequencies = (tf[0] * s,) + tf[1:]
    experiment.init()
    state = experiment.get_initial_state(
        Nxyz=(Nxyz[0] // s,) + Nxyz[1:], Lxyz=(Lxyz[0] / s,) + Lxyz[1:], **kw
    )
    return state


%time state1 = get_small_state(s=32, Nxyz=(2**14,))
%time state2 = get_small_state(s=32, Nxyz=(2**15,))
states = [state1, state2]

evolvers = []
dt = 0.2 * states[0].t_scale
T = 2000 * dt
for state in states:
    state.t = 0 * u.ms
    state.cooling_phase = 1 + 0.01j
    e = EvolverABM(state, dt=0.2 * state.t_scale)
    e._steps = int(np.round(T / e.dt))
    evolvers.append(e)

plt.figure(figsize=(20, 5))
errs = []
with NoInterrupt(ignore=True) as interrupted:
    while evolvers[0].t < expt.t_final and not interrupted:
        plt.clf()
        for e in evolvers:
            e.evolve(e._steps)
            e.y.plot()
            plt.twinx()
            Va, Vb, Vab = e.y.get_Vext()
            plt.plot(e.y.xyz[0], Va)
            plt.plot(e.y.xyz[0], Vb)
        display(plt.gcf())
        clear_output(wait=True)
```

## Full

```{code-cell} ipython3
%pylab inline --no-import-all
from IPython.display import display, clear_output
import bec2

reload(bec2)
import soc_experiments

reload(soc_experiments)
from soc_experiments import u, Experiment1

expt = Experiment1()
args = dict(Nxyz=(2 ** 14,), Lxyz=(1000 * u.micron,))
#%time state = expt.get_initial_state(**args)
# state.rotating_phases = False
# state.plot()
state = expt.get_state(**args)
expt.B_gradient
```

```{code-cell} ipython3
from utils import Frames
```

```{code-cell} ipython3
with Frames("_data/experiment1") as frames:
    pass
_state[0].plot()
plt.xlim(0, 20)
```

```{code-cell} ipython3
%pylab inline
import utils
```

```{code-cell} ipython3
reload(utils)
import runs
```

```{code-cell} ipython3
reload(runs)
plt.figure(figsize=(20, 5))
_state = [None]
```

```{code-cell} ipython3
def plot_state(state):
    _state[0] = state
    state.plot()
    plt.xlim(-200, 200)
```

```{code-cell} ipython3
# runs.view_experiment1(plot_state, cooling=0)
# runs.view_experiment1(plot_state, cooling=0.005)
runs.view_experiment1(plot_state, cooling=0.01)
# runs.view_experiment1(plot_state, cooling=0.05)
```

```{code-cell} ipython3
%pylab inline
import utils
```

```{code-cell} ipython3
reload(utils)
import runs
```

```{code-cell} ipython3
reload(runs)
plt.figure(figsize=(20, 5))
```

```{code-cell} ipython3
def plot_state(state):
    state.plot()
    plt.xlim(0, 100)
```

```{code-cell} ipython3
runs.view_experiment1(plot_state)
```

```{code-cell} ipython3
from IPython.display import clear_output, display
from pytimeode.evolvers import EvolverABM
from mmfutils.contexts import NoInterrupt
```

```{code-cell} ipython3
state.t = 0 * u.ms
state.cooling_phase = 1 + 0.01j
e = EvolverABM(state, dt=0.1 * state.t_scale)
plt.figure(figsize=(40, 5))
with NoInterrupt(ignore=True) as interrupted:
    while e.t < expt.t_final and not interrupted:
        e.evolve(2000)
        plt.clf()
        e.y.plot()
        plt.twinx()
        Va, Vb, Vab = e.y.get_Vext()
        plt.plot(e.y.xyz[0], Va)
        plt.plot(e.y.xyz[0], Vb)
        display(plt.gcf())
        clear_output(wait=True)
```

```{code-cell} ipython3
e.y.experiment.t_expansion / u.ms
```

```{code-cell} ipython3
%pylab inline --no-import-all
from IPython.display import display, clear_output
import bec2

reload(bec2)
import soc_experiments

reload(soc_experiments)
from soc_experiments import u, Experiment1

expt = Experiment1()
%time state = expt.get_initial_state(Nxyz=(2**12/3,))
state.rotating_phases = False
state.plot()
```

```{code-cell} ipython3
x = state.xyz[0]
# plt.plot(x, np.angle(state[0]))
# *np.exp(1j*state.xyz[0]*state.k_R)
plt.plot(state.kxyz[0][0] / state.k_R, abs(np.fft.fft(state[0])), "+")
# plt.xlim(-2,)
```

```{code-cell} ipython3
from IPython.display import clear_output, display
from pytimeode.evolvers import EvolverABM
from mmfutils.contexts import NoInterrupt
```

```{code-cell} ipython3
state.t = 0 * u.ms
e = EvolverABM(state, dt=0.1 * state.t_scale)
plt.figure(figsize=(20, 5))
with NoInterrupt(ignore=True) as interrupted:
    while e.t < expt.t_final and not interrupted:
        e.evolve(2000)
        plt.clf()
        e.y.plot()
        plt.twinx()
        Va, Vb, Vab = e.y.get_Vext()
        plt.plot(e.y.xyz[0], Va)
        plt.plot(e.y.xyz[0], Vb)
        display(plt.gcf())
        clear_output(wait=True)
```

```{code-cell} ipython3
%pylab inline --no-import-all
from IPython.display import display, clear_output
import bec2

reload(bec2)
import soc_experiments

reload(soc_experiments)
from soc_experiments import u, Experiment1

expt = Experiment1(Rx_TF=24 * u.micron, detuning_E_R=0.1)
%time state = expt.get_initial_state(Nxyz=(2**12/4,), Lxyz=(120*u.micron,))
state.plot()

from IPython.display import clear_output, display
from pytimeode.evolvers import EvolverABM
from mmfutils.contexts import NoInterrupt

state.t = 0 * u.ms
e = EvolverABM(state, dt=0.05 * state.t_scale)
plt.figure(figsize=(20, 5))
with NoInterrupt(ignore=True) as interrupted:
    while e.t < expt.t_final and not interrupted:
        e.evolve(2000)
        plt.clf()
        e.y.plot()
        plt.twinx()
        Va, Vb, Vab = e.y.get_Vext()
        plt.plot(e.y.xyz[0], Va)
        plt.plot(e.y.xyz[0], Vb)
        display(plt.gcf())
        clear_output(wait=True)
```

```{code-cell} ipython3
%pylab inline --no-import-all
from IPython.display import display, clear_output
import bec2

reload(bec2)
import soc_experiments

reload(soc_experiments)
from soc_experiments import u, Experiment1

expt = Experiment1(Rx_TF=24 * u.micron, detuning_E_R=0.1)
%time state = expt.get_initial_state(Nxyz=(2**12/16,), Lxyz=(120*u.micron,), tries=100)
state.plot()

from IPython.display import clear_output, display
from pytimeode.evolvers import EvolverABM
from mmfutils.contexts import NoInterrupt

state.t = 0 * u.ms
e = EvolverABM(state, dt=0.5 * state.t_scale)
plt.figure(figsize=(20, 5))
with NoInterrupt(ignore=True) as interrupted:
    while e.t < expt.t_final and not interrupted:
        e.evolve(2000)
        plt.clf()
        e.y.plot()
        plt.twinx()
        Va, Vb, Vab = e.y.get_Vext()
        plt.plot(e.y.xyz[0], Va)
        plt.plot(e.y.xyz[0], Vb)
        display(plt.gcf())
        clear_output(wait=True)
```

```{code-cell} ipython3
%pylab inline --no-import-all
from IPython.display import display, clear_output
import bec2

reload(bec2)
import soc_experiments

reload(soc_experiments)
from soc_experiments import u, Experiment1

expt = Experiment1(Rx_TF=4 * u.micron, detuning_E_R=0.1)
%time state = expt.get_initial_state(Nxyz=(2**12/2,), Lxyz=(120*u.micron,), tries=100)
state.plot()

from IPython.display import clear_output, display
from pytimeode.evolvers import EvolverABM
from mmfutils.contexts import NoInterrupt

state.t = 0 * u.ms
e = EvolverABM(state, dt=0.5 * state.t_scale)
plt.figure(figsize=(20, 5))
with NoInterrupt(ignore=True) as interrupted:
    while e.t < expt.t_final and not interrupted:
        e.evolve(2000)
        plt.clf()
        e.y.plot()
        plt.twinx()
        Va, Vb, Vab = e.y.get_Vext()
        plt.plot(e.y.xyz[0], Va)
        plt.plot(e.y.xyz[0], Vb)
        display(plt.gcf())
        clear_output(wait=True)
```

Note that only the spin waves develop here: the total density remains constant.

```{code-cell} ipython3
import bec2

reload(bec2)
import soc_experiments

reload(soc_experiments)
from soc_experiments import State, u, Experiment
import minimize

reload(minimize)

e = Experiment()
s2 = e.get_state(
    Nxyz=(2 ** 12 / 8,), Lxyz=(1000 * u.micron,), cooling_phase=1 + 0.000j, Omega=0.0
)

s2[1, ...] = 0.0
m = minimize.Minimize(s2, fix_N=True)
assert m.check_state()
s = m.minimize(psi_tol=1e-14, _test=True)
s.plot()
```

```{code-cell} ipython3
plt.plot(s2.xyz[0], abs(s2.get_energy_density()))
plt.plot(s2.xyz[0], abs(s2[0, ...]))
```

```{code-cell} ipython3
Va, Vb, Vab = s.get_Vext()
x = s.xyz[0]
plt.plot(x / u.micron, Va)
plt.plot(x / u.micron, Vb)
plt.xlim(-20, 20)
plt.ylim(-0.001, 0.002)
```

```{code-cell} ipython3
s = m.minimize(E_tol=1e-16, psi_tol=1e-16, tries=10, disp=1, _test=True)
```

```{code-cell} ipython3
%pylab inline --no-import-all
from IPython.display import clear_output, display
from pytimeode.evolvers import EvolverABM
from mmfutils.contexts import NoInterrupt
```

```{code-cell} ipython3
e = EvolverABM(s, dt=0.0081 * s.t_scale)
with NoInterrupt(ignore=True) as interrupted:
    while e.t < 1 * u.s and not interrupted:
        e.evolve(1000)
        plt.clf()
        e.y.plot()
        display(plt.gcf())
        clear_output(wait=True)
```

```{code-cell} ipython3
:init_cell: true

%pylab inline --no-import-all
from IPython.display import display, clear_output, HTML
from pytimeode.evolvers import EvolverABM
from mmfutils.contexts import NoInterrupt, coroutine
import mmfutils.plot.animation

reload(mmfutils.plot.animation)
from mmfutils.plot.animation import MyFuncAnimation

import soc_experiments

reload(soc_experiments)
from soc_experiments import u, State, Experiment
import minimize

reload(minimize)


@coroutine
def plotter(fig=None, plot=True):
    if fig is None:
        fig = plt.figure(figsize=(20, 5))
    plt.clf()
    lines = [
        plt.plot([], [], "k-")[0],
        plt.plot([], [], "b:")[0],
        plt.plot([], [], "g:")[0],
    ]
    title = plt.title("")
    x = None
    plt.xlabel("x [micron]")
    plt.ylabel("n")
    plt.xlim(-300, 300)
    plt.ylim(0, 300)

    while True:
        state = yield lines
        na, nb = state.get_density()
        E = state.get_energy()
        N = state.get_N()
        t = state.t
        title_text = "t={:.4f}ms, N={:.4f}, E={:.4f}".format(t / u.ms, N, E)
        if x is None:
            x = state.xyz[0] / u.micron
            # lines.extend(plt.plot(x, na, 'b'))
            # lines.extend(plt.plot(x, nb, 'g'))
        lines[0].set_data(x, na + nb)
        lines[1].set_data(x, na)
        lines[2].set_data(x, nb)
        title.set_text(title_text)

        if plot:
            display(fig)
        else:
            display("{} < 1000ms".format(t / u.ms))
        clear_output(wait=True)
```

```{code-cell} ipython3
e = Experiment()
factor = 1
s = e.get_state(Nxyz=(2 ** 12 / factor,), Lxyz=(1000 * u.micron,), cooling_phase=1)
s[1, ...] = 0
s.init()
s.delta = 100
m = minimize.Minimize(s, fix_N=True)
s1 = m.minimize(psi_tol=1e-12)
s1.delta = 0
s1.plot()
```

```{code-cell} ipython3
def get_data(factor=1, steps=100, cooling_phase=1 + 0.0001j, psi_tol=1e-12):
    """Large factors are faster but less accurate"""
    e = Experiment()
    s = e.get_state(
        Nxyz=(2 ** 12 / factor,), Lxyz=(1000 * u.micron,), cooling_phase=1 + 0.00j
    )
    s[1, ...] = 0
    if psi_tol:
        # s.constraint = 'Nab'
        s.delta = 100.0
        s.init()
        m = minimize.Minimize(s, fix_N=True)
        s = m.minimize(psi_tol=1e-6)
        m.state = s
        s = m.minimize(psi_tol=psi_tol)
        s.delta = 0.0
        # s.coonstraint = 'N'

    # s.cooling_phase = 1.0j
    # e = EvolverABM(s, dt=0.064/f*s.t_scale)
    # e.evolve(100)
    # s = e.get_y()

    s.cooling_phase = cooling_phase
    s.t = 0.0

    e = EvolverABM(s, dt=0.06 / factor * s.t_scale)
    with NoInterrupt(ignore=True) as interrupted:
        while e.t < 1 * u.s and not interrupted:
            e.evolve(steps)
            yield e.y


fig = plt.figure(figsize=(20, 5))
with plotter(fig=fig, plot=True) as plot_data:
    anim = MyFuncAnimation(
        fig,
        plot_data,
        frames=get_data(factor=8, steps=1000, psi_tol=1e-12),
        save_count=None,
    )
    filename = "SOC_Solitons.mp4"
    anim.save(
        filename=filename,
        fps=20,
        extra_args=["-vcodec", "libx264", "-pix_fmt", "yuv420p"],
    )
plt.close("all")
clear_output()
display(
    HTML('<video src="{0}" width="100%", type="video/mp4" controls/>'.format(filename))
)
```

```{code-cell} ipython3
import soc_experiments
```

```{code-cell} ipython3
reload(soc_experiments)
r = soc_experiments.RunSOC1()
r.run(factor=8)
```

```{code-cell} ipython3
s = self = r.s
x = self.xyz[0]
V_Bs = self.B_gradient * x[None, :] * np.asarray(self.mu_Bs)[:, None]
plt.plot(x, -s.delta / 2 + V_Bs[0])
plt.plot(x, +s.delta / 2 + V_Bs[1])
```

```{code-cell} ipython3
k = s.kxyz[0]
plt.plot(k, u.hbar ** 2 * (k - s.k_R) ** 2 / 2 / u.m - s.delta / 2.0, ".")
plt.plot(k, u.hbar ** 2 * (k + s.k_R) ** 2 / 2 / u.m + s.delta / 2.0, ".")
```

Here we turn off the SOC and start with equal populations.

```{code-cell} ipython3
def get_data(factor=1, steps=100, cooling_phase=1 + 0.0001j, psi_tol=1e-12):
    """Large factors are faster but less accurate"""
    e = Experiment(rabi_frequency=0.0)
    s = e.get_state(
        Nxyz=(2 ** 12 / factor,), Lxyz=(1000 * u.micron,), cooling_phase=1 + 0.00j
    )

    s[1, ...] = 0
    if psi_tol:
        # s.constraint = 'Nab'
        s.init()
        m = minimize.Minimize(s, fix_N=True)
        s = m.minimize(psi_tol=1e-6)
        m.state = s
        s = m.minimize(psi_tol=psi_tol)
        # s.coonstraint = 'N'
    else:
        s.cooling_phase = 1.0j
        e = EvolverABM(s, dt=0.06 / factor * s.t_scale)
        e.evolve(1000)
        s = e.get_y()

    s[1, ...] = s[0, ...]
    s /= np.sqrt(2)
    s.cooling_phase = cooling_phase
    s.t = 0.0

    e = EvolverABM(s, dt=0.06 / factor * s.t_scale)
    with NoInterrupt(ignore=True) as interrupted:
        while e.t < 1 * u.s and not interrupted:
            e.evolve(steps)
            yield e.y


fig = plt.figure(figsize=(20, 5))
with plotter(fig=fig, plot=True) as plot_data:
    anim = MyFuncAnimation(
        fig, plot_data, frames=get_data(factor=2, steps=1000), save_count=None
    )
    filename = "Rabi_Solitons.mp4"
    anim.save(
        filename=filename,
        fps=20,
        extra_args=["-vcodec", "libx264", "-pix_fmt", "yuv420p"],
    )
plt.close("all")
clear_output()
display(
    HTML('<video src="{0}" width="100%", type="video/mp4" controls/>'.format(filename))
)
```

```{code-cell} ipython3
%pylab inline --no-import-all
from IPython.display import display, clear_output
from pytimeode.evolvers import EvolverABM
from mmfutils.contexts import NoInterrupt

import bec2

reload(bec2)
from bec2 import State2, u, Experiment

e = Experiment(rabi_frequency=0.0)
f = 2
s = e.get_state(
    Nxyz=(2 ** 12 / 16 * f,), Lxyz=(1000 * u.micron,), cooling_phase=1 + 0.00j
)
s[1, ...] = 0

s.cooling_phase = 1.0j
e = EvolverABM(s, dt=0.004 * f * s.t_scale)
e.evolve(1000)

s = e.get_y()
s.cooling_phase = 1 + 0.01j
s.t = 0.0
s[1, ...] = s[0, ...]
s /= np.sqrt(2)

e = EvolverABM(s, dt=0.004 * f * s.t_scale)
with NoInterrupt(ignore=True) as interrupted:
    while e.t < 1 * u.s and not interrupted:
        e.evolve(1000)
        plt.clf()
        e.y.plot()
        display(plt.gcf())
        clear_output(wait=True)
```

```{code-cell} ipython3
import cProfile
```

```{code-cell} ipython3
%time cProfile.run('e.evolve(2000)', filename='prof.dat')
```

```{code-cell} ipython3
%load_ext line_profiler
%lprun -f s.compute_dy_dt e.evolve(2000)
```

```{code-cell} ipython3
%pylab inline --no-import-all
from IPython.display import display, clear_output
from pytimeode.evolvers import EvolverABM
from mmfutils.contexts import NoInterrupt

import bec2

reload(bec2)
from bec2 import State2, u, Experiment

e = Experiment()
f = 16
s = e.get_state(
    Nxyz=(2 ** 12 / 16 * f,), Lxyz=(1000 * u.micron,), cooling_phase=1 + 0.0j
)
s[1, ...] = 0
s.plot()

e = EvolverABM(s, dt=0.01 * f * s.t_scale)
with NoInterrupt(ignore=True) as interrupted:
    while e.t < 1 * u.s and not interrupted:
        e.evolve(500)
        plt.clf()
        e.y.plot()
        display(plt.gcf())
        clear_output(wait=True)
```

```{code-cell} ipython3
%pylab inline --no-import-all
from IPython.display import display, clear_output
import bec2

reload(bec2)
from bec2 import State2, u

E_R = u.hbar * 2 * np.pi * 3683.8 * u.Hz
s = State2(
    Nxyz=(2 ** 12 / 64,),
    Lxyz=(1000 * u.micron,),
    ws=2 * np.pi * np.array([3.0, 273.0, 277.0]) * u.Hz,
    N=0.8e6,
    k_R=u.m * (5.8 * u.mm / u.s) / u.hbar,
    delta=2 * np.pi * u.hbar * 2.5 * u.kHz,
    Omega=0.1 * E_R,
    cooling_phase=1 + 0.1j,
)
s[1, ...] = 0
s.plot()
```

# Solitons

+++

Here we want to look at self-stabilized solitons in a SOC.  To do this, we want to start with an initial state that is well localized in both position and momentum space.

```{code-cell} ipython3
%pylab inline --no-import-all
import mmf_setup.set_path.hgroot
from IPython.display import display, clear_output
from gpe import bec2

reload(bec2)
from gpe import utils

reload(utils)
from gpe import soc_experiments

reload(soc_experiments)
from gpe.minimize import MinimizeState

from soc_experiments import u, State2, ExperimentSoliton

expt = ExperimentSoliton(dxyz=(0.03 * u.micron,), cells_x=20, detuning_E_R=0.0)
state = expt.get_state()
np.random.seed(2)
state[...] *= np.exp(2j * np.pi * np.random.random(state.shape))
state.plot()

state.cooling_phase = 1 + 1j
state.t = -100 * u.ms
e = EvolverABM(state, dt=0.5 * state.t_scale)
with NoInterrupt(ignore=True) as interrupted:
    while e.t < 0.0 * u.ms and not interrupted:
        e.evolve(200)
        plt.clf()
        e.y.plot()
        plt.twinx()
        plt.plot(e.y.xyz[0], e.y.get_Vext()[0])
        plt.plot(e.y.xyz[0], e.y.get_Vext()[1])
        display(plt.gcf())
        clear_output(wait=True)
```

```{code-cell} ipython3
s = e.get_y()
s.t = 0
s.cooling_phase = 1.0 + 0.0j
e1 = EvolverABM(s, dt=0.5 * s.t_scale)
with NoInterrupt(ignore=True) as interrupted:
    while e1.t < 10.0 * u.ms and not interrupted:
        e1.evolve(200)
        plt.clf()
        e1.y.plot()
        plt.twinx()
        plt.plot(e1.y.xyz[0], e1.y.get_Vext()[0])
        plt.plot(e1.y.xyz[0], e1.y.get_Vext()[1])
        display(plt.gcf())
        clear_output(wait=True)
```

```{code-cell} ipython3
%pylab inline --no-import-all
import mmf_setup.set_path.hgroot
from IPython.display import display, clear_output
from gpe import bec2

reload(bec2)
from gpe import utils

reload(utils)
from gpe import soc

reload(soc)
from gpe.minimize import MinimizeState

from soc_experiments import u, State2, ExperimentSoliton

expt = ExperimentSoliton(dxyz=(0.03 * u.micron,), cells_x=2000, detuning_E_R=0.0)
state = expt.get_state()
np.random.seed(2)
state[...] *= np.exp(0.1j * np.pi * np.random.random(state.shape))
state.plot()

state.cooling_phase = 1 + 1j
state.t = -100 * u.ms
e = EvolverABM(state, dt=0.5 * state.t_scale)
with NoInterrupt(ignore=True) as interrupted:
    while e.t < 0.0 * u.ms and not interrupted:
        e.evolve(200)
        plt.clf()
        e.y.plot()
        plt.twinx()
        plt.plot(e.y.xyz[0], e.y.get_Vext()[0])
        plt.plot(e.y.xyz[0], e.y.get_Vext()[1])
        display(plt.gcf())
        clear_output(wait=True)
```

```{code-cell} ipython3
# s = e.get_y()
s.t = 0
s.cooling_phase = 1.0
e1 = EvolverABM(s, dt=0.5 * s.t_scale)
with NoInterrupt(ignore=True) as interrupted:
    while e1.t < 10.0 * u.ms and not interrupted:
        e1.evolve(200)
        plt.clf()
        e1.y.plot()
        plt.twinx()
        plt.plot(e1.y.xyz[0], e1.y.get_Vext()[0])
        plt.plot(e1.y.xyz[0], e1.y.get_Vext()[1])
        display(plt.gcf())
        clear_output(wait=True)
```

```{code-cell} ipython3
s = e1.get_y()
```

```{code-cell} ipython3
%pylab inline --no-import-all
import bec2
from bec2 import u


class Study:
    def __init__(self, **kw):
        self.__dict__.update(kw)
        self.init()


import soc


class State1(soc.State1):

    Ex = soc.Dispersion()
    Ex._d0 = 0.0
    Ex.__init__()
    Ex._k0 = 0.0


s = State1(Nxyz=(128 * 2,), Lxyz=(100.0 * u.micron,))
s.plot()
s.ws = [0.0]
```

```{code-cell} ipython3
%pylab inline --no-import-all
from IPython.display import display, clear_output
from pytimeode.evolvers import EvolverABM
from mmfutils.contexts import NoInterrupt
```

```{code-cell} ipython3
s.cooling_phase = 1
e = EvolverABM(s, dt=0.5 * s.t_scale)
with NoInterrupt(ignore=True) as interrupted:
    while e.t < 100.0 * u.ms and not interrupted:
        e.evolve(200)
        plt.clf()
        e.y.plot()
        plt.twinx()
        plt.plot(e.y.xyz[0], e.y.get_Vext())
        display(plt.gcf())
        clear_output(wait=True)
```

# Tests

```{code-cell} ipython3
%pylab inline --no-import-all
from IPython.display import display, clear_output
import bec2

reload(bec2)
import bec

reload(bec)
import soc

reload(soc)
import minimize

reload(minimize)
from soc_experiments import State, u, Experiment

e = Experiment()
s = e.get_state(
    Nxyz=(2 ** 12 / 8,),
    Lxyz=(1000 * u.micron,),
    cooling_phase=1 + 0.000j,
    constraint="N",
)
s[0, ...] = 1.0
s[1, ...] = np.sqrt(0.5)

m = minimize.MinimizeState(s, fix_N=True)
assert m.check()
s1 = m.minimize(psi_tol=1e-6, tries=10, disp=1)
# m.state = s1
m = minimize.MinimizeState(s1, fix_N=True)
assert m.check()
s1 = m.minimize(psi_tol=1e-15, tries=100, disp=1)
s1.plot()
print (s1.get_Ns())
```

# Magnetic Moments

+++

For $^{87}$Rb, the Hamiltonian in the presence of a magnetic field $\vect{B}=B\uvect{z}$ is:

$$
  A\vect{I}\cdot\vect{J} + \overbrace{g\mu_B B}^{C} J_z + \overbrace{-\frac{\mu}{I}B}^{D}I_z
$$

where $\vect{I}$ and $\vect{J}$ are the operators for nuclear spin and electronic angular momentum respectively.


The hyperfine states are classified in terms of total angular momentum $\vect{F} = \vect{I} + \vect{J}$ and

$$
  \vect{I}\cdot\vect{J} = \frac{1}{2}[F(F+1) - I(I+1) - J(J+1)].
$$

Here are the parameters for $^{87}$Rb (see Table 3.1 in Pethick and Smith):

$$
  Z = 37, \qquad
  N = 50, \qquad
  I = \frac{3}{2}, \qquad
  \mu = 2.751\mu_N, \qquad
  g \approx 2, \qquad
  \nu_{\mathrm{hf}} = 6835 \mathrm{MHz} = \left(I + \frac{1}{2}\right)\frac{2\pi A}{\hbar}.
$$


Here are the energies of the various states:

\begin{align}
  \ket{2, -2} &= \ket{-\tfrac{3}{2}, -\tfrac{1}{2}}, &
            E &= \frac{3}{4}A - \frac{1}{2}C -\frac{3}{2}D
         \approx \frac{3}{4}A - \frac{1}{2}C,\\
  \ket{1,  1} &= \sqrt{\tfrac{3}{4}}\ket{\tfrac{3}{2}, \tfrac{-1}{2}}
               - \sqrt{\tfrac{1}{4}}\ket{\tfrac{1}{2}, \tfrac{1}{2}}, &
            E &= -\frac{A}{4} + D - \sqrt{\frac{3}{4}A^2 + \frac{1}{4}(A+C-D)^2}
         \approx -\frac{A}{4} - \sqrt{\frac{3}{4}A^2 + \frac{1}{4}(A+C)^2} \\
  \ket{1,  0} &= \sqrt{\tfrac{1}{2}}\ket{\tfrac{1}{2}, \tfrac{-1}{2}}
               - \sqrt{\tfrac{1}{2}}\ket{\tfrac{-1}{2}, \tfrac{1}{2}}, &
            E &= -\frac{A}{4} - \sqrt{A^2 + \frac{1}{4}(C-D)^2}
         \approx -\frac{A}{4} - \sqrt{A^2 + \frac{1}{4}C^2} \\
  \ket{1, -1} &= \sqrt{\tfrac{3}{4}}\ket{\tfrac{-3}{2}, \tfrac{1}{2}}
               - \sqrt{\tfrac{1}{4}}\ket{\tfrac{-1}{2}, \tfrac{-1}{2}}, &
            E &= -\frac{A}{4} - D - \sqrt{\frac{3}{4}A^2 + \frac{1}{4}(A-C+D)^2}
         \approx -\frac{A}{4} - \sqrt{\frac{3}{4}A^2 + \frac{1}{4}(A-C)^2}.
\end{align}

For large fields we have:

\begin{align}
  \ket{2, -2}:&& E &\approx +\frac{3}{4}A - \frac{g\mu_B}{2}B,\\
  \ket{1, -1}:&& E &\approx +\frac{1}{4}A - \frac{g\mu_B}{2}B,\\
  \ket{1,  0}:&& E &\approx -\frac{1}{4}A - \frac{g\mu_B}{2}B,\\
  \ket{1,  1}:&& E &\approx -\frac{3}{4}A - \frac{g\mu_B}{2}B,
\end{align}

while for small fields we have:

\begin{align}
  \ket{2, -2}:&& E &\approx +\frac{3}{4}A - \frac{g\mu_B}{2}B,\\
  \ket{1, -1}:&& E &\approx -\frac{5}{4}A + \frac{g\mu_B}{4}B,\\
  \ket{1,  0}:&& E &\approx -\frac{5}{4}A,\\
  \ket{1,  1}:&& E &\approx -\frac{5}{4}A - \frac{g\mu_B}{4}B.
\end{align}
