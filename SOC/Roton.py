# ---
# jupyter:
#   jupytext:
#     formats: ipynb,py
#     text_representation:
#       extension: .py
#       format_name: light
#       format_version: '1.5'
#       jupytext_version: 1.4.0
#   kernelspec:
#     display_name: Python 2 (Ubuntu, plain)
#     language: python
#     name: python2-ubuntu
# ---

# + init_cell=true
import mmf_setup

mmf_setup.nbinit()
from gpe.imports import *
from mmfutils.plot import animation

# -

# # Roton

import soc_roton

reload(soc_roton)
from soc_roton import ExperimentRotonSmall, u

args = dict(
    xi=1.0,
    cells_x=100,
    dx=0.1 * u.micron,
    imprint_region_micron=5.0,
    imprint_k_k_r=2.0,
    t__imprint=0.01,
)
e = ExperimentRotonSmall(**args)
s = e.get_initial_state(E_tol=1e-5, use_scipy=True)
s.plot()

e = EvolverABM(s, dt=0.01 * s.t_scale)
fig = None
with NoInterrupt() as interrupted:
    while not interrupted:
        e.evolve(10)
        fig = e.y.plot(fig, show_momenta=True)
        display(fig)
        clear_output(wait=True)

# # Excitations

# Here we consider trying to excite a homogeneous cloud with a moving potential.
#
# Time-scales:
#
# * `T__R`:
# * `T__r`:

# +
import gpe.soc

reload(gpe.soc)
import soc_phonon

reload(soc_phonon)
from soc_phonon import ExperimentPhonon, u
from gpe import utils

args = dict(
    xi_micron=1.0,
    l_r_micron=0.5,
    d=1.0,
    w=0.0,
    dxyz=(0.1 * u.micron,),
    cells_x=100,
    barrier_k_k_r=1.0,
    barrier_depth_V_TF=0,
    barrier_v=1.0,
    tube=False,
)
expt = ExperimentPhonon(**args)
s = expt.get_initial_state(E_tol=1e-5, use_scipy=True)
s.plot()
# -

s.cooling_phase = 1j
s.constraint = None
e = EvolverABM(s, dt=0.3 * s.t_scale)
fig = None
history = []
skip = 10
with NoInterrupt() as interrupted:
    n = 0
    while not interrupted:
        e.evolve(100)
        history.append(e.get_y())
        if n % skip == 0:
            plt.clf()
            pe = e.y.plot(
                history=history,
                show_history_ab=True,
                show_momenta=True,
                k_range_k_r=(-3, 3),
            )
            display(plt.gcf())
            clear_output(wait=True)
        n += 1

# +
import gpe.soc

reload(gpe.soc)
import soc_catch_and_release

reload(soc_catch_and_release)
from soc_catch_and_release import ExperimentCatchAndReleaseSmall, u
from gpe import utils


class Experiment(ExperimentCatchAndReleaseSmall):
    V_TF = 0.1
    x_TF = None
    T__x = np.inf
    t__step = 100.0  # Time over which to turn on Barrier depth.
    t__barrier = 0.0
    barrier_depth_nK = -10.0
    barrier_width_micron = 2.0
    cells_x = 100
    dxyz = (0.2 * u.micron,)
    tube = False

    t__wait = 2 * 100.0

    def init(self):
        ExperimentCatchAndReleaseSmall.init(self)
        self.barrier_depth_t_ = utils.get_smooth_transition(
            [0.0, self.barrier_depth, 0.0],
            durations=[0, self.t__barrier],
            transitions=[self.t__step, self.t__step],
        )

        ExperimentCatchAndReleaseSmall.init(self)


expt = Experiment()
s = expt.get_initial_state(use_scipy=True)
# s.plot();
# -

e = EvolverABM(s, dt=0.3 * s.t_scale)
history = []
skip = 10
with NoInterrupt() as interrupted:
    n = 0
    while not interrupted:
        e.evolve(100)
        history.append(e.get_y())
        if n % skip == 0:
            plt.clf()
            e.y.plot(history=history)
            display(plt.gcf())
            clear_output(wait=True)
        n += 1
