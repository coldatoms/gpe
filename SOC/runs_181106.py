"""Simulations corresponding to the experimental runs data_180129.

"""
import itertools
import numpy as np

# Set sys.path to include hgroot so modules can be found.
import mmf_setup.set_path.hgroot

from mmfutils.contexts import NoInterrupt

import mmfutils.performance.threads
import mmfutils.performance.fft

import sys

from gpe import utils

import soc_catch_and_release

u = soc_catch_and_release.u

_DATA_DIR = "_data"


def set_threads(numexpr=1, fft=1, *v):
    if v:
        numexpr = fft = v[0]
    mmfutils.performance.threads.set_num_threads(numexpr)
    mmfutils.performance.fft.set_num_threads(fft)


# Needs testing, but performance on swan did not show an appreciable benefit to
# using multiple cores.
set_threads(1)


class RunBase:
    """Defines a set of runs based on experimental and simulation parameters.

    Provide a range of parameters for either the experiment or the simulation
    and the run() method will iterate through all combinations.
    """

    dt_ = 1.0

    experiment_params = dict(
        cooling_phase=[1.0, 1 + 0.005j, 1 + 0.01j],
    )

    simulation_params = {}

    @property
    def params_exp(self):
        """Return the list of parameter values in the same order as
        the experiments.
        """
        params = self.experiment_params
        keys = params.keys()
        iterators = [params[_key] for _key in keys]
        return [dict(zip(keys, values)) for values in itertools.product(*iterators)]

    @property
    def experiments(self):
        return [self.Experiment(**_params) for _params in self.params_exp]

    @property
    def params_sim(self):
        params = self.simulation_params
        keys = params.keys()
        iterators = [params[_key] for _key in keys]
        return [dict(zip(keys, values)) for values in itertools.product(*iterators)]

    @property
    def simulations(self):
        return [
            utils.Simulation(experiment=expt, dt_=self.dt_, data_dir=_DATA_DIR, **_params)
            for expt in self.experiments
            for _params in self.params_sim
        ]

    def run(self):
        with NoInterrupt(ignore=True) as interrupted:
            for sim in self.simulations:
                if interrupted:
                    break
                sim.run()
                if interrupted:
                    break
                sim.run_images()


class Run_InitialState_small(RunBase):
    """1d Tube Simulations corresponding to::

    data_181106/InitialState.txt
    """

    experiment_params = dict(
        cooling_phase=[1.0, 1 + 0.001j],  # , 1+0.005j],  # 1+0.01j, 1+0.015j],
        tube=[True],
        # rabi_frequency_E_R=[1.5],
        x_TF=[150 * u.micron],
        barrier_depth_nK=[-52.5],
        t__barrier=[0],
        t__image=[1.0],
        cells_x=[400],
    )
    simulation_params = dict(
        image_ts_=[np.array([0])],
        t__final=[0],
    )
    Experiment = soc_catch_and_release.ExperimentCatchAndReleaseSmall


class Run_InitialState(RunBase):
    """1d Tube Simulations corresponding to::

    data_181106/InitialState.txt
    """

    experiment_params = dict(
        cooling_phase=[1.0],  # 1+0.001j, 1+0.005j],  # 1+0.01j, 1+0.015j],
        tube=[True],
        # rabi_frequency_E_R=[1.5],
        x_TF=[170 * u.micron],
        barrier_depth_nK=[-52.5],
        t__barrier=[0],
        t__image=[1.0],
        Lx=[500 * u.micron],
    )
    simulation_params = dict(
        image_ts_=[np.array([0])],
        t__final=[0],
    )
    Experiment = soc_catch_and_release.ExperimentCatchAndRelease


if __name__ == "__main__":
    cmd = sys.argv[1]
    if cmd in locals():
        locals()[cmd]().run()
    else:
        raise ValueError("Command {} not found.".format(cmd))
