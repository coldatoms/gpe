"""Defines simulations to run with checkpoint/restart capabilities.

To use this, inherit from ExperimentBase, and pass any important variables to
the `local_dict` argument of the ExperimentBase.__init__() method.  These
variables will be used to generate unique filenames for each run.
"""
import itertools

import numpy as np

import mmf_setup.set_path.hgroot

from mmfutils.contexts import NoInterrupt

import mmfutils.performance.threads
import mmfutils.performance.fft

import sys
import os

from gpe import utils


from SOC import soc_vortex

u = soc_vortex.u

_DATA_DIR = "_data"


def set_threads(numexpr=1, fft=1, *v):
    if v:
        numexpr = fft = v[0]
    mmfutils.performance.threads.set_num_threads(numexpr)
    mmfutils.performance.fft.set_num_threads(fft)


# Needs testing, but performance on swan did not show an appreciable benefit to
# using multiple cores.
_NUM_THREADS = int(os.environ.get("OMP_NUM_THREADS", "1"))
set_threads(_NUM_THREADS)


class RunBase:
    dt_ = 0.5

    experiment_params = dict(
        cooling_phase=[1.0, 1 + 0.005j, 1 + 0.01j],
    )

    simulation_params = {}

    @property
    def experiments(self):
        params = self.experiment_params
        keys = params.keys()
        iterators = [params[_key] for _key in keys]
        return [
            self.Experiment(**dict(zip(keys, values)))
            for values in itertools.product(*iterators)
        ]

    @property
    def simulations(self):
        params = self.simulation_params
        keys = params.keys()
        iterators = [params[_key] for _key in keys]
        return [
            utils.Simulation(
                experiment=expt,
                dt_=self.dt_,
                data_dir=_DATA_DIR,
                **dict(zip(keys, values))
            )
            for expt in self.experiments
            for values in itertools.product(*iterators)
        ]

    def run(self, sim_number=None, image_ts_=None):
        if image_ts_ is not None:
            self.simulation_params["image_ts_"] = [image_ts_]

        if sim_number is None:
            sims = self.simulations
        else:
            sims = [self.simulations[sim_number]]

        with NoInterrupt(ignore=True) as interrupted:
            for sim in sims:
                if interrupted:
                    break
                sim.run()
                if interrupted:
                    break
                sim.run_images()


class RunVortex2(RunBase):
    """
    The state is cooled in to the minimum at t=0.
    Then the  barrier is turned on at t=0 and off at t=10.
    """

    experiment_params = dict(
        cooling_phase=[1.0, 1 + 0.01j],
        vortex=[(5, y) for y in np.linspace(-3.5, 3.5, 15)],
        single_band=[False, True],
        barrier_depth_nK=[-52.5],
    )
    simulation_params = dict(
        t__final=[2.0],
    )
    Experiment = soc_vortex.ExperimentVortexPair


class RunVortex3(RunBase):
    """
    The state is cooled in to the minimum at t=0.
    Then the  barrier is turned on at t=0 and off at t=10.
    """

    experiment_params = dict(
        cooling_phase=[1.0, 1 + 0.01j],
        vortex_y=np.linspace(-3.5, 3.5, 15),
        single_band=[False, True],
        barrier_depth_nK=[-60, -150],
    )
    simulation_params = dict(
        t__final=[8.0],
    )
    Experiment = soc_vortex.ExperimentVortexPair3


class RunVortexMF(RunBase):
    """
    Single Vortex rings of varrying size in SOC
    are cooled to find their equilibrium radii.

    The state is cooled in to the minimum at t=0.
    """

    dt_ = 0.1
    experiment_params = dict(
        v_x=[
            -3.0 * u.mm / u.s,
            -2.5 * u.mm / u.s,
            -2.0 * u.mm / u.s,
            -1.5 * u.mm / u.s,
            -1.0 * u.mm / u.s,
            -0.5 * u.mm / u.s,
            0.0 * u.mm / u.s,
            0.5 * u.mm / u.s,
            1.0 * u.mm / u.s,
            1.5 * u.mm / u.s,
            2.0 * u.mm / u.s,
            2.5 * u.mm / u.s,
            3.0 * u.mm / u.s,
        ],
        cooling_phase=[1.0, 1 + 0.01j],
        vortex=[
            (0, -3.5),
            (0, -3.0),
            (0, -2.5),
            (0, -2.0),
            (0, -1.5),
            (0, -1.0),
            (0, -0.5),
            (0, 0.0),
            (0, 0.5),
            (0, 1.0),
            (0, 1.5),
            (0, 2.0),
            (0, 2.5),
            (0, 3.0),
            (0, 3.5),
        ],
    )
    simulation_params = dict(
        t__final=[8.0],
    )
    Experiment = soc_vortex.ExperimentVortexMovingFrame


class RunVortexMF_LowRes(RunBase):
    """
    Single Vortex rings of varrying size in SOC
    are cooled to find their equilibrium radii.

    The state is cooled in to the minimum at t=0.
    """

    dt_ = 0.1
    experiment_params = dict(
        Lx=[40],
        R=[5],
        Nr=[2**6],
        Nx=[2**10],
        v_x=[
            -3.0 * u.mm / u.s,
            -2.5 * u.mm / u.s,
            -2.0 * u.mm / u.s,
            -1.5 * u.mm / u.s,
            -1.0 * u.mm / u.s,
            -0.5 * u.mm / u.s,
            0.0 * u.mm / u.s,
            0.5 * u.mm / u.s,
            1.0 * u.mm / u.s,
            1.5 * u.mm / u.s,
            2.0 * u.mm / u.s,
            2.5 * u.mm / u.s,
            3.0 * u.mm / u.s,
        ],
        cooling_phase=[1.0, 1 + 0.01j],
        vortex=[
            (0, -3.5),
            (0, -3.0),
            (0, -2.5),
            (0, -2.0),
            (0, -1.5),
            (0, -1.0),
            (0, -0.5),
            (0, 0.0),
            (0, 0.5),
            (0, 1.0),
            (0, 1.5),
            (0, 2.0),
            (0, 2.5),
            (0, 3.0),
            (0, 3.5),
        ],
    )
    simulation_params = dict(
        t__final=[8.0],
    )
    Experiment = soc_vortex.ExperimentVortexMovingFrame


class RunVortexMF_NoSOC_LowRes(RunBase):
    """
    Single Vortex rings of varrying size in SOC
    are cooled to find their equilibrium radii.

    The state is cooled in to the minimum at t=0.
    """

    dt_ = 0.1
    experiment_params = dict(
        detuning_kHz=[0],
        rabi_frequency_E_R=[0],
        Lx=[40],
        R=[5],
        Nr=[2**6],
        Nx=[2**10],
        v_x=[
            -3.0 * u.mm / u.s,
            -2.5 * u.mm / u.s,
            -2.0 * u.mm / u.s,
            -1.5 * u.mm / u.s,
            -1.0 * u.mm / u.s,
            -0.5 * u.mm / u.s,
            0.0 * u.mm / u.s,
            0.5 * u.mm / u.s,
            1.0 * u.mm / u.s,
            1.5 * u.mm / u.s,
            2.0 * u.mm / u.s,
            2.5 * u.mm / u.s,
            3.0 * u.mm / u.s,
        ],
        cooling_phase=[1.0, 1 + 0.01j],
        vortex=[
            (0, -3.5),
            (0, -3.0),
            (0, -2.5),
            (0, -2.0),
            (0, -1.5),
            (0, -1.0),
            (0, -0.5),
            (0, 0.0),
            (0, 0.5),
            (0, 1.0),
            (0, 1.5),
            (0, 2.0),
            (0, 2.5),
            (0, 3.0),
            (0, 3.5),
        ],
    )
    simulation_params = dict(
        t__final=[8.0],
    )
    Experiment = soc_vortex.ExperimentVortexMovingFrame


if __name__ == "__main__":
    # runs_car_fast_final Run0
    # runs_car_fast_final Run0 5
    # runs_car_fast_final Run0 5 "10,12"
    cmd = sys.argv[1]
    sim_number = image_ts_ = None
    if len(sys.argv) > 2:
        sim_number = int(sys.argv[2])
    if len(sys.argv) > 3:
        image_ts_ = list(map(float, sys.argv[3].split(",")))
    if cmd in locals():
        locals()[cmd]().run(sim_number=sim_number, image_ts_=image_ts_)
    else:
        raise ValueError("Command {} not found.".format(cmd))
