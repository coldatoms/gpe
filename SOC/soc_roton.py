import numpy as np

from scipy.integrate import odeint, quad

from gpe import utils
from gpe.soc import Experiment, u


class ExperimentRoton(Experiment):
    """Start in the ground state, then imprint a counter-propagating phase
    gradient.
    """

    t_unit = u.ms
    B_gradient_mG_cm = 0  # No counterflow

    detuning_kHz = 20.0
    rabi_frequency_E_R = 1.9  # Rabi frequency 1.0 - 2.9

    initial_imbalance = None  # Start in true ground state.

    t__image = 7  # time for expansion when imaging
    t__final = np.inf

    # Length of various regions for imprinting.  The first and last regions
    # will have the +-k phase imprint while the middle region will be flat.
    t__imprint = 1.0  # Time to imprint gradient
    imprint_k_k_r = 1.0
    imprint_plateau_micron = 10.0
    imprint_region_micron = 1.0

    def init(self):
        Experiment.init(self)

        tmp_ = utils.get_smooth_transition(
            [0, 1, 0],
            durations=[0.0, self.t__imprint],
            transitions=[0.1 * self.t__imprint] * 2,
        )

        # Compute adjustment factor so that the transition integrates to T.
        factor = self.t__imprint / quad(tmp_, 0, 1.2 * self.t__imprint)[0]
        self.imprint_amplitude_t_ = utils.get_smooth_transition(
            [0, factor, 0],
            durations=[0.0, self.t__imprint],
            transitions=[0.1 * self.t__imprint] * 2,
        )

    def get_Vt(self, state):
        x = state.xyz[0]
        k = self.imprint_k_k_r * self.k_r
        dX = self.imprint_region_micron * u.micron
        X = self.imprint_plateau_micron * u.micron
        T = self.t__imprint * self.t_unit
        V = self.imprint_amplitude_t_(state.t / self.t_unit) * state.hbar * k * dX / T

        x0 = [-X / 2.0 - dX, -X / 2.0, X / 2.0, X / 2.0 + dX]
        V0 = [0, V, V, 0]

        Va = Vb = np.interp(x, x0, V0)
        return (Va, Vb)


class ExperimentRotonSmall(ExperimentRoton):
    """Small versions of the Adiabticity experiment allowing control of the
    dimensionless parameters.  All parameters are either dimensionless, or
    specified in micron or ms.
    """

    t_unit = u.ms

    w = 0.475
    d = 1.36

    l_r = 1.0 / 7.9
    xi = 0.1
    T__x = 326
    T__perp = 3.6
    T__R = 0.271

    trapping_frequencies_Hz = None

    t__detuning_ramp = 10

    def init(self):
        # Compute k_r, E_R, etc.
        hbar = u.hbar
        k_r = 1.0 / self.l_r / u.micron
        T_R = self.T__R * self.t_unit
        recoil_frequency = 1.0 / T_R

        E_R = 2 * np.pi * recoil_frequency * hbar
        m = 1.0 / (2 * E_R / (hbar * k_r) ** 2)
        Ts = np.array([self.T__x, self.T__perp, self.T__perp]) * self.t_unit

        assert self.trapping_frequencies_Hz is None
        self.trapping_frequencies_Hz = 1.0 / Ts / u.Hz

        self.recoil_frequency_Hz = recoil_frequency / u.Hz

        self.rabi_frequency_E_R = 4 * self.w
        self.rabi_frequency_kHz = None

        self.detuning_kHz = None
        self.detuning_E_R = 4 * self.d

        self.ms = (m, m)

        # Set V_TF to give specified xi
        xi = self.xi * u.micron
        self.x_TF = None
        self.V_TF = ((hbar / xi) ** 2 / 2 / m,) * 2

        ExperimentRoton.init(self)
