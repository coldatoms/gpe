# ---
# jupyter:
#   jupytext:
#     formats: ipynb,py
#     text_representation:
#       extension: .py
#       format_name: light
#       format_version: '1.5'
#       jupytext_version: 1.4.0
#   kernelspec:
#     display_name: Python [conda env:_gpe]
#     language: python
#     name: conda-env-_gpe-py
# ---

import mmf_setup

mmf_setup.nbinit(theme="mmf")

# # Experimental Parameters
#
# $$
# \Omega = 0.5E_R \\
# \delta_{final} =
#   \begin{cases}
#   -1 \\
#   -2 \\
#   -5
#   \end{cases} KHz \\
# \delta_{initial} = 20 + \delta_{final}
# $$
#
# - Initial state is entirely in the $ \left|\downarrow \right> = \left|1,-1 \right> $ state
# - Initial state has a Thomas-Fermi radius between 233-237 micron.
# - Raman Lasers are ramped on at t=0 over at 10 ms time period.

# ## Experiment Set up

# +
# %pylab inline
from gpe.soc import Experiment, u
from gpe import utils


class ExperimentDetune(Experiment):
    states = ((1, -1), (1, 0))
    x_TF = 235 * u.micron
    t_unit = u.ms
    final_detuning_kHz = -1.0
    detuning_kHz = 20.0 + final_detuning_kHz

    B_gradient_mG_cm = 0  # Magnetic field gradients are on the order of 5-10 mG/cm
    recoil_frequency_Hz = 1843.0
    rabi_frequency_E_R = 0.5
    trapping_frequencies_Hz = (3.07, 278, 278)

    initial_imbalance = 0.0  # Nothing RF transfered, all in state[...][0]

    t__detuning_ramp = 10.0
    t__wait = 800.0  # Time until expansion
    t__image = 7.0

    def init(self):
        self.final_detuning = self.final_detuning_kHz * u.kHz * (2 * np.pi * u.hbar)
        self.t__final = self.t__detuning_ramp + self.t__wait
        Experiment.init(self)

        self._delta = utils.get_smooth_transition(
            [self.detuning, self.final_detuning],
            durations=[0.0, self.t__wait],
            transitions=[self.t__detuning_ramp],
        )

    def delta_t_(self, t_):
        return self._delta(t_)


# -

# ## Create a state

# +
# %pylab inline --no-import-all
from gpe import utils, bec2, tube2, minimize, soc
from gpe.bec2 import u

reload(bec2)
reload(tube2)
reload(soc)

expt = ExperimentDetune(Lxyz=(800 * u.micron,), Nxyz=(2**13,))


s0 = expt.get_initial_state(cool_steps=0)

plt.plot(s0.xyz[0], abs(s0[...][0]))
plt.plot(s0.xyz[0], abs(s0[...][1]))

# m = minimize.MinimizeState(s0, fix_N=True)
# s = m.minimize(tries=100)
# plt.plot(s.xyz[0],abs(s[...][0]))
# plt.plot(s.xyz[0],abs(s[...][1]))

# -

# ## Run experiment on swan

# +
# This could take a while, depends on Nxyz

plt.semilogy(ka, abs(na))
plt.semilogy(ka, abs(nb))

# -

expt.plot_dispersion()


# +
expt = ExperimentDetune(Lxyz=(1000,))
s = expt.get_state()
s.Lxyz, s.Nxyz, expt.dir_name

r = RunDetune()
r.experiments[-1].final_detuning_kHz

# +
import runs

reload(runs)


class RunDetune(runs.RunBase):
    experiment_params = dict(
        Lxyz=[(800 * u.micron,)],
        Nxyz=[(2**5,)],
        cooling_phase=[1.0, 1 + 0.001j],
        final_detuning_kHz=[-1.0, -2.0, -5.0],
    )
    Experiment = ExperimentDetune


r = RunDetune()
r.run()
# -

import runs

reload(runs)
# runs.set_threads(2)
r = RunDetune()
s = r.simulations[0]
s.get_state(0).xyz

# ##  Run in notebook

# +
# %pylab inline --no-import-all
from gpe import utils, bec2, tube2, minimize, soc
from gpe.bec2 import u

reload(bec2)
reload(tube2)
reload(soc)

experiment = ExperimentDetune(
    State=soc.State2Tube, Lxyz=(470 * u.micron,), Nxyz=(2**8,), N=4.5e5
)

s0 = experiment.get_initial_state(cool_dt_t_scale=0.00114)
# s0.constraint = 'Nab'

# s0.plot()
# Don't fix N... this should respect mu and Rx_TF
m = minimize.MinimizeState(s0, fix_N=True)
s = m.minimize(tries=100)
s.plot()

# +
from IPython.display import clear_output, display
from pytimeode.evolvers import EvolverABM
from mmfutils.contexts import NoInterrupt

s1 = s.copy()
s1.cooling_phase = 1 + 0.000001j
# s1.ws[0] = 0.
# s1[...][1] = 0.


sig_z = []
e = EvolverABM(s2, dt=0.0005 * s.t_scale)
fig = None
with NoInterrupt() as interrupted:
    while not interrupted:
        e.evolve(200)
        na, nb = e.y.get_Ns()
        sig_z.append((nb - na) / (nb + na))
        plt.clf()
        fig = e.y.plot(fig)
        display(fig)
        clear_output(wait=True)


sig_z = np.asarray(sig_z)
# -


# # Visualizing Run data

# +
# %pylab inline
from IPython.display import clear_output
import numpy as np
import scipy as sp
import scipy.signal
from mmfutils.plot import imcontourf
from gpe.utils import Simulation2
from soc_detuning_ramp import u, ExperimentDetune


expt0 = ExperimentDetune(cooling_phase=1.0, t__barrier=0)
sim0 = Simulation2(
    expt0,
    image_ts_=[
        0,
        10,
        20,
        40,
        60,
        80,
        100,
        150,
        200,
        250,
        300,
        350,
        400,
        450,
        500,
        550,
        600,
        650,
        700,
        750,
        800,
        810,
    ],
)

expt10 = ExperimentCatchAndRelease(cooling_phase=1.0, t__barrier=10)
sim10 = Simulation2(
    expt10,
    image_ts_=[
        10,
        11,
        12,
        13,
        14,
        15,
        16,
        17,
        18,
        19,
        20,
        21,
        22,
        23,
        24,
        25,
        20,
        30,
        35,
        40,
    ],
)

sim, expt = sim0, expt0

A = 8.85
delta = 1.116 * u.micron

ts = np.array(sim.image_ts_)
xs = expt.get_state().xyz[0]


def image(t_, **kw):
    args = dict(Ny=100, pixel_size=delta)
    args.update(kw)
    state = sim.get_state(t_=t_, image=True)
    xs, ns = sim.experiment.simulate_image(state, **args)
    return xs, ns


Xs = image(10)[0]
ns = np.array([image(_t)[1].sum(axis=0) for _t in ts])
clear_output()
