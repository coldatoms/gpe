# -*- coding: utf-8 -*-
# ---
# jupyter:
#   jupytext:
#     formats: ipynb,py
#     text_representation:
#       extension: .py
#       format_name: light
#       format_version: '1.5'
#       jupytext_version: 1.4.0
#   kernelspec:
#     display_name: Python [conda env:_gpe]
#     language: python
#     name: conda-env-_gpe-py
# ---

# + init_cell=true
import mmf_setup

mmf_setup.nbinit()
# -

# # Spin-Orbit Coupled BECs

# Here we describe simulations that model Peter Engels's experiments.  They study a tightly confined two-component BEC with the $\ket{1,-1}$ and $\ket{1,0}$ states of $^{87}$Rb in the presence of counter-flow and spin-orbit coupling (SOC).  The Hamiltonian for the system is
#
# The Hamiltonian is:
#
# $$
#   \I\hbar\partial_t
#   \begin{pmatrix}
#     \psi_a\\
#     \psi_b
#   \end{pmatrix}
#   =
#   \begin{pmatrix}
#     \frac{-\hbar^2 \nabla^2}{2m} + V_a - \mu - \frac{\delta}{2} + g_{aa}n_a + g_{ab}n_b & \frac{\Omega}{2}e^{2\I k_r x} \\
#     \frac{\Omega}{2}e^{-2\I k_r x} & \frac{-\hbar^2 \nabla^2}{2m} + V_b - \mu + \frac{\delta}{2} + g_{ab}n_a + g_{bb}n_b
#   \end{pmatrix}
#   \begin{pmatrix}
#     \psi_a\\
#     \psi_b
#   \end{pmatrix}.
# $$

# ## Rotating Phase Basis (RPB)

# We start by performing a phase rotation:
#
# $$
#   \begin{pmatrix}
#     \tilde{\psi}_a\\
#     \tilde{\psi}_b
#   \end{pmatrix}
#   =
#   e^{-\I\vect{k}_r x \mat{\sigma}_z}
#   \begin{pmatrix}
#    \psi_a\\
#    \psi_b
#   \end{pmatrix}
#   =
#   \begin{pmatrix}
#     e^{-\I \vect{k}_r x}\psi_a\\
#     e^{\I \vect{k}_r x}\psi_b
#   \end{pmatrix}
# $$
#
# so that
#
# $$
#   \I\hbar\partial_t
#   \begin{pmatrix}
#     \tilde{\psi}_a\\
#     \tilde{\psi}_b
#   \end{pmatrix}
#   =
#   \begin{pmatrix}
#     \frac{\hbar^2 (-\I\vect{\tilde{\nabla}} + \vect{k}_r)^2}{2m} + V_a - \mu - \frac{\delta}{2} + g_{aa}n_a + g_{ab}n_b & \frac{\Omega}{2} \\
#     \frac{\Omega}{2} & \frac{\hbar^2 (-\I\vect{\tilde{\nabla}} - \vect{k}_r)^2}{2m} + V_b - \mu + \frac{\delta}{2} + g_{ab}n_a + g_{bb}n_b
#   \end{pmatrix}
#   \begin{pmatrix}
#     \tilde{\psi}_a\\
#     \tilde{\psi}_b
#   \end{pmatrix}.
# $$
#
# so that

# $$
#   \I\hbar\partial_t
#   \tilde{\Psi}
#   =
#   \left[
#     \frac{\hbar^2\left(
#     -\I\mat{1}\tilde{\nabla} + \vect{k}_r\mat{\sigma}_z
#   \right)^2}{2m}
#   -
#   \mu\mat{1}
#   -
#   \delta\frac{\mat{\sigma}_z}{2}
#   +
#   \Omega\frac{\mat{\sigma}_x}{2}
#   \right]\tilde{\Psi}
#   =
#   \left[
#     \left(
#       \frac{-\hbar^2\tilde{\nabla}^2 + k_r^2}{2m}
#       -
#       \mu
#     \right)\mat{1}
#     -
#     \left(
#       \delta
#       -
#       \frac{2\hbar^2(-\I\vect{\tilde{\nabla}})\cdot\mat{k}_r}{m}
#     \right)
#     \frac{\mat{\sigma}_z}{2}
#     +
#     \Omega\frac{\mat{\sigma}_x}{2}
#   \right]\tilde{\Psi}\\
#   \mu \equiv \mu - \frac{V_a + V_b + (g_{aa} + g_{ab})n_a + (g_{ab} + g_{bb})n_b}{2}, \qquad
#   \frac{\delta}{2} \equiv \frac{\delta}{2} - \frac{V_a - V_b + (g_{aa} - g_{ab})n_a + (g_{ab} - g_{bb})n_b}{2}.
# $$
#
# In case of  of equal couplings, the latter expression is just the detuning.  Note that the derivatives in this expression apply to states in the RPB, not to the original states.
#
# To avoid any complications (these fictitious phases must be carefully applied to initial states and currents for example) we do not use this transform in the actual code, however, it is very useful for analysis.

# ## Galilean Transformations

# Here we consider the physics of this system in a frame moving with velocity $v$ along the $x$ axis.  Thus, a point $x_v$ in the moving frame has position $x = x_v + vt$ in the lab frame.
#
# We start with the statement of Galilean covariance for a single-component system with external potential $V(\vect{x})$:
#
# $$
#   \I\hbar \dot{\psi}(x, t) = \left(\frac{-\hbar^2\nabla^2}{2m} + V(x)\right)\psi(x, t).
# $$
#
# The Galilean boost should provide a wavefunction $\psi_v(x_v, t)$ such that
#
# $$
#   \I\hbar \dot{\psi}_v(x_v, t) = \left(\frac{-\hbar^2\nabla_v^2}{2m} + V(x_v + vt)\right)\psi_v(x_v, t).
# $$
#
# A quick calculation shows that this holds under the following Galilean transformation:
#
# $$
#   \psi_{v}(x_v, t) = e^{-\I\phi}\psi(x_v+vt, t), \qquad
#   \hbar\phi = mvx_v + \frac{1}{2}mv^2t.
# $$

# In our system, the corresponding transformation gives:
#
# $$
#   \Psi_v(x_v, t) = e^{-\I \phi \mat{1}}\Psi(x+vt, t), \qquad
#   \hbar\phi = mvx_v + \frac{1}{2}mv^2 t, \qquad
#   \I\hbar\dot{\Psi}_v(x_v, t) = \mat{H}_v\Psi_v(x_v, t), \\
#   \mat{H}_v =
#   \begin{pmatrix}
#     \frac{\hbar^2 k^2}{2m} + V_a(x_v + vt) & \frac{\Omega}{2}e^{2\I k_R (x_v+vt)}\\
#     \frac{\Omega}{2}e^{-2\I k_R (x_v+vt)} & \frac{\hbar^2 k^2}{2m} + V_b(x_v + vt)
#   \end{pmatrix}
# $$
#
# Thus, one can again define a transformed state to remove the spatial dependence of the off-diagonal terms, adding a term to the Schrödinger equation which will adjust the detuning:
#
# $$
#   \begin{aligned}
#     \Psi_v(x_v, t) &= e^{\I k_R (x_v + vt) \mat{\sigma}_z} \tilde{\Psi}_v(x_v, t), \\
#      \I\hbar\partial_t \Psi_v(x_v, t) &=
#      e^{\I k_R (x_v + vt) \mat{\sigma}_z} (\I\hbar\partial_t - \hbar k_R v \mat{\sigma}_z)\tilde{\Psi}(x_v, t),
#   \end{aligned}\\
#   \I\hbar\dot{\tilde{\Psi}}(x_v, t) = \mat{\tilde{H}}_v\tilde{\Psi}(x_v, t), \qquad
#   \mat{\tilde{H}}_v =
#   \begin{pmatrix}
#     \frac{\hbar^2 (k+k_R)^2}{2m} + V_a(x_v + vt) + \hbar k_R v & \frac{\Omega}{2}\\
#     \frac{\Omega}{2} & \frac{\hbar^2 (k-k_R)^2}{2m} + V_b(x_v + vt) - \hbar k_R v
#   \end{pmatrix}.
# $$
# In other words, the detuning depends on the velocity of the local frame:
#
# $$
#   \delta_{\vect{v}} = \delta - 2\hbar k_R v, \qquad d_v = d + 2v
# $$
#
# where $v$ is measured in units of $\hbar k_R/m = 1$.

# ## Accelerating Frame

# We start by considering a primitive transformation to an accelerating frame:
#
# $$
#   \vect{x} = \vect{x}_v + \vect{x}_0(t), \qquad
#   \psi'[\vect{x}_v, t] = \psi[\vect{x}_v + \vect{x}_0(t), t], \\
#   \I\hbar\partial_t \psi'(\vect{x}_v, t) =
#   \I\hbar\partial_t\psi[x_v + x_0(t), t] +
#   \I\hbar\dot{\vect{x}}_0(t)\cdot\vect{\nabla}\psi[x_v + x_0(t), t].
# $$
#
# Gradients are not affected by this transformation, but the time-dependence of the coordinate change induces an additional piece that must be added to the Hamiltonian:
#
# $$
#   \I\hbar\partial_t \psi'(\vect{x}_v, t) = \Bigl(
#     \op{H} - \dot{\vect{x}}_0(t)\cdot\vect{\op{p}}
#   \Bigr)\psi'(\vect{x}_v, t).
# $$
#
# This is a valid and practical implementation of the transformation to an accelerating references, but is not the usual form of a Galilean transformation.  We present it here as it will correspond to the transformation of type (B) considered below.  Now we consider the more conventional transformation which includes the phase modification.  We effect a similar transformation as in the previous section:
#
# $$
#   x = x_v + x_0(t), \qquad
#   \psi_{v}(x_v, t) = e^{-\I\phi}\psi(x_v + x_0(t), t), \qquad
#   \hbar\phi = m\dot{x}_0(t)x_v + \int_0^{t}\frac{m\dot{x}_0^2(t)}{2}\d{t},\\
#   \I\hbar \dot{\psi}_v(x_v, t)
#   = \left(\frac{-\hbar^2\nabla_v^2}{2m} + V\bigl(x_v + x_0(t)\bigr) + m\ddot{x}_0(t)x_v\right)\psi_v(x_v, t).
# $$
#
# Here the only difference is that we pick up an additional linear potential corresponding to the fictitious force from the acceleration.

# Applying this to the two-component system we have
# $$
#   x_v(t) = x - x_0(t), \qquad
#   \Psi(x, t) = \exp\left\{
#     \frac{\left[mx_v\dot{x}_0(t) + \int_0^{t}\frac{m\dot{x}_0^2(t)}{2}\d{t}\right]\mat{1}
#     - \hbar k_R [x_v + x_0(t)]\mat{\sigma}_{z}}{\I\hbar}
#   \right\}
#   \tilde{\Psi}_{v}(x_v, t)\\
#   \I\hbar \dot{\tilde{\Psi}}_{v}(x_v, t)
#   = \mat{\tilde{H}}_v \tilde{\Psi}_{v}(x_v, t)\\
#   \mat{\tilde{H}}_v =
#   \begin{pmatrix}
#     \frac{\hbar^2 (k+k_R)^2}{2m} + V_a\bigl(x_v + x_0(t)\bigr) + mx_v\ddot{x}_0(t)  + \hbar k_R \dot{x}_0(t) & \frac{\Omega}{2}\\
#     \frac{\Omega}{2} & \frac{\hbar^2 (k-k_R)^2}{2m} + V_b\bigl(x_v + x_0(t)\bigr) + mx_v\ddot{x}_0(t) - \hbar k_R \dot{x}_0(t)
#   \end{pmatrix}.
# $$

# ### Moving Bucket/Detuning Ramp Equivalence.

# From the previous discussion, we see that modulating the detuning $\delta(t)$ with potentials $V[x]$ is equivalent to moving to a different frame with velocity $\dot{x}_0(t) = [\delta(0) - \delta_v(t)]/2\hbar k_R$ with effective potentials $V[x_v + x_0(t)] + mx_v \ddot{x}_0(t)$.  If the potential is harmonic $V(x) = m\omega^2x^2/2$, then:

# $$
#   V[x_v + x_0(t)] + m x_v \ddot{x}_0(t) =
#   \frac{m\omega^2}{2}[x_v + x_0(t)]^2 + m[x_v + x_0(t)] \ddot{x}_0(t) - m x_0(t) \ddot{x}_0(t) =
#   \frac{m\omega^2}{2}\left[
#     x_v + x_0(t) + \frac{\ddot{x}_0(t)}{\omega^2}
#   \right]^2 - \frac{m\ddot{x}^2_0(t)}{2\omega^2} - m x_0(t) \ddot{x}_0(t)
# $$

# Reversing this, suppose we have a harmonic potential moving with position $y_0(t)$ so that in the lab frame, the system has $V[x - y_0(t)]$.  We can cancel the effect of this motion boosting to a frame with position $x_0(t)$ such that
#
# $$
#   x_0(t) + \frac{\ddot{x}_0(t)}{\omega^2} = y_0(t).
# $$
#
# Similarly, this system can be simulated by manipulating the detuning as:
#
# $$
#   \delta(t) = \delta_0 - 2\hbar k_R \dot{x}_0(t).
# $$

# ### Classical Transformation

# #### (L) Lab Frame: $H(x, p)$

# It will be useful to consider the classical analogue for the motion of the center of mass of the cloud.  To do this, we formulate the classical problem with an arbitrary dispersion:
#
# $$
#   H(x, p) = E_d[p] + V[x].
# $$
#
# We shall call this the "Lab Frame" (L), and the dispersion here may have some detuning $d$.

# #### (A) Moving Frame A: $H_v(x_v, p_v)$

# It is useful to consider this system in a moving frame with coordinate $x_v = x - x_0(t)$ which can be effected using the following generating function $G(p_v, x)$ for a Canonical transform:
#
# $$
#   G(p_v, x) =  p_v[x - x_0(t)] + m x \dot{x}_0(t), \\
#   x_v = \pdiff{G}{p_v} = x - x_0(t), \qquad
#   p = \pdiff{G}{x} = p_v + m\dot{x}_0(t),\\
#   H'(x_v, p_v) = H + \pdiff{G}{t}
#   = E_d[p_v + m \dot{x}_0(t)] - p_v\dot{x}_0(t) + V[x_v + x_0(t)] + m [x_v + x_0(t)] \ddot{x}_0(t),
# $$
#
# The form of this transform is fixed by the condition that $x_v = x - x_0(t)$ and the condition that one usually wants $p_v = p - m\dot{x_0}(t)$.
#
# *Note: The latter condition refers to some mass $m$ which has no meaning in the general problem.  Thus a better approach may be to simply take $G(p_v, x) =  p_v[x - x_0(t)]$ in which case $p_v = p$ is the momentum in the lab frame.  We consider this below for frame (B).  This is consistent with the general problem which does not display Galilean covariance.  However, we will be comparing with a system that has Galilean covariance in which $m$ is defined, so we use the augmented transform here.*

# Now we consider a harmonic potential $V(x) = m\omega^2x^2/2$.  In this case we find the following Hamiltonian in the moving frame:
#
# $$
#   H_v(x_v, p_v) = E_d[p_v + m \dot{x}_0(t)] - p_v\dot{x}_0(t)
#   + \frac{m\omega^2}{2}\left[x_v + x_0(t) + \frac{\ddot{x}_0(t)}{\omega^2}\right]^2
#   - \frac{m\ddot{x}_0^2(t)}{2\omega^2}.
# $$

# Now, if we choose $\delta(t) = 2\hbar k_R\dot{x}_0(t)$ and express this in dimensionless coordinates with units of $\hbar = k_R = 2E_R = 1$ so that $d = \delta/4E_R$, then we have:
#
# $$
#   d = \dot{\tilde{x}}_0, \qquad
#   y_0(t) = x_0(t) + \frac{\ddot{x}_0(t)}{\omega^2},\\
#   \tilde{H}_{v}(\tilde{x}_v, \tilde{k}_v) =
#   \tilde{E}_d(\tilde{k}_v + d) - \tilde{k}_vd + \frac{\tilde{\omega}^2}{2}[\tilde{x}_v + \tilde{y}_0(t)]^2 - \text{const}
#   = \tilde{E}_{0}(\tilde{k}_v) + \frac{\tilde{\omega}^2}{2}[\tilde{x}_v + \tilde{y}_0(t)]^2 - \text{const}.
# $$

# Here we note that the boosted dispersion $\tilde{E}_d(\tilde{k}_v + d) - \tilde{k}_vd = \tilde{E}_0(\tilde{k}_v) + \text{const}$.  In other-words, by boosting to a frame with $\dot{x}_0=\delta(t)/2\hbar k_R$, we have removed the effects of the detuning:
#
# $$
#   E_{d}(k+d) - kd = \frac{(k+d)^2+1}{2} \pm \sqrt{(k+d-d)^2+w^2} - kd\\
#                   = \frac{k^2+1}{2} \pm \sqrt{k^2+w^2} + \frac{d^2}{2}
#                   = E_{0}(k) + \frac{d^2}{2}.
# $$
#
# Thus:
#
# $$
#   E_{d}(k+d) = E_{0}(k) + kd + \frac{d^2}{2}, \quad
#   E_{d}(k) = E_{0}(k-d) + kd - \frac{d^2}{2}\\
# $$

# We shall refer to this transformation as Moving Frame (A): $H_v(x_v, p_v)$.

# #### (B) Moving Frame: $H_v'(x_v, p)$

# An alternative canonical transformation is to drop the $mx\dot{x}_0(t)$ piece from the generator $G(p_v, x)$:
#
# $$
#   G(p_v, x) = p_v[x - x_0(t)], \qquad
#   x_v = x - x_0(t), \qquad p_v = p, \qquad
#   H'_v(x_v, p) = E_d[p] - p\dot{x}(t) + V[x_v + x_0(t)].
# $$
#
# We shall refer to this transformation as Moving Frame (B): $H'_v(x_v, p)$.

# ### Summary

# #### Lab Frame (L)
#
# $$
#   H(x, k)=E_d(k)+V(x),\\
#   \dot{x} = E_d'(k), \qquad
#   \dot{k} = -V'(x).
# $$
#
# #### Moving Frame (A)
#
# $$
#   x_v = x - x_0, \qquad
#   k_v = k - m\dot{x}_0\\
#   H_v(x_v, k_v) = E_0(k_v) + V(x_v + x_0) + (x_v + x_0) \ddot{x}_0,\\
#   \dot{x}_v = E_0'(k_v), \qquad
#   \dot{k}_v = -V'(x_v + x_0) - \ddot{x}_0.
# $$
#
# #### Moving Frame (B)
#
# $$
#   x_v = x - x_0, \qquad
#   k_v = k,\\
#   H_v'(x_v, k) = E_d(k) - k\dot{x}_0 + V(x_v + x_0),\\
#   \dot{x}_v = E_d'(k) - \dot{x}_0, \qquad
#   \dot{k} = -V'(x_v + x_0).
# $$

# ### Stationary Solutions

# *(To simplify the notations, we drop the tildes here: everything is dimensionless.)*
#
# To test this, we consider the stationary solution or ground state at finite $d$ in the lab frame.  This has momentum $k=k_0$ such that $E'_{d}(k_0) = 0$.  Here are the corresponding solutions in the three frames:
#
# | Lab Frame (L)             | Moving Frame (A)              | Moving Frame (B)                        |
# |---------------------------|-------------------------------|-----------------------------------------|
# | $H(x, k)=E_d(k)+V(x)$ | $H_v(x_v,k_v)=E_0(k_v)+V(x_v+dt)$ | $H'_v(x_v,k) = E_d(k) - kd + V(x_v+dt)$ |
# | $x=0$                 | $x_v = x - dt$                    | $x_v = x - dt$                          |
# | $k=k_0$               | $k_v = k_0 - d$                   | $k = k_0$                               |
# | $\dot{x}=E_d'(k_0)=0$ | $\dot{x}_v=E_0'(k_v)=-d$          | $\dot{x}_v=E'_d(k_0)-d$                 |
# | $\dot{k}=-V'(0)=0$    | $\dot{k}_v=-V'(x_v-dt)=0$         | $\dot{k}_v=-V'(x_v-dt)=0$               |
#
# We have used $E_0'(k_v) = E_d'(k_v+d)-d = -d$.
#

# # Homogeneous States and the Bloch Sphere

# In the rotating phase basis, we can consider homogeneous states.  (*States with a different relative momentum will develop density oscillations.*)
#
# $$
#   \tilde{\Psi} = e^{\I \tilde{k} x}
#   \begin{pmatrix}
#     \sqrt{n_a}\\
#     \sqrt{n_b}
#   \end{pmatrix}.
# $$
#
# $$
#   \I\hbar\partial_t
#   \tilde{\Psi}
#   =
#   \left[
#     \frac{\hbar^2\left(
#     \vect{\tilde{k}}\mat{1} + \vect{k}_R\mat{\sigma}_z
#   \right)^2}{2m}
#   -
#   \bar{\mu}\mat{1}
#   -
#   \bar{\delta}\frac{\mat{\sigma}_z}{2}
#   +
#   \Omega\frac{\mat{\sigma}_x}{2}
#   \right]\tilde{\Psi}
#   =
#   \overbrace{
#   \left[
#     \left(
#       \frac{\tilde{k}^2 + k_R^2}{2m}
#       -
#       \bar{\mu}
#     \right)\mat{1}
#     -
#     \left(
#       \bar{\delta}
#       -
#       \frac{2\hbar^2\vect{\tilde{k}}\cdot\mat{k}_R}{m}
#     \right)
#     \frac{\mat{\sigma}_z}{2}
#     +
#     \Omega\frac{\mat{\sigma}_x}{2}
#   \right]}^{\mat{H}}
#   \tilde{\Psi}\\
#   \bar{\mu} \equiv \mu - \frac{(g_{aa} + g_{ab})n_a + (g_{ab} + g_{bb})n_b}{2}, \qquad
#   \frac{\bar{\delta}}{2} \equiv \frac{\delta}{2} - \frac{(g_{aa} - g_{ab})n_a + (g_{ab} - g_{bb})n_b}{2},\\
#   \frac{\mat{H}}{\hbar} = \omega_0\mat{1} + \vect{\omega}\cdot\frac{\vec{\mat{\sigma}}}{2}, \qquad
#   \hbar\omega_0 = \frac{\hbar^2(\tilde{k}^2 + k_R^2)}{2m} - \bar{\mu} , \qquad
#   \hbar\vec{\omega} = \begin{pmatrix}
#     \Omega\\
#     0\\
#     \frac{2\hbar^2\vect{\tilde{k}}\cdot\mat{k}_R}{m} - \bar{\delta}
#   \end{pmatrix}.
# $$
#
# In the case of equal couplings, the latter expression is just the detuning.
#
# $$
#   \bar{\mu} \equiv \mu - gn, \qquad
#   \bar{\delta} \equiv \delta.
# $$

# The Hamiltonian thus induces the following rotations on the Bloch sphere:
#
# $$
#   \tilde{\Psi}_{t} = e^{-\I t \omega_0}\exp\left(-\I t \vec{\omega}\cdot \frac{\vec{\mat{\sigma}}}{2}\right)\tilde{\Psi}_0.
# $$
#
# The overall phase evolution can be removed by subtracting an appropriate constant chemical potential
#
# $$
#   \mu = \bar{\mu} + gn = \frac{\tilde{k}^2 + k_r^2}{2m} + gn,
# $$
#
# but has no physical consequence.  To understand the behaviour of these systems, we construct the density matrix.  Recall that:
#
# $$
#   \mat{\sigma}_i \cdot\mat\sigma_{j} = \delta_{ij}\mat{1} + \I\epsilon_{ijk}\mat{\sigma}_{k}, \qquad
#   \Tr\mat{\sigma}_{i}\cdot\mat{\sigma}_{j} = 2\delta_{ij}, \\
#   \mat{\sigma}_i \cdot\mat\sigma_{j}\cdot\mat\sigma_{k}
#   = \I e_{ijk}\mat{1} + (d_{ij}s_k - d_{ik}s_j + d_{jk}s_i) \qquad
#   \mat{\sigma}_i \cdot\mat\sigma_{j}\cdot\mat\sigma_{k}\cdot\mat\sigma_{l}
#   = (d_{ij}d_{kl} - d_{il}d_{jk} + d_{ik}d_{jl})\mat{1}
#   + \I (d_{ij}e_{lkn} + d_{lk}e_{ijn} - d_{ln}e_{ijk} + d_{kn}e_{ijl})\mat{\sigma}_n\\
#   \Tr(\mat{\sigma}_i \cdot\mat\sigma_{j}\cdot\mat\sigma_{k}) = 2\I e_{ijk}, \qquad
#   \Tr(\mat{\sigma}_i \cdot\mat\sigma_{j}\cdot\mat\sigma_{k}\cdot\mat\sigma_{l})
#   = 2(d_{ij}d_{kl} - d_{il}d_{jk} + d_{ik}d_{jl})
# $$
#
# The state can thus be parametrized using the density matrix:
#
# $$
#   \mat{\rho} = \ket{\tilde{\Psi}}\bra{\tilde{\Psi}}
#   = \frac{1}{2}\left(\mat{1} + \vec{a}\cdot\vec{\mat{\sigma}}\right).
# $$
#
# Note that this satisfies the requisite properties of normalization $\Tr\mat{\rho} = 1$ and hermiticity $\mat{\rho} = \mat{\rho}^\dagger$.  To represent a pure state we must also have $\mat{\rho}^2 = \mat{\rho}$ which requires $\norm{\vec{a}}_2 = 1$.  The effect of the Hamiltonion is to rotate the vector $\vec{a}\rightarrow\vec{a}'$ under SO(3):
#
# $$
#   a'_i = \frac{a_j}{2}\Tr\left[\mat{\sigma}_{i}\cdot
#   \exp\left(-\I t \vec{\omega}\cdot \frac{\vec{\mat{\sigma}}}{2}\right)
#   \cdot\mat{\sigma}_j
#   \cdot\exp\left(\I t \vec{\omega}\cdot \frac{\vec{\mat{\sigma}}}{2}\right)
#   \right], \qquad
#   \vec{a}' = e^{t\vec{\omega}\times}\cdot\vec{a}.
# $$
#
# Stationary states of the system thus correspond to $\vec{a} \propto \vec{\omega}$.

# In dimensionless units we have:
#
# $$
#   \frac{\vec{\omega}}{4E_R} = \begin{pmatrix}
#     w\\
#     0\\
#     \tilde{k} - d
#   \end{pmatrix}
# $$
#
# Thus, the pure states have
#
# $$
#   \vec{a}_{\pm} = \frac{\pm 1}{D}
#   \begin{pmatrix}
#     w\\
#     0\\
#     \tilde{k} - d
#   \end{pmatrix}, \qquad
#   D = \sqrt{(\tilde{k}-d)^2 + w^2},
# $$
#
# with the ground state having the lower sign.

# From these we can immediately express the spin-quasi-momentum mapping:
#
# $$
#   \frac{n_a}{n_b} = \frac{\rho_{00}}{\rho_{11}}
#   = \frac{\tfrac{1}{2}(1 + a_z)}{\tfrac{1}{2}(1 - a_z)}
#   = \frac{1 - K}{1 + K}, \qquad
#   K = \frac{\tilde{k}-d}{D} = \frac{\tilde{k}-d}{\sqrt{(\tilde{k}-d)^2 + w^2}}.
# $$

# ## Thomas-Fermi Approximation

# We construct our initial states using the Thomas-Fermi approximation.  Here we assume that locally the potential varies sufficiently slowly that gradients in the density can be neglected.

# ## Homogeneous Phases

# %pylab inline --no-import-all
k = np.linspace(-5, 5, 1000)
d = -1.0
w = 0.5 / 4
D = np.sqrt((k - d) ** 2 + w**2)
plt.plot(k, (k**2 + 1) / 2.0 - D)
plt.plot(k, (k**2 + 1) / 2.0 + D)

# We now take a brief detour to present the eigenvalues and eigenvectors of the following matrix:
#
# $$
#   \mat{M} = \begin{pmatrix}
#     A+B & C\\
#     C & A-B
#   \end{pmatrix}, \qquad
#   E_{\pm} = A \pm \overbrace{\sqrt{B^2 + C^2}}^{D},\\
#   M - E_{\pm} \mat{1} =
#   \begin{pmatrix}
#     B \mp D & C\\
#     C & -B\mp D
#   \end{pmatrix},\\
#   \frac{u_\pm}{v_\pm} = \frac{B \pm D}{C}
#   = \frac{C}{-B \pm D},\\
#   u_\pm = \frac{B \pm D}{\sqrt{2B(B\pm D) + 2C^2}},
#   \qquad v_{\pm} = \frac{C}{\sqrt{2B(B\pm D) + 2C^2}}\\
#   \frac{\abs{u_{\pm}}^2}{\abs{v_{\pm}}^2} = \frac{B\pm D}{-B\pm D}
#   = \frac{1\pm\frac{B}{D}}{1\mp\frac{B}{D}}
# $$

# +
import numpy as np

np.random.seed(1)
A, B, C = np.random.random(3) - 0.5
D = np.sqrt(B**2 + C**2)
Em, Ep = Es = A - D, A + D
M = np.array([[A + B, C], [C, A - B]])
u_p, v_p = np.divide([B + D, C], np.sqrt(2 * (B * (B + D) + C**2)))
u_m, v_m = np.divide([B - D, C], np.sqrt(2 * (B * (B - D) + C**2)))
Es_, U_ = np.linalg.eigh(M)
((u_m_, u_p_), (v_m_, v_p_)) = U_

assert np.allclose(Es, Es_)
assert np.allclose(u_m_ / v_m_, [C / (-B - D), (B - D) / C])
assert np.allclose(u_p_ / v_p_, [C / (-B + D), (B + D) / C])
assert np.allclose(u_m_ / v_m_, u_m / v_m)
assert np.allclose(u_p_ / v_p_, u_p / v_p)
# -

# In particular, for homogeneous states, we can diagonalize this Hamiltonian after the rotating the phase to obtain two branches with dispersions.  Here we express everything in terms of the energy scale $2E_R$ to make the coefficients dimensionless.  (We use $2E_R$ because in our natural units where $\hbar = m = 1$, this is $2E_R = 1$.)  We specialize these formula to the case where $\vect{k}_R = k_R\uvect{x}$:
#
# $$
#   E_R = \frac{\hbar^2 k_R^2}{2m}, \qquad \tilde{k} = \frac{k}{k_R}\\
#   A = \frac{\epsilon_{+}(\vect{k})}{2E_R} = \frac{\tilde{k}^2 + 1}{2} - \frac{\mu_+}{2E_R}, \qquad
#   B = \frac{\epsilon_{-}(\vect{k})}{2E_R} = \tilde{k} - \frac{\mu_{-}}{2E_R}, \qquad
#   C = \frac{\Omega}{4E_R}, \qquad
#   \mu_{\pm} = \frac{\mu_a \pm \mu_b}{2},
#   \\
#   \mu_{+} = \mu - \frac{V_a + V_b + g_{aa}n_a + g_{bb}n_b + g_{ab}(n_a+n_b)}{2}, \qquad
#   \mu_{-} = \frac{\delta}{2} - \frac{V_a - V_b + g_{aa}n_a - g_{bb}n_b - g_{ab}(n_a-n_b)}{2}.
# $$
#
# To touch base with previous notations such as in our negative mass paper where we assume $g_{aa} = g_{bb} = g_{ab}$, $V_a = V_b$, and $\mu_{+} = 0$ (by adjusting $\mu$), the dispersion becomes:
#
# $$
#   \mu_{-} = \frac{\delta}{2}, \qquad
#   d = \frac{\delta}{4E_R}, \qquad
#   w = \frac{\Omega}{4E_R},\\
#   \begin{aligned}
#   \tilde{E}_{\pm}(\tilde{k}) &= \frac{E_{\pm}}{2E_R}
#   = \frac{\tilde{k}^2 + 1}{2} \pm \sqrt{(\tilde{k} - d)^2 + w^2}, \\
#   \tilde{E}_{\pm}'(\tilde{k}) &= \tilde{k} \pm \frac{\tilde{k} - d}{\sqrt{(\tilde{k} - d)^2 + w^2}}\\
#   \tilde{E}_{\pm}''(\tilde{k}) &= 1 \pm \frac{w^2}{\sqrt{(\tilde{k} - d)^2 + w^2}^3}
#   \end{aligned}
# $$

# ### Spin-Quasi-momentum Mapping
#
# $$
#   K = \frac{\sgn(\tilde{k}-d)}{\sqrt{1 + \left(\frac{w}{\tilde{k}-d}\right)^2}}, \qquad
#   \frac{n_a}{n_b} = \frac{1-K}{1+K}\\
#   \frac{n_a}{n_a + n_b} = \frac{1-K}{2}, \qquad
#   \frac{n_b}{n_a + n_b} = \frac{1+K}{2}, \\
# $$

# %pylab inline --no-import-all
import gpe.soc

reload(gpe.soc)
from gpe.soc import Dispersion

E = Dispersion(w=1.5 / 4, d=0.543 / 4)
ks = np.linspace(-3, 3, 100)
l = plt.plot(ks, E(ks).T)
plt.plot(ks, E(ks, d=1).T, ls="--")
plt.plot(ks, E(ks, d=2).T, ls=":")
plt.axvline(E.get_k0(), c="y")
plt.xlabel("$k/k_r$")
plt.ylabel("$E/2E_R$")
plt.ylim(-2, 4)
plt.title("d={0.d}, w={0.w}".format(E))

# The following plot demonstrates the uniform convergence of Newton's method for finding the ground state in 5 steps.

ws = np.linspace(-2, 2, 100)[:, None]
ds = np.linspace(-2, 2, 101)[None, :]
E = Dispersion(w=ws, d=ds)
k0 = E.get_k0(N=10)
N = 100
k0_ = -np.sign(ds) / 2
Ns = 0 * ws * ds
tol = 1e-12
for n in range(N):
    assert np.allclose(np.sign(k0_), -np.sign(ds))
    err = abs(k0_ - k0)
    Ns = np.where(err > tol, n, Ns)
    k0_ = E.newton(k0_)
plt.pcolormesh(ws.ravel(), ds.ravel(), Ns.T)
plt.colorbar(label="steps")
plt.xlabel("w")
plt.ylabel("d")

# ## Ground State

# The ground state must satisfy $E'_-(\tilde{k}_0) = 0$ which implies:
#
# $$
#   \tilde{k}_0 = \frac{\tilde{k}_0 - d}{\sqrt{(\tilde{k}_0 - d)^2 + w^2}}.
# $$
#
# Note that the true minimum has sign $\sgn(k_0) = - \sgn(d)$, so we can rewrite this condition as:
#
# $$
#   \abs{\tilde{k}_0} = \frac{1}{\sqrt{1 + \left(\frac{w}{\abs{\tilde{k}_0} + \abs{d}}\right)^2}} \leq 1.
# $$

# In the rotating phase basis, the Hamiltonian is:
#
# $$
#   \frac{\mat{H}}{2E_R} =
#     \left(
#       \frac{\tilde{k}_0^2 + 1}{2}
#       -
#       \frac{\bar{\mu}}{2E_R}
#     \right)\mat{1}
#     -
#     \left(
#       d      -
#       \tilde{k}_0
#     \right)
#     \mat{\sigma}_z
#     +
#     w\mat{\sigma}_x
# $$

# The previous relationships allow us to express the two bands in the $a$, $b$ basis by noting that:
#
# $$
#   u_{\pm} = \braket{a|\pm}, \qquad v_{\pm} = \braket{b|\pm}.
# $$

# +
import numpy as np
from scipy.linalg import expm
from gpe.utils import pauli_matrices, levi_civita

np.random.random
np.random.seed(1)
a = np.random.random(3)
a /= np.linalg.norm(a)
wt = np.random.random(3) * 0.1

one = np.eye(2)


def get_rho(a):
    return (one + np.einsum("a, aij->ij", a, pauli_matrices)) / 2.0


L2 = np.einsum("a,aij->ij", wt, -1j * pauli_matrices / 2.0)
L3 = np.einsum("a,aij->ij", wt, -levi_civita[3])
U2 = expm(L2)
U3 = expm(L3)

rho = U2.dot(get_rho(a)).dot(U2.T.conj())
rho1 = get_rho(U3.dot(a))
assert np.allclose(rho, rho1)
# -

# The population of the upper/lower branches can be computed as:
#
# $$
#   n_{\pm} = \frac{1\pm K}{2}n_a + \frac{1\mp K}{2}n_b.
# $$

# Note that although this phase rotation does not affect the densities, it will affect currents, and needs to be carefully applied to, for example, initial states.  In the code, we add a property `rotating_phases` which can be set or unset, applying the appropriate transformation to the underlying state and including or removing the $\vect{k}_r$ terms from the kinetic energy.

# # BdG Analysis

# ## Single Component

# For homogeneous states we can perform a Bogoliubov-de Gennes analysis of the small amplitude modes.  We start with the GPE for a single component, but with arbitrary dispersion $E_-(\op{p})$ and energy density $\mathcal{E}(n)$.  This will be appropriate, for example, if the lower branch of the dispersion is occupied.  We include a complex phase $\eta$ to allow for dissipation (relevant for modeling the UFG for example):
#
# $$
#   \I\hbar e^{\I\eta}\dot{\Psi} = \op{H}[n]\Psi = \left(E_-(\op{p}) + \mathcal{E}'(n)\right)\Psi, \qquad
#   \op{H}[n]\Psi = \frac{\delta E[\Psi]}{\delta \Psi^\dagger}
# $$
#
# which follows from minimizing the following energy functional:
#
# $$
#   E[\Psi] = \int \left(
#     \Psi^\dagger E_-(\op{p})\Psi + \mathcal{E}(\Psi^\dagger\Psi)
#   \right)\d{x}.
# $$
#
# For reference, the usual GPE has:
#
# $$
#   E_{-}(\op{p}) = \frac{\op{p}^2}{2m}, \qquad
#   \mathcal{E}(n) = \frac{g}{2}n^2.
# $$
#
# Consider the following homogeneous solution of this generalized GPE:
#
# $$
#   \Psi_k(x,t) = \sqrt{n_0}e^{\I (kx - \omega_k t)}, \qquad
#   \hbar e^{\I\eta_k}\omega_k = \bigl(E_-(k) + \mathcal{E}'(n_0)\bigr).
# $$

# Note that this might not be the ground state if $k \neq k_0$ which minimizes $E_-(k)$, but is a homogeneous solution of the time-dependent GPE.  It represents the ground state boosted to a frame with phase velocity $k-k_0$ and group velocity $v = {E_-}'(k)$.
#
# From here we shall only consider fluctuations about this state, so we include the chemical potential shift in the Hamiltonian $\mathcal{E}'(n) \rightarrow \mathcal{E}'(n) - \mu$ where $\mu = e^{\I\eta_k}\hbar\omega_k =  E_{k} + \mathcal{E}'(n_0)$ so that the state $\Psi_k(x) = \sqrt{n_0}e^{\I kx}$ is time-independent: $\op{H}[n_0]\Psi_k = 0$.
#
# To this, we add a small perturbation of order $\delta$ so that the wavefunction has the form:
#
# $$
#   \Psi = \Psi_k + \delta \Psi_1.
# $$
#
# Furthermore, we restrict the perturbation so that it does not change the total particle number (to order $\delta$) which requires that $\Psi_1$ be orthononal to $\Psi_k$:
#
# $$
#   \delta n = \delta \Psi_1^\dagger\Psi_k + \text{h.c.} + \order(\delta^2), \qquad
#   \int \delta n = \int \delta \Psi_1^\dagger \Psi_k \d{x} = 0.
# $$
#
# The energy of this new state will be
#
# $$
#   E[\Psi] - E[\Psi_0]
#   = \int\left(\delta\Psi_1^\dagger \frac{\delta E[\Psi]}{\delta \Psi^\dagger} + \text{h.c.}\right)\d{x}
#   = \int\left(\delta\Psi_1^\dagger \op{H}[n_0 + \delta n]\Psi + \text{h.c.}\right)\d{x}\\
#   = \delta^2\int\left(
#     2\Psi_1^\dagger \op{H}[n_0]\Psi_1
#     +
#     (\Psi_1^\dagger \Psi_k + \text{h.c.})^2
#   \mathcal{E}''(n_0)
#   \right)\d{x}
#   = \delta^2\int\left(
#     2\Psi_1^\dagger \op{H}[n_0]\Psi_1
#     +
#     (\Psi_1^\dagger \Psi_k + \text{h.c.})^2
#   \mathcal{E}''(n_0)
#   \right)\d{x}
# $$
#
# We write the perturbation as:
#
# $$
#   \Psi(x,t) = e^{\I kx}\left(\sqrt{n_0} + u e^{\I(qx - \omega t)} + v^* e^{-\I(qk-\omega t)}\right)\\
#   \Psi_1 = \sqrt{n_0}\left(u e^{\I((k+q)x - \omega t)}
#   + v^* e^{\I((k-q)x + \omega t)}\right)\\
#   \delta n = 2 n_0\Re\left(
#     u e^{\I(qx - \omega t)} + v^* e^{-\I(qx - \omega t)}
#   \right)
# $$
#
# $$
# \begin{pmatrix}
#     A - \omega  & D\\
#     D & B + \omega
#   \end{pmatrix}
#   \cdot
#   \begin{pmatrix}
#   u \\
#   v
#   \end{pmatrix} = 0, \qquad
#   \omega = \frac{A-B}{2} \pm \sqrt{\frac{(A+B)^2}{4} - D^2},\\
#   \eta = \frac{A+B}{2D}, \qquad
#   \cot\theta = \frac{u}{v} = \frac{1}{-\eta \pm \sqrt{\eta^2 - 1}}
#                            = -\eta \mp \sqrt{\eta^2 - 1} = a = \frac{A-\omega}{D},\\
#   v = \sin\theta = \frac{1}{\sqrt{1+a^2}} = \frac{1}{\sqrt{\abs{2\eta a}}}\\
#   u = av = \cos\theta = \frac{a}{\sqrt{1+a^2}},\\
#   1+a^2 = 2\eta^2 \pm 2\eta\sqrt{\eta^2 - 1} = -2\eta a\\
# $$
#
# Let $u = \cos\theta$ and $v=\sin\theta$, then
#
# $$
#   \cot\theta = \frac{D}{-\frac{A+B}{2} \pm \sqrt{\frac{(A+B)^2}{4} - D^2}}
#               = \frac{-\frac{A+B}{2} \mp \sqrt{\frac{(A+B)^2}{4} - D^2}}{D}.
# $$

# +
# %pylab inline
from gpe import bec


class State(bec.State):
    def get_Vext(self):
        return 0.0


s = State(Nxyz=(8 * 4,), Lxyz=(2.0,), mu=0.5)
s0 = s.copy()
mu = s.mu
n = 3.0
k = 0
q = 2 * np.pi * n / s.Lxyz[0]
Ek = (s.hbar * k) ** 2 / 2 / s.m
A = (s.hbar * (k + q)) ** 2 / 2 / s.m - Ek + mu
B = (s.hbar * (k - q)) ** 2 / 2 / s.m - Ek + mu
D = mu
eta = (A + B) / 2.0 / D
w = (A - B) / 2.0 + np.sqrt((A + B) ** 2 / 4.0 - D**2)
w = (A - B) / 2.0 - np.sqrt((A + B) ** 2 / 4.0 - D**2)
a = (A - w) / D
eta = (A + B) / 2.0 / D
v = 1.0 / np.sqrt(abs(2 * a * eta))
u = a * v
u, v
x = s.xyz[0]

T = abs(2 * np.pi / w)

delta = 0.43392059
s[...] += (
    delta * np.exp(1j * x * k) * (u * np.exp(1j * q * x) + v.conj() * np.exp(-1j * q * x))
)

s0.plot()
s.plot()

E_phonon = s.get_energy() - s0.get_energy()
N_phonon = E_phonon / (s.hbar * w)
N_phonon

# +
from gpe.imports import *

dt = 0.1 * s.t_scale
steps = int(np.ceil(T / dt))
dt = T / steps
e = EvolverABM(s, dt=dt)
e.evolve(steps)

s.plot()
e.y.plot()
# -

with NoInterrupt() as interrupted:
    while not interrupted:
        e.evolve(10)
        plt.clf()
        e.y.plot()
        display(plt.gcf())
        clear_output(wait=True)

import numpy as np

np.random.seed(2)
A, B, D = np.random.random(3)
D /= 10
s = np.array([1, -1])
ws = (A - B) / 2.0 + s * np.sqrt((A + B) ** 2 / 4.0 - D**2)
assert np.allclose([np.linalg.det([[A - ws[n], D], [D, B + ws[n]]]) for n in [0, 1]], 0)
eta = (A + B) / 2 / D
w = ws[0]
a = -eta + np.sqrt(eta**2 - 1)
a, 1 + a**2, -2 * eta * a
u = np.sqrt(abs(1.0 / 2.0 / eta / a))
v = a * u
u, v
np.dot([[A - w, D], [D, B + w]], [u, v])

# This represents two plane waves of magnitude $\sim \delta$ with wave-vector $q$ and frequency $\omega$.  The BdG equations express the relationship between $\omega$ and $k$ in order for this to be a solution of the GPE to order $\delta$ (dropping order $\delta^2$ and higher terms).

# Once we solve this, the energy-density of the perturbed state will be:
#
# $$
#   \mathcal{E}_{\mathrm{phonon}} = \I\hbar \frac{\int\left( \Psi^\dagger E_-(\op{p})\Psi + \mathcal{E}(n)\right) \d{x}}{V}
#   =
#   \hbar\delta^2 \omega
#   \left(
#   (u^* e^{-\I(qx - \omega t)} + v e^{\I(qx-\omega t)})
#   (u e^{\I(qx - \omega t)} - v^* e^{-\I(qx-\omega t)})
#   \right)
#   =
#   \hbar\delta^2 \omega\left(\abs{u}^2 - \abs{v}^2\right)
# $$
#
# with a small perturbation of order $\delta$ with momentum $q$ and frequency $\omega$ on top of a background state of homogeneous density $n$ and wave-vector $k$.  This gives rise to the following normal modes:
#
# $$
#   \begin{pmatrix}
#     E(k+q) - E(k) + \mathcal{E}'(n) - \hbar\omega  & \mathcal{E}'(n)\\
#     \mathcal{E}'(n) & E(k-q) - E(k) + \mathcal{E}'(n) + \hbar\omega
#   \end{pmatrix}
#   \cdot
#   \begin{pmatrix}
#   u \\
#   v
#   \end{pmatrix} =
#   \begin{pmatrix}
#     A - \omega  & D\\
#     D & B + \omega
#   \end{pmatrix}
#   \cdot
#   \begin{pmatrix}
#   u \\
#   v
#   \end{pmatrix} = 0.
# $$
#
# The spectrum is given by:
#
# \begin{align}
#   0 &= -AB + (B-A)\hbar\omega + \hbar^2\omega^2 + D^2,\\
#   \hbar\omega &= \frac{A-B}{2} \pm \sqrt{\frac{(A+B)^2}{4} - D^2}
#                = E_1 \pm \sqrt{\frac{E_2}{2}\left(\frac{E_2}{2} + 2\mathcal{E}'(n)\right)},\\
#   E_1(q) &= \frac{E(k+q) - E(k-q)}{2}
#           = qE'(k) + \order(q^3)
#           = 0 + \frac{\alpha q^3}{8M_*^2c_s} + \order(q^5),\\
#   E_2(q) &= E(k+q) + E(k-q)- 2E(k)
#           = q^2E''(k) + \order(q^4)
#           = \frac{q^2}{M_*} + \frac{\beta q^4}{4M_*^3c_s^2} + \order(q^6),
# \end{align}
#
# Here we have included the following characterization of the non-quadratic terms:
#
# $$
#   E(k+q) = E_0 + \frac{q^2}{2M_*} + \frac{\alpha q^3}{8c_sM_*^2}
#   + \frac{\beta q^4}{8c_s^2M_*^3}
#   + \order(q^4), \\
#   c_s = \sqrt{\frac{gn_0}{M_*}} = \sqrt{\frac{\mu}{M_*}}.
# $$

# ### Dimensionless Characterization

# As when discussing the single-particle dispersions, we use $2E_R$ as a natural units for $\hbar\omega$.  It is also useful to introduce the speed $c_R = \hbar k_R / M_*$ obtaining the following dimensionless relationships:
#
# $$
#   \tilde{\omega}(k) = \frac{\hbar\omega(kk_R)}{2E_R}
#   = \tilde{E}_- \pm \sqrt{\tilde{E}_+\bigl(\tilde{E}_+ + 2\tilde{\mu}\bigr)},\\
#   \tilde{E}_{\pm} = \frac{E_{\pm}}{2E_R}, \qquad
#   \tilde{\mu} = \frac{\mathcal{E}'(n)}{2E_R} = \frac{\mu}{2E_R}.
# $$

# The sign of the overall energy is given by the expression
#
# $$
#   \begin{pmatrix}
#     u^* & v^*
#   \end{pmatrix}
#   \begin{pmatrix}
#     A & D\\
#     D & B
#   \end{pmatrix}
#   \begin{pmatrix}
#     u\\
#     v
#   \end{pmatrix}
#   =
#   \omega (\abs{u}^2 - \abs{v}^2)
# $$
#
# The eigenvalues of this matrix are:
#
# $$
#   E_+ \pm \sqrt{E_-^2 + D^2}
# $$
#
# hence the condition for energetic stability is that $E_+ > - \sqrt{E_-^2+D^2}$.

# ### Non-Quadratic Dispersion

# In the ground state of the lower band, we have
#
# $$
#   E(k) = E_0 + \frac{(k-k_0)^2}{2m^*} + C (k-k_0)^3 + \order(k-k_0)^4\\
#   \frac{1}{m^*} = E''(k_0), \qquad
#   C = \frac{E'''(k_0)}{3!}.
# $$
#
# The BdG spectrum about $k=k_0$ then simplifies to:
#
# $$
#   \omega = \pm cq\left(1 + \frac{q^2}{2m_*}\frac{1}{4m_*c^2}\right) + C q^3 + \order(q^5), \qquad
#   c = \sqrt{\frac{gn}{m_*}}, \qquad
# $$
#
# $$
#   E(k) = \frac{k^2+1}{2} - D, \qquad
#   D = \sqrt{(k-d)^2 + w^2}\\
#   E'(k) = k - K(k), \qquad
#   K(k) = D'(k) = \frac{k-d}{D}\\
#   E''(k) = 1 - K'(k) = 1 - \frac{w^2}{D^3}\\
#   E'''(k) = 3\frac{w^2(k-d)}{D^5} = 3\frac{w^2}{D^4}K(k)\\
# $$
#
# In the ground state, $K(k_0) = k_0$ and $D_0 = D(k_0) = (k_0-d)/k_0$, so
#
# $$
#   E''(k_0) = \frac{1}{m_*} = 1 - \frac{w^2}{D_0^3}\\
#   E'''(k_0) = C = 3\frac{w^2k_0}{D_0^4}
#   = \frac{3}{D_0}\left(1 - \frac{1}{m_*}\right)\\
# $$

# ## Two Components

# We now consider the two-component theory after applying the rotating phase transformation.  As discussed above, the homogeneous states are described by a single quasi-momentum $k$ and can be perturbed as follows:
#
# $$
#   \tilde{\Psi} = e^{\I k x}\left[
#   \Psi_0
#   +
#   \delta
#   \Psi_1
#   \right]
#   = e^{\I k x}\left[
#   \overbrace{
#     \begin{pmatrix}
#       \sqrt{n_a}\\
#       \sqrt{n_b}
#     \end{pmatrix}
#   }^{\Psi_0}
#   + \delta \overbrace{
#     (U e^{\I(qx - \omega t)}
#     + V^* e^{-\I(qx-\omega t)})}^{\Psi_1}
#   \right].
# $$
#
# $$
#   \I\hbar\partial_t
#   \tilde{\Psi}
#   =
#   \left[
#     \frac{\hbar^2\left(
#     -\I\mat{1}\nabla + \vect{k}_r\mat{\sigma}_z
#   \right)^2}{2m}
#   -
#   \mu\mat{1}
#   -
#   \delta\frac{\mat{\sigma}_z}{2}
#   +
#   \Omega\frac{\mat{\sigma}_x}{2}
#   \right]\tilde{\Psi}
#   =
#   \left[
#     \left(
#       \frac{-\hbar^2\nabla^2 + k_r^2}{2m}
#       -
#       \mu
#     \right)\mat{1}
#     -
#     \left(
#       \delta
#       -
#       \frac{2\hbar^2(-\I\vect{\nabla})\cdot\mat{k}_r}{m}
#     \right)
#     \frac{\mat{\sigma}_z}{2}
#     +
#     \Omega\frac{\mat{\sigma}_x}{2}
#   \right]\tilde{\Psi}\\
#   \mu \equiv \mu - \frac{V_a + V_b + (g_{aa} + g_{ab})n_a + (g_{ab} + g_{bb})n_b}{2}, \qquad
#   \frac{\delta}{2} \equiv \frac{\delta}{2} - \frac{V_a - V_b + (g_{aa} - g_{ab})n_a + (g_{ab} - g_{bb})n_b}{2}.
# $$
#
# In case of  of equal couplings, the latter expression is just the detuning.
#
# To avoid any complications (these fictitious phases must be carefully applied to initial states and currents for example) we do not use this transform in the actual code, however, it is very useful for analysis.
