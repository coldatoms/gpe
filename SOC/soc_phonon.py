import numpy as np
import scipy.interpolate

sp = scipy

from gpe import utils
from gpe.soc import Experiment, u

__all__ = ["ExperimentPhonon"]


class ExperimentPhonon(Experiment):
    """Phonon Explorer.

    Here we start with a uniform gas, then we turn on a cosine potential with a
    fixed wavelength and moving at a fixed speed to see if this will generate
    phonons.

    Attributes
    ----------
    barrier_height : float
       Barrier height.
    barrier_k : float
       Specifies the width of the barrier (1/k) or the wavevector of
       the cosine modulation.
    barrier_type : 'gaussian', 'cos'
       Type of barrier.

    drive_type : 'gaussian', 'cos'
       Continuous drive or pulse.
    drive_w : float
       Frequency of drive or width of pulse.
    drive_t0_w : float
       When to start the gaussian drive (the initial signal will be
       less than exp(-(drive_t0_w*w)**2/2))

    """

    t_unit = u.ms

    barrier_type = "gaussian"  # Type of barrier
    barrier_k_k_r = 1.0  # Barrier lattice momentum
    barrier_height_V_TF = 0.1  # Depth of barrier in units of mu

    drive_type = "gaussian"  # Type of driving term
    drive_w_E_R = 2.0  # Drive width/frequency in units of E_R/hbar
    drive_t0_w = 7.0  # 7 implies 1e-11 suppression.

    B_gradient_mG_cm = 0  # No counterflow

    # Dimensionless parameters specifying dispersion.
    w = 0.475
    d = 1.36

    # These are used to set k_r, the mass, and the density.
    l_r_micron = 1.0 / 7.9  # Length associated with k_r
    T__R = 0.271  # Period associated with E_R
    xi_micron = 0.026  # Healing length

    cells_x = 100
    dx = 0.05 * u.micron

    T__x = np.inf  # No trapping potential.
    T__perp = 3.6
    trapping_frequencies_Hz = None

    t__image = 10
    t__final = 10

    initial_imbalance = None  # Start in true ground state.

    def init(self):
        # Compute k_r, E_R, etc.
        hbar = u.hbar
        l_r = self.l_r_micron * u.micron
        k_r = 1.0 / l_r
        T_R = self.T__R * self.t_unit
        recoil_frequency = 1.0 / T_R

        E_R = 2 * np.pi * recoil_frequency * hbar
        m = 1.0 / (2 * E_R / (hbar * k_r) ** 2)
        Ts = np.array([self.T__x, self.T__perp, self.T__perp]) * self.t_unit

        assert self.trapping_frequencies_Hz is None
        self.trapping_frequencies_Hz = 1.0 / Ts / u.Hz

        self.recoil_frequency_Hz = recoil_frequency / u.Hz

        self.rabi_frequency_E_R = 4 * self.w
        self.rabi_frequency_kHz = None

        self.detuning_E_R = 4 * self.d
        self.detuning_kHz = None

        self.ms = (m, m)

        self.drive_w = self.drive_w_E_R * E_R / hbar

        # Set mu to give specified xi.  To do this, we need to estimate the
        # minimum of the dispersion relationship so we first need to create a
        # state, then estimate E0 using the discrete ks.
        self.x_TF = None
        self.V_TF = 0
        self.barrier_height = 0

        Experiment.init(self)
        self.barrier_k = self.barrier_k_k_r * self.k_r
        state = self.get_state()

        ks_ = state.basis.kx / k_r
        Em_ = (ks_**2 + 1.0) / 2 - np.sqrt((ks_ - self.d) ** 2 + self.w**2)
        ind = np.argmin(Em_)
        # k0 = ks_[ind] * k_r
        # E0 = Em_[ind] * 2*E_R

        xi = self.xi_micron * u.micron
        self.V_TF = (hbar / xi) ** 2 / 2 / m

        self.barrier_height = self.barrier_height_V_TF * self.V_TF
        Experiment.init(self)

    def _barrier_potential(self, x_barrier_k):
        """Return the barrier potential without the power factor.

        x_barrier_k : x*barrier_k
        """
        if self.barrier_type == "gaussian":
            # Gaussian barrier
            V_barrier = np.exp(-((x_barrier_k) ** 2) / 2.0)
        else:
            # Cosine barrier
            V_barrier = np.cos(x_barrier_k)

        return V_barrier, V_barrier

    def barrier_height_t_(self, t_):
        tw = t_ * self.t_unit * self.drive_w
        if self.drive_type == "gaussian":
            height = np.exp(-((tw - self.drive_t0_w) ** 2) / 2.0)
        else:
            height = np.sin(tw)
        return self.barrier_height * height

    def get_Vt(self, state):
        t_ = state.t / self.t_unit
        barrier_height = self.get("barrier_height", t_=t_)
        barrier_k = self.get("barrier_k", t_=t_)

        x = state.xyz_trap[0]

        if False:
            # Make barrier potential periodic in box
            x0 = self.get("barrier_x", t_=t_)
            Lx = self._get_Lx(state)
            x = (x - x0 - x.min()) % Lx + x.min()

        Va, Vb = self._barrier_potential(x_barrier_k=x * barrier_k)
        return barrier_height * Va, barrier_height * Vb
