"""Visualization and interactions with the Catch and Release set of
experiments.

"""
from __future__ import division

import numpy as np
from matplotlib import pyplot as plt

import mmf_setup.set_path.hgroot

from pytimeode.evolvers import EvolverABM
from mmfutils.contexts import NoInterrupt

from IPython.display import display, clear_output

import os.path
import gpe.bec2
import gpe.tube2
from gpe.soc import gmean

import SOC.soc_catch_and_release
from SOC.soc_catch_and_release import ExperimentCatchAndReleaseSmall, u

MOVIE_DIR = os.path.join(
    "/Users/mforbes/Desktop/Shared/Google Drive", "SOC Bucket:Detuning/SimulationMovies"
)

_locals = {}


def run(
    tube=False,
    gaussian=True,
    x_TF=232.5 * u.micron,
    dx=0.1 * u.micron,
    pause=False,
    t__final=np.inf,
    t__max=2,
    T__x=None,
    cells_x=100,
    parity=True,
    steps=10,
    display_interval=2,
    E_tol=1e-10,
    history=None,
    rescale=True,
    return_expt=False,
    dt_t_scale=0.4,
    cooling_phase=1.0,
    basis_type="1D",
    plot_args=None,
    plot=True,
    cool_steps=0,
    **kw
):
    """Run the simulation.

    Arguments
    ---------
    tube : bool
       If `True`, then run the simulation in the quasi-3D tube geometry simulating
       the radial directions with a dynamic Gaussian profile.  Otherwise, this is
       a 1D simulation with no fluctuations in the transverse direction.  Expansion
       will only be meaningful if `tube=True` or if one uses an axial basis.
       Note: if `True`, then the integrated 1D line density is plotted, whereas
       the central density is plotted if `False`.
    barrier_width_micron, barrier_depth_nK : float
       Barrier potential parameters (same as full experiment)
    gaussian : bool
       If `True`, then the barrier potential is a Guassian, otherwise, it is a
       truncated Harmonic potential with the same depth and curvature.
    x_TF : float
       TF radius in original experiment.  This is used to set the background density.
       Use a small value such as `x_TF=1*u.micron` to set the background
       density to zero.
    pause : bool
       If True, then wait for use input before starting evolution.
    t__final : float
       Time to start imaging in ms.  The evolution from `t__final` to `t__max` will
       invoke the expansion process.
    t__max : float
       Time to stop evolution in ms.
    T__x : float, None
       Trap period along x axis (not used for initial state.)
    cells_x : int
       Number of k_R cells.  This is the size of our box.
    dx : float
       Size of grid spacing.
    history : None, list
       If not None, then store the states here for plotting and show them.
    steps : int
       Number of steps to evolve each iteration.
    rescale : bool
       If True, then re-plot each step.  Slower, but allows the scale of the
       axes to change.
    display_interval : float
       Interval in seconds between display updates.
    E_tol : float
       Tolerance for minimizer that finds the ground state.
    plot_args : dict, None
       Arguments to pass to the plot function.
    """
    import time

    global _locals
    if t__final < t__max:
        assert tube or basis_type == "axial"

    """
    if kw.get('basis_type', None) == 'axial' and 'Nr' not in kw:
        # Compute radial dimensions for axial basis
        kw_ = dict(kw, basis_type='1D')
        expt_ = SOC.soc_catch_and_release.ExperimentCatchAndReleaseSmall(
            barrier_width_micron=barrier_width_micron,
            barrier_depth_nK=barrier_depth_nK,
            T__x=None, cells_x=None,
            dx=dx, x_TF=x_TF,
            **kw_)
        ws = expt_.get('ws', t_=0)
        wx, wr = ws[0], gmean(ws[1:])
        kw['Nr'] = int(np.ceil(expt_.Nx * wx/wr))
        kw['R'] = expt_.Lx/2.0 * wx/wr
    """
    if basis_type == "axial" and dt_t_scale == 0.4:
        # Axial expansion needs smaller dt
        dt_t_scale = 0.2

    expt = SOC.soc_catch_and_release.ExperimentCatchAndReleaseSmall(
        tube=tube,
        basis_type=basis_type,
        T__x=T__x,
        dx=dx,
        gaussian=gaussian,
        x_TF=x_TF,
        t__final=t__final,
        cells_x=cells_x,
        **kw
    )
    if return_expt:
        return expt
    s0 = expt.get_state()

    if plot:
        plot_elements = s0.plot()
        plt.ylabel("TF Density")
        display(plot_elements.fig)

    _locals.update(locals())
    s = expt.get_initial_state(cool_steps=cool_steps, E_tol=E_tol, use_scipy=True)
    _locals.update(locals())
    if plot:
        plot_elements = s.plot()
        plt.ylabel("Ground State Density")
        display(plot_elements.fig)
        if pause:
            raw_input("Ground State: Press enter to evolve")

        clear_output(wait=True)

    s.cooling_phase = cooling_phase
    e = EvolverABM(s, dt=dt_t_scale * s.t_scale)
    if history is not None:
        history.append(s)
    tic = time.time()
    if plot:
        plot_elements = None
        plot_args_ = dict(
            show_momenta=False, parity=parity, pane_height=2, space=0.2, history=history
        )
        if plot_args is not None:
            plot_args_.update(plot_args)
        plot_args = plot_args_

    with NoInterrupt() as interrupted:
        while not interrupted and e.t / expt.t_unit < t__max:
            e.evolve(steps)
            if history is not None:
                history.append(e.get_y())
            if time.time() - tic >= display_interval:
                tic = time.time()
                if plot:
                    if rescale:
                        plot_elements = None
                    plot_elements = e.y.plot(plot_elements=plot_elements, **plot_args)
                    display(plot_elements.fig)
                    clear_output(wait=True)
    if plot:
        plot_elements = e.y.plot(plot_elements=plot_elements, **plot_args)
        display(plot_elements.fig)
        plt.close("all")
    _locals.update(locals())
    return e.get_y()


def make_movie(
    filename,
    history,
    show_n=2,
    show_momenta=1,
    show_history=2,
    show_history_a=1,
    show_history_b=1,
    show_log_history=False,
    pane_height=2,
    dpi=None,
    **kw
):
    history[0].make_movie(
        os.path.join(MOVIE_DIR, filename),
        states=history,
        show_log_history=show_log_history,
        pane_height=pane_height,
        show_n=show_n,
        show_momenta=show_momenta,
        show_history=show_history,
        show_history_a=show_history_a,
        show_history_b=show_history_b,
        dpi=dpi,
        **kw
    )
