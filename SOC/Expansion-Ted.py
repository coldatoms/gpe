# ---
# jupyter:
#   jupytext:
#     formats: ipynb,py
#     text_representation:
#       extension: .py
#       format_name: light
#       format_version: '1.5'
#       jupytext_version: 1.4.0
#   kernelspec:
#     display_name: Python 2 (Ubuntu Linux)
#     language: python
#     name: python2-ubuntu
# ---

import mmf_setup

mmf_setup.nbinit()
# import mmf_setup.set_path.hgroot

# # Experiment 5

# ### Make initial state

# +
# %pylab inline --no-import-all
from gpe import utils, bec2, tube2, minimize
import soc_experiments
from soc_experiments import u

reload(bec2)
reload(tube2)
import soc_experiments

reload(soc_experiments)

experiment = soc_experiments.Experiment5(
    State=soc_experiments.State2tube, Lxyz=(400 * u.micron,), Nxyz=(2 ** 11,), N=2e5
)
s0 = experiment.get_state()

s0.plot()
# Don't fix N... this should respect mu and Rx_TF
m = minimize.MinimizeState(s0, fix_N=False)
s = m.minimize()
s.plot()
# -

# ### add noise
#
# Add random noise by adding extra of low momentum componets


# +
s1 = s.copy()
s1.plot()


s1_f = Ky = s1.fft(s1[...])

np.random.seed(4)
mag = 1e-1
noise = mag * (
    np.random.rand(*s1_f.shape) - 0.5 + 1j * np.random.rand(*s1_f.shape) - 0.5j
)

ks = np.asarray(s1.kxyz).ravel().reshape(*s1[...].shape)
kutoff = (
    abs(ks).sum() / 2.0 / (ks.shape[1])
)  # average k, length scales in the adiabatic nb
# kutoff = 0.1
inds = np.where(abs(ks) > kutoff)

noise[inds] = 0.0 * (1.0 + 1j)
noise_x = s1.ifft(noise)
new_s1 = s1[...] * (1.0 + noise_x)


s1[...] = new_s1

s1.plot()


####################
# Alternative way to add random noise
# np.random.seed = 4
# mag = 1e-2
# noise = mag*(np.random.rand(*s[...].shape) - 0.5
# + 1j * np.random.rand(*s[...].shape) - 0.5j)
# vacuum = np.where(s[...] <= 1e-6)
# noise[vacuum] = 0.*(1.+1j)

# s[...] = s[...] + s[...]*noise
# s.plot()


# -

# ### Evolve

# +
from IPython.display import clear_output, display
from pytimeode.evolvers import EvolverABM
from mmfutils.contexts import NoInterrupt

s2 = s1.copy()
s2.cooling_phase = 1.0 + 0.0001j

e = EvolverABM(s2, dt=0.05 * s.t_scale)
fig = None
with NoInterrupt() as interrupted:
    while not interrupted:
        e.evolve(200)
        plt.clf()
        fig = e.y.plot(fig)
        display(fig)
        clear_output(wait=True)
# -

with NoInterrupt() as interrupted:
    while not interrupted:
        e.evolve(200)
        plt.clf()
        fig = e.y.plot(fig)
        display(fig)
        clear_output(wait=True)


# # Testing Tube with NM data

# +
from soc_experiments import Experiment, u


class ExperimentNM(Experiment):
    """words"""

    Rx_TF = 24.0 * u.micron
    t_unit = u.ms
    detuning_kHz = None
    detuning_E_R = 0.54
    # detuning_E_R = 1.36
    B_gradient_mG_cm = 0
    recoil_frequency_Hz = 1843.0
    rabi_frequency_E_R = 2.5
    trapping_frequencies_Hz = (25, 170, 154)

    initial_imbalance = 0.0  # Nothing RF transfered, all in state[...][0]

    t__wait = 20.0  # Time to wait before imaging (up to 500ms)
    t__expand = 7  # Time for expansion

    @property
    def t__expansion(self):
        return self.t__wait


# -


s.gs
u.scattering_lengths
_a, _b = ((1, -1), (1, 0))
scattering_lengths = np.array(
    [u.scattering_lengths[_k] for _k in [(_a, _a), (_b, _b), (_a, _b)]]
)
gs = 4 * np.pi * u.hbar ** 2 / u.m * scattering_lengths
gs, scattering_lengths / u.a_B

s.gs


# +
# %pylab inline --no-import-all
from gpe import utils, bec2, tube2, minimize, soc_experiments
from soc_experiments import u

reload(bec2)
reload(tube2)
import soc_experiments

reload(soc_experiments)

experiment = ExperimentNM(
    State=soc_experiments.State2tube, Lxyz=(140 * u.micron,), Nxyz=(2 ** 11,), N=2e5
)
s0 = experiment.get_state()

# s0.gs = np.asarray([1e2,1e2,1e2])
s0.gs = np.asarray([1e-8, 1e-8, 1e-8])


# s0.plot()
# plt.xlim(-40, 40)
# Don't fix N... this should respect mu and Rx_TF
m = minimize.MinimizeState(s0, fix_N=False)
s = m.minimize()
s.plot()
plt.xlim(-40, 40)

# +
from IPython.display import clear_output, display
from pytimeode.evolvers import EvolverABM
from mmfutils.contexts import NoInterrupt

s1 = s.copy()
s1.cooling_phase = 1 + 0.000001j
s1.ws[0] = 0.0
# s1[...][1] = 0.

e = EvolverABM(s1, dt=0.5 * s.t_scale)
fig = None
with NoInterrupt() as interrupted:
    while not interrupted:
        e.evolve(200)
        plt.clf()
        fig = e.y.plot(fig)
        display(fig)
        clear_output(wait=True)
# -


# ## NM sims: tube2 vs 1D

# +
from soc_experiments import Experiment, u


class ExperimentNM(Experiment):
    """words"""

    Rx_TF = 24.0 * u.micron
    t_unit = u.ms
    detuning_kHz = None
    detuning_E_R = 0.54
    B_gradient_mG_cm = 0
    recoil_frequency_Hz = 1843.0
    rabi_frequency_E_R = 2.5
    trapping_frequencies_Hz = (25, 170, 154)

    initial_imbalance = 0.0  # Nothing RF transfered, all in state[...][0]

    t__wait = 20.0  # Time to wait before imaging (up to 500ms)
    t__expand = 7  # Time for expansion

    @property
    def t__expansion(self):
        return self.t__wait


# +
# %pylab inline --no-import-all
from gpe import utils, bec2, tube2, minimize
import soc_experiments
from soc_experiments import u

reload(bec2)
reload(tube2)
import soc_experiments

reload(soc_experiments)

experiment = ExperimentNM(
    State=soc_experiments.State2, Lxyz=(140 * u.micron,), Nxyz=(2 ** 11,), N=2e5
)

experimentt = ExperimentNM(
    State=soc_experiments.State2tube, Lxyz=(140 * u.micron,), Nxyz=(2 ** 11,), N=2e5
)

s0 = experiment.get_state()
st0 = experimentt.get_state()


m = minimize.MinimizeState(s0, fix_N=False)
s = m.minimize()
mt = minimize.MinimizeState(st0, fix_N=False)
st = mt.minimize()


# plt.subplot(211)
s.plot()
plt.xlim(-40, 40)
# plt.subplot(221)
st.plot()
plt.xlim(-40, 40)

# +
from IPython.display import clear_output, display
from pytimeode.evolvers import EvolverABM
from mmfutils.contexts import NoInterrupt

s1 = s.copy()
s1.cooling_phase = 1 + 0.000001j
s1.ws[0] = 0.0

st1 = st.copy()
st1.cooling_phase = 1 + 0.000001j
st1.ws[0] = 0.0


e = EvolverABM(s1, dt=0.5 * s.t_scale)
et = EvolverABM(st1, dt=0.5 * st.t_scale)

fig = plt.figure(figsize=(10, 5))
with NoInterrupt() as interrupted:
    while not interrupted:
        e.evolve(200)
        et.evolve(200)
        na, nb = e.y.get_density()
        x = e.y.xyz[0]
        plt.clf()
        plt.plot(x, na + nb, "-")
        na, nb = et.y.get_central_density()
        plt.plot(x, na + nb, ":")
        display(plt.gcf())
        clear_output(wait=True)
        # display(figt)

# -

# from ipywidgets import widgets
#
# out1 = widgets.Output()
# out2 = widgets.Output()
# x = s.xyz[0]
# f1 = plt.figure()
# plt.plot(x, x**2)
# with out1:
#     display(f1)
# f2 = plt.figure()
# plt.plot(x, x**3)
# with out2:
#     display(f2)
# plt.close('all')
#
# display(out1, out2)
#


# Hmm... Fixing $\mu$ does not seem to be sufficient.  There might also be an offset needed due to the $\sigma$...  Should be able to compute analytically.  How about 1D?  (After trying, it seems also to behave similarly, so there is probably an issue with the inter-species interactions.)

# +
experiment = soc_experiments.ExperimentNM(
    State=soc_experiments.State2, Lxyz=(140 * u.micron,), Nxyz=(2 ** 8,), N=2e5
)
s0 = experiment.get_state()
# s = soc_experiments.State2tube(experiment=experiment, Nxyz=(2**8,), Lxyz=(300.0,), ws=ws,
#                           Omega=Omega, delta=delta, k_r=k_r, t=1.)

s0.plot()
plt.xlim(-40, 40)
s0[...] *= 0.8  # Hack for now until mu is fixed.
m = minimize.MinimizeState(s0, fix_N=True)
s = m.minimize()
s.plot()
plt.xlim(-40, 40)
# -

# Still not working.  Check dispersion...

# Here is an anlysis based on the TF model.  We use this to imprint the correct TF density on the state and then minimize.  This seems to give reasonable results.

# For comparison, here we summarize some features of a trapped Bose gas:
#
# Here we derive the results for a harmonically trapped gas in a spherical trap using the local density approximation (LDA) or Thomas-Fermi (TF) approximation:
#
# \begin{gather}
#    V(r) = \frac{m\omega^2 r^2}{2}, \tag{HO traping potential}\\
#    a^2 = \frac{\hbar}{m\omega}, \tag{HO length}\\
#    R_{TF}^2 = \frac{2\mu}{m\omega^2} = a^2 \frac{2m\mu}{\hbar\omega},
#      \tag{Thomas-Fermi or cloud radius}\\
#    \mu_e(r) = \mu - V(r) = \frac{m\omega^2(R_{TF}^2 - r^2)}{2},
#      \tag{local chemical potential}\\
#    n(r) = \frac{m\omega^2(R_{TF}^2 - r^2)}{2g}
#      \tag{density}\\
#    N = \int n(r) \d^3{x} = \frac{4 m \omega^2 R^5 \pi}{15 g}
#      = \frac{16\pi}{15 g\omega^3}\sqrt{\frac{2\mu^5}{m^3}}
#    \tag{particle number}
# \end{gather}
#
# To generalize this to 3D with different trapping frequencies note that the TF radius in each direction $R_i \propto 1/\omega_i$ so we introduce dimensionless coordinates $\tilde{x}_i = x_i/R_i$:
#
# $$
#   \mu = \frac{m\omega_i^2R_i^2}{2}, \qquad
#   \frac{m\omega_i^2}{2} = \frac{\mu}{R_i^2}, \qquad
#   R_i = \sqrt{\frac{2\mu}{m}}\frac{1}{\omega_i},\\
#   n = \frac{\mu - m\sum_i \omega_i^2x_i^2/2}{g}
#     = \mu\frac{1 - \tilde{r}^2}{g},\\
#   N = R_xR_yR_z \int n(\tilde{r})\d^3{\tilde{x}} =
#       \frac{1}{\omega_x\omega_y\omega_z}\sqrt{\frac{2\mu}{m}}^3\frac{8\pi \mu}{15 g} =
#       \frac{16\pi}{15 g \omega_x\omega_y\omega_z}\sqrt{\frac{2\mu^5}{m^3}}
# $$
#
# Here we consider the NPSEQ, which provides a model for the integrated 1D density:
#
# $$
#   \abs{\psi(z)}^2 = \int\d{x}\d{y}n(x, y, z)
#   = \frac{\mu R_xR_y}{g} \int (1-\tilde{r}_\perp^2-\tilde{z}^2)2\pi\tilde{r}_\perp \d{\tilde{r}_\perp^2}
#   = \frac{\pi \mu R_xR_y (1-\tilde{z}^2)}{2 g}
#   = \frac{\pi \mu (1-\tilde{z}^2)}{m\omega_x\omega_y g}
# $$

# $$
#   \abs{\psi(z)}^2 = \int\d{x}\d{y}n(x, y, z)
#   = \frac{\mu R_xR_y}{g} \int (1-\tilde{r}_\perp^2-\tilde{z}^2)2\pi\tilde{r}_\perp \d{\tilde{r}_\perp}
#   = \frac{\pi \mu R_xR_y (1-2\tilde{z}^2)}{2 g}
#   = \frac{\pi \mu^2 (1-2\tilde{z}^2)}{m\omega_x\omega_y g}
# $$

# +
ws = s0.ws
w_perp = np.sqrt(ws[1] * ws[2])
g = s0.gs[0]
w = np.prod(ws) ** (1.0 / 3.0)
R = s0.experiment.Rx_TF
m = s0.ms[0]
mu = m * ws[0] ** 2 * (R ** 2) / 2.0
mu, s0.mu
N = 16 * np.pi / 15 / g / w ** 3 * np.sqrt(2 * mu ** 5 / m ** 3)
print (N)

R, np.sqrt(2.0 * mu / m) / ws
n_1D = np.pi * mu / m / (ws[1] * ws[2]) / g
print (n_1D, np.sqrt(n_1D))
sigma2 = np.sqrt(2 * (g * n_1D / 4 / np.pi + s.hbar ** 2 / 2 / m) / m / w_perp ** 2)
print (sigma2)

# -

x = s0.xyz[0]
x_tilde = x / R
n_1D = np.pi * mu * np.maximum(0, (1 - x_tilde ** 2)) / m / (ws[1] * ws[2]) / g
s0[0] = np.sqrt(n_1D) / 2.7
s0.init()
s0.plot()
plt.xlim(-40, 40)
s = minimize.MinimizeState(s0).minimize()
s.plot()
plt.xlim(-40, 40)

# +
from IPython.display import clear_output, display
from pytimeode.evolvers import EvolverABM
from mmfutils.contexts import NoInterrupt

s1 = s.copy()
s1.cooling_phase = 1 + 0.000001j
s1.ws[0] = 0.0
# s1[...][1] = 0.

e = EvolverABM(s1, dt=0.01 * s.t_scale)
fig = None
with NoInterrupt() as interrupted:
    while not interrupted:
        e.evolve(200)
        plt.clf()
        fig = e.y.plot(fig)
        display(fig)
        clear_output(wait=True)

# +
from IPython.display import clear_output

m = minimize.MinimizeState(s)
m.check()


def callback(state, n=[0]):
    n[0] += 1
    if n[0] % 100 != 0:
        return
    plt.clf()
    state.plot()
    display(plt.gcf())
    clear_output(wait=True)


s = m.minimize(callback=callback)
s.plot()
# -

s1.Omega, s1.delta

# +
# %pylab inline --no-import-all
from gpe import utils, bec2, tube2, minimize

reload(bec2)
reload(tube2)
import soc_experiments

reload(soc_experiments)


experiment = soc_experiments.Experiment1(State=tube2.State2)
s = tube2.State2(experiment=experiment, Nxyz=(2 ** 7,), Lxyz=(10.0,))
s[...] = np.random.random(s[...].shape)
# s.gs = (0.01, 0.01, 0.01)

m = minimize.MinimizeState(s)
m.check()
s = m.minimize()
s.plot()
# -


# # 2 components

# ## Bunny ear experiments

# +
# %pylab inline --no-import-all
from gpe import utils, bec2, tube2, minimize

reload(bec2)
reload(tube2)
import soc_experiments

reload(soc_experiments)

s = soc_experiments.State2tube(experiment=soc_experiments.Experiment1)


# +
# %pylab inline --no-import-all
from gpe import utils, bec2, tube2, minimize

reload(bec2)
reload(tube2)
import soc_experiments

reload(soc_experiments)

s = State2tube(experiment=Experimen1)


# experiment = soc_experiments.Experiment1(State=tube2.State2)
# s = tube2.State2(experiment=experiment, Nxyz=(2**7,), Lxyz=(10.0,))
experiment = soc_experiments.Experiment1(State=tube2.StateGPEdrZ)
s = tube2.StateGPEdrZ(experiment=experiment, Nxyz=(2 ** 7,), Lxyz=(10.0,))
s[...] = np.random.random(s[...].shape)
# s.gs = (0.01, 0.01, 0.01)

m = minimize.MinimizeState(s)
m.check()
s = m.minimize()
s.plot()

# +
from IPython.display import clear_output, display
from pytimeode.evolvers import EvolverABM
from mmfutils.contexts import NoInterrupt

s1 = s.copy()
s1.cooling_phase = 1 + 0.01j
# s1[0,:] *= np.sign(s.xyz[0] - 2.0)
e = EvolverABM(s1, dt=0.5 * s.t_scale)
fig = None
with NoInterrupt() as interrupted:
    while not interrupted:
        e.evolve(200)
        plt.clf()
        fig = e.y.plot(fig)
        display(fig)
        clear_output(wait=True)
# -

# ## tube vs tube2

# A comparison between the 1 and 2 component codes.
#
# Here the interspieces scattering and the off diagonal coupling have been set to zero  ($g_{ab}, \Omega = 0$). In this sinario neither component see eachother and acts like  the 1 component state.
#
# Below I check to see in the interanl potentail $V_{int}$, the crossectional varience $\sigma(z,t)$, and density $n(z,t)$ are the same

# ### mcheck

# +
# %pylab inline --no-import-all
from gpe import utils, bec2, tube2, minimize, tube

reload(bec2)
reload(tube2)
reload(soc_experiments)
reload(tube)

seed = 228

args = dict(Nxyz=(2 ** 9,), Lxyz=(50.0,))
g = 10.0
Omega = 0.0

s1 = tube.State(g=g, **args)
np.random.seed(seed)
s1[...] = 1.0 * (
    np.random.random(s1.shape) + np.random.random(s1.shape) * 1j - 0.5 - 0.5j
)

m1 = minimize.MinimizeState(s1)
print m1.check()
# s10 = m1.minimize()
# s10.plot()

s2 = tube2.StateGPEdrZ(gs=(g, g, 0.0), Omega=Omega, **args)
np.random.seed(seed)
s2[...] = 1.0 * (
    np.random.random(s2.shape) + np.random.random(s2.shape) * 1j - 0.5 - 0.5j
)
m2 = minimize.MinimizeState(s2)
print m2.check()
# s20 = m.minimize()
# s20.plot()


# s1.plot()
# s2.plot()

plt.subplot(211)
s1.plot()
plt.subplot(212)
s2.plot()


# -

# ### Evolve

# +
# %pylab inline --no-import-all
from IPython.display import clear_output, display
from pytimeode.evolvers import EvolverABM
from mmfutils.contexts import NoInterrupt


dt = 0.01
se1 = s1.copy()
se1.cooling_phase = 1j
e1 = EvolverABM(se1, dt=dt)

se2 = s2.copy()
se2.cooling_phase = 1j
e2 = EvolverABM(se2, dt=dt)


# Comparison Gaussians
sigma2sHO = np.asarray(
    [
        _hbar / _m / _w
        for _hbar, _m, _w in zip(
            [s1.hbar, s2.hbar], [s1.m, s2.ms[0]], [s1.get_ws(t=0)[0], s2.get_ws(t=0)[0]]
        )
    ]
)

gaussians = np.asarray(
    [
        np.exp(-(_x ** 2) / 2.0 / _sigma2) / np.sqrt(np.sqrt(np.pi * _sigma2))
        for _x, _sigma2 in zip([s1.xyz[0], s2.xyz[0]], sigma2sHO)
    ]
)


fig = None
plt.figure(figsize=(15, 7))

with NoInterrupt() as interrupted:
    while not interrupted:
        e1.evolve(200)
        e2.evolve(200)
        plt.clf()
        plt.subplot(211)
        plt.plot(s1.xyz[0], gaussians[0])
        e1.y.plot()
        plt.subplot(212)
        e2.y.plot()
        plt.plot(s2.xyz[0], gaussians[1])
        display(plt.gcf())
        # display(fig)
        clear_output(wait=True)


# -

# ## Conservation of energy during retrapping after expansion
#
# The cloud is trapped, then expands, then is trapped again

# NOT COMPLETE!!

# +
# %pylab inline --no-import-all
from gpe import utils, bec2, tube2, minimize, tube

reload(bec2)
reload(tube2)
import soc_experiments

reload(soc_experiments)
reload(tube)

seed = 228

args = dict(Nxyz=(2 ** 9,), Lxyz=(50.0,))
g = 10.0
Omega = 0.0

s1 = tube.State(g=g, **args)
np.random.seed(seed)
s1[...] = 1.0 * (
    np.random.random(s1.shape) + np.random.random(s1.shape) * 1j - 0.5 - 0.5j
)

m1 = minimize.MinimizeState(s1)
print m1.check()
# s10 = m1.minimize()
# s10.plot()

s2 = tube2.StateGPEdrZ(gs=(g, g, 0.0), Omega=Omega, **args)
np.random.seed(seed)
s2[...] = 1.0 * (
    np.random.random(s2.shape) + np.random.random(s2.shape) * 1j - 0.5 - 0.5j
)
m2 = minimize.MinimizeState(s2)
print m2.check()
# s20 = m.minimize()
# s20.plot()


# s1.plot()
# s2.plot()

plt.subplot(211)
s1.plot()
plt.subplot(212)
s2.plot()


# -

# ## 3D vs tube

# +
# %pylab inline --no-import-all
# import mmf_setup.set_path.hgroot
from gpe import utils, bec2, tube2, minimize, tube

reload(bec2)
reload(tube2)
reload(tube)

import soc_experiments

reload(soc_experiments)

Nxyz = (2 ** 7,)
Lxyz = (20.0 * bec2.u.micron,)
s0 = soc_experiments.Experiment1(tube=False, Nxyz=Nxyz * 3, Lxyz=Lxyz * 3).get_state()
s1 = soc_experiments.Experiment1(tube=True, Nxyz=Nxyz, Lxyz=Lxyz).get_state()
s1.plot()
# -

s0.t_expansion, s1.t_expansion, s0.shape, s1.shape

s0.xyz[0].shape, s1.xyz[0].shape[0] / 2.0 ** 14
