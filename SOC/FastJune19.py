# ---
# jupyter:
#   jupytext:
#     formats: ipynb,py
#     text_representation:
#       extension: .py
#       format_name: light
#       format_version: '1.5'
#       jupytext_version: 1.4.0
#   kernelspec:
#     display_name: Python [conda env:_gpe3]
#     language: python
#     name: conda-env-_gpe3-py
# ---



# + [markdown] toc=true
# <h1>Table of Contents<span class="tocSkip"></span></h1>
# <div class="toc"><ul class="toc-item"><li><span><a href="#Save-and-Load-1D-data" data-toc-modified-id="Save-and-Load-1D-data-1"><span class="toc-item-num">1&nbsp;&nbsp;</span>Save and Load 1D data</a></span></li><li><span><a href="#Match-Data" data-toc-modified-id="Match-Data-2"><span class="toc-item-num">2&nbsp;&nbsp;</span>Match Data</a></span><ul class="toc-item"><li><span><a href="#Different-x_TF" data-toc-modified-id="Different-x_TF-2.1"><span class="toc-item-num">2.1&nbsp;&nbsp;</span>Different x_TF</a></span></li><li><span><a href="#1D-profiles,-sim-vs-exp" data-toc-modified-id="1D-profiles,-sim-vs-exp-2.2"><span class="toc-item-num">2.2&nbsp;&nbsp;</span>1D profiles, sim vs exp</a></span></li><li><span><a href="#Tubes" data-toc-modified-id="Tubes-2.3"><span class="toc-item-num">2.3&nbsp;&nbsp;</span>Tubes</a></span></li><li><span><a href="#Tube-V-Axial" data-toc-modified-id="Tube-V-Axial-2.4"><span class="toc-item-num">2.4&nbsp;&nbsp;</span>Tube V Axial</a></span><ul class="toc-item"><li><span><a href="#Interactive" data-toc-modified-id="Interactive-2.4.1"><span class="toc-item-num">2.4.1&nbsp;&nbsp;</span>Interactive</a></span></li></ul></li><li><span><a href="#Axial" data-toc-modified-id="Axial-2.5"><span class="toc-item-num">2.5&nbsp;&nbsp;</span>Axial</a></span></li><li><span><a href="#3D" data-toc-modified-id="3D-2.6"><span class="toc-item-num">2.6&nbsp;&nbsp;</span>3D</a></span><ul class="toc-item"><li><span><a href="#Plotting" data-toc-modified-id="Plotting-2.6.1"><span class="toc-item-num">2.6.1&nbsp;&nbsp;</span>Plotting</a></span></li><li><span><a href="#Evolution" data-toc-modified-id="Evolution-2.6.2"><span class="toc-item-num">2.6.2&nbsp;&nbsp;</span>Evolution</a></span></li><li><span><a href="#Exapnsion" data-toc-modified-id="Exapnsion-2.6.3"><span class="toc-item-num">2.6.3&nbsp;&nbsp;</span>Exapnsion</a></span></li></ul></li></ul></li><li><span><a href="#Load-and-Plot-Data" data-toc-modified-id="Load-and-Plot-Data-3"><span class="toc-item-num">3&nbsp;&nbsp;</span>Load and Plot Data</a></span></li><li><span><a href="#Match-Expansion-Image" data-toc-modified-id="Match-Expansion-Image-4"><span class="toc-item-num">4&nbsp;&nbsp;</span>Match Expansion Image</a></span><ul class="toc-item"><li><span><a href="#Plot-Radial-sims" data-toc-modified-id="Plot-Radial-sims-4.1"><span class="toc-item-num">4.1&nbsp;&nbsp;</span>Plot Radial sims</a></span></li><li><span><a href="#compare-xTF,-radial-sims-vs-exp" data-toc-modified-id="compare-xTF,-radial-sims-vs-exp-4.2"><span class="toc-item-num">4.2&nbsp;&nbsp;</span>compare xTF, radial sims vs exp</a></span></li><li><span><a href="#Data-v-Sim" data-toc-modified-id="Data-v-Sim-4.3"><span class="toc-item-num">4.3&nbsp;&nbsp;</span>Data v Sim</a></span></li></ul></li><li><span><a href="#Different-Plot-Functions" data-toc-modified-id="Different-Plot-Functions-5"><span class="toc-item-num">5&nbsp;&nbsp;</span>Different Plot Functions</a></span><ul class="toc-item"><li><span><a href="#Unknown-func" data-toc-modified-id="Unknown-func-5.1"><span class="toc-item-num">5.1&nbsp;&nbsp;</span>Unknown func</a></span></li><li><span><a href="#Main-function" data-toc-modified-id="Main-function-5.2"><span class="toc-item-num">5.2&nbsp;&nbsp;</span>Main function</a></span></li><li><span><a href="#compare-1-vs-2-bands" data-toc-modified-id="compare-1-vs-2-bands-5.3"><span class="toc-item-num">5.3&nbsp;&nbsp;</span>compare 1 vs 2 bands</a></span></li><li><span><a href="#Tube-v-Axial" data-toc-modified-id="Tube-v-Axial-5.4"><span class="toc-item-num">5.4&nbsp;&nbsp;</span>Tube v Axial</a></span></li><li><span><a href="#Image-v-NonImage" data-toc-modified-id="Image-v-NonImage-5.5"><span class="toc-item-num">5.5&nbsp;&nbsp;</span>Image v NonImage</a></span></li><li><span><a href="#Old-func" data-toc-modified-id="Old-func-5.6"><span class="toc-item-num">5.6&nbsp;&nbsp;</span>Old func</a></span></li><li><span><a href="#Wigner-de-Vills" data-toc-modified-id="Wigner-de-Vills-5.7"><span class="toc-item-num">5.7&nbsp;&nbsp;</span>Wigner de Vills</a></span></li><li><span><a href="#Hist-plot" data-toc-modified-id="Hist-plot-5.8"><span class="toc-item-num">5.8&nbsp;&nbsp;</span>Hist plot</a></span></li><li><span><a href="#3D-Plots" data-toc-modified-id="3D-Plots-5.9"><span class="toc-item-num">5.9&nbsp;&nbsp;</span>3D Plots</a></span><ul class="toc-item"><li><span><a href="#2D-plots" data-toc-modified-id="2D-plots-5.9.1"><span class="toc-item-num">5.9.1&nbsp;&nbsp;</span>2D plots</a></span></li></ul></li></ul></li><li><span><a href="#Movie-Making" data-toc-modified-id="Movie-Making-6"><span class="toc-item-num">6&nbsp;&nbsp;</span>Movie Making</a></span><ul class="toc-item"><li><span><a href="#Chose-data" data-toc-modified-id="Chose-data-6.1"><span class="toc-item-num">6.1&nbsp;&nbsp;</span>Chose data</a></span></li><li><span><a href="#Load-data" data-toc-modified-id="Load-data-6.2"><span class="toc-item-num">6.2&nbsp;&nbsp;</span>Load data</a></span></li><li><span><a href="#Move-function" data-toc-modified-id="Move-function-6.3"><span class="toc-item-num">6.3&nbsp;&nbsp;</span>Move function</a></span></li><li><span><a href="#Load-and-make-for-2-states" data-toc-modified-id="Load-and-make-for-2-states-6.4"><span class="toc-item-num">6.4&nbsp;&nbsp;</span>Load and make for 2 states</a></span></li><li><span><a href="#2-state-plot" data-toc-modified-id="2-state-plot-6.5"><span class="toc-item-num">6.5&nbsp;&nbsp;</span>2 state plot</a></span></li><li><span><a href="#3D" data-toc-modified-id="3D-6.6"><span class="toc-item-num">6.6&nbsp;&nbsp;</span>3D</a></span></li></ul></li></ul></div>

# + init_cell=true
import mmf_setup;mmf_setup.nbinit()
import sys, os
import SOC
_SOC_PATH = os.path.dirname(SOC.__file__)
if _SOC_PATH not in sys.path:
    sys.path.append(_SOC_PATH)
from gpe.imports import *
from mmfutils.plot import imcontourf
# -

# This notebook is for plotting and making movies. Many of the below code was used to make various movies or save data.

# # Save and Load 1D data 

# +
# %pylab inline --no-import-all
from gpe.soc import ExperimentBarrier, u
import SOC.soc_car_fast#;reload(SOC.soc_car_fast)
from SOC.soc_car_fast import ExperimentCARFast, ExperimentCARFastSmall


#Sim data
import runs_car_fast_final
r0 = runs_car_fast_final.Run_IPG_xTF150()
#r0 = runs_car_fast_final.Run_IPG_tube()
#r0 = runs_car_fast_final.Run_IPG_NoSOC_SB()
#r0 = runs_car_fast_final.Run_IPG_NoSOC()

x_TF = 150
barrier_depth_nK = -90

sim = [_sims for _sims in r0.simulations
       if _sims.experiment.barrier_depth_nK == barrier_depth_nK
       and _sims.experiment.x_TF == x_TF
       ][0]

params = sim.experiment.items()
print(params)

# -

s = sim.get_state(0)
[_x.shape for _x in s.xyz]






# +
#ts = np.arange(10,28,2)
#ts = np.arange(10,26,2)
#ts = np.arange(10,30,2)
#ts = np.arange(10,24,2)
#ts = np.arange(26,30,2)
#ts = np.arange(24,30,2)
#ts = np.arange(24,30,4)
ts = np.arange(0,50.5,0.5)
#ts = np.arange(0,50,2)

ns = []
#s = sim.get_state(t_=0, image=True)
#ns.append(s.get_density())


for _t_ in ts:
    s = sim.get_state(t_=_t_, image=True)
    ns.append(s.get_density())

    

# -



# +

ns = np.asarray(ns)
#ts_new = np.zeros(ts.shape[0]+1)
#ts_new[1:] = ts
ts = np.asarray(ts)
x = np.asarray(s.xyz[0])
params = np.asarray(params)
filename = 'CAR_FAST_IPG_Barrier{}nK_xTF{:.0f}_Expanded_Tube.npz'.format(abs(params[0][1]), params[-1][1])
#filename = 'CAR_FAST_IPG_Barrier{}nK_Expanded_Tube.npz'.format(abs(params[0][1]))
#filename_LOS = 'CAR_FAST_IPG_Barrier{}nK_xTF{:.0f}_LOS_Expanded.npz'.format(abs(params[0][1]), params[-1][1])
np.savez(filename, x=x, ns=ns, ts=ts, params=params)
#np.savez(filename_LOS, x=x, r=r, ns=ns_LOS, ts=ts, params=params)

print(filename)
#print(filename_LOS)
                                                                      


# +

ns = np.asarray(ns)
ns_LOS = np.asarray(ns_LOS)
ts = np.asarray(ts)
x = np.asarray(s.xyz[0])
r = np.asarray(s.xyz[1][0])
params = np.asarray(params)
filename = 'CAR_FAST_IPG_Barrier{}nK_xTF{:.0f}_Expanded.npz'.format(abs(params[0][1]), params[-1][1])
filename_LOS = 'CAR_FAST_IPG_Barrier{}nK_xTF{:.0f}_LOS_Expanded.npz'.format(abs(params[0][1]), params[-1][1])
np.savez(filename, x=x, r=r, ns=ns, ts=ts, params=params)
np.savez(filename_LOS, x=x, r=r, ns=ns_LOS, ts=ts, params=params)

print(filename)
print(filename_LOS)
                                                                      


# +
from mmfutils.plot import imcontourf
    
data = np.load(filename, allow_pickle=True)

a = data['x']
#b = data['r']
c = data['ns']
d = data['ts']
e = dict(data['params'])

np.allclose(x, data['x'])
#np.allclose(r, data['r'])
np.allclose(ns, data['ns'])
np.allclose(ts, data['ts'])

#plt.plot(a,c[0])
#imcontourf(a, b, c[4].sum(axis=0), aspect=1)
plt.plot(a,c[4].sum(axis=0))    

# +
import load_example;reload(load_example)
"""
data = load_example.load_2D_state(filename=None,
                                  SOC=True,
                                  barrier_depth=barrier_depth_nK,
                                  x_TF=x_TF,
                                  LOS=False,
                                  mirror_x=True)

x = data[0]
r = data[1]
ns = data[2]
ts = data[3]
params = data[4]
"""
data = load_example.load_1D_state(barrier_depth=barrier_depth_nK,
                                  x_TF=153,
                                  tube=True)

x = data[0]
ns = data[1]
ts = data[2]
params = data[3]

#imcontourf(x, r, ns[6].sum(axis=0), aspect=1)
plt.plot(x,ns[4].sum(axis=0))


# +
import load_example;reload(load_example)
data = load_example.load_2D_state(filename=None,
                                  SOC=True,
                                  barrier_depth=barrier_depth_nK,
                                  x_TF=x_TF,
                                  LOS=True,
                                  mirror_x=True)
x = data[0]
r = data[1]
ns = data[2]
ts = data[3]
params = data[4]


imcontourf(x, r, ns[6].sum(axis=0), aspect=1)


# -

# # Match Data

# ## Different x_TF 

# +
# %pylab inline --no-import-all
from gpe.soc import ExperimentBarrier, u
import SOC.soc_car_fast#;reload(SOC.soc_car_fast)
from SOC.soc_car_fast import ExperimentCARFast, ExperimentCARFastSmall


from gpe.plot_utils import MPLGrid
plt.figure(figsize=(15, 3*3*2))
grid = MPLGrid(direction='down', space=0.0)


# Sim data
import runs_car_fast_final
#r0 = runs_car_fast_final.Run0()
#r0 = runs_car_fast_final.Run_IPG()
#r0 = runs_car_fast_final.Run_IPG_small()
#r0 = runs_car_fast_final.Run_IPG_small_xTF150_SB()
#r0 = runs_car_fast_final.Run_IPG_xTF150()
#r1 = runs_car_fast_final.Run_IPG_xTF150_SB()
r0 = runs_car_fast_final.Run_IPG_NoSOC_SB()

x_TF = 150
barrier_depth_nK = -30

sim0 = [_sims for _sims in r0.simulations
        if _sims.experiment.barrier_depth_nK == barrier_depth_nK
        #and _sims.experiment.x_TF == x_TF
       ]
params0 = [_sim.experiment for _sim in sim0]


# Experiment DATA
filename0 = '1D_DATA_'
filename1 = 'C_R_fast_noSOC_{:.0f}uW.npz'.format(abs(barrier_depth_nK)/2)
res = np.load(filename0 + filename1)
ts = res['t']
ns = res['ns']
x = res['x']

n_scale = ns[0].max()
n_scale = np.trapz(ns[0], x[0])


for _r in range(3):
    ax = grid.next()
    s = sim0[_r].get_state(t_=12, image=True)
    N_scale = np.trapz(s.get_density_x().sum(axis=0), s.xyz[0].ravel())
    
    _x = s.xyz[0]
    plt.plot(_x, s.get_density_x().sum(axis=0)/N_scale, alpha=0.6)
    
    plt.plot(x[0]-50, ns[0]/n_scale)
    plt.xlim(-350,350)
    plt.ylim(0,.02)
    plt.legend();

plt.xlabel(r'$x$ [micron]', size='xx-large')
plt.legend()


    
# -

# ## 1D profiles, sim vs exp

# +
# %pylab inline --no-import-all
from gpe.soc import ExperimentBarrier, u
import SOC.soc_car_fast#;reload(SOC.soc_car_fast)
from SOC.soc_car_fast import ExperimentCARFast, ExperimentCARFastSmall


from gpe.plot_utils import MPLGrid
plt.figure(figsize=(15, 3*3))
grid = MPLGrid(direction='down', space=0.0)


#Sim data
import runs_car_fast_final
#r0 = runs_car_fast_final.Run0()
#r0 = runs_car_fast_final.Run_IPG()
#r0 = runs_car_fast_final.Run_IPG_small()
r0 = runs_car_fast_final.Run_IPG_small_xTF150()



sim  = r0.simulations[0]
params = sim.experiment



#Experiment DATA
filename0 = '1D_DATA_'
filename1 = 'C_R_fast_15uW.npz'
res = np.load(filename0 + filename1)
ts = res['t']
ns = res['ns']
x = res['x']

n_scale = ns[0].max()
n_scale = np.trapz(ns[0], x[0])


_ts = np.array([10, 16, 26, 36, 46])
#ts = np.array([10, 16, 26])

for _t in _ts:
    ax = grid.next()
    s = sim.get_state(t_=_t, image=True)
    N_scale = np.trapz(s.get_density_x().sum(axis=0), s.xyz[0].ravel())
    
    #args = dict(vmin=0,vmax=650)
    #x = s.basis.xyz[0]
    _x = s.xyz[0]
    rr = s.xyz[1][0]
    
    plt.plot(_x, s.get_density_x().sum(axis=0)/N_scale, alpha=0.6, label=r'n_small')
    _ind = np.where(ts==_t-10)[0][0]
    plt.plot(x[0], ns[_ind]/n_scale, label=r'190425')
    
    plt.text(0.05, 0.9, 't_wait={}ms$, \qquad $t_exp=10.1ms'.format(_t-10),
             horizontalalignment='left',
             verticalalignment='center', transform=ax.transAxes)
    plt.legend();

plt.xlabel(r'$x$ [micron]', size='xx-large')
plt.legend()

#name = 'Sim_V_Data_fast_Barrier30nK_IMAGE_INCOMPLETE'
#plt.savefig(name);

    
    
# -



# ## Tubes 

# +
# %pylab inline --no-import-all
from gpe.soc import ExperimentBarrier, u
import SOC.soc_car_fast#;reload(SOC.soc_car_fast)
from SOC.soc_car_fast import ExperimentCARFast, ExperimentCARFastSmall


from gpe.plot_utils import MPLGrid
plt.figure(figsize=(15, 3*3))
grid = MPLGrid(direction='down', space=0.0)


#Sim data
import runs_car_fast_final
r0 = runs_car_fast_final.Run_IPG_small_tube()
sim_150  = r0.simulations[0]
sim_205  = r0.simulations[1]


#Experiment DATA
#filename0 = '1D_DATA_'
#filename1 = 'C_R_fast_15uW.npz'
#res = np.load(filename0 + filename1)
#ts = res['t']
#ns = res['ns']
#x = res['x']


_ts = np.arange(0,)

for _t in _ts:
    ax = grid.next()
    #t = np.floor(_t*10)/10
    filename = 'frame_{}.0000_image_10.1000.npy'.format(_t)

    s[...] = np.load(directory+filename)
    s.t = _t
    
    #args = dict(vmin=0,vmax=650)
    #x = s.basis.xyz[0]
    _x = s.xyz[0]
    rr = s.xyz[1][0]
    plt.plot(_x, s.get_density_x().sum(axis=0)/N, alpha=0.6, label=r'n_small')
    _ind = np.where(ts==_t-10)[0][0]
    plt.plot(x[0], ns[_ind]/n_scale, label=r'190425')
    
    plt.text(0.05, 0.9, 't_wait={}ms$, \qquad $t_exp=10.1ms'.format(_t-10),
             horizontalalignment='left',
             verticalalignment='center', transform=ax.transAxes)
    plt.legend();

plt.xlabel(r'$x$ [micron]', size='xx-large')
plt.legend()

#name = 'Sim_V_Data_fast_Barrier30nK_IMAGE_INCOMPLETE'
#plt.savefig(name);

    
    
# -



# ## Tube V Axial 

# +
# %pylab inline --no-import-all
from gpe.soc import ExperimentBarrier, u
import SOC.soc_car_fast#;reload(SOC.soc_car_fast)
from SOC.soc_car_fast import ExperimentCARFast, ExperimentCARFastSmall


from gpe.plot_utils import MPLGrid
plt.figure(figsize=(15, 3*3))
grid = MPLGrid(direction='down', space=0.0)


#Sim data
import runs_car_fast_final
r0 = runs_car_fast_final.Run_IPG_xTF150()
r1 = runs_car_fast_final.Run_IPG_tube()

x_TF = 150
barrier_depth_nK = -30

sim0 = [_sims for _sims in r0.simulations
         if _sims.experiment.x_TF == x_TF
         and _sims.experiment.barrier_depth_nK == barrier_depth_nK][0]

sim1 = [_sims for _sims in r1.simulations
         if _sims.experiment.x_TF > x_TF+1
         and _sims.experiment.barrier_depth_nK == barrier_depth_nK][0]

params0 = sim0.experiment.items()
params1 = sim1.experiment.items()
print(params0)
print(params1)


# +
from gpe.plot_utils import MPLGrid
plt.figure(figsize=(15, 3*3*6*2))
grid = MPLGrid(direction='down', space=0.0)

ts_ = np.arange(10,52,2)
dt=0.5
ts_ = np.arange(0,50+dt,dt)

for _t in ts_:
    ax = grid.next()
    plt.sca(ax)
    _t_ = np.floor(_t*10)/10
    t_image = 10.1
    s0 = sim0.get_state(t_=_t_, image=t_image);
    s1 = sim1.get_state(t_=_t_, image=t_image);
    clear_output()
    ax.plot(s0.xyz[0], s0.get_density_x().sum(axis=0), label=r'axial')
    ax.plot(s1.xyz[0], s1.get_density_x().sum(axis=0), alpha=0.5, label=r'tube')
    ax.set_xlim(-100,100)
    plt.text(0.05, 0.9, 't_wait={}ms$, \qquad $t_exp={}ms'.format(_t_-10, t_image),
             horizontalalignment='left',
             verticalalignment='center', transform=ax.transAxes)
    plt.legend(loc='upper right');

plt.xlabel(r'$x$ [micron]', size='xx-large')
plt.legend()

#name = 'Sim_V_Data_fast_Barrier30nK_IMAGE_INCOMPLETE'
#plt.savefig(name);

    
    
# -

# ### Interactive 

# +
# %pylab inline --no-import-all
from gpe.soc import ExperimentBarrier, u
import SOC.soc_car_fast#;reload(SOC.soc_car_fast)
from SOC.soc_car_fast import ExperimentCARFast, ExperimentCARFastSmall


from gpe.plot_utils import MPLGrid
plt.figure(figsize=(15, 3*3))
grid = MPLGrid(direction='down', space=0.0)


#Sim data
import runs_car_fast_final
r0 = runs_car_fast_final.Run_IPG_xTF150()
r1 = runs_car_fast_final.Run_IPG_tube()


x_TF = 150
barrier_depth_nK = -90

sim0 = [_sims for _sims in r0.simulations
         if _sims.experiment.x_TF == x_TF
         and _sims.experiment.barrier_depth_nK == barrier_depth_nK][0]

sim1 = [_sims for _sims in r1.simulations
         if _sims.experiment.x_TF > x_TF+1
         and _sims.experiment.barrier_depth_nK == barrier_depth_nK][0]

params0 = sim0.experiment.items()
params1 = sim1.experiment.items()

print(params0)
print(params1)



# +
from gpe.plot_utils import MPLGrid
plt.figure(figsize=(15, 3*3*6))
grid = MPLGrid(direction='down', space=0.0)

dt=0.5
ts_ = np.arange(0,50+dt,dt)

states = {(_t_, _image): (sim0.get_state(t_=_t_, image=_image), 
                          sim1.get_state(t_=_t_, image=_image))
          for _t_ in ts_
          for _image in [True, False]}


    


# +
from ipywidgets import interact

@interact(t=(0,50,0.5))
def draw_frame(t=10, x=0, image=False):
    fig=plt.figure(figsize=(15,3))
    _t_ = np.floor(t*10)/10
    xlim = 100
    
    s0, s1 = states[(_t_, image)]
    plt.subplot(311)
    ax = plt.gca()
    ax.plot(s0.xyz[0], s0.get_density_x().sum(axis=0), label=r'axial')
    ax.plot(s1.xyz[0], s1.get_density_x().sum(axis=0), alpha=0.7, label=r'tube')
    ax.set_xlim(-xlim,xlim)
    
    plt.subplot(312)
    r = s0.xyz[1][0]
    ns = s0.get_density()
    radial_ns =  np.concatenate((ns[:,:,::-1], ns), axis=2)
    #radial_ns =  np.concatenate((ns[:,::-1], ns), axis=1)
    imcontourf(s0.xyz[0], np.concatenate((-r[::-1],r)),
               radial_ns.sum(axis=0), 
               interpolation='none',
               #diverging=True,
               #aspect=1,
               #**args
              )
    plt.xlim(-xlim,xlim)
    
    plt.subplot(313)
    r = s0.xyz[1][0]
    ns = s0.get_density()
    radial_ns =  np.concatenate((ns[:,:,::-1], ns), axis=2)
    #radial_ns =  np.concatenate((ns[:,::-1], ns), axis=1)
    imcontourf(s0.xyz[0], np.concatenate((-r[::-1],r)),
               radial_ns.sum(axis=0), 
               interpolation='none',
               #diverging=True,
               #aspect=1,
               #**args
              )
    plt.xlim(-xlim,xlim)
    
    #plt.text(0.05, 0.9, 't_wait={}ms$, \qquad $t_exp=0ms'.format(_t_-10),
    #         horizontalalignment='left',
    #         verticalalignment='center', transform=ax.transAxes)
    #plt.legend(loc='upper right');
    #return fig




#draw_frame(t=5);
# -



# ## Axial

# +
# %pylab inline --no-import-all
from gpe.soc import ExperimentBarrier, u
import SOC.soc_car_fast#;reload(SOC.soc_car_fast)
from SOC.soc_car_fast import ExperimentCARFast, ExperimentCARFastSmall

#Sim data
import runs_car_fast_final
r0 = runs_car_fast_final.Run_IPG_xTF150()
r1 = runs_car_fast_final.Run_IPG_tube()
r2 = runs_car_fast_final.Run_IPG_small_3D()
#r2 = runs_car_fast_final.Run_IPG_small_3D_alt()

x_TF = 150
barrier_depth_nK = -90
y_tilt = 0.


sim0 = [_sims for _sims in r0.simulations
        if _sims.experiment.x_TF == x_TF
        and _sims.experiment.barrier_depth_nK == barrier_depth_nK][0]

sim1 = [_sims for _sims in r1.simulations
        if _sims.experiment.x_TF > x_TF+1
        and _sims.experiment.barrier_depth_nK == barrier_depth_nK][0]

sim2 = [_sims for _sims in r2.simulations
        if _sims.experiment.x_TF == x_TF
        and _sims.experiment.barrier_depth_nK == barrier_depth_nK
        and _sims.experiment.y_tilt == y_tilt][0]

params0 = sim0.experiment.items()
params1 = sim1.experiment.items()
params2 = sim2.experiment.items()

print(params0)
print(params1)
print(params2)

# -


states = []
dt = 0.5
ts_ = np.arange(0,50+dt,dt)
for _t_ in ts_:
    t = np.floor(_t_*10)/10
    #print(t)
    s0 = sim0.get_state(t_=t, image=False)
    states.append(s0)



# +
import mmfutils.plot as mmfplt
from mmfutils.plot import imcontourf
from gpe.plot_utils import MPLGrid
from ipywidgets import interact, interact_manual

xlim = 50

@interact(t=(0,50,dt),x_cut=(-xlim,xlim,0.1))
def draw_frame(t=10, x_cut=0):
    fig = plt.figure(figsize=(12,3))
    fig.clear()
    
    master_grid = MPLGrid(fig=fig,
                           direction='down',
                           right=False,
                           #scale=True,
                           space=0.0, 
                           #share=True,
                           size=1
                          )

    grid = master_grid.grid(direction='right',
                            #right=False,
                            #scale=True,
                            space=0.0, 
                            #share=True,
                            size=1
                           )

    cb_grid = master_grid.grid(direction='down',
                               #right=True,
                               #scale=True,
                               space=0.0, 
                               #share=True,
                               size=0.5
                              )
    
    
    fsize = 7.
    params = {'font.size': fsize}
    plt.rcParams.update(params)
    
    #xlim = 100
    _t_ = np.floor(t*10)/10
    it = np.where(ts_>=_t_)[0][0]
    s0 = states[it]
    
    ns = na, nb = s0.get_density()
    radial_ns = radial_na,  radial_nb =  np.concatenate((ns[:,:,::-1], ns), axis=2)
    psi = psia, psib = s0.get_psi()
    radial_psi = radial_psia,  radial_psib =  np.concatenate((psi[:,:,::-1], psi), axis=2)
    
    x, r = s0.xyz
    x=x.ravel()
    r=r.ravel()
    r = np.concatenate((-r[::-1],r))
    
    title = r't_wait$={:.1f}$ms, $\qquad $Barrier Depth$={}$nK '.format(s0.t/s0.t_unit-10,
                                                                        s0.experiment.barrier_depth_nK) 
    plt.title(title)
    
    ix = np.where(x>=x_cut)[0][0]
    ############################
    ax = ax1 = grid.next()
    plt.plot(r, radial_ns.sum(axis=0)[ix])
    plt.plot(r, radial_na[ix])
    plt.plot(r, radial_nb[ix])
    
    ############################
    ax = ax2 = grid.next()
    factor = 1/2/np.pi
    phase_a = factor*np.angle(radial_psia[ix])
    plt.plot(r, phase_a)
    #plt.plot(r, mmfplt.colors.color_angle(phase_a)) 
    #           interpolation='none',
    
    ############################
    ax = ax3 = grid.next()
    factor = 1/2/np.pi
    phase_b = factor*np.angle(radial_psib[ix])
    plt.plot(r, phase_b)
    
    ############################
    #ax = ax4 = grid.next()
    
    ############################
    cax = cax1 = cb_grid.next()
    imcontourf(x, r, 
               radial_ns.sum(axis=0), 
               interpolation='none',
               #diverging=True,
               aspect=1,
               #**args
              )
    plt.xlim(-xlim,xlim)
    plt.axvline(x=x_cut,c='r')
    
    
    plt.tight_layout()
    #return fig
# -



# ## 3D 

# +
# %pylab inline --no-import-all
from gpe.soc import ExperimentBarrier, u
import SOC.soc_car_fast#;reload(SOC.soc_car_fast)
from SOC.soc_car_fast import ExperimentCARFast, ExperimentCARFastSmall


from gpe.plot_utils import MPLGrid
plt.figure(figsize=(15, 3*3))
grid = MPLGrid(direction='down', space=0.0)


#Sim data
import runs_car_fast_final
r0 = runs_car_fast_final.Run_IPG_xTF150()
r1 = runs_car_fast_final.Run_IPG_tube()
r2 = runs_car_fast_final.Run_IPG_small_3D()
r2 = runs_car_fast_final.Run_IPG_small_3D_alt()

x_TF = 150
barrier_depth_nK = -90
y_tilt = 0.


sim0 = [_sims for _sims in r0.simulations
        if _sims.experiment.x_TF == x_TF
        and _sims.experiment.barrier_depth_nK == barrier_depth_nK][0]

sim1 = [_sims for _sims in r1.simulations
        if _sims.experiment.x_TF > x_TF+1
        and _sims.experiment.barrier_depth_nK == barrier_depth_nK][0]

sim2 = [_sims for _sims in r2.simulations
        if _sims.experiment.x_TF == x_TF
        and _sims.experiment.barrier_depth_nK == barrier_depth_nK
        and _sims.experiment.y_tilt == y_tilt][0]

params0 = sim0.experiment.items()
params1 = sim1.experiment.items()
params2 = sim2.experiment.items()

print(params0)
print(params1)
print(params2)

# -


states = []
dt=0.5
ts_ = np.arange(0,21+dt,dt)
for _t_ in ts_:
    t = np.floor(_t_*10)/10
    if t%0.5 == 0:
        s0 = sim0.get_state(t_=t, image=False)
        s1 = sim1.get_state(t_=t, image=False)
        s2 = sim2.get_state(t_=t, image=False)
    else:
        #s0 = sim0.get_state(t_=t, image=False)
        s1 = sim1.get_state(t_=t, image=False)
        s2 = sim2.get_state(t_=t, image=False)
    states.append((s0,s1,s2))
    


# +
from ipywidgets import interact

@interact(t=(0,21,0.5))
def draw_frame(t=10):
    fig=plt.figure(figsize=(15,3))
    _t_ = np.floor(t*10)/10
    xlim = 29
    #xlim = 59
    
    it = np.where(ts_==_t_)[0][0]
    s0, s1, s2,  = states[it]
    

    plt.subplot(311)
    ax = plt.gca()
    x,y,z = s2.xyz
    ax.plot(s0.xyz[0], s0.get_density_x().sum(axis=0)/s0.get_N(), label=r'axial')
    ax.plot(s1.xyz[0], s1.get_density_x().sum(axis=0)/s1.get_N(), alpha=0.7, label=r'tube')
    ax.plot(x.ravel(), s2.get_density_x().sum(axis=0)/s2.get_N()*12, label=r'3D')
    ax.set_xlim(-xlim,xlim)
    plt.legend(loc='upper right')
    title = r't_wait$={:.1f}$ms, $\qquad $Barrier Depth$={}$nK '.format(_t_-10,
                                                                        s0.experiment.barrier_depth_nK) 
    ax.set_title(title)
    
    
    
    plt.subplot(312)
    r = s0.xyz[1][0]
    ns = s0.get_density()
    radial_ns =  np.concatenate((ns[:,:,::-1], ns), axis=2)
    #radial_ns =  np.concatenate((ns[:,::-1], ns), axis=1)
    imcontourf(s0.xyz[0], np.concatenate((-r[::-1],r)),
               radial_ns.sum(axis=0)/s1.get_N(), 
               interpolation='none',
               #diverging=True,
               #aspect=1,
               #**args
              )
    plt.xlim(-xlim,xlim)
    
    plt.subplot(313)
    #args = dict(vmin=0,vmax=0.000005)
    ns = s2.get_density()
    imcontourf(x.ravel(), y.ravel(),
               ns.sum(axis=0)[:, :, z.ravel().shape[0]//2]/s2.get_N(), 
               interpolation='none',
               #diverging=True,
               #aspect=1,
               #**args
              )
    plt.xlim(-xlim,xlim)
# -


s3D = []
dt=4
ts_ = np.arange(0,20+dt,dt)
for _t_ in ts_:
    t = np.floor(_t_*10)/10
    print(t)
    s3D.append(sim2.get_state(t_=t, image=False))
    


# +
import mmfutils.plot as mmfplt
from ipywidgets import interact, interact_manual

@interact_manual(t=(0,21,dt),x_cut=(-29,29,0.1))
def draw_frame(t=10, x_cut=0):
    fig=plt.figure(figsize=(15,10))
    _t_ = np.floor(t*10)/10
    xlim = 29
    #xlim = 59
    
    it = np.where(ts_>_t_)[0][0]
    #s0, s1, s2  = states[it]
    s2 = s3D[it] 
    
    ns = na, nb = s2.get_density()
    psi = psia, psib = s2.get_psi()
    x,y,z = s2.xyz
    
    #################################
    plt.subplot(711)
    plt.plot(x.ravel(), s2.get_density_x().sum(axis=0), label='n')
    plt.plot(x.ravel(), s2.get_density_x()[0], label='na')
    plt.plot(x.ravel(), s2.get_density_x()[1], alpha=0.7, label='nb')
    plt.plot(x.ravel(), s2.get_density_x()[1]*s2.get_Ns()[0]/s2.get_Ns()[1], alpha=0.7, label='nb_scale')
    plt.axvline(x=x_cut,c='r')
    plt.xlim(-xlim,xlim)    
    title = r't_wait$={:.1f}$ms, $\qquad $Barrier Depth$={}$nK '.format(s2.t/s2.t_unit-10,
                                                                        s2.experiment.barrier_depth_nK) 
    plt.title(title)
    plt.legend(loc='upper right')
    
    #################################
    plt.subplot(712)
    imcontourf(x.ravel(), y.ravel(),
               np.log10(ns.sum(axis=0)[:, :, z.ravel().shape[0]//2] + 1e-8), 
               interpolation='none',
               #diverging=True,
               #aspect=1,
               #**args
              )
    plt.axvline(x=x_cut,c='r')
    plt.xlim(-xlim,xlim)    
    
    #################################    
    plt.subplot(713)
    imcontourf(x.ravel(), y.ravel(),
               na[:, :, z.ravel().shape[0]//2], 
               interpolation='none',
               #diverging=True,
               #aspect=1,
               #**args
              )
    #plt.axvline(x=x_cut,c='r')
    plt.xlim(-xlim,xlim)    
    
    #################################
    plt.subplot(714)
    factor = 5
    phase_a = factor*np.angle(psia[:, :, z.ravel().shape[0]//2])
    imcontourf(x.ravel(), y.ravel(),
               mmfplt.colors.color_angle(phase_a), 
               interpolation='none',
               #diverging=True,
               #aspect=1,
               #**args
              )
    #plt.axvline(x=x_cut,c='r')
    plt.xlim(-xlim,xlim)    
    
    #################################    
    plt.subplot(715)
    factor = 5
    phase_a = factor*np.angle(psib[:, :, z.ravel().shape[0]//2])
    imcontourf(x.ravel(), y.ravel(),
               mmfplt.colors.color_angle(phase_a), 
               interpolation='none',
               #diverging=True,
               #aspect=1,
               #**args
              )
    plt.xlim(-xlim,xlim)    
    
    #################################    
    plt.subplot(716)
    #args = dict(vmin=0,vmax=0.000005)
    imcontourf(x.ravel(), y.ravel(),
               nb[:, :, z.ravel().shape[0]//2], 
               interpolation='none',
               #diverging=True,
               #aspect=1,
               #**args
              )
    plt.xlim(-xlim,xlim)    
    
    #################################    
    plt.subplot(717)
    #args = dict(vmin=0,vmax=0.000005)
    ix = np.where(x.ravel()>x_cut)[0][0]
    imcontourf(y.ravel(), z.ravel(),
               ns.sum(axis=0)[ix], 
               interpolation='none',
               #diverging=True,
               aspect=1,
               #**args
              )
    
# -



# ### Plotting

# +
import mmfutils.plot as mmfplt
from ipywidgets import interact
from mmfutils.plot import imcontourf
from gpe.plot_utils import MPLGrid
#plt.figure(figsize=(15, 3*5))
#grid = MPLGrid(direction='down', space=0.1)

xlim = 29
@interact(t=(0,20,dt),x_cut=(-xlim,xlim,0.1))
def plot_func_2(t=(0,21,dt),
                x_cut0=(-29,29,0.1),
                x_cut1=(-29,29,0.1),
                x_cut2=(-29,29,0.1),
                fig=None,
               ):
    
    if fig is None:
        fig = plt.figure(figsize=(20,6))
    fig.clear()
    
    master_grid = MPLGrid(fig=fig,
                           direction='down',
                           right=False,
                           #scale=True,
                           space=0.05, 
                           share=True,
                           size=0.02
                          )

    grid = master_grid.grid(direction='right',
                            #right=False,
                            #scale=True,
                            space=0.01, 
                            share=True,
                            size=1
                           )

    cb_grid = master_grid.grid(direction='down',
                               #right=True,
                               #scale=True,
                               space=0.1, 
                               #share=True,
                               size=1
                              )
    
    
    fsize = 7.
    params = {'font.size': fsize}
    plt.rcParams.update(params)
    
    _t_ = np.floor(t*10)/10
    it = np.where(ts_>_t_)[0][0]
    #s0, s1, s2  = states[it]
    s2 = s3D[it] 

    ns = na, nb = s2.get_density()
    psi = psia, psib = s2.get_psi()
    x,y,z = s2.xyz
    
    
    ############################
    ax = ax1 = grid.next()
    ############################
    ax = ax2 = grid.next()
    ############################
    ax = ax3 = grid.next()
    ############################
    ax = ax4 = grid.next()
    
    ############################
    cax = cax1 = cb_grid.next()
    
    
    plt.tight_layout()
    return fig
# -

plot_func_2();



# ### Evolution

# +
from gpe.plot_utils import MPLGrid
plt.figure(figsize=(15, 3*3*6))
grid = MPLGrid(direction='down', space=0.0)

ts_ = np.arange(10,52,2)
dt=1
ts_ = np.arange(0,50+dt,dt)

for _t in ts_:
    ax = grid.next()
    plt.sca(ax)
    _t_ = np.floor(_t*10)/10
    t_image = 0
    s0 = sim0.get_state(t_=_t_, image=t_image);
    s1 = sim1.get_state(t_=_t_, image=t_image);
    clear_output()
    ax.plot(s0.xyz[0], s0.get_density_x().sum(axis=0), label=r'axial')
    ax.plot(s1.xyz[0], s1.get_density_x().sum(axis=0), alpha=0.8, label=r'tube')
    ax.set_xlim(-100,100)
    plt.text(0.05, 0.9, 't_wait={}ms$, \qquad $t_exp={}ms'.format(_t_-10, t_image),
             horizontalalignment='left',
             verticalalignment='center', transform=ax.transAxes)
    plt.legend(loc='upper right');

plt.xlabel(r'$x$ [micron]', size='xx-large')
plt.legend()

#name = 'Sim_V_Data_fast_Barrier30nK_IMAGE_INCOMPLETE'
#plt.savefig(name);

    
    
# -

# ### Exapnsion 

# +
from gpe.plot_utils import MPLGrid
plt.figure(figsize=(15, 3*3*3))
grid = MPLGrid(direction='down', space=0.0)

ts_ = np.arange(0,10.1,0.5)

for _t in ts_:
    ax = grid.next()
    plt.sca(ax)
    _t_ = np.floor(_t*10)/10
    time = 35
    s0 = sim0.get_state(t_=time, image=_t_);
    s1 = sim1.get_state(t_=time, image=_t_);
    clear_output()
    ax.plot(s0.xyz[0], s0.get_density_x().sum(axis=0), label=r'axial')
    ax.plot(s1.xyz[0], s1.get_density_x().sum(axis=0), alpha=0.8, label=r'tube')
    ax.set_xlim(-100,100)
    ax.set_ylim(0,3500)
    plt.text(0.05, 0.9, 't_wait={}ms$, \qquad $t_exp={}ms'.format(time-10, _t_),
             horizontalalignment='left',
             verticalalignment='center', transform=ax.transAxes)
    plt.legend(loc='upper right');

plt.xlabel(r'$x$ [micron]', size='xx-large')
plt.legend()

#name = 'Sim_V_Data_fast_Barrier30nK_IMAGE_INCOMPLETE'
#plt.savefig(name);

    
    
# -



# # Load and Plot Data

# +
from mmfutils.plot import imcontourf
from gpe.plot_utils import MPLGrid
plt.figure(figsize=(15, 3*6*5))
grid = MPLGrid(direction='down', space=0.1)
    
#Sim data
import runs_car_fast
r0 = runs_car_fast.Run_barrier()
s = r0.simulations[0]
for _t in s.ts_[1:]:
    ax = grid.next()
    _state = s.get_state(_t, image=False)
    _n_sim = _state.get_density_x().sum(axis=0)
    plt.plot(_state.basis.xyz[0], _n_sim)
    plt.text(0.2, 0.9, 't_wait={}'.format(_t),
             horizontalalignment='right',
             verticalalignment='center', transform=ax.transAxes)
    
    plt.legend()
    #plt.savefig("NoSOC_expVsim")
    #plt.xlim(-20,-10)




# -



# +
# %pylab inline --no-import-all
from gpe.soc import ExperimentBarrier, u
import SOC.soc_catch_and_release#;reload(SOC.soc_catch_and_release)
from SOC.soc_catch_and_release import ExperimentCatchAndRelease

from gpe.plot_utils import MPLGrid
plt.figure(figsize=(15, 3*1))
grid = MPLGrid(direction='down', space=0.1)

#data directory
directory = '_data/ExperimentCatchAndRelease/x_TF=205.0/Nx=8000/Lx=480.0/t__wait=50/barrier_depth_nK=-138/t__barrier=10/cooling_phase=1.0/basis_type=axial/'

#Sim data
import runs_car_fast
r0 = runs_car_fast.Run_barrier()
sim = r0.simulations[0]
params = sim.experiment


args = dict(Lx=params.Lx,
            Nx=params.Nx,
            basis_type=params.basis_type,
            tube=params.tube,
            cooling_phase=params.cooling_phase,
            
            detuning_kHz=params.detuning_kHz, 
            rabi_frequency_E_R=params.rabi_frequency_E_R,
            barrier_depth_nK=params.barrier_depth_nK,
            x_TF=params.x_TF,
            
            t__wait=params.t__wait,
            t__image=params.t__image,
            t__barrier=params.t__barrier,
            t__final=params.t__final,
            )

ee = ExperimentCatchAndRelease(**args)
s = ee.get_state()

ts = np.arange(0,50+1)
for _t in ts[1:3]:
    ax = grid.next()
    filename = 'frame_{}.0000.npy'.format(_t)
    #filename = 'frame_{}.0000_image_10.1000.npy'.format(_t)
    s[...] = np.load(directory+filename)
    s.t = _t
    _n_sim = s.get_density_x().sum(axis=0)
    _n_sim_tot = s.get_N()
    plt.plot(s.basis.xyz[0], _n_sim/_n_sim_tot, alpha=0.7)
    plt.text(0.1, 0.9, 't={:.3f}ms'.format(s.t),
             horizontalalignment='left',
             verticalalignment='center', transform=ax.transAxes)
    
    

# -

# # Match Expansion Image
#

# +
# %pylab inline --no-import-all
from gpe.soc import ExperimentBarrier, u
import SOC.soc_car_fast#;reload(SOC.soc_car_fast)
from SOC.soc_car_fast import ExperimentCARFast, ExperimentCARFastSmall


from gpe.plot_utils import MPLGrid
plt.figure(figsize=(15, 3*2))
grid = MPLGrid(direction='down', space=0.1)


#Sim data
import runs_car_fast_final
#r0 = runs_car_fast_final.Run0()
#r0 = runs_car_fast_final.Run_IPG()
r0 = runs_car_fast_final.Run_IPG_small()
sim  = r0.simulations[0]
params = sim.experiment
# -



# ## Plot Radial sims 

# +
# %pylab inline --no-import-all
from gpe.soc import ExperimentBarrier, u
import SOC.soc_car_fast#;reload(SOC.soc_car_fast)
from SOC.soc_car_fast import ExperimentCARFast, ExperimentCARFastSmall


from gpe.plot_utils import MPLGrid
plt.figure(figsize=(15*2, 3*2*5))
grid = MPLGrid(direction='down', space=.1)


#Sim data
import runs_car_fast_final
#r0 = runs_car_fast_final.Run0()
#r0 = runs_car_fast_final.Run_IPG()
#r0 = runs_car_fast_final.Run_IPG_small()
r0 = runs_car_fast_final.Run_IPG_small_xTF150()
sim  = r0.simulations[0]
#params = sim.experiment


#ts = np.arange(0,31.5,0.5)
#ts = np.arange(1,50,0.1)
ts = np.array([10, 16, 26, 36, 46])
#ts = np.array([10, 16])

for _t in ts:
    ax = grid.next()
    s = sim.get_state(t_=_t, image=True);
    #args = dict(vmin=0,vmax=650)
    x = s.xyz[0]
    r = s.xyz[1][0]
    r = np.concatenate((-r[::-1],r))
    ns = s.get_density()
    radial_ns =  np.concatenate((ns[:,:,::-1], ns), axis=2)
    #plt.title('t_wait={}ms, t_exp=10.1ms'.format(_t-10))
    imcontourf(x, r, 
               radial_ns.sum(axis=0), 
               interpolation='none',
               #diverging=True,
               aspect=1,
               #**args
              )

    #plt.ylabel(r'$r$ [micron]')# , size='xx-large')
plt.xlabel(r'$x$ [micron]', size='xx-large')
plt.legend()

#name = 'fast_axial_small_Barrier30nK_IMAGE'
#plt.savefig(name);

    
    
# -







# ## compare xTF, radial sims vs exp 

# +
# %pylab inline --no-import-all
from gpe.soc import ExperimentBarrier, u
import SOC.soc_car_fast#;reload(SOC.soc_car_fast)
from SOC.soc_car_fast import ExperimentCARFast, ExperimentCARFastSmall


from gpe.plot_utils import MPLGrid
fig = plt.figure(figsize=(15*2, 3*2*5*2))
#fig = plt.figure(figsize=(15*3, 6))

grid = MPLGrid(direction='down', space=.1)





master_grid = MPLGrid(fig=fig,
                       direction='down',
                       #right=False,
                       #scale=True,
                       #space=0.1, 
                       #share=True,
                       size=1.
                      )

grid = master_grid.grid(direction='down',
                        #right=False,
                        #scale=True,
                        #space=1.5, 
                        share=True,
                        #size=1
                       )


#Sim data
import runs_car_fast_final
#r0 = runs_car_fast_final.Run0()
#r0 = runs_car_fast_final.Run_IPG()
r1 = runs_car_fast_final.Run_IPG_small()
sim1  = r1.simulations[1]
r0 = runs_car_fast_final.Run_IPG_small_xTF150()
sim  = r0.simulations[1]
#params = sim.experiment


import DATA.data_190425 as data_module;reload(data_module)
d = data_module.data['C_R_fast_30uW.txt']

from gpe.plot_utils import MPLGrid
plt.figure(figsize=(20, 3*5*2))
grid = MPLGrid(direction='down', space=0.1)

    

xL = 250
xR = 750
yD = 50
yU = 400
y_off = 500



#ts = np.arange(0,31.5,0.5)
#ts = np.arange(1,50,0.1)
ts = np.array([16, 26, 36, 46])
#ts = np.array([10, 16])


grids = [grid.grid(direction='down',
                  #right=False,
                  #scale=True,
                  #space=1., 
                  share=True,
                  #size=1
                 )
         for _t in ts]

for _t in ts:
    _ind = np.where(_t==ts)[0][0]
    ax = grids[_ind].next()
    s = sim.get_state(t_=_t, image=True);
    #args = dict(vmin=0,vmax=650)
    x = s.xyz[0]
    r = s.xyz[1][0]
    r = np.concatenate((-r[::-1],r))
    ns = s.get_density()
    n_scale = np.trapz(s.get_density_x().sum(axis=0), s.xyz[0].ravel())
    radial_ns =  np.concatenate((ns[:,:,::-1], ns), axis=2)
    plt.title('t_wait={}ms, t_exp=10.1ms'.format(_t-10))
    imcontourf(x, r, 
               radial_ns.sum(axis=0)/n_scale, 
               interpolation='none',
               #diverging=True,
               aspect=1,
               #**args
              )
    plt.ylabel(r'x_TF=150', size='xx-large')
    ax = grids[_ind].next()
    _x,y,n = d.load(t_wait=_t-10, t_exp=10.1)
    n = n[0]
    x_offset = 14
    _x = _x[xL:xR] - (_x[xL:xR][-1] + _x[xL:xR][0])/2 - x_offset
    y = y[yD:yU] - y[yD:yU][0]
    #n_1D = n[xL:xR, yD:yU].sum(axis=1) - n[xL:xR, yD+y_off:yU+y_off].sum(axis=1)    
    n = n[xL:xR, yD:yU] - n[xL:xR, yD+y_off:yU+y_off]
    n_scale = np.trapz(n.sum(axis=1), x[xL:xR].ravel())
    _x_ind = np.where(abs(_x)<x[-1])[0]
    y_diff = r[-1]-r[0]
    _y_ind_U = np.where(y>55+y_diff)[0][0]
    _y_ind_D = np.where(y>55)[0][0]
    imcontourf(_x[_x_ind[0]:_x_ind[-1]],
               y[_y_ind_D:_y_ind_U],
               n[_x_ind[0]:_x_ind[-1], _y_ind_D:_y_ind_U]/n_scale,
               interpolation='none',
               #diverging=True,
               aspect=1,
               #**args
              )
    #plt.xlim(x[0],x[-1])
    #plt.ylim(0,200)
    
    ax = grids[_ind].next()
    s = sim1.get_state(t_=_t, image=True);
    #args = dict(vmin=0,vmax=650)
    x = s.xyz[0]
    r = s.xyz[1][0]
    r = np.concatenate((-r[::-1],r))
    ns = s.get_density()
    n_scale = np.trapz(s.get_density_x().sum(axis=0), s.xyz[0].ravel())
    radial_ns =  np.concatenate((ns[:,:,::-1], ns), axis=2)
    #plt.title('t_wait={}ms, t_exp=10.1ms'.format(_t-10))
    imcontourf(x, r, 
               radial_ns.sum(axis=0)/n_scale, 
               interpolation='none',
               #diverging=True,
               aspect=1,
               #**args
              )
    plt.ylabel(r'x_TF=205', size='xx-large')


    #plt.ylabel(r'$r$ [micron]')# , size='xx-large')
plt.xlabel(r'$x$ [micron]', size='xx-large')
plt.legend()

name = 'CAR_fast_small_Barrier60nK_xTF_compariosn_IMAGE'
#plt.savefig(name);

    
    
# -



# ## Data v Sim 

from DATA import data_190425 as expt_soc
expt_soc = {30: expt_soc.data['C_R_fast_15uW.txt'],
                    60: expt_soc.data['C_R_fast_30uW.txt'],
                    90: expt_soc.data['C_R_fast_45uW.txt']}
expt_soc[30]

# +
# %pylab inline --no-import-all
from gpe.soc import ExperimentBarrier, u
import SOC.soc_car_fast#;reload(SOC.soc_car_fast)
from SOC.soc_car_fast import ExperimentCARFast, ExperimentCARFastSmall

from gpe.plot_utils import MPLGrid
plt.figure(figsize=(20, 3*5*2))
grid = MPLGrid(direction='down', space=0.1)


#Sim data
import runs_car_fast_final
#r0 = runs_car_fast_final.Run0()
#r0 = runs_car_fast_final.Run_IPG()
#r1 = runs_car_fast_final.Run_IPG_small()
#sim1  = r1.simulations[1]
r0 = runs_car_fast_final.Run_IPG_xTF150()
sim  = r0.simulations[0]
#params = sim.experiment


import DATA.data_190425 as data_module;reload(data_module)
d = data_module.data['C_R_fast_15uW.txt']
 


#Select ROI box, in micron    
xL = 250
xR = 750
yD = 50
yU = 400
y_off = 500 #Usess a new ROI shifter up by y_off to subtract off background



#ts = np.arange(0,31.5,0.5)
#ts = np.arange(1,50,0.1)
ts = np.array([16, 26, 36, 46])
#ts = np.array([10, 16])


grids = [grid.grid(direction='down',
                  #right=False,
                  #scale=True,
                  #space=1., 
                  share=True,
                  #size=1
                 )
         for _t in ts]

for _t in ts:
    _ind = np.where(_t==ts)[0][0]
    ax = grids[_ind].next()
    s = sim.get_state(t_=_t, image=True);
    #args = dict(vmin=0,vmax=650)
    x = s.xyz[0]
    r = s.xyz[1][0]
    r = np.concatenate((-r[::-1],r))
    ns = s.get_density()
    n_scale = np.trapz(s.get_density_x().sum(axis=0), s.xyz[0].ravel())
    radial_ns =  np.concatenate((ns[:,:,::-1], ns), axis=2)
    plt.title('t_wait={}ms, t_exp=10.1ms'.format(_t-10))
    imcontourf(x, r, 
               radial_ns.sum(axis=0)/n_scale, 
               interpolation='none',
               #diverging=True,
               aspect=1,
               #**args
              )
    
    ax = grids[_ind].next()
    
    ##### Experiment
    _x,y,n = d.load(t_wait=_t-10, t_exp=10.1)
    n = n[0]
    x_offset = 4  # shifts left or right, in micron
    y_thresh = 55 #Bottom on ROI, in micron 

    _x = _x[xL:xR] - (_x[xL:xR][-1] + _x[xL:xR][0])/2 - x_offset
    y = y[yD:yU] - y[yD:yU][0]
    #n_1D = n[xL:xR, yD:yU].sum(axis=1) - n[xL:xR, yD+y_off:yU+y_off].sum(axis=1)    
    n = n[xL:xR, yD:yU] - n[xL:xR, yD+y_off:yU+y_off]
    n_scale = np.trapz(n.sum(axis=1), x[xL:xR].ravel())
    _x_ind = np.where(abs(_x)<x[-1])[0]
    y_diff = r[-1]-r[0] #radial size of simulatuion, in micron
    _y_ind_U = np.where(y>y_thresh+y_diff)[0][0]
    print(y_diff)
    _y_ind_D = np.where(y>y_thresh)[0][0]
    imcontourf(_x[_x_ind[0]:_x_ind[-1]],
               y[_y_ind_D:_y_ind_U],
               n[_x_ind[0]:_x_ind[-1], _y_ind_D:_y_ind_U]/n_scale,
               interpolation='none',
               #diverging=True,
               aspect=1,
               #**args
              )
    #plt.xlim(x[0],x[-1])
    #plt.ylim(0,200)
    
    

    #plt.ylabel(r'$r$ [micron]')# , size='xx-large')
plt.xlabel(r'$x$ [micron]', size='xx-large')
plt.legend()

name = 'CAR_fast_small_Barrier60nK_xTF_compariosn_IMAGE'
#plt.savefig(name);

    
    
# -



# # Different Plot Functions 

# Theses plotting functions are for making movies below. The movie making software needs individual frames so the changing parameters are passed to the plot function making each frame. 

# ## Unknown func 

# +

from mmfutils.plot import imcontourf


def plot_func(state, fig=None):
    from mmfutils.plot import imcontourf
    if fig is None:
        fig = plt.figure(figsize=(15,15))
    fig.clear()
    
    master_grid = MPLGrid(fig=fig,
                           direction='right',
                           #right=False,
                           #scale=True,
                           space=0.1, 
                           share=False,
                           size=0.02
                          )

    grid = master_grid.grid(direction='down',
                            #right=False,
                            #scale=True,
                            space=0.1, 
                            #share=True,
                            size=1
                           )
    sub_grid = grid.grid(direction='down',
                            #right=False,
                            #scale=True,
                            space=0.1, 
                            #share=True,
                            size=1
                        )

    cb_grid = master_grid.grid(direction='down',
                               right=True,
                               #scale=True,
                               space=0, 
                               share=True,
                               size=0.01
                              )
 
    x = state.basis.xyz[0]
    r = state.basis.xyz[1][0]

    ax = ax1 = sub_grid.next()
    ax.plot(x, state.get_density_x().sum(axis=0), label=r'n')
    ax.plot(x, state.get_density_x()[0],ls=':', label=r'n_a')
    ax.plot(x, state.get_density_x()[1],ls=':', label=r'n_b')
    plt.ylim(0,21000)
    ax.set_ylabel(r'$n$', size='xx-large')
    title = r'$t={:2f}$ ms'.format(state.t) 
    ax.set_title(title);
    ax.legend();

    ax = ax2 = grid.next()
    cut0 = x.shape[0]//2
    cut1 = np.where(x>100)[0][0]
    imcontourf(x[cut0:cut1], r, 
               state.get_density().sum(axis=0)[cut0:cut1,...], 
               interpolation='none',
               #diverging=True,
               #aspect=1,
              )
    ax.set_ylabel(r'Right', size='xx-large')
    ax = ax2 = grid.next()
    cut2 = np.where(x<-100)[0][-1]
    imcontourf(x[cut0:cut1], r, 
               state.get_density().sum(axis=0)[cut2:cut0,...][::-1], 
               interpolation='none',
               #diverging=True,
               #aspect=1,
              )
    ax.set_ylabel(r'Left', size='xx-large')
    ax.set_xlabel(r'x [micron]', size='xx-large')
    
    _ax = cb_grid.next()
    _ax.set_visible(False)
    cax = cb_grid.next()
    cb = plt.colorbar(cax=cax)
    plt.clim(0,800)
    cb.set_label('n', size='xx-large')
    
    return fig


# +
_t = 7
filename = 'frame_{}.0000.npy'.format(_t)
s[...] = np.load(directory+filename)
s.t = _t
    


plot_func(s, fig=None);
# -





# ## Main function

# +

from mmfutils.plot import imcontourf
from gpe.plot_utils import MPLGrid
#plt.figure(figsize=(15, 3*5))
#grid = MPLGrid(direction='down', space=0.1)

def plot_func(state, fig=None):
    from mmfutils.plot import imcontourf
    if fig is None:
        fig = plt.figure(figsize=(15,6))
    fig.clear()
    
    master_grid = MPLGrid(fig=fig,
                           direction='right',
                           #right=False,
                           #scale=True,
                           space=0.1, 
                           share=False,
                           size=0.02
                          )

    grid = master_grid.grid(direction='down',
                            #right=False,
                            #scale=True,
                            space=0.1, 
                            #share=True,
                            size=1
                           )
    sub_grid = grid.grid(direction='down',
                            #right=False,
                            #scale=True,
                            space=0.1, 
                            #share=True,
                            size=2
                        )

    cb_grid = master_grid.grid(direction='down',
                               right=True,
                               #scale=True,
                               space=0, 
                               share=True,
                               size=0.01
                              )
 
    x = state.xyz[0]
    r = state.xyz[1][0]
    r = np.concatenate((-r[::-1],r))
    #r = np.linspace(-r[-1],r[-1],2*r.shape[0])

    ax = ax1 = sub_grid.next()
    ax.plot(x, state.get_density_x().sum(axis=0), label=r'n')
    ax.plot(x, state.get_density_x()[0],ls=':', label=r'n_a')
    ax.plot(x, state.get_density_x()[1],ls=':', label=r'n_b')
    plt.ylim(0,5500)
    ax.set_ylabel(r'$n$', size='xx-large')
    title = r'$t={:2f}$ms, $ \qquad $ Single Band={}, $\qquad $xTF$={}$micron '.format(state.t/state.t_unit,
                                                                                       state.single_band,
                                                                                       150) 
    ax.set_title(title);
    ax.legend();

    ax = ax2 = grid.next()
    args = dict(vmin=0,vmax=500)
    cut0 = x.shape[0]//2
    cut1 = np.where(x>100)[0][0]
    ns = state.get_density()
    #radial_ns =  np.concatenate((ns[:,:,::-1], ns), axis=2)
    radial_ns =  np.concatenate((ns[:,::-1], ns), axis=1)
    imcontourf(x[cut0:cut1], r, 
               #radial_ns.sum(axis=0)[cut0:cut1,...], 
               radial_ns[cut0:cut1,...], 
               interpolation='none',
               #diverging=True,
               aspect=1,
               **args
              )
    ax.set_ylabel(r'Right', size='xx-large')
    ax = ax2 = grid.next()
    cut2 = np.where(x<-100)[0][-1]
    imcontourf(x[cut0:cut1], r, 
               #radial_ns.sum(axis=0)[cut2:cut0,...][::-1], 
               radial_ns[cut2:cut0,...][::-1], 
               interpolation='none',
               #diverging=True,
               aspect=1,
               **args
              )
    ax.set_ylabel(r'Left', size='xx-large')
    ax.set_xlabel(r'x [micron]', size='xx-large')
    
    _ax = cb_grid.next()
    _ax.set_visible(False)
    cax = cb_grid.next()
    cb = plt.colorbar(cax=cax)
    cb.set_label('n', size='xx-large')
    
    return fig


# +
_t_ = 9.5
s = sim.get_state(t_=_t_, image=False)
    


plot_func(s, fig=None);

# -



# ## compare 1 vs 2 bands 

# +

from mmfutils.plot import imcontourf
from gpe.plot_utils import MPLGrid
#plt.figure(figsize=(15, 3*5))
#grid = MPLGrid(direction='down', space=0.1)

def plot_func_2(state0, state1, fig=None):
    from mmfutils.plot import imcontourf
    if fig is None:
        fig = plt.figure(figsize=(20,6))
    fig.clear()
    
    master_grid = MPLGrid(fig=fig,
                           direction='right',
                           right=False,
                           #scale=True,
                           space=0.05, 
                           share=False,
                           size=0.02
                          )

    grid = master_grid.grid(direction='down',
                            #right=False,
                            #scale=True,
                            space=0.01, 
                            share=True,
                            size=1
                           )

    cb_grid = master_grid.grid(direction='down',
                               #right=True,
                               #scale=True,
                               space=0.1, 
                               #share=True,
                               size=0.01
                              )
    xlim = 100
    state = state0
    x = state.xyz[0]
    xi = np.where(abs(x)<xlim)[0]

    r = state.xyz[1][0]
    r = np.concatenate((-r[::-1],r))

    ############################
    ax = ax1 = grid.next()
    ax.plot(x[xi[0]:xi[-1]], state.get_density_x().sum(axis=0)[xi[0]:xi[-1]])#, label=r'n')
    #ax.plot(x[xi[0]:xi[-1]], state.get_density_x()[0][xi[0]:xi[-1]],ls=':', label=r'n_a')
    #ax.plot(x[xi[0]:xi[-1]], state.get_density_x()[1][xi[0]:xi[-1]],ls=':', label=r'n_b')
    #plt.ylim(0,5700)
    ax.set_ylabel(r'Not SB ', size='xx-large')
    time = state.t/state.t_unit - 10
    title = r'$t={:.1f}$ms, $\qquad $xTF$={}$micron, $\qquad $Barrier Depth$={}$nK '.format(time,
                                                                                            150,
                                                                                            state.experiment.barrier_depth_nK) 
    ax.set_title(title)
    ax.legend()
    ax.set_xlim(-xlim,xlim)

    
    ############################
    ax = ax2 = grid.next()
    args = dict(vmin=0,vmax=500)
    ns = state.get_density()
    radial_ns =  np.concatenate((ns[:,:,::-1], ns), axis=2)
    #radial_ns =  np.concatenate((ns[:,::-1], ns), axis=1)
    imcontourf(x[xi[0]:xi[-1]], r, 
               radial_ns.sum(axis=0)[xi[0]:xi[-1],...], 
               #radial_ns[xi[0]:xi[-1],...], 
               interpolation='none',
               #diverging=True,
               aspect=1,
               #**args
              )
    ax.set_xlim(-xlim,xlim)
        
        
    state = state1
    x = state.xyz[0]
    xi = np.where(abs(x)<xlim)[0]
    r = state.xyz[1][0]
    r = np.concatenate((-r[::-1],r))
    
    _ax = cb_grid.next()
    _ax.set_visible(False)
    cax = cb_grid.next()
    cb = plt.colorbar(cax=cax)
    #cb.set_label('n', size='xx-large')
    
    ############################
    ax = ax3 = grid.next()
    ns = state.get_density()
    #radial_ns =  np.concatenate((ns[:,:,::-1], ns), axis=2)
    radial_ns =  np.concatenate((ns[:,::-1], ns), axis=1)
    imcontourf(x[xi[0]:xi[-1]], r, 
               #radial_ns.sum(axis=0)[xi[0]:xi[-1],...], 
               radial_ns[xi[0]:xi[-1],...], 
               interpolation='none',
               #diverging=True,
               aspect=1,
               #**args
              )
    #ax.set_ylabel(r'Right', size='xx-large')
    ax.set_xlim(-xlim,xlim)
    
    
    cax = cb_grid.next()
    cb = plt.colorbar(cax=cax)
    #cb.set_label('n', size='xx-large')
    _ax = cb_grid.next()
    _ax.set_visible(False)
    
    ############################
    ax = ax4 = grid.next()
    ax.plot(x[xi[0]:xi[-1]], state.get_density_x().sum(axis=0)[xi[0]:xi[-1]])#, label=r'n')
    #ax.plot(x[xi[0]:xi[-1]], state.get_density_x()[0][xi[0]:xi[-1]],ls=':', label=r'n_a')
    #ax.plot(x[xi[0]:xi[-1]], state.get_density_x()[1][xi[0]:xi[-1]],ls=':', label=r'n_b')
    #plt.ylim(0,5700)
    ax.set_ylabel('SB', size='xx-large')
    ax.legend();
    ax.set_xlim(-xlim,xlim)
    #plt.ylim(0,3500)
    
    
    ax.set_xlabel(r'x [micron]', size='xx-large')
    

    

    plt.tight_layout()

    
    return fig

# +
_t_ = 0+10
s0 = sim0.get_state(t_=_t_, image=False)
s1 = sim1.get_state(t_=_t_, image=False)
    


plot_func_2(s0, s1, fig=plt.figure(figsize=(30,5)));

# -





# ## Tube v Axial 

# +

from mmfutils.plot import imcontourf
from gpe.plot_utils import MPLGrid
#plt.figure(figsize=(15, 3*5))
#grid = MPLGrid(direction='down', space=0.1)

def plot_func_2(state0, state1, fig=None):
    from mmfutils.plot import imcontourf
    if fig is None:
        fig = plt.figure(figsize=(20,6))
    fig.clear()
    
    master_grid = MPLGrid(fig=fig,
                           direction='right',
                           right=False,
                           #scale=True,
                           space=0.05, 
                           share=True,
                           size=0.02
                          )

    grid = master_grid.grid(direction='down',
                            #right=False,
                            #scale=True,
                            space=0.01, 
                            share=True,
                            size=1
                           )

    cb_grid = master_grid.grid(direction='down',
                               #right=True,
                               #scale=True,
                               space=0.1, 
                               #share=True,
                               size=0.01
                              )
    xlim = 100
    state = state0
    x = state.xyz[0]
    xi = np.where(abs(x)<xlim)[0]

    r = state.xyz[1][0]
    r = np.concatenate((-r[::-1],r))

    ############################
    ax = ax1 = grid.next()
    ax.plot(x[xi[0]:xi[-1]], state.get_density_x().sum(axis=0)[xi[0]:xi[-1]])#, label=r'n')
    #ax.plot(x[xi[0]:xi[-1]], state.get_density_x()[0][xi[0]:xi[-1]],ls=':', label=r'n_a')
    #ax.plot(x[xi[0]:xi[-1]], state.get_density_x()[1][xi[0]:xi[-1]],ls=':', label=r'n_b')
    #plt.ylim(0,5000)
    ax.set_ylabel(r't_exp = 0ms', size='xx-large')
    time = state.t/state.t_unit - 10
    title = r't_wait$={:.1f}$ms, $\qquad $xTF$={}$micron, $\qquad $Barrier Depth$={}$nK '.format(time,
                                                                                            150,
                                                                                            state.experiment.barrier_depth_nK) 
    ax.set_title(title)
    ax.legend()
    ax.set_xlim(-xlim,xlim)

    
    ############################
    ax = ax2 = grid.next()
    args = dict(vmin=0,vmax=450)
    ns = state.get_density()
    #radial_ns =  np.concatenate((ns[:,:,::-1], ns), axis=2)
    radial_ns =  np.concatenate((ns[:,::-1], ns), axis=1)
    imcontourf(x[xi[0]:xi[-1]], r, 
               #radial_ns.sum(axis=0)[xi[0]:xi[-1],...], 
               radial_ns[xi[0]:xi[-1],...], 
               interpolation='none',
               #diverging=True,
               #aspect=1,
               #**args
              )
    ax.set_xlim(-xlim,xlim)
        
        
    ############################
    state = state1
    x_image_offset = 3.5  # State moves during expansion
    x = state.xyz[0] -  x_image_offset
    xi = np.where(abs(x)<xlim)[0]
    r = state.xyz[1][0]
    r = np.concatenate((-r[::-1],r))

    _ax = cb_grid.next()
    _ax.set_visible(False)
    cax = cb_grid.next()
    cb = plt.colorbar(cax=cax)
    #cb.set_label('n', size='xx-large')
    
    ax = ax3 = grid.next()
    args = dict(vmin=0,vmax=2.5)
    
    ns = state.get_density()
    #radial_ns =  np.concatenate((ns[:,:,::-1], ns), axis=2)
    radial_ns =  np.concatenate((ns[:,::-1], ns), axis=1)
    imcontourf(x[xi[0]:xi[-1]], r, 
               #radial_ns.sum(axis=0)[xi[0]:xi[-1],...], 
               radial_ns[xi[0]:xi[-1],...], 
               interpolation='none',
               #diverging=True,
               #aspect=1,
               #**args
              )
    #ax.set_ylabel(r'Right', size='xx-large')
    ax.set_xlim(-xlim,xlim)
    
    
    cax = cb_grid.next()
    cb = plt.colorbar(cax=cax)
    #cb.set_label('n', size='xx-large')
    _ax = cb_grid.next()
    _ax.set_visible(False)
    
    ############################
    ax = ax4 = grid.next()
    ax.plot(x[xi[0]:xi[-1]], state.get_density_x().sum(axis=0)[xi[0]:xi[-1]])#, label=r'n')
    #ax.plot(x[xi[0]:xi[-1]], state.get_density_x()[0][xi[0]:xi[-1]],ls=':', label=r'n_a')
    #ax.plot(x[xi[0]:xi[-1]], state.get_density_x()[1][xi[0]:xi[-1]],ls=':', label=r'n_b')
    #plt.ylim(0,5000)
    ax.set_ylabel(r't_exp = 10.1ms', size='xx-large')
    ax.legend();
    ax.set_xlim(-xlim,xlim)
    
    
    ax.set_xlabel(r'x [micron]', size='xx-large')
    

    

    plt.tight_layout()

    
    return fig
# -



# +
_t_ = 8+10
s0 = sim.get_state(t_=_t_, image=False)
s1 = sim.get_state(t_=_t_, image=True)
    


plot_func_2(s0, s1, fig=plt.figure(figsize=(32,5)));

# -



# ## Image v NonImage 



# +

from mmfutils.plot import imcontourf
from gpe.plot_utils import MPLGrid
#plt.figure(figsize=(15, 3*5))
#grid = MPLGrid(direction='down', space=0.1)

def plot_func_2(state0, state1, fig=None):
    from mmfutils.plot import imcontourf
    if fig is None:
        fig = plt.figure(figsize=(20,6))
    fig.clear()
    
    master_grid = MPLGrid(fig=fig,
                           direction='right',
                           right=False,
                           #scale=True,
                           space=0.05, 
                           share=True,
                           size=0.02
                          )

    grid = master_grid.grid(direction='down',
                            #right=False,
                            #scale=True,
                            space=0.01, 
                            share=True,
                            size=1
                           )

    cb_grid = master_grid.grid(direction='down',
                               #right=True,
                               #scale=True,
                               space=0.1, 
                               #share=True,
                               size=0.01
                              )
    xlim = 100
    state = state0
    x = state.xyz[0]
    xi = np.where(abs(x)<xlim)[0]

    r = state.xyz[1][0]
    r = np.concatenate((-r[::-1],r))

    ############################
    ax = ax1 = grid.next()
    ax.plot(x[xi[0]:xi[-1]], state.get_density_x().sum(axis=0)[xi[0]:xi[-1]])#, label=r'n')
    #ax.plot(x[xi[0]:xi[-1]], state.get_density_x()[0][xi[0]:xi[-1]],ls=':', label=r'n_a')
    #ax.plot(x[xi[0]:xi[-1]], state.get_density_x()[1][xi[0]:xi[-1]],ls=':', label=r'n_b')
    #plt.ylim(0,5000)
    ax.set_ylabel(r't_exp = 0ms', size='xx-large')
    time = state.t/state.t_unit - 10
    title = r't_wait$={:.1f}$ms, $\qquad $xTF$={}$micron, $\qquad $Barrier Depth$={}$nK '.format(time,
                                                                                            150,
                                                                                            state.experiment.barrier_depth_nK) 
    ax.set_title(title)
    ax.legend()
    ax.set_xlim(-xlim,xlim)

    
    ############################
    ax = ax2 = grid.next()
    args = dict(vmin=0,vmax=450)
    ns = state.get_density()
    #radial_ns =  np.concatenate((ns[:,:,::-1], ns), axis=2)
    radial_ns =  np.concatenate((ns[:,::-1], ns), axis=1)
    imcontourf(x[xi[0]:xi[-1]], r, 
               #radial_ns.sum(axis=0)[xi[0]:xi[-1],...], 
               radial_ns[xi[0]:xi[-1],...], 
               interpolation='none',
               #diverging=True,
               #aspect=1,
               #**args
              )
    ax.set_xlim(-xlim,xlim)
        
        
    ############################
    state = state1
    x_image_offset = 3.5  # State moves during expansion
    x = state.xyz[0] -  x_image_offset
    xi = np.where(abs(x)<xlim)[0]
    r = state.xyz[1][0]
    r = np.concatenate((-r[::-1],r))

    _ax = cb_grid.next()
    _ax.set_visible(False)
    cax = cb_grid.next()
    cb = plt.colorbar(cax=cax)
    #cb.set_label('n', size='xx-large')
    
    ax = ax3 = grid.next()
    args = dict(vmin=0,vmax=2.5)
    
    ns = state.get_density()
    #radial_ns =  np.concatenate((ns[:,:,::-1], ns), axis=2)
    radial_ns =  np.concatenate((ns[:,::-1], ns), axis=1)
    imcontourf(x[xi[0]:xi[-1]], r, 
               #radial_ns.sum(axis=0)[xi[0]:xi[-1],...], 
               radial_ns[xi[0]:xi[-1],...], 
               interpolation='none',
               #diverging=True,
               #aspect=1,
               #**args
              )
    #ax.set_ylabel(r'Right', size='xx-large')
    ax.set_xlim(-xlim,xlim)
    
    
    cax = cb_grid.next()
    cb = plt.colorbar(cax=cax)
    #cb.set_label('n', size='xx-large')
    _ax = cb_grid.next()
    _ax.set_visible(False)
    
    ############################
    ax = ax4 = grid.next()
    ax.plot(x[xi[0]:xi[-1]], state.get_density_x().sum(axis=0)[xi[0]:xi[-1]])#, label=r'n')
    #ax.plot(x[xi[0]:xi[-1]], state.get_density_x()[0][xi[0]:xi[-1]],ls=':', label=r'n_a')
    #ax.plot(x[xi[0]:xi[-1]], state.get_density_x()[1][xi[0]:xi[-1]],ls=':', label=r'n_b')
    #plt.ylim(0,5000)
    ax.set_ylabel(r't_exp = 10.1ms', size='xx-large')
    ax.legend();
    ax.set_xlim(-xlim,xlim)
    
    
    ax.set_xlabel(r'x [micron]', size='xx-large')
    

    

    plt.tight_layout()

    
    return fig

# +
_t_ = 8+10
s0 = sim.get_state(t_=_t_, image=False)
s1 = sim.get_state(t_=_t_, image=True)
    


plot_func_2(s0, s1, fig=plt.figure(figsize=(32,5)));

# -





# ## Old func 

# +

from mmfutils.plot import imcontourf


def plot_func(state, fig=None):
    from mmfutils.plot import imcontourf
    if fig is None:
        fig = plt.figure(figsize=(15,6))
    fig.clear()
    
    master_grid = MPLGrid(fig=fig,
                           direction='right',
                           #right=False,
                           #scale=True,
                           space=0.1, 
                           share=False,
                           size=0.02
                          )

    grid = master_grid.grid(direction='down',
                            #right=False,
                            #scale=True,
                            space=0.1, 
                            #share=True,
                            size=1
                           )
    sub_grid = grid.grid(direction='down',
                            #right=False,
                            #scale=True,
                            space=0.1, 
                            #share=True,
                            size=2
                        )

    cb_grid = master_grid.grid(direction='down',
                               right=True,
                               #scale=True,
                               space=0, 
                               share=True,
                               size=0.01
                              )
 
    x = state.basis.xyz[0]
    r = state.basis.xyz[1][0]
    r = np.concatenate((-r[::-1],r))
    #r = np.linspace(-r[-1],r[-1],2*r.shape[0])

    ax = ax1 = sub_grid.next()
    ax.plot(x, state.get_density_x().sum(axis=0), label=r'n')
    ax.plot(x, state.get_density_x()[0],ls=':', label=r'n_a')
    ax.plot(x, state.get_density_x()[1],ls=':', label=r'n_b')
    plt.ylim(0,10000)
    ax.set_ylabel(r'$n$', size='xx-large')
    title = r'$t={:2f}$ ms'.format(state.t) 
    ax.set_title(title);
    ax.legend();

    ax = ax2 = grid.next()
    args = dict(vmin=0,vmax=650)
    cut0 = x.shape[0]//2
    cut1 = np.where(x>100)[0][0]
    ns = state.get_density()
    radial_ns =  np.concatenate((ns[:,:,::-1], ns), axis=2)
    imcontourf(x[cut0:cut1], r, 
               radial_ns.sum(axis=0)[cut0:cut1,...], 
               interpolation='none',
               #diverging=True,
               aspect=1,
               **args
              )
    ax.set_ylabel(r'Right', size='xx-large')
    ax = ax2 = grid.next()
    cut2 = np.where(x<-100)[0][-1]
    imcontourf(x[cut0:cut1], r, 
               radial_ns.sum(axis=0)[cut2:cut0,...][::-1], 
               interpolation='none',
               #diverging=True,
               aspect=1,
               **args
              )
    ax.set_ylabel(r'Left', size='xx-large')
    ax.set_xlabel(r'x [micron]', size='xx-large')
    
    _ax = cb_grid.next()
    _ax.set_visible(False)
    cax = cb_grid.next()
    cb = plt.colorbar(cax=cax)
    #plt.clim(0,800)
    cb.set_label('n', size='xx-large')
    
    return fig
# -





# ## Wigner de Vills 

# +
from mmfutils.plot import imcontourf
from mmfutils.math import wigner
from gpe.plot_utils import MPLGrid


def plot_func_wigner(state, fig=None):
    if fig is None:
        fig = plt.figure(figsize=(15,15))
    #plt.clf()
    fig.clear()
    #pe = state.plot(history=history)
    

    #grid = MPLGrid(fig=fig,
    #               direction='down',
    #               #right=False,
    #               #scale=True,
    #               space=0.1, 
    #               share=True,
    #               size=1
    #              )


    master_grid = MPLGrid(fig=fig,
                           direction='right',
                           #right=False,
                           #scale=True,
                           space=0.1, 
                           share=False,
                           size=1
                          )

    grid = master_grid.grid(direction='down',
                            #right=False,
                            #scale=True,
                            space=0.1, 
                            share=True,
                            size=1
                           )

    cb_grid = master_grid.grid(direction='down',
                               right=True,
                               #scale=True,
                               space=0, 
                               share=True,
                               size=0.05
                              )

    x = state.basis.xyz[0]
    dx = x[1] - x[0]
    r = state.basis.xyz[1][0]
    
    ax = ax1 = grid.next()
    ax.plot(x, state.get_density().sum(axis=0).sum(axis=1), label=r'n')
    ax.plot(x, state.get_density()[0].sum(axis=1),ls=':', label=r'n_a')
    ax.plot(x, state.get_density()[1].sum(axis=1),ls=':', label=r'n_b')
    plt.ylim(0,12000)
    ax.set_ylabel(r'$n$', size='xx-large')
    title = r'$t={:2f}$ ms'.format(state.t) 
    ax.set_title(title);
    ax.legend();

    ax = ax2 = grid.next()
    ws_a, P_a = wigner.wigner_ville(state[...][0].sum(axis=1), dt=dx, skip=1, pad=False)
    #imcontourf(x, ws_a/state.k_r, P_a, interpolation='none', diverging=True)
    y = np.log10(abs(P_a)-abs(P_a).min()+1)
    #dr = 1.3
    #y = np.maximum(np.log10(abs(P_a)/abs(P_a).max() + dr), 0)
    imcontourf(x, ws_a/s.k_r, 
               np.sign(P_a)*y, 
               interpolation='none', diverging=True)
    ax.set_ylabel(r'$k$ [$k_R$]', size='xx-large')
    ax.set_ylim(-2.,2.)

    _ax = cb_grid.next()
    _ax.set_visible(False)
    cax = cb_grid.next()
    cb = plt.colorbar(cax=cax)
    #cb.set_label('ss')
    
    return fig


# -

plot_func_wigner(s, fig=None);







# ## Hist plot 

# +

from mmfutils.plot import imcontourf
from gpe.plot_utils import MPLGrid
#plt.figure(figsize=(15, 3*5))
#grid = MPLGrid(direction='down', space=0.1)

def plot_func(state, fig=None):
    args = dict(
        plot_elements=None,
        fig=fig,
        data=None,
        grid=None,
        show_n=True,
        show_na=True,
        show_nb=True,
        show_log_n=False,
        show_momenta=False,
        show_mixtures=False,
        show_V=False,
        show_history_ab=True,
        show_history_a=False,
        show_history_b=False,
        show_log_history=False,
        combine_momenta_and_history=False,
        dynamic_range=100,
        log_dynamic_range=5,

        parity=False,

        # kwargs for get_momenta_data
        clip_momenta_modes=1,
        k_range_k_r=(-2.5, 2.5),
        nk_amplitude_factor=0.5,
        mu_background=None,

        symmetric=True,
        fig_width=15, pane_height=5,
        space=0.01,
        history=history,
        split=False,
        rescale=False,

        plot_dim=None,

        subplot_spec=None,
        )
    
    fig = state.plot(**args);
    return fig
    


# -

plot_func(history[0])

# +
plot_func(history[0])

filename = 'Fast_Barrier{}nK_History_Expanded.png'.format(abs(barrier_depth_nK))
print(filename)
plt.savefig(filename)
# -





history[2].plot(
    plot_elements=None,
    fig=None,
    data=None,
    grid=None,
    show_n=True,
    show_na=True,
    show_nb=True,
    show_log_n=False,
    show_momenta=False,
    show_mixtures=False,
    show_V=False,
    show_history_ab=True,
    show_history_a=False,
    show_history_b=False,
    show_log_history=False,
    combine_momenta_and_history=False,
    dynamic_range=100,
    log_dynamic_range=5,

    parity=False,

    # kwargs for get_momenta_data
    clip_momenta_modes=1,
    k_range_k_r=(-2.5, 2.5),
    nk_amplitude_factor=0.5,
    mu_background=None,

    symmetric=True,
    fig_width=15, pane_height=5,
    space=0.01,
    history=history,
    split=False,
    rescale=False,

    plot_dim=None,

    subplot_spec=None,
    );



# ## 3D Plots 

# +

from mmfutils.plot import imcontourf
from gpe.plot_utils import MPLGrid
#plt.figure(figsize=(15, 3*5))
#grid = MPLGrid(direction='down', space=0.1)

def plot_func_3(state0, state1, state2, fig=None):
    from mmfutils.plot import imcontourf
    if fig is None:
        fig = plt.figure(figsize=(20,6))
    fig.clear()
    
    master_grid = MPLGrid(fig=fig,
                           direction='right',
                           right=False,
                           #scale=True,
                           space=0.05, 
                           share=False,
                           size=0.02
                          )

    grid = master_grid.grid(direction='down',
                            #right=False,
                            #scale=True,
                            space=0.01, 
                            share=True,
                            size=1
                           )

    cb_grid = master_grid.grid(direction='down',
                               #right=True,
                               #scale=True,
                               space=0.1, 
                               #share=True,
                               size=0.01
                              )
    xlim = 50
    state = state0
    x,y,z = state.xyz
    xi = np.where(abs(x)<xlim)[0]

    
    
    
    ############################
    ax = ax0 = grid.next()
    args = dict(vmin=0,vmax=500)
    ns = state.get_density()
    imcontourf(x[xi[0]:xi[-1]], y, 
               ns.sum(axis=0)[xi[0]:xi[-1], ..., z.shape[0]//2], 
               interpolation='none',
               #diverging=True,
               aspect=1,
               #**args
              )
    ax.set_xlim(-xlim,xlim)
    ax.set_ylabel(r'y_tilt={}'.format(state.experiment.y_tilt), size='xx-large')

    
    ############################    
    ax = ax1 = grid.next()
    state = state1
    x,y,z = state.xyz
    xi = np.where(abs(x)<xlim)[0]
    ns = state.get_density()
    imcontourf(x[xi[0]:xi[-1]], y, 
               ns.sum(axis=0)[xi[0]:xi[-1], ..., z.shape[0]//2], 
               interpolation='none',
               #diverging=True,
               aspect=1,
               #**args
              )
    ax.set_xlim(-xlim,xlim)
    ax.set_ylabel(r'y_tilt={}'.format(state.experiment.y_tilt), size='xx-large')
    
        
    ############################
    ax = ax2 = grid.next()
    state = state2
    x,y,z = state.xyz
    xi = np.where(abs(x)<xlim)[0]
    
    ns = state.get_density()
    imcontourf(x[xi[0]:xi[-1]], y, 
               ns.sum(axis=0)[xi[0]:xi[-1], ..., z.shape[0]//2], 
               interpolation='none',
               #diverging=True,
               aspect=1,
               #**args
              )
    ax.set_xlim(-xlim,xlim)
    ax.set_ylabel(r'y_tilt={}'.format(state.experiment.y_tilt), size='xx-large')

    
    ax.set_xlabel(r'x [micron]', size='xx-large')
    
    plt.tight_layout()    
    return fig

# +
# %pylab inline --no-import-all
from gpe.soc import ExperimentBarrier, u
import SOC.soc_car_fast#;reload(SOC.soc_car_fast)
from SOC.soc_car_fast import ExperimentCARFast

#Sim data
import runs_car_fast_final
#r0 = runs_car_fast_final.Run0()
#r0 = runs_car_fast_final.Run_IPG()
#r0 = runs_car_fast_final.Run_IPG_small()
#r0 = runs_car_fast_final.Run_IPG_small_xTF150_SB()
#r0 = runs_car_fast_final.Run_IPG_xTF150()
#r0 = runs_car_fast_final.Run_IPG_xTF150_SB()
#r0 = runs_car_fast_final.Run_IPG_NoSOC()
#r0 = runs_car_fast_final.Run_IPG_NoSOC_SB()
#r0 = runs_car_fast_final.Run_IPG_NoSOC_SB()
#r0 = runs_car_fast_final.Run_IPG_small_3D
r0 = runs_car_fast_final.Run_IPG_small_3D()



x_TF = 150
barrier_depth_nK = -30
tilt=0

sim = [_sims for _sims in r0.simulations
       if _sims.experiment.barrier_depth_nK == barrier_depth_nK
       and _sims.experiment.x_TF == x_TF
       and _sims.experiment.y_tilt == tilt
      ][0]

params = sim.experiment.items()
print(params)

s = sim.get_state(t_=24, image=True);




# +
_t_ = 5+10
s0 = sim.get_state(t_=_t_, image=False)
#s1 = sim[1].get_state(t_=_t_, image=False)
#s2 = sim[2].get_state(t_=_t_, image=False)
    


plot_func_3(s0, s0, s0, fig=plt.figure(figsize=(35,5)));

# -

# ### 2D plots

# +

from mmfutils.plot import imcontourf
from gpe.plot_utils import MPLGrid
#plt.figure(figsize=(15, 3*5))
#grid = MPLGrid(direction='down', space=0.1)

def plot_func_3(t, n0, n1, n2, n3, fig=None):
    from mmfutils.plot import imcontourf
    if fig is None:
        fig = plt.figure(figsize=(20,6))
    fig.clear()
    
    master_grid = MPLGrid(fig=fig,
                           direction='right',
                           right=False,
                           #scale=True,
                           space=0.05, 
                           share=False,
                           size=0.02
                          )

    grid = master_grid.grid(direction='down',
                            #right=False,
                            #scale=True,
                            space=0.01, 
                            share=True,
                            size=1
                           )

    cb_grid = master_grid.grid(direction='down',
                               #right=True,
                               #scale=True,
                               space=0.1, 
                               #share=True,
                               size=0.01
                              )
    xlim = 50
    #xlim = 12
    x,y,z = s0.xyz
    xi = np.where(abs(x)<xlim)[0]
    args = dict(vmin=0,vmax=500)
    
    
    ############################
    ax = ax0 = grid.next()
    n = n0
    imcontourf(x[xi[0]:xi[-1]], y, 
               n[xi[0]:xi[-1]], 
               interpolation='none',
               #diverging=True,
               aspect=1,
               #**args
              )
    ax.set_xlim(-xlim,xlim)
    ax.set_ylabel(r'y_tilt={}'.format(0))
    title = r'$t={:.1f}$ms, $\qquad $Barrier Depth$={}$nK, $\qquad $xTF$={}$micron, $\qquad $dx$={}$micron'.format(t-10,
                                                                                                                   -30,
                                                                                                                   150,
                                                                                                                   0.2
                                                                                                                   ) 
    ax.set_title(title)
    ax.legend()

    
    ############################
    ax = ax0 = grid.next()
    n = n1
    imcontourf(x[xi[0]:xi[-1]], y, 
               n[xi[0]:xi[-1], ...], 
               interpolation='none',
               #diverging=True,
               aspect=1,
               #**args
              )
    ax.set_xlim(-xlim,xlim)
    ax.set_ylabel(r'y_tilt={}'.format(0.02))
    
    
    ############################
    ax = ax0 = grid.next()
    n = n2
    imcontourf(x[xi[0]:xi[-1]], y, 
               n[xi[0]:xi[-1], ...], 
               interpolation='none',
               #diverging=True,
               aspect=1,
               #**args
              )
    ax.set_xlim(-xlim,xlim)
    ax.set_ylabel(r'y_tilt={}'.format(0.1))

    ############################
    ax = ax0 = grid.next()
    n = n3
    imcontourf(x[xi[0]:xi[-1]], y, 
               n[xi[0]:xi[-1], ...], 
               interpolation='none',
               #diverging=True,
               aspect=1,
               #**args
              )
    ax.set_xlim(-xlim,xlim)
    ax.set_ylabel(r'y_tilt={}'.format(1))

    
    ax.set_xlabel(r'x [micron]', size='xx-large')
    
    plt.tight_layout()    
    return fig

# +
_t_ = -9+10
Nx, Ny, Nz = s0.basis.Nxyz

n0 = sim[0].get_state(t_=_t_, image=False).get_density().sum(axis=(0,3))
n1 = sim[1].get_state(t_=_t_, image=False).get_density().sum(axis=(0,3))
n2 = sim[2].get_state(t_=_t_, image=False).get_density().sum(axis=(0,3))
n3 = sim[3].get_state(t_=_t_, image=False).get_density().sum(axis=(0,3))
"""
n0 = sim[0].get_state(t_=_t_, image=False).get_density().sum(axis=(0))[...,Nz//2]
n1 = sim[1].get_state(t_=_t_, image=False).get_density().sum(axis=(0))[...,Nz//2]
n2 = sim[2].get_state(t_=_t_, image=False).get_density().sum(axis=(0))[...,Nz//2]
n3 = sim[3].get_state(t_=_t_, image=False).get_density().sum(axis=(0))[...,Nz//2]
n0 = sim[0].get_state(t_=_t_, image=False).get_density().sum(axis=(0))[Nx//2-16,...]
n1 = sim[1].get_state(t_=_t_, image=False).get_density().sum(axis=(0))[...,Nz//2]
n2 = sim[2].get_state(t_=_t_, image=False).get_density().sum(axis=(0))[...,Nz//2]
n3 = sim[3].get_state(t_=_t_, image=False).get_density().sum(axis=(0))[...,Nz//2]
"""

plot_func_3(_t_, n0, n1, n2, n3, fig=plt.figure(figsize=(35,5)));
#plt.imshow(n0)
# -





# # Movie Making 

# Preparing data for a movie is different depending on the simulation parameters and whats being plotted. This section shows how to prepare the data in different scenarios.

# ## Chose data 

# +
# %pylab inline --no-import-all
from gpe.soc import ExperimentBarrier, u
import SOC.soc_car_fast#;reload(SOC.soc_car_fast)
from SOC.soc_car_fast import ExperimentCARFast

#Sim data
import runs_car_fast_final
#r0 = runs_car_fast_final.Run0()
#r0 = runs_car_fast_final.Run_IPG()
#r0 = runs_car_fast_final.Run_IPG_small()
#r0 = runs_car_fast_final.Run_IPG_small_xTF150_SB()
r0 = runs_car_fast_final.Run_IPG_xTF150()
#r0 = runs_car_fast_final.Run_IPG_xTF150_SB()
#r0 = runs_car_fast_final.Run_IPG_NoSOC()
#r0 = runs_car_fast_final.Run_IPG_NoSOC_SB()
#r0 = runs_car_fast_final.Run_IPG_NoSOC_SB()
#r0 = runs_car_fast_final.Run_IPG_small_3D



x_TF = 150
barrier_depth_nK = -30

sim = [_sims for _sims in r0.simulations
       if _sims.experiment.barrier_depth_nK == barrier_depth_nK
       and _sims.experiment.x_TF == x_TF
      ][0]

params = sim.experiment.items()
print(params)

s = sim.get_state(t_=0, image=False);

# -

# ## Load data 

history = []
dt = 2
ts = np.arange(10,50+dt,dt)
#ts = np.array([10, 16, 26, 36, 46])
for _t_ in ts:
    t = np.floor(_t_*10)/10
    s = sim.get_state(t_=t, image=True)
    history.append(s.copy())



# +
history = []

t_IMG = 20
ts = np.arange(0,t_IMG+0.5,0.5)

for _t_ in ts:
    t = np.floor(_t_*10)/10
    s = sim.get_state(t_=_t_, image=False)
    history.append(s.copy())

for _t_ in np.arange(0.5,10.1,0.5):
    s = sim.get_state(t_=ts[-1], image=_t_)
    history.append(s.copy())

s = sim.get_state(t_=ts[-1], image=10.1)
history.append(s.copy())
    
# -

# ## Move function 

# +
from gpe import plot_for_khalid

fig = plt.figure(figsize=(20,6))
plot_for_khalid.make_movie('Fast_Barrier{}nK_History_Expanded.mp4'.format(abs(barrier_depth_nK)),
                           history, 
                           lambda state:plot_func(state, fig=fig),fps=12, fig=fig)

# -



# +
from gpe import plot_for_khalid

fig = plt.figure(figsize=(15,6))
plot_for_khalid.make_movie('fast_axial_Barrier{}nK_IPG_SingleBand.mp4'.format(abs(params.barrier_depth_nK)),
                           history, 
                           lambda state:plot_func(state, fig=fig),fps=12, fig=fig)

# -



# ## Load and make for 2 states 

# +
# %pylab inline --no-import-all
from gpe.soc import ExperimentBarrier, u
import SOC.soc_car_fast#;reload(SOC.soc_car_fast)
from SOC.soc_car_fast import ExperimentCARFast


#Sim data
import runs_car_fast_final
#r0 = runs_car_fast_final.Run0()
#r0 = runs_car_fast_final.Run_IPG()
#r0 = runs_car_fast_final.Run_IPG_small()
#r0 = runs_car_fast_final.Run_IPG_small_xTF150_SB()
r0 = runs_car_fast_final.Run_IPG_xTF150()
#r1 = runs_car_fast_final.Run_IPG_xTF150_SB()
#r0 = runs_car_fast_final.Run_IPG_NoSOC()
#r1 = runs_car_fast_final.Run_IPG_NoSOC_SB()
r1 = runs_car_fast_final.Run_IPG_tube()



x_TF = 150
barrier_depth_nK = -30



sim0 = [_sims for _sims in r0.simulations
         if _sims.experiment.x_TF == x_TF
         and _sims.experiment.barrier_depth_nK == barrier_depth_nK][0]

sim1 = [_sims for _sims in r1.simulations
         if _sims.experiment.x_TF > x_TF+1
         and _sims.experiment.barrier_depth_nK == barrier_depth_nK][0]

params0 = sim0.experiment.items()
params1 = sim1.experiment.items()

print(params0)
print(params1)

#s0 = sim0.get_state(t_=0, image=False);
#s0.plot();
#s1 = sim1.get_state(t_=0, image=False);
#s1.plot();


# +
history = []

ts = np.arange(0,50.5,0.5)
#ts = np.arange(0,5,0.5)

#ts = np.array([10, 16, 26, 36, 46])
10, 12, 14, 16, 18,
             20, 22, 24, 26, 28,
             30, 32, 34, 36, 38,
             40, 42, 44, 46, 48,
             50
for _t_ in ts:
    t = np.floor(_t_*10)/10
    s0 = sim0.get_state(t_=_t_, image=False)
    s1 = sim1.get_state(t_=_t_, image=False)
    
    history.append((s0.copy(),s1.copy()))
# -



# +
from gpe import plot_for_khalid

fig = plt.figure(figsize=(30,5))
plot_for_khalid.make_movie('QuickCheck_Barrier{}nK.mp4'.format(abs(params0.barrier_depth_nK)),
                           history, 
                           lambda states:plot_func_2(states[0], states[1], fig=fig), fps=12, fig=fig)

# -



# ## 2 state plot 

# +
# %pylab inline --no-import-all
from gpe.soc import ExperimentBarrier, u
import SOC.soc_car_fast#;reload(SOC.soc_car_fast)
from SOC.soc_car_fast import ExperimentCARFast


#Sim data
import runs_car_fast_final
#r = runs_car_fast_final.Run_IPG_xTF150()
r = runs_car_fast_final.Run_IPG_NoSOC_SB()

sim  = r.simulations[0]
params = sim.experiment


s0 = sim.get_state(t_=10, image=False);
#s0.plot();
s1 = sim.get_state(t_=10, image=True);
#s1.plot();
#print(sim.)


# +
history = []

ts = np.arange(10,28,0.5)
for _t_ in ts:
    t = np.floor(_t_*10)/10
    if _t_%2 == 0:
        s0 = sim.get_state(t_=_t_, image=False)
        s1 = sim.get_state(t_=_t_, image=True)
    else:
        s0 = sim.get_state(t_=_t_, image=False)
    
    history.append((s0.copy(),s1.copy()))

# +
from gpe import plot_for_khalid

fig = plt.figure(figsize=(32,5))
plot_for_khalid.make_movie('Fast_NoIMGvIMG_CHECK_Barrier{}nK.mp4'.format(abs(params.barrier_depth_nK)),
                           history, 
                           lambda states:plot_func_2(states[0], states[1], fig=fig), fps=12, fig=fig)

# -



# ## 3D 

# +
# %pylab inline --no-import-all
from gpe.soc import ExperimentBarrier, u
import SOC.soc_car_fast#;reload(SOC.soc_car_fast)
from SOC.soc_car_fast import ExperimentCARFast


#Sim data
import runs_car_fast_final
r0 = runs_car_fast_final.Run_IPG_small_3D()


barrier_depth_nK = -90     # -30, -60, -90
dx = 0.2*u.micron          # 0.1, 0.5, 1
y_tilt = np.asarray([0, 0.02,0.1, 1])

sim = [_sims for _sims in r0.simulations
       if _sims.experiment.barrier_depth_nK == barrier_depth_nK
       #and _sims.experiment.barrier_depth_nK == barrier_depth_nK
       and _sims.experiment.dx == dx
       #and _sims.experiment.y_tilt == y_tilt
       ]


s0 = sim[0].get_state(t_=20, image=False)

# +
from mmfutils.plot import imcontourf

s0 = sim[0].get_state(t_=18, image=False)
s1 = sim[0].get_state(t_=18, image=True)


fig = plt.figure(figsize=(35,5))
plt.subplot(211)
imcontourf(s0.xyz[0], s0.xyz[1], 
           s0.get_density().sum(axis=(0,3)),
           #radial_ns[xi[0]:xi[-1],...], 
           interpolation='none',
           #diverging=True,
           aspect=1,
           #**args
              )

plt.subplot(212)
imcontourf(s1.xyz[0], s1.xyz[1], 
           s1.get_density().sum(axis=(0,3)),
           #radial_ns[xi[0]:xi[-1],...], 
           interpolation='none',
           #diverging=True,
           aspect=1,
           #**args
              )

# +

ts = np.arange(0,251,1)/10
for _t_ in ts[0:5]:
    #t = np.floor(_t_*10)/10
    s0 = sim[0].get_state(t_=_t_, image=False).copy()

    print(_t_)

# +
history = []

ts = np.arange(0,25,0.01)
for _t_ in ts:
    t = np.floor(_t_*10)/10
    s0 = sim[0].get_state(t_=t, image=False).copy()
    s1 = sim[1].get_state(t_=t, image=False).copy()
    s2 = sim[2].get_state(t_=t, image=False).copy()
    s3 = sim[3].get_state(t_=t, image=False).copy()
    
    history.append((s0.get_density().sum(axis=(0,3)),
                    s1.get_density().sum(axis=(0,3)),
                    s2.get_density().sum(axis=(0,3)),
                    s3.get_density().sum(axis=(0,3))
                   ))
# -

`
