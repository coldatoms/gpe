# -*- coding: utf-8 -*-
# ---
# jupyter:
#   jupytext:
#     formats: ipynb,py
#     text_representation:
#       extension: .py
#       format_name: light
#       format_version: '1.5'
#       jupytext_version: 1.4.0
#   kernelspec:
#     display_name: Python 2 (Ubuntu, plain)
#     language: python
#     name: python2-ubuntu
# ---

# + init_cell=true
import mmf_setup

mmf_setup.nbinit()
from gpe.imports import *
from mmfutils.plot import animation

# -

# Here we explore the physics of the SOC models with some small simulations.

# Time-scales:
#
# * Trap periods:  (326ms, 3.6ms, 3.6ms)
# * Bloch Oscillations:

# # RF Pulses

# Here we consider using an RF pulse to transform atoms from one state to the other, and then watch what happens.  The description is as follows (in dimensionless units $k \equiv k/k_r$ and $E \equiv E/2E_R$):
#
# $$
#   E_-(k) = \frac{k^2+1}{2} - \sqrt{(k-d)^2 + w^2}\\
#   0 = E_{-}'(k_0) = k_0 - \frac{k_0-d}{\sqrt{(k_0-d)^2 + w^2}},\qquad
#   k_0 = \frac{k_0-d}{\sqrt{(k_0-d)^2 + w^2}}, \\
# $$

# Let us discuss this in terms of the lab basis $(N_a,k_a;N_b,k_b)$ and the rotating phase basis $[N_a,k_a-k_r;N_b,k_b+k_r]$.  The ground state has $[N-dN,k_0+k_r;dN,k_0-k_r] = (N-dN, k_0;dN,k_0)$ and must have zero total momentum $(N-dN)(k_0+k_r) = -dN(k_0-k_r)$ from which we get the spin–quasi-momentum mapping once we use the condition above for $k_0$ at the minimum of the dispersion:
#
# $$
#   \frac{N_b - N_a}{N_b + N_a} = \frac{k - d}{\sqrt{(k-d)^2 + w^2}}
#   = \frac{2dN}{N} - 1 = \frac{k_0}{k_r}.
# $$

# +
# %pylab inline
import gpe.soc

reload(gpe.soc)
import soc_adiabaticity

reload(soc_adiabaticity)
from soc_adiabaticity import u

params = dict(
    tube=False, d=0.2, xi=1.0, T__x=10.0, dxyz=(0.1,), cells_x=100, t__final=np.inf
)
p1 = dict(params)
p2 = dict(params, T__x=2.0)
p3 = dict(params, T__x=2.0, d=0.02, cells_x=40)
p4 = dict(params, T__x=2.0, d=0.02, dxyz=(0.05,), cells_x=200)
p5 = dict(params, T__x=2.0, d=0.02, dxyz=(0.02,), cells_x=400)
p6 = dict(params, T__x=2.0, dxyz=(0.05,), cells_x=60)
p = p6a = dict(params, T__x=4.0, dxyz=(0.05,), cells_x=100)
p = p6a = dict(params, T__x=np.inf, dxyz=(0.01,), cells_x=5)


expt = soc_adiabaticity.ExperimentAdiabaticitySmall(**p)
state = expt.get_initial_state(E_tol=1e-6, use_scipy=True)
state.plot(show_mixtures=True, show_momenta=True)
# -

history[0].plot(show_momenta=True)

s = state.copy()
s[0, ...], s[1, ...] = state[1, ...], state[0, ...]
s.cooling_phase = 1.0  # +0.01j
e = EvolverABM(s, dt=0.1 * state.t_scale)
fig = None
skip = 50
history = [s]
with NoInterrupt() as interrupted:
    n = 0
    while not interrupted:
        e.evolve(50)
        history.append(e.get_y())
        if n % skip == 0:
            plt.clf()
            fig = e.y.plot(
                fig=fig,
                show_mixtures=True,
                show_momenta=True,
                k_range_k_r=(-4, 4),
                show_history=history,
            )
            display(fig)
            clear_output(wait=True)
        n += 1

# Here is how you can make a movie.  See the documentation for make_movie
# for options.
state.make_movie("im.mp4", states=history)

plt.figure(figsize=(15, 5))
s.plot_history(states=history, log=True, vmin=-5)

plt.figure(figsize=(15, 5))
s.plot_history(states=history, log=True, vmin=-5)

from soc_adiabaticity import u
from mmfutils.plot import imcontourf

plt.figure(figsize=(15, 10))
# s.plot_history(states=history, log=-5)
# s.plot_history(states=history, log=False)
# plt.colorbar()
ns = np.array([_s.get_density().sum(axis=0) for _s in history])
ts = [_s.t / u.ms for _s in history]
xs = _s.xyz[0]
imcontourf(ts, xs, np.log10(ns), vmin=-5)
plt.colorbar()

# First we defined some notations:
#
# $$
#   \Psi = \begin{pmatrix}
#     n_a e^{\I k_a x}\\
#     n_b e^{\I k_b x}
#   \end{pmatrix} \equiv [(n_a,k_a); (n_b,k_b)], \qquad
#   \tilde{\Psi} = \begin{pmatrix}
#     n_a e^{\I q_a x}\\
#     n_b e^{\I q_b x}
#   \end{pmatrix} \equiv <(n_a,q_a); (n_b,q_b)>, \qquad
#   q_a = k_a - k_r, \qquad
#   q_b = k_b + k_r.
# $$
#
# basis be []
#
# 1. The ground state
# 1. The RF pulse moves the cloud to roughly the other minimum of the dispersion.
# 2. The switch of the states
# 2. On short times, there are some excitations in the upper branch that move away from the cloud.
# 3. There is long range deformation of the cloud, and the CM shifts right in the trap.
# 4. Once the trap has had time to act ($t\approx T_x/2$) the momentum distribution starts changing, and the negative mass region starts to get occupied.  At this point the cloud experiences modulational instabilities.
#
# `p3`, `p3a` display an interesting feature.  One clearly sees two small blobs shooing off to either side of the main blob.  These bounce off the trap and then interfere in the main blob again.

# # Holoviews

# +
import holoviews as hv

hv.extension("matplotlib")
from gpe.plot_utils import MPLFigure
from gpe.utils import Simulation

expt = soc_adiabaticity.ExperimentAdiabaticitySmall(
    # cells_x=100,
    cells_x=300
)
sim = Simulation(dt_=0.1, experiment=expt)

t = hv.Dimension("t", values=sim.saved_ts_)


def plot_t(t):
    print(t)
    s = sim.get_state(t)
    s.plot()
    fig = plt.gcf()
    plt.close(fig)
    return MPLFigure(fig)


plt.close("all")
hv.HoloMap({_t: plot_t(_t) for _t in t.values})  # , kdims=[t])
# -

# # Adiabaticity

# +
# #%pylab inline  ***Do not do this if using holoviews!
from matplotlib import pyplot as plt
import numpy as np
import soc_adiabaticity

reload(soc_adiabaticity)
import holoviews as hv

hv.extension("matplotlib")
from gpe.plot_utils import MPLFigure
from gpe.utils import Simulation

expt = soc_adiabaticity.ExperimentAdiabaticitySmall(
    # cells_x=100,
    cells_x=300
)
sim = Simulation(dt_=0.1, experiment=expt)

t = hv.Dimension("t", values=sim.saved_ts_)


def plot_t(t):
    print(t)
    s = sim.get_state(t)
    s.plot()
    fig = plt.gcf()
    plt.close(fig)
    return MPLFigure(fig)


plt.close("all")
hv.HoloMap({_t: plot_t(_t) for _t in t.values})  # , kdims=[t])
# -

sim.run()


# +
from IPython.display import clear_output, display

# %pylab inline --no-import-all
import soc_experiments

reload(soc_experiments)
from soc_experiments import ExperimentLoading, u
from gpe.utils import Simulation

expt = ExperimentLoading(cells_x=100, trapping_frequencies_Hz=(278.0, 278.0, 278.0))
s = expt.get_initial_state()
clear_output()
s.plot()
# -

expt

# +
# %%file tst_run.py
import soc_experiments

reload(soc_experiments)
from soc_experiments import ExperimentLoading, u
from gpe.utils import Simulation

expt = ExperimentLoading(cells_x=300, trapping_frequencies_Hz=(278.0, 278.0, 278.0))
sim = Simulation(experiment=expt, dt_=0.1)
sim.run()

# +
import soc_experiments

reload(soc_experiments)
from soc_experiments import ExperimentLoading, u
from gpe.utils import Simulation

expt = ExperimentLoading(cells_x=100, trapping_frequencies_Hz=(278.0, 278.0, 278.0))
sim = Simulation(experiment=expt, dt_=0.1)
sim.view()
# -

# %pylab nbagg
import holoviews as hv
from gpe import plot_utils

hv.HoloMap({i: plot_utils.figure_fn(i) for i in range(10)})

import holoviews as hv

hv.extension("matplotlib")
from gpe import plot_utils

reload(plot_utils)
s = sim.get_state(0)
f = plot_utils.MPLFigure(s.plot())
plt.close("all")
f
