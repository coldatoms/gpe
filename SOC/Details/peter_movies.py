"""Movies for Peter."""

import logging
import os.path
import sys

import numpy as np
from matplotlib import pyplot as plt

import mmf_setup.set_path.hgroot
from mmfutils.contexts import NoInterrupt
from pytimeode.evolvers import EvolverABM

import SOC.soc_catch_and_release
from SOC.soc_catch_and_release import u


MOVIE_DIR = os.path.join(
    "/Users/mforbes/Desktop/Shared/Google Drive", "SOC Bucket:Detuning/SimulationMovies"
)

_locals = {}


def run(
    tube=False,
    barrier_width_micron=2.0,
    barrier_depth_nK=-200,
    gaussian=True,
    x_TF=232.5 * u.micron,
    pause=False,
    t__final=np.inf,
    t__max=2,
    T__x=np.inf,
    cells_x=100,
    parity=True,
    steps=10,
    display_interval=2,
    E_tol=1e-10,
    history=None,
    display=True,
    **kw
):
    """Run the simulation.

    Arguments
    ---------
    tube : bool
       If `True`, then run the simulation in the quasi-3D tube geometry simulating
       the radial directions with a dynamic Gaussian profile.  Otherwise, this is
       a 1D simulation with no fluctuations in the transverse direction.  Expansion
       will only be meaningful if `tube=True`.  Note: if `True`, then the integrated
       1D line density is plotted, whereas the central density is plotted if `False`.
    barrier_width_micron, barrier_depth_nK : float
       Barrier potential parameters (same as full experiment)
    gaussian : bool
       If `True`, then the barrier potential is a Guassian, otherwise, it is a
       truncated Harmonic potential with the same depth and curvature.
    x_TF : float
       TF radius in original experiment.  This is used to set the background density.
       Use a small value such as `x_TF=1*u.micron` to set the background
       density to zero.
    pause : bool
       If True, then wait for use input before starting evolution.
    t__final : float
       Time to start imaging in ms.  The evolution from `t__final` to `t__max` will
       invoke the expansion process.
    t__max : float
       Time to stop evolution in ms.
    T__x : float
       Trap period along x axis (not used for initial state.)
    cells_x : int
       Number of k_R cells.  This is the size of our box.
    dx : (float,)
       Size of grid spacing.
    history : None, list
       If not None, then store the states here for plotting and show them.
    steps : int
       Number of steps to evolve each iteration.
    display_interval : float
       Interval in seconds between display updates.
    E_tol : float
       Tolerance for minimizer that finds the ground state.
    """
    import time

    global _locals
    reload(SOC.soc_catch_and_release)
    if t__final < t__max:
        assert tube

    expt = SOC.soc_catch_and_release.ExperimentCatchAndReleaseSmall(
        tube=tube,
        barrier_width_micron=barrier_width_micron,
        barrier_depth_nK=barrier_depth_nK,
        T__x=T__x,
        gaussian=gaussian,
        x_TF=x_TF,
        t__final=t__final,
        cells_x=cells_x,
        **kw
    )

    s0 = expt.get_state()
    fig = s0.plot()
    plt.ylabel("TF Density")

    if display:
        import IPython.display

        IPython.display.display(fig)

    _locals.update(locals())
    s = expt.get_initial_state(cool_steps=200, E_tol=E_tol, use_scipy=True)
    _locals.update(locals())
    fig = s.plot()
    plt.ylabel("Ground State Density")
    if display:
        IPython.display.display(fig)
        IPython.display.clear_output(wait=True)

    if pause:
        raw_input("Ground State: Press enter to evolve")

    s.cooling_phase = 1.0
    e = EvolverABM(s, dt=0.4 * s.t_scale)
    fig = plt.figure(figsize=(15, 10))
    pe = None
    if history is not None:
        history.append(s)
    tic = time.time()
    with NoInterrupt() as interrupted:
        while not interrupted and e.t / expt.t_unit < t__max:
            e.evolve(steps)
            if history is not None:
                history.append(e.get_y())
            if time.time() - tic >= display_interval:
                tic = time.time()
                pe = e.y.plot(
                    plot_elements=pe,
                    fig=fig,
                    show_momenta=True,
                    parity=parity,
                    history=history,
                )
                if display:
                    IPython.display.display(fig)
                    IPython.display.clear_output(wait=True)
                else:
                    logging.info("t_={} < {}".format(e.t / expt.t_unit, t__max))
    pe = e.y.plot(
        plot_elements=pe, fig=fig, show_momenta=True, parity=parity, history=history
    )
    if display:
        IPython.display.display(pe)
    plt.close("all")
    _locals.update(locals())
    return e.get_y()


def make_movie(
    filename,
    history,
    show_n=2,
    show_momenta=1,
    show_history_ab=2,
    show_history_a=1,
    show_history_b=1,
    show_log_history=False,
    pane_height=2,
    dpi=None,
    display=True,
    **kw
):
    movie_file = os.path.join(MOVIE_DIR, filename)
    logging.info("Making movie {}".format(movie_file))
    history[0].make_movie(
        movie_file,
        states=history,
        show_log_history=show_log_history,
        pane_height=pane_height,
        show_n=show_n,
        show_momenta=show_momenta,
        show_history_ab=show_history_ab,
        show_history_a=show_history_a,
        show_history_b=show_history_b,
        dpi=dpi,
        display=display,
        **kw
    )


def make_rotons(display=False):
    logging.info("Making roton movies.  Generating data.")
    with NoInterrupt() as interrupted:
        for barrier_depth_nK in [-0.1, -1, -200, -400]:
            args = dict(
                barrier_depth_nK=barrier_depth_nK,
                barrier_width_micron=4.0,
                barrier_k_k_r=1.3,
                dx=0.05 * u.micron,
                x_TF=232.5 * u.micron,
                E_tol=1e-16,
                display=display,
                t__max=2,
            )
            history = []
            run(history=history, steps=10, **args)
            make_movie(
                "expanding_{}nK_roton.mp4".format(abs(barrier_depth_nK)),
                history,
                display=display,
            )
            if interrupted:
                break


def make_phonons(
    display=False,
    experiment=False,
    rabi_frequency_E_R=1.5,
    t__final=np.inf,
    t__image=10.1,
):
    logging.info("Making phonon movies.  Generating data.")
    args = dict(
        rabi_frequency_E_R=rabi_frequency_E_R,
        steps=20,
        display=display,
        t__final=t__final,
    )

    if experiment:
        barrier_depth_nKs = [-20, -45, -200, -400]
        barrier_depth_nKs = [-400]  # -20, -45, -400]
        if not np.isinf(t__final):
            t__max = t__final + t__image
        else:
            t__max = 40.0

        args.update(
            cells_x=700,
            dx=0.08 * u.micron,
            tube=True,
            T__x=None,
            barrier_width_micron=4.8,
            t__max=t__max,
        )
        filename = "expanding_exp_{barrier_depth_nK}nK{image}.mp4"
    else:
        barrier_depth_nKs = [-0.1, -1, -20, -45, -200, -400]
        barrier_depth_nKs = ([-20],)  # , -45]
        args.update(
            cells_x=100,
            dx=0.1 * u.micron,
            x_TF=232.5 * u.micron,
            barrier_width_micron=4.0,
            E_tol=1e-16,
            t__max=2,
        )
        filename = "expanding_box_{barrier_depth_nK}nK_{rabi_frequency_E_R}ER{image}.mp4"

    movie_args = {}
    if rabi_frequency_E_R == 0:
        # No significant b component.
        movie_args["show_history_b"] = False

    with NoInterrupt() as interrupted:
        for barrier_depth_nK in barrier_depth_nKs:
            args.update(barrier_depth_nK=barrier_depth_nK)
            history = []
            run(history=history, **args)
            make_movie(
                filename.format(
                    barrier_depth_nK=abs(barrier_depth_nK),
                    rabi_frequency_E_R=rabi_frequency_E_R,
                    image="" if np.isinf(t__final) else "_image_{}ms".format(t__final),
                ),
                history,
                display=display,
                **movie_args
            )
            if interrupted:
                break


def make_rf(display=False, experiment=False, T__x=4.0, t__max=10):
    logging.info("Making RF pulse movies.  Generating data.")
    args = dict(steps=10, display=display)

    import SOC.soc_adiabaticity
    from SOC.soc_adiabaticity import u

    params = dict(
        tube=False, d=0.2, xi_micron=1.0, T__x=6.0, dx=0.1, cells_x=100, t__final=np.inf
    )
    p1 = dict(params)
    p2 = dict(params, T__x=2.0)
    p3 = dict(params, T__x=2.0, d=0.02, cells_x=40)
    p4 = dict(params, T__x=2.0, d=0.02, dx=0.05, cells_x=200)
    p5 = dict(params, T__x=2.0, d=0.02, dx=0.02, cells_x=400)
    p6 = dict(params, T__x=2.0, dx=0.05, cells_x=60)
    p = p6a = dict(params, T__x=T__x, dx=0.05, cells_x=100)

    expt = SOC.soc_adiabaticity.ExperimentAdiabaticitySmall(**p)
    return expt
    state = expt.get_initial_state(E_tol=1e-6, use_scipy=True)
    if display:
        import IPython.display

        state.plot(show_mixtures=True, show_momenta=True)

    s = state.copy()
    s[0, ...], s[1, ...] = state[1, ...], state[0, ...]
    s.cooling_phase = 1.0  # +0.01j
    e = EvolverABM(s, dt=0.1 * state.t_scale)
    fig = None
    skip = 50
    history = [s]
    plot_args = dict(
        show_mixtures=False,
        show_momenta=True,
        history=history,
        show_history_ab=1,
        show_history_a=1.5,
        show_history_b=1.5,
    )
    with NoInterrupt() as interrupted:
        n = 0
        while e.t / expt.t_unit < t__max and not interrupted:
            e.evolve(200)
            history.append(e.get_y())
            if display and n % skip == 0:
                plt.clf()
                pe = e.y.plot(pe, **plot_args)

                IPython.display.display(pe.fig)
                IPython.display.clear_output(wait=True)
            elif n % skip == 0:
                logging.info("t_={} < {}".format(e.t / expt.t_unit, t__max))
            n += 1

        filename = "rf_transfer_Tx={}ms_t_max={}ms.mp4".format(p["T__x"], t__max)
        make_movie(filename, history, display=display, **plot_args)


if __name__ == "__main__":
    logging.basicConfig()
    logging.getLogger().setLevel(logging.INFO)
    if len(sys.argv) == 1:
        # make_rotons()
        # make_phonons()
        # make_phonons(rabi_frequency_E_R=0)
        make_phonons(experiment=True)
        # make_phonons(experiment=True, T__final=4.0)
        # make_phonons(experiment=True, T__final=10.0)
        # make_phonons(experiment=True, T__final=20.0)
        # make_rf()
    elif len(sys.argv) >= 2:
        args = eval("dict({})".format(",".join(sys.argv[2:])))
        locals()[sys.argv[1]](**args)
