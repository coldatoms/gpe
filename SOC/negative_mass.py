from soc_experiments import Experiment, u


class ExperimentNM(Experiment):
    """words"""

    Rx_TF = 24.0 * u.micron
    t_unit = u.ms
    detuning_kHz = None
    detuning_E_R = 0.54
    B_gradient_mG_cm = 0
    recoil_frequency_Hz = 1843.0
    rabi_frequency_E_R = 2.5
    trapping_frequencies_Hz = (25, 170, 154)

    initial_imbalance = 0.0  # Nothing RF transfered, all in state[...][0]

    t__final = 20.0  # Time to wait before imaging (up to 500ms)
    t__image = 7  # Time for expansion
