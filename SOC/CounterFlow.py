# ---
# jupyter:
#   jupytext:
#     formats: ipynb,py:light
#     text_representation:
#       extension: .py
#       format_name: light
#       format_version: '1.5'
#       jupytext_version: 1.4.1
#   kernelspec:
#     display_name: Python [conda env:.conda-_gpe]
#     language: python
#     name: conda-env-.conda-_gpe-py
# ---

# + init_cell=true
import mmf_setup

mmf_setup.nbinit()
import sys, os
import SOC

_SOC_PATH = os.path.dirname(SOC.__file__)
if _SOC_PATH not in sys.path:
    sys.path.append(_SOC_PATH)
from gpe.imports import *
from mmfutils.plot import imcontourf

# -

# # Problem

# Here we consider the behavior of systems with a Hamiltonian:
#
# $$
# \op{H}
#   = \begin{pmatrix}
#     \overbrace{K(p+\hbar k_R) - \mu + \frac{\delta}{2} + g_{aa}n_a + g_{ab}n_b}^{K_a(p)} & \frac{\Omega}{2}\\
#     \frac{\Omega}{2}& \underbrace{K(p-\hbar k_R) - \mu - \frac{\delta}{2} + g_{ab}n_a + g_{bb}n_b}_{K_b(p)}
#   \end{pmatrix}, \\
#   K(p) = \frac{p^2}{2m}.
# $$
#
# This can be by appropriate boosting or phase rotations from both counter-flowing superfluids and spin-orbit coupling equal Rashba and Dresselhaus type.

# ## Homogeneous States

# We start with homogeneous states:
#
# $$
#   \Psi(x) = e^{\I k x} \begin{pmatrix}
#     \sqrt{n_a}\\
#     \sqrt{n_b}
#   \end{pmatrix}.
# $$
#
# The states depend on the following dimensionless parameters:
#
# $$
#   d = \frac{\delta}{4E_R}, \qquad
#   w = \frac{\Omega}{4E_R}, \\
#   e_1 = \frac{\bar{n}}{4E_R}\frac{g_{aa} + g_{bb} + 2g_{ab}}{8}, \qquad
#   e_2 = \frac{\bar{n}}{4E_R}\frac{g_{aa} + g_{bb} - 2g_{ab}}{8}, \qquad
#   e_3 = \frac{\bar{n}}{4E_R}\frac{g_{bb} - g_{aa}}{4}.
# $$
#
# These units emerge naturally when we take $4E_R = 2\hbar^2 k_R^2/m = 1$.  I.e. $k_R = \hbar = m/2 = 1$.
#
# For homogeneous states, the effective single-particle dispersion relationship has the form:
#
# $$
#   \frac{E_{\pm}(k) + \mu}{2E_R} = \frac{k^2+1}{2} \pm \sqrt{(k-\tilde{d})^2 + w^2}
#   + \frac{(g_{aa}+g_{ab})n_a - (g_{ab}+g_{bb})n_b}{8E_R}, \\
#   \tilde{d} = d + \frac{(g_{aa}-g_{ab})n_a + (g_{ab}-g_{bb})n_b}{4E_R}.
# $$
#
# If we express the densities in terms of the total density and density difference $n_{\pm} = n_{a} \pm n_{b}$, then we have:
#
# $$
#   n_{a} = \frac{n_+ +n_-}{2}, \qquad n_{b} = \frac{n_+ -n_-}{2},\\
#   \frac{E_{\pm}(k) + \mu}{2E_R} = \frac{k^2+1}{2} \pm \sqrt{(k-\tilde{d})^2 + w^2}
#   + 2e_1 + e_3\frac{n_-}{n_+}, \qquad
#   \tilde{d} = d + 2e_3 + 4e_2\frac{n_{-}}{n_{+}}.
# $$

# ## Supersolidity

# We start with the results of [Supersolids](Supersolid.ipynb).  The supersolid phase has $n_a = n_b$, hence $n_- = 0$.  Supersolidity emerges in a trangular region where:
#
# $$
#   \lim_{w\rightarrow 0}: \quad \abs{d + 2e_3} \leq 4e_2, \qquad
#   \lim_{d \rightarrow 0}: \quad w < \sqrt{\frac{1}{1+\frac{e_1}{e_2}}}.
# $$

# ## Tube

# The idea in [Martone:2015] was to separate physically the two clouds so that the overlap density is small.  This reduces the effective inter-species coupling $\tilde{g}_{ab}$.  For the counterflow experiments, the effect of the trap geometry on the coupling constants follows from the [NPSEQ.ipynb](../Docs/NPSEQ.ipynb):
#
# $$
#   \tilde{g}_{ii} = \frac{g_{ii}}{2\pi\sigma_i^2}, \qquad
#   \tilde{g}_{ab} = \frac{g_{ab}}{\pi(\sigma_a^2+\sigma_b^2)},\\
#   \frac{m\omega_\perp^2}{2}n_{i} = \frac{g_{ii}n_i^2}{4\pi\sigma_i^4} + \frac{\hbar^2n_i}{2m\sigma_i^4} + \frac{g_{ab}n_an_b}{\pi (\sigma_a^2+\sigma_b^2)^2}.
# $$

# In the limit that $n_a = n_b = n_+/2$, we have
#
# $$
#   \tilde{g}_{ii} = \frac{g_{ii}}{2\pi\sigma_i^2}, \qquad
#   \tilde{g}_{ab} = \frac{g_{ab}}{\pi(\sigma_a^2+\sigma_b^2)},\\
#   m\omega_\perp^2 = \frac{g_{ii}n_{+}}{4\pi\sigma_i^4} + \frac{\hbar^2}{m\sigma_i^4} + \frac{g_{ab}n_+}{\pi (\sigma_a^2+\sigma_b^2)^2}.
# $$

# %pylab inline --no-import-all
from gpe.tube2 import Sigma2s, u
from soc_counter_flow import ExperimentCF

e = ExperimentCF(tube=True)
g_a, g_b, g_ab = e.gs
s = e.get_state()
n = s.get_density().sum(0).max()
s.get_sigma2s()

# ## Linear Response

# The linear response of this system can be obtained from the BdG equations:
#
# $$
#   \left[
#   \begin{pmatrix}
#     K_a(p) + g_{aa} n_a & g_{aa} n_a & \frac{\Omega}{2} + g_{ab}\sqrt{n_an_b} & g_{ab}\sqrt{n_an_b}\\
#     g_{aa} n_a & K_a(p) + g_{aa} n_a & g_{ab}\sqrt{n_an_b} & \frac{\Omega}{2} + g_{ab}\sqrt{n_an_b}\\
#     \frac{\Omega}{2} + g_{ab}\sqrt{n_an_b} & g_{ab}\sqrt{n_an_b} & K_b(p) + g_{bb} n_b & g_{bb} n_b\\
#     g_{ab}\sqrt{n_an_b} & \frac{\Omega}{2} + g_{ab}\sqrt{n_an_b} & g_{bb} n_b & K_b(p) + g_{bb} n_b
#   \end{pmatrix}
#   -
#   \omega
#   \begin{pmatrix}
#   1\\
#   & -1\\
#   & & 1\\
#   & & & -1
#   \end{pmatrix}
#   \right]
#   \cdot
#   \begin{pmatrix}
#     a_+\\
#     a_-\\
#     b_+\\
#     b_-
#   \end{pmatrix},\\
#   K_a(q) = \frac{(q + k_R)^2}{2m} - \mu - \frac{\delta}{2} + g_{aa}n_a + g_{ab} n_b,\\
#   K_b(q) = \frac{(q - k_R)^2}{2m} - \mu + \frac{\delta}{2} + g_{ab}n_a + g_{bb} n_b.
# $$


s.get_density().sum(0).max()

from SOC.soc_catch_and_release import u
import soc_counter_flow

reload(soc_counter_flow)
from soc_counter_flow import Homogeneous, ExperimentCF

e = ExperimentCF()
s = e.get_state()
kR = abs(e.B_gradient * np.diff(e.mu_Bs) * 700 * u.ms / u.hbar / 2)[0]
h = Homogeneous(k_R=kR, delta=e.Delta0, Omega=2 * e.Omega0, gs=s.gs)
h.get_homogeneous_state(0, 1.0, band=1)
_4ER = 4 * (u.hbar * kR) ** 2 / 2 / s.ms[0]
d = e.Delta0 / _4ER
w = 2 * e.Omega0 / _4ER


def get_imag(p, n_fact=0.1):
    Eph = h.get_phonon_dispersion(p=p, n=n_fact * s.get_density().sum(0).max())
    q = np.linspace(-kR, kR, 500)
    Es = np.array(list(map(Eph, q)))
    return abs(Es.imag).max()


q = np.linspace(kR / 2.3, kR / 1.8, 20)
plt.plot(q / (kR / 2), list(map(lambda q: get_imag(q, 0.5), q)))
plt.plot(q / (kR / 2), list(map(lambda q: get_imag(q, 0.3), q)))
plt.plot(q / (kR / 2), list(map(lambda q: get_imag(q, 0.1), q)))

Eph = h.get_phonon_dispersion(p=0.5, n=0.1 * s.get_density().sum(0).max())
q = np.linspace(-3, 3, 100)
Es = np.array(list(map(Eph, q)))
# plt.plot(q, Eph(q).T)
plt.plot(q, Es, "+", ms=1)
# plt.ylim(-0.5,0.5)
abs(Es.imag).max()

# # Peter's Experiment

# * https://arxiv.org/abs/1005.2610
#

import runs_counter_flow

rs = [runs_counter_flow.RunMMF(), runs_counter_flow.RunMMFtube()]
sims = [_r.simulations[0] for _r in rs]
ss = [_sim.get_state(t_=0) for _sim in sims]

from gpe.plot_utils import MPLGrid


def plot(ss):
    s, st = ss
    ns = s.get_density()
    nst = st.get_central_density(TF=True)
    nst = st.get_density()
    grid = MPLGrid(direction="down", share=True)
    for s, ns in zip(ss, [ns, nst]):
        grid.next()
        x = s.xyz[0].ravel()
        plt.plot(x, ns.sum(0), "-C0")
        plt.plot(x, ns[0], ":C1")
        plt.plot(x, ns[1], "--C2")

    s2a, s2b = st.get_sigma2s()
    na, nb = st.get_density()
    ga, gb, gab = st.gs
    ga_, gb_ = ga / 2 / np.pi / s2a, gb / 2 / np.pi / s2b
    gab_ = gab / np.pi / (s2a + s2b)
    grid.next()
    plt.plot(x, gab_**2 / ga_ / gb_)


fig = plt.figure(figsize=(20, 5))
NoInterrupt.unregister()
with NoInterrupt() as interrupted:
    for t_ in sims[0].ts_[600::1]:
        if interrupted:
            break
        ss = [_sim.get_state(t_) for _sim in sims]
        plt.clf()
        plot(ss)
        plt.suptitle(f"t_={t_:.4f}")
        display(fig)
        clear_output(wait=True)

# # Ted's Experiments

# +
from SOC.soc_catch_and_release import u
import soc_counter_flow

reload(soc_counter_flow)
from soc_counter_flow import ExperimentCounterFlow


class Experiment_B(ExperimentCounterFlow):
    def B_gradient_t_(self, t_):
        if t_ <= 0:
            return 0
        else:
            return self.B_gradient


# +
args = dict(
    tube=False,
    basis_type="1D",
    species=((1, -1), (2, 0)),
    dx=0.1 * u.micron,
    # Lx=10*u.micron,
    # x_TF=3.*u.micron, recoil_frequency_Hz=1
    Lx=100 * u.micron,
    x_TF=20.0 * u.micron,
    B_gradient_mG_cm=0.017 * 1000 * 10,
    initial_imbalance=0.5,
)

e = Experiment_B(args)
s = e.get_state()
s.t = 0.1
Va, Vb, Vab = s.get_Vext()

plt.plot(s.basis.xyz[0], Va - Va.min())
plt.plot(s.basis.xyz[0], Vb - Vb.min())
plt.plot(s.basis.xyz[0], Vab)
# -

from gpe.minimize import MinimizeState

s = e.get_state()
s.gs = (s.gs[0], s.gs[1], 10 * s.gs[2])
m = MinimizeState(s)
m.check()
s0 = m.minimize()
s0.plot()

s.get_Vint()

s = e.get_initial_state()
s.plot()

ga, gb, gab = s.gs
ga

# +
# s.copy().evolve_to?
# -

s.copy().evolve_to

# +
from pytimeode.evolvers import EvolverSplit

history = []


def callback(state, n=[0]):
    history.append(state.copy())
    if n[0] % 100 == 0:
        pe = state.plot(
            history=history,
            show_log_n=True,
            show_history_ab=True,
            show_history_a=True,
            show_history_b=True,
        )
        display(pe.fig)
        clear_output(wait=True)
        plt.close("all")
    n[0] += 1


t__final = 1000
s.cooling_phase = 1 + 0.1j
s.copy().evolve_to(
    t_end=t__final * s.t_unit,
    dt_t_scale=0.3,
    # Evolver=EvolverSplit,
    evolve_steps=20,
    callback=callback,
)
# -

# # Michael's Code

import soc_counter_flow

reload(soc_counter_flow)
u = soc_counter_flow.u
e = soc_counter_flow.ExperimentCF(tube=True)
s = e.get_state()
s.t = 1.0
Va, Vb, Vab = s.experiment.get_Vext(s)
plt.plot(s.xyz[0], Va)
plt.plot(s.xyz[0], Vb)

e = soc_counter_flow.ExperimentCF(
    trapping_frequencies_Hz=2.0 * np.pi * np.array([1.5, 178, 145]) * 10,
    dx=0.2 * u.micron,
    Lx=100 * u.micron,
    x_TF=20.0 * u.micron,
    tube=True,
    # detuning_kHz=0.03,
    B_gradient_mG_cm=0.017 * 1000 * 100,
    initial_imbalance=0.0,
)
s = e.get_state()
s.t = 1.0
Va, Vb, Vab = s.experiment.get_Vext(s)
plt.plot(s.xyz[0], Va)
plt.plot(s.xyz[0], Vb)

s = e.get_initial_state()

s.plot()

s = e.get_state()
x = s.xyz[0]
s[0, ...] = 100 * np.exp(-((x - 0 * 10) ** 2) / 100)
s[1, ...] = 0 * 100 * np.exp(-((x + 10) ** 2) / 100)
s.plot()

s.constraint = "N"


def callback(state, _N=[0]):
    if _N[0] % 100 == 0:
        plt.clf()
        state.plot()
        plt.twinx()
        Va, Vb, Vab = s.get_V()
        plt.plot(s.xyz[0], Va)
        plt.plot(s.xyz[0], Vb)
        display(plt.gcf())
        clear_output(wait=True)
    _N[0] += 1


m = MinimizeState(s, fix_N=True)
m.check()
s0 = m.minimize(callback=callback)
# s0.plot()

m = MinimizeState(s0, fix_N=True)
m.check()
s1 = m.minimize(callback=callback)

s = e.get_initial_state()  # Should see immiscibility
s.plot()

s = e.get_state()
s[...] = s0[...]

# +
history = []


def callback(state, n=[0]):
    history.append(state.copy())
    if n[0] % 20 == 0:
        # pe = state.plot(history=history,
        #                show_log_n=True,
        #                show_history_ab=True,
        #                show_history_a=True,
        #                show_history_b=True)
        # display(pe.fig)

        state.plot()
        display(plt.gcf())
        clear_output(wait=True)
        plt.close("all")
    n[0] += 1


t__final = 1000
s.cooling_phase = 1
s.copy().evolve_to(
    t_end=t__final * e.t_unit, dt_t_scale=0.2, evolve_steps=20, callback=callback
)

# +


args0 = dict(
    tube=False,
    basis_type="1D",
    species=((1, -1), (1, 0)),
    Lx=1000 * u.micron,
    Nx=2**8,
    x_TF=400.0 * u.micron,
    recoil_frequency_Hz=1,
)

args1 = dict(
    tube=False,
    basis_type="1D",
    dx=0.1 * u.micron,
    species=((1, -1), (2, 0)),
    Lx=8 * u.micron,
    x_TF=2.0 * u.micron,
    trapping_frequencies_Hz=2.0 * np.pi * np.array([10, 178, 145]),
)


e = Experiment_B(
    args1, recoil_frequency_Hz=0.00000000001, B_gradient_mG_cm=0.017 * 1000 * 10 * 10
)
e = Experiment_B(
    dx=0.1 * u.micron,
    Lx=100 * u.micron,
    x_TF=20.0 * u.micron,
    B_gradient_mG_cm=0.017 * 1000 * 10,
)

s = e.get_state()
plt.plot(s.basis.xyz[0], s.get_density().sum(axis=0))
s = e.get_initial_state()
plt.plot(s.basis.xyz[0], s.get_density().sum(axis=0))


# +
s.t = 0.1
Va, Vb, Vab = s.get_Vext()

plt.plot(s.basis.xyz[0], Va - Va.min())
plt.plot(s.basis.xyz[0], Vb - Vb.min())
# plt.plot(s.basis.xyz[0], Vab)
# -

e.B_gradient, e.B_gradient_mG_cm, e.mu_Bs


# +
history = []


def callback(state, n=[0]):
    history.append(state.copy())
    if n[0] % 20 == 0:
        pe = state.plot(
            history=history,
            show_log_n=True,
            show_history_ab=True,
            show_history_a=True,
            show_history_b=True,
        )
        display(pe.fig)
        clear_output(wait=True)
        plt.close("all")
    n[0] += 1


t__final = 1000
s.copy().evolve_to(
    t_end=t__final * s.t_unit, dt_t_scale=0.3, evolve_steps=20, callback=callback
)
# -


# ##

# +
e = Experiment_B()
e = Experiment_B(
    args1, recoil_frequency_Hz=0.00000000001, B_gradient_mG_cm=0.017 * 1000 * 10 * 100
)

k = 2 * np.pi / e.Lx / 2
f = e.B_gradient * e.mu_Bs[0]
dt = k / f
dt, dt / e.t_unit

# +
ddelta = e.B_gradient * e.mu_Bs[0] * e.Lx / 4

w0 = np.sqrt(e.Omega**2 + e.delta**2)
w1 = np.sqrt(e.Omega**2 + (e.delta + ddelta) ** 2)

u.hbar / (w1 - w0) / e.t_unit * 2 * np.pi

# -

w = np.sqrt(e.rabi_frequency_kHz**2 + e.detuning_kHz**2)
1 / w

e.delta, e.Omega

e.B_gradient * e.mu_Bs[0] * e.Lx / 4


ExperimentCounterFlow.rabi_fre

# %pdb

va, vb, vab = s.get_Vext()

plt.plot(s.basis.xyz[0], (va + vb) / 2)


plt.plot(s.basis.xyz[0], s.get_density().sum(axis=0))


# The counter flow magnetic feild shifts the harmonic potenital for the two atoms. We want to find the difference in the new harmonic potentials $V_b-V_a$ and express it as a force. Below the vertical difference between the traps is $\delta_0$ and the horizontal difference between the traps is $\delta_1$
#
#
# $$
# \begin{align}
# V_a = a(x-x_0)^2 + b, \qquad V_b & = V_a + cx + d \\
# & = ax^2 + (c-2ax_0)x  + (b + ax_0^2 + d)
# \end{align}
# $$
#
# expressions for $\delta_0$ and $\delta_1$
#
#
# $$
# \begin{align}
# \delta_1 & = x_{b min} - x_{a min} =  -\frac{c-2ax_0}{2a} - x_0 = -\frac{c}{2a} \\
# \delta_0 & = V_b(x_{b min}) - V_a(x_{a min})  =
# \left( a\left(-\frac{c}{2a}+ x_0 - x_0\right)^2 + b + c\left(-\frac{c}{2a} + x_0\right) + d  \right) - b =
# -\frac{c^2}{4a}  + c x_0  + d
# \end{align}
# $$
#
#
# Solving for $c$ and $d$
#
#

# $$
# c = -2a\delta_1, \qquad
# d = \delta_0 + \frac{c^2}{4a}  - c x_0  = \delta_0 + a\delta_1(\delta_1  + 2 x_0)
# $$
#
# The linear shift is
#
# $$
# V_b - V_a = cx+d = \left(-2a\delta_1\right)x+\left(\delta_0 + a\delta_1(\delta_1  + 2 x_0)\right) =-2a\delta_1 (x - x_0 - \delta_1/2 ) + \delta_0
# $$
#
# and the resulting force and acceleration
#
# $$
# \begin{align}
# F & = -\nabla V = -\nabla (V_b - V_a) = -\nabla (cx+d) = -c = 2a\delta_1 \\
#  & = ma
# \end{align}
# $$
#
# for our parameters
#
# $$
# a  = \frac{2a \delta_1}{m} = \frac{2 \left(\frac{m\omega^2}{2}\right) \delta_1}{m} = \omega^2 \delta_1
# $$
#
#

# In experiment the trapping frequencies are $(0.00030373,  0.02751203,  0.02750807)$

import numpy as np

ws = np.asarray([0.00030373, 0.02751203, 0.02750807])
delta1 = 1.0
a = ws[0] ** 2 * delta1
print(a)
