import numpy as np

import soc_adiabaticity


def test_ClassicalModel_E():
    c = soc_adiabaticity.ClassicalControl(w=0.475, omega2=1.0)
    k = np.linspace(-3, 3, 1000)
    k_ = (k[1:] + k[:-1]) / 2.0
    Ek = c.E(k, d=0.1)
    dEk = c.dE(k, d=0.1)
    assert np.allclose(c.dE(k_, d=0.1), np.diff(Ek) / np.diff(k), rtol=0.003)
    assert np.allclose(c.ddE(k_, d=0.1), np.diff(dEk) / np.diff(k), rtol=0.013)
