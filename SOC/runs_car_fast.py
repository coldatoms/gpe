"""Defines simulations to run with checkpoint/restart capabilities.

To use this, inherit from ExperimentBase, and pass any important variables to
the `local_dict` argument of the ExperimentBase.__init__() method.  These
variables will be used to generate unique filenames for each run.
"""
import itertools

import numpy as np

import mmf_setup.set_path.hgroot

from mmfutils.contexts import NoInterrupt

import mmfutils.performance.threads
import mmfutils.performance.fft

import sys

from gpe import utils

import soc_catch_and_release

u = soc_catch_and_release.u

_DATA_DIR = "_data"


def set_threads(numexpr=1, fft=1, *v):
    if v:
        numexpr = fft = v[0]
    mmfutils.performance.threads.set_num_threads(numexpr)
    mmfutils.performance.fft.set_num_threads(fft)


# Needs testing, but performance on swan did not show an appreciable benefit to
# using multiple cores.
set_threads(1)


class RunBase:
    dt_ = 1.0

    experiment_params = dict(
        cooling_phase=[1.0, 1 + 0.005j, 1 + 0.01j],
    )

    simulation_params = {}

    @property
    def experiments(self):
        params = self.experiment_params
        keys = params.keys()
        iterators = [params[_key] for _key in keys]
        return [
            self.Experiment(**dict(zip(keys, values)))
            for values in itertools.product(*iterators)
        ]

    @property
    def simulations(self):
        params = self.simulation_params
        keys = params.keys()
        iterators = [params[_key] for _key in keys]
        return [
            utils.Simulation(
                experiment=expt,
                dt_=self.dt_,
                data_dir=_DATA_DIR,
                **dict(zip(keys, values))
            )
            for expt in self.experiments
            for values in itertools.product(*iterators)
        ]

    def run(self):
        with NoInterrupt(ignore=True) as interrupted:
            for sim in self.simulations:
                if interrupted:
                    break
                sim.run()
                if interrupted:
                    break
                sim.run_images()


class Run0(RunBase):
    """
    The state is cooled in to the minimum at t=0.
    Then the  barrier is turned on at t=0 and off at t=10.
    """

    experiment_params = dict(
        Lx=[480 * u.micron],
        Nx=[2**13],
        cooling_phase=[1.0, 1 + 0.001j],
        basis_type=["axial"],
        t__wait=[50],
        t__barrier=[10],
        x_TF=[205 * u.micron],
        barrier_depth_nK=[-46],
    )
    simulation_params = dict(
        image_ts_=[
            [
                10,
                11,
                12,
                13,
                14,
                15,
                16,
                17,
                18,
                19,
                20,
                21,
                22,
                23,
                24,
                25,
                30,
                35,
                40,
                45,
                50,
            ]
        ]
    )
    Experiment = soc_catch_and_release.ExperimentCatchAndRelease


class Run_XTF(RunBase):
    """
    We need to match the x_TF of the experimental state.
    Before turning on SOC it is estimated to be around 235 micron,
    however after ramping on SOC its estimated to be around 205.
    By matching cross sections we can estimate the size.
    """

    experiment_params = dict(
        Lx=[480 * u.micron],
        Nx=[2**13],
        cooling_phase=[1.0],
        basis_type=["axial"],
        t__wait=[15],
        t__barrier=[10],
        x_TF=[
            155 * u.micron,
            165 * u.micron,
            175 * u.micron,
            185 * u.micron,
            190 * u.micron,
            195 * u.micron,
            200 * u.micron,
            205 * u.micron,
            210 * u.micron,
            215 * u.micron,
            220 * u.micron,
            225 * u.micron,
        ],
        barrier_depth_nK=[-46],
    )

    simulation_params = dict(image_ts_=[[0, 10, 11, 12, 13, 14, 15]])
    Experiment = soc_catch_and_release.ExperimentCatchAndRelease


class Run_barrier(RunBase):
    """
    This is to test larger and smaller barriers.
    The state is cooled in to the minimum at t=0.
    Then the  barrier is turned on at t=0 and off at t=10.
    """

    experiment_params = dict(
        Lx=[480 * u.micron],
        Nx=[2**13],
        cooling_phase=[1.0],
        basis_type=["axial"],
        t__wait=[50],
        t__barrier=[10],
        x_TF=[205 * u.micron],
        barrier_depth_nK=[-46 * 3, -10, -46 * 2],
    )
    simulation_params = dict(
        image_ts_=[
            [
                0,
                10,
                11,
                12,
                13,
                14,
                15,
                16,
                17,
                18,
                19,
                20,
                21,
                22,
                23,
                24,
                25,
                30,
                35,
                40,
                45,
                50,
            ]
        ]
    )
    Experiment = soc_catch_and_release.ExperimentCatchAndRelease


class Run_IPG(Run0):
    """
    Maren has some alternative data using an IPG laser which causes
    tighter confinment in the transverse direction, increasing the
    trapping potential.
    These runs may have a smaller x_TF, this needs to be checked.
    """

    experiment_params = dict(
        detuning_kHz=[3.49],
        x_TF=[205 * u.micron],
        # Lx=[480*u.micron],
    )
    simulation_params = dict(
        image_ts_=[
            [
                10,
                11,
                12,
                13,
                14,
                15,
                16,
                17,
                18,
                19,
                20,
                21,
                22,
                23,
                24,
                25,
                30,
                35,
                40,
                45,
                50,
            ]
        ]
    )

    Experiment = soc_catch_and_release.ExperimentCatchAndRelease


class Run_IPG_XTF(Run_IPG):
    """
    The IPG too has a small x_TF, these runs are designed to matching
    with experiment.
    """

    experiment_params = dict(
        x_TF=[
            115 * u.micron,
            125 * u.micron,
            135 * u.micron,
            145 * u.micron,
            155 * u.micron,
            165 * u.micron,
            170 * u.micron,
            175 * u.micron,
            180 * u.micron,
            185 * u.micron,
            190 * u.micron,
            195 * u.micron,
        ],
    )
    simulation_params = dict(image_ts_=[[0, 10, 11, 12, 13, 14, 15]])
    Experiment = soc_catch_and_release.ExperimentCatchAndRelease


if __name__ == "__main__":
    cmd = sys.argv[1]
    if cmd in locals():
        locals()[cmd]().run()
    else:
        raise ValueError("Command {} not found.".format(cmd))
