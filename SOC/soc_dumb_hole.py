from __future__ import division, print_function, with_statement, absolute_import

import inspect

import numpy as np
import scipy.interpolate
from scipy.stats.mstats import gmean

sp = scipy

from matplotlib import pyplot as plt

from mmfutils.contexts import coroutine, NoInterrupt

from pytimeode.evolvers import EvolverABM

from gpe import utils, bec2
from gpe.minimize import MinimizeState
from gpe.soc import u, IExperiment, implementer, SOCMixin, Dispersion

__all__ = ["ExperimentCatchAndRelease"]


class State2(SOCMixin, bec2.State):
    single_band = False

    def __init__(self, experiment, **kw):
        self.experiment = experiment
        super().__init__(**kw)

        self._time_independent_Vext = False
        self._Vext = [None, None]  # Cache for performance

    def set_initial_state(self):
        k_r = self.get("k_r")
        E = self.get_dispersion()
        ns = self.experiment.n_0 / 2.0

        if self.single_band:
            ns = ns.sum(axis=0)
            ks = E.get_k0() * k_r
        else:
            k0 = E.get_k0() * k_r

            ka = k0 + k_r
            kb = k0 - k_r
            ks = np.array([ka, kb])[self.bcast]

        x = self.xyz[0]
        self.data[...] = np.exp(1j * ks * x) * np.sqrt(ns)
        self._N = self.get_N()

    def plot(self):
        x = self.xyz[0]
        na, nb = self.get_density_x()
        plt.plot(x, na + nb, "k--")
        plt.plot(x, na, "g-")
        plt.plot(x, nb, "b-")


@implementer(IExperiment)
class ExperimentDumbHole(utils.ExperimentBase):
    """Experiment for controlling a dumb hole simulation.

    Here we start with a homogeneous BEC with SOC, then we spatially
    modulate the detuning as specified by the following functions:

    get_delta_t(t_):
       This is the method used to compute the potential.  Overload
       this method to provide alternative experiments.  We use the
       following parameters in this

    ddelta_k_k_r:
       Maximum amplitude of the detuning shift in units of k_R.


    Other Parameters
    ----------------
    dx : None, (float,)
       If specified (not `None`), then `Nx` will be computed such that his
       minimum lattice spacing is realized, guaranteeing a certain UV cutoff.
    initial_imbalance : float
       Initial state is prepared in (1,-1) then this fraction is transfered to
       the (1, 0) state.
    Omega : float
       This is the strength of the spin-orbit coupling.
    delta : float
       This is the value of detuning in energy units.
    B_gradient : float
       This is the strength of the magnetic field gradient which induces a
       counterflow.
    x0 : float
       Location of the center of the trapping potential.

    The following attributes are guaranteed to exist for use by the State
    class.

    Attributes
    ----------
    k_r : float
       Recoil wave-vector.
    E_R : float
       Recoil energy `E_R = (hbar*k_r)**2/2/m`.
    """

    ######################################################################
    # Attributes required by IExperiment
    State = None

    t_unit = u.ms  # All times specified in this unit
    t_name = "ms"
    t__final = np.inf  # Time when potentials are turned off for expansion.
    t__image = 0  # Expansion time for imaging
    # End of attributes required by IExperiment
    ######################################################################

    ######################################################################
    # Parameters
    #
    # Convenience parameters
    #
    # These parameters can be specified and if specified will cause
    # the parameters below to be recomputed.  The should not be used
    # directly in the code.
    healing_length = None  # Healing length
    c_s = None  # Speed of sound

    # Default values.  These are not expected to change, but could be changed
    # by passing kwargs to the base class __init__ method.
    m = 1.0
    g = 1.0  # Coupling constants: determined from states
    dx = 0.1  # Lattice spacing
    n_0 = 1.0  # Background density
    Lx = 100  # Approximate length of box (will be rounded
    #   to an integer number of cells).
    k_r = 1.0  # Recoil wavelength
    d = 0.2  # Dimensionless detuning
    w = 0.1  # Dimensionless SOC strength

    ddelta_k_r = 1.0  # Maximum shift in delta in units of k_r

    cooling_phase = 1.0
    ws = (0.0,)
    dim = 1

    basis_type = "1D"

    ######################################################################
    # Methods required by IExperiment
    def init(self):
        msgs = []

        # First process the convenience parameters
        mu0 = None
        if self.healing_length is not None:
            if self.c_s is not None:
                self.m = self.hbar / (np.sqrt(2) * self.healing_length * self.c_s)
            mu0 = self.hbar**2 / (2 * self.m * self.healing_length**2)
        elif self.c_s is not None:
            mu0 = self.m * self.c_s**2

        if mu0 is not None:
            self.g = mu0 / self.n_0

        # Now check and compute the convenience parameters which we
        # store with an underscore so we keep track of which
        # parameters the user actually set.
        if self.c_s is not None:
            self._c_s = np.sqrt(self.g * self.n_0 / self.m)
            assert np.allclose(self._c_s, self.c_s)
        if self.healing_length is not None:
            self.healing_length_ = self.hbar**2 / (2 * self.m * self.g * self.n_0)
            assert np.allclose(self.healing_length_, self.healing_length)

        self.ms = (self.m, self.m)
        self.gs = (self.g, self.g, self.g)
        self.E_R = (u.hbar * self.k_r) ** 2 / 2.0 / self.m
        self.Omega = self.w * 4 * self.E_R
        self.delta = self.d * 4 * self.E_R

        Lk = 2 * np.pi / self.k_r  # Unit cell length of k_r lattice.
        cells_x = int(np.round(self.Lx / Lk))
        self.Lx = cells_x * Lk

        self.Nx = utils.get_good_N(self.Lx / self.dx)

        # Collect all time-dependent methods.
        self.time_dependent_parameters = {
            _name[:-3]: _meth
            for (_name, _meth) in inspect.getmembers(self, predicate=callable)
            if _name.endswith("_t_")
        }

        for name in self.time_dependent_parameters:
            # method_name = name + "_t_"
            info_name = name + "_info"
            if not hasattr(self, info_name):
                setattr(self, info_name, (1.0, name))

        self.msgs = msgs

    def get_state(self, expt=False):
        """Quickly return an appropriate initial state."""
        state_args = dict(
            experiment=self,
            x_TF=None,
            cooling_phase=self.cooling_phase,
            t=0.0,
            k_r=self.k_r,
            gs=self.gs,
            ms=self.ms,
            twist=(0, 0),
            constraint="N",
        )

        Lx = self.Lx
        Nx = self.Nx

        state = State2(Nxyz=(self.Nx,), Lxyz=(self.Lx,), **state_args)

        # These should be provided by the Experiment so we remove them from the
        # state to avoid any confusion.
        del state.Omega, state.delta

        return state

    def get_initial_state(
        self,
        perturb=0.0,
        E_tol=1e-12,
        psi_tol=1e-12,
        disp=1,
        tries=20,
        cool_steps=100,
        cool_dt_t_scale=0.1,
        minimize=True,
        **kw
    ):
        """Return an initial state with the specified population fractions."""
        state = self.get_state()
        state.cooling_phase = 1.0

        if state.rotating_phases:
            k_r = self.get("k_r", t_=state.t)
            state[0, ...] *= np.exp(1j * state.xyz[0] * k_r)

        # Cool to minimum with fixed total number
        state.constraint = "N"
        fix_N = True

        state.init()

        if minimize:
            m = MinimizeState(state, fix_N=fix_N)
            self._debug_state = m  # Store in case minimize fails
            if "use_scipy" not in kw:
                kw["tries"] = tries
            state = m.minimize(E_tol=E_tol, psi_tol=psi_tol, disp=disp, **kw)

        self._debug_state = state  # Store in case evolve fails

        if cool_steps > 1:
            # Cool a bit to remove any fluctuations.
            state.cooling_phase = 1j
            dt = cool_dt_t_scale * state.t_scale
            state.t = -dt * cool_steps
            evolver = EvolverABM(state, dt=dt)
            evolver.evolve(cool_steps)
            state = evolver.get_y()

        del self._debug_state

        psi0 = state[...]
        # Rely on get_state for all other parameters like t, cooling_phase etc.
        self._state = state = self.get_state()
        state[...] = psi0
        return state

    def get_Vext(self, state, fiducial=False, expt=False):
        """Return (V_a, V_b, V_ab), the external potentials.

        For `t_ > self.t__final`, all the potentials are set to zero.

        Arguments
        ---------
        state : IState
           Current state.  Use this to get the time `state.t` and
           abscissa `state.basis.xyz`.
        expt : bool
           If `True`, then return the proper experimental potential
           rather than the potential used in the simulation.

        See Also
        --------
        * interface.IExperiment.get_Vext

        """
        # Convert times into experiment units
        t_ = state.t / self.t_unit

        Omega = self.get("Omega", t_=t_)
        delta = self.get("delta", t_=t_)
        k_r = self.get("k_r", t_=t_)

        zero = np.zeros(state.shape)
        x = state.xyz[0]

        if t_ > self.t__final:
            _Vext = (zero,) * 3
            return _Vext

        Va = Vb = zero

        if state.single_band:
            V_ab = zero
            V_Bs = (zero, zero)
        else:
            delta_x = self.get_delta_t_(t_=t_, state=state)
            self._d_x = delta_x / 4 / self.E_R
            if state.rotating_phases:
                V_ab = Omega / 2.0
            else:
                k_r = self.get("k_r", t_=t_)
                V_ab = Omega / 2.0 * np.exp(2j * x * k_r)

        _Vext = (Va - delta_x / 2.0, Vb + delta_x / 2.0, V_ab)

        return _Vext

    # End of methods required by IExperiment
    ######################################################################

    ######################################################################
    # Parameter access
    def get(self, name, t_):
        """Return the value of the parameter `name` at time `t_`.

        If the method `name_t_()` exists, it is called, otherwise the attribute
        `name` is used.
        """
        _meth = self.time_dependent_parameters.get(name)
        if _meth is None:
            return getattr(self, name)
        else:
            return _meth(t_)

    def gs_t_(self, t_):
        gs = np.array(self.gs)
        return gs

    ######################################################################
    # External potentials etc.
    # Here we define the time-dependent parts of the problems
    def get_delta_t_(self, t_, state=None):
        """Return the detuning at time t_.

        For this problem, we spatially module the
        detuning is modulated in space (which we turn on at"""
        delta = self.get("delta", t_=t_)
        ddelta_k_r = self.get("ddelta_k_r", t_=t_)
        k_r = self.get("k_r", t_=t_)

        if state is None:
            state = self.get_state()
        zero = np.zeros(state.shape)
        x = state.xyz[0]

        # Spatially dependent delta.  Turn on over t_scale time-period
        amp = k_r * ddelta_k_r * utils.step(t_, state.t_scale)

        # cos modulation
        delta_step = (1 - np.cos(2 * np.pi * x / self.Lx)) / 2.0

        # Linear slope up then down.
        delta_step = utils.get_smooth_transition(
            [0.0, 1.0, 0.0],
            [self.Lx / 4.0, self.Lx / 4.0],
            [self.Lx / 4.0, self.Lx / 4.0],
        )(x + self.Lx / 2)
        delta_x = delta + amp * delta_step
        self._d_x = delta_x / 4 / self.E_R

        return delta + amp * delta_step

    ######################################################################
    # Here are some methods to help with defining the external potential.
    def get_dispersion(self, t_=0.0):
        """Return Dispersion instance."""
        Omega = self.get("Omega", t_=t_)
        delta = self.get("delta", t_=t_)
        E_R = self.get("E_R", t_=t_)
        disp = Dispersion(w=Omega / 4 / E_R, d=delta / 4 / E_R)

        # If we want, we can center this on zero as follows, but this makes it
        # harder to compare with the two-band model.
        # k0 = disp.get_k0()
        # E0 = disp(k0)[0]
        # disp = Dispersion(k0=k0, E0=E0, w=Omega/4/E_R, d=delta/4/E_R)
        return disp

    @coroutine
    def plotter(self, fig=None, plot=True):
        from matplotlib import pyplot as plt

        if fig is None:
            fig = plt.figure(figsize=(20, 5))
        plt.clf()
        lines = [
            plt.plot([], [], "k-")[0],
            plt.plot([], [], "b:")[0],
            plt.plot([], [], "g:")[0],
        ]
        title = plt.title("")
        x = None
        plt.xlabel("x [micron]")
        plt.ylabel("n")
        plt.xlim(-300, 300, auto=None)
        plt.ylim(0, 300, auto=None)

        while True:
            state = yield lines
            na, nb = state.get_density_x()
            E = state.get_energy()
            N = state.get_N()
            t = state.t
            title_text = "t={:.4f}ms, N={:.4f}, E={:.4f}".format(t / u.ms, N, E)
            if x is None:
                x = state.xyz[0] / u.micron
                # lines.extend(plt.plot(x, na, 'b'))
                # lines.extend(plt.plot(x, nb, 'g'))
            lines[0].set_data(x, na + nb)
            lines[1].set_data(x, na)
            lines[2].set_data(x, nb)
            title.set_text(title_text)

            if plot:
                import IPython.display

                IPython.display.display(fig)
                IPython.display.clear_output(wait=True)
            else:
                print("{} < 1000ms".format(t / u.ms))

    def run(
        self,
        get_data,
        fig=None,
        figsize=(20, 5),
        plot=True,
        factor=8.0,
        filename="generated_plots.mp4",
    ):
        if plot:
            import IPython.display
            from matplotlib import pyplot as plt
            from mmfutils.plot.animation import MyFuncAnimation

            if fig is None:
                fig = plt.figure(figsize=figsize)

            with self.plotter(fig=fig, plot=plot) as plot_data:
                anim = MyFuncAnimation(
                    fig,
                    plot_data,
                    frames=get_data(factor=factor, steps=1000),
                    save_count=None,
                )

                anim.save(
                    filename=filename,
                    fps=20,
                    extra_args=["-vcodec", "libx264", "-pix_fmt", "yuv420p"],
                )
                plt.close("all")
                IPython.display.clear_output()
        self.video = IPython.display.HTML(
            '<video src="{0}" width="100%", type="video/mp4" controls/>'.format(filename)
        )

    def plot_dispersion(self, t_=0.0, klims=(-2, 2), hv=None):
        k = np.linspace(-2.5, 2.5, 100)

        d = self.get("delta", t_=t_) / self.E_R / 4
        w = self.get("Omega", t_=t_) / self.E_R / 4
        D = np.sqrt((k - d) ** 2 + w**2)
        Em = 0.5 * (k**2 + 1) - D
        Ep = 0.5 * (k**2 + 1) + D
        if hv:
            return hv.Curve((k, Em)) + hv.Curve((k, Ep))
        else:
            from matplotlib import pyplot as plt

            plt.plot(k, Em, k, Ep)
