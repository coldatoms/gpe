"""Defines simulations to run with checkpoint/restart capabilities.

To use this, inherit from ExperimentBase, and pass any important variables to
the `local_dict` argument of the ExperimentBase.__init__() method.  These
variables will be used to generate unique filenames for each run.

Run 5: Fix sign of magnetic field gradient using this.  (It looks wrong in the
current code).  Re-run counterflow experiment.

Run 1: 
* Fix cooling
* There might be fluctuations run-to-run at zero detuning 30-50Hz
* Maybe slight counter flow.  See Keynote file.

Run 6: 
* Explore role of gradients on loading.


"""
import itertools

import mmf_setup.set_path.hgroot

from mmfutils.contexts import NoInterrupt

import mmfutils.performance.threads
import mmfutils.performance.fft

import sys

from gpe import utils

import soc_experiments

_DATA_DIR = "_data"


def set_threads(numexpr=1, fft=1, *v):
    if v:
        numexpr = fft = v[0]
    mmfutils.performance.threads.set_num_threads(numexpr)
    mmfutils.performance.fft.set_num_threads(fft)


# Needs testing, but performance on swan did not show an appreciable benefit to
# using multiple cores.
set_threads(1)


class RunBase:
    dt_ = 1.0

    experiment_params = dict(
        cooling_phase=[1.0, 1 + 0.005j, 1 + 0.01j, 1 + 0.015j]  # , 0.02]
    )

    @property
    def experiments(self):
        params = self.experiment_params
        keys = params.keys()
        iterators = [params[_key] for _key in keys]
        return [
            self.Experiment(**dict(zip(keys, values)))
            for values in itertools.product(*iterators)
        ]

    @property
    def simulations(self):
        return [
            utils.Simulation(experiment=expt, dt_=self.dt_, data_dir=_DATA_DIR)
            for expt in self.experiments
        ]

    def run(self):
        with NoInterrupt(ignore=True) as interrupted:
            for sim in self.simulations:
                if interrupted:
                    break
                sim.run()


class Run1(RunBase):
    experiment_params = dict(
        cooling_phase=[1.0, 1 + 0.005j, 1 + 0.01j, 1 + 0.015j],  # , 0.02]
        t__wait=[0, 50, 100],
        detuning_kHz=[0.050, 0.200],
    )
    Experiment = soc_experiments.Experiment1


class Run1a(Run1):
    Experiment = soc_experiments.Experiment1a


class Run2(RunBase):
    Experiment = soc_experiments.Experiment2


class Run3(RunBase):
    Experiment = soc_experiments.Experiment3


class Run4(RunBase):
    Experiment = soc_experiments.Experiment4


class Run5(RunBase):
    # Negative gradient (no transfer)
    # Positive gradient has transfer
    # Use to check signs of gradient.
    Experiment = soc_experiments.Experiment5

    experiment_params = dict(
        RunBase.experiment_params,
        B_gradient_mG_cm=[-6, 0, 7, 10, 12, 17],
        t__counterflow=[10.0],
    )


class Run1cf(RunBase):
    Experiment = soc_experiments.Experiment1cf


# class Run2cf(RunBase):
#     Experiment = soc_experiments.Experiment2cf


class Run3cf(RunBase):
    Experiment = soc_experiments.Experiment3cf


if __name__ == "__main__":
    cmd = sys.argv[1]
    if cmd in locals():
        locals()[cmd]().run()
    else:
        raise ValueError("Command {} not found.".format(cmd))
