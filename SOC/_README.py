# ---
# jupyter:
#   jupytext:
#     formats: ipynb,py
#     text_representation:
#       extension: .py
#       format_name: light
#       format_version: '1.5'
#       jupytext_version: 1.4.0
#   kernelspec:
#     display_name: Python 2 (Ubuntu, plain)
#     language: python
#     name: python2-ubuntu
# ---

# # Spin-Orbit Coupled BECs

# This directory contains code and notebooks exploring spin-orbit coupled (SOC) BECs as realized in Peter Engel's lab at WSU.  Here is an overview:
#
# * [Adiabaticity.ipynb](Adiabaticity.ipynb): Exploration of how quickly one can manipulate SOC states.  The initial exploration prepares a state with large detuning in the ground state, then looks at how quickly one can change the detuning.
# * [Homogeneous.ipynb](Homogeneous.ipynb): Discussion of the properties of homogeneous phases, including the idea of rotating through the Bloch sphere as well as a discussion of the single-band model and how to convert from the two-component model to the single band model.
#
# * [Soliton Gas.ipynb](Soliton Gas.ipynb): Exploration of a gas of solitons.
#
# * [SOC Solitons.ipynb](SOC Solitons.ipynb): Large collection of various experiments.
