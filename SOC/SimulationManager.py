# ---
# jupyter:
#   jupytext:
#     formats: ipynb,py
#     text_representation:
#       extension: .py
#       format_name: light
#       format_version: '1.5'
#       jupytext_version: 1.4.0
#   kernelspec:
#     display_name: Python 2 (Ubuntu, plain)
#     language: python
#     name: python2-ubuntu
# ---

# + init_cell=true
import mmf_setup

mmf_setup.nbinit()
from gpe.imports import *

# + init_cell=true
import gpe.utils

reload(gpe.utils)
import soc_catch_and_release

reload(soc_catch_and_release)
from gpe.utils import SimulationManager

sm = SimulationManager(data_dir="_data")
print sm.find_simulations()
sm.load_simulations()
# -

ss = sm._simulations
ss.__dir__()

ss.barrier_depth_nK


class A:
    def __dir__(self):
        return ["x=3"]


a = A()

ss._keys

filename = "_data/ExperimentCatchAndRelease/cooling_phase=(1+0.001j)/t__barrier=0/barrier_depth_nK=-200.0/experiment.py"
d = {}
exec (open(filename).read(), d)
