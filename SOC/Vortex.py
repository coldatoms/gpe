# ---
# jupyter:
#   jupytext:
#     formats: ipynb,py
#     text_representation:
#       extension: .py
#       format_name: light
#       format_version: '1.5'
#       jupytext_version: 1.4.0
#   kernelspec:
#     display_name: Python [conda env:_gpe3]
#     language: python
#     name: conda-env-_gpe3-py
# ---


import mmf_setup

mmf_setup.nbinit()
from gpe.imports import *

# # Single Vortex, cylindrical trap

import runs_vortex

r = runs_vortex.RunVortex1()
r.simulations[0]


import gpe.soc

reload(gpe.soc)
from gpe.utils import evolve
import soc_vortex

reload(soc_vortex)
e = soc_vortex.ExperimentVortex()
# %time s0 = e.get_state()
# %time s = e.get_initial_state(use_scipy=True, cool_steps=10, E_tol=1e-8)
e.plot(s)

import soc_vortex

reload(soc_vortex)
e = soc_vortex.ExperimentVortex()
e.plot(s)


# +
# plt.colorbar?
# -

s.cooling_phase = 1.0 + 0.1j
for y in evolve(state=s, dt_t_scale=0.2, steps=100, display=True):
    e.plot(y)

s.cooling_phase = 1j
for y in evolve(state=s, dt_t_scale=0.1, steps=10, display=True):
    e.plot(y)

# # VortexPairs

import gpe.soc

reload(gpe.soc)
import soc_vortex

reload(soc_vortex)
e = soc_vortex.ExperimentVortexPair()
s = e.get_initial_state()
x, y = s.xyz
imcontourf(x, y, s.get_density(), aspect=1)


def find_vortex(state, state0=s, d=3):
    """Return the approximate location of the vortex in the upper plane."""
    psi = state.get_psi()[0]
    n = abs(psi) / abs(state0.get_psi()[0])
    Ny = psi.shape[1]
    x, y = state.xyz
    y = y[:, Ny // 2 : Ny]
    psi = psi[..., Ny // 2 : Ny]
    n = n[..., Ny // 2 : Ny]
    ix, iy = np.unravel_index(np.argmin(n), n.shape)
    z = (x + 1j * y)[ix - d : ix + d, iy - d : iy + d]
    f = psi[ix - d : ix + d, iy - d : iy + d]
    return np.roots(np.polyfit(z.ravel(), f.ravel(), 1)).ravel()[0]


from gpe.plot_utils import MPLGrid

s.cooling_phase = 1j
ev = EvolverABM(s, dt=0.2 * s.t_scale)
fig = plt.figure(figsize=(10, 5))
Es = []
rs = []
with NoInterrupt() as interrupted:
    while not interrupted:
        ev.evolve(100)
        x, y = s.xyz
        plt.clf()
        z = find_vortex(ev.y, s)
        r = z.imag
        E = ev.y.get_energy()
        Es.append(E)
        rs.append(r)
        na, nb = ev.y.get_density()
        na /= na.max()
        nb /= nb.max()
        grid = MPLGrid(direction="right")
        grid.next()
        imcontourf(x, y, na, aspect=1)
        plt.plot([z.real], [z.imag], "x")

        grid.next()
        imcontourf(x, y, nb, aspect=1)
        plt.plot([z.real], [z.imag], "x")

        grid.next()
        imcontourf(x, y, np.transpose((na.T, nb.T, 0 * na.T)), aspect=1)
        plt.plot([z.real], [z.imag], "x")

        plt.suptitle(f"t={ev.t/s.t_unit:0.2f}, E={E:0.2f}, r={r:0.2f}")
        display(fig)
        clear_output(wait=True)

plt.plot(rs, Es)

np.roots(np.polyfit([0, 1], [1, 2], 1))

psi[ix - 5 : ix + 5, iy - 5 : iy + 5]

import gpe.soc

reload(gpe.soc)
import soc_vortex

reload(soc_vortex)
expt = soc_vortex.ExperimentVortex(dx=0.3)
s = expt.get_state()
s.plot()

s.cooling_phase = 1j
e = EvolverABM(s, dt=0.01 * s.t_scale)
fig = None
with NoInterrupt() as interrupted:
    while not interrupted:
        e.evolve(100)
        plt.clf()
        fig = e.y.plot(fig)
        display(fig)
        clear_output(wait=True)


# +
from mmfutils import plot as mmfplt

# mmfplt.colors.color_complex?
# -

# # Double Pairs
#

import gpe.soc

reload(gpe.soc)
import soc_vortex

reload(soc_vortex)
e = soc_vortex.ExperimentVortexPair()
s = e.get_initial_state()
x, y = s.xyz
imcontourf(x, y, s.get_density(), aspect=1)

# +
# %pylab inline --no-import-all
from gpe.soc import ExperimentBarrier, u
import SOC.soc_vortex
from SOC.soc_vortex import ExperimentVortexPair3

# Sim data
import runs_vortex

r0 = runs_vortex.RunVortex3()


barrier_depth_nK = -150  # -60, -150
cooling_phase = 1 + 0.01j  # 1.0, 1+0.01j
single_band = False

# vortex_y=np.linspace(-3.5, 3.5, 15),


sims = [
    _sims
    for _sims in r0.simulations
    if _sims.experiment.barrier_depth_nK == barrier_depth_nK
    and _sims.experiment.cooling_phase == cooling_phase
    and _sims.experiment.single_band == single_band
]

params = [_sim.experiment.items() for _sim in sims]
print(params)

# -


# +
N0 = []
for _sim in sims:
    s = _sim.get_state(t_=0, image=False)
    N0.append(s.get_N())

N0 = np.asarray(N0)

# +
ts = np.linspace(0, 8, 81)

Nmax = []
for _sim in sims:
    for _t in ts:
        time = np.floor(_t * 100) / 100
        s = _sim.get_state(t_=time, image=False)
        Nmax.append(s.get_density().sum(axis=0).max())

Nmax = np.asarray(Nmax)
N_max = Nmax.max()
# -


# +
from mmfutils.plot import imcontourf
from gpe.plot_utils import MPLGrid

# plt.figure(figsize=(15, 3*5))
# grid = MPLGrid(direction='down', space=0.1)


def plot_func(t=0, fig=None):
    from mmfutils.plot import imcontourf

    if fig is None:
        xy_ratio = 8.85878962345765
        fig = plt.figure(figsize=(4 * xy_ratio, 15 + 1))
    fig.clear()

    grid = MPLGrid(fig=fig, direction="down", space=0.0, share=True)

    # plt.suptitle(f"t={t:.2g}ms")
    args = dict(vmin=0, vmax=N_max / N0.max())

    for _sim, _N0 in zip(sims[:-1], N0[:-1]):
        sub_grid = grid.grid(direction="right", space=0.0, share=True)

        s = _sim.get_state(t_=t, image=False)
        x, y = [_x.ravel() for _x in s.xyz]
        n = na, nb = s.get_density() / _N0
        sigma_z = (na - nb) / (na + nb)

        ax = sub_grid.next()
        imcontourf(x, y, n.sum(axis=0), aspect=1, **args)
        ax = sub_grid.next()
        imcontourf(x, y, na, aspect=1, **args)
        ax = sub_grid.next()
        imcontourf(x, y, nb, aspect=1, vmin=0, vmax=N_max / N0.max() / 10)
        ax = sub_grid.next()
        imcontourf(x, y, np.sign(sigma_z) * sigma_z**4, aspect=1, vmin=-1, vmax=1)
        # imcontourf(x, y, sigma_z, aspect=1, vmin=-1, vmax=1)

    sub_grid = grid.grid(direction="right", space=0.0, share=True)

    s = sims[-1].get_state(t_=t, image=False)
    x, y = [_x.ravel() for _x in s.xyz]
    n = na, nb = s.get_density() / N0[-1]
    sigma_z = (na - nb) / (na + nb)

    ax = sub_grid.next()
    imcontourf(x, y, n.sum(axis=0), aspect=1, **args)
    ax.set_xlabel(r"n", size="xx-large")
    ax = sub_grid.next()
    imcontourf(x, y, na, aspect=1, **args)
    ax.set_xlabel(r"na", size="xx-large")
    ax = sub_grid.next()
    imcontourf(x, y, nb, aspect=1, vmin=0, vmax=N_max / N0.max() / 10)
    ax.set_xlabel(r"nb", size="xx-large")
    ax = sub_grid.next()
    imcontourf(x, y, np.sign(sigma_z) * sigma_z**2, aspect=1, vmin=-1, vmax=1)
    # imcontourf(x, y, sigma_z, aspect=1, vmin=-1, vmax=1)

    ax.set_xlabel(r"$\sigma_z$", size="xx-large")

    plt.tight_layout()


plot_func(t=2, fig=None)
# -


# +
from gpe import plot_for_khalid

history = np.floor(ts * 100) / 100
history = history.tolist()
xy_ratio = 8.85878962345765
fig = plt.figure(figsize=(4 * xy_ratio, 15 + 1))

plot_for_khalid.make_movie(
    "VortexLines_Pairs_Barrier{}nK_cooled.mp4".format(barrier_depth_nK),
    history,
    lambda t: plot_func(t=t, fig=fig),
    fps=12,
    fig=fig,
)


# -


# # Runs

import runs_vortex

r = runs_vortex.RunVortex1()
sim = r.simulations[4]

s0 = sim.get_state(0)
ns = [sim.get_state(t_).get_density().sum(axis=0) for t_ in sim.ts_]
x, y = s0.xyz
plt.figure(figsize=(10, 10))
with NoInterrupt() as interrupted:
    for n in ns:
        if interrupted:
            break
        plt.clf()
        imcontourf(x, y, n, aspect=1)
        display(plt.gcf())
        clear_output(wait=True)
        # plt.close('all')
