# ---
# jupyter:
#   jupytext:
#     formats: ipynb,py
#     text_representation:
#       extension: .py
#       format_name: light
#       format_version: '1.5'
#       jupytext_version: 1.4.0
#   kernelspec:
#     display_name: Python 2 (Ubuntu, plain)
#     language: python
#     name: python2-ubuntu
# ---

# + init_cell=true
import mmf_setup

mmf_setup.nbinit(theme="mmf")

# + [markdown] toc=true
# <h1>Table of Contents<span class="tocSkip"></span></h1>
# <div class="toc" style="margin-top: 1em;"><ul class="toc-item"><li><span><a href="#Introduction" data-toc-modified-id="Introduction-1"><span class="toc-item-num">1&nbsp;&nbsp;</span>Introduction</a></span><ul class="toc-item"><li><span><a href="#Bloch-Rotations" data-toc-modified-id="Bloch-Rotations-1.1"><span class="toc-item-num">1.1&nbsp;&nbsp;</span>Bloch Rotations</a></span></li></ul></li><li><span><a href="#Classical-Model" data-toc-modified-id="Classical-Model-2"><span class="toc-item-num">2&nbsp;&nbsp;</span>Classical Model</a></span><ul class="toc-item"><li><span><a href="#Detuning-Ramp" data-toc-modified-id="Detuning-Ramp-2.1"><span class="toc-item-num">2.1&nbsp;&nbsp;</span>Detuning Ramp</a></span></li><li><span><a href="#High-Level-Description" data-toc-modified-id="High-Level-Description-2.2"><span class="toc-item-num">2.2&nbsp;&nbsp;</span>High-Level Description</a></span><ul class="toc-item"><li><span><a href="#Scales" data-toc-modified-id="Scales-2.2.1"><span class="toc-item-num">2.2.1&nbsp;&nbsp;</span>Scales</a></span></li></ul></li></ul></li><li><span><a href="#Homogeneous-States" data-toc-modified-id="Homogeneous-States-3"><span class="toc-item-num">3&nbsp;&nbsp;</span>Homogeneous States</a></span></li><li><span><a href="#Trapped-States" data-toc-modified-id="Trapped-States-4"><span class="toc-item-num">4&nbsp;&nbsp;</span>Trapped States</a></span></li></ul></div>
# -

# # Introduction

# Here we explore adiabatic and quasi-adiabatic manipulation of SOC BECs.  There are two key ideas:
#
# 1. For homogeneous states, the Hamiltonian acts as a rotation of the state through the Bloch sphere,
# 2. The detuning is equivalent to a doppler shift.  Thus, detuning ramps $\delta(t)$ simulate a moving external potential.  (This equivalence can be made exact for harmonic traps).
#
# The goal here is to manipulate trapped gases by using the appropriate classical analogues.  We start by making these two key points precise.  *(Details may be found in the [`Homogeneous.ipynb`](Homogeneous.ipynb) notebook.)*

# ## Bloch Rotations

# If everything is adiabatic, we expect the system to track the ground state of the single-band model.  Here we work in the rotating phase picture where the original two-component wave-function is:
#
# $$
#   \Psi_k = e^{\I k_R x \mat{\sigma}_z}\tilde{\Psi}_k = e^{\I k x}\begin{pmatrix}
#     e^{\I k_R x} \sqrt{n_a}\\
#     e^{-\I k_R x} \sqrt{n_b}
#   \end{pmatrix}.
# $$
#
# The dispersion relationship for homogeneous states is:
#
# $$
#   \tilde{E}_{\pm}(\tilde{k}) = \frac{E_{\pm}(k)}{2E_{R}} = \frac{\tilde{k}^2 + 1}{2} \pm \sqrt{(\tilde{k}-d)^2 + w^2}, \qquad
#   \tilde{k} = \frac{k}{k_R}, \qquad
#   d = \frac{\delta}{4E_R}, \qquad
#   w = \frac{\Omega}{4E_R}, \qquad
#   E_R = \frac{\hbar^2k_R^2}{2m}.
# $$
#
# Neglecting the evolution of this overall phase by subtracting an appropriate constant, we obtain the following rotation on the Bloch sphere:
#
# $$
#   \tilde{\Psi} = \exp\left(-\I t \vec{\omega}\cdot \frac{\vec{\mat{\sigma}}}{2}\right)\tilde{\Psi}, \qquad
#   \hbar\vec{\omega} = \begin{pmatrix}
#     \Omega\\
#     0\\
#     \frac{2\hbar^2 k k_R}{m} - \delta
#   \end{pmatrix}
#   =
#   4E_R
#   \begin{pmatrix}
#     w\\
#     0\\
#     \tilde{k} - d
#   \end{pmatrix}.
# $$
#
# The timescale (period) for these rotations is set by the recoil timescale $T_R = 2\pi \hbar/4E_R$:
#
# $$
#   T_{\omega} = \frac{2\pi}{\omega}
#   = \frac{2\pi \hbar}
#          {\sqrt{\left(\frac{2\hbar^2 k k_R}{m} - \delta\right)^2 + \Omega^2}}
#   = \frac{T_R}
#          {\sqrt{(\tilde{k} - d)^2 + w^2}}, \qquad
#   T_R = \frac{\pi\hbar}{2E_R}.
# $$

# The minimum of the dispersion is at:
#
# $$
#   \tilde{E}'(\tilde{k}) = \frac{k_R E'(k)}{2E_R} = \tilde{k}- \frac{\tilde{k} - d}{\sqrt{(\tilde{k}-d)^2+w^2}} = 0\\
# d = \tilde{k}\left(1 - \frac{w}{\sqrt{1-\tilde{k}^2}}\right)
# $$

# # Classical Model

# ## Detuning Ramp

# As discussed in [`Homogeneous.ipynb`](Homogeneous.ipynb), ramping the detuning in a harmonic potential is equivalent to moving the potential.  Let us consider two systems: one with a fixed harmonic potential and a variable detuning $\delta(t)$:
#
# $$
#   \delta(t), \qquad V(x) = \frac{m\omega^2x^2}{2}.
# $$
#
# The other in a frame moving with position $x_0(t)$ where the potential moves with position $y_0(t)$:
#
# $$
#   \delta = 0, \qquad V(x) = \frac{m\omega^2}{2}[x - y_0(t)]^2, \qquad
#   y_0(t) = x_0(t) + \frac{\ddot{x}_0(t)}{\omega^2}.
# $$
#
# The density profiles should be the same if measured from a frame moving with $x_0(t)$, i.e. as a function of $x_v = x - x_0(t)$ if $\delta(t)$ and $y_0(t)$ are related by:
#
# $$
#   \delta(t) = \delta(0) - 2\hbar k_R \dot{x}_0(t).
# $$

# Let's consider the classical dynamics of the second system, describing the cloud as a classical particle with position $x(t)$ and quasi-momentum $k(t)$.  The Hamiltonian equations of motion are:
#
# $$
#   \dot{x}(t) = E'_{d=0}(k), \qquad
#   \dot{k}(t) = -\omega^2[x - y_0(t)].
# $$
#
# Expressing everything in dimensionless units where $k_R = 2E_R = 1$, we have
#
# $$
#   d = -\dot{f}, \qquad
#   \tilde{y}_0 = f + \frac{\ddot{f}}{\tilde{\omega}^2}, \qquad
#   \dot{\tilde{x}} = \tilde{E}'_{d=0}(\tilde{k}), \qquad
#   \dot{\tilde{k}} = -\tilde{\omega}^2(\tilde{x} - \tilde{y}_0),
# $$
#
# The external control of this system can be specified by the function $f(\tilde{t})$.

# As a simple example, consider a stationary gas in the minimum with $d(\tilde{t}) = d_0 > 0$ constant.  In this case $f(\tilde{t}) = -d_0 \tilde{t}$, which corresponds to a trap moving left $\tilde{y}_0 = -d_0\tilde{t}$.  The cloud should be sitting at $k_0$ in this minimum of the dispersion so that $d_0 =$ in the minimum

# For example, we start with a symmetric example, where parity will ensure that we start and end in the ground state.  We start from a cloud in the ground state $\tilde{E}'_{-d_0}(-\tilde{k}_0) = 0$, $q_0 = 0$, and manipulate the cloud into the symmetric ground state $\tilde{E}'_{d_0}(\tilde{k}_0) = 0$, $q_1 = 0$ using a function:
#
# $$
#   f(\tilde{t}) = d_0 (\tilde{t} - \tfrac{T}{2})\tanh\left(\alpha\tan\frac{\pi}{2T}\bigl(\tilde{t}-\tfrac{T}{2}\bigr)\right)
# $$
#
#
# $$
#   \dot{f}_0 = -d_0, \qquad \dot{f}_1 = d_1, \qquad
# $$

c.dE(c.k0, d=0)

# %pylab inline --no-import-all
t = np.linspace(0, 10, 100)
T = 10.0
alpha = 1.0
plt.plot(t, (t - T / 2.0) * np.tanh(alpha * np.tan(np.pi / 2 * (t - T / 2.0) / T)))
plt.gca().set_aspect(1)

# We start with a symmetric As a simple example, we ramp from $\tilde{k}_0 = -0.9$ to $\tilde{k}_1 = 0.9$

c.d0

expt._time_dependent_parameters["B_gradient"]

# %pylab inline --no-import-all
# %load_ext autoreload
# %autoreload 2
import numpy as np
import soc_adiabaticity
from soc_adiabaticity import ClassicalControlSymmetric

c = ClassicalControlSymmetric(
    k0=-0.95, w=0.475, T=36.90036900369003, alpha=3.0, omega2=0.073441
)
ts = np.linspace(0, c.T, 100)
xs, ks = c.solveA(c.y0(0), c.k0, ts)
ys = c.y0(ts)
ds = c.d(ts)
# plt.plot(ts, ys/expt.k_r, label='Trap center')
# plt.plot(ts, xs/expt.k_r, label='Cloud center')
# plt.legend()
plt.plot(ts, (xs - ys) / expt.k_r)
plt.twinx()
plt.plot(ts, ds, ":")
ks[-1]

# %pylab inline --no-import-all
# %load_ext autoreload
# %autoreload 2
from gpe import soc
import soc_adiabaticity

reload(soc_adiabaticity)
expt = soc_adiabaticity.ExperimentAdiabaticityClassicalSymmetric(
    tube=False, T__R=0.2, cells_x=20
)
s0 = expt.get_state()
s1 = expt.get_initial_state()
s1.plot(show_mixtures=True, show_momenta=True)

# +
from IPython.display import display, clear_output
from pytimeode.evolvers import EvolverABM
from mmfutils.contexts import NoInterrupt

e = EvolverABM(s1, dt=0.2 * s1.t_scale)
fig = None
txs = []
skip = 10
x = e.y.xyz[0]
with NoInterrupt() as i:
    n = 0
    while not i and e.t < s1.t_expansion:
        e.evolve(100)
        na, nb = e.y.get_density()
        txs.append((e.t, (x * (na + nb)).sum() / (na + nb).sum()))
        if n % skip == 0:
            fig = e.y.plot(fig, show_mixtures=True, show_momenta=True)
            display(plt.gcf())
            clear_output(wait=True)
        n += 1
# -

# Here is the motion of the center of mass:

ts, xs = np.array(txs[:-1]).T
plt.plot(ts / expt.t_unit / expt.T__R, xs)

# Our classical code does not give the same result!

# +
c = expt._classical_control
# c.omega2 = 1.4   # This almost works!

ts = np.linspace(0, c.T, 100)
xs, ks = c.solveL(0, c.k0, ts)
# ys = c.y0(ts)
# ds = c.d(ts)
# plt.plot(ts, ys/expt.k_r, label='Trap center')
plt.plot(ts, xs / expt.k_r, label="Cloud center")
# plt.plot(ts, (xs[0]+ts*c.dE(c.k0, c.d(0)))/expt.k_r)
plt.legend()
# plt.plot(ts, (xs-ys)/expt.k_r)
# plt.twinx()
# plt.plot(ts, ds, ':')
ks[-1]

ts, xs = np.array(txs[:-1]).T
plt.plot(ts / expt.t_unit / expt.T__R, xs)
# -

# At least the classical code is consistent with itself!  Here we solve in all three frames:

ts = np.linspace(0, c.T, 100)
x_L, k_L = c.solveL(0, c.k0, ts)
x0s = c.x0(ts)
x_A, k_A = c.solveA(-c.x0(0), c.k0 - c.d(0), ts)
x_B, k_B = c.solveB(-c.x0(0), c.k0, ts)
plt.plot(ts, x_L)
plt.plot(ts, x_A + x0s)
plt.plot(ts, x_B + x0s)


expt = soc_adiabaticity.ExperimentAdiabaticityClassicalSymmetric(
    tube=False, T__R=0.2, cells_x=20, move_trap=True
)
s0 = expt.get_state()
s1 = expt.get_initial_state()
s1.plot(show_mixtures=True, show_momenta=True)

# +
from IPython.display import display, clear_output
from pytimeode.evolvers import EvolverABM
from mmfutils.contexts import NoInterrupt

e = EvolverABM(s1, dt=0.5 * s1.t_scale)
fig = None
txs = []
skip = 10
x = e.y.xyz[0]
with NoInterrupt() as i:
    n = 0
    while not i and e.t < s1.t_expansion:
        e.evolve(100)
        na, nb = e.y.get_density()
        txs.append((e.t, (x * (na + nb)).sum() / (na + nb).sum()))
        if n % skip == 0:
            fig = e.y.plot(fig, show_mixtures=True, show_momenta=True)
            display(plt.gcf())
            clear_output(wait=True)
        n += 1
# -

(c.x0(c.T / 2) + c.x0(c.T / 2, d=2) / c.omega2) / s1.k_r

# +
from soc_adiabaticity import ClassicalControlSymmetric


class C(ClassicalControlSymmetric):
    def _x0(self, t, d=0):
        if d == 0:
            return self.d0 * t
        elif d == 1:
            return self.d0
        else:
            return 0.0

    def y0(self, t):
        return 0.0


c = C(k0=c.k0, w=c.w, omega2=c.omega2, T=c.T, alpha=c.alpha)
# -

# Consider preparing the initial state in the ground state at some detuning $\delta > 0$.

# Here we consider the following procedure:
#
# 1. Start with a large `detuning` $\delta \approx 20$kHz, and prepare the system in the ground state.
# 2. Ramp $\delta$ to a `final_detuning` of $-1$kHz or so over a period of time `t__detuning_ramp`.
#
# If this ramp is slow, one should maintain a coherent population of the ground state, but at some point, the ramp will cease to be adiabatic.

# ## High-Level Description

# Note that the group velocity is $\tilde{v}(k) = k_R E'(k)/2E_R$, hence:
#
# $$
#   \diff{\tilde{v}(k)}{d} = \frac{2(\tilde{k}-d)^2 + w^2}{\sqrt{(\tilde{k}-d)^2+w^2}^3}.
# $$
#
# At the minimum, this becomes:
#
# $$
#   \diff{\tilde{v}(k)}{d} = \frac{2(\tilde{k}-d)^2 + w^2}{(\tilde{k}-d)^3}\tilde{k}^3
# $$

# $$
#   \diff{\tilde{v}(k)}{d} = \frac{2(\tilde{k}-d)^2 + w^2}{\sqrt{(\tilde{k}-d)^2+w^2}^3}
# $$

# ### Scales
#
# In the limit $g = g_{aa} = g_{bb} = g_{ab}$, the physics is governed by the following dimensionless parameters.  Everything here is scaled in units of either $2E_R$ (energies),  microns (lengths), or ms (`t_unit` for times):
#
# * Detuning $d$ and SOC strength $w$.
# * Central density $n_0$ in ($1/\mu$m$)^3$.
# * $l_r = 1/k_r$ (in microns).
# * Healing length $\xi = \sqrt{\hbar^2/2m\mu}$ (in $\mu$m).
# * Trap period $T = 2\pi /\omega$ (in ms).
# * Recoil timescale $T_R = \pi \hbar/2E_R$ (in ms).
#
# Here are the experimental values:
#
# * $d$ starts at $1.36$ and changes to $-0.068$
# * $0.25 < w < 0.73$.
# * $k_r \approx 7.9/\mu$m.
# * $n_0 \approx 160/\mu$m$^3$.
# * $\xi \approx 0.026\mu$m
# * $T_x \approx 326$ms
# * $T_\perp \approx 3.6$ms
# * $T_R = \pi\hbar/2E_R \approx 0.086$ms
# * $T_\omega \approx 0.13$ms

# +
from soc_adiabaticity import ExperimentAdiabaticity, u

e0 = ExperimentAdiabaticity(Nxyz=(2 ** 6,))
s0 = e0.get_state()
s0.plot()
na, nb = s0.get_central_density()
kR = e0.k_r
m = s0.ms.mean()
ER = s0.hbar ** 2 * kR ** 2 / 2 / m
n0 = max(na.max(), nb.max())
g = s0.gs.mean()
mu = g * n0
xi = np.sqrt(s0.hbar ** 2 / 2 / m / mu)
TR = 2 * np.pi * s0.hbar / (4 * ER)
res = [
    "d={:.2f}".format(e0.detuning / 4 / ER),
    "d_final={:.2f}".format(e0.final_detuning / 4 / ER),
    "w={:.2f}".format(e0.Omega / 4 / ER),
    "k_R={:.2g}um".format(kR / u.micron),
    "n_0={:.2f}/um^3".format(n0 * u.micron ** 3),
    "xi={:.2f}um".format(xi / u.micron),
    "Tx={:.2f}ms".format(2 * np.pi / s0.ws[0] / u.ms),
    "Tperp={:.2f}ms".format(2 * np.pi / s0.ws[1:].mean() / u.ms),
    "TR={:.3f}ms".format(TR / u.ms),
]
print (" ".join(res))

# print(e1.k_r**2*2/e1.ms[1], e1.final_detuning)

# -

print (e1.k_r ** 2 * 2 / e1.ms[1], e1.delta)
print (e1.k_r ** 2 * 2 / e1.ms[1], e1.final_detuning)

2 * np.pi / ((e1.k_r ** 2 * 2 / e1.ms[1] - e1.delta) ** 2 + e1.Omega ** 2) / u.ms

# # Homogeneous States

# We start with a homogeneous state in a small box (since the homogeneous states will just Bloch rotate) and check a few relationships.

# +
# %pylab inline --no-import-all
from IPython.display import display, clear_output
from pytimeode.evolvers import EvolverABM
from mmfutils.contexts import NoInterrupt
import soc_adiabaticity

reload(soc_adiabaticity)
from soc_adiabaticity import ExperimentAdiabaticity, ExperimentAdiabaticitySmall, u

e0 = ExperimentAdiabaticitySmall(
    w=0.475,
    d=1.36,
    final_d=-0.0679,
    l_r=1.0 / 7.9,
    xi=0.026,
    T__x=np.inf,
    T__perp=3.6,
    T__R=0.271,
    cells_x=20,
    dxyz=(0.1 * u.micron,),
)

s0 = e0.get_initial_state()
s0[:] *= 1 + 0.01 * np.random.random(s0.shape)
s = s0.copy()

e = EvolverABM(s, dt=0.2 * s.t_scale)
fig = None
with NoInterrupt() as i:
    while not i:
        e.evolve(500)
        fig = e.y.plot(fig)
        display(plt.gcf())
        clear_output(wait=True)

# +
import numpy as np
import soc_adiabaticity

reload(soc_adiabaticity)
from soc_adiabaticity import ExperimentAdiabaticity, ExperimentAdiabaticitySmall, u

e0 = ExperimentAdiabaticitySmall(
    w=0.475,
    d=1.36,
    final_d=-0.0679,
    l_r=1.0 / 7.9,
    xi=0.026,
    T__x=326,
    T__perp=3.6,
    T__R=0.271,
)

e1 = ExperimentAdiabaticity()
for _k in [
    "recoil_frequency_Hz",
    "k_r",
    "trapping_frequencies_Hz",
    "rabi_frequency",
    "detuning",
    "final_detuning",
]:
    assert np.allclose(getattr(e0, _k), getattr(e1, _k), rtol=0.01)

# +
# %pylab inline --no-import-all
from IPython.display import display, clear_output
from pytimeode.evolvers import EvolverABM
from mmfutils.contexts import NoInterrupt
from soc_adiabaticity import ExperimentAdiabaticity, ExperimentAdiabaticitySmall, u

expt = ExperimentAdiabaticitySmall(
    # T__x=np.inf,
    t__detuning_ramp=0.13 * 10,
    l_r=1.0,
    xi=2.0,
    cells_x=10,
    dxyz=[0.5],
)
s = expt.get_initial_state()
s.plot()
print s.ms
# -

e = EvolverABM(s, dt=0.2 * s.t_scale)
fig = None
with NoInterrupt() as i:
    while not i:
        e.evolve(500)
        fig = e.y.plot(fig)
        display(plt.gcf())
        clear_output(wait=True)

# # Trapped States

# +
# %pylab inline --no-import-all
from IPython.display import display, clear_output
from pytimeode.evolvers import EvolverABM
from mmfutils.contexts import NoInterrupt
from soc_adiabaticity import ExperimentAdiabaticity, ExperimentAdiabaticitySmall, u

expt = ExperimentAdiabaticitySmall(
    T__x=1.0,
    t__detuning_ramp=0.2,
    t__wait=20.0,
    l_r=1.0,
    xi=0.1,
    # final_d=-1.0,
    final_d=-0.0679,
    w=0.73,
    tube=True,
    cells_x=20,
    dxyz=[0.5],
)
s = expt.get_initial_state()
s.plot()
k = np.linspace(-3, 3, 100)
plt.figure()
plt.plot(k, (k ** 2 + 1) / 2 - np.sqrt((k - expt.d) ** 2 + expt.w ** 2))
plt.plot(k, (k ** 2 + 1) / 2 - np.sqrt((k - expt.final_d) ** 2 + expt.w ** 2))

# -

s.cooling_phase = 1
e = EvolverABM(s, dt=0.4 * s.t_scale)
fig = None
with NoInterrupt() as i:
    while not i:
        e.evolve(500)
        fig = e.y.plot(fig)
        # plt.clf()
        # e.y.plot_k()
        display(plt.gcf())
        clear_output(wait=True)


e = s.experiment
na, nb = s.get_central_density()
n0 = (na + nb).max()
print (e.detuning / (4 * e.E_R), e.final_detuning / (4 * e.E_R))
print (e.Omega / (4 * e.E_R))
g = s.gs.mean()
m = s.ms.mean()
print (e.k_r * u.micron)
print (n0 * u.micron ** 3)
xi = np.sqrt(g * n0 / 2 / m) * s.hbar
print (xi / u.micron)
Ts = 2 * np.pi / s.ws
print (Ts / e.t_unit)
print (2 * np.pi * s.hbar / e.E_R / e.t_unit)

# +
from soc_adiabaticity import ExperimentAdiabaticity, u

expt = ExperimentAdiabaticity(Nxyz=(2 ** 10,))
s = expt.get_initial_state()

# +
# %pylab inline
from IPython.display import clear_output

s = expt._debug_state
from pytimeode.evolvers import EvolverABM

s.cooling_phase = 1.0j
s.t = -100
print s.get_energy()
e = EvolverABM(s, dt=0.005 * s.t_scale)
for n in range(100):
    e.evolve(600)
    na, nb = e.y.get_density()
    x = e.y.xyz[0]
    plt.clf()
    plt.semilogy(x, na)
    display(plt.gcf())
    clear_output(wait=True)


# -
