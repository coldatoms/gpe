import numpy as np
import scipy.interpolate

sp = scipy

from gpe import utils
from gpe.soc import u
import gpe.soc

__all__ = ["Experiment"]


class Experiment(gpe.soc.Experiment):
    """Self-trapping "experiment".

    Attributes
    ----------

    """

    t_unit = u.ms
    B_gradient_mG_cm = 0  # No counterflow

    detuning_kHz = 2.0
    rabi_frequency_E_R = 1.5  # Rabi frequency

    initial_imbalance = None  # Start in true ground state.

    x_TF = 232.5 * u.micron  # Thomas Fermi "radius" (where V(x_TF) = mu)
    t__wait = 10  # Time to wait before expansion
    t__image = 10.1  # Time for expansion
    gaussian = False

    w = 0.475
    d = 0

    healing_length_micron = 0.2  # Healing length at center in micron
    x_TF = 2.0 * u.micron

    trapping_frequencies_Hz = None
    trapping_frequency_perp_Hz = 278.0

    barrier_width_micron = 2
    barrier_depth_mu = 2.0  # Barrier depth as a fraction of mu
    t__barrier = 0.1  # Time to turn on barrier

    cells_x = 100
    dxyz = (0.05,)
    tube = False

    def init(self):
        gpe.soc.Experiment.init(self)

        # Compute k_r, E_R, etc.
        hbar = u.hbar
        m = self.ms[0]

        E_R = self.E_R
        # k_r = np.sqrt(E_R/4/(hbar**2/2/m))

        self.rabi_frequency_E_R = 4 * self.w
        self.rabi_frequency_kHz = None

        self.detuning_kHz = None
        self.detuning_E_R = 4 * self.d
        detuning = self.detuning_E_R * E_R

        # Set mu to give specified xi
        xi = self.healing_length_micron * u.micron
        mu = (hbar / xi) ** 2 / 2 / m
        self.mus = (mu, mu)

        mu = (hbar / xi) ** 2 / 2 / m + detuning

        w_x = np.sqrt(2.0 * mu / m) / self.x_TF
        T_x = w_x / (2 * np.pi)
        assert self.trapping_frequencies_Hz is None
        self.trapping_frequencies_Hz = (
            T_x / u.Hz,
            self.trapping_frequency_perp_Hz,
            self.trapping_frequency_perp_Hz,
        )

        if not hasattr(self, "barrier_depth"):
            self.barrier_depth = self.barrier_depth_mu * mu
        if not hasattr(self, "barrier_width"):
            self.barrier_width = self.barrier_width_micron * u.micron

        self.barrier_depth_t_ = utils.get_smooth_transition(
            [0, self.barrier_depth], durations=[0.0], transitions=[self.t__barrier]
        )

        if not hasattr(self, "t__final") or np.isinf(self.t__final):
            # Only set if it has not already been set!
            self.t__final = self.t__barrier + self.t__wait

        gpe.soc.Experiment.init(self)
        self.Omega_t_ = utils.get_smooth_transition(
            [8 * self.Omega, self.Omega], durations=[0.0], transitions=[self.t__barrier]
        )
        gpe.soc.Experiment.init(self)

    def get_Vt(self, state):
        t_ = state.t / self.t_unit
        x = state.xyz_trap[0]
        if self.gaussian:
            # Gaussian barrier
            V_barrier = self.get("barrier_depth", t_=t_) * np.exp(
                -((x / self.barrier_width) ** 2) / 2.0
            )
        else:
            # Harmonic barrier
            V_barrier = self.get("barrier_depth", t_=t_) * np.maximum(
                0, 1 - (x / self.barrier_width) ** 2 / 2.0
            )
        return V_barrier, V_barrier
