"""Defines simulations to run with checkpoint/restart capabilities.

To use this, inherit from ExperimentBase, and pass any important variables to
the `local_dict` argument of the ExperimentBase.__init__() method.  These
variables will be used to generate unique filenames for each run.
"""
import itertools

import numpy as np

import mmf_setup.set_path.hgroot

from mmfutils.contexts import NoInterrupt

import mmfutils.performance.threads
import mmfutils.performance.fft

import sys

from gpe import utils

import soc_catch_and_release

u = soc_catch_and_release.u

_DATA_DIR = "_data"


def set_threads(numexpr=1, fft=1, *v):
    if v:
        numexpr = fft = v[0]
    mmfutils.performance.threads.set_num_threads(numexpr)
    mmfutils.performance.fft.set_num_threads(fft)


# Needs testing, but performance on swan did not show an appreciable benefit to
# using multiple cores.
set_threads(1)


class RunBase:
    dt_ = 1.0

    experiment_params = dict(
        cooling_phase=[1.0, 1 + 0.005j, 1 + 0.01j],
    )

    simulation_params = {}

    @property
    def experiments(self):
        params = self.experiment_params
        keys = params.keys()
        iterators = [params[_key] for _key in keys]
        return [
            self.Experiment(**dict(zip(keys, values)))
            for values in itertools.product(*iterators)
        ]

    @property
    def simulations(self):
        params = self.simulation_params
        keys = params.keys()
        iterators = [params[_key] for _key in keys]
        return [
            utils.Simulation(
                experiment=expt,
                dt_=self.dt_,
                data_dir=_DATA_DIR,
                **dict(zip(keys, values))
            )
            for expt in self.experiments
            for values in itertools.product(*iterators)
        ]

    def run(self):
        with NoInterrupt(ignore=True) as interrupted:
            for sim in self.simulations:
                if interrupted:
                    break
                sim.run()
                if interrupted:
                    break
                sim.run_images()


class Run0(RunBase):
    experiment_params = dict(
        cooling_phase=[1.0, 1 + 0.001j],  # , 1+0.005j],  # 1+0.01j, 1+0.015j],
        t__barrier=[0],
    )
    simulation_params = dict(
        image_ts_=[
            [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 20, 25, 30, 35, 40]
        ]
    )
    Experiment = soc_catch_and_release.ExperimentCatchAndRelease


class Run10(RunBase):
    t__barrier = 10
    experiment_params = dict(
        cooling_phase=[1.0, 1 + 0.001j],  # , 1+0.005j],  # 1+0.01j, 1+0.015j],
        t__barrier=[t__barrier],
    )
    simulation_params = dict(
        image_ts_=[
            t__barrier
            + np.array(
                [
                    0,
                    1,
                    2,
                    3,
                    4,
                    5,
                    6,
                    7,
                    8,
                    9,
                    10,
                    11,
                    12,
                    13,
                    14,
                    15,
                    20,
                    25,
                    30,
                    35,
                    40,
                ]
            )
        ]
    )
    Experiment = soc_catch_and_release.ExperimentCatchAndRelease


class Run0_200(Run0):
    """Same as Run0 but with -200nK barrier"""

    experiment_params = dict(
        cooling_phase=[1.0, 1 + 0.001j, 1 + 0.005j],  # 0+0.01j, 1+0.015j],
        barrier_depth_nK=[-200.0],
        t__barrier=[0],
    )


class Run0_400(Run0):
    """Same as Run0 but explicit -400nK barrier"""

    experiment_params = dict(
        cooling_phase=[1.0, 1 + 0.001j],  # , 1+0.005j],  # 1+0.01j, 1+0.015j],
        barrier_depth_nK=[-400.0],
        t__barrier=[0],
    )


class Run0_200_axial(Run0_200):
    """Same as Run0_200 but with axial symmetry"""

    experiment_params = dict(
        cooling_phase=[1.0, 1 + 0.001j],  # , 1+0.005j],  # 1+0.01j, 1+0.015j],
        basis_type=["axial"],
        barrier_depth_nK=[-200.0],
        t__barrier=[0],
    )
    simulation_params = dict(
        image_ts_=[
            np.array(
                [
                    0,
                    1,
                    2,
                    3,
                    4,
                    5,
                    6,
                    7,
                    8,
                    9,
                    10,
                    11,
                    12,
                    13,
                    14,
                    15,
                    20,
                    25,
                    30,
                    35,
                    40,
                ]
            )
        ]
    )
    Experiment = soc_catch_and_release.ExperimentCatchAndRelease


class Run0_50_axial_small(RunBase):
    """Same as Run0_50 but with axial symmetry"""

    experiment_params = dict(
        cooling_phase=[1.0, 1 + 0.001j],  # , 1+0.005j],  # 1+0.01j, 1+0.015j],
        basis_type=["axial"],
        barrier_depth_nK=[-50.0],
        t__barrier=[0],
        cells_x=[1600],
    )
    simulation_params = dict(
        image_ts_=[
            np.array([0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 20, 25, 30])
        ]  # , 35, 40])]   This box is too small for 30ms expansion
    )
    Experiment = soc_catch_and_release.ExperimentCatchAndReleaseSmall


class Run0_100_axial_small(RunBase):
    """Same as Run0_200 but with axial symmetry"""

    experiment_params = dict(
        cooling_phase=[1.0, 1 + 0.001j],  # , 1+0.005j],  # 1+0.01j, 1+0.015j],
        basis_type=["axial"],
        barrier_depth_nK=[-100.0],
        t__barrier=[0],
        cells_x=[1600],
    )
    simulation_params = dict(
        image_ts_=[
            np.array([0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 20, 25])
        ]  # , 30, 35, 40])]   This box is too small for 30ms expansion
    )
    Experiment = soc_catch_and_release.ExperimentCatchAndReleaseSmall


class Run0_200_axial_small(Run0_200):
    """Same as Run0_200 but with axial symmetry"""

    experiment_params = dict(
        cooling_phase=[1.0, 1 + 0.001j],  # , 1+0.005j],  # 1+0.01j, 1+0.015j],
        basis_type=["axial"],
        barrier_depth_nK=[-200.0],
        t__barrier=[0],
        cells_x=[1600],
    )
    simulation_params = dict(
        image_ts_=[
            np.array([0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 20, 25])
        ]  # , 30, 35, 40])]   This box is too small for 30ms expansion
    )
    Experiment = soc_catch_and_release.ExperimentCatchAndReleaseSmall


class Run0_400_axial_small(Run0_400):
    """Same as Run0_400 but with axial symmetry"""

    experiment_params = dict(
        cooling_phase=[1.0, 1 + 0.001j],  # , 1+0.005j],  # 1+0.01j, 1+0.015j],
        basis_type=["axial"],
        barrier_depth_nK=[-400.0],
        t__barrier=[0],
        cells_x=[1600],
    )
    simulation_params = dict(
        image_ts_=[
            np.array([0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 20, 25])
        ]  # , 30, 35, 40])]   This box is too small for 30ms expansion
    )
    Experiment = soc_catch_and_release.ExperimentCatchAndReleaseSmall


class Run10_200(Run10):
    """Same as Run10 but with smaller barrier"""

    t__barrier = 10
    experiment_params = dict(
        cooling_phase=[1.0, 1 + 0.001j],  # , 1+0.005j],  # 1+0.01j, 1+0.015j],
        barrier_depth_nK=[-200.0],
        t__barrier=[t__barrier],
    )
    simulation_params = dict(
        image_ts_=[
            t__barrier
            + np.array(
                [
                    0,
                    1,
                    2,
                    3,
                    4,
                    5,
                    6,
                    7,
                    8,
                    9,
                    10,
                    11,
                    12,
                    13,
                    14,
                    15,
                    20,
                    25,
                    30,
                    35,
                    40,
                ]
            )
        ]
    )
    Experiment = soc_catch_and_release.ExperimentCatchAndRelease


class Run10_200_axial(Run10):
    """Same as Run10_200 but with axial symmetry"""

    t__barrier = 10
    experiment_params = dict(
        cooling_phase=[1.0, 1 + 0.001j],  # , 1+0.005j],  # 1+0.01j, 1+0.015j],
        basis_type=["axial"],
        barrier_depth_nK=[-200.0],
        t__barrier=[t__barrier],
    )
    simulation_params = dict(
        image_ts_=[
            t__barrier
            + np.array(
                [
                    0,
                    1,
                    2,
                    3,
                    4,
                    5,
                    6,
                    7,
                    8,
                    9,
                    10,
                    11,
                    12,
                    13,
                    14,
                    15,
                    20,
                    25,
                    30,
                    35,
                    40,
                ]
            )
        ]
    )
    Experiment = soc_catch_and_release.ExperimentCatchAndRelease


class Run1a(RunBase):
    """Old runs done before correct parameters obtained"""

    experiment_params = dict(
        cooling_phase=[1.0],
        t__barrier=[0, 2],
    )
    Experiment = soc_catch_and_release.ExperimentCatchAndRelease1a


class Run1b(RunBase):
    """Old runs done before correct parameters obtained"""

    experiment_params = dict(
        cooling_phase=[1.0],
        t__barrier=[0, 2],
    )
    Experiment = soc_catch_and_release.ExperimentCatchAndRelease1b


if __name__ == "__main__":
    cmd = sys.argv[1]
    if cmd in locals():
        locals()[cmd]().run()
    else:
        raise ValueError("Command {} not found.".format(cmd))
