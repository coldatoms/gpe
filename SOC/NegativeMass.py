# ---
# jupyter:
#   jupytext:
#     formats: ipynb,py
#     text_representation:
#       extension: .py
#       format_name: light
#       format_version: '1.5'
#       jupytext_version: 1.4.0
#   kernelspec:
#     display_name: Python 2 (Ubuntu, plain)
#     language: python
#     name: python2-ubuntu
# ---

# + init_cell=true
import mmf_setup

mmf_setup.nbinit()
from gpe.imports import *

# -

# # Negative Mass Hydrodynamics

# In this notebook we reproduce the results of our first paper [Khamehchi:2017] using the new code.
#
# [Khamehchi:2017]: https://doi.org/10.1103/PhysRevLett.118.155301 (M.~A. Khamehchi, Khalid Hossain, M.~E. Mossman, Yongping Zhang, Thomas Busch, Michael McNeil Forbes, and Peter Engels, "Negative mass hydrodynamics in a spin-orbit--coupled Bose-Einstein condensate", prl 118(15), 155301 (2017) [1612.04055](http://arXiv.org/abs/1612.04055))
#

# ## Ground State

# +
# %load_ext autoreload
# %autoreload 2
from SOC.soc_negative_mass import ExperimentNegativeMass, u

e = ExperimentNegativeMass()
s0 = e.get_state()
s0.plot()
# -

# %%time
s1 = e.get_initial_state()
s1.plot()

np.save("nm_ground_state.npy", s1[...])


# ## $\delta=0.54E_R$

e1 = ExperimentNegativeMass()
s1 = e.get_state()
s1[...] = np.load("nm_ground_state.npy")
history1 = [s1]

ev = EvolverABM(history1[-1], dt=0.5 * s0.t_scale)
pe = None
with NoInterrupt() as interrupted:
    while not interrupted:
        ev.evolve(100)
        history1.append(ev.get_y())
        pe = ev.y.plot(pe, history=history1)
        display(pe.fig)
        clear_output(wait=True)

# ## $\delta=1.36E_R$

e1a = ExperimentNegativeMass(detuning_E_R=1.36)
s1a = e1a.get_state()
s1a[...] = np.load("nm_ground_state.npy")
history1a = [s1a]

ev = EvolverABM(history1a[-1], dt=0.5 * s0.t_scale)
pe = None
with NoInterrupt() as interrupted:
    while not interrupted:
        ev.evolve(100)
        history1a.append(ev.get_y())
        pe = ev.y.plot(pe, history=history1a)
        display(pe.fig)
        clear_output(wait=True)

# ## $\delta=2.71E_R$

e1b = ExperimentNegativeMass(detuning_E_R=2.71)
s1b = e1b.get_state()
s1b[...] = np.load("nm_ground_state.npy")
history1b = [s1b]

ev = EvolverABM(history1b[-1], dt=0.2 * s0.t_scale)
pe = None
with NoInterrupt() as interrupted:
    while not interrupted:
        ev.evolve(100)
        history1b.append(ev.get_y())
        pe = ev.y.plot(pe, history=history1b)
        display(pe.fig)
        clear_output(wait=True)
