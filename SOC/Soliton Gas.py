# ---
# jupyter:
#   jupytext:
#     formats: ipynb,py
#     text_representation:
#       extension: .py
#       format_name: light
#       format_version: '1.5'
#       jupytext_version: 1.4.0
#   kernelspec:
#     display_name: Python [conda env:_gpe]
#     language: python
#     name: conda-env-_gpe-py
# ---

# + init_cell=true
import mmf_setup

mmf_setup.nbinit()
import holoviews as hv

hv.extension("bokeh")
# -

# # Bloch Sphere

# We represent homogeneous states as:
#
# $$
#   \ket{\psi} = \sqrt{n}\begin{pmatrix}
#     \cos\frac{\theta}{2}\\
#     e^{\I\phi}\sin\frac{\theta}{2}
#   \end{pmatrix}.
# $$
#
# Note that a vector $\vect{a} = (a_x, a_y, a_z) = (\sin\theta\cos\phi, \sin\theta\sin\phi, \cos\theta)$,
#
#
# $$
#   \vect{a}\cdot\vect{\sigma} =
#   \begin{pmatrix}
#     c_\theta^2 - s_\theta^2 & 2s_\theta c_\theta e^{\I\phi}\\
#     2s_\theta c_\theta e^{-\I\phi} & -c_\theta^2 + s_\theta^2
#   \end{pmatrix}
# \\
#   \begin{pmatrix}
#     2s_\theta c_\theta\cos\phi\\
#     2s_\theta c_\theta\sin\phi\\
#     2c_\theta^2 - 1
#   \end{pmatrix}
# $$

# $$
#   \sqrt{a}\begin{pmatrix}
#     \sqrt{(1+a_z^2)/2}\\
#     (a_x +\I a_y)/2/\sqrt{(1+a_z^2)/2}
#   \end{pmatrix}
# $$

# +
import logging
import soc_soliton_gas

reload(soc_soliton_gas)
import gpe.soc

reload(gpe.soc)
from soc_soliton_gas import ExperimentLinearResponse, u
from gpe.utils import Simulation2

expt = ExperimentLinearResponse(
    t__SOC_on=0.1,
    cells_x=10,
    dxyz=(0.05 * u.micron,),
    initial_state=(287 / u.micron**3, 0, 0),  # Max density in Experiment1
    trapping_frequencies_Hz=(0.0, 278.08, 278.04),
)
sim = Simulation2(
    experiment=expt,
    dt_=0.1,
    t__final=10,
    # logging_level=logging.ERROR
)
sim.run()
# sim.delete_data()

# +
# %%output size=100
# %%opts QuadMesh [height=300 width=900 colorbar=True tools=['hover'] toolbar='above']
# %%opts Image [height=300 width=900 colorbar=True tools=['hover'] toolbar='above']
# %%opts Curve [height=300 width=900] {+framewise}
# %%opts QuadMesh (cmap='viridis')
# %%opts Image (cmap='viridis')

sim.view_hv()
# -

s = expt.get_state()
s[1]

# # Solitons

# Here we start at the negative-mass state with a perturbation.  Hopefully this will develop solitons.

# +
import copy, logging
import numpy as np
import gpe.soc

reload(gpe.soc)
from soc_soliton_gas import ExperimentLinearResponse, u
from gpe.utils import Simulation2, step


class ExperimentSoliton1(ExperimentLinearResponse):
    pulse = "step"
    edge_width = 1.0
    pulse_width = 5.0
    pulse_height = 0.01
    cells_x = 100
    rabi_frequency_E_R = 1.0

    t__wait = 10.0  # Time to wait before imaging (up to 500ms)
    t__expand = 7  # Time for expansion

    @property
    def t__expansion(self):
        return self.t__wait

    def Omega_t_(self, t_):
        return self.Omega

    def delta_t_(self, t_):
        return self.detuning

    def get_initial_state(self):
        state = self.get_state()
        x = state.xyz[0]

        if self.pulse == "gaussian":
            pulse = np.exp(-((x / self.pulse_width) ** 2) / 2.0)
        elif self.pulse in set(["step", "basin"]):
            pulse = -np.vectorize(step)(
                x - self.pulse_width / 2.0, self.edge_width
            ) + np.vectorize(step)(x + self.pulse_width / 2.0, self.edge_width)
            if self.pulse == "basin":
                # Basis of one phase in the middle of the other
                k = 2.0 * (pulse - 0.5) * state.k_r
                state[0] *= pulse * np.exp(1j * (state.k_r + k) * x)
                state[1] *= (1 - pulse) * np.exp(1j * (-state.k_r + k) * x)
                return state
        else:
            np.random.seed(1)
            pulse = np.random.random(x.shape)
        f = 1 + self.pulse_height * pulse
        state[0] *= f * np.exp(1j * state.k_r * x)
        state[1] *= f * np.exp(-1j * state.k_r * x)
        return state

    def get_initialized_state(self, state):
        """Return a valid state initialized from `state` for expansion."""
        state_ = self.get_state()
        state_[...] = state
        return state_


expt1 = ExperimentSoliton1(
    dxyz=(0.1 * u.micron,),
    cooling_phase=1 + 0.01j,
    detuning_kHz=0.0,
    initial_state=(287 / u.micron**3, 0, 0),  # Max density in Experiment1
    trapping_frequencies_Hz=(0.0, 278.08, 278.04),
)

expt2 = ExperimentSoliton1(
    pulse_height=0.1,
    dxyz=(0.1 * u.micron,),
    cooling_phase=1 + 0.05j,
    detuning_kHz=0.0,
    initial_state=(287 / u.micron**3, 0, 0),  # Max density in Experiment1
    trapping_frequencies_Hz=(0.0, 278.08, 278.04),
)

expt3 = ExperimentSoliton1(
    pulse="random",
    pulse_height=0.1,
    dxyz=(0.05 * u.micron,),
    cooling_phase=1 + 0.05j,
    detuning_kHz=0.0,
    initial_state=(287 / u.micron**3, 0, 0),  # Max density in Experiment1
    trapping_frequencies_Hz=(0.0, 278.08, 278.04),
)

args = dict(
    pulse="basin",
    pulse_width=50.0,
    cells_x=300,
    dxyz=(0.05 * u.micron,),
    detuning_kHz=0.0,
    initial_state=(287 / u.micron**3, 0, 0),  # Max density in Experiment1
    trapping_frequencies_Hz=(0.0, 278.08, 278.04),
)

expt4 = ExperimentSoliton1(cooling_phase=1 + 1j, t__wait=50.0, t__expand=0.0, **args)
expt4_exp = ExperimentSoliton1(cooling_phase=1.0, t__wait=0.0, **args)

expt5 = ExperimentSoliton1(
    cells_x=1000,
    pulse="random",
    pulse_height=0.1,
    dxyz=(0.05 * u.micron,),
    cooling_phase=1j,  # 1+0.5j,
    detuning_kHz=0.0,
    initial_state=(287 / u.micron**3, 0, 0),  # Max density in Experiment1
    trapping_frequencies_Hz=(0.0, 278.08, 278.04),
)

sim = Simulation2(
    experiment=expt4,
    dt_=0.1,
    # t__final=100,
    # logging_level=logging.ERROR
)
sim_exp = Simulation2(
    experiment=expt4_exp,
    extends=(sim, 20.0),
    dt_=0.1
    # logging_level=logging.ERROR
)
sim.run()
# sim_exp.run()

# +
# %%output size=100
# %%opts QuadMesh [height=300 width=900 colorbar=True tools=['hover'] toolbar='above']
# %%opts Image [height=300 width=900 colorbar=True tools=['hover'] toolbar='above']
# %%opts Curve [height=300 width=900] {+framewise}
# %%opts QuadMesh (cmap='viridis')
# %%opts Image (cmap='viridis')

sim.view_hv("b")
# -

from pytimeode.evolvers import EvolverABM
from mmfutils.contexts import NoInterrupt

s_ = s1.copy()
s_.cooling_phase = 1.0
s_.t = s.t_expansion
e = EvolverABM(s_, dt=0.1 * s_.t_scale)
with NoInterrupt(ignore=True) as i:
    while not i:
        e.evolve(1000)
        plt.clf()
        e.y.plot()
        display(plt.gcf())
        clear_output(wait=True)
        plt.close("all")

# # Soliton Gas

# Here we want to setup a soliton gas.  We will randomly specify the location of the solitonic transitions and then add a randomized phase field to give them motion.

expt3.plot_dispersion(hv=hv).cols(1)

# +
import copy, logging
import numpy as np
import gpe.soc

reload(gpe.soc)
import soc_soliton_gas

reload(soc_soliton_gas)
from soc_soliton_gas import ExperimentSolitonGas, u
from gpe.utils import Simulation2, step

expt1 = ExperimentSolitonGas()
expt2 = ExperimentSolitonGas(cooling_phase=1 + 0.05j)
expt3 = ExperimentSolitonGas(detuning_E_R=0.0, detuning_kHz=0.0, cooling_phase=1 + 0.05j)

expt4 = ExperimentSolitonGas(detuning_E_R=0.1, detuning_kHz=0.0, cooling_phase=1 + 0.05j)

expt5 = ExperimentSolitonGas(
    detuning_E_R=0.1,
    detuning_kHz=0.0,
    healing_length_micron=10.0,
    cooling_phase=1 + 0.05j,
)

expt5t = ExperimentSolitonGas(
    detuning_E_R=0.1,
    detuning_kHz=0.0,
    T__x=20.0,
    healing_length_micron=10.0,
    cooling_phase=1 + 0.05j,
)

expt5t1 = ExperimentSolitonGas(
    detuning_E_R=0.1,
    detuning_kHz=0.0,
    T__x=50.0,
    healing_length_micron=10.0,
    cooling_phase=1 + 0.05j,
)

sim = Simulation2(
    experiment=expt5t,
    dt_=0.1,
    # t__final=100,
    # logging_level=logging.ERROR
)

# #!rm -rf _data/ExperimentSolitonGas/
display(sim.experiment.plot_dispersion(hv=hv))
sim.run()


# -

# %%output size=100
# %%opts QuadMesh [height=300 width=900 colorbar=True tools=['hover'] toolbar='above']
# %%opts Image [height=300 width=900 colorbar=True tools=['hover'] toolbar='above']
# %%opts Curve [height=300 width=900] {+framewise}
# %%opts QuadMesh (cmap='viridis')
# %%opts Image (cmap='viridis')
sim.view_hv()

s = sim.get_state(t_=0)
na, nb = s.get_density()
gaa, gab, gab = s.gs
s.mu = gaa * na.max()
Va, Vb, Vab = s.get_Vext()
hv.Curve((x, Va))
na.max()

s = sim.get_state(0)
s.mu = s.get_mu()
from gpe.minimize import MinimizeState

m = MinimizeState(s, fix_N=False)
s1 = m.minimize()

# %pylab inline
from IPython.display import clear_output

s = sim.get_state(8)
from gpe.minimize import MinimizeState

m = MinimizeState(s)
states = []


def callback(state, n=[0]):
    if n[0] % 100 == 0:
        states.append(state.copy())
        plt.clf()
        state.plot()
        display(plt.gcf())
        clear_output(wait=True)
        plt.close("all")
    n[0] += 1


s1 = m.minimize(callback=callback)

s1 = states[-1]

# +
# %%output size=100
# %%opts QuadMesh [height=300 width=900 colorbar=True tools=['hover'] toolbar='above']
# %%opts Image [height=300 width=900 colorbar=True tools=['hover'] toolbar='above']
# %%opts Curve [height=300 width=900] {+framewise}
# %%opts QuadMesh (cmap='viridis')
# %%opts Image (cmap='viridis')

# Pixel size = 1.1166u.micron
sim_exp.view_hv(species="b")
# -

# %pylab osx
sim.view()

# +
# %%output size=100
# %%opts QuadMesh [height=300 width=900 colorbar=True tools=['hover'] toolbar='above']
# %%opts Image [height=300 width=900 colorbar=True tools=['hover'] toolbar='above']
# %%opts Curve [height=300 width=900] {+framewise}
# %%opts QuadMesh (cmap='viridis')
# %%opts Image (cmap='viridis')

from IPython.display import clear_output
import numpy as np
import xarray as xr
from holoviews.operation.datashader import regrid, datashade

import mmf_setup.set_path.hgroot
import gpe.utils

reload(gpe.utils)
import gpe.visualize

reload(gpe.visualize)
import gpe.soc

reload(gpe.soc)
import runs

reload(runs)

run = runs.Run5()
ind = 6
expt = run.experiments[ind]
sim = run.simulations[ind]


class Simulation(gpe.visualize.SimulationMixin2, gpe.utils.Simulation):
    pass


sim = Simulation(experiment=expt, dt_=sim.dt_)
d = sim.data
clear_output()
l = sim.view_hv()
l
# -

# %debug

sim._set_states()
sim._states[0].kxyz[2]

#
