# ---
# jupyter:
#   jupytext:
#     formats: ipynb,py
#     text_representation:
#       extension: .py
#       format_name: light
#       format_version: '1.5'
#       jupytext_version: 1.4.0
#   kernelspec:
#     display_name: Python 2 (Ubuntu, plain)
#     language: python
#     name: python2-ubuntu
# ---

import cProfile

# %load_ext line_profiler

import runs

reload(runs)
# runs.set_threads(2)
r = runs.Run1()
s = r.simulations[0]
s.t__final = 0
s.run()

s.checkpoint = False
s.t__final = 0.5
s.dt_ = 0.5
# runs.set_threads(2)
# from mmfutils.performance import fft
# fft.set_num_threads(2)
cProfile.run("s.run()", "prof.dat")

# +
# Numpy fft: 1 core: 0->0.5 takes 52s
# Numpy fft: 2 cores: 0->0.5 takes s
# Numpy fft: 4 cores: 0->0.5 takes s
# PyFFTW: 1 core: 0->0.5 takes 39s  (100% CPU usage)
# PyFFTW: 2 core: 0->0.5 takes 33s  (127% CPU usage)
# PyFFTW: 4 core: 0->0.5 takes 30s  (153% CPU usage)
# PyFFTW: 8 core: 0->0.5 takes 30s  (194% CPU usage)
# -

import numpy as np

s_ = s.experiment.get_state()
X = s_.data * 1
from mmfutils.performance import fft

runs.set_threads(4)
fft.set_num_threads(4)
fft_ = fft.get_fft_pyfftw(X * 1)
# %timeit Y = fft_(X)

import numpy as np
import runs
from mmfutils.performance import fft

runs.set_threads(4)
fft.set_num_threads(8)
np.random.seed(2)
shape = (128, 128, 128)
shape = (1, 128 * 128)
X = np.random.random(shape) + 1j * np.random.random(shape)
fft_ = fft.get_fftn_pyfftw(X * 1)
# %timeit Y = fft_(X)

# +
# fft.get_fftn_pyfftw?
# -

fft._THREADS
