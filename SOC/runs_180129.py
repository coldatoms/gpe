"""Simulations corresponding to the experimental runs data_180129.

"""
import itertools
import numpy as np

# Set sys.path to include hgroot so modules can be found.
import mmf_setup.set_path.hgroot

from mmfutils.contexts import NoInterrupt

import mmfutils.performance.threads
import mmfutils.performance.fft

import sys

from gpe import utils

import soc_catch_and_release

u = soc_catch_and_release.u

_DATA_DIR = "_data"


def set_threads(numexpr=1, fft=1, *v):
    if v:
        numexpr = fft = v[0]
    mmfutils.performance.threads.set_num_threads(numexpr)
    mmfutils.performance.fft.set_num_threads(fft)


# Needs testing, but performance on swan did not show an appreciable benefit to
# using multiple cores.
set_threads(1)


class RunBase:
    """Defines a set of runs based on experimental and simulation parameters.

    Provide a range of parameters for either the experiment or the simulation
    and the run() method will iterate through all combinations.
    """

    dt_ = 1.0

    experiment_params = dict(
        cooling_phase=[1.0, 1 + 0.005j, 1 + 0.01j],
    )

    simulation_params = {}

    @property
    def experiments(self):
        params = self.experiment_params
        keys = params.keys()
        iterators = [params[_key] for _key in keys]
        return [
            self.Experiment(**dict(zip(keys, values)))
            for values in itertools.product(*iterators)
        ]

    @property
    def simulations(self):
        params = self.simulation_params
        keys = params.keys()
        iterators = [params[_key] for _key in keys]
        return [
            utils.Simulation(
                experiment=expt,
                dt_=self.dt_,
                data_dir=_DATA_DIR,
                **dict(zip(keys, values))
            )
            for expt in self.experiments
            for values in itertools.product(*iterators)
        ]

    def run(self):
        with NoInterrupt(ignore=True) as interrupted:
            for sim in self.simulations:
                if interrupted:
                    break
                sim.run()
                if interrupted:
                    break
                sim.run_images()


class Run_SLOW_24uW_axial_old(RunBase):
    """3D Simulations with axial symmetry corresponding to::

        data_180129/C&R_SlowLoad_24uW_1.txt
                    C&R_SlowLoad_24uW_2.txt
                    C&R_SlowLoad_24uW_3.txt

    These are done in a slightly smaller box and the barrier_depth_nK is -50
    rather than -50.4nK because they were run earlier (hence the old).  They
    should still reproduce the results well.
    """

    experiment_params = dict(
        cooling_phase=[1.0, 1 + 0.001j],  # , 1+0.005j],  # 1+0.01j, 1+0.015j],
        basis_type=["axial"],
        barrier_depth_nK=[-50.0],
        t__barrier=[0],
        cells_x=[1600],
    )
    simulation_params = dict(
        image_ts_=[
            np.array([0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 20, 25, 30])
        ]  # , 35, 40])] Box is too small for 30ms expansion
    )
    Experiment = soc_catch_and_release.ExperimentCatchAndReleaseSmall


class Run_SLOW_13uW_small(RunBase):
    """3D Simulations with axial symmetry corresponding to::

    data_180129/C&R_SlowLoad_13uW_1.txt
                C&R_SlowLoad_13uW_2.txt
                C&R_SlowLoad_13uW_3.txt
    """

    experiment_params = dict(
        cooling_phase=[1.0],  # , 1+0.005j],  # 1+0.01j, 1+0.015j],
        tube=["true"],
        x_TF=[280 * u.micron],
        barrier_depth_nK=[-27.3],
        t__barrier=[0],
        cells_x=[400],
    )
    simulation_params = dict(
        image_ts_=[
            np.array([0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10])
        ],  # , 11, 12, 13, 14, 15, 20, 25, 30, 35, 40])]
        t__final=[10],
    )
    Experiment = soc_catch_and_release.ExperimentCatchAndReleaseSmall


class Run_SLOW_6uW_small(RunBase):
    """3D Simulations with axial symmetry corresponding to::"""

    experiment_params = dict(
        cooling_phase=[1.0],  # , 1+0.005j],  # 1+0.01j, 1+0.015j],
        tube=["true"],
        x_TF=[280 * u.micron],
        barrier_depth_nK=[-12.6],
        t__barrier=[0],
        cells_x=[400],
    )
    simulation_params = dict(
        image_ts_=[
            np.array([0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10])
        ],  # , 11, 12, 13, 14, 15, 20, 25, 30, 35, 40])]
        t__final=[10],
    )
    Experiment = soc_catch_and_release.ExperimentCatchAndReleaseSmall


class Run_SLOW_13uW_axial_small(RunBase):
    """3D Simulations with axial symmetry corresponding to::

        data_180129/C&R_SlowLoad_24uW_1.txt
                    C&R_SlowLoad_24uW_2.txt
                    C&R_SlowLoad_24uW_3.txt

    These are done in a slightly smaller box and the barrier_depth_nK is -50
    rather than -50.4nK because they were run earlier (hence the old).  They
    should still reproduce the results well.
    """

    experiment_params = dict(
        cooling_phase=[1.0],  # , 1+0.005j],  # 1+0.01j, 1+0.015j],
        basis_type=["axial"],
        x_TF=[280 * u.micron],
        barrier_depth_nK=[-27.3],
        t__barrier=[0],
        cells_x=[400],
    )
    simulation_params = dict(
        image_ts_=[
            np.array([0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10])
        ],  # , 11, 12, 13, 14, 15, 20, 25, 30, 35, 40])]
        t__final=[10],
    )
    Experiment = soc_catch_and_release.ExperimentCatchAndReleaseSmall


'''
class Run_SLOW(RunBase):
    """Detailed imaging procedure of the initial state.

    data_180517/C&Rbase_twait_0ms.txt
    """
    dt_ = 0.5
    key = 'C&Rbase_twait_0ms.txt'
    
    experiment_params = dict(
        rabi_frequency_E_R=[0.0],   # No SOC
        cooling_phase=[1.0],
        Lx=[500*u.micron],
        t__barrier=[0],
        tube=[True],
    )
    simulation_params = dict(
        image_ts_=[[0, 2, 4, 6, 8, 10, 12, 14]],
        t__final=[14],
    )
    Experiment = soc_catch_and_release.ExperimentCatchAndRelease
    
    
class Run_FAST(RunBase):
    """Detailed imaging procedure of the initial state.

    data_180517/C&Rbase_50uW_fast_texp3ms.txt
    """
    dt_ = 0.5
    key = 'C&Rbase_50uW_fast_texp3ms.txt'
    
    experiment_params = dict(
        rabi_frequency_E_R=[0.0],   # No SOC
        cooling_phase=[1.0],
        Lx=[500*u.micron],
        t__barrier=[10.0],
        tube=[True],
        t__image=[3],
        barrier_depth_nK=[-105],  # 50 uW
    )
    simulation_params = dict(
        image_ts_=[[10, 12, 14, 16, 18, 20, 22, 24]],
        t__final=[24],
    )
    Experiment = soc_catch_and_release.ExperimentCatchAndRelease
'''

if __name__ == "__main__":
    cmd = sys.argv[1]
    if cmd in locals():
        locals()[cmd]().run()
    else:
        raise ValueError("Command {} not found.".format(cmd))
