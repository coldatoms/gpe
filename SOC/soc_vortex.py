import numpy as np

from scipy.integrate import odeint

from gpe import utils
from gpe.soc import Experiment
from SOC.soc_catch_and_release import (
    u,
    ExperimentCatchAndRelease,
    ExperimentCatchAndReleaseSmall,
)


class ExperimentVortex(ExperimentCatchAndRelease):
    """Vortex in a BEC with SOC along the x axis.

    This class provides two ways of defining the parameters.  The
    first is in terms of the experimental values.  The second is in
    terms of dimensionless parameters.
    """

    t_unit = u.ms
    B_gradient_mG_cm = 0  # No counterflow

    # Experimental parameters
    trapping_frequencies_Hz = (3.07, 278.08, 278.04)  # Trap.

    x_TF = 155 * u.micron  # Thomas Fermi "radius" (where V(x_TF) = mu)

    # SOC Parameters
    recoil_frequency_Hz = 3683.8  # Sets E_R and k_r
    detuning_kHz = 2.0  # Detuning (delta = d*4*E_R)
    detuning_E_R = None

    rabi_frequency_E_R = 1.5  # Rabi frequency (Omega = w*4*E_R)

    initial_imbalance = None  # Start in true ground state.
    t__image = 7  # time for expansion when imaging
    t__final = np.inf

    # Optional dimensionless way of defining the various parameters.
    w = 0.375  # Alternative for Omega = w*4*E_R
    d = 1.36  # Alternative for delta = d*4*E_R
    l_r = 1.27 * u.micron  # Alternative for k_R = 2*np.pi / l_r
    healing_length = 0.24 * u.micron  # Alternative for x_TF
    T__R = 0.271  # Alternative for E_R

    # Default: use experimental parameters
    w = d = l_r = healing_length = T__R = None

    # Basis and Box
    R = 5.0 * u.micron
    dx = 0.12 * u.micron

    # Cylindrical potential
    V_width = 1 * u.micron
    V_height_mu = 10.0
    V_R = 0.9

    # Gaussian finger
    Vf_sigma = 0.5 * u.micron
    Vf_height_mu = 0.5
    Vf_v_c = 0.2

    tube = False

    def init(self):
        # Compute everything as before from experimental parameters
        super().init()

        m = np.mean(self.ms)
        E_R = self.E_R
        k_r = self.k_r
        detuning = self.detuning
        Omega = self.Omega

        # Now update
        hbar = u.hbar
        T__R = getattr(self, "T__R", None)
        if T__R:
            T_R = self.T__R * self.t_unit
            recoil_frequency = 1.0 / T_R
            E_R = 2 * np.pi * recoil_frequency * hbar
            self.recoil_frequency_Hz = recoil_frequency / u.Hz

        l_r = getattr(self, "l_r", None)
        if l_r:
            k_r = 1.0 / self.l_r
            m = 1.0 / (2 * E_R / (hbar * k_r) ** 2)
            self.ms = (m, m)

        d = getattr(self, "d", None)
        if d:
            del self.detuning
            self.detuning_kHz = None
            self.detuning_E_R = 4 * d
            detuning = d * 4 * E_R

        w = getattr(self, "w", None)
        if w:
            del self.rabi_frequency
            self.rabi_frequency_E_R = 4 * w
            self.rabi_frequency_kHz = None
            Omega = w * 4 * E_R

        # Set mu to give specified xi
        healing_length = getattr(self, "healing_length", None)
        if healing_length:
            self.mu = (hbar / healing_length) ** 2 / 2 / m

        self.Lxyz = (2 * self.R,) * 2
        N = utils.get_good_N(2 * self.R / self.dx)
        self.Nxyz = (N, N)

        super().init()

        assert np.allclose(self.E_R, E_R)
        assert np.allclose(self.k_r, k_r)
        assert np.allclose(self.detuning, detuning)
        assert np.allclose(self.Omega, Omega)
        assert np.allclose(np.mean(self.ms), m)

    def get_Vtrap(self, state, xyz):
        """Return (V_a, V_b, V_ab), the external potentials."""
        # Convert times into experiment units
        t_ = state.t / self.t_unit
        V_TF = self.fiducial_V_TF

        x, y = xyz[:2]
        r = np.sqrt(x**2 + y**2)
        R = self.R * self.V_R
        dR = self.V_width
        Va = utils.get_smooth_transition(
            [0, self.V_height_mu * V_TF], durations=[R - dR], transitions=[dR]
        )(r)
        Vb = Va.copy()

        Va_t, Vb_t = self.get_Vt(state=state)
        Va += Va_t
        Vb += Vb_t
        return Va, Vb

    def get_Vt(self, state):
        """Start with a potential off to the side which we bring in
        and stop in the center."""
        if hasattr(self, "_computing_fiducial_V_TF"):
            return (0.0, 0.0)
        x0 = -self.R - 1 * self.Vf_sigma
        mu = self.fiducial_V_TF
        Vf = self.Vf_height_mu * mu
        c_s = np.sqrt(mu / np.mean(self.ms))
        v = self.Vf_v_c * c_s
        x_t = min(0, x0 + v * state.t)
        sigma = self.Vf_sigma

        x, y = state.xyz[:2]
        Vt = Vf * np.exp(-((x - x_t) ** 2 + y**2) / 2 / sigma**2)
        return (Vt, Vt)

    def get_state(self, initialize=True):
        state = super().get_state(initialize=initialize)
        if initialize:
            x, y = self._xyz_trap(state)[:2]
            state[...] *= np.exp(1j * np.angle(x + 1j * y))
        return state

    def plot(self, state, fig=None):
        from gpe.plot_utils import MPLGrid
        from matplotlib import pyplot as plt
        from mmfutils.plot import imcontourf

        if fig is None:
            fig = plt.figure(figsize=(15, 5))

        plt.clf()
        grid = MPLGrid(fig=fig, direction="right")

        t_ = state.t / self.t_unit
        x, y = state.xyz[:2]
        na, nb = state.get_density()
        n = na + nb
        E = state.get_energy()
        Ns = state.get_Ns()

        for _n in [na, nb, n]:
            _grid = grid.grid(direction="down")
            cax = _grid.next(0.06)
            _grid.next()
            imcontourf(x / u.micron, y / u.micron, _n, aspect=1)
            plt.xlabel("x [micron]")
            plt.ylabel("y [micron]")
            plt.colorbar(cax=cax, orientation="horizontal")
            cax.xaxis.set_ticks_position("top")

        plt.suptitle(f"t_={t_:.4f}, Ns=({Ns[0]:.1f}, {Ns[1]:.1f}), E={E:.2f}")

    def get_initial_state(self, use_scipy=True, E_tol=1e-8, **kw):
        # We don't want to minimize too much because we often lose the vortex.
        return super().get_initial_state(use_scipy=use_scipy, E_tol=E_tol, **kw)


class ExperimentVortexPair(ExperimentCatchAndReleaseSmall):
    """Here we imprint a vortex anti-vortex pair symmetric across the
    axis in a 2D box.

    We imprint the pair in the majority component then cool a bit.
    """

    recoil_frequency_Hz = 3683.8
    mu_nK = 105.0
    detuning_E_R = 0.54
    detuning_kHz = None
    rabi_frequency_E_R = 1.5
    trapping_frequencies_Hz = (3.07, 278, 278)
    basis_type = "2D"
    single_band = True
    cells_x = 40
    R = 3.0 * u.micron
    old_cells_x = False
    T__x = np.inf

    # The experiment puts 4 vortices symmetric about the axes.  This
    # parameter specifies the location of one positive-orientation
    # vortex
    vortex_cooling_t_ = 0.2  # Time to cool vortices
    vortex = (5, -3)
    vortices = None  # Can explicitly override.
    barrier_depth_nK = -150
    barrier_width_micron = 2.0  # Width of central Gaussian beam
    t__barrier = 0.0  # Slow loading

    def init(self):
        self.mu = self.mu_nK * u.nK
        super().init()
        if self.R is not None:
            Ly = 2 * self.R
            self.Lxyz = (self.Lx, Ly)
            self.Nxyz = [utils.get_good_N(_L / self.dx) for _L in self.Lxyz]

        if self.vortices is None:
            x0_, y0_ = self.vortex
            self.vortices = [(x0_, y0_), (-x0_, -y0_)]

    def get_initial_state(self, _initial=False):
        """Return the initial state.

        Arguments
        ---------
        vortices : [(x, y)]
           List of vortex locations in microns.  These are positively
           oriented - use a negative y to imprint a pair of the
           opposite orientation.
        """

        if _initial:
            return super().get_initial_state()

        if self.get("barrier_depth", t_=0) == 0:
            # Initializes from a single unit cell assuming that the
            # density is constant.
            args = self.get_persistent_rep()[1]
            args["cells_x"] = 1

            s0 = self.__class__(**args).get_initial_state(_initial=True)
            state = self.get_state()
            Nx = s0.basis.Nxyz[0]
            for n in range(self.cells_x):
                if self.single_band:
                    state[Nx * n : Nx * (n + 1), ...] = s0
                else:
                    state[:, Nx * n : Nx * (n + 1), ...] = s0[:]
        else:
            state = super().get_initial_state()

        # Now imprint the vortex pairs
        x, y = state.xyz
        psi_vortex = np.prod(
            [
                (x - x0_ * u.micron + 1j * (y - y0_ * u.micron / 2.0))
                * (x - x0_ * u.micron - 1j * (y + y0_ * u.micron / 2.0))
                for (x0_, y0_) in self.vortices
            ],
            axis=0,
        )

        phase = np.exp(1j * np.angle(psi_vortex))
        if self.single_band:
            state *= phase
        else:
            state[0, ...] *= phase

        if self.vortex_cooling_t_ > 0:
            state.cooling_phase = 1j
            state.t = -self.vortex_cooling_t_ * self.t_unit
            state0 = utils.evolve_to(state, t=0.0)
        else:
            state0 = state
        state = self.get_state()
        state.set_psi(state0.get_psi())
        return state

    def plot(self, state):
        from matplotlib import pyplot as plt
        from mmfutils import plot as mmfplt
        from gpe.plot_utils import MPLGrid

        plt.clf()
        x, y = state.xyz
        x_, y_ = x / u.micron, y / u.micron

        if self.single_band:
            psi = state.get_psi()
            n = state.get_density()
            n_x = n.sum(axis=1)
            ns = [n]
            psis = [psi]
            nxs = [n_x]
        else:
            psis = psi_a, psi_b = state.get_psi()
            ns = n_a, n_b = state.get_density()
            n = n_a + n_b
            nxs = state.get_density_x()

        grid0 = MPLGrid(direction="right")
        for n, nx, psi in zip(ns, nxs, psis):
            grid1 = grid0.grid(direction="down", share=True, space=0)
            grid1.next(0.5)
            plt.plot(x_.ravel(), nx)
            grid1.next()
            mmfplt.imcontourf(x_, y_, n, aspect=1)
            grid1.next()
            mmfplt.imcontourf(x_, y_, mmfplt.colors.color_angle(np.angle(psi)), aspect=1)
        E = state.get_energy()
        N = state.get_N()
        plt.suptitle(f"t={state.t/self.t_unit:.2g}t0, E={E:.2f}, N={N:.2f}")


class ExperimentVortexPair3(ExperimentVortexPair):
    """Higher resolution."""

    cells_x = 100
    R = 4.5 * u.micron
    vortex_y = 3.0
    vortex_xs = [5, 15]

    def init(self):
        self.vortices = [(x0_, self.vortex_y) for x0_ in self.vortex_xs] + [
            (-x0_, -self.vortex_y) for x0_ in self.vortex_xs
        ]
        super().init()


class ExperimentVortexMovingFrame(Experiment):
    """A Vortex ring in a BEC with SOC in the moving frame.

    A vortex ring moving at a velcotiy has an equilibrium size
    in which the its own flow prepels it in one direction and
    the bouyant force from a tube propells it equall in the
    oposite direction.

    However, in a SOC sysyem moving toward or away from the raman lasers
    detunes their light breaking the gaelian covariance. The detuning
    changes the equlibrium radii.


    This experiemnt will find the equlibrium radii for a variety
    of initial flow velocities (+-) in the SOC system.
    """

    v_x = 0  # Moving Frame Velocity

    recoil_frequency_Hz = 3683.8
    mu_nK = 105.0
    detuning_kHz = 2.0
    detuning_E_R = None
    rabi_frequency_E_R = 1.5
    initial_imbalance = None
    trapping_frequencies_Hz = (3.49, 278, 278)
    basis_type = "axial"
    single_band = False

    # The experiment puts 4 vortices symmetric about the axes.  This
    # parameter specifies the location of one positive-orientation
    # vortex
    vortex_cooling_t_ = 0.2  # Time to cool vortices
    vortex_x, vortex_r = vortex = (0, 2.0)
    vortices = None  # Can explicitly override.

    def init(self):
        self.mu = self.mu_nK * u.nK

        if self.vortices is None:
            x0_, r0_ = self.vortex
            self.vortices = [(x0_, r0_)]

        super().init()
        if self.R is not None:
            Lr = 2 * self.R
            self.Lxyz = (self.Lx, Lr)
            self.Nxyz = [utils.get_good_N(_L / self.dx) for _L in self.Lxyz]

        if self.vortices is None:
            x0_, r0_ = self.vortex
            self.vortices = [(x0_, r0_)]

    def get_initial_state(self):
        """Return the initial state.

        Arguments
        ---------
        vortices : [(x, r)]
           List of vortex locations in microns.  These are positively
           oriented - use a negative r to imprint a pair of the
           opposite orientation.

        v_x: 0
            The velocity of the moving reference frame.
            If truly related to the laser detuning resonable
            papameters should be the far detuning parameter from
            experimtent.
        """

        state = super().get_initial_state()
        self._states = [state.copy()]

        # Imprint the vortex ring
        x, r = state.xyz
        # Here we aproximate the ring as a vortex anti vortex pair
        psi_vortex = np.prod(
            [
                (x - x0_ * u.micron + 1j * (r - r0_ * u.micron / 2.0))
                * (x - x0_ * u.micron - 1j * (r + r0_ * u.micron / 2.0))
                for (x0_, r0_) in self.vortices
            ],
            axis=0,
        )

        phase = np.exp(1j * np.angle(psi_vortex))
        # state *= phase
        state.set_psi(phase * state.get_psi())

        self._states.append(phase)
        self._states.append(state.copy())

        if self.vortex_cooling_t_ > 0:
            state.cooling_phase = 1j
            state.t = -self.vortex_cooling_t_ * self.t_unit
            state0 = utils.evolve_to(state, t=0.0)
        else:
            state0 = state
        state = self.get_state()
        state.set_psi(state0.get_psi())
        self._states.append(state.copy())

        return state

    def plot(self, state):
        from matplotlib import pyplot as plt
        from mmfutils import plot as mmfplt
        from gpe.plot_utils import MPLGrid

        plt.clf()
        x, r = state.xyz
        x_, r_ = x / u.micron, r / u.micron

        psis = psi_a, psi_b = state.get_psi()
        ns = n_a, n_b = state.get_density()
        n = n_a + n_b
        nxs = state.get_density_x()

        grid0 = MPLGrid(direction="right")
        for n, nx, psi in zip(ns, nxs, psis):
            grid1 = grid0.grid(direction="down", share=True, space=0)
            grid1.next(0.5)
            plt.plot(x_.ravel(), nx)
            grid1.next()
            mmfplt.imcontourf(x_, r_, n, aspect=1)
            grid1.next()
            mmfplt.imcontourf(x_, r_, mmfplt.colors.color_angle(np.angle(psi)), aspect=1)
        E = state.get_energy()
        N = state.get_N()
        plt.suptitle(f"t={state.t/self.t_unit:.2g}t0, E={E:.2f}, N={N:.2f}")
