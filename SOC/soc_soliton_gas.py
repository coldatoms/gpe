import numpy as np

from gpe import utils
from gpe.soc import Experiment, Bloch, State2, State2Tube, u


class ExperimentLinearResponse(Experiment):
    """Here we look at the linear response of the homogeneous."""

    t_unit = u.ms

    cells_x = 20
    dxyz = (0.05 * u.micron,)

    # We specify the initial state in terms of a vector on the Bloch sphere.
    # The length of this vector is the density, and the direction gives the
    # phase imprint.
    initial_state = (0.0, 0.0, -1.0)

    detuning_kHz = 1.0
    final_detuning_kHz = -1.0
    B_gradient_mG_cm = 0
    rabi_frequency_E_R = 1.9  # Rabi frequency 1.0 - 2.9

    trapping_frequencies_Hz = (0.0 * 3.07, 278.08, 278.04)

    t__detuning_ramp = 10
    t__SOC_on = 10  # Time to ramp on SOC fields (10-100ms)
    t__detuning_ramp = 100  # Time to ramp detuning from negative to positive
    t__wait = 0.0  # Time to wait before imaging (up to 500ms)
    t__image = 7  # Time for expansion

    def init(self):
        self.t__final = self.t__wait
        Experiment3.init(self)
        self.state_args["trap_type"] = "cos"

    def get_state(self):
        if self.State is None:
            if self.tube:
                State = State2Tube
            else:
                State = State2
        else:
            State = self.State

        state = State(experiment=self, **self.state_args)

        state[:, ...] = Bloch.get_psi(a=self.initial_state)[state.bcast]
        return state

    def get_initial_state(self):
        return self.get_state()

    @property
    def t__final(self):
        return self.t__SOC_on + self.t__detuning_ramp + self.t__wait

    def init(self):
        self.final_detuning = self.final_detuning_kHz * u.kHz * (2 * np.pi * u.hbar)

        Experiment.init(self)

        self._Omega = utils.get_smooth_transition(
            [0.0, self.Omega, 0.0],
            durations=[0, self.t__detuning_ramp + self.t__wait],
            transitions=[self.t__SOC_on, 0.1],
        )

        self._delta = utils.get_smooth_transition(
            [self.detuning, self.final_detuning, 0.0],
            durations=[self.t__SOC_on, self.t__wait],
            transitions=[self.t__detuning_ramp, 0.1],
        )

    def Omega_t_(self, t_):
        return self._Omega(t_)

    def delta_t_(self, t_):
        return self._delta(t_)


class ExperimentSolitonGas(ExperimentLinearResponse):
    cells_x = 100
    healing_length_micron = 2.0
    detuning_kHz = None
    detuning_E_R = 0.1

    rabi_frequency_E_R = 2.9
    random_seed = 1
    edge_width = 0.2
    solitons = 10  # Must be even

    t__wait = 10.0  # Time to wait before imaging (up to 500ms)
    t__image = 7  # Time for expansion

    T__x = np.inf  # Trapping period in x direction

    def init(self):
        T_x = self.T__x * self.t_unit
        self.trapping_frequencies_Hz = (1.0 / T_x / u.Hz, 278.08, 278.04)
        ExperimentLinearResponse.init(self)

    def Omega_t_(self, t_):
        return self.Omega

    def delta_t_(self, t_):
        return self.delta

    def get_initial_state(self):
        state = self.get_state()
        x = state.xyz[0]
        Lx = state.Lxyz[0]
        g = state.gs.mean()
        m = state.ms.mean()

        # Set density based on healing length
        n0 = state.hbar**2 / (2 * m * self.healing_length_micron * u.micron) / g

        # Random soliton locations
        np.random.seed(self.random_seed)
        assert self.solitons % 2 == 0
        x_soliton = np.array(sorted(np.random.random(self.solitons) * Lx + x[0]))
        pulse = 0
        for n_, x_ in enumerate(x_soliton):
            pulse += (-1) ** n_ * np.vectorize(utils.step)(
                x - x_ - self.edge_width / 2.0, self.edge_width
            )

        # Basis of one phase in the middle of the other
        k = 2.0 * (pulse - 0.5) * state.k_r
        state[0] = np.sqrt(n0) * pulse * np.exp(1j * (state.k_r + k) * x)
        state[1] = np.sqrt(n0) * (1 - pulse) * np.exp(1j * (-state.k_r + k) * x)
        return state

    def get_initialized_state(self, state):
        """Return a valid state initialized from `state` for expansion."""
        state_ = self.get_state()
        state_[...] = state
        return state_
