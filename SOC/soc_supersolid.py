"""Demonstration of supersolid phase with SOC."""

import numpy as np
import scipy.interpolate

sp = scipy

from matplotlib import pyplot as plt

from gpe import utils
from gpe.soc import Experiment, u

__all__ = ["Supersolid"]


class Supersolid(Experiment):
    """Supersolid Explorer.

    This version uses experimental parameters.

    Here we explore the formation of a supersolid in a BEC with SOC.
    The parameters in the class follow the experimental parameters
    explored in the Engels Group.

    Attributes
    ----------
    barrier_height : float
       Barrier height.
    barrier_k : float
       Specifies the width of the barrier (1/k) or the wavevector of
       the cosine modulation.
    barrier_type : 'gaussian', 'cos'
       Type of barrier.

    drive_type : 'gaussian', 'cos'
       Continuous drive or pulse.
    drive_w : float
       Frequency of drive or width of pulse.
    drive_t0_w : float
       When to start the gaussian drive (the initial signal will be
       less than exp(-(drive_t0_w*w)**2/2))

    """

    t_unit = u.ms

    # Experimental Parameters

    # background potential
    lattice_k_k_r = 0.6
    lattice_height_V_TF = 0 * 0.001  # Depth of barrier in units of mu

    barrier_type = "gaussian"  # Type of barrier
    barrier_x = 1  # x location of barrier
    barrier_k_k_r = 0  # Barrier lattice momentum
    barrier_height_V_TF = 10.0  # Depth of barrier in units of mu

    drive_type = "gaussian"  # Type of driving term: 'static', 'gaussian', 'cos'
    drive_w_E_R = 2.0  # Drive width/frequency in units of E_R/hbar
    drive_t0_w = 7.0  # 7 implies 1e-11 suppression.

    B_gradient_mG_cm = 0  # No counterflow

    # Dimensionless parameters specifying dispersion.
    w = 0.8
    d = 0

    # These are used to set k_r, the mass, and the density.
    l_r_micron = 1.0 / 7.9  # Length associated with k_r
    T__R = 0.271  # Period associated with E_R
    xi_micron = 0.026  # Healing length

    cells_x = 10
    dx = 0.05 * u.micron

    T__x = 1.0  # Strong trapping.
    # T__x = 10.0               # Weak trapping.
    # T__x = np.inf             # No trapping.
    T__perp = 3.6
    trapping_frequencies_Hz = None

    t__image = 10
    t__final = 10

    # Start with 1D
    basis_type = "1D"
    tube = False

    initial_imbalance = None  # Start in true ground state.

    def init(self):
        # Compute k_r, E_R, etc.
        hbar = u.hbar
        l_r = self.l_r_micron * u.micron
        k_r = 1.0 / l_r
        T_R = self.T__R * self.t_unit
        recoil_frequency = 1.0 / T_R

        E_R = 2 * np.pi * recoil_frequency * hbar
        m = 1.0 / (2 * E_R / (hbar * k_r) ** 2)
        Ts = np.array([self.T__x, self.T__perp, self.T__perp]) * self.t_unit

        assert self.trapping_frequencies_Hz is None
        self.trapping_frequencies_Hz = 1.0 / Ts / u.Hz

        self.recoil_frequency_Hz = recoil_frequency / u.Hz

        self.rabi_frequency_E_R = 4 * self.w
        self.rabi_frequency_kHz = None

        self.detuning_E_R = 4 * self.d
        self.detuning_kHz = None

        self.ms = (m, m)

        self.drive_w = self.drive_w_E_R * E_R / hbar

        # Set mu to give specified xi.  To do this, we need to estimate the
        # minimum of the dispersion relationship so we first need to create a
        # state, then estimate E0 using the discrete ks.
        self.x_TF = None
        self.V_TF = 0
        self.barrier_height = 0
        self.lattice_height = 0

        Experiment.init(self)
        self.barrier_k = self.barrier_k_k_r * self.k_r
        self.lattice_k = self.lattice_k_k_r * self.k_r
        state = self.get_state()

        ks_ = state.basis.kx / k_r
        Em_ = (ks_**2 + 1.0) / 2 - np.sqrt((ks_ - self.d) ** 2 + self.w**2)
        ind = np.argmin(Em_)
        # k0 = ks_[ind] * k_r
        # E0 = Em_[ind] * 2*E_R

        xi = self.xi_micron * u.micron
        self.V_TF = (hbar / xi) ** 2 / 2 / m

        self.barrier_height = self.barrier_height_V_TF * self.V_TF
        self.lattice_height = self.lattice_height_V_TF * self.V_TF
        Experiment.init(self)

    def _barrier_potential(self, x_barrier_k):
        """Return the barrier potential without the power factor.

        x_barrier_k : x*barrier_k
        """
        if self.barrier_type == "gaussian":
            # Gaussian barrier
            V_barrier = np.exp(-((x_barrier_k) ** 2) / 2.0)
        else:
            # Cosine barrier
            V_barrier = np.cos(x_barrier_k)
        return V_barrier, V_barrier

    def _lattice_potential(self, x_lattice_k):
        """Return the lattie potential without the power factor.

        x_lattice_k : x*lattice_k
        """
        V_lattice = np.cos(x_lattice_k)

        return V_lattice, V_lattice

    def barrier_height_t_(self, t_):
        tw = t_ * self.t_unit * self.drive_w
        if self.drive_type == "gaussian":
            height = np.exp(-((tw - self.drive_t0_w) ** 2) / 2.0)
        elif self.drive_type == "sin":
            height = np.sin(tw)
        else:
            height = 1.0
        return self.barrier_height * height

    def get_Vt(self, state):
        t_ = state.t / self.t_unit
        barrier_height = self.get("barrier_height", t_=t_)
        barrier_k = self.get("barrier_k", t_=t_)

        lattice_height = self.get("lattice_height", t_=t_)
        lattice_k = self.get("lattice_k", t_=t_)

        x = state.xyz_trap[0]

        if False:
            # Make barrier potential periodic in box
            x0 = self.get("barrier_x", t_=t_)
            Lx = self._get_Lx(state)
            x = (x - x0 - x.min()) % Lx + x.min()

        Va, Vb = self._barrier_potential(x_barrier_k=(x - self.barrier_x) * barrier_k)
        Va_, Vb_ = self._lattice_potential(x_lattice_k=x * lattice_k)
        Va = barrier_height * Va + lattice_height * Va_
        Vb = barrier_height * Vb + lattice_height * Vb_
        return Va, Vb

    def get_state(self, expt=False, initialize=True):
        """Quickly return an appropriate initial state."""
        state = super().get_state(expt=expt, initialize=initialize)
        na, nb = state.get_density()
        n = na + nb
        state[0, ...] = n / 2.0
        state[1, ...] = n / 2.0
        return state

    def plot(self, state, **kw):
        state.plot(**kw)

        E = state.get_energy()
        Na, Nb = state.get_Ns()
        N = Na + Nb
        na, nb = state.get_density()
        nbar = (na + nb).mean()
        gaa, gbb, gab = self.gs
        E_R = self.E_R
        w = self.Omega / 4 / E_R
        d = self.detuning / 4 / E_R
        es = (
            np.array(
                [(gaa + gbb + 2 * gab) / 8, (gaa + gbb - 2 * gab) / 8, (gbb - gaa) / 3]
            )
            * nbar
            / 4
            / E_R
        )
        e1, e2, e3 = es
        t, t0 = state.t, state.t0
        wc = np.sqrt(1 / (1 + e1 / e2))
        plt.suptitle(
            f"t={t/t0}t0, "
            # + f"Ns={Na:.4f}+{Nb:.4f}={N:.4f}, E={E:.4f}, "
            + f"es = [{es[1]:.2f}, {es[1]:.2f}, {es[2]:.2f}], "
            + f"w={w:.4f}, wc={wc:.4f}, d={d:.4f}"
        )
        return locals()
