import numpy as np

from gpe import soc
from gpe import utils
from gpe.soc import u, Experiment, ExperimentStub, State2, State2Tube

__all__ = [
    "Experiment",
    "Experiment1",
    "Experiment1a",
    "Experiment2",
    "Experiment3",
    "Experiment4",
    "Experiment5",
    "Experiment1cf",
    "Experiment3cf",
    "ExperimentStub",
]


class Experiment1(soc.Experiment):
    """
    LoadingZeroDetuning

    * No counterflow (gradient)
    * Roughly zero detuning.
    * Start with 100% in (1, -1), get ground state, then transfer 50% to (1,0).
    * Ramp on SOC/Raman couplings over 100ms (10ms should work too).
    * `rabi_frequency_E_R=1.9`
    * Wait for up to 200ms
    * Image after expansion for 7ms with everything off.
    """

    t_unit = u.ms
    detuning_kHz = 0.010  # Up to 50Hz or 0.015 E_R.  Try 200Hz
    B_gradient_mG_cm = 0  # Might have some gradient
    rabi_frequency_E_R = 1.9  # Rabi frequency 1.0 - 2.9

    initial_imbalance = 0.5  # Fractional populations in initial state.

    t__SOC_on = 10  # Time to ramp on SOC fields
    t__wait = 100  # Time to wait before imaging
    t__image = 7  # Time for expansion

    @property
    def t__final(self):
        return self.t__SOC_on + self.t__wait

    def init(self):
        soc.Experiment.init(self)

        self._Omega = utils.get_smooth_transition(
            [0.0, self.Omega, 0.0],
            durations=[0, self.t__wait],
            transitions=[self.t__SOC_on, 0.1],
        )

    def Omega_t_(self, t_):
        return self._Omega(t_)


class Experiment1a(Experiment1):
    """Same as Experiment1, but with larger detuning."""

    detuning_kHz = 0.200


class Experiment2(Experiment1):
    """Same as Experiment1 but with larger detuning."""

    detuning_E_R = 0.25
    detuning_kHz = None


class Experiment3(soc.Experiment):
    """Start in (1, -1) state (no transfer).
    detuning = 1kHz
    Ramp on SOC over 100 ms
    Ramp detuning from 1kHz to -1kHz over 100ms
    """

    t_unit = u.ms
    detuning_kHz = 1.0
    final_detuning_kHz = -1.0
    B_gradient_mG_cm = 0
    rabi_frequency_E_R = 1.9  # Rabi frequency 1.0 - 2.9

    initial_imbalance = 0.0  # Nothing RF transfered

    t__SOC_on = 10  # Time to ramp on SOC fields (10-100ms)
    t__detuning_ramp = 100  # Time to ramp detuning from negative to positive
    t__wait = 0.0  # Time to wait before imaging (up to 500ms)
    t__image = 7  # Time for expansion

    @property
    def t__final(self):
        return self.t__SOC_on + self.t__detuning_ramp + self.t__wait

    def init(self):
        self.final_detuning = self.final_detuning_kHz * u.kHz * (2 * np.pi * u.hbar)

        soc.Experiment.init(self)

        self._Omega = utils.get_smooth_transition(
            [0.0, self.Omega, 0.0],
            durations=[0, self.t__detuning_ramp + self.t__wait],
            transitions=[self.t__SOC_on, 0.1],
        )

        self._delta = utils.get_smooth_transition(
            [self.detuning, self.final_detuning, 0.0],
            durations=[self.t__SOC_on, self.t__wait],
            transitions=[self.t__detuning_ramp, 0.1],
        )

    def Omega_t_(self, t_):
        return self._Omega(t_)

    def delta_t_(self, t_):
        return self._delta(t_)


class Experiment4(Experiment3):
    """Same as Experiment3 but different final detuning and ramp time."""

    final_detuning_kHz = -4.0
    t__detuning_ramp = 50  # Time to ramp detuning from negative to positive


class ExperimentRamp(Experiment3):
    """Start in ground state with
    20kHz detuned

    Omega_E_R  up to 2.5
    Ramp to -1kHz over 10 - 500ms  (When is it adiabatic in the left well).
    """

    final_detuning_kHz = -4.0
    t__detuning_ramp = 50  # Time to ramp detuning from negative to positive


class Experiment5(soc.Experiment):
    """Start in (1, -1) state (no transfer).
    detuning = 1kHz
    Ramp on SOC over 100 ms
    Ramp detuning from 1kHz to -1kHz over 100ms
    """

    t_unit = u.ms
    detuning_kHz = 2.0
    detuning_E_R = None
    B_gradient_mG_cm = 10
    rabi_frequency_E_R = 1.9  # Rabi frequency 1.0 - 2.9

    initial_imbalance = 0.0  # Nothing RF transfered

    t__SOC_on = 10  # Time to ramp on SOC fields (10-100ms)
    t__counterflow_on = 1  # Time to ramp on counterflow
    t__counterflow = 10  # Time to hold counterflow
    t__counterflow_off = 1  # Time to ramp off counterflow
    t__wait = 0.0  # Time to wait before imaging (up to 500ms)
    t__image = 7  # Time for expansion

    @property
    def t__final(self):
        return (
            self.t__SOC_on
            + self.t__counterflow_on
            + self.t__counterflow
            + self.t__counterflow_off
            + self.t__wait
        )

    def init(self):
        soc.Experiment.init(self)

        self._Omega = utils.get_smooth_transition(
            [0.0, self.Omega, 0.0],
            durations=[
                0,
                self.t__counterflow_on
                + self.t__counterflow
                + self.t__counterflow_off
                + self.t__wait,
            ],
            transitions=[self.t__SOC_on, 0.1],
        )

        self._B_gradient = utils.get_smooth_transition(
            [0.0, self.B_gradient, 0.0],
            durations=[self.t__SOC_on, self.t__counterflow, self.t__wait],
            transitions=[self.t__counterflow_on, self.t__counterflow_off],
        )

    def Omega_t_(self, t_):
        return self._Omega(t_)

    def B_gradient_t_(self, t_):
        return self._B_gradient(t_)


class Experiment6(soc.Experiment):
    """Start in (1, -1) state (no transfer).

    Goal is to adiabatically load into "(1, -1)" state close to zero detuning.
    What is the role of counterflow.

    Can do RF pulse after ramp and imaging.

    Trap frequencies (don't know) less
    R_TF less

    Jump on Raman (0.01ms)

    detuning = 1kHz
    Ramp on SOC over 100 ms
    Ramp detuning from 1kHz to -1kHz over 100ms

    Goal here is to see what happens with gradient or without.

    What would counterflow do to adiabatic ramp on.
    """

    t_unit = u.ms
    detuning_kHz = 21.0
    final_detuning_kHz = 1.0

    # Somewhere between these
    # trapping_frequencies_Hz = (3, 273, 277)          # 0.04
    # trapping_frequencies_Hz = (2.4, 229, 222)        # 0.031
    trapping_frequencies_Hz = (2.81, 255, 257)  # 0.035

    # N drops less than factor of 2.
    Rx_TF = 200 * u.micron

    B_gradient_mG_cm = 0

    rabi_frequency_E_R = 2.35

    initial_imbalance = 0.0  # Nothing RF transfered

    t__SOC_on = 0  # Time to ramp on SOC fields (10-100ms)
    t__detuning_ramp = 100  # Time to ramp detuning from negative to positive
    t__wait = 0.0  # Time to wait before imaging (up to 500ms)
    t__image = 7  # Time for expansion

    @property
    def t__final(self):
        return (
            self.t__SOC_on
            + self.t__detuningcounterflow_on
            + self.t__counterflow
            + self.t__counterflow_off
            + self.t__wait
        )

    def init(self):
        soc.Experiment.init(self)

        self._Omega = utils.get_smooth_transition(
            [0.0, self.Omega, 0.0],
            durations=[
                0,
                self.t__counterflow_on
                + self.t__counterflow
                + self.t__counterflow_off
                + self.t__wait,
            ],
            transitions=[self.t__SOC_on, 0.1],
        )

    def Omega_t_(self, t_):
        return self._Omega(t_)

    def B_gradient_t_(self, t_):
        return self.B_gradient


class Experiment1cf(soc.Experiment):
    """
    * Start with 100% in (1, -1), get ground state, then transfer 50% to (1,0).
    * No SOC or Raman couplings.
    * Apply gradient (10ms ramp) and hold from 0 to 400ms, then jump off
      (instantaneous) and wait 200ms.
    * Image ater expansion for 7ms with everything off.
    """

    t_unit = u.ms

    initial_imbalance = 0.5  # Fractional populations in initial state.
    rabi_frequency_E_R = 0.0

    t__gradient_on = 10  # Time to ramp on gradient
    t__gradient = 100  # Time gradient is on (0 - 400)
    t__gradient_off = 0.1  # Time to ramp off gradient
    t__wait = 100  # Time to wait before imaging
    t__image = 7  # Time for expansion

    @property
    def t__final(self):
        return (
            self.t__gradient_on + self.t__gradient + self.t__gradient_off + self.t__wait
        )

    def init(self):
        soc.Experiment.init(self)

        self._B_gradient = utils.get_smooth_transition(
            [0.0, self.B_gradient, 0.0],
            durations=[0.0, self.t__gradient],
            transitions=[self.t__gradient_on, self.t__gradient_off],
        )

    def B_gradient_t_(self, t_):
        return self._B_gradient(t_)


class Experiment3cf(soc.Experiment):
    """
    * Start with 100% in (1, -1), get ground state, then transfer 50% to (1,0),
      but in the presence of the gradient.
    * Wait for 1s.
    * Ramp on Raman coupling (fixed detuning) over 10ms, 100ms, 200ms.  Final
      amplitude 1kHz to 10.7kHz (initial calculations at 5kHz).
    * Watch over 100ms to 200ms.
    * Image after expansion for 7ms with everything off.
    """

    t_unit = u.ms

    initial_imbalance = 0.5  # Fractional populations in initial state.

    rabi_frequency_kHz = 5  # Maximum Rabi frequency 1 to 10.7kHz

    t__initial_wait = 1000  # Initial wait
    t__rabi_on = 10  # Time to ramp on Raman coupling (10, 100, 200)
    t__wait = 100  # Time to wait before imaging (100-200ms)
    t__image = 7  # Time for expansion

    @property
    def t__final(self):
        return self.t__initial_wait + self.t__rabi_on + self.t__wait

    def init(self):
        soc.Experiment.init(self)

        self._Omega = utils.get_smooth_transition(
            [0.0, self.Omega, 0.0],
            durations=[self.t__initial_wait, self.t__wait],
            transitions=[self.t__rabi_on, 0.1],
        )

    def B_gradient_t_(self, t_):
        return self.B_gradient

    def Omega_t_(self, t_):
        return self._Omega(t_)


class ExperimentSoliton(soc.Experiment):
    """Experiment for exploring solitons.
    * No counterflow (gradient) or detuning
    """

    t_unit = u.ms

    initial_imbalance = 0.5  # Fractional populations in initial state.

    t__wait = 0  # Time to wait before imaging
    t__image = 7  # Time for expansion
    detuning_E_R = 0.0
    detuning_kHz = None
    B_gradient_mG_cm = 0.0
    rabi_frequency_E_R = 1.9  # Rabi frequency 1.0 - 2.9
    dxyz = (0.1 * u.micron,)
    cells_x = 100

    def init(self):
        self.t__final = self.t__wait
        soc.Experiment.init(self)
        self.state_args["trap_type"] = "cos"
