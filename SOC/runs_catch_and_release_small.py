"""Defines simulations to run with checkpoint/restart capabilities.

To use this, inherit from ExperimentBase, and pass any important variables to
the `local_dict` argument of the ExperimentBase.__init__() method.  These
variables will be used to generate unique filenames for each run.
"""
import itertools

import numpy as np

import mmf_setup.set_path.hgroot

from mmfutils.contexts import NoInterrupt

import mmfutils.performance.threads
import mmfutils.performance.fft

import sys

from gpe import utils

import soc_catch_and_release

u = soc_catch_and_release.u

_DATA_DIR = "_data"


def set_threads(numexpr=1, fft=1, *v):
    if v:
        numexpr = fft = v[0]
    mmfutils.performance.threads.set_num_threads(numexpr)
    mmfutils.performance.fft.set_num_threads(fft)


# Needs testing, but performance on swan did not show an appreciable benefit to
# using multiple cores for 1D simulations
set_threads(4)


class RunBase:
    dt_ = 1.0

    experiment_params = dict(
        cooling_phase=[1.0, 1 + 0.005j, 1 + 0.01j],
    )

    simulation_params = {}

    @property
    def experiments(self):
        params = self.experiment_params
        keys = params.keys()
        iterators = [params[_key] for _key in keys]
        return [
            self.Experiment(**dict(zip(keys, values)))
            for values in itertools.product(*iterators)
        ]

    @property
    def simulations(self):
        params = self.simulation_params
        keys = params.keys()
        iterators = [params[_key] for _key in keys]
        return [
            utils.Simulation(
                experiment=expt,
                dt_=self.dt_,
                data_dir=_DATA_DIR,
                **dict(zip(keys, values))
            )
            for expt in self.experiments
            for values in itertools.product(*iterators)
        ]

    def run(self):
        with NoInterrupt(ignore=True) as interrupted:
            for sim in self.simulations:
                if interrupted:
                    break
                sim.run()
                if interrupted:
                    break
                sim.run_images()


class RunImageDemo(RunBase):
    experiment_params = dict(
        cooling_phase=[1.0],  # , 1+0.001j, 1+0.005j],  # 1+0.01j, 1+0.015j],
        basis_type=["1D"],
        tube=[True],
        barrier_depth_nK=[-50.0],
        t__barrier=[0],
        cells_x=[400],
    )
    simulation_params = dict(
        image_ts_=[np.array([0, 10])],
        t__final=[10.0],
    )
    Experiment = soc_catch_and_release.ExperimentCatchAndReleaseSmall


class RunAxial50(RunBase):
    experiment_params = dict(
        cooling_phase=[1.0],  # , 1+0.001j, 1+0.005j],  # 1+0.01j, 1+0.015j],
        basis_type=["axial"],
        barrier_depth_nK=[-50.0],
        t__barrier=[0],
        cells_x=[400],
    )
    simulation_params = dict(
        image_ts_=[np.array([0, 1, 2, 3])],
        t__final=[3.0],
    )
    Experiment = soc_catch_and_release.ExperimentCatchAndReleaseSmall


class RunAxial100(RunBase):
    experiment_params = dict(
        cooling_phase=[1.0],  # , 1+0.001j, 1+0.005j],  # 1+0.01j, 1+0.015j],
        basis_type=["axial"],
        barrier_depth_nK=[-100.0],
        t__barrier=[0],
        cells_x=[400],
    )
    simulation_params = dict(
        image_ts_=[np.array([0, 1, 2, 3])],
        t__final=[3.0],
    )
    Experiment = soc_catch_and_release.ExperimentCatchAndReleaseSmall


class RunAxial200(RunBase):
    experiment_params = dict(
        cooling_phase=[1.0],  # , 1+0.001j, 1+0.005j],  # 1+0.01j, 1+0.015j],
        basis_type=["axial"],
        barrier_depth_nK=[-200.0],
        t__barrier=[0],
        cells_x=[400],
    )
    simulation_params = dict(
        image_ts_=[np.array([0, 1, 2, 3])],
        t__final=[3.0],
    )
    Experiment = soc_catch_and_release.ExperimentCatchAndReleaseSmall


class RunAxial400(RunBase):
    experiment_params = dict(
        cooling_phase=[1.0],  # , 1+0.001j, 1+0.005j],  # 1+0.01j, 1+0.015j],
        basis_type=["axial"],
        barrier_depth_nK=[-400.0],
        t__barrier=[0],
        cells_x=[400],
    )
    simulation_params = dict(
        image_ts_=[np.array([0, 1, 2, 3])],
        t__final=[3.0],
    )
    Experiment = soc_catch_and_release.ExperimentCatchAndReleaseSmall


if __name__ == "__main__":
    cmd = sys.argv[1]
    if cmd in locals():
        locals()[cmd]().run()
    else:
        raise ValueError("Command {} not found.".format(cmd))
