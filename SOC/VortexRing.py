# ---
# jupyter:
#   jupytext:
#     formats: ipynb,py
#     text_representation:
#       extension: .py
#       format_name: light
#       format_version: '1.5'
#       jupytext_version: 1.4.0
#   kernelspec:
#     display_name: Python [conda env:_gpe3]
#     language: python
#     name: conda-env-_gpe3-py
# ---

# + init_cell=true
import mmf_setup;mmf_setup.nbinit()
from gpe.imports import *
# -

# # Implement

# + init_cell=true
import gpe.soc;reload(gpe.soc)
import soc_vortex;reload(soc_vortex)
from SOC.soc_catch_and_release import u

args = dict(vortex=(0, 2.),
            Lx=60,
            R=5,
            Nr=2**5,
            Nx=2**7,
            )

e0 = soc_vortex.ExperimentVortexMovingFrame(v_x=0*u.ms/u.s, **args)
#e0 = soc_vortex.ExperimentVortexMovingFrame(v_x=np.pi*u.ms/u.s, **args)
#e0 = soc_vortex.ExperimentVortexMovingFrame(twist=0, **args)
#e1 = soc_vortex.ExperimentVortexMovingFrame(twist=np.pi, **args)

#s = e.get_initial_state()
s0 = e0.get_state()
#s1 = e1.get_state()
#print(s0.twist, s1.twist)

# + init_cell=true
s = e0.get_initial_state()

# +
x, r = [_x.ravel() for _x in s.xyz]

psi, phase, psi_v, psi_cool = e0._states

psi = psi_v
#psi = psi_v*(-1j)

plt.subplot(311)
imcontourf(x, r, psi.data[0].imag)
plt.subplot(312)
imcontourf(x, r, psi.get_psi()[0].imag)

np.allclose(psi.data, psi.get_psi())

# -

plt.plot(r, np.angle(psi.data[0,-2,:]))
plt.plot(r, np.angle(psi.data[0,-1,:]))
plt.plot(r, np.angle(psi.data[0,0,:]))
plt.plot(r, np.angle(psi.data[0,1,:]))
#plt.plot(r, np.angle(psi.get_psi()[0,-1,:]))
#plt.plot(r, np.angle(psi.get_psi()[0,0,:]))
print(np.angle(psi.data[0,-2,16]),
      np.angle(psi.data[0,-1,16]),
      np.angle(psi.data[0,0,16]),
      np.angle(psi.data[0,1,16]),
      np.angle(psi.get_psi()[0,-1,16]),
      np.angle(psi.get_psi()[0,0,16]))

plt.plot(x, np.angle(phase[:,0]/phase[-1,0]))
plt.axhline(e0.twist)

# +
plt.plot(r, psi.data[0,-1,:].imag)
plt.plot(r, psi.data[0,0,:].imag)
#plt.plot(r, psi.get_psi()[0,-1,:].imag)
#plt.plot(r, psi.get_psi()[0,0,:].imag)




# +
plt.plot(r, psi.data[0,-1,:].imag - psi.data[0,0,:].imag)

plt.plot(r, psi.get_psi()[0,-1,:].imag - psi.get_psi()[0,0,:].imag)

# -

e0.twist





import pdb
pdb.run('psi.get_psi()')



# +


x, r = [_x.ravel() for _x in s.xyz]
r = np.concatenate((-r[::-1],r))

ns = na, nb = s.get_density()
radial_ns =  np.concatenate((ns[:,:,::-1], ns), axis=2)
#psi = psia, psib = s.get_psi()
psi = psia, psib = e0._state.data
radial_psi =  np.concatenate((psi[:,:,::-1], psi), axis=2)

plt.figure(figsize=(5,10))
plt.subplot(411)
imcontourf(x, r, radial_ns.sum(axis=0), aspect=1)
plt.subplot(412)
imcontourf(x, r, radial_psi.sum(axis=0).imag, aspect=1)
plt.colorbar()
plt.subplot(413)
imcontourf(x, r, radial_psi.sum(axis=0).real, aspect=1)
plt.colorbar()
plt.subplot(414)
imcontourf(x, r, np.angle(radial_psi.sum(axis=0)), aspect=1)
plt.colorbar()
#phase.imag[-1].mean(), phase.imag[0].mean()

# +

plt.figure(figsize=(5,10))
plt.subplot(411)
imcontourf(x, r, radial_ns[0], aspect=1)
plt.subplot(412)
imcontourf(x, r, radial_psi[0].imag, aspect=1)
plt.colorbar()
plt.subplot(413)
imcontourf(x, r, radial_psi[0].real, aspect=1)
plt.colorbar()
plt.subplot(414)
imcontourf(x, r, np.angle(radial_psi[0]), aspect=1)
plt.colorbar()
#phase.imag[-1].mean(), phase.imag[0].mean()
# -



# +

x, r = [_x.ravel() for _x in s.xyz]
r = np.concatenate((-r[::-1],r))

ns = na, nb = s.get_density()
radial_ns =  np.concatenate((ns[:,:,::-1], ns), axis=2)
psi = psia, psib = s.get_psi()
radial_psi =  np.concatenate((psi[:,:,::-1], psi), axis=2)

plt.figure(figsize=(5,10))
plt.subplot(411)
imcontourf(x, r, radial_ns.sum(axis=0), aspect=1)
plt.subplot(412)
imcontourf(x, r, radial_psi.sum(axis=0).imag, aspect=1)
plt.colorbar()
plt.subplot(413)
imcontourf(x, r, radial_psi.sum(axis=0).real, aspect=1)
plt.colorbar()
plt.subplot(414)
imcontourf(x, r, np.angle(radial_psi.sum(axis=0)), aspect=1)
plt.colorbar()
#phase.imag[-1].mean(), phase.imag[0].mean()

# +

plt.figure(figsize=(5,10))
plt.subplot(411)
imcontourf(x, r, radial_ns[0], aspect=1)
plt.subplot(412)
imcontourf(x, r, radial_psi[0].imag, aspect=1)
plt.colorbar()
plt.subplot(413)
imcontourf(x, r, radial_psi[0].real, aspect=1)
plt.colorbar()
plt.subplot(414)
imcontourf(x, r, np.angle(radial_psi[0]), aspect=1)
plt.colorbar()
#phase.imag[-1].mean(), phase.imag[0].mean()
# -



# # Plotting

# ## Vortex_r = 2, x_x=0

# +
# %pylab inline --no-import-all
from gpe.soc import ExperimentBarrier, u
import SOC.soc_vortex
from SOC.soc_vortex import ExperimentVortexPair3

#Sim data
import runs_vortex
r0 = runs_vortex.RunVortexMF_LowRes()


v_x = 0                  #  -3.0, -2.5, -2.0, -1.5, -1.0, -0.5, 0.0, 0.5, 1.0, 1.5, 2.0, 2.5, 3.0
cooling_phase = 1.0      #  1.0, 1+0.01j
vortex_r = 2.            #  -3.5, -3.0, -2.5, -2.0, -1.5, -1.0, -0.5, 0.0, 0.5, 1.0, 1.5, 2.0, 2.5, 3.0, 3.5
        


sims = [_sims for _sims in r0.simulations
       if _sims.experiment.vortex_r == vortex_r
       and _sims.experiment.cooling_phase == cooling_phase
       and _sims.experiment.v_x == v_x
       ]

params = [_sim.experiment.items() for _sim in sims]
print(params)


# +
N0 = []
for _sim in sims:
    s = _sim.get_state(t_=0, image=False);
    N0.append(s.get_N())
    
N0 = np.asarray(N0)

# +
ts = np.linspace(0,8,81)

Nmax = []
for _sim in sims:
    for _t in ts:
        time = np.floor(_t*100)/100
        s = _sim.get_state(t_=time, image=False);
        Nmax.append(s.get_density().sum(axis=0).max())

Nmax = np.asarray(Nmax)
N_max = Nmax.max()
# -







# +
t = 0
s = sims[-1].get_state(t_=t, image=False);
x, r = [_x.ravel() for _x in s.xyz]
r = np.concatenate((-r[::-1],r))
ns = na, nb = s.get_density()/N0[-1]
radial_ns = na_r, nb_r = np.concatenate((ns[:,:,::-1], ns), axis=2)
sigma_z = (na_r - nb_r) / (na_r + nb_r)

imcontourf(x, r, radial_ns.sum(axis=0), aspect=1)
    
# -



# +
Rx = 20
x, r = np.meshgrid(np.linspace(-Rx,Rx,100), 
                   np.linspace(-5,5,100),
                   indexing='ij',
                   sparse=True)
r0=2
z1 = x + 1j*(r-r0)
z2 = x - 1j*(r+r0)

a = 0

phase = np.exp(1j*np.angle(z1*z2)+a)

#imcontourf(x, r, phase*radial_ns.sum(axis=0), aspect=1)
plt.subplot(311)
imcontourf(x, r, phase.imag, aspect=1)
plt.colorbar()
plt.subplot(312)
imcontourf(x, r, phase.real, aspect=1)
plt.colorbar()
plt.subplot(313)
imcontourf(x, r, np.angle(phase), aspect=1)
plt.colorbar()
phase.imag[-1].mean(), phase.imag[0].mean()
# -

plt.plot(r.ravel(), np.angle(phase)[0,:])
plt.plot(r.ravel(), np.angle(phase)[-1,:])

# $$
# \phi(x,r) = e^{\I}
# $$
#
#
#
# $$ 
# x = \pm \frac{L}{2} \\
# r = 0 \\
# $$



# +
Rx = 20
x, r = np.meshgrid(np.linspace(-Rx,Rx,100), 
                   np.linspace(-5,5,100),
                   indexing='ij',
                   sparse=True)
r0=2
z1 = x + 1j*(r-r0)
z2 = x - 1j*(r+r0)
phase = np.exp(1j*np.angle(z1*z2))

np.exp(1j*np.angle((-L/2 + 1j*(0-r0))*(-L/2 - 1j*(0+r0)))) - 
np.exp(1j*np.angle((L/2 + 1j*(0-r0))*(L/2 - 1j*(0+r0))))


a = phase[-1, phase.shape[1]//2] - phase[-0, phase.shape[1]//2]
# -

phase.shape[1]//2



phase[-1,phase.shape[1]//2]









# +
from mmfutils.plot import imcontourf
from gpe.plot_utils import MPLGrid
#plt.figure(figsize=(15, 3*5))
#grid = MPLGrid(direction='down', space=0.1)

def plot_func(t=0, fig=None):
    from mmfutils.plot import imcontourf
    if fig is None:
        xr_ratio = 1.9
        fig = plt.figure(figsize=(12, 2))
    fig.clear()

    
    grid = MPLGrid(fig=fig, 
                   direction='down',
                   space=0., 
                   share=True)
    
    
    plt.suptitle(f"t={t:.2g}ms, vortex_r={params[0][-1][-1]:.1g}, cooling={params[0][4][-1]}")
    args = dict(vmin=0, vmax=N_max/N0.max())

    sub_grid = grid.grid(direction='right',
                             space=0.,
                             share=True)
        
    s = sims[-1].get_state(t_=t, image=False);
    x, r = [_x.ravel() for _x in s.xyz]
    r = np.concatenate((-r[::-1],r))
    ns = na, nb = s.get_density()/N0[-1]
    radial_ns = na_r, nb_r = np.concatenate((ns[:,:,::-1], ns), axis=2)
    sigma_z = (na_r - nb_r) / (na_r + nb_r)

    ax = sub_grid.next()
    imcontourf(x, r, radial_ns.sum(axis=0), aspect=1, **args)
    ax.set_ylabel(r'v_x={}'.format(params[-1][-2][-1]))
    ax.set_xlabel(r'n', size='xx-large')    
    ax = sub_grid.next()
    imcontourf(x, r, na_r, aspect=1, **args)
    ax.set_xlabel(r'na', size='xx-large')
    ax = sub_grid.next()
    imcontourf(x, r, nb_r, aspect=1, vmin=0, vmax=N_max/N0.max()/10)
    ax.set_xlabel(r'nb', size='xx-large')
    ax = sub_grid.next()
    imcontourf(x, r, np.sign(sigma_z)*sigma_z**2, aspect=1, vmin=-1, vmax=1)
    #imcontourf(x, r, sigma_z, aspect=1, vmin=-1, vmax=1)
    
    ax.set_xlabel(r'$\sigma_z$', size='xx-large')
    
    #plt.tight_layout()
    
    
plot_func(t=0.1, fig=None)    

# +
from gpe import plot_for_khalid

history = np.floor(ts*100)/100
history = history.tolist()
fig = plt.figure(figsize=(12, 2))
    
plot_for_khalid.make_movie('VortexRings_vr{}_vx{}.mp4'.format(vortex_r, v_x),
                           history, 
                           lambda t:plot_func(t=t, fig=fig), fps=12, fig=fig)




# -

# ## v_x
#
# For a radial separations plot a  moving reference

# +
# %pylab inline --no-import-all
from gpe.soc import ExperimentBarrier, u
import SOC.soc_vortex
from SOC.soc_vortex import ExperimentVortexPair3

#Sim data
import runs_vortex
r0 = runs_vortex.RunVortexMF_LowRes()


v_x = -2.5               #  -3.0, -2.5, -2.0, -1.5, -1.0, -0.5, 0.0, 0.5, 1.0, 1.5, 2.0, 2.5, 3.0
cooling_phase = 1.0      #  1.0, 1+0.01j
vortex_r = 2.            #  -3.5, -3.0, -2.5, -2.0, -1.5, -1.0, -0.5, 0.0, 0.5, 1.0, 1.5, 2.0, 2.5, 3.0, 3.5
        


sims = [_sims for _sims in r0.simulations
       if _sims.experiment.vortex_r == vortex_r
       and _sims.experiment.cooling_phase == cooling_phase
       #and _sims.experiment.v_x == v_x
       ]

params = [_sim.experiment.items() for _sim in sims]
print(params)


# +
N0 = []
for _sim in sims:
    s = _sim.get_state(t_=0, image=False);
    N0.append(s.get_N())
    
N0 = np.asarray(N0)

# +
ts = np.linspace(0,8,81)

Nmax = []
for _sim in sims:
    for _t in ts:
        time = np.floor(_t*100)/100
        s = _sim.get_state(t_=time, image=False);
        Nmax.append(s.get_density().sum(axis=0).max())

Nmax = np.asarray(Nmax)
N_max = Nmax.max()
# -



# +
from mmfutils.plot import imcontourf
from gpe.plot_utils import MPLGrid
#plt.figure(figsize=(15, 3*5))
#grid = MPLGrid(direction='down', space=0.1)

def plot_func(t=0, fig=None):
    from mmfutils.plot import imcontourf
    if fig is None:
        xr_ratio = 1.9
        fig = plt.figure(figsize=(4*xr_ratio, 13))
    fig.clear()

    
    grid = MPLGrid(fig=fig, 
                   direction='down',
                   space=0., 
                   share=True)
    
    
    plt.suptitle(f"t={t:.2g}ms, vortex_r={params[0][-1][-1]:.1g}, cooling={params[0][4][-1]}")
    args = dict(vmin=0, vmax=N_max/N0.max())

    for _sim, _params, _N0 in zip(sims[:-1], params[:-1], N0[:-1]):
        sub_grid = grid.grid(direction='right',
                             space=0.,
                             share=True)
        
        s = _sim.get_state(t_=t, image=False);
        x, r = [_x.ravel() for _x in s.xyz]
        r = np.concatenate((-r[::-1],r))
        ns = na, nb = s.get_density()/_N0
        radial_ns = na_r, nb_r = np.concatenate((ns[:,:,::-1], ns), axis=2)
        sigma_z = (na_r - nb_r) / (na_r + nb_r)

        ax = sub_grid.next()
        imcontourf(x, r, radial_ns.sum(axis=0), aspect=1, **args)
        ax.set_ylabel(r'v_x={}'.format(_params[-2][-1]))
        ax = sub_grid.next()
        imcontourf(x, r, na_r, aspect=1, **args)
        ax = sub_grid.next()
        imcontourf(x, r, nb_r, aspect=1, vmin=0, vmax=N_max/N0.max()/10)
        ax = sub_grid.next()
        imcontourf(x, r, np.sign(sigma_z)*sigma_z**2, aspect=1, vmin=-1, vmax=1)
        #imcontourf(x, r, sigma_z, aspect=1, vmin=-1, vmax=1)

    
    sub_grid = grid.grid(direction='right',
                             space=0.,
                             share=True)
        
    s = sims[-1].get_state(t_=t, image=False);
    x, r = [_x.ravel() for _x in s.xyz]
    r = np.concatenate((-r[::-1],r))
    ns = na, nb = s.get_density()/N0[-1]
    radial_ns = na_r, nb_r = np.concatenate((ns[:,:,::-1], ns), axis=2)
    sigma_z = (na_r - nb_r) / (na_r + nb_r)

    ax = sub_grid.next()
    imcontourf(x, r, radial_ns.sum(axis=0), aspect=1, **args)
    ax.set_ylabel(r'v_x={}'.format(params[-1][-2][-1]))
    ax.set_xlabel(r'n', size='xx-large')    
    ax = sub_grid.next()
    imcontourf(x, r, na_r, aspect=1, **args)
    ax.set_xlabel(r'na', size='xx-large')
    ax = sub_grid.next()
    imcontourf(x, r, nb_r, aspect=1, vmin=0, vmax=N_max/N0.max()/10)
    ax.set_xlabel(r'nb', size='xx-large')
    ax = sub_grid.next()
    imcontourf(x, r, np.sign(sigma_z)*sigma_z**2, aspect=1, vmin=-1, vmax=1)
    #imcontourf(x, r, sigma_z, aspect=1, vmin=-1, vmax=1)
    
    ax.set_xlabel(r'$\sigma_z$', size='xx-large')
    
    #plt.tight_layout()
    
    
plot_func(t=0.1, fig=None)    
# -



# +
from gpe import plot_for_khalid

history = np.floor(ts*100)/100
history = history.tolist()
xr_ratio = 1.9
fig = plt.figure(figsize=(4*xr_ratio, 13))
    
plot_for_khalid.make_movie('VortexRings_vr{}.mp4'.format(vortex_r),
                           history, 
                           lambda t:plot_func(t=t, fig=fig), fps=12, fig=fig)




# -





# ## vortex_r
#
# For each moving reference frame plot the different radial separations

# +
# %pylab inline --no-import-all
from gpe.soc import ExperimentBarrier, u
import SOC.soc_vortex
from SOC.soc_vortex import ExperimentVortexPair3

#Sim data
import runs_vortex
r0 = runs_vortex.RunVortexMF_LowRes()


v_x = -2.5               #  -3.0, -2.5, -2.0, -1.5, -1.0, -0.5, 0.0, 0.5, 1.0, 1.5, 2.0, 2.5, 3.0
cooling_phase = 1.0   #  1.0, 1+0.01j
vortex_r = 2.5       #  -3.5, -3.0, -2.5, -2.0, -1.5, -1.0, -0.5, 0.0, 0.5, 1.0, 1.5, 2.0, 2.5, 3.0, 3.5
        


sims = [_sims for _sims in r0.simulations
       if _sims.experiment.v_x == v_x
       and _sims.experiment.cooling_phase == cooling_phase
       #and _sims.experiment.vortex_r == vortex_r
       ]

params = [_sim.experiment.items() for _sim in sims]
print(params)


# +
N0 = []
for _sim in sims:
    s = _sim.get_state(t_=0, image=False);
    N0.append(s.get_N())
    
N0 = np.asarray(N0)

# +
ts = np.linspace(0,8,81)

Nmax = []
for _sim in sims:
    for _t in ts:
        time = np.floor(_t*100)/100
        s = _sim.get_state(t_=time, image=False);
        Nmax.append(s.get_density().sum(axis=0).max())

Nmax = np.asarray(Nmax)
N_max = Nmax.max()
# -





# +
from mmfutils.plot import imcontourf
from gpe.plot_utils import MPLGrid
#plt.figure(figsize=(15, 3*5))
#grid = MPLGrid(direction='down', space=0.1)

def plot_func(t=0, fig=None):
    from mmfutils.plot import imcontourf
    if fig is None:
        scale = 4
        xr_ratio = 3.9888764582490666*0+1.9
        fig = plt.figure(figsize=(4*xr_ratio,15))
    fig.clear()

    
    grid = MPLGrid(fig=fig, 
                   direction='down',
                   space=0., 
                   share=True)
    
    
    plt.suptitle(f"t={t:.2g}ms, vx={params[0][-2][-1]:.1g}, cooling={params[0][4][-1]}")
    args = dict(vmin=0, vmax=N_max/N0.max())

    for _sim, _params, _N0 in zip(sims[:-1], params[:-1], N0[:-1]):
        sub_grid = grid.grid(direction='right',
                             space=0.,
                             share=True)
        
        s = _sim.get_state(t_=t, image=False);
        x, r = [_x.ravel() for _x in s.xyz]
        r = np.concatenate((-r[::-1],r))
        ns = na, nb = s.get_density()/_N0
        radial_ns = na_r, nb_r = np.concatenate((ns[:,:,::-1], ns), axis=2)
        sigma_z = (na_r - nb_r) / (na_r + nb_r)

        ax = sub_grid.next()
        imcontourf(x, r, radial_ns.sum(axis=0), aspect=1, **args)
        ax.set_ylabel(r'v_x={}'.format(_params[-1][-1]))
        ax = sub_grid.next()
        imcontourf(x, r, na_r, aspect=1, **args)
        ax = sub_grid.next()
        imcontourf(x, r, nb_r, aspect=1, vmin=0, vmax=N_max/N0.max()/10)
        ax = sub_grid.next()
        imcontourf(x, r, np.sign(sigma_z)*sigma_z**2, aspect=1, vmin=-1, vmax=1)
        #imcontourf(x, r, sigma_z, aspect=1, vmin=-1, vmax=1)

    
    sub_grid = grid.grid(direction='right',
                             space=0.,
                             share=True)
        
    s = sims[-1].get_state(t_=t, image=False);
    x, r = [_x.ravel() for _x in s.xyz]
    r = np.concatenate((-r[::-1],r))
    ns = na, nb = s.get_density()/N0[-1]
    radial_ns = na_r, nb_r = np.concatenate((ns[:,:,::-1], ns), axis=2)
    sigma_z = (na_r - nb_r) / (na_r + nb_r)

    ax = sub_grid.next()
    imcontourf(x, r, radial_ns.sum(axis=0), aspect=1, **args)
    ax.set_ylabel(r'v_x={}'.format(params[-1][-1][-1]))
    ax.set_xlabel(r'n', size='xx-large')    
    ax = sub_grid.next()
    imcontourf(x, r, na_r, aspect=1, **args)
    ax.set_xlabel(r'na', size='xx-large')
    ax = sub_grid.next()
    imcontourf(x, r, nb_r, aspect=1, vmin=0, vmax=N_max/N0.max()/10)
    ax.set_xlabel(r'nb', size='xx-large')
    ax = sub_grid.next()
    imcontourf(x, r, np.sign(sigma_z)*sigma_z**2, aspect=1, vmin=-1, vmax=1)
    #imcontourf(x, r, sigma_z, aspect=1, vmin=-1, vmax=1)
    
    ax.set_xlabel(r'$\sigma_z$', size='xx-large')
    
    #plt.tight_layout()
    
    
plot_func(t=0.1, fig=None)    
# -



plot_func(t=0.2, fig=None)    

plot_func(t=0.3, fig=None)    

plot_func(t=0.4, fig=None)    

plot_func(t=7.5, fig=None)    


