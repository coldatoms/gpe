import numpy as np
import scipy.interpolate

sp = scipy

from gpe import utils
from gpe.soc import Experiment, u

__all__ = ["ExperimentDetune"]


class ExperimentDetune(Experiment):
    """Detuning Ramp experiment.

    Here we start with an elongated cloud in the ground state of a harmonic
    trap with some value of the SOC parameters.  The detuning is ramped from
    \delta_i to \delta_f over 10ms where \delta_i = 20KhZ + \delta_f,
    \delta_f = {-1,-2,-5}


    Attributes
    ----------
    t__wait : float
       If this is `inf`, then we start in the ground state including the
       attractive central beam, otherwise, we start in the ground state without
       the central beam, and then wait for this time while the central beam is
       on before turning it off.

    """

    states = ((1, -1), (1, 0))
    x_TF = 235 * u.micron
    t_unit = u.ms
    final_detuning_kHz = -1.0
    detuning_kHz = 20.0 + final_detuning_kHz

    B_gradient_mG_cm = 0  # Magnetic field gradients are on the order of 5-10 mG/cm
    recoil_frequency_Hz = 1843.0
    rabi_frequency_E_R = 0.5
    trapping_frequencies_Hz = (3.07, 278, 278)

    initial_imbalance = 0.0  # Nothing RF transfered, all in state[...][0]

    t__detuning_ramp = 10.0
    t__wait = 800.0  # Time until expansion
    t__image = 7.0

    def init(self):
        self.final_detuning = self.final_detuning_kHz * u.kHz * (2 * np.pi * u.hbar)
        self.t__final = self.t__detuning_ramp + self.t__wait
        Experiment.init(self)

        self._delta = utils.get_smooth_transition(
            [self.detuning, self.final_detuning],
            durations=[0.0, self.t__wait],
            transitions=[self.t__detuning_ramp],
        )

    def delta_t_(self, t_):
        return self._delta(t_)
