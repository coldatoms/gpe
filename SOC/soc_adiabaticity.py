import numpy as np

from scipy.integrate import odeint

from gpe import utils
from gpe.soc import Experiment, u


class ExperimentAdiabaticity(Experiment):
    """Start in ground state ."""

    t_unit = u.ms
    B_gradient_mG_cm = 0  # No counterflow

    detuning_kHz = 20.0
    final_detuning_kHz = -1.0
    rabi_frequency_E_R = 1.9  # Rabi frequency 1.0 - 2.9

    initial_imbalance = None  # Start in true ground state.

    t__detuning_ramp = 100  # Time to ramp detuning from negative to positive
    t__image = 7  # time for expansion when imaging
    t__final = np.inf

    def init(self):
        self.final_detuning = self.final_detuning_kHz * u.kHz * (2 * np.pi * u.hbar)

        Experiment.init(self)

        self.delta_t_ = utils.get_smooth_transition(
            [self.detuning, self.final_detuning],
            durations=[0.0],
            transitions=[self.t__detuning_ramp],
        )


class ExperimentAdiabaticitySmall(ExperimentAdiabaticity):
    """Small versions of the Adiabticity experiment allowing control of the
    dimensionless parameters.  All parameters are either dimensionless, or
    specified in micron or ms.
    """

    t_unit = u.ms

    w = 0.475
    d = 1.36
    final_d = -0.0679

    l_r_micron = 1.0 / 7.9
    xi_micron = 0.026
    T__x = 326
    T__perp = 3.6
    T__R = 0.271

    trapping_frequencies_Hz = None

    t__detuning_ramp = 10

    def init(self):
        # Compute k_r, E_R, etc.
        hbar = u.hbar
        l_r = self.l_r_micron * u.micron
        k_r = 1.0 / l_r
        T_R = self.T__R * self.t_unit
        recoil_frequency = 1.0 / T_R

        E_R = 2 * np.pi * recoil_frequency * hbar
        m = 1.0 / (2 * E_R / (hbar * k_r) ** 2)
        Ts = np.array([self.T__x, self.T__perp, self.T__perp]) * self.t_unit

        assert self.trapping_frequencies_Hz is None
        self.trapping_frequencies_Hz = 1.0 / Ts / u.Hz
        ws = 2 * np.pi / Ts

        self.recoil_frequency_Hz = recoil_frequency / u.Hz

        self.rabi_frequency_E_R = 4 * self.w
        self.rabi_frequency_kHz = None

        self.detuning_kHz = None
        self.detuning_E_R = 4 * self.d

        self.final_detuning_kHz = self.final_d * 4 * E_R / (2 * np.pi * hbar * u.kHz)

        self.ms = (m, m)

        # Set mu to give specified xi.  To do this, we need to estimate the
        # minimum of the dispersion relationship so we first need to create a
        # state, then estimate E0
        self._fiducial_V_TF = 0.0
        self.barrier_depth = 0

        ExperimentAdiabaticity.init(self)
        state = self.get_state()

        ks_ = state.basis.kx.ravel() / k_r
        Em = (ks_**2 + 1.0) / 2 - np.sqrt((ks_ - self.d) ** 2 + self.w**2)
        ind = np.argmin(Em)
        # k0 = ks_[ind] * k_r
        E0 = Em[ind] * 2 * E_R

        xi = self.xi_micron * u.micron
        mu = (hbar / xi) ** 2 / 2 / m + E0
        self.mus = (mu,) * 2
        self._fiducial_V_TF = mu
        ExperimentAdiabaticity.init(self)


class ExperimentAdiabaticityClassicalSymmetric(ExperimentAdiabaticitySmall):
    """Small versions of the Adiabticity experiment using the classical
    control.
    """

    t_unit = u.ms

    w = 0.475
    k0 = -0.95

    l_r_micron = 1.0 / 7.9
    xi_micron = 0.5
    T__perp = 3.6
    T__R = 1.0
    T__x = 3.0

    cells_x = 10
    dxyz = (0.05 * u.micron,)

    trapping_frequencies_Hz = None

    t__detuning_ramp = 10

    move_trap = False

    def init(self):
        ExperimentAdiabaticitySmall.init(self)
        self._classical_control = ClassicalControlSymmetric(
            w=self.w,
            k0=self.k0,
            omega2=(self.T__R / self.T__x) ** 2,
            T=self.t__detuning_ramp / self.T__R,
            alpha=3.0,
        )

    def delta_t_(self, t_):
        if self.move_trap:
            return 0.0
        else:
            return self._classical_control.d(t_ / self.T__R) * self.E_R

    def xyz0_t_(self, t_):
        y0 = self._classical_control.y0(t_ / self.T__R) / self.k_r
        if self.move_trap:
            y0 += self._classical_control.y0_HO(t_ / self.T__R) / self.k_r
        return (y0,) + (0.0,) * (self.dim - 1)


class ClassicalControl:
    """Classical control.  The purpose of this class is to provide a
    time-dependent detuning `d(t)` controlling the behavior of the system.
    Various subclasses will tune this to implement various forms of optimal
    control.

    Everything here is dimensionless, expressed in units of $2E_R = k_R = hbar
    = 1$.  We assume that the potential is harmonic with `k = omega2` so that
    the force is `-omega2*x`.

    The movement should be specified in terms of the function `x0(t)` which is
    the position of the moving frame as a function of time.  The derivative of
    this is the detuning in lab frame $d(t) = x_0'(t)$.  The function `x0(t,
    d)` must compute the first two derivatives.
    """

    def __init__(self, w, omega2):
        self.w = w
        self.omega2 = omega2

    def E(self, k, d=0):
        D = np.sqrt((k - d) ** 2 + self.w**2)
        return (k**2 + 1) / 2.0 - D

    def dE(self, k, d=0):
        D = np.sqrt((k - d) ** 2 + self.w**2)
        return k - (k - d) / D

    def ddE(self, k, d=0):
        D = np.sqrt((k - d) ** 2 + self.w**2)
        return 1 - self.w**2 / D**3

    def d(self, t):
        """Return the detuning."""
        return self.x0(t=t, d=1)

    def y0(self, t):
        """Return the location of the trap center in the lab frame."""
        return 0.0  # self.x0(t=t) + self.x0(t=t, d=2)/self.omega2

    def y0_HO(self, t):
        """Return the location of the trap center in the lab frame required to
        emulate the behavior of the detuning for a Harmonic potential."""
        return self.x0(t=t) + self.x0(t=t, d=2) / self.omega2

    def x0(self, t, d=0):
        """Return the center of the moving frame in the lab frame."""
        raise NotImplementedError

    def rhsL(self, q, t):
        x, k = q
        dx = self.dE(k=k, d=self.d(t=t))
        dk = -self.omega2 * (x - self.y0(t=t))
        return [dx, dk]

    def solveL(self, x0, k0, ts):
        q = [x0, k0]
        return odeint(self.rhsL, q, ts).T

    def rhsA(self, q, t):
        """EQM for frame A coordinates `q=(x_v, k_v)`."""
        x_v, k_v = q
        dx_v = self.dE(k=k_v, d=0)
        dk_v = -self.omega2 * (x_v + self.x0(t=t) - self.y0(t=t)) - self.x0(t=t, d=2)
        return [dx_v, dk_v]

    def solveA(self, x_v0, k_v0, ts):
        q_v = [x_v0, k_v0]
        return odeint(self.rhsA, q_v, ts).T

    def rhsB(self, q, t):
        """EQM for frame B coordinates `q=(x_v, k)`."""
        x_v, k = q
        dx_v = self.dE(k=k, d=self.d(t=t)) - self.x0(t=t, d=1)
        dk = -self.omega2 * (x_v + self.x0(t=t) - self.y0(t=t))
        return [dx_v, dk]

    def solveB(self, x_v0, k0, ts):
        q_v = [x_v0, k0]
        return odeint(self.rhsB, q_v, ts).T


class ClassicalControlSymmetric(ClassicalControl):
    """Toy model where we specify k0 and k1"""

    def __init__(self, k0=-0.8, w=0.4, omega2=1.0, T=1.0, alpha=3.0):
        self.w = w
        self.k0 = k0
        self.d0 = self.get_d(k=k0, w=w)
        assert np.allclose(self.d0, -self.get_d(k=-k0, w=w))
        self.omega2 = omega2
        self.T = T
        self.alpha = alpha
        from sympy import var, lambdify, tanh, tan, pi

        alpha, t, T, d0 = var("alpha, t, T, d0")
        x0 = -d0 * (t - T / 2) * tanh(alpha * tan(pi / 2 * (t - T / 2) / T))
        self._x0 = {}
        for d in range(3):
            self._x0[d] = lambdify([t, T, d0, alpha], x0.diff(t, d), "numpy")

    @staticmethod
    def get_d(k, w):
        """Return the detuning at which k is a minimum."""
        return k * (1 - w / np.sqrt(1 - k**2))

    def x0(self, t, d=0):
        return self._x0[d](t, self.T, self.d0, self.alpha) + 0 * t


class ClassicalControl2(ClassicalControl):
    """Toy model where we specify k0 and k1"""

    def __init__(self, k0=-0.8, k1=0.8, w=0.4, omega2=1.0, T=1.0):
        self.w = w
        self.k0, self.k1 = k0, k1
        self.d0 = self.get_d(k=k0, w=w)
        self.d1 = self.get_d(k=k1, w=w)
        self.omega2 = omega2
        self.T = T
        from sympy import var, lambdify

        t, T, d10 = var("t, T, d10")
        f = d10 * t**2 / T / 2
        self._f = {}
        for d in range(3):
            self._f[d] = lambdify([t, T, d10], f.diff(t, d), "numpy")

    @staticmethod
    def get_d(k, w):
        """Return the detuning at which k is a minimum."""
        return k * (1 - w / np.sqrt(1 - k**2))

    def f(self, t, d=0):
        return self._f[d](t, self.T, self.d1 - self.d0) + 0 * t
