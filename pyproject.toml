[project]

name = "gpe"
version = "0.4.dev0"
description = "Simulating superfluids with the Gross Pitaevskii-like Equations (GPEs)."
license = {text = "BSD-3-Clause"}
authors = [
  {name = "Michael McNeil Forbes", email = "michael.forbes+python@gmail.com"},
]

readme = "README.md"
classifiers = [
    "Development Status :: 4 - Beta",
    "Intended Audience :: Developers",
    "Topic :: Software Development :: Libraries :: Python Modules",
    "Topic :: Utilities",
    "Programming Language :: Python",
    "Programming Language :: Python :: 3",
    "Programming Language :: Python :: 3 :: Only",
    "Programming Language :: Python :: 3.10",
    "Programming Language :: Python :: 3.11",
]

######################################################################
# Dependencies

requires-python = '>=3.10,<3.14'
dependencies = [
    # Pip dependencies (cookiecutter.pip_dependencies)
    "mmf-setup",
    #
    # ipykernel will bring in ipython, but this has specific python
    # restrictions that we codify here.
    'ipython >= 7.16.3; python_version < "3.7"',
    'ipython >= 7.34.0; python_version < "3.8"',
    'ipython >= 8.12.3; python_version < "3.9"',
    'ipython >= 8.18.1; python_version < "3.10"',
    'ipython >= 8.20.0; python_version >= "3.10"',
    #
    'ipykernel >= 6.16.2; python_version < "3.8"',
    'ipykernel >= 6.29.0; python_version >= "3.8"',
    #
    'setuptools >= 68.0.0; python_version < "3.8"',
    'setuptools >= 69.0.3; python_version >= "3.8"',
    'setuptools-scm >= 7.1.0; python_version < "3.8"',
    'setuptools-scm >= 8.0.4; python_version >= "3.8"',
    #
    'mmf-setup>=0.4.11',
    "mmfutils>=0.7.0",
    "persist >= 3.2.1",
    "numexpr >= 2.10.0",
    "xarray >= 2024.3.0",
    "wrapt>=1.16.0",
    #
    # Need source until issues like https://github.com/pyFFTW/pyFFTW/issues/349 are resolved
    # affecting Apple's M1 platforms.
    #"pyFFTW @ git+https://github.com/pyFFTW/pyFFTW.git",
    #"pyFFTW>=0.13.1",
    "pyFFTW @ git+https://github.com/pyFFTW/pyFFTW.git",
    "tqdm>=4.66.2",
    #
    # Optional dependencies from git or hg.  You might want to use a local version:
    # see the local target below and .myenv file.
    # "gpe @ git+https://gitlab.com/coldatoms/gpe@topic/default/tube",
    # "pytimeode @ git+https://gitlab.com/coldatoms/pytimeode@topic/0.10.0/evolve_to",
]

# Optional dependencies for testing, building the docs, etc.
[project.optional-dependencies]
local = [
    # Optional local overrides for use while developing.  I make symlinks here so that
    # I don't need to use git or hg to install them.
    "pytimeode @ file:///${PROJECT_ROOT}/_ext/pytimeode",
]
test = [
    'pytest>=7.0.1',
    'coverage[toml]; python_version < "3.7"', 
    'coverage[toml]>=7.2.2; python_version >= "3.7"',
    'pytest-cov',
    'pytest-html',
    'pytest-xdist',
    'psutil',
    'gpe[full]',
]
doc = [
    'myst-nb >= 0.17.2; python_version < "3.9"',
    'myst-nb >= 1.0.0; python_version >= "3.9"',
    #
    'sphinx >= 7.1.2; python_version < "3.9"',
    'sphinx >= 7.2.6; python_version >= "3.9"',
    #
    'sphinx-book-theme >= 0.0.39; python_version < "3.9"', 
    'sphinx-book-theme >= 1.1.0; python_version >= "3.9"',
    'sphinx-jupyterbook-latex >= 0.5.2; python_version < "3.9"',
    'sphinx-jupyterbook-latex >= 1.0.0; python_version >= "3.9"',
    #
    'sphinx-togglebutton >= 0.3.2',
    'sphinx-design >= 0.5.0',
    'sphinxext-opengraph >= 0.9.1',
    'sphinx-comments >= 0.0.3',
    'nbsphinx >= 0.9.3',
    #
    'sphinx-autobuild >= 2021.3.14',
    'sphinx-autoapi >= 3.4.0',
    #
    'sphinxcontrib-zopeext >= 0.4.3',
    'sphinxcontrib-bibtex >= 2.3.0',
    #
    'mock >= 5.1.0',
    'callgraph4py >= 2.0.0',
    'nbconvert >= 7.14.2',
    'matplotlib >= 3.9.1',
]
# Support for jupyter notebooks with extensions.
notebook = [
    'black >= 23.12.1',
    'notebook < 7',
    'jupytext >= 1.16.1',
    'RISE >= 5.7.1',
    'jupyter-contrib-nbextensions >= 0.7.0',
    'nbclassic >= 1.0.0',
    'jupyter-console>=6.6.3',
    'jupyter-server<2',  # https://github.com/jupyter/notebook/issues/6702
    'ipywidgets>=8.1.5',
    'tqdm',
]

full = [
    'scipy >= 1.5.4; python_version < "3.7"',
    'scipy >= 1.7.3; python_version < "3.10" and python_version >= "3.7"',
    'scipy >= 1.10.1; python_version >= "3.10"',
    # This version of numpy has universal wheels
    'numpy >= 1.21.0; python_version >= "3.8"',
    'matplotlib >= 3.4.3',
    'uncertainties >= 3.1.5',
]

all = [
    'gpe[full,test,doc,notebook]',
]

[project.urls]
Issues = 'https://gitlab.com/coldatoms/gpe/issues'
GitLab = 'https://gitlab.com/coldatoms/gpe'


[tool.pdm]
[tool.pdm.dev-dependencies]
tools = [
      'black>21.4b2',
      'mercurial>=5.7.1',
      'hg-git>=0.10.0',
      'hg-evolve>=10.3.1',
]
dev = [
    "pip>=21.3.1",
]

[tool.pdm.build]
includes = []
[build-system]

requires = ["pdm-backend"]
build-backend = "pdm.backend"
######################################################################
# MMF-Setup
# The following allows you to add this ROOT directory to your path so you
# can import packages with the following:
#
#     import mmf_setup
#     mmf_setup.set_path()

[tool.mmf_setup]
ROOT = 'src'

######################################################################
# PyTest
[tool.pytest.ini_options]
# Note: see the discussion in https://github.com/pytest-dev/pytest/issues/2042
# If your doctests are installed, then you should provide the module here (gpe)
# and add --pyargs to the options below.  Otherwise, you will get ImportMismatchError
# for the doctests.  Use the package name or local paths here.
testpaths = [ "gpe", "./tests" ]
#testpaths = [ "src/gpe", "tests" ]
markers = [
    "bench: mark test as a benchmark.  (Might be slow, or platform dependent)",
    "slow: mark test as a slow",
    "mem: mark memory profiling tests, which can be finicky, esp. on Mac OS X.",
]
addopts = [
    "--pyargs",  # Interpret arguments as packages.  Avoids ImportMismatchError error.
    "-m not bench",
    "-m not slow",
    "-m not mem",
    "--ignore-glob=SOC/*",
    "--ignore-glob=envs/*",
		"--ignore=src/gpe/imports.py", # gpe.imports is only for interactive use with a notebook.
    "--doctest-modules",
    "--cov",
    "--cov-report=html",
    #"--cov-report=xml",
    "--cov-fail-under=85",
    "--no-cov-on-fail",
    "--cov-append",
    #"--html=_artifacts/pytest_report.html",
    #"--junitxml=_artifacts/junit.xml",
    #"-x",
    #"--pdb",
    #"--flake8",  # Disable until https://github.com/tholo/pytest-flake8/issues/92
]

doctest_optionflags = [
    "ELLIPSIS",
    "NORMALIZE_WHITESPACE",
    "IGNORE_EXCEPTION_DETAIL",
    ]

######################################################################
# Coverage
[tool.coverage.run]
# https://github.com/marketplace/actions/coveralls-python#coveragepy-configuration
branch = true
relative_files = true
parallel = true
omit = []
source = [ "src/gpe" ]

[tool.coverage.paths]
source = ["src/gpe", "**/site-packages"]

[tool.coverage.html]
directory = "build/_coverage"

#[tool.coverage.xml]
#directory = "build/_coverage"

[tool.coverage.report]
fail_under = 96

######################################################################
# Black
[tool.black]
line-length = 90
exclude = '''
    /(
        \.git
      | \.hg
      | \.nox
      | \.venv
      | _build
      | build
      | dist
    )/
'''