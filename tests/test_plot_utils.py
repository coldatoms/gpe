from gpe.plot_utils import MPLGrid


class TestMPLGrid:
    def test_empty_grid(self):
        """Test that empty grids don't raise exceptions."""
        g = MPLGrid()
        g.adjust()
        g.grid()
