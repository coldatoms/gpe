import itertools
import numpy as np

from mmfutils.math.differentiate import differentiate

from pytimeode import evolvers
from pytimeode.utils.testing import TestState as _TestState
from gpe import exact_solutions, minimize
from gpe.utils import evolve_to
from gpe import bec
import gpe.gpu.bec
from gpe.gpu import bases, cupy as cp

import pytest

pytestmark = pytest.mark.skipif(not cp, reason="No GPU support")


class State(bec.StateHOMixin, gpe.gpu.bec.StateBase):
    pass


@pytest.fixture(params=[True, False])
def subtract_mu(request):
    """This "fixture" provides the argument subtract_mu to all tests that have
    this as an argument, and runs those tests with both subtract_mu=True and
    subtract_mu=False.

    See: https://docs.pytest.org/en/latest/fixture.html#fixture-parametrize
    """
    yield request.param


@pytest.fixture(
    params=[
        dict(Nxyz=(_Nx,) * _dim, Lxyz=(10 * bec.u.micron,) * _dim, symmetric_grid=_grid)
        for _Nx, _dim, _grid, in itertools.product([2**3, 3**2], [1, 2], [True, False])
    ]
)
def state(request):
    """This "fixture" provides different states to test."""
    state = State(**request.param)
    state.cooling_phase = 1j
    state.t = -1000.0 * bec.u.ms
    e = evolvers.EvolverABM(state, dt=0.0001 * bec.u.ms)
    e.evolve(10)
    state = e.get_y()
    assert not np.allclose(0, state.get_energy())
    yield state


@pytest.fixture(
    params=[
        bases.SphericalBasis(N=2**3, R=20 * bec.u.micron),
        bases.CylindricalBasis(Nxr=(2**3,) * 2, Lxr=(20 * bec.u.micron,) * 2),
    ]
)
def basis(request):
    yield request.param


def test_state(state):
    """Test for consistency between SplitOperator and ABM interfaces"""
    t = _TestState(state)
    assert all(t.check_split_operator(normalize=True))


def test_basis(basis):
    """Test for consistency between SplitOperator and ABM interfaces"""

    state = State(basis=basis)
    state.cooling_phase = 1j
    state.t = -1000.0 * bec.u.ms
    e = evolvers.EvolverABM(state, dt=0.0001 * bec.u.ms)
    e.evolve(10)
    state = e.get_y()
    assert not np.allclose(0, state.get_energy())
    t = _TestState(state)
    assert all(t.check_split_operator(normalize=True))


def test_state_N_1():
    """Test that singleton dimensions are put in the center."""
    state = State(Nxyz=(4, 1, 1), symmetric_grid=False)
    assert np.allclose(state.xyz[1], 0)
    assert np.allclose(state.xyz[2], 0)


def test_H(state, subtract_mu):
    """Test Hamiltonian generation."""
    pytest.skip("Don't test get_H() until mmfutils provides laplacian matrix")
    H = state.get_H(subtract_mu=subtract_mu)
    dy = state.empty()
    state.compute_dy_dt(dy=dy, subtract_mu=subtract_mu)
    dy /= state._phase
    assert np.allclose(dy[...].ravel(), H.dot(state[...].ravel()))


def test_energy(state):
    """Check that H(y) is the derivative of the energy."""
    cp.random.seed(10)
    dx = (
        cp.random.random(state.data.shape)
        + cp.random.random(state.data.shape) * 1j
        - 0.5
        - 0.5j
    )

    def f(h):
        s_ = state.copy()
        s_[...] += h * dx
        E = s_.get_energy()
        return E

    res = differentiate(f, h0=0.001)
    assert not np.allclose(0, res)

    Hy = state.get_Hy(subtract_mu=False)
    # dy = state.empty()
    # state.compute_dy_dt(dy, subtract_mu=False)
    # dy /= state._phase

    dx_ = state.empty()
    dx_.data[...] = dx
    assert np.allclose(res, 2 * dx_.braket(Hy).real)


def test_t_scale(state):
    """Regression test for t_scale error."""
    E_max = (state.hbar * abs(state.basis.kx).max()) ** 2 / 2.0 / state.m
    t_scale = state.hbar / E_max

    assert np.allclose(state.t_scale, t_scale)


def test_StateTwist_x_v_x():
    """Test the boost capability of StateTwist_x using the analytic
    bright soliton solution."""
    state = exact_solutions.BrightSoliton(Nx=256, Lx=35.0, v=2.0, v_x=2.0)
    assert np.allclose(
        0, state.compute_dy_dt(state.copy(), subtract_mu=True)[...], atol=3e-6
    )
    state = exact_solutions.BrightSoliton(Nx=256, Lx=35.0, v=2.0, v_x=0.0)
    assert not np.allclose(
        0, state.compute_dy_dt(state.copy(), subtract_mu=True)[...], atol=3e-6
    )

    # Compare an evolved moving state and the stationary state.
    Nx = 64
    Lx = 20.0
    v = 2.0
    dt_t_scale = 0.4
    s0 = exact_solutions.BrightSoliton(Nx=Nx, Lx=Lx, v=v, v_x=0.0)
    s1 = exact_solutions.BrightSoliton(Nx=Nx, Lx=Lx, v=v, v_x=-v)
    s1a = exact_solutions.BrightSoliton(Nx=Nx, Lx=Lx, v=v, v_x=-v, twist=0.1)
    s2 = exact_solutions.BrightSoliton(Nx=Nx, Lx=Lx, v=v, v_x=v)
    s2L = exact_solutions.BrightSoliton(Nx=2 * Nx, Lx=2 * Lx, v=v, v_x=v)

    T = Lx / v

    # Check that s2 is stationary
    assert np.allclose(0, s2.compute_dy_dt(s2.copy(), subtract_mu=True)[...], atol=0.002)
    assert np.allclose(
        0, s2L.compute_dy_dt(s2L.copy(), subtract_mu=True)[...], atol=0.0001
    )

    # Check evolution
    n0 = s0.get_density()
    s0 = evolve_to(s0, T / 2.0, dt_t_scale=dt_t_scale)
    assert not np.allclose(s0.get_density(), n0, rtol=1.0)
    s0 = evolve_to(s0, T, dt_t_scale=dt_t_scale)
    assert np.allclose(s0.get_density(), n0, atol=0.0001 * n0.max())

    s1 = evolve_to(s1, T / 2.0, dt_t_scale=dt_t_scale)
    assert np.allclose(s1.get_density(), n0, atol=0.0001 * n0.max())

    # Check that twist does not affect the frame velocity.
    s1a = evolve_to(s1a, T / 2.0, dt_t_scale=dt_t_scale)
    assert np.allclose(s1a.get_density(), n0, atol=0.0001 * n0.max())


def test_StateScaleBase():
    """Test scaling coordinates."""
    dim = 1
    s0 = State(Nxyz=(32,) * dim)
    s0 = minimize.MinimizeState(s0).minimize()

    class StateScale(bec.StateScaleBase):
        def get_lambdas(self, t=None):
            if t is None:
                t = self.t

            ws = np.array([1, 2, 3])[: self.dim]
            lams = 1.0 + 0.1 * np.cos(ws * t)
            dlams = -0.1 * ws * np.sin(ws * t)
            ddlams = -0.1 * ws**2 * np.cos(ws * t)
            return (lams, dlams, ddlams)
