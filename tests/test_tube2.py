from __future__ import absolute_import, division, print_function, unicode_literals
import itertools
import numpy as np
from scipy.stats.mstats import gmean

from zope.interface.verify import verifyObject, DoesNotImplement

from mmfutils.math.differentiate import differentiate

from pytimeode import evolvers, interfaces
from pytimeode.utils.testing import TestState as _TestState

from gpe import bec2, tube2, minimize


import pytest


class StateNew(tube2.StateGPEdrZ):
    def get_ws(self, t=None):
        if t is None:
            t = self.t
        if t <= 0:
            return self.ws
        else:
            return np.zeroslike(self.ws)


class StateOld(tube2.StateGPEdrZ):
    """Old state to test backwards compatibility"""

    def get_ws_perp(self, t=None):
        if t is None:
            t = self.t
        if t <= 0:
            return self.ws[1:]
        else:
            return np.zeroslike(self.ws[1:])


@pytest.fixture(params=[StateNew, StateOld])
def State(request):
    yield request.param


@pytest.fixture(params=[True, False])
def subtract_mu(request):
    """This "fixture" provides the argument subtract_mu to all tests that have
    this as an argument, and runs those tests with both subtract_mu=True and
    subtract_mu=False.

    See: https://docs.pytest.org/en/latest/fixture.html#fixture-parametrize
    """
    yield request.param


@pytest.fixture(params=["N", "Nab"])
def constraint(request):
    """Different types of constraint"""
    yield request.param


@pytest.fixture(
    params=[
        dict(Nxyz=(_Nx,), Lxyz=(10 * bec2.u.micron,), gs=_gs)
        for _Nx, _gs in itertools.product(
            [2**3, 2**5], [(0.1,) * 3, (10,) * 3, (3, 4, 5)]
        )
    ]
)
def state(request, State):
    """This "fixture" provides different states to test."""
    state = State(**request.param)
    # state.cooling_phase = 1j
    # state.t = -1000.0*bec2.u.ms
    # e = evolvers.EvolverABM(state, dt=0.0001*bec2.u.ms)
    # e.evolve(10)
    # state = e.get_y()
    assert not np.allclose(0, state.get_energy())
    yield state


def test_energy(state):
    """Check that H(y) is the derivative of the energy."""
    np.random.seed(10)
    dx = (
        np.random.random(state.data.shape)
        + np.random.random(state.data.shape) * 1j
        - 0.5
        - 0.5j
    )

    def f(h):
        s_ = state.copy()
        s_[...] += h * dx
        E = s_.get_energy()
        return E

    res = differentiate(f, h0=0.001)
    assert not np.allclose(0, res)

    Hy = state.get_Hy(subtract_mu=False)
    # dy = state.empty()
    # state.compute_dy_dt(dy, subtract_mu=False)
    # dy /= state._phase

    dx_ = state.empty()
    dx_.set_psi(dx)
    assert np.allclose(res, 2 * sum(dx_.braket(Hy)).real)


def test_mcheck(state):
    """Test that the state can be minimized"""
    m1 = minimize.MinimizeState(state)
    assert m1.check()


def test_sigma2s(state):
    """Test that the calculated sigma fulfills its equation"""
    sigma2s = state.get_sigma2s()
    abs_Phi2s = state.get_density()
    gaa, gbb, gab = state.gs
    w0_perp = state.w0_perp
    ms = state.ms
    hbar = state.hbar

    _rhs = (
        gaa * abs_Phi2s[0] ** 2 / 4.0 / np.pi / sigma2s[0] ** 2
        + hbar**2 * abs_Phi2s[0] / 2.0 / ms[0] / sigma2s[0] ** 2
        + gab * abs_Phi2s.prod(axis=0) / np.pi / (sigma2s.sum(axis=0)) ** 2
    )
    _lhs = ms[0] * w0_perp**2 * abs_Phi2s[0] / 2.0
    assert np.allclose(_lhs, _rhs)
