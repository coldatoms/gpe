from __future__ import absolute_import, division, print_function, unicode_literals
import itertools
import numpy as np
from scipy.stats.mstats import gmean

from zope.interface.verify import verifyObject, DoesNotImplement

from mmfutils.math.differentiate import differentiate

from pytimeode import evolvers, interfaces
from pytimeode.utils.testing import TestState as _TestState

from gpe import bec, tube, minimize


import pytest


@pytest.fixture(params=[True, False])
def subtract_mu(request):
    """This "fixture" provides the argument subtract_mu to all tests that have
    this as an argument, and runs those tests with both subtract_mu=True and
    subtract_mu=False.

    See: https://docs.pytest.org/en/latest/fixture.html#fixture-parametrize
    """
    yield request.param


class StateNew(tube.StateGPEdrZ):
    def get_ws(self, t=None):
        return self.ws


class StateOld(tube.StateGPEdrZ):
    """Old state to test backwards compatibility"""

    def get_ws_perp(self, t=None):
        return self.ws[1:]


@pytest.fixture(params=[StateNew, StateOld])
def State(request):
    yield request.param


@pytest.fixture(
    params=[
        dict(Nxyz=(_Nx,), Lxyz=(10 * bec.u.micron,), g=_g)
        for _Nx, _g in itertools.product([2**3, 2**5], [0.1, 3.0, 10])
    ]
)
def state(request, State):
    """This "fixture" provides different states to test."""
    state = State(**request.param)
    assert not np.allclose(0, state.get_energy())
    yield state


def test_energy(state):
    """Check that H(y) is the derivative of the energy."""
    np.random.seed(10)
    dx = (
        np.random.random(state.data.shape)
        + np.random.random(state.data.shape) * 1j
        - 0.5
        - 0.5j
    )

    def f(h):
        s_ = state.copy()
        s_[...] += h * dx
        E = s_.get_energy()
        return E

    res = differentiate(f, h0=0.001)
    assert not np.allclose(0, res)

    Hy = state.get_Hy(subtract_mu=False)

    dx_ = state.empty()
    dx_.set_psi(dx)
    assert np.allclose(res, 2 * dx_.braket(Hy).real)


def test_mcheck(state):
    """Test that the state can be minimized"""
    m1 = minimize.MinimizeState(state)
    assert m1.check()


def test_n_TF(State):
    """Test that the Thomas Fermi density works well."""
    for ws, atol in (((1.1, 1.2, 1.2), 4.1e-5), ((1.1, 1.2, 1.3), 1.6e-3)):
        state0 = State(Lxyz=(10.0,), Nxyz=(64,), ws=ws, g=10.0)
        V_TF = state0.get_V_TF(x_TF=2.0)
        n_TF = state0.get_n_TF(V_TF=V_TF)
        state0.mu = state0.get_mu_from_V_TF(V_TF=V_TF)
        state = minimize.MinimizeState(state0, fix_N=False).minimize()
        err = abs((state.get_density() - n_TF) / n_TF.max()).max()
        assert np.allclose((state.get_density() - n_TF) / n_TF.max(), 0, atol=atol)
