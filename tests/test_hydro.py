import pytest
import numpy as np

from gpe import hydro_1d
from gpe.bec import u
from gpe.Examples.hydro_1d import Experiment


@pytest.fixture(
    params=[
        dict(
            Lx=32 * u.micron,
            g=1,
            m=1,
            omega=2
            * np.pi
            * np.asarray(
                [
                    1000,
                ]
            )
            * u.Hz,
            V0p=0,
            x0_t=0,
            x_TF=10 * u.micron,
        )
    ]
)
def expt(request):
    expt = Experiment(**request.param)
    yield expt


@pytest.fixture
def state(expt):
    s = expt.get_state()
    yield s


def test_density(state):
    w = state.experiment.omega
    x_TF = state.x_TF
    m = state.m
    g = state.experiment.g
    Vext = state.get_Vext()

    mu = 0.5 * m * (w * x_TF) ** 2
    n = (mu - Vext) / g
    n = np.where(n < 0, 0, n)

    assert np.allclose(state.get_density(), n)


def test_N(state):
    N = state.get_N()
    assert N == 6


def test_evo(state):
    n0 = state.get_density()
    state.evolve(t_final=1 * u.ms, show_plot=False)
    nT = state.get_density()
    assert np.allclose(n0, nT, atol=1e-2)


def test_mu(state):
    mu = 0.5 * state.m * (state.experiment.omega * state.x_TF) ** 2
    assert np.allclose(mu, state.get_V_TF(), atol=1e-7)
