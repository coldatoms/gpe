# import mmf_setup.set_path.hgroot

import numpy as np

import pytest

from gpe.minimize import MinimizeState
from mmfutils.math.differentiate import differentiate
from gpe import ufg  # , ufg_vortex_lattice


@pytest.fixture(params=[dict(Nxyz=(4, 1, 1))])
def experiment(request):
    yield ufg.Experiment(**request.param)


@pytest.fixture
def state(experiment):
    yield experiment.get_state()


@pytest.fixture(params=[True, False])
def subtract_mu(request):
    yield request.param


class TestDimer:
    def test_t_unit(self, state):
        t1 = state.experiment.t_unit
        t2 = state.experiment.hbar / state.experiment.eF
        assert np.allclose(t1, t2)

    def test_edf(self, state):
        muF = 1.23
        nF = state.get_nF_TF(muF)
        assert np.allclose(state.edf(nF, d=1), muF)
        assert np.allclose(state.get_nF_TF(-1.2), 0)

    def test_homogeneous(self, experiment):
        e = experiment
        nF = e.kF**3 / 3 / np.pi**2
        eF = (e.hbar * e.kF) ** 2 / 2 / e.mF
        EFG = 3 / 5 * eF * nF
        assert np.allclose(e.muF, e.xi * eF)

        s = e.get_state()
        m = MinimizeState(s)
        assert m.check()
        s0 = m.minimize()

    def test_pxyz_list(self, state):
        l1 = []
        assert type(l1) == type(state.pxyz)

    def test_state_N_1(self, experiment):
        """Test that singleton dimensions are put in the center."""
        state = ufg.DimerState(
            Nxyz=(4, 1, 1), symmetric_grid=False, experiment=experiment
        )
        assert np.allclose(state.xyz[1], 0)
        assert np.allclose(state.xyz[2], 0)

    @pytest.mark.skip("get_H() not yet supported")
    def test_H(state, subtract_mu):
        """Test Hamiltonian generation."""
        H = state.get_H(subtract_mu=subtract_mu)
        dy = state.empty()
        state.compute_dy_dt(dy=dy, subtract_mu=subtract_mu)
        dy /= state._phase
        assert np.allclose(dy[...].ravel(), H.dot(state[...].ravel()))

    def test_energy(self, state):
        """Check that H(y) is the derivative of the energy."""
        np.random.seed(10)
        dx = state.empty()
        dx.set_psi(
            np.random.random(state.data.shape)
            + np.random.random(state.data.shape) * 1j
            - 0.5
            - 0.5j
        )

        def f(h):
            s_ = state.copy()
            s_[...] += h * dx
            E = s_.get_energy()
            return E

        res = differentiate(f, h0=0.001)
        # assert not np.allclose(0, res)

        Hy = state.get_Hy(subtract_mu=False)
        # dy = state.empty()
        # state.compute_dy_dt(dy, subtract_mu=False)
        # dy /= state._phase

        assert np.allclose(res, 2 * dx.braket(Hy).real)
