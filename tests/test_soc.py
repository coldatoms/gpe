from __future__ import absolute_import, division, print_function, unicode_literals
import itertools
import numpy as np
from scipy.stats.mstats import gmean

from zope.interface.verify import verifyObject, DoesNotImplement

from mmfutils.math.differentiate import differentiate

from pytimeode import evolvers, interfaces
from pytimeode.utils.testing import TestState as _TestState

from gpe import soc, minimize


import pytest


@pytest.fixture(
    params=[
        dict(tube=True),
        # dict(tube=False, basis_type='1D'),
        # dict(tube=False, basis_type='axial'),
        # dict(tube=False, basis_type='2D')
    ]
)
def expt(request):
    expt = soc.Experiment(**request.param)
    yield expt


def test_energy(expt):
    """Check that H(y) is the derivative of the energy."""
    state = expt.get_state()
    state[...] += 0.01
    state.normalize()
    np.random.seed(10)
    dx = (
        np.random.random(state.data.shape)
        + np.random.random(state.data.shape) * 1j
        - 0.5
        - 0.5j
    )

    def f(h):
        s_ = state.copy()
        s_[...] += h * dx
        E = s_.get_energy()
        return E

    res = differentiate(f, h0=0.001)
    assert not np.allclose(0, res)

    Hy = state.get_Hy(subtract_mu=False)
    dx_ = state.empty()
    dx_.set_psi(dx)
    assert np.allclose(res, 2 * sum(dx_.braket(Hy)).real)


def test_mcheck(expt):
    """Test that the state can be minimized"""
    state = expt.get_state()
    m1 = minimize.MinimizeState(state)
    assert m1.check()


def _test_n_TF():
    """Test that the Thomas Fermi density works well."""

    state0 = State(
        Lxyz=(10.0,), Nxyz=(64,), ws=(1.1, 1.2, 1.3), g=10.0
    )  # Make sure TF is fairly good approximation
    V_TF = state0.get_V_TF(x_TF=2.0)
    n_TF = state0.get_n_TF(V_TF=V_TF)
    state0.mu = state0.get_mu_from_V_TF(V_TF=V_TF)
    state = minimize.MinimizeState(state0, fix_N=False).minimize()
    assert np.allclose((state.get_density() - n_TF) / n_TF.max(), 0, atol=2.8e-4)
