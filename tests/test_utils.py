from __future__ import absolute_import, division, print_function, unicode_literals

import glob
import os
import shutil
import psutil
import tempfile

import numpy as np

from gpe import utils

import pytest


@pytest.fixture()
def tmpdir():
    """Provides a temporary directory for testing."""
    tmpdir = tempfile.mkdtemp()
    yield tmpdir
    shutil.rmtree(tmpdir)


def test_step():
    """Test the step function."""
    t1 = 2.0
    for alpha in [1.0, 3.0, 7.0]:
        for t in [-0.1, 0.0]:
            assert utils.step(t, t1, alpha) == 0
        for t in [t1 // 2]:
            assert utils.step(t, t1, alpha) == 0.5
        for t in [t1, t1 + 1.0]:
            assert utils.step(t, t1, alpha) == 1


@pytest.fixture(params=[0, 2, 4, 6])
def order(request):
    yield request.param


def test_x2_2(order):
    x = np.linspace(0.1, 0.1, 10)
    assert np.allclose(utils.x2_2(x), x**2 / 2.0)
    assert np.allclose(utils.x2_2(x, order), utils.x2_2(x + 2 * np.pi, order))
    assert not np.allclose(utils.x2_2(x, order), utils.x2_2(x + np.pi, order))


def test_x2_2_coverage():
    """Coverage test of exceptions."""
    with pytest.raises(NotImplementedError) as err:
        utils.x2_2(0.0, order=1.2)
    assert str(err.value) == "Got order=1.2.  Must be 0, 2, 4, or 6."


def test_good_Ns():
    assert np.allclose(
        utils.good_Ns(30),
        [
            2,
            3,
            2**2,
            5,
            2 * 3,
            2**3,
            3**2,
            2 * 5,
            2**2 * 3,
            3 * 5,
            2**4,
            2 * 3**2,
            2**2 * 5,
            2**3 * 3,
            5**2,
            3**3,
            2 * 3 * 5,
        ],
    )
    assert utils.good_Ns(32796)[-1] == 32768


def test_smooth_transition():
    f = utils.get_smooth_transition([0, 1.34, -2.1], [1.1, 1.5], [0.1, 0.2])
    assert np.allclose([f(-10), f(0), f(0.5), f(1.1)], 0)
    assert np.allclose([f(1.2), f(2.0), f(2.7)], 1.34)
    assert np.allclose([f(2.9), f(3.0), f(100.0)], -2.1)

    # Check smoothness of transitions.
    boundaries = [(1.09, 1.21), (2.69, 2.91)]
    mins = []
    maxs = []
    for N in [1000, 2000]:
        _mins = []
        _maxs = []
        for t0, t1 in boundaries:
            ts = np.linspace(t0, t1, N)
            dt = np.diff(ts)[0]
            fs = np.vectorize(f)(ts)
            dfs = np.diff(fs) / dt
            ddfs = np.diff(dfs) / dt
            dddfs = np.diff(ddfs) / dt
            _mins.append([dfs.min(), ddfs.min(), dddfs.min()])
            _maxs.append([dfs.max(), ddfs.max(), dddfs.max()])
        mins.append(_mins)
        maxs.append(_maxs)
    assert np.allclose(*mins, rtol=3e-4)
    assert np.allclose(*maxs, rtol=3e-4)


@pytest.mark.parametrize(
    "use_array_input", [True, False], ids=["array_input", "float_input"]
)
def test_smooth_transition_exists(use_array_input):
    """Smooth func interpolates between its `fs` values instead of just jumping."""
    ts = np.arange(0, 10, 0.1)

    fs = [0, 1]
    durations = [4]
    transitions = [2]
    f = utils.get_smooth_transition(fs, durations, transitions)

    if use_array_input:
        ys = f(ts)

    else:
        ys = [f(t) for t in ts]

    assert np.diff(ys).max() < np.diff(fs).max()


def myglob(*path_components):
    """My version of glob that sorts the results."""
    files = sorted(map(os.path.basename, glob.glob(os.path.join(*path_components))))
    return files


class TestFrames:
    def test_frames_default(self, tmpdir):
        with utils.Frames(tmpdir, "w") as frames:
            frames[1.0] = [1, 2, 3]

            # No files saved until flush() called since we did not use
            # immediate mode.
            files = myglob(tmpdir, "*")
            assert files == []

            frames.immediate = True
            frames[1.1] = [2, 2, 3]

            # Previous files not saved until flush(), but new files are saved.
            files = myglob(tmpdir, "*")
            assert files == ["frame_1.1000.npy"]

            frames[100.4] = [3, 2, 3]
            frames[1.4] = [4, 2, 3]

            # Test tuples and lists of frames
            frames[(1.4, 0.1)] = [4, 2, 1]
            frames[(1.4, 0.2)] = [4, 2, 2]

        files = myglob(tmpdir, "*")
        assert files == [
            "frame_1.0000.npy",
            "frame_1.1000.npy",
            "frame_1.4000.npy",
            "frame_1.4000_image_0.1000.npy",
            "frame_1.4000_image_0.2000.npy",
            "frame_100.4000.npy",
        ]

        assert list(frames.keys()) == [1.0, 1.1, 1.4, (1.4, 0.1), (1.4, 0.2), 100.4]

        # Can use strings as keys too.
        del frames["100.4"]
        del frames[("1.4", "0.2")]
        files = myglob(tmpdir, "*")
        assert files == [
            "frame_1.0000.npy",
            "frame_1.1000.npy",
            "frame_1.4000.npy",
            "frame_1.4000_image_0.1000.npy",
        ]

        del frames

        frames = utils.Frames(tmpdir, "r")

        assert list(frames.keys()) == [1.0, 1.1, 1.4, (1.4, 0.1)]

        assert frames[1.0].tolist() == [1, 2, 3]
        assert frames[1.1].tolist() == [2, 2, 3]
        assert frames[1.4].tolist() == [4, 2, 3]
        assert frames[(1.4, 0.1)].tolist() == [4, 2, 1]

    def test_frames_in_memory(self, tmpdir):
        with utils.Frames(tmpdir, "m") as frames:
            frames[1.0] = [1, 2, 3]

            assert not myglob(tmpdir, "*")

            frames.immediate = True
            frames[1.1] = [2, 2, 3]

            # No files
            assert not myglob(tmpdir, "*")

            frames[100.4] = [3, 2, 3]
            frames[1.4] = [4, 2, 3]
            frames[(1.4, 0.1)] = [4, 2, 1]
            frames[(1.4, 0.2)] = [4, 2, 2]

            assert list(frames.keys()) == [1.0, 1.1, 1.4, (1.4, 0.1), (1.4, 0.2), 100.4]

        # No files
        assert not myglob(tmpdir, "*")

        assert list(frames.keys()) == [1.0, 1.1, 1.4, (1.4, 0.1), (1.4, 0.2), 100.4]

        # Can use strings as keys too.
        del frames["100.4"]
        del frames[("1.4", "0.2")]
        assert not myglob(tmpdir, "*")

        assert list(frames.keys()) == [1.0, 1.1, 1.4, (1.4, 0.1)]

        assert frames[1.0].tolist() == [1, 2, 3]
        assert frames[1.1].tolist() == [2, 2, 3]
        assert frames[1.4].tolist() == [4, 2, 3]
        assert frames[(1.4, 0.1)].tolist() == [4, 2, 1]

    def test_frames_default_context(self, tmpdir):
        with utils.Frames(tmpdir, "w") as frames:
            frames[1.0] = [1, 2, 3]
            files = myglob(tmpdir, "*")
            assert files == []

        files = myglob(tmpdir, "*")
        assert files == ["frame_1.0000.npy"]

    def test_frames_default_no_context(self, tmpdir):
        frames = utils.Frames(tmpdir, "w")
        frames[1.0] = [1, 2, 3]
        files = myglob(tmpdir, "*")
        assert files == ["frame_1.0000.npy"]

    def test_frames_immediate(self, tmpdir):
        with utils.Frames(tmpdir, "w", immediate=True) as frames:
            frames[1.0] = [1, 2, 3]
            files = myglob(tmpdir, "*")
            assert files == ["frame_1.0000.npy"]

    def test_setting_data_twice(self, tmpdir):
        with utils.Frames(tmpdir, "w") as frames:
            frames[1.1] = [1, 2, 3]
            with pytest.raises(LookupError) as err:
                frames[1.1] = [2, 3, 4]
            assert str(err.value) == "Data for key=1.1 already set. (Call del first.)"

        with utils.Frames(tmpdir) as frames:
            with pytest.raises(LookupError) as err:
                frames[1.1] = [2, 3, 4]
            assert str(err.value) == "File for key=1.1 already exists. (Call del first.)"

            with pytest.raises(LookupError) as err:
                del frames[1.2]
            assert str(err.value) == "1.2"

            del frames[1.1]
            frames[1.1] = [2, 3, 4]

    def test_saving_over_existing_file(self, tmpdir):
        with utils.Frames(tmpdir, "w") as frames:
            frames[1.1] = [1, 2, 3]

        with pytest.raises(IOError) as err:
            with utils.Frames(tmpdir, "w") as frames:
                del frames._files[frames.key_to_ikey(1.1)]
                frames[1.1] = [2, 3, 4]
        assert str(err.value).endswith("frame_1.1000.npy already exists!")

    def test_coverage(self, tmpdir):
        with utils.Frames(tmpdir, "w") as frames:
            assert frames._filename_to_ikey("") == ()

    def test_missing_file(self, tmpdir):
        with utils.Frames(tmpdir, "w") as frames:
            frames[1.1] = [1, 2, 3]
            frames[1.2] = [2, 2, 3]
            frames[1.3] = [3, 2, 3]

        del frames
        frames = utils.Frames(tmpdir, "r")
        assert frames[1.2].tolist() == [2, 2, 3]

        # Deleting file does not alter loaded data.
        os.remove(frames.get_filename(1.2))
        assert frames[1.2].tolist() == [2, 2, 3]

        # Deleting file does not alter loaded data...
        os.remove(frames.get_filename(1.1))

        # ... but data is only loaded on demand.
        with pytest.raises(LookupError):
            assert frames[1.1].tolist() == [1, 2, 3]

        files = myglob(tmpdir, "*")
        assert files == ["frame_1.3000.npy"]

        # Loaded data can be saved again by a flush, but must make writable.
        with pytest.raises(ValueError) as err:
            frames.flush()
        assert str(err.value) == "Cannot flush data in mode='r'"

        frames.mode = "rw"
        frames.flush()

        assert list(frames.keys()) == [1.2, 1.3]

        files = myglob(tmpdir, "*")
        assert files == ["frame_1.2000.npy", "frame_1.3000.npy"]

        # Or if you delete the file and force an update of the keys:
        os.remove(frames.get_filename(1.2))
        assert frames[1.2].tolist() == [2, 2, 3]
        assert list(frames.keys()) == [1.2, 1.3]

        files = myglob(tmpdir, "*")
        assert files == ["frame_1.3000.npy"]

        assert frames.keys(update=True)
        assert list(frames.keys()) == [1.3]
        with pytest.raises(LookupError):
            assert frames[1.2].tolist() == [2, 2, 3]

        del frames

        # Subsequent loading will not have the data.
        frames = utils.Frames(tmpdir, "r")
        assert list(frames.keys()) == [1.3]

    def test_get_filename(self, tmpdir):
        frames = utils.Frames(tmpdir, "w")
        frames[1.1] = [1, 2, 3]

        assert np.allclose([1, 2, 3], np.load(frames.get_filename(1.1)))

    def test_malformed_filename(self, tmpdir):
        frames = utils.Frames(tmpdir, "w")
        frames[1.1] = [1, 2, 3]
        with open(os.path.join(frames.data_dir, "frame_bad_file.npy"), "w") as f:
            f.write("hi")

        frames = utils.Frames(tmpdir, "r")
        with pytest.warns(UserWarning) as err:
            list(frames.keys())
        assert len(err) == 1
        assert (
            err[0]
            .message.args[0]
            .startswith("Skipping malformed frame name frame_bad_file.npy (should be")
        )

    def test_data_dir_creation(self, tmpdir):
        frames = utils.Frames(os.path.join(tmpdir, "new"), "w")
        assert not os.path.exists(frames.data_dir)
        frames._get_data_dir()
        assert os.path.exists(frames.data_dir)

    def test_reading_from_nowhere(self, tmpdir):
        frames = utils.Frames(os.path.join(tmpdir, "new"), "r")
        with pytest.raises(ValueError) as err:
            frames._get_data_dir()
        assert str(err.value).startswith("Cannot read (mode='r') since directory")
        assert str(err.value).endswith(" does not exist.")

    def test_reading_from_file(self, tmpdir):
        with open(os.path.join(tmpdir, "new"), "w") as f:
            f.write("Hi")

        frames = utils.Frames(os.path.join(tmpdir, "new"), "r")
        with pytest.raises(IOError) as err:
            frames._get_data_dir()
        assert str(err.value).startswith("Specified data_dir=")
        assert str(err.value).endswith(" is not a directory.")

    def test_read_only_warning(self, tmpdir):
        with pytest.warns(UserWarning) as err:
            with utils.Frames(tmpdir, "r") as frames:
                frames[1.1] = [1, 2, 3]
        assert len(err) == 1
        assert (
            "Setting Frame[1.1] in mode='r' will not get saved to disk!"
            == err[0].message.args[0]
        )

    def test_precision_loss_error(self, tmpdir):
        """Test that an error is raise if precision is lost."""
        frames = utils.Frames(tmpdir, "w", decimal_precision=4)
        with pytest.raises(ValueError) as err:
            frames[1.12341] = [1, 2, 3]
        assert "Precision lost when converting key: 1.12341->Decimal('1.1234')" == str(
            err.value
        )

    def test_decimal_conversion(self):
        """Test error with numpy type conversion to Decimal."""
        frames = utils.Frames(tmpdir, "w", decimal_precision=4)
        assert frames.Decimal(np.int64(1)) == frames.Decimal(1)
        if hasattr(np, "float128"):
            assert frames.Decimal(np.float128(1.2)) == frames.Decimal(1.2)

    def test_increased_precision(self, tmpdir):
        """Test that if files require higher decimal_precision, it is increased."""
        frames = utils.Frames(tmpdir, "w", decimal_precision=8)
        frames[1.12345678] = [1, 2, 3]

        with utils.Frames(tmpdir, "r", decimal_precision=4) as frames:
            assert frames.decimal_precision == 8

    def test_frame_0_issue(self, tmpdir):
        with utils.Frames(tmpdir, "w") as frames:
            frames[0.0] = [1, 2, 3]
            assert 0.0 in list(frames.keys())

    def test_issue_2(self, tmpdir):
        """Test issue where an inappropriately timed KeyboardInterrupt
        will prevent the context from exiting properly leading to an
        error NotImplementedError: Nested contexts not supported.
        """
        frames = utils.Frames(tmpdir, "w")
        frames._issue_2 = True
        with pytest.raises(KeyboardInterrupt):
            with frames as f:
                f[1.0] = [1, 2, 3]

        del frames._issue_2
        with frames as f:
            f[2.0] = [2, 3, 4]

        assert frames[1.0].tolist() == [1, 2, 3]
        assert frames[2.0].tolist() == [2, 3, 4]

    def test_checkpoint(self, tmpdir):
        with utils.Frames(tmpdir, "w") as frames:
            frames[1.0] = [1, 2, 3]

            # No files saved until flush() called since we did not use
            # immediate mode.
            files = myglob(tmpdir, "*")
            assert files == []

            frames.checkpoint(1.1, [1, 2, 4])

            # Checkpoints should be saved immediately however.
            files = myglob(tmpdir, "*")
            assert files == ["check_1.1000.npy"]

        files = myglob(tmpdir, "*")
        assert files == [
            "check_1.1000.npy",
            "frame_1.0000.npy",
        ]

        with utils.Frames(tmpdir, "r") as frames:
            assert frames.keys() == [1.0, 1.1]
            assert np.allclose(frames[1.0], [1, 2, 3])
            assert np.allclose(frames[1.1], [1, 2, 4])

    @pytest.mark.mem
    def test_issue_memory(self, tmpdir):
        """Test issue where the size required to store frames
        in memory should remain the same and not increase."""

        # Frame.param: max_mem = 5*MB
        # Storing 1 frame 8 MB
        dtype = complex
        obj_size = np.dtype(dtype).itemsize
        MB = 1024**2
        frame_size = 8 * MB
        N = int(frame_size / obj_size)

        def mem_frames():
            memory_info = psutil.Process().memory_info()
            return np.round((memory_info.vms - mem_0) / frame_size, 1)

        memory_info = psutil.Process().memory_info()
        mem_0 = memory_info.vms

        with utils.Frames(tmpdir, "w") as frame_:
            frame_[0] = np.arange(N, dtype=dtype)
        assert np.allclose(1, mem_frames())
        # del frame_[0]

        # Storing 5 frames each 8 MB
        with utils.Frames(tmpdir, "w") as frames:
            for i in range(5):
                frames[i + 1] = np.zeros(N, dtype=dtype)
        mem_5 = psutil.Process().memory_info().vms

        # Total of 6 saved frames, 8 MB each
        assert np.allclose(6, mem_frames())


class TestSimulation:
    def test1(self, tmpdir):
        experiment = utils.ExperimentExample(image_ts_=(1.0,))
        t_unit = experiment.t_unit
        simulation = utils.Simulation(
            dt_=0.3, dt_t_scale=0.1, experiment=experiment, data_dir=tmpdir
        )
        assert np.allclose(simulation.get_ts_(), [0.0, 0.3, 0.6, 0.9, 1.0])

        simulation.run()

        files = myglob(tmpdir, "ExperimentExample", "*py")
        assert files == [
            "experiment.py",
            "frame_0.0000.npy",
            "frame_0.3000.npy",
            "frame_0.6000.npy",
            "frame_0.9000.npy",
            "frame_1.0000.npy",
        ]
        simulation = utils.Simulation(
            dt_=0.1, dir_name=simulation.dir_name, data_dir=tmpdir
        )
        state = simulation.get_state(1.0)
        assert np.allclose(state.t, 1.0 * t_unit)
        assert np.allclose(state[...], state.answer())

    def test_max_t_0(self, tmpdir):
        """Should be able to limit simulations with max_t_.

        This also tests max_t_ == 0 which creates some empty lists.
        """
        experiment = utils.ExperimentExample(image_ts_=[1.0, 2.0])
        simulation = utils.Simulation(
            dt_=0.3, dt_t_scale=0.1, max_t_=0, experiment=experiment, data_dir=tmpdir
        )
        assert np.allclose(simulation.get_ts_(), [0.0])

        simulation.run()

        files = myglob(tmpdir, "ExperimentExample", "*py")
        assert files == ["experiment.py", "frame_0.0000.npy"]
        simulation = utils.Simulation(
            dt_=0.1, dir_name=simulation.dir_name, data_dir=tmpdir
        )
        state = simulation.get_state(0.0)
        assert np.allclose(state[...], state.answer())

    def test_params(self, tmpdir):
        experiment = utils.ExperimentExample(amplitude=2.0)
        simulation = utils.Simulation(
            dt_=0.3,
            max_t_=0.9,
            dt_t_scale=0.1,
            experiment=experiment,
            data_dir=tmpdir,
        )
        assert np.allclose(simulation.get_ts_(), [0.0, 0.3, 0.6, 0.9])

        simulation.run()

        files = myglob(tmpdir, "ExperimentExample/amplitude=2.0", "*py")
        assert files == [
            "experiment.py",
            "frame_0.0000.npy",
            "frame_0.3000.npy",
            "frame_0.6000.npy",
            "frame_0.9000.npy",
        ]
        simulation = utils.Simulation(
            dt_=0.1, dir_name=simulation.dir_name, data_dir=tmpdir
        )
        state = simulation.get_state(0.9)
        assert np.allclose(state[...], state.answer())

    def test_no_checkpoint(self, tmpdir):
        experiment = utils.ExperimentExample()
        simulation = utils.Simulation(
            dt_=0.3,
            dt_t_scale=0.1,
            max_t_=0.9,
            experiment=experiment,
            checkpoint=False,
            data_dir=tmpdir,
        )
        assert np.allclose(simulation.get_ts_(), [0.0, 0.3, 0.6, 0.9])

        simulation.run()

        assert not os.path.exists(os.path.join(tmpdir, "ExperimentExample"))
        state = simulation.get_state(0.9)
        assert np.allclose(state[...], state.answer())

    def test_imaging(self, tmpdir):
        """Test the formation of expansion images."""
        # Should be able to specify in both experiment and simulation.
        experiment = utils.ExperimentExample(image_ts_=[0.2], t__image=0.15)
        with pytest.warns(DeprecationWarning) as err:
            simulation = utils.Simulation(
                dt_=0.1,
                dt_t_scale=0.01,
                max_t_=0.5,
                image_ts_=[0.45],
                experiment=experiment,
                data_dir=tmpdir,
            )
        assert len(err) == 1
        assert (
            err[0]
            .message.args[0]
            .startswith("image_ts_ is deprecated: please use experiment.image_ts_")
        )

        assert np.allclose(simulation.get_ts_(), [0, 0.1, 0.2, 0.3, 0.4, 0.45, 0.5])
        simulation.run()
        files = myglob(tmpdir, "ExperimentExample", "*py")
        assert files == [
            "experiment.py",
            "frame_0.0000.npy",
            "frame_0.1000.npy",
            "frame_0.2000.npy",
            "frame_0.3000.npy",
            "frame_0.4000.npy",
            "frame_0.4500.npy",
            "frame_0.5000.npy",
        ]
        simulation.run_images()

        files = myglob(tmpdir, "ExperimentExample", "*py")
        assert files == [
            "experiment.py",
            "frame_0.0000.npy",
            "frame_0.1000.npy",
            "frame_0.2000.npy",
            "frame_0.2000_image_0.1000.npy",
            "frame_0.2000_image_0.1500.npy",
            "frame_0.3000.npy",
            "frame_0.4000.npy",
            "frame_0.4500.npy",
            "frame_0.4500_image_0.1000.npy",
            "frame_0.4500_image_0.1500.npy",
            "frame_0.5000.npy",
        ]

        state = experiment.get_state()
        for t_ in [0, 0.1, 0.2, 0.3, 0.4, 0.45, 0.5]:
            state = simulation.get_state(t_=t_)
            assert np.allclose(state[0], state.answer())

        for t_ in simulation.image_ts_:
            state = simulation.get_state(t_=t_, t__image=True)
            # Low tolerance here because the non-analytic transition at the
            # start of imaging introduces a rather large error.
            assert np.allclose(state[0], state.answer(), rtol=0.002)

    def test_delegation(self):
        """Check that simulations respect experiment.Evolver and
        experiment.dt_t_scale when set.
        """

        class Experiment(utils.ExperimentExample):
            dt_t_scale = 2
            Evolver = "E2"

        experiment0 = utils.ExperimentExample()
        simulation = utils.Simulation(dt_t_scale=1, Evolver="E1", experiment=experiment0)
        assert simulation.dt_t_scale == 1
        assert simulation.Evolver == "E1"

        experiment1 = Experiment()
        simulation = utils.Simulation(dt_t_scale=1, Evolver="E1", experiment=experiment1)

        assert simulation.dt_t_scale == 2
        assert simulation.Evolver == "E2"

    def test_checkpoints(self, tmpdir):
        """Test checkpoint_dt_ < dt_."""
        experiment = utils.ExperimentExample()
        simulation = utils.Simulation(
            dt_=0.3,
            dt_t_scale=0.1,
            max_t_=1.0,
            checkpoint_dt_=0.2,
            checkpoints_to_retain=10,
            experiment=experiment,
            data_dir=tmpdir,
        )
        assert np.allclose(simulation.get_ts_(), [0.0, 0.3, 0.6, 0.9, 1.0])

        simulation.run()

        files = myglob(tmpdir, "ExperimentExample", "*py")
        # Note: the checkpoint numbers here are not exactly checkpoint_dt_ because we
        # don't actually change the simulation dt_ to preserve accuracy and performance.
        assert files == [
            "check_0.1800.npy",
            "check_0.4800.npy",
            "check_0.7800.npy",
            "experiment.py",
            "frame_0.0000.npy",
            "frame_0.3000.npy",
            "frame_0.6000.npy",
            "frame_0.9000.npy",
            "frame_1.0000.npy",
        ]

        simulation = utils.Simulation(
            dt_=0.2,
            max_t_=1.0,
            dir_name=simulation.dir_name,
            checkpoint_dt_=0.1,
            checkpoints_to_retain=1,
            data_dir=tmpdir,
            allow_negative_dt=False,
        )
        simulation.run()
        state = simulation.get_state(1.0)
        assert np.allclose(state[...], state.answer())

        files = myglob(tmpdir, "ExperimentExample", "*py")
        assert files == [
            "check_0.7800.npy",
            "experiment.py",
            "frame_0.0000.npy",
            "frame_0.2000.npy",
            "frame_0.3000.npy",
            "frame_0.4000.npy",
            "frame_0.6000.npy",
            "frame_0.8000.npy",
            "frame_0.9000.npy",
            "frame_1.0000.npy",
        ]


def test_pauli_matrices():
    sx, sy, sz = ss = utils.pauli_matrices
    for s in ss:
        assert np.allclose(s.dot(s), np.eye(2))
    assert np.allclose(sx.dot(sy), 1j * sz)
    assert np.allclose(sy.dot(sz), 1j * sx)
    assert np.allclose(sz.dot(sx), 1j * sy)


class TestExperiment:
    def test_set_unknown_attribute_init(self):
        """Test that setting an unknown attribute through the constructor
        raises an ValueError.
        """
        with pytest.raises(ValueError) as err:
            utils.ExperimentExample(unknown_parameter=1)
        assert str(err.value).startswith(
            "ExperimentExample got unexpected keyword argument(s) "
            + "['unknown_parameter']."
        )

    def test_unused_class_parameter(self):
        e = utils.ExperimentExample()
        e.get_initial_state()
        assert not e._unused_keys()

        # Now make an unused parameter
        class Experiment(utils.ExperimentExample):
            unused_parameter = 3
            used_parameter = 4

            def init(self):
                self.used_parameter
                utils.ExperimentExample.init(self)

        e = Experiment()

        # This should use all parameters
        e.get_initial_state()

        assert e._unused_keys() == set(["unused_parameter"])

    def test_set_special_params(self):
        """Regression test identifying bug where special parameters
        could not be set in the constructor."""

        e = utils.ExperimentExample(max_key_length=4)
        assert e.max_key_length == 4

    def test_key_order(self):
        """Regression test for issue_4.

        Experiment.dir_name should be unique and reproducible each
        run.  This is a hard test to trigger since the failure depends
        on the python internals.
        """

        class Experiment(utils.ExperimentExample):
            sort_keys = True
            a = 1
            d = 2
            x = 3
            z = 4
            important_key = 1.23

        e = Experiment(a=3, d=2, x=1, important_key=0)
        assert e.dir_name == "Experiment/a=3/d=2/important_key=0/x=1"

        # Make sure the important_key comes first.
        e = Experiment(a=3, d=2, x=1, important_key=0, key_order=["important_key"])
        assert e.key_order == ["important_key"]
        assert e.dir_name == "Experiment/important_key=0/a=3/d=2/x=1"
