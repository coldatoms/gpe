from __future__ import absolute_import, division, print_function, unicode_literals
import itertools
import numpy as np

from mmfutils.math.differentiate import differentiate

from pytimeode import evolvers
from pytimeode.utils.testing import TestState as _TestState
from gpe import bec_basic as bec

import pytest


@pytest.fixture(params=[True, False])
def subtract_mu(request):
    """This "fixture" provides the argument subtract_mu to all tests that have
    this as an argument, and runs those tests with both subtract_mu=True and
    subtract_mu=False.

    See: https://docs.pytest.org/en/latest/fixture.html#fixture-parametrize
    """
    yield request.param


@pytest.fixture(
    params=[
        dict(Nxyz=(_Nx,) * _dim, Lxyz=(10 * bec.u.micron,) * _dim, symmetric_grid=_grid)
        for _Nx, _dim, _grid in itertools.product([2**3, 3**2], [1, 2], [True, False])
    ]
)
def state(request):
    """This "fixture" provides different states to test."""
    state = bec.State(**request.param)
    state.cooling_phase = 1j
    state.t = -1000.0 * bec.u.ms
    e = evolvers.EvolverABM(state, dt=0.0001 * bec.u.ms)
    e.evolve(10)
    state = e.get_y()
    assert not np.allclose(0, state.get_energy())
    yield state


def test_state(state):
    """Test for consistency between SplitOperator and ABM interfaces"""
    t = _TestState(state)
    assert all(t.check_split_operator(normalize=True))


def test_state_N_1():
    """Test that singleton dimensions are put in the center."""
    state = bec.State(Nxyz=(4, 1, 1), symmetric_grid=False)
    assert np.allclose(state.xyz[1], 0)
    assert np.allclose(state.xyz[2], 0)


def test_H(state, subtract_mu):
    """Test Hamiltonian generation."""
    H = state.get_H(subtract_mu=subtract_mu)
    dy = state.empty()
    state.compute_dy_dt(dy=dy, subtract_mu=subtract_mu)
    dy /= state._phase
    assert np.allclose(dy[...].ravel(), H.dot(state[...].ravel()))


def test_energy(state):
    """Check that H(y) is the derivative of the energy."""
    np.random.seed(10)
    dx = (
        np.random.random(state.data.shape)
        + np.random.random(state.data.shape) * 1j
        - 0.5
        - 0.5j
    )

    def f(h):
        s_ = state.copy()
        s_[...] += h * dx
        E = s_.get_energy()
        return E

    res = differentiate(f, h0=0.001)
    assert not np.allclose(0, res)

    Hy = state.get_Hy(subtract_mu=False)
    # dy = state.empty()
    # state.compute_dy_dt(dy, subtract_mu=False)
    # dy /= state._phase

    assert np.allclose(res, 2 * state.braket(dx, Hy).real)
