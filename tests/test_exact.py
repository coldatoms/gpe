from __future__ import absolute_import, division, print_function, unicode_literals

import numpy as np

from gpe import exact_solutions

import pytest


def get_H(V, psi_tol=1e-4):
    """Return an appropriate Hamiltonian to solve for the state
    returned by V.E_psi_exact().
    """

    # Compute L
    x = np.linspace(-100.0, 100.0, 1024)
    dx = np.diff(x)[0]
    E0, psi = V.E_psi_exact(x)
    psi_t = np.fft.fftshift(np.fft.fft(psi))
    inds = np.where(abs(psi) / abs(psi).max() > psi_tol)[0]
    L = x[inds[-1]] - x[inds[0]]
    x0 = (x[inds[-1]] + x[inds[0]]) / 2

    # Compute k_c
    k = np.fft.fftshift(2 * np.pi * np.fft.fftfreq(len(x), dx))
    inds = np.where(abs(psi_t) / abs(psi_t).max() > psi_tol)[0]
    k_c = max(abs(k[inds[0]]), abs(k[inds[-1]]))

    N = int(np.ceil(k_c * L / np.pi)) // 2 * 2
    dx = L / N
    x = np.arange(N) * dx - L / 2.0 + x0
    k = 2 * np.pi * np.fft.fftfreq(N, dx)
    K_ = np.fft.ifft((V.hbar * k) ** 2 / 2 / V.m * np.fft.fft(np.eye(N)))
    H_ = K_ + np.diag(V(x))
    return H_
    assert np.allclose(H_, H_.T.conj())
    assert np.allclose(np.linalg.eigvalsh(H_)[0], E0)


def test_HarmonicOscillator():
    s = exact_solutions.HarmonicOscillator()
    ds = s.empty()
    s.compute_dy_dt(ds, subtract_mu=True)
    assert np.allclose(ds.ravel(), 0)


def _test_PoschlTeller():
    V = exact_solutions.PoschlTeller()

    psi_tol = 1e-5

    # Compute L
    x = np.linspace(-100.0, 100.0, 1024)
    dx = np.diff(x)[0]
    E0, psi = V.E_psi_exact(x)
    psi_t = np.fft.fftshift(np.fft.fft(psi))
    inds = np.where(abs(psi) / abs(psi).max() > psi_tol)[0]
    L = x[inds[-1]] - x[inds[0]]
    x0 = (x[inds[-1]] + x[inds[0]]) / 2

    # Compute k_c
    k = np.fft.fftshift(2 * np.pi * np.fft.fftfreq(len(x), dx))
    inds = np.where(abs(psi_t) / abs(psi_t).max() > psi_tol)[0]
    k_c = max(abs(k[inds[0]]), abs(k[inds[-1]]))

    N = int(np.ceil(k_c * L / np.pi)) // 2 * 2
    dx = L / N
    x = np.arange(N) * dx - L / 2.0 + x0
    k = 2 * np.pi * np.fft.fftfreq(N, dx)
    K_ = np.fft.ifft((V.hbar * k) ** 2 / 2 / V.m * np.fft.fft(np.eye(N)))
    H_ = K_ + np.diag(V(x))
    assert np.allclose(H_, H_.T.conj())
    assert np.allclose(np.linalg.eigvalsh(H_)[0], E0)
