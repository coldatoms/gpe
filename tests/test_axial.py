from __future__ import absolute_import, division, print_function, unicode_literals
import itertools
import numpy as np

from zope.interface.verify import verifyObject, DoesNotImplement

from mmfutils.math.differentiate import differentiate
from mmfutils.math.bases import CylindricalBasis

from pytimeode import evolvers, interfaces
from pytimeode.utils.testing import TestState as _TestState

from gpe import axial, minimize

import pytest

u = axial.u


@pytest.fixture(params=[True, False])
def subtract_mu(request):
    """This "fixture" provides the argument subtract_mu to all tests that have
    this as an argument, and runs those tests with both subtract_mu=True and
    subtract_mu=False.

    See: https://docs.pytest.org/en/latest/fixture.html#fixture-parametrize
    """
    yield request.param


@pytest.fixture(params=["N", "Nab"])
def constraint(request):
    """Different types of constraint"""
    yield request.param


@pytest.fixture(
    params=[
        dict(Nxr=(_Nx, _Nx // 2), Lxr=(10 * u.micron, 5 * u.micron), twist=False, g=_g)
        for _Nx, _g in itertools.product([2**3, 3**2], [0.1, 10, 3])
    ]
)
def state(request):
    """This "fixture" provides different states to test."""
    args = dict(request.param)
    basis = CylindricalBasis(
        Nxr=args.pop("Nxr"),
        Lxr=args.pop("Lxr"),
        symmetric_x=False,
        twist=args.pop("twist"),
        boost_px=0.0,
    )

    class StateAxial(axial.bec.StateHOMixin, axial.StateAxialBase):
        pass

    state = StateAxial(basis=basis, **args)
    assert not np.allclose(0, state.get_energy())
    yield state


@pytest.fixture(
    params=[
        dict(Nxr=(_Nx, _Nx // 2), Lxr=(10 * u.micron, 5 * u.micron), twist=False, gs=_gs)
        for _Nx, _gs in itertools.product(
            [2**3, 3**2], [(0.1,) * 3, (10,) * 3, (3, 4, 5)]
        )
    ]
)
def state2(request):
    """This "fixture" provides different states to test."""
    args = dict(request.param)
    basis = CylindricalBasis(
        Nxr=args.pop("Nxr"),
        Lxr=args.pop("Lxr"),
        symmetric_x=False,
        twist=args.pop("twist"),
        boost_px=0.0,
    )

    class State2Axial(axial.bec2.StateHOMixin, axial.State2AxialBase):
        pass

    state2 = State2Axial(basis=basis, **args)
    state2[...] += 0.01
    assert not np.allclose(0, state2.get_energy())
    yield state2


def test_energy(state2):
    """Check that H(y) is the derivative of the energy."""
    np.random.seed(10)
    dx = state2.empty()
    dx.set_psi(
        np.random.random(state2.data.shape)
        + np.random.random(state2.data.shape) * 1j
        - 0.5
        - 0.5j
    )

    def f(h):
        s_ = state2.copy()
        s_[...] += h * dx
        E = s_.get_energy()
        return E

    res = differentiate(f, h0=0.001)
    assert not np.allclose(0, res)

    Hy = state2.get_Hy(subtract_mu=False)
    assert np.allclose(res, 2 * sum(dx.braket(Hy)).real)


def test_mcheck(state):
    """Test that the state can be minimized"""
    m1 = minimize.MinimizeState(state)
    assert m1.check()


@pytest.mark.skip("Expansion incomplete")
def test_sigma2s(state2):
    """Test that the calculated sigma fulfills its equation"""
    sigma2s = state2.get_sigma2s()
    abs_Phi2s = state2.get_density()
    gaa, gbb, gab = state2.gs
    w0_perp = state2.w0_perp
    ms = state2.ms
    hbar = state2.hbar

    _rhs = (
        gaa * abs_Phi2s[0] ** 2 / 4.0 / np.pi / sigma2s[0] ** 2
        + hbar**2 * abs_Phi2s[0] / 2.0 / ms[0] / sigma2s[0] ** 2
        + gab * abs_Phi2s.prod(axis=0) / np.pi / (sigma2s.sum(axis=0)) ** 2
    )
    _lhs = ms[0] * w0_perp**2 * abs_Phi2s[0] / 2.0
    assert np.allclose(_lhs, _rhs)
