from __future__ import absolute_import, division, print_function, unicode_literals
import itertools
import numpy as np

from mmfutils.math.differentiate import differentiate

from pytimeode import evolvers
from pytimeode.utils.testing import TestState as _TestState

from gpe import exact_solutions, bec, minimize

import pytest


def _get_HO_state(dim=1):
    s = exact_solutions.HarmonicOscillators(Lxyz=(17.0,) * dim, Nxyz=(46,) * dim)
    rng = np.random.default_rng(seed=10)
    s.set_psi(rng.random(size=s.shape) + rng.random(size=s.shape) * 1j - 0.5 - 0.5j)
    s.normalize()
    return s


@pytest.fixture
def state(request):
    s = exact_solutions.HarmonicOscillator()
    rng = np.random.default_rng(seed=10)
    s.set_psi(rng.random(size=s.shape) + rng.random(size=s.shape) * 1j - 0.5 - 0.5j)
    s.normalize()
    yield s


@pytest.fixture
def state2(request):
    yield _get_HO_state(dim=2)


@pytest.fixture
def state3(request):
    yield _get_HO_state(dim=2)


@pytest.fixture(params=[True, False])
def fix_N(request):
    yield request.param


class TestMinimize:
    def test_bec(self, state, fix_N):
        m = minimize.MinimizeState(state, fix_N=fix_N)
        assert m.check()

    def test_bec_Etol(self, state, fix_N):
        """Test that optimizer respects tolerance constraints."""
        m = minimize.MinimizeState(state, fix_N=fix_N)
        assert m.check()
        E_tol = 1.4e-5
        s1 = m.minimize(use_scipy=True, E_tol=E_tol)
        s2 = s1.copy()
        s2[...] = s1.psi_exact
        err = s1.get_energy() - s2.get_energy()

        # Solution should be better than tolerance
        assert abs(err) < 2 * E_tol

        # But not too much better
        assert abs(err) > E_tol / 10
        assert np.allclose(s1.get_density(), s2.get_density(), atol=2.6e-3)

    def test_regression_1(self, state, fix_N):
        """Regression in check state: fails if x is not normalized."""
        m = minimize.MinimizeState(state, fix_N=fix_N)
        x = m.x + 0.1  # Make x not normalized
        assert m.check(x)

    def test_regression_2(self, state, fix_N):
        """Regression: minimization at fixed mu fails for large density initial
        state."""
        state[...] = 10
        m = minimize.MinimizeState(state, fix_N=fix_N)
        x = m.x + 0.1  # Make x not normalized
        assert m.check(x)


class TestMinimizeFixedPhase:
    def test_bec(self, state2):
        assert len(state2.shape) > 1
        phase = np.ones_like(state2.xyz[0])
        m = minimize.MinimizeStateFixedPhase(state2, phase)
        assert m.check()
