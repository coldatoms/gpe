9 Jan 2025
==========
MMF starting review of hydro_1d code.
* Moved Experiment to `gpe/Examples/hydro_1d`

Comments for Saptarshi:
* `StateFVBase.evolve`:
  * I did not know what many arguments meant.  Please document if not obvious.
  * I refactored to use `evolve_to`.
  * Try to localize (Keep `dy = self.copy()` close to where it is used.)
  * States should use real times `t`.  Don't confuse with the dimensionless
    `t_ = t/experiment.t_unit` which is used for recording.


29 June 2024
============
Working on dr-GPE.  From StateScaleBase we have _get_V_() = Vext + Vint + Vcorr.  Can we
incorporate all of the NPSEQ stuff in Vint?

28 June 2024
============
I think that there is a bug in the tube code: the `get_sigmas()` should almost certainly
depend on `get_lambdas()`.  I am going to implement this properly, but we should go back
and see if the previous version simplifies.


27 June 2024
============
* Upon further inspection, we should increase the `mxsteps` and tolerance of the
  `odeint` integrator, but this was not a problem in the code.  We have fixed this.
* I think there is a problem with the density returned by the tube code.  Since this is
  an integrated 1D density by construction, it should only have the scaling along the x
  axis included, but currently, we have all of the scaling, including along y and z.
  
  When determining the scale factors, I think we should always solve for the three
  lambdas, but we should onlu 

There might be a problem with the densities when scaling along x.


26 June 2024
============
Working through `Tutorial/10_WritingTests.md` for tube code.
* Passing in `Lxyz` and `Nxyz` causes problems.  Since the idea is for the tube code to
  represent 3D states, we should allow these - possibly even for plotting.  We intercept
  multidimensional values and pass through only 1D sizes for specifying the `basis`.
* Potentials for tube and 3D state do not match because `st0.get_ws() != s0.get_ws()`.
  The issue is that we were using the class-variable idiom for testing, but this is not
  yet supported by the states themselves. For now, we will switch to the `Experiment`
  idiom to prevent this error.
* Something strange is happening with the minimization of the Tube state.  The minimizer
  is just returning the TF state.  After some debugging, we found that `Vext` was
  changing from run to run, which was caused by mutating a memoized array.  We have
  added code to `bec.py` to try to make the array read-only if memoizing.
  
  This also demonstrates that we are adding the potential twice in the current code if
  we inherit from `tube.StateHOMixin`.  We modify this also to consistently use
  `get_ws()` now.  We could probably simply this, removing defaults.  Will do this later
  when we add `experiment` support formally.
* When executing the time-dependence, I ran into a bug if `get_ws()` returns a list or
  tuple.  Wrapping this in the code now.
* When executing the time dependence, something is off.
  

20 May 2024
===========
* Updated documentation about NPSEQ and Sigmas.  Implemented first version in `tube.py`,
  but needs more thorough testing.  Specifically:
  1. Check that the factor of `lam_perp2` can be simply applied.
  2. Check the minimization with `minimize_chemical_potential` flag.
  3. Compare results with and without this flag.  Make sure it works.

5 May 2024
==========
* Used CSV Export/Import to move issues from Heptapod to GitLab.
* Noted https://gitlab.com/coldatoms/gpe/-/issues/4.  Where should the chemical
  potential be set for initial states?
  * `minimize` will assume this is done in `get_Vext()`.
  * `set_initial_state` calls `get_n_TF`.
  
  Here is my proposal:
  
  1. If `t<0` (or `t is None`), then the state should subtract `mu` in `_get_Vext_()`.
  2. Set `t=None` initially so that calling `minimize()` on a state will do the right
     thing.  This will be set to `0` in `pre_evolve_hook()`.  Part of this change
     required adding `pre_minimize_hook()` to `IStateMinimizeGPE` which does the same as
     `pre_evolve_hook()` but does not set `t`.
  3. Provide a method `get_t()` which returns a number.  This is used, for example, for
     plotting, or in a moving frame where the frame should be at `v*t`.
  4. The functions used by `set_initial_state()` -- `get_mu_from_V_TF()`,
     `get_V_TF_from_mu()`, and `get_n_TF()` --  will now raise a warning if `t is not
     None` and will assume that `self.mu` is subtracted.
     

9 February 2024
===============
* Clarify meaning of `t__image`, `t__final`, and `times_`.  These are now described in
  `gpe/interfaces.py`.

12 December 2023
================
* Start work on 0.3
* Pin Python<3.12
* Start working on new GPU interface - instead of sundering, we simply pass a gpu=True
  argument.  This means we must deprecate the _xyz_ property for GPU use.  Will keep
  .xyz for plotting.

15 June 2023
============
* Marked memory tests with `mem` and skip.  I am getting very inconsistent results on
  Mac OS X:
  - [Measuring memory usage in Python: it’s tricky!](
     https://pythonspeed.com/articles/measuring-memory-python)
 
12 June 2023
============
Starting general cleanup.
* Added Makefile and environment for tests.  First we will get tests passing, then move
  to PDM or Poetry.  Just run `make test`, or `make shell` then `pytest`.
* Several spurious dependencies are breaking things.
  * No `piston` module in `gpe/disc.py`.  Where is this?
  

28 March 2018
=============

* Refactoring `soc.py` to allow for Axial and 3D simulations.  Running into
  several design decisions. 
  - Since Axial bases do not have y and x components, we restrict the SOC to
    the x direction.
  - We still need expansion - so must refactor this out of tube.
  - Transverse sizes are currently determined by the trapping frequencies.
    This will probably break the SOC vortex stuff but that should probably be
    implemented separately.
    
16 April 2018
=============

* I think that the chemical potentials are properly being set.
* Initial ansatz for axial states is not good.  Can we use the Gaussian defined
  by tube's sigma? 
* Still an issue with `soc_catch_and_release` for Small axial tubes - Radial 
  cloud is probably not properly set because radial extent is underestimate.

17 April 2018
=============

* There was an issue with setting the fiducial states to get the chemical potential.
  The issue is that we have three different potentials:

  `V_exp`: External potential w/o `Vt` for experiment.  This is where `x_TF` means
     something: V_exp(x_TF) is used in the TF approximation to get the initial 
     density.  This is called the "fiducial" potential.  In the code it has
     `expt==True` and `fiducial==True` 
  `V_exp + Vt`: This includes the time-dependent potential which may be used to
     find the initial state.  Experimentally, one often starts with V_exp, then
     "adiabatically" transfers the cloud to the ground state with Vt.  The
     particle number might drop sightly.  The chemical potential here will need
     to be readjusted in order to keep the particle number fixed to the same
     value established without Vt.  In the code this has expt==True and
     fiducial==False.
  `V_ext + Vt`: The third potential is present when using a small box
     (i.e. `cells_x`).  In this case we should be fixing the chemical potential
     in terms of `V_exp + Vt`, but since the box is restricted, we cannot adjust
     the chemical potential to fix N.  In the code this has `expt==False`.

  Another complication is that, to accurately fix the chemical potential in the
  `V_exp + Vt` case, we may need a bigger box than is actually used.  To deal
  with this we create an expt_state which is full size.

5 May 2018
==========
Updating Simulation class to allow better interactive use:
* Allow running simulations interactively in a notebook with display.
* Save simulation data to a temporary folder by default.
* Allow user to mange simulation data - deleting it or moving it to a permanent
  location.

<Incomplete>

9 May 2018
==========
The -200nK axial simulations seem to be giving too wide of a separation.  Also,
the expansion on the SOC side seems to be *faster*!

I am having some simulation issues with the expansion in the axial case - for
some reason the full simulations Run0_200_axial_small are failing to converge
during imaging.  The regular evolution seems fine, but I needed to reduce the
`dt_t_scale` to 0.005 and was still having some issues for `t=9` or `t=10`.
Strangely, evolution of the small systems works in `Expanding_Bubbles` works fine
with `dt_t_scale=0.2` even.

I am breaking off a small set of runs to reproduce these results in
`runs_catch_and_release_small.py`.  For speed, we start with `t=0ms` and `t=1ms`
expansions.


10 May 2018
===========
The previous issues are demonstrated in the Debugging.ipynb notebook.  Evolving
from 0 to 3ms is fine, but loading the 2ms expansion causes problems with
evolution.

The issue is that E_max is scaled by the expansion.  This is a bug because of
the K_factor hack we are using... this was fixed in e88c8afd9c4d.

Doing small runs:
 * runs_catch_and_release_small.RunAxial200() fails with Jacobian error after
   12 bad tries.  Manually setting tries=10 in soc.py but this should be fixed.
   UPDATE: This should be fixed now with version c11fda4760bd of mmfutils and
   version b138a2f35069 of gpe.
   
**INCOMPLETE**
Another issue with the axial code is the energy_density which is not correctly
calculated.  (Energy should be conserved when expanding.)  After trying several
things here, I have not yet succeeded but it is not clear why.  Maybe something
to do with the corrections to V_ext?


15 May 2018
===========
* Factored out the registration code so that it can be used for simulations too
  (this is in version 9610d8a6a1ed of the paper project).
* Note: location of the barrier might vary by as much as 5micron.

The current data does not match the experiment (-200nK barrier depth).  In
particular:
* The t_=0 frame expands too much.
* Expansion speeds are too large ~3.1mm/s:
  Maren has:
  * SOC:   1.780(17) and -2.355(28) mm/s
  * noSOC: 2.203(18) and -2.328(26) mm/s
* Density contrast is too large.

As a starting point:
* The t_=0 frame expands to about 18(1)micron peak-peak.
* The peaks are about 1.3(1) * n0 and the central trough is about 0.7*n0, and
  the difference is about 0.65(5)*n0

* Playing a bit with the tube code, it seems that a barrier height of -50nK
  should reproduce the 18micron expansion.

16-17 May 2018
==============
* Confirm barrier width and depth.  Previous versions of code had correct
  formula for barrier_potential with barrier_width_micron=4.8 but power
  conversions were wrong (previous -200nK were ~ -45nK).
* Make sure that small experiments default to underlying experimental values by
  default (i.e. rely on kw rather than explicit values.)
 
11 June 2018
============
* I though a possible explanation for different speeds seen in Maren's data
  might be due to the registration of the cloud center.  With Nb/Na = va/vb = 0.028,
  one has zero total momentum when va = 0.33mm/s, however, this motion should
  appear in all frames giving a registration shift of 3.3microns after 10.1ms
  of expansion.  Since the velocity measured compares only the relative
  positions, this should cancel out.

20 June 2018
============
* Working on gpe/soc.py::State1 for SOC with modified dispersion.  Documenting
  the use in Docs/SOC.ipynb.  I am running into some issues with the initial
  state preparation which seems overly complex.  Revisiting to see if we can
  make this simple again.

  * One thing that is complicating stuff is the search for time-dependent
    parameters in the `Experiment.init()` which uses
    `inspect.getmembers(self, predicate=callable)`.  This "gets" `fiducial_V_TF`
    triggering an early call to `get_fiducial_V_TF()`.  This is now preempted.
  * I am also trying to make bec.py and bec2.py mirror each other better, so I
    am renaming the key methods so that they match:

    `get_Vs_TF()` -> `get_V_TF()`

* I tried implementing Maren's procedure of quenching into a state with SOC to
  measure the frequency of rotation on the Bloch sphere in
  `SOC/Catch and Release.ipynb`. This does not match the expectation: in
  particular, as Omega -> 0 with delta=0 one still has w=4E_R/hbar.

  * Made code a bit more robust with respect to these issues, but still have
    not resolved it.

21 June 2018
============
* Resolved issue with Maren's quenching procedure: the ground state with no
  phases corresponds to a state with k=-k_r in the rotating phase basis.  With
  this included in the Bloch sphere picture, I get the correct frequency.
  
